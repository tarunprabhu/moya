\documentclass[10pt]{scrartcl}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{lmodern}
\usepackage{mdframed}
\usepackage{subcaption}
\usepackage{url}
\usepackage{wrapfig}

\author{Tarun Prabhu}
\title{Moya -- User Manual}

\newcommand{\annotation}[1]{\texttt{#1}}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\cmd}[1]{\texttt{#1}}
\newcommand{\varname}[1]{\texttt{#1}}

\begin{document}
\maketitle

Moya is a programmer-guided JIT-compiler for C, C++ and Fortran. \\

At a high level, the workflow for using Moya is as follows:

\begin{enumerate}
\item The programmer adds annotations (structured comments) to the source
  code specifying which parts of the code should be JIT compiled.

\item She then writes a configuration file which Moya will use to prepare the
  code for analysis and also to build the executable. 
  
\item The programmer then calls Moya which processes the annotations, performs
  a static analysis and builds the application. 
\end{enumerate}

More details about each step in this process can be found in Section
\ref{sec:usingmoya}. Moya consists of three main components:

\begin{itemize}
\item \textbf{Aeryn}: This is a tool which performs the whole-program static
  analysis on the input code. It is purely a compile-time tool.
  It generates an XML file (which will be referred
  to as the metadata file in the remainder of this manual)
  and other LLVM bitcode
  files which will be used by Talyn to perform JIT compilation.

\item \textbf{Talyn}: Talyn is the runtime component which actually performs the
  JIT-compilation and is responsible for managing the generated code.

\item \textbf{Pilot}: Pilot is the annotation processor which reads the
  programmer annotations, prepares the code for Aeryn to analyze and
  inserts the calls to Talyn in the code. 

\end{itemize}


The following paragraph is a somewhat more technical overview of what Moya
does. It may be helpful in understanding Moya's capabilities and limitations. \\

The main runtime optimization that Moya performs is dynamic constant propagation.
In order to enable this, it performs a whole-program mutability analysis and
identifies dynamic constants in the program. Dynamic constants are those
objects in
the program (which may be primitives such as ints or floats) which are
constant within a JIT region. A region is a subgraph within the callgraph
of a program which we are interested in JIT'ing. 
The programmer demarcates regions using annotations. Not all the functions
which are called
within a region are JIT'ted - the programmer must explicitly specify which
functions must be JIT'ted (this is in order to reduce the compilation overhead
by ignoring functions which might not benefit from JIT-compilation). This is
also done using annotations.
After performing the static analysis, Moya uses the annotations to insert
calls to the JIT-compiler which, at runtime, performs dynamic constant
propagation
and other optimizations and executes this runtime-generated, optimized code. \\


\section{Building Moya}

\subsection{Setting up dependencies}
\label{sec:dependencies}

Moya has some dependencies which are listed below. Moya has only been
tested with the versions of the libraries specified. If the exact
version is absolutely necessary, this will be made clear. We also provide
instructions on how to build these dependencies. Note that these instructions
are incomplete. They only specify the major configure/build options which
\text{must} be used. For more instructions, refer to the documentation of the
respective tools. \\

\subsection{Flang}
Flang is the only supported Fortran compiler. A version of Flang that is
patched to support Moya annotations is included in the Moya source code and
will be downloaded automatically when you checkout the source. 

\subsubsection{Python 3}
Python 3.3 or higher is required. 

\subsection{GNU Gold Linker}
The gold linker must be present on the system. 

\subsection{PAPI}
Moya may work without PAPI but this has not been tested.

\subsection{Getting and Building Moya}

Moya can be obtained from \url{https://bitbucket.org/tarunprabhu/moya}. 

\begin{figure}[h!]
  \begin{tabular}{l}
    \cmd{\$ git clone --recursive \url{git@bitbucket.org:tarunprabhu/moya/} moya} \\
  \end{tabular}
\end{figure}

Instructions to download the dependencies and to build Moya are provided in the
moya/INSTALL file. 


\section{Using Moya}
\label{sec:usingmoya}

This section expands on the high level overview already presented before. 

\subsection{Annotating the code}

The annotations in Moya are grouped into two classes: control annotations and
enabling annotations. The control-annotations
are used to specify when the JIT compiler should run.
The enabling annotations are used to demarcate regions
and specify which functions should be JIT-compiled.  \\


We first describe the enabling annotations:

\begin{itemize}[label={}]
\item \textbf{\annotation{!\$moya specialize}} \\[1mm]
  This marks a function to be JIT'ted. It must be inserted immediately before
  the function definition. 
  Functions which are called within a region but which are not marked with
  this annotation will not be JIT'ted. If a marked function is never called
  within a region, it will never be JIT'ted. If a marked function is called
  outside a JIT'ted region, the statically compiled function will be used
  even if a JIT'ted version exists. \\
  
\item \textbf{\annotation{!\$moya jit}} \\[1mm]
  This marks the start of a JIT region. \\

\item \textbf{\annotation{!\$moya end jit}} \\[1mm]
  This marks the end of a JIT region. 
  In addition the region must respect the restrictions presented in
  Section \ref{sec:regionrestrictions}. \\

\end{itemize}

\begin{wrapfigure}{t!}{0.5\textwidth}
  \begin{mdframed}
  \centering
  \begin{lstlisting}[language=Fortran]
   subroutine fn1(i)    
     implicit none
     integer :: i

     !$moya jit
     if(i .gt. 0) then
       call do_this()
     else
       call do_that()
     end if
     !$moya end jit
   end subroutine fn1
    
   !$moya specialize
   subroutine do_this
    
   end subroutine do_this

  \end{lstlisting}
  \end{mdframed}
  \vspace{-3mm}
  \caption{Example use of annotations}
  \label{fig:example}
  \vspace{-12mm}
\end{wrapfigure}

Figure \ref{fig:example} is an example of how the annotations are used in
code.

\subsubsection{Restrictions on JIT-Regions}
\label{sec:regionrestrictions}

The annotation processor replaces some annotations with function calls. This
is crucial to understanding the limitations on marking JIT regions. For the
remainder of this section, assume that every \annotation{jit} annotation
is replaced with a call to \code{moya\_begin()} and every \annotation{end jit}
annotation is replaced with \code{moya\_end()}.

For a region to be valid, \textit{both} of the following conditions must hold:

\begin{enumerate}
\item For each distinct \annotation{regionid}, $i$,
  \code{moya begin()} must dominate \code{moya\_end()}. Therefore, the
  \annotation{jit} annotation must dominate the
  \annotation{end jit} annotation.
  (In a control-flow graph, a node $d$ dominates
  a node $n$ if every path from the entry node to $n$ must go through $d$. In
  other words, any time \code{moya\_end(i)} is called, \code{moya\_begin(i)}
  must have been called before it).
  %% TODO: Cite Wikipedia and the textbook
\item For each distinct \annotation{regionid}, $i$,
  \code{moya\_end()} must post-dominate \code{moya\_begin()}. Therefore, the
  \annotation{end jit} annotation must post-dominate the
  \annotation{jit} annotation.
  (In a control-flow graph, a node $z$ post-dominates a node $n$, if every path
  from $n$ to the exit node must go through $z$. In other words, any time
  \code{moya\_begin(i)} is called, \code{moya\_end(i)} must be called
  before the program terminates).
\end{enumerate}

  Figures \ref{fig:goodregions} contains
examples of valid regions. Figures \ref{fig:badregions},
contains examples of invalid regions. Figure \ref{fig:badregion1} is invalid
because there is a path through the code where \code{moya\_end(0)} could be 
called without having called \code{moya\_begin(0)}. This can occur if \code{n}
$<$ 1. On the other hand in Figure \ref{fig:badregion2}, if \code{n} $<$ 1,
\code{moya\_end(0)} will never be called. 

\begin{figure}[t]
  \begin{mdframed}
  \centering
  \begin{subfigure}{0.32\textwidth}
    \begin{lstlisting}[language=Fortran]
do i = 1, n
  ! $moya jit
  call do_this()
  call do_that()
  call do_other()
  ! $moya end jit
end do      
    \end{lstlisting}
    \caption{\space}
    \label{fig:goodregion1}
  \end{subfigure}
  \begin{subfigure}{0.32\textwidth}
    \begin{lstlisting}[language=Fortran]
! $moya jit
do i = 1, n
  call do_this()
  call do_that()
  call do_other()
end do      
! $moya end jit
    \end{lstlisting}
    \caption{\space}
    \label{fig:goodregion2}
  \end{subfigure}
  \begin{subfigure}{0.32\textwidth}
    \begin{lstlisting}[language=Fortran]
!$moya jit
if(i .gt. 0) then
  call do_this()
else
  call do_that()
end if
!$moya end jit
    \end{lstlisting}
    \caption{\space}
    \label{fig:goodregion3}
  \end{subfigure}
  \end{mdframed}
  \caption{Valid regions}
  \label{fig:goodregions}
\end{figure}


\begin{figure}[t]
  \begin{mdframed}
    \centering
    \begin{subfigure}{0.32\textwidth}
      \begin{lstlisting}[language=Fortran]
do i = 1, n
  !$moya jit
  call do_this()
  call do_that()
  call do_other()
end do
!$moya end jit
      \end{lstlisting}
      \caption{\space}
      \label{fig:badregion1}
    \end{subfigure}
    \begin{subfigure}{0.32\textwidth}
      \begin{lstlisting}[language=Fortran]
!$moya jit
do i = 1, n
  call do_this()
  call do_that()
  call do_other()
  !$moya end jit
end do      
      \end{lstlisting}
      \caption{\space}
      \label{fig:badregion2}
    \end{subfigure}
    \begin{subfigure}{0.32\textwidth}
      \begin{lstlisting}[language=Fortran]
if(i .gt. 0) then
  !$moya jit
  call do_this()
else
  call do_that()
end if
!$moya end jit
      \end{lstlisting}
      \caption{\space}      
      \label{fig:badregion3}
    \end{subfigure}
  \end{mdframed}
  \caption{Invalid regions}
  \label{fig:badregions}
\end{figure}


\section{Building a Moya-enabled application}
\label{sec:buildingapplication}

In the application's build system, use Moya's compilers. This might involve
setting the FC environment variable or the -DCMAKE\_Fortran\_COMPILER flag.
Figure \ref{fig:building} gives an example of how this might be done. 

\begin{figure}
  \cmd{\$ CC=/path/to/moya-cc FC=/path/to/moya-fort ./configure ...}\\
  \cmd{\$ cmake -DCMAKE\_C\_COMPILER=/path/to/moya-cc -DCMAKE\_Fortran\_COMPILER=/path/to/moya-fort ...}
\end{figure}

\section{Running a Moya-enabled application}
\label{sec:runningapplication}

Nothing special needs to be done when running a Moya-enabled application.
We present a high-level overview of the control-flow
at runtime of such an application. Figure \ref{fig:moya} is a graphical
representation of the same. 

\begin{enumerate}
\item The program begins executing as ``normal'' (as if Moya were not used and
  no JIT compilation was to be performed).

\item Control reaches the point where we had inserted a \annotation{jit}
  annotation. At this point, control ``switches'' to Moya. From here on, until
  the corresponding \annotation{end jit} annotation is encountered, JIT compilation
  may be performed and the JIT'ted code executed.

\item Once the \annotation{end} annotation is encountered, program execution
  reverts to executing ``normally''.

\item The previous two steps may occur repeatedly before the program terminates.
\end{enumerate}

\begin{figure}[t]
  \begin{mdframed}
    \centering
    \includegraphics[scale=0.4]{figures/thenew.eps}
  \end{mdframed}
  \caption{Moya-enabled application control-flow}
  \label{fig:moya}
\end{figure}



\section{Caveats}
\label{sec:caveats}

Moya is still under heavy development. It is likely to be very buggy and its
internal structure and capabilities are constantly evolving. There are several
features which are partially completed. This section contains some of the
limitations and issues which should be kept in mind. \\

\subsection{Library-Awareness}
\label{sec:libraryawareness}

Library functions are
defined as those functions whose code is unavailable. For instance, in the case
of Fortran code compiled using the GNU Fortran compiler, several libgfortran
library functions are likely to be used in the code. \\

Since Moya relies heavily on the static analysis tool, Aeryn, for correctness,
Aeryn needs to be library-aware. Aeryn performs a flow-insensitive,
context-insensitive mutability analysis. For this analysis to be correct, Aeryn
needs to know if library functions modify any data (this is only relevant when
pointers are passed to the library functions). We have built in support for
several libraries into Moya. Specifically, Moya currently is aware of:

\begin{itemize}
\item MPI
\item libflang
\item libc
\item libm (Math library)
\item LLVM
\item libz
\item Linux Syscalls
\item hwloc
\item libomp
\item liblomp
\item Moya (Moya is aware of itself)
\end{itemize}

\textbf{NOTE}: The support for all of these libraries is incomplete.
Not all the routines
provided by these libraries have been modeled. If you encounter a function which
has not been modeled, please contact Moya's developers. \\

If library functions are encountered for which no models exist, Aeryn will
conservatively assume that all the arguments to the functions are modified.
While this is correct, the performance after JITting may be no better than the
performance without JITting. \\

\textbf{NOTE}: Currently we do not provide any way for the user to build their
own library models. The only way to do this is to modify Moya's source code.
The code aeryn/models/Lib*.cpp can be used as templates for adding support for a
new library. \\

\subsection{C/C++ Support}
\label{sec:cc++support}

Currently, C and C++ are not supported. However, you may still build an
application which consists of C and Fortran code (C++ might also work, but
this has not been tested) as long as C code is not called within the JIT region.
However, Aeryn's static analysis may not work
correctly for C code, especially if the C code takes the address of variables
or performs any form of pointer arithmetic that is not simple array
dereferencing. \\

\textbf{NOTE}: Use of Fortran 2003 \code{ISO\_C\_Binding} is not supported. \\

\subsection{MPI}
\label{sec:mpisupport}

Currently, only MPICH and OpenMPI are supported. 
\end{document}
