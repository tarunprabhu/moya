subroutine pp(me, other)

  use mpi

  integer :: me, other
  integer :: status(MPI_STATUS_SIZE)
  integer :: sreq, rreq, ierr

  call mpi_isend(me, 1, MPI_INT, mod(me+1, 2), 0, MPI_COMM_WORLD, sreq, ierr)
  call mpi_irecv(other, 1, MPI_INT, mod(me+1, 2), 0, MPI_COMM_WORLD, rreq, ierr)

  call mpi_wait(sreq, status, ierr)
  call mpi_wait(rreq, MPI_STATUS_IGNORE, ierr)

end subroutine pp

program pingpong

  use mpi

  integer :: me, other
  integer :: ierr
  
  call mpi_init(ierr);
  call mpi_comm_rank(MPI_COMM_WORLD, me, ierr);

  !$moya jit
  call pp(me, other);
  !$moya end jit

  call mpi_finalize(ierr);

  
end program pingpong
