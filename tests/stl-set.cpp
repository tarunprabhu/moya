#include <set>
#include <iostream>
#include <cmath>

using namespace std;

typedef float T;
typedef set<T> Container;

void constructors() {
  Container s1;
  Container s2({1, 2, 3, 4});
  Container s3(s2);
  Container s4(s1.begin(), s1.end());
}

void operators(Container& s) {
  Container s5;
  Container s6;

  s6 = {*s.begin()};
  s5 = s6;
}

void allocators(Container& s) {
  s.get_allocator();
}

void iterators(Container& s) {
  auto b = s.begin();
  auto e = s.end();
  auto cb = s.cbegin();
  auto ce = s.cend();
  auto rb = s.rbegin();
  auto re = s.rend();
  auto crb = s.crbegin();
  auto cre = s.crend();

  std::cout << *b << " " << *e << " " << *cb << " " << *rb << " " << *re << " "
            << *crb << " " << *cre << "\n";
}

void iterators_math(Container& s) {
  Container::iterator it = s.begin();
  Container::iterator jt = it;
  Container::reverse_iterator rit = s.rbegin();
  Container::reverse_iterator rjt = rit;

  cos(*it);
  ++it;
  it++;
  --it;
  it--;
  tan(*jt);
  for(T t : s)
    sin(t);

  acos(*rit);
  ++rit;
  rit++;
  --rit;
  rit--;
  atan(*rjt);
}

void iterators_comp(Container& s) {
  Container::iterator it = s.begin();
  Container::iterator jt = s.end();
  Container::reverse_iterator rit = s.rbegin();
  Container::reverse_iterator rjt = s.rend();

  sinh(it == jt);
  asinh(it != jt);
  cosh(rit == rjt);
  acosh(rit != rjt);
}

void capacity(Container& s) {
  s.empty();
  s.size();
  s.max_size();
}

void modifiers(Container& st) {
  Container s({50, 51});
  
  s.clear();

  s.insert(13);
  s.insert(s.begin(), 14);
  s.insert(st.begin(), st.end());
  s.insert({9, 10, 11, 12});

  s.erase(s.begin());
  s.erase(s.begin(), s.end());
  s.erase(13);
}

void swap(Container& s) {
  Container st;
  st.swap(s);
}

void lookups(Container& s, T element) {
  s.count(element);
  s.find(element);
  s.equal_range(element);
  s.lower_bound(element);
  s.upper_bound(element);
}

void observers(Container& s) {
  // s.key_comp();
  // s.value_comp();
}

int main(int argc, char* argv[]) {
  T element = argc;
  Container s({element});

  constructors();
  allocators(s);
  operators(s);
  iterators(s);
  iterators_math(s);
  iterators_comp(s);
  capacity(s);
  modifiers(s);
  // swap(s);
  lookups(s, element);
  observers(s);

  return 0;
}
