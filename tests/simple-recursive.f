!$moya specialize  
      function rec(n) result(r)
      r = n
      end function rec

      program simple

!$moya jit      
      m = rec(3)
!$moya end jit
      end program simple
