program inner
  implicit none
  integer :: m

  m = 4
  !$moya jit
  call func(3)
  !$moya end jit
contains
  !$moya specialize
  subroutine func(n)
    implicit none
    integer :: n
    write(*, '(I4)') n+m
  end subroutine func
end program inner
