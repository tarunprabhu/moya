#include <stdio.h>
#include <stdlib.h>

int fpub(int);
int gpub(int);

int main(int argc, char *argv[]) {
  int m = argc > 1 ? atoi(argv[1]) : 42;
  int n = argc > 2 ? atoi(argv[2]) : 73;

#pragma moya jit
  printf("%d\n", fpub(m) + gpub(n));

  return 0;
}

