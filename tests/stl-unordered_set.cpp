#include <unordered_set>
#include <cmath>

using namespace std;

typedef int T;
typedef unordered_set<T> Container;

void constructors() {
  Container s1;
  Container s2({1, 2, 3, 4});
  Container s3(s2);
  Container s4(s3.begin(), s3.end());  
}

void operators(Container& s) {
  Container s5;
  Container s6;

  s6 = {5, 6, 7, 8};
  s5 = s6;
}

void iterators(Container& s) {
  s.begin();
  s.end();
  s.cbegin();
  s.cend();
}

void iterators_math(Container& s) {
  Container::iterator it = s.begin();
  Container::iterator jt = it;

  cos(*it);
  ++it;
  it++;

  tan(*jt);
  for(T t : s)
    sin(t);
}

void iterators_comp(Container& s) {
  Container::iterator it = s.begin();
  Container::iterator jt = it;

  acos(it == jt);
  asin(it != jt);
}

void capacity(Container& s) {
  s.empty();
  s.size();
  s.max_size();
}

void modifiers(Container& s) {
  Container st({50, 51});
  
  s.clear();

  s.insert(13);
  s.insert(s.begin(), 14);
  s.insert(st.begin(), st.end());
  s.insert({9, 10, 11, 12});

  s.erase(s.begin());
  s.erase(s.begin(), s.end());
  s.erase(13);
}

void swap(Container& s) {
  Container st;
  st.swap(s);
}

void lookups(Container& s, T element) {
  s.count(element);
  s.find(element);
  s.equal_range(element);
}

void buckets(Container& s, T element, int bucket) {
  s.begin(bucket);
  s.end(bucket);
  s.cbegin(bucket);
  s.cend(bucket);

  s.bucket_count();
  s.max_bucket_count();
  s.bucket_size(bucket);
  s.bucket(element);
}

void buckets_iter_ops(Container& s, int bucket) {
  auto it = s.begin(bucket);
  auto jt = it;

  cosh(*it);
  ++it;
  it++;

  tanh(*jt);
  for(T t : s)
    sinh(t);

  log(it == jt);
  log2(it != jt);
}

void hashes(Container& s, int count, float max_load_factor) {
  s.load_factor();

  s.max_load_factor();
  s.max_load_factor(max_load_factor);

  s.rehash(count);

  s.reserve(count);
}

void observers(Container& s) {
  // s.hash_function();
  // s.key_eq();
}

int main(int argc, char* argv[]) {
  Container s;
  int count = argc;
  T element = argc;
  float max_load_factor = argc;
  int bucket = argc;

  constructors();
  operators(s);
  capacity(s);
  iterators(s);
  iterators_math(s);
  iterators_comp(s);
  modifiers(s);
  lookups(s, element);
  buckets(s, element, bucket);
  buckets_iter_ops(s, bucket);
  hashes(s, count, max_load_factor);
  observers(s);

  return 0;
}
