#include <iostream>

using namespace std;

class Class {
private:
  int m;

protected:
  int get() const { return m; }
  
public:
  Class(int m) : m(m) { ; }
  int add1() { return get() + 1; }
};

int main(int argc, char* argv[]) {
  Class obj(argc);

  cout << "m: " << obj.add1() << "\n";
  
  return 0;
}
