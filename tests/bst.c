#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct NodeT {
  int val;
  struct NodeT *left;
  struct NodeT *right;
} Node;

Node* make(int val) {
  Node *n = (Node*)malloc(sizeof(Node));
  n->val = val;
  n->left = NULL;
  n->right = NULL;
  return n;
}

void unmake(Node *root) {
  if(root) {
    unmake(root->left);
    unmake(root->right);
    free(root);
  }
}

void insert(Node *root, Node *n, Node *ins, int dir) {
  if(root) {
    if(n->val < root->val)
      insert(root->left, n, root, 1);
    else
      insert(root->right, n, root, 2);
  } else {
    if(dir == 1)
      root->left = n;
    else if(dir == 2)
      root->right = n;
  }
}

#pragma moya specialize
int search(Node *root, int key) {
  if(root) {
    if(root->val == key)
      return 1;
    else if(key < root->val)
      return search(root->left, key);
    else if(key > root->val)
      return search(root->right, key);
  }
  return 0;
}

int main(int argc, char *argv[]) {
  int n = atoi(argv[1]);
  Node *root = make(atoi(argv[2]));
  for(unsigned i = 3; i < argc; i++)
    insert(root, make(atoi(argv[i])), NULL, 0);
  int key = atoi(argv[argc]);

#pragma moya jit
  {
  printf("search: %d\n", search(root, key));
  }

  unmake(root);
  return 0;
}
