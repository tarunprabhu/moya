#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct MyStructT {
  int *buf;
  int size;
} MyStruct;

void init_a(MyStruct *a, int n) {
  a->buf = (int*)malloc(sizeof(int)*n);
  a->size = n;
}

void init_b(MyStruct *b, int n) {
  b->buf = (int*)malloc(sizeof(int)*n);
  b->size = n;
}

void init_c(MyStruct *c, int n) {
  c->buf = (int*)malloc(sizeof(int)*n);
  c->size = n;
}

void init_d(MyStruct *d, int n) {
  d->buf = (int*)malloc(sizeof(int)*n);
  d->size = n;
}

#pragma moya specialize
void print(MyStruct *s) {
  for(int i = 0; i < s->size; i++)
    printf("%d ", s->buf[i]);
  printf("\n");
}

int main(int argc, char *argv[]) {
  MyStruct a, b, c, d;
  int n = atoi(argv[1]);
  init_a(&a, n);
  init_c(&c, n);

  memcpy(&b, &a, sizeof(MyStruct));
  memcpy(&d, &c, sizeof(MyStruct));

#pragma moya jit
  {
  print(&b);
  print(&d);
  }

  free(a.buf);
  free(b.buf);
  free(c.buf);
  free(d.buf);
  
  return 0;
}
