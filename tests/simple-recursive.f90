module tmp
contains
  !$moya specialize
  subroutine rec(n)
    implicit none
    integer(4) :: n
    n = n + 1
  end subroutine rec
end module tmp

program simple_recursive
  use tmp

  !$moya jit
  call rec(23)
  !$moya end jit
end program simple_recursive
