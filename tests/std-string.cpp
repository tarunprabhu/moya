#include <string>
#include <iostream>
#include <cmath>

using namespace std;

void constructors(int count, int pos, char c) {
  string s1;
  string s3(count, c);
  string s4(s3, pos, count);
  string s5("hello", 5);
  string s6("world");
  string s7(s3);
  string s8({'a', 'b', 'c'});
  string s9(s3.begin(), s3.end());
}

void allocators(string& s) {
  s.get_allocator();
}

void operators(string& s, char c) {
  string st;

  st = s;
  st = "baz";
  st = c;
  st = {'d', 'e', 'f'};
}

void assign(string& s, int count, int pos, char c) {
  string st;
  
  st.assign(count, c);
  st.assign(s);
  st.assign(s, pos, count);
  st.assign("foo", 3);
  st.assign("bar");
  // s.assign(s.begin(), s.end());
  st.assign({'g', 'h', 'i'});
}

void access(string& s, int pos, char c) {
  s.at(pos);

  s[pos];
  s[pos] = c;

  s.front();
  s.back();
  s.data();
  s.c_str();
}

void iterators(string& s) {
  auto ib = s.begin();
  auto ie = s.end();
  auto icb = s.cbegin();
  auto ice = s.cend();
  auto irb = s.rbegin();
  auto ire = s.rend();
  auto icrb = s.crbegin();
  auto icre = s.crend();

  std::cerr << *ib << " " << *ie << " " << *icb << " " << *ice << " " << *irb
            << *ire << " " << *icrb << " " << *icre << "\n";
}

void iterators_math(string& s, int count) {
  string::iterator it = s.begin();
  string::iterator jt = s.end();

  cos(*it);
  ++it;
  it++;
  --it;
  it--;
  jt = it + count;
  jt = it - count;
  it += count;
  it -= count;
  tan(it[count]);

  it.base();
  log(*jt);
  for(char t : s)
    sin(t);
}

void iterators_comp(string& s) {
  string::iterator it = s.begin();
  string::iterator jt = s.end();

  sinh(it == jt);
  asinh(it != jt);
  cosh(it < jt);
  acosh(it <= jt);
  tanh(it > jt);
  atanh(it >= jt);
}

void capacity(string& s, int count) {
  s.empty();
  s.size();
  s.max_size();

  s.reserve(count);
  s.capacity();
  s.shrink_to_fit();
}

void modifiers(string& st, int count, int pos, char c) {
  char* cstr = nullptr;
  string s;
  
  s.clear();

  s.insert(pos, count, c);
  s.insert(pos, "this");
  s.insert(pos, "is", 2);
  s.insert(pos, s);
  s.insert(st.begin(), c);
  s.insert(st.begin(), count, c);
  s.insert(s.begin(), st.begin(), st.end());
  s.insert(s.begin(), {'t', 'e', 's', 't'});

  s.erase(pos, count);
  s.erase(s.begin());
  s.erase(s.begin(), s.end());

  s.push_back(c);
  s.pop_back();

  s.append(count, c);
  s.append(s);
  s.append(s, pos, count);
  s.append("boo", 3);
  // s.append(s.begin(), s.end());
  s.append({'h', 'u', 'r', 'r', 'a', 'y'});

  s += s;
  s += c;
  s += "blah";
  s += {'y', 'i', 'k', 'e', 's'};

  s.compare(s);
  s.compare(pos, count, s);
  s.compare(pos, count, s, pos, count);
  s.compare("oops!");
  s.compare(pos, count, "bam!");
  s.compare(pos, count, "whoa", 4);

  s.replace(pos, count, s);
  s.replace(s.begin(), s.end(), s);
  s.replace(pos, count, s, pos, count);
  s.replace(s.begin(), s.end(), st.begin(), st.end());
  s.replace(pos, count, "lorem", 5);
  s.replace(s.begin(), s.end(), "ipsum", 5);
  s.replace(pos, count, "dolor");
  s.replace(s.begin(), s.end(), "sic");
  s.replace(pos, count, count, c);
  s.replace(s.begin(), s.end(), count, c);
  s.replace(s.begin(), s.end(), {'a', 'm', 'e', 'r'});

  s.substr(pos, count);

  s.copy(cstr, count, pos);

  s.resize(count);
  s.resize(count, c);
}

void find(string& s, int count, int pos, char c) {
  s.find(s, pos);
  s.find("Peter", pos, count);
  s.find("Piper", pos);
  s.find(c, pos);

  s.rfind(s, pos);
  s.rfind("picked", pos, count);
  s.rfind("peck", pos);
  s.rfind(c, pos);

  s.find_first_of(s, pos);
  s.find_first_of("pickled", pos, count);
  s.find_first_of("pepper", pos);
  s.find_first_of(c, pos);

  s.find_first_not_of(s, pos);
  s.find_first_not_of("she", pos, count);
  s.find_first_not_of("sells", pos);
  s.find_first_not_of(c, pos);

  s.find_last_of(s, pos);
  s.find_last_of("pickled", pos, count);
  s.find_last_of("pepper", pos);
  s.find_last_of(c, pos);

  s.find_last_not_of(s, pos);
  s.find_last_not_of("she", pos, count);
  s.find_last_not_of("sells", pos);
  s.find_last_not_of(c, pos);
}

void swap(string& s) {
  string st;
  st.swap(s);
}

void compare(string& x1, string& x2) {
  sinh(x1 == x2);
  asinh(x1 != x2);
  cosh(x1 < x2);
  acosh(x1 <= x2);
  tanh(x1 > x2);
  atanh(x1 >= x2);
}

int main(int argc, char* argv[]) {
  int pos = argc;
  int count = argc;
  char c = argc;
  string s({c});

  constructors(pos, count, c);
  allocators(s);
  operators(s, c);
  assign(s, pos, count, c);
  access(s, pos, c);
  iterators(s);
  iterators_math(s, count);
  iterators_comp(s);
  capacity(s, count);
  modifiers(s, pos, count, c);
  // swap(s);
  compare(s, s);

  return 0;
}
