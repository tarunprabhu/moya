#include <fstream>
#include <iostream>

using namespace std;

#pragma moya specialize
int read(ifstream& in) {
  int s;
  in >> s;
  return s;
}

int main(int argc, char* argv[]) {
  ifstream in(argv[1]);
  int ret;
#pragma moya jit
  {
  ret = read(in);
  cout << in.tellg() << endl;
  }
  
  return ret;
}
