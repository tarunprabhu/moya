// This is based on:
// https://computing.llnl.gov/tutorials/openMP/samples/C/omp_mm.c

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NRA 62 /* number of rows in matrix A */
#define NCA 15 /* number of columns in matrix A */
#define NCB 7  /* number of columns in matrix B */

#pragma moya specialize
clock_t matmul(double a[NRA][NCA], double  b[NCA][NCB], double c[NRA][NCB]) {
  clock_t tick = clock();
  int tid, i, j, k, chunk;
  chunk = 10;
#pragma omp parallel shared(a, b, c, chunk) private(tid, i, j, k)
  {
    tid = omp_get_thread_num();
#pragma omp for schedule(static, chunk)
    for(i = 0; i < NRA; i++) {
      printf("Thread=%d did row=%d\n",tid,i);
      for(j = 0; j < NCB; j++)
        for(k = 0; k < NCA; k++)
          c[i][j] += a[i][k] * b[k][j];
    }
  }
  return clock()-tick;
}


int main(int argc, char* argv[]) {
  int tid, nthreads, i, j, k, chunk;
  double a[NRA][NCA],
      b[NCA][NCB],   
      c[NRA][NCB];   

  for(i = 0; i < NRA; i++)
    for(j = 0; j < NCA; j++)
      a[i][j] = i + j;
  for(i = 0; i < NCA; i++)
    for(j = 0; j < NCB; j++)
      b[i][j] = i * j;
  for(i = 0; i < NRA; i++)
    for(j = 0; j < NCB; j++)
      c[i][j] = 0;

#pragma moya jit
  {
  matmul(a, b, c);
  }
  
  /*** Print results ***/
  printf("******************************************************\n");
  printf("Result Matrix:\n");
  for(i = 0; i < NRA; i++) {
    for(j = 0; j < NCB; j++)
      printf("%6.2f   ", c[i][j]);
    printf("\n");
  }
  printf("******************************************************\n");
  printf("Done.\n");
}
