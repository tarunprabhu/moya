#!/usr/bin/env python3

# FIXME: Look for examples in subdirectories

# FIXME: This isn't really a test yet, but just runs all the examples in this
# folder and in subdirectories. At some point, this will have to be changed to
# use, perhaps, the LLVM testing framework to actually have proper tests.

usage = 'run-tests.py [--no-c][--no-cxx][--no-fortran]'

from os import path
import signal
import sys

stop_on_error = False

# [string], string, string => (int, int)
def run_tests(files, compiler, flags=''):
    from os import system
    cmdbase = '{} {} {} 2> /dev/null'
    passed = 0
    failed = 0
    for f in files:
        cmd = cmdbase.format(compiler, f, flags)
        print(cmd)
        if system(cmd) != 0:
            failed += 1
            print('*** FAILED: ' + f)
            if stop_on_error:
                q = input('Continue ([Y]/N): ')
                if q == 'n' or q == 'N':
                    sys.exit(1)
        else:
            passed += 1
    return passed, failed

# [string], [string] => [string]
def filter(files, exts):
    ret = []
    for f in files:
        _, _, ext = f.rpartition('.')
        if ext in exts:
            ret.append(f)
    return ret

# [string], string => (int, int)
def run_c(files, prefix):
    exts = ['c', 'C']
    compiler = path.join(prefix, 'moya-cc')
    return run_tests(filter(files, exts), compiler)

# [string], string => (int, int)
def run_cxx(files, prefix):
    exts = ['cxx', 'cpp', 'cc', 'CC', 'CPP', 'CXX']
    compiler = path.join(prefix, 'moya-cxx')
    return run_tests(filter(files, exts), compiler, '-std=c++11')

# [string], string => (int, int)
def run_fortran(files, prefix):
    exts = ['fpp', 'f90', 'f95', 'f03', 'f08', 'f',
            'FPP', 'F90', 'F95', 'F03', 'F08', 'F']
    compiler = path.join(prefix, 'moya-fort')
    return run_tests(filter(files, exts), compiler)

def main():
    from os import listdir
    from sys import argv

    global stop_on_error
    
    fns = [run_c, run_cxx, run_fortran]
    for arg in argv[1:]:
        if arg == '--no-c':
            fns.remove(run_c)
        elif arg == '--no-cxx':
            fns.remove(run_cxx)
        elif arg == '--no-fortran':
            fns.remove(run_fortran)
        elif arg == '--stop-on-error':
            stop_on_error = True;
        else:
            print('usage: ' + usage)
            exit(1)
    
    prefix = '../install/bin'
    files = listdir()

    passed = 0
    failed = 0
    for fn in fns:
        (p, f) = fn(files, prefix)
        passed += p
        failed += f
        
    print('Passed: {}\nFailed: {}'.format(passed, failed))

    
if __name__ == '__main__':
    main()
