program inner_array_ret
  implicit none
  integer :: n
  integer, allocatable :: a(:)

  n = 3
  !$moya jit
  a = func(4)
  !$moya end jit
contains
  !$moya specialize
  function func(m) result(a)
    implicit none
    integer, allocatable :: a(:)
    integer :: m

    allocate(a(m+n))
  end function func
end program inner_array_ret
