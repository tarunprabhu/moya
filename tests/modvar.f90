module global
  integer(4), allocatable :: array(:)
end module global

program modvar
  use global

  implicit none

  allocate(array(4))
  array(1) = 3

  write(*, '(I4)') array(1)
  deallocate(array)
  
end program modvar
