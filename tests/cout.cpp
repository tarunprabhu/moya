#include <iostream>

using namespace std;

class MyClass {
public:
  int data;

  friend ostream& operator<<(ostream& os, const MyClass&);
};

ostream& operator<<(ostream& os, const MyClass& c) {
  os << c.data;
  return os;
}

int main(int argc, char* argv[]) {
  int i;
  long l;
  float f;
  double d;
  MyClass c;
  cout << c << f << i << l << f << d << c << endl;
  return 0;
}
