#include <mpi.h>

#include <stdio.h>

#pragma moya specialize
void pingpong(int *me, int *other) {
  MPI_Request sreq, rreq;

  MPI_Isend(me, 1, MPI_INT, (*me+1)%2, 0, MPI_COMM_WORLD, &sreq);
  MPI_Irecv(other, 1, MPI_INT, (*me+1)%2, 0, MPI_COMM_WORLD, &rreq);

  MPI_Wait(&sreq, MPI_STATUS_IGNORE);
  MPI_Wait(&rreq, MPI_STATUS_IGNORE);
}

int main(int argc, char *argv[]) {
  int me, other;
  
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &me);

#pragma moya jit
  {
  pingpong(&me, &other);
  }


  printf("%d %d\n", me, other);
  
  MPI_Finalize();

  return 0;
}
