#include <array>
#include <cmath>

using namespace std;

typedef float T;
typedef array<T, 4> Container;

void access(Container& a, int index, T element) {
  ceil(a.at(index));

  a[index] = 5;
  floor(a[index]);

  round(a.front());
  lrint(a.back());
  a.data();
}

void iterators(Container& a) {
  a.begin();
  a.end();
  a.cbegin();
  a.cend();
  a.rbegin();
  a.rend();
  a.crbegin();
  a.crend();
}

void iterators_math(Container& a, int count) {
  Container::iterator it = a.begin();
  Container::iterator jt = a.end();

  cos(*it);
  ++it;
  it++;
  --it;
  it--;
  jt = it + count;
  jt = it - count;
  it += count;
  it -= count;
  tan(it[count]);

  log(*jt);
  for(T t : a)
    sin(t);
}

void iterators_comp(Container& a) {
  Container::iterator it = a.begin();
  Container::iterator jt = a.end();

  sinh(it == jt);
  asinh(it != jt);
  cosh(it < jt);
  acosh(it <= jt);
  tanh(it > jt);
  atanh(it >= jt);
}

void capacity(Container& a) {
  a.empty();
  a.size();
  a.max_size();
}

void operations(Container& a, T element) {
  a.fill(6);
}

void swap(Container& a) {
  Container at;
  at.swap(a);
}

void compare(Container& x1, Container& x2) {
  sinh(x1 == x2);
  asinh(x1 != x2);
  cosh(x1 < x2);
  acosh(x1 <= x2);
  tanh(x1 > x2);
  atanh(x1 >= x2);
}

int main(int argc, char* argv[]) {
  Container a = {1, 2, 3, 4};
  int index = argc;
  int count = argc;
  T element = argc;

  access(a, index, element);
  iterators(a);
  iterators_math(a, count);
  iterators_comp(a);
  capacity(a);
  operations(a, element);
  swap(a);
  compare(a, a);

  return 0;
}
