module string_ret
contains
  !$moya specialize
  function ret(s) result(t)
    character(len=*) :: s
    character(len=:), allocatable :: t
    t = s
  end function ret
end module string_ret

program string_ret_main
  use string_ret
  character(len=10) :: t

  !$moya jit
  t = ret("hello")
  !$moya end jit
end program string_ret_main
