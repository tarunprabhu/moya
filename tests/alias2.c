#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct MyStructT {
  int val;
  struct MyStructT *next;
} MyStruct;

MyStruct* make(int val) {
  MyStruct *ret = (MyStruct*)malloc(sizeof(MyStruct));
  ret->val = val;
  ret->next = ret;
  return ret;
}

#pragma moya specialize
void print(MyStruct *head) {
  for(MyStruct *curr = head; curr; curr = curr->next)
    printf("%d ", curr->val);
  printf("\n");
}

int main(int argc, char *argv[]) {
  MyStruct *p = NULL;
  int opt = atoi(argv[argc-1]);
  switch(opt) {
  case 1: 
    p = make(atoi(argv[argc]));
    break;
  case 2:
    p = make(-atoi(argv[argc]));
    break;
  default:
    break;
  }

#pragma moya jit
  {
  print(p);
  }
  
  return 0;
}
