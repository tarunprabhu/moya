#include <stack>
#include <cmath>

using namespace std;

typedef float T;
typedef std::stack<T> Container;

void constructors() {
  Container s1;
  Container s2(s1);
}

void operators(Container& s) {
  Container s3;
  s3 = s;
}

void access(Container& s) {
  ceil(s.top());
}

void capacity(Container& s) {
  s.empty();
  s.size();
}

void swap(Container& s) {
  Container st;
  st.swap(s);
}

void modifiers(Container& s) {
  s.push(1);
  s.pop();
}

void compare(Container& s1, Container& s2) {
  sinh(s1 == s2);
  asinh(s1 != s2);
  cosh(s1 < s2);
  acosh(s1 <= s2);
  tanh(s1 > s2);
  atanh(s1 >= s2);
}

int main(int argc, char* argv[]) {
  Container s({2, 3, 4});

  constructors();
  operators(s);
  access(s);
  capacity(s);
  swap(s);
  modifiers(s);
  compare(s, s);

  return 0;
}
