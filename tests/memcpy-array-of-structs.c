#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#pragma moya specialize
typedef struct MyStructT {
  int a;
  int b;
} MyStruct;

void print(MyStruct *s) {
  printf("%d %d\n", s->a, s->b);
}

int main(int argc, char *argv[]) {
  MyStruct arr[4];
  int n = atoi(argv[1]);
  MyStruct *dst = (MyStruct*)malloc(sizeof(MyStruct)*n);

  for(int i = 0; i < 4; i++) {
    arr[i].a = 2*i;
    arr[i].b = 2*i + 1;
  }
  
  memcpy(dst, arr, sizeof(MyStruct)*n);

#pragma moya jit
  {
  print(&dst[atoi(argv[2])]);
  }
  
  return 0;
}
