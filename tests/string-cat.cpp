#include <iostream>
#include <string>

using namespace std;

#pragma moya specialize
string string_cat(string& s1, string& s2) {
  return s1 + s2;
}

int main(int argc, char* argv[]) {
  string s1(argv[1]);
  string s2(argv[2]);
  string s3;
#pragma moya jit
  {
  s3 = string_cat(s1, s2);
  }

  return s3.size();
}
