module tmp
  type MyType
     integer(4) :: val
     type(MyType), pointer :: next
  end type MyType

contains
  !$moya specialize ignore(t) max(10+20)
  subroutine print(t)
    implicit none

    type(MyType) :: t

    write(*, '(I8)') t%val
  end subroutine print
end module tmp

program derived

  use tmp
  implicit none

  type(MyType) :: s
  type(MyType), allocatable :: arr(:)

  allocate(arr(2))
  s%val = 5
!  s%next = s

  !$moya jit
  call print(s)
  !$moya end jit

  s%val = 73

end program derived
