module array_ret
contains
  !$moya specialize
  function ret(n) result(b)
    implicit none
    integer :: n
    integer, allocatable :: b(:)

    allocate(b(n))
  end function ret

  subroutine call_ret(n)
    implicit none
    integer :: n
    integer, allocatable :: b(:)
    b = ret(n)
  end subroutine call_ret
end module array_ret

program array_ret_main
  use array_ret

  !$moya jit
  call call_ret(4)
  !$moya end jit
end program array_ret_main
