#include <iostream>
#include <string>

using namespace std;

#pragma moya specialize
void print(string s) {
  cout << s << endl;
}

int main(int argc, char* argv[]) {
  // int l = stoi(argv[argc - 2]);
  // char c = argv[argc - 1][0];
  // int rand = stoi(argv[argc]);

  string s0;
  // string s1;
  // string s2(l, c);
  // string s3(s1, 1, rand);
  // string s4(s3.c_str(), rand);
  // string s5(s3.c_str());
  string s6(s0.begin(), s0.end());
  // string s7 = {c, (char)rand, c};

#pragma moya jit
  {
  print(s0);
  }

  return 0;
}
