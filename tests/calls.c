#include <stdio.h>
#include <stdlib.h>

int f1(int a) {
  return a + random();
}

int f2(int a) {
  return random() + f1(a);
}

#pragma moya specialize
int f3(int a) {
  return random() + f2(a);
}

int f4(int a) {
  return random() + f3(a);
}

int f5(int a) {
  return random() + f4(a);
}

int main(int argc, char *argv[]) {
  int a = atoi(argv[argc-1]);
  int t;
#pragma moya jit
  {
  t = f5(a);
  }
  return t;
}
