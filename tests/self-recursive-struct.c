#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct NodeT {
  int val;
  struct NodeT *next;
} Node;

#pragma moya specialize
void serialize(Node *n) {
  Node *curr = n;
  while(curr != NULL)
    printf("%d->", n->val);
  printf("NULL");
}

int main(int argc, char *argv[]) {
  Node n;
  n.val = atoi(argv[argc]);
  n.next = &n;

#pragma moya jit
  {
  serialize(&n);
  }
  
  return n.val;
}
