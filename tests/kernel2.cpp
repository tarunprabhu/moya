#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;

class MyClass2 {
private:
  int* a;
  int* b;
  int* c;
  int m, n, p;

private:
  void initialize() {
    memset(a, 0, m * sizeof(int));
    for(int i = 0; i < n; i++)
      b[i] = random();
    for(int i = 0; i < p; i++)
      c[i] = random();
  }

  void print(int* a, int m) {
    for(int i = 0; i < m; i++)
      cout << a[i] << " ";
    cout << endl;
  }

  void kernel2();

public:
  MyClass2(int m, int n, int p)
      : a(new int[m]), b(new int[n]), c(new int[p]), m(m), n(n), p(p) {
    initialize();
  }

  ~MyClass2() {
    delete[] c;
    delete[] b;
    delete[] a;
  }

  void run() {
#pragma moya jit
    {
    kernel2();
    }

    print(a, m);
  }
};

#pragma moya specialize
void MyClass2::kernel2() {
  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      for(int k = 0; k < p; k++)
        a[i] += (b[j] * c[k]);
}

int main(int argc, char* argv[]) {
  int m = argc > 1 ? atoi(argv[1]) : 1000;
  int n = argc > 1 ? atoi(argv[2]) : 1000;
  int p = argc > 1 ? atoi(argv[3]) : 5;

  MyClass2 c(m, n, p);
  c.run();

  return 0;
}
