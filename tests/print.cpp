#include <iostream>
#include <cstdint>

using namespace std;

#pragma moya specialize
void print(int i, long l, float f) {
  cout << i << " " << l << " " << f << "\n";
}

int main(int argc, char* argv[]) {
  int i = random();
  long l = random();
  float f = rand();

#pragma moya jit
  {
  print(i, l, f);
  }

  return 0;
}
