#include <queue>
#include <cmath>

using namespace std;

typedef float T;
typedef queue<T> Container;

void constructors() {
  Container q1;
  Container q2(q1);
}

void operators(Container& q) {
  Container q3;
  q3 = q;
}

void access(Container& q) {
  ceil(q.front());
  floor(q.back());
}

void capacity(Container& q) {
  q.empty();
  q.size();
}

void modifiers(Container& q) {
  q.push(1);
  q.pop();
}

void swap(Container& q) {
  Container qt;
  qt.swap(q);
}

void compare(Container& q1, Container& q2) {
  sinh(q1 == q2);
  asinh(q1 != q2);
  cosh(q1 < q2);
  acosh(q1 <= q2);
  tanh(q1 > q2);
  atanh(q1 >= q2);
}

int main(int argc, char* argv[]) {
  Container q({2, 3, 4});

  constructors();
  operators(q);
  access(q);
  capacity(q);
  modifiers(q);
  swap(q);
  compare(q, q);

  return 0;
}
