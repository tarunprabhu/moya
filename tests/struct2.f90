module global
  type inner
     real(4), allocatable :: data(:)
  end type inner

  type outer
     type(inner), allocatable :: wrapper(:)
  end type outer

  type(outer) :: object
end module global

program struct2
  use global

  implicit none

  allocate(object%wrapper(1))
  allocate(object%wrapper(1)%data(4))

  object%wrapper(1)%data(2) = 37.26

  write(*, '(F4.3)') object%wrapper(1)%data(2)

  deallocate(object%wrapper(1)%data)
  deallocate(object%wrapper)
end program struct2
