#include <stdio.h>
#include <stdlib.h>

typedef struct ArrayT {
  int *buf;
  int size;
} Array;

Array* allocate(int n) {
  Array *a = (Array*)malloc(sizeof(Array));
  a->buf = (int*)malloc(sizeof(int)*n);
  a->size = n;
  return a;
}

void deallocate(Array *a) {
  free(a->buf);
  free(a);
}

#pragma moya specialize
void vec_add(Array *a, Array *b, Array *c) {
  for(int i = 0; i < c->size; i++)
    c->buf[i] = a->buf[i] + b->buf[i];
}

void print(Array *a) {
  for(int i = 0; i < a->size; i++)
    printf("%d ", a->buf[i]);
  printf("\n");
}

int main(int argc, char *argv[]) {
  int n = atoi(argv[1]);
  Array *a = allocate(n);
  Array *b = allocate(n);
  Array *c = allocate(n);

#pragma moya jit
  {
  vec_add(a, b, c);
  }
  
  deallocate(c);
  deallocate(b);
  deallocate(a);
  
  return 0;
}
