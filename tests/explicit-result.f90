module explicit_result
contains
  !$moya specialize
  function sub1(n) result(r)
    implicit none

    integer(4) :: n, r

    r = n - 1
  end function sub1
end module explicit_result

program explicit_result_main
  use explicit_result

  !$moya jit
  write(*, '(I4)') sub1(24)
  !$moya end jit
  
end program explicit_result_main
