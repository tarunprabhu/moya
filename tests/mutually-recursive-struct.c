#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct AT;
struct BT;

typedef struct AT {
  int val;
  struct BT *next_b;
} A;

typedef struct BT {
  int val;
  struct AT *next_a;
} B;

A* make_a(int val) {
  A* ret = (A*)malloc(sizeof(A));
  ret->val = val;
  ret->next_b = NULL;
  return ret;
}

B* make_b(int val) {
  B* ret = (B*)malloc(sizeof(B));
  ret->val = val;
  ret->next_a = NULL;
  return ret;
}

#pragma moya specialize
void pingpong(A *sa) {
  for(A *t = sa; sa; ) {
    B *sb = t->next_b;
    printf("(%d, %d) ", t->val, sb->val);
    t = sb->next_a;
  }
  printf("\n");
}

int main(int argc, char *argv[]) {
  A* a = make_a(atoi(argv[argc-1]));
  B* b = make_b(atoi(argv[argc]));

  a->next_b = b;
  b->next_a = a;

#pragma moya jit
  {
  pingpong(a);
  }

  free(b);
  free(a);
  
  return 0;
}
