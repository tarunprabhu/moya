module mymodule
    type MyType
     integer(4) :: val
     type(MyType), pointer :: next
  end type MyType

contains
  
  !$moya specialize
  subroutine print(s)
    implicit none

    type(MyType) :: s

    write(*, '(I8)') s%val
  end subroutine print  
end module mymodule

  
program derived

  use mymodule
  implicit none
  
  type(MyType), allocatable :: s(:)
  type(MyType) t

  allocate(s(3))
  
  s(1)%val = 5
  s(1)%next = s(2)

  !$moya jit
  t = s(2)
  call print(t)
  t = s(3)
  call print(t)
  !$moya end jit

  deallocate(s)
  
end program derived
