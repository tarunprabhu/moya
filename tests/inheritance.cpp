#include <iostream>
#include <cstdlib>
#include <sstream>

using namespace std;

int woof = 37;
int meow = 73;

class Animal {
protected:
  int _sound;

protected:
  Animal(int sound) : _sound(sound) { ; }

public:
  virtual ~Animal() { ; }
  int sound() { return _sound; }
};

class Dog : public Animal {
public:
  Dog() : Animal(woof) { ; }
  virtual ~Dog() { ; }
};

class Cat : public Animal {
public:
  Cat() : Animal(meow) { ; }
  virtual ~Cat() { ; }
};

#pragma moya specialize
int sing(Animal* animal) {
  int song = 1;
  for(int i = 0; i < random(); i++)
    song += animal->sound();
  return song;
}

int main(int argc, char* argv[]) {
  int opt = atoi(argv[1]);
  Animal* animal = nullptr;
  if(opt)
    animal = new Dog();
  else
    animal = new Cat();

#pragma moya jit
  {
  int song = sing(animal);
  cout << song << endl;
  }

  delete animal;

  return 0;
}
