#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>

using namespace std;

void print(double *a, int m, int n) {
  for(int i = 0; i < m; i++) {
    for(int j = 0; j < n; j++)
      cout << setw(4) << a[i*n + j] << " ";
    cout << endl;
  }
  cout << endl;
}

void init(double *a, int m, int n) {
  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      a[i*m + j] = i*m + j + 1;
}

void init0(double *a, int m, int n) {
  memset(a, 0, sizeof(double)*m*n);
}

#pragma moya specialize
clock_t matmul(double* a, double* b, double* c, int m, int n, int p) {
  clock_t tick = clock();
  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      for(int k = 0; k < p; k++)
        a[i * n + j] += b[i * m + k] * c[k * p + j];
  return clock() - tick;
}

int main(int argc, char* argv[]) {
  int size = 4;
  int m = argc > 1 ? atoi(argv[1]) : size;
  int n = argc > 2 ? atoi(argv[2]) : size;
  int p = argc > 3 ? atoi(argv[3]) : size;
  double* a = new double[m * p];
  double* b = new double[m * n];
  double* c = new double[n * p];

  init0(a, m, p);
  init(b, m, n);
  init(c, n, p);

  clock_t time = 0;
#pragma moya jit
  {
  for(int i = 0; i < 1000; i++)
    time += matmul(a, b, c, m, n, p);
  }
  cout << "time: " << time << endl;
  
  return 0;
}
