#include <iostream>

using namespace std;

class Animal {
protected:
  int color;
  int age;

public:
  Animal(int color, int age) : color(color), age(age) { ; }
  ~Animal() { ; }

  virtual int sound() const = 0;
};

class Dog : public Animal {
protected:
public:
  Dog(int color, int age) : Animal(color, age) { ; }
  ~Dog() { ; }

  #pragma moya specialize
  virtual int sound() const {
    return 37;
  }
};

class Cat : public Animal {
protected:
public:
  Cat(int color, int age) : Animal(color, age) { ; }
  ~Cat() { ; }

  #pragma moya specialize
  virtual int sound() const {
    return 73;
  }
};

int main(int argc, char* argv[]) {
  Animal* animal = nullptr;
  int color = stoi(argv[2]), age = stoi(argv[3]);

  if(stoi(argv[1]))
    animal = new Dog(color, age);
  else
    animal = new Cat(color, age);

#pragma moya jit
  {
  cout << "Animal said: " << animal->sound() << "\n";
  }

  return 0;
}
