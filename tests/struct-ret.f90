module tmp
  type struct
     integer(8) :: data
     real(8) :: real_data
     character :: c
  end type struct

contains

  type(struct) function ret(i, d, c)
    implicit none
    type(struct) :: s
    integer(8) :: i
    real(8) :: d
    character :: c

    ret = s
  end function ret
end module tmp

program struct_ret_main
  use tmp

  implicit none
  type(struct) :: s
  integer(8) :: i = 2
  real(8) :: f = 2.3
  s = ret(i, f, 'c')
end program struct_ret_main

