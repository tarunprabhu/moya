#include <forward_list>
#include <cmath>

using namespace std;

typedef long T;
typedef forward_list<T> Container;

void constructors(int count) {
  Container l1;
  Container l2(count);
  Container l3(count, 1);
  Container l4(l3.begin(), l3.end());
  Container l5(l3);
  Container l6({2, 3, 4});
}

void operators(Container& l) {
  Container l7;
  Container l8;

  l8 = {5, 6, 7, 8};
  l7 = l8;
}

void assign(Container& l, int count) {
  Container lt({50, 51});
  
  l.assign(count, 9);
  l.assign(lt.begin(), lt.end());
  l.assign({10, 11, 12});
}

void allocators(Container& l) {
  l.get_allocator();
}

void access(Container& l) {
  ceil(l.front());
}

void iterators(Container& l) {
  l.before_begin();
  l.cbefore_begin();
  l.begin();
  l.end();
  l.cbegin();
  l.cend();
}

void iterators_math(Container& l) {
  Container::iterator it = l.begin();
  Container::iterator jt = l.end();

  cos(*it);
  ++it;
  it++;

  tan(*jt);
  for(T t : l)
    sin(t);
}

void iterators_comp(Container& l) {
  Container::iterator it = l.begin();
  Container::iterator jt = l.end();

  sinh(it == jt);
  asinh(it != jt);
}

void capacity(Container& l) {
  l.empty();
  l.max_size();
}

void modifiers(Container& l, int count) {
  Container lt({52, 53});

  l.clear();

  l.insert_after(l.begin(), 17);
  l.insert_after(l.begin(), count, 18);
  l.insert_after(l.begin(), lt.begin(), lt.end());
  l.insert_after(l.begin(), {13, 14, 15, 16});

  l.erase_after(l.begin());
  l.erase_after(l.begin(), l.end());

  l.push_front(19);
  l.pop_front();

  l.resize(count);
  l.resize(count, 20);
}

void swap(Container& l) {
  Container l9;
  l9.swap(l);
}

void operations(Container& l) {
  Container l10;

  l10.merge(l);
  l10.merge(l, greater<T>());

  l10.splice_after(l10.begin(), l);
  l10.splice_after(l10.begin(), l, l.begin());
  l10.splice_after(l10.begin(), l, l.begin(), l.end());

  round(l10.front());
  
  l.remove(13);
  l.remove_if(logical_not<T>());

  l.reverse();

  l.unique();
  l.unique(greater<T>());

  l.sort();
  l.sort(greater<T>());
}

void compare(Container& l1, Container& l2) {
  sinh(l1 == l2);
  asinh(l1 != l2);
  cosh(l1 < l2);
  acosh(l1 <= l2);
  tanh(l1 > l2);
  atanh(l1 >= l2);
}

int main(int argc, char* argv[]) {
  Container l;
  int count = argc;

  constructors(count);
  operators(l);
  assign(l, count);
  access(l);
  allocators(l);
  iterators(l);
  iterators_math(l);
  iterators_comp(l);
  modifiers(l, count);
  swap(l);
  operations(l);
  compare(l, l);

  return 0;
}
