#include <stdio.h>
#include <stdlib.h>

#pragma moya specialize
int fact(int n) {
  if(n <= 2)
    return 1;
  return n * fact(n-1);
}

int main(int argc, char *argv[]) {
  int f;
#pragma moya jit
  {
    f = fact(atoi(argv[argc-1]));
  }
  
  return f;
}
