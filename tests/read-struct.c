#include <stdio.h>
#include <stdlib.h>

typedef struct MyStructT {
  long i;
  char c;
} MyStruct;

#pragma moya specialize
void print(MyStruct *t) {
  printf("*{%ld, %c}\n", t->i, t->c);
}

MyStruct get(MyStruct *s, int idx) {
  return s[idx];
}

int main(int argc, char *argv[]) {
  MyStruct s[] = { { 42, 'a' }, { 73, 'z' } };
  int idx = atoi(argv[1]);

  MyStruct t = get(s, idx);

#pragma moya jit
  {
  print(&t);
  }
  
  return 0;
}
