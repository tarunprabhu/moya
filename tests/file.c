#include <stdio.h>

#pragma moya specialize ignore(f)
void write(FILE *f, const char *s) {
  fprintf(f, "%s\n", s);
}

int main(int argc, char* argv[]) {
  FILE *f = fopen(argv[1], "w");

  #pragma moya jit
  {
  write(f, argv[2]);
  }

  fclose(f);
  return 0;
}
