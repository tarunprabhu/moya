#include <stdio.h>
#include <stdlib.h>

typedef struct ArrayT {
  int *nums;
  int n;
} Array;

#pragma moya specialize
void init(Array **array, int n) {
  if(*array)
    (*array)->nums = (int*)calloc(n, sizeof(int));
}

int main(int argc, char *argv[]) {
  int n = atoi(argv[argc]);

#pragma moya jit
  {
  init(NULL, n);
  }

  return n;
}
