#include <list>
#include <cmath>
#include <functional>

using namespace std;

typedef float T;
typedef list<T> Container;

void constructors(int count) {
  Container l1;
  Container l2(count);
  Container l3(count, 1);
  Container l4(l3.begin(), l3.end());
  Container l5(l3);
  Container l6({1, 2, 3, 4});
}

void operators(Container& l) {
  Container l7;
  Container l8;

  l7 = {5, 6, 7, 8};
  l8 = l;
}

void assign(Container& l, int count) {
  Container lt({50, 51});
  
  l.assign(count, 9);
  l.assign(lt.begin(), lt.end());
  l.assign({10, 11, 12});
}

void allocators(Container& l) {
  l.get_allocator();
}

void access(Container& l) {
  ceil(l.front());
  floor(l.back());
}

void iterators(Container& l) {
  l.begin();
  l.end();
  l.cbegin();
  l.cend();
  l.rbegin();
  l.rend();
  l.crbegin();
  l.crend();
}

void iterators_math(Container& l) {
  Container::iterator it = l.begin();
  Container::iterator jt = l.end();

  cos(*it);
  ++it;
  it++;
  --it;
  it--;

  tan(*jt);
  for(T t : l)
    sin(t);
}

void iterators_comp(Container& l) {
  Container::iterator it = l.begin();
  Container::iterator jt = l.end();

  sinh(it == jt);
  asinh(it != jt);
}

void capacity(Container& l) {
  l.empty();
  l.size();
  l.max_size();
}

void modifiers(Container& l, int count) {
  Container lt({52, 53});
  
  l.clear();

  l.insert(l.begin(), 17);
  l.insert(l.begin(), count, 18);
  l.insert(l.begin(), lt.begin(), lt.end());
  l.insert(l.begin(), {13, 14, 15, 16});

  l.erase(l.begin());
  l.erase(l.begin(), l.end());

  l.push_back(19);
  l.push_front(20);
  l.pop_back();
  l.pop_front();

  l.resize(count);
  l.resize(count, 21);
}

void swap(Container& l) {
  Container l9;
  l9.swap(l);
}

void operations(Container& l, int count) {
  Container l10;

  l10.merge(l);
  l10.merge(l, greater<T>());

  l10.splice(l10.begin(), l);
  l10.splice(l10.begin(), l, l.begin());
  l10.splice(l10.begin(), l, l.begin(), l.end());

  round(l10.front());
  
  l.remove(13);
  l.remove_if(logical_not<T>());

  l.reverse();

  l.unique();
  l.unique(greater<T>());

  l.sort();
  l.sort(greater<T>());
}

int main(int argc, char* argv[]) {
  Container l;
  int count = argc;

  constructors(count);
  operators(l);
  access(l);
  allocators(l);
  assign(l, count);
  capacity(l);
  iterators(l);
  iterators_math(l);
  iterators_comp(l);
  modifiers(l, count);
  swap(l);
  operations(l, count);

  return 0;
}
