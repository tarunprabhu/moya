#include <iostream>
#include <sstream>

using namespace std;

class MyClass {
private:
  double g;
  float f;
  long l;
  int i;
  char c;

public:
  MyClass(double g, float f, long l, int i, char c)
      : g(g), f(f), l(l), i(i), c(c) {
    ;
  }
  ~MyClass() { ; }

  string str_double() const {
    stringstream ss;
    ss << "double: " << g;
    return ss.str();
  }

  string str_float() const {
    stringstream ss;
    ss << "float: " << f;
    return ss.str();
  }

  string str_long() const {
    stringstream ss;
    ss << "long: " << l;
    return ss.str();
  }

  string str_int() const {
    stringstream ss;
    ss << "int: " << i;
    return ss.str();
  }

  string str_char() const {
    stringstream ss;
    ss << "char: " << c;
    return ss.str();
  }

  #pragma moya specialize
  string str() const {
    stringstream ss;
    ss << str_double() << endl
       << str_float() << endl
       << str_long() << endl
       << str_int() << endl
       << str_char() << endl;
    return ss.str();
  }
};

int main(int argc, char* argv[]) {
  double g = stod(argv[1]);
  float f = stof(argv[2]);
  long l = stol(argv[3]);
  int i = stoi(argv[4]);
  char c = argv[5][1];

  MyClass o(g, f, l, i, c);

#pragma moya jit
  {
  cout << o.str() << endl;
  }

  return 0;
}
