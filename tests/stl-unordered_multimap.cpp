#include <unordered_map>
#include <cmath>

using namespace std;

typedef float K;
typedef double V;
typedef unordered_multimap<K, V> Container;

void constructors() {
  Container m1;
  Container m2({{1, 2}, {3, 4}});
  Container m3(m2);
  Container m4(m3.begin(), m3.end());
}

void operators(Container& m) {
  Container m5;
  Container m6;

  m6 = {{5, 6}, {7, 8}};
  m5 = m6;
}

void allocators(Container& m) {
  m.get_allocator();
}

void iterators(Container& m) {
  m.begin();
  m.end();
  m.cbegin();
  m.cend();
}

void iterators_math(Container& m) {
  Container::iterator it = m.begin();
  Container::iterator jt = it;

  cos(it->first);
  sin((*it).second);
  it++;
  ++it;

  tan(jt->first);
  atan((*jt).second);
  for(auto it : m) {
    acos(it.first);
    asin(it.second);
  }
}

void iterators_comp(Container& m) {
  Container::iterator it = m.begin();
  Container::iterator jt = m.end();

  tan(it == jt);
  atan(it != jt);
}

void capacity(Container& m) {
  m.empty();
  m.size();
  m.max_size();
}

void modifiers(Container& m) {
  Container mt({{50, 51}});

  m.clear();

  m.insert(pair<K, V>(13, 14));
  m.insert(m.begin(), pair<K, V>(15, 16));
  m.insert(mt.begin(), mt.end());
  m.insert({{9, 10}, {11, 12}});

  m.erase(m.begin());
  m.erase(m.begin(), m.end());
  m.erase(13);
}

void swap(Container& m) {
  Container mt;
  mt.swap(m);
}

void lookups(Container& m, K key) {
  m.count(key);
  m.find(key);
  m.equal_range(key);
}

void buckets(Container& m, K key, int bucket, int count) {
  m.begin(bucket);
  m.end(bucket);
  m.cbegin(bucket);
  m.cend(bucket);

  m.bucket_count();
  m.max_bucket_count();

  m.bucket_size(count);
  m.reserve(key);
}

void buckets_iter_ops(Container& m, int bucket) {
  auto it = m.begin(bucket);
  auto jt = it;

  cosh(it->first);
  sinh((*it).second);
  ++it;
  it++;

  tanh(jt->first);
  atanh((*jt).second);
  for(auto it : m) {
    acosh(it.first);
    asinh(it.second);
  }

  exp(it == jt);
  exp2(it != jt);
}

void hashes(Container& m, int count, float max_load_factor) {
  m.load_factor();
  m.max_load_factor();
  m.max_load_factor(max_load_factor);
  m.rehash(count);
  m.reserve(count);
}

void observers(Container& m) {
  // m.hash_function();
  // m.key_eq();
}

int main(int argc, char* argv[]) {
  Container m;
  int count = argc;
  K key = argc;
  V val = argc;
  float max_load_factor = argc;
  int bucket = argc;

  constructors();
  operators(m);
  capacity(m);
  iterators(m);
  iterators_math(m);
  iterators_comp(m);
  modifiers(m);
  lookups(m, key);
  buckets(m, key, bucket, count);
  buckets_iter_ops(m, bucket);
  hashes(m, count, max_load_factor);
  observers(m);

  return 0;
}
