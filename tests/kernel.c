#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void initialize(int *a, int *b, int *c, int m, int n, int p) {
  memset(a, 0, m*sizeof(int));
  for(int i = 0; i < n; i++)
    b[i] = random() % 100;
  for(int i = 0; i < p; i++)
    c[i] = random() % 100;
}

void print(int *a, int m) {
  for(int i = 0; i < m; i++)
    printf("%d ", a[i]);
  printf("\n");
}

#pragma moya specialize
void kernel(int *a, int *b, int *c, int m, int n, int p) {
  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      for(int k = 0; k < p; k++)
        a[i] += (b[j]*c[k]);
}

int main(int argc, char *argv[]) {
  int m = argc > 1 ? atoi(argv[1]) : 1000;
  int n = argc > 2 ? atoi(argv[2]) : 1000;
  int p = argc > 3 ? atoi(argv[3]) : 5;
    
  int *a = (int*)malloc(sizeof(int)*m);
  int *b = (int*)malloc(sizeof(int)*n);
  int *c = (int*)malloc(sizeof(int)*p);

  initialize(a, b, c, m, n, p);

#pragma moya jit
  {
  kernel(a, b, c, m, n, p);
  }

  print(a, m);
  
  free(c);
  free(b);
  free(a);

  return 0;
}
