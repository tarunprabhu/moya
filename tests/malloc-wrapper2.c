#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct ArrayT {
  double* data;
  unsigned long size;
} Array;

unsigned long min(unsigned long a, unsigned long b) {
  return a < b ? a : b;
}

Array* allocate(unsigned long size) {
  Array* ret = (Array*)malloc(sizeof(Array));
  ret->data = (double*)malloc(sizeof(double)*size);
  ret->size = size;
  return ret;
}

void deallocate(Array* a) {
  free(a->data);
  free(a);
}

// @moya jit()
void add(const Array* a, const Array* b, Array* c) {
  for(unsigned long i = 0; i < c->size; i++)
    c->data[i] = a->data[i] + b->data[i];
}

// @moya jit()
void print(const Array* a) {
  for(unsigned long i = 0; i < a->size; i++)
    printf("%g ", a->data[i]);
  printf("\n");
}

int main(int argc, char* argv[]) {
  Array* a = allocate(atoi(argv[1]));
  Array* b = allocate(atoi(argv[1]));
  Array* c = allocate(atoi(argv[1]));

  // @moya enable()
  add(a, b, c);
  print(c);
  // @moya disable()

  deallocate(c);
  deallocate(b);
  deallocate(a);
  
  return 0;
}
