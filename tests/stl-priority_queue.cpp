#include <queue>

using namespace std;

typedef float T;
typedef priority_queue<T> Container;

void constructors() {
  Container q1;
  Container q2(greater<T>);
  Container q3(q1);
  // Container q4(q.begin(), q.end());
}

void operators(Container& q) {
  Container q4;
  q4 = q;
}

void access(Container& q) {
  ceil(q.top());
}

void capacity(Container& q) {
  q.empty();
  q.size();
}

void modifiers(Container& q, T element) {
  q.push(1);
  q.pop();
}

void swap(Container& q) {
  Container qt;
  qt.swap(q);
}

int main(int argc, char* argv[]) {
  Container q;

  constructors();
  operators(q);
  access(q);
  capacity(q);
  modifiers(q);
  swap(q);

  return 0;
}
