module declare
contains
  !$moya declare memsafe
  subroutine dummy_fn(s)
    implicit none
    integer(4) :: s
  end subroutine dummy_fn
end module declare

program moya_declare_main
  use declare

  !$moya jit
  call dummy_fn(23)
  !$moya end jit
end program moya_declare_main
