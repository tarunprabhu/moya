class C {
public:
  int n;
  int m;
  char c;
  long d;

#pragma moya specialize ignore(n)
  C fun(C c, C d, int n) {
    if(n == 0)
      return c;
    else
      return d;
  }

  int fun2() {
    return n;
  }
};


int main() {
  C c, d;
  int e;
  int r;
#pragma moya jit
  {
  C f = c.fun(c, d, e);
  r = f.fun2();
  }
  return r;
}
