#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void initialize(int **bufs, int m, int n) {
  for(int i = 0; i < m; i++) {
    int *ptr = bufs[i];
    for(int j = 0; j < n; j++)
      ptr[j] = random();
  }
}

void copy(int **dsts, int **srcs, int m) {
  memcpy(dsts, srcs, sizeof(int*)*m);
}

#pragma moya specialize
void print(int *arr, int n) {
  for(int i = 0; i < n; i++)
    printf("%d ", arr[i]);
  printf("\n");
}

int main(int argc, char *argv[]) {
  int** dsts;
  int** srcs;
  int m = atoi(argv[1]);
  int n = atoi(argv[2]);
  int k = atoi(argv[3]);

  dsts = (int**)malloc(sizeof(int*)*m);
  srcs = (int**)malloc(sizeof(int*)*m);
  for(int i = 0; i < 2; i++)
    srcs[i] = (int*)malloc(sizeof(int)*n);

  initialize(srcs, m, n);
  copy(dsts, srcs, m);

#pragma moya jit
  {
  print(dsts[k], n);
  }

  for(int i = 0; i < 2; i++)
    free(srcs[i]);
  free(srcs);
  free(dsts);
       
  return 0;
}
