#include <omp.h>
#include <cstdio>

using namespace std;

#pragma moya specialize
int fn(int id) {
  return id + 1000;
}

int main(int argc, char *argv[]) {
  int ids[8];
#pragma omp parallel
  {
#pragma moya jit
    {
    ids[omp_get_thread_num()] = fn(omp_get_thread_num());
    }
  }


  for(int i = 0; i < 8; i++)
    printf("%d: %d\n", i, ids[i]);

  return 0;
}
