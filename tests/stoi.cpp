#include <iostream>
#include <string>

using namespace std;

#pragma moya specialize
void print(int n) {
  cout << n << endl;
}

int main(int argc, char* argv[]) {
  int n = stoi(argv[argc - 1]);

#pragma moya jit
  {
  print(n);
  }

  return n;
}
