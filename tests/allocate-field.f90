program alloca
  type Derived
     real, allocatable :: field(:)
  end type Derived
  type(Derived), target :: d
  type(Derived), pointer :: s
  s => d
  allocate(s%field(3))
end program alloca
