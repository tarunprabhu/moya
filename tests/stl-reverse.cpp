#include <cmath>
#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <forward_list>
#include <set>
#include <unordered_set>

using namespace std;

long double r;

void print(long double r) {
  cout << r << endl;
}

int main(int argc, char* argv[]) {
  vector<int> v = {1};
  deque<int> d = {2};
  list<int> l = {3};
  forward_list<int> f = {4};
  set<int> s = {5};
  unordered_set<int> u = {6};

  r += sin(*v.rbegin());
  r += cos(*d.rbegin());
  r += tan(*l.rbegin());
  r += sinh(*f.begin());
  r += cosh(*s.rbegin());
  r += tanh(*u.begin());

  r += asin(*v.crbegin());
  r += acos(*d.crbegin());
  r += atan(*l.crbegin());
  r += asinh(*f.cbegin());
  r += acosh(*s.crbegin());
  r += atanh(*u.cbegin());

  print(r);
  
  return 0;
}
