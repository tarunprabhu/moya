#include <mpi.h>

#include <stdio.h>

/// @moya jit()
void pingpong(int *me, int *other) {
  MPI_Request sreq, rreq;

  MPI_Send(MPI_BOTTOM, 1, MPI_INT, (*me+1)%2, 0, MPI_COMM_WORLD);
  MPI_Recv(MPI_BOTTOM, 1, MPI_INT, (*me+1)%2, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
}

int main(int argc, char *argv[]) {
  int me, other;
  
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &me);

  /// @moya enable()
  pingpong(&me, &other);
  /// @moya disable()


  printf("%d %d\n", me, other);
  
  MPI_Finalize();

  return 0;
}
