#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void initialize(int **bufs, int m, int n) {
  for(int i = 0; i < m; i++) {
    int *ptr = bufs[i];
    for(int j = 0; j < n; j++)
      ptr[j] = random();
  }
}

#pragma moya specialize
void print(int *arr, int n) {
  for(int i = 0; i < n; i++)
    printf("%d ", arr[i]);
  printf("\n");
}

int main(int argc, char *argv[]) {
  int** arrs;
  int m = atoi(argv[1]);
  int n = atoi(argv[2]);
  int k = atoi(argv[3]);

  arrs = (int**)malloc(sizeof(int*)*m);
  memset(arrs, 0, m*sizeof(*arrs));
  for(int i = 0; i < 2; i++)
    arrs[i] = (int*)malloc(sizeof(int)*n);

  initialize(arrs, m, n);

#pragma moya jit
  {
  print(arrs[k], n);
  }

  for(int i = 0; i < 2; i++)
    free(arrs[i]);
  free(arrs);
       
  return 0;
}
