module implicit_result
contains
  !$moya specialize ignore(n)
  integer(4) function add1(n)
    implicit none

    integer(4) :: n

    add1 = n + 1
  end function add1
end module implicit_result

program implicit_result_main
  use implicit_result

  !$moya jit
  write(*, '(I4)') add1(23)
  !$moya end jit
end program implicit_result_main
