module kernel

contains

  !$moya specialize
  subroutine kernel_heart(a, b, c, m, n, p)
    implicit none
    integer(4), allocatable :: a(:), b(:), c(:)
    integer(4) :: m, n, p
    integer(4) :: i, j, k

    do i = 1, m
       do j = 1, n
          do k = 1, p
             a(i) = a(i) + b(j)*c(k)
          end do
       end do
    end do
  end subroutine kernel_heart

end module kernel

program kernel2
  use kernel
  
  implicit none
  integer(4), allocatable :: a(:), b(:), c(:)
  integer(4) :: m, n, p
  character(len=32) :: arg

  m = 1001
  if(command_argument_count() > 0) then
     call get_command_argument(1, arg)
     read(arg, *) m
  end if

  n = 1002
  if(command_argument_count() > 1) then
     call get_command_argument(2, arg)
     read(arg, *) n
  end if

  p = 1003
  if(command_argument_count() > 2) then
     call get_command_argument(3, arg)
     read(arg, *) p
  end if

  allocate(a(m), b(n), c(p))
  call initialize(a, b, c)

  !$moya jit
  call kernel_heart(a, b, c, m, n, p)
  !$moya end jit

  call print(a, 5)
  deallocate(a, b, c)
  
contains
  subroutine initialize(a, b, c)
    implicit none

    integer(4), allocatable :: a(:), b(:), c(:)
    integer(4) :: i
    real :: t

    do i = 1, m
       a(i) = 0
    end do

    do i = 1, n
       call random_number(t)
       b(i) = 1!floor(t*38423)
    end do

    do i = 1, p
       call random_number(t)
       c(i) = 2!floor(t*38423)
    end do
  end subroutine initialize

  subroutine print(a, m)
    implicit none

    integer(4), allocatable :: a(:)
    integer(4) :: m
    integer(4) :: i

    do i = 1, m
       write(*, '(I10)') a(i)
    end do
  end subroutine print
  
end program kernel2
