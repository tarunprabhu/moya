#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct AllT {
  int *a;
  int *b;
  int *c;
  int m;
  int n;
  int p;
} All;

void initialize(All *all) {
  memset(all->a, 0, all->m*sizeof(int));
  for(int i = 0; i < all->n; i++)
    all->b[i] = rand();
  for(int i = 0; i < all->p; i++)
    all->c[i] = rand();
}

void print(int *a, int m) {
  for(int i = 0; i < m; i++)
    printf("%d ", a[i]);
  printf("\n");
}

#pragma moya specialize
void kernel(All *all) {
  for(int i = 0; i < all->m; i++)
    for(int j = 0; j < all->n; j++)
      for(int k = 0; k < all->p; k++)
        all->a[i] += (all->b[j]*all->c[k]);
}

int main(int argc, char *argv[]) {
  All all;
  
  all.m = argc > 1 ? atoi(argv[1]) : 1000;
  all.n = argc > 1 ? atoi(argv[2]) : 1000;
  all.p = argc > 1 ? atoi(argv[3]) : 5;
    
  all.a = (int*)malloc(sizeof(int)*all.m);
  all.b = (int*)malloc(sizeof(int)*all.n);
  all.c = (int*)malloc(sizeof(int)*all.p);

  initialize(&all);

#pragma moya jit
  {
  kernel(&all);
  }

  free(all.c);
  free(all.b);
  free(all.a);
  
  print(all.a, all.m);
}
