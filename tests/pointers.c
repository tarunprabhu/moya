#include <stdio.h>
#include <stdlib.h>

void init_m(int argc, char *argv[], int *m) {
  *m = (argc > 2 ? atoi(argv[2]) : 37); 
}

void init_n(int argc, char *argv[], int *n) {
  *n = (argc > 3 ? atoi(argv[3]) : 73); 
}

#pragma moya specialize
int rotate(int *n) {
  int t = *n;
  int r = 0;
  while(t != 0) {
    r += t%10;
    t /= 10;
  }
  return t;
}

int main(int argc, char *argv[]) {
  int m, n;
  init_m(argc, argv, &m);
  init_n(argc, argv, &n);

  int *a = &m;
  int *b = &n;
  int opt = atoi(argv[1]);
  int r;

#pragma moya jit
  {
  if(opt)
    r = rotate(a);
  else
    r = rotate(b);
  }

  printf("r: %d\n", r);
}
