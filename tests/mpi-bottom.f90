subroutine pp(me, other)

  use mpi

  integer :: me, other
  integer :: sreq, rreq, ierr

  call mpi_isend(MPI_BOTTOM, 1, MPI_INT, mod(me+1, 2), 0, MPI_COMM_WORLD, sreq, ierr)
  call mpi_recv(MPI_BOTTOM, 1, MPI_INT, mod(me+1, 2), 0, MPI_COMM_WORLD, rreq, ierr)

end subroutine pp

program pingpong

  use mpi

  integer :: me, other
  integer :: ierr
  
  call mpi_init(ierr);
  call mpi_comm_rank(MPI_COMM_WORLD, me, ierr);

  ! @moya enable()
  call pp(me, other);
  ! @moya disable()

  call mpi_finalize(ierr);

  
end program pingpong
