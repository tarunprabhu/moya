module global
  type struct
     real(8), allocatable :: data(:)
  end type struct

  type(struct) :: s
end module global

program modstruct
  use global

  implicit none

  allocate(s%data(3))
  s%data(1) = 34.1

  write(*, '(F8.3)') s%data(1)
  deallocate(s%data)
end program modstruct
