#include <stdio.h>
#include <stdlib.h>

typedef struct AnimalT {
  int(*sound)();
} Animal;

int bark() {
  return 37;
}

int meow() {
  return 73;
}

Animal* make_animal() {
  return (Animal*)malloc(sizeof(Animal));
}

Animal* make_dog() {
  Animal *dog = make_animal();
  dog->sound = bark;
  return dog;
}

Animal* make_cat() {
  Animal *cat = make_animal();
  cat->sound = meow;
  return cat;
}

#pragma moya specialize
int sing(Animal *animal) {
  int song = 1;
  for(int i = 0; i < 4; i++)
    song *= animal->sound();
  return song;
}

int main(int argc, char *argv[]) {
  Animal *animal = NULL;
  int opt = atoi(argv[1]);
  if(opt)
    animal = make_dog();
  else
    animal = make_cat();

  int song;
#pragma moya jit
  {
  song = sing(animal);
  }

  free(animal);
  printf("song: %d\n", song);
  return 0;
}
