#include <vector>
#include <cstdlib>

using namespace std;

int main(int argc, char* argv[]) {
  vector<int*> ptrs;
  vector<int> lens;

  for(unsigned i = 0; i < argc; i++) {
    int l = atoi(argv[i]);
    ptrs.push_back(new int[l]);
    lens.push_back(l);
  }

  for(unsigned i = 0; i < ptrs.size(); i++)
    for(unsigned j = 0; j < lens.at(i); j++)
      ptrs[i][j] = random();

  return 0;
}
