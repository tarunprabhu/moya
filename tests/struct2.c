#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct AllT {
  struct ArraysT {
    int *a;
    int *b;
    int *c;
  } arrays;
  struct SizesT {
    int m;
    int n;
    int p;
  } sizes;
} All;

void initialize(All *all) {
  memset(all->arrays.a, 0, all->sizes.m*sizeof(int));
  for(int i = 0; i < all->sizes.n; i++)
    all->arrays.b[i] = rand();
  for(int i = 0; i < all->sizes.p; i++)
    all->arrays.c[i] = rand();
}

void print(int *a, int m) {
  for(int i = 0; i < m; i++)
    printf("%d ", a[i]);
  printf("\n");
}

#pragma moya specialize
void kernel(All *all) {
  for(int i = 0; i < all->sizes.m; i++)
    for(int j = 0; j < all->sizes.n; j++)
      for(int k = 0; k < all->sizes.p; k++)
        all->arrays.a[i] += (all->arrays.b[j]*all->arrays.c[k]);
}

int main(int argc, char *argv[]) {
  All all;
  
  all.sizes.m = argc > 1 ? atoi(argv[1]) : 1000;
  all.sizes.n = argc > 1 ? atoi(argv[2]) : 1000;
  all.sizes.p = argc > 1 ? atoi(argv[3]) : 5;
    
  all.arrays.a = (int*)malloc(sizeof(int)*all.sizes.m);
  all.arrays.b = (int*)malloc(sizeof(int)*all.sizes.n);
  all.arrays.c = (int*)malloc(sizeof(int)*all.sizes.p);

  initialize(&all);

#pragma moya jit
  {
  kernel(&all);
  }

  free(all.arrays.c);
  free(all.arrays.b);
  free(all.arrays.a);
  
  print(all.arrays.a, all.sizes.m);
}
