#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct NodeT {
  int val;
  struct NodeT *next;
} Node;

Node* make(int val) {
  Node *n = (Node*)malloc(sizeof(Node));
  n->val = val;
  n->next = NULL;
  return n;
}

#pragma moya specialize
void print(Node *head) {
  for(Node *curr = head; curr; curr = curr->next)
    printf("%d ", curr->val);
  printf("\n");
}

int main(int argc, char *argv[]) {
  Node *head = make(atoi(argv[1]));
  Node *prev = head;
  for(unsigned i = 2; i < argc; i++) {
    prev->next = make(atoi(argv[i]));
    prev = prev->next;
  }

#pragma moya jit
  {
  print(head);
  }
  
  return 0;
}
