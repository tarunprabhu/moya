#include <iostream>
#include <cstdlib>
#include <vector>
#include <memory>

using namespace std;

class Allocator {
private:
  vector<unique_ptr<int>> allocated;
  
public:
  void* allocate(int size) {
    allocated.emplace_back(new int[size]);
    int* r = allocated.back().get();
    for(int i = 0; i < size; i++)
      r[i] = random();
    return r;
  }
};

#pragma moya specialize
void vec_add(int *a, int *b, int *c, int n) {
  for(int i = 0; i < n; i++)
    c[i] = a[i] + b[i];
}

int main(int argc, char *argv[]) {
  Allocator alloc;
  int n = atoi(argv[1]);
  int *a = (int*)alloc.allocate(n);
  int *b = (int*)alloc.allocate(n);
  int *c = (int*)alloc.allocate(n);

#pragma moya jit
  {
  vec_add(a, b, c, n);
  }

  for(int i = 0; i < n; i++)
    cout << c[i] << " ";
  cout << endl;
  
  return 0;
}

