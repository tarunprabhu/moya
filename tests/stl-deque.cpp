#include <deque>
#include <cmath>

using namespace std;

typedef float T;
typedef deque<T> Container;

void constructors(int count) {
  Container d1;
  // Container d2(count);
  // Container d3(count, 1);
  Container d4(d1.begin(), d1.end());
  // Container d5(d3);
  // Container d6({2, 3, 4});
}

void operators(Container& d) {
  Container d7;
  Container d8;

  d7 = {5, 6, 7, 8};
  d8 = d7;
}

void assign(Container& d, int count) {
  Container dt({50, 51});
  
  d.assign(count, 17);
  d.assign(dt.begin(), dt.end());
  d.assign({18, 19, 20});
}

void allocators(Container& d) {
  d.get_allocator();
}

void access(Container& d, int index) {
  ceil(d.at(index));
  
  d[index] = 21;
  floor(d[index]);

  round(d.front());
  lrint(d.back());
}

void iterators(Container& d) {
  d.begin();
  d.end();
  d.cbegin();
  d.cend();
  d.rbegin();
  d.rend();
  d.crbegin();
  d.crend();
}

void iterators_math(Container& d, int count) {
  Container::iterator it = d.begin();
  Container::iterator jt = d.end();

  cos(*it);
  ++it;
  it++;
  --it;
  it--;
  jt = it + count;
  jt = it - count;
  it += count;
  it -= count;
  tan(it[count]);

  log(*jt);
  for(T t : d)
    sin(t);
}

void iterators_comp(Container& d) {
  Container::iterator it = d.begin();
  Container::iterator jt = d.end();

  sinh(it == jt);
  asinh(it != jt);
  cosh(it < jt);
  acosh(it <= jt);
  tanh(it > jt);
  atanh(it >= jt);
}

void capacity(Container& d) {
  d.empty();
  d.size();
  d.max_size();
  d.shrink_to_fit();
}

void modifiers(Container& d, int count) {
  Container dt({52, 53});
  
  d.clear();

  d.insert(d.begin(), 22);
  d.insert(d.begin(), count, 23);
  d.insert(d.begin(), dt.begin(), dt.end());
  d.insert(d.begin(), {9, 10, 11, 12});

  d.erase(d.begin());
  d.erase(d.begin(), d.end());

  d.push_back(24);
  d.push_front(25);

  d.pop_back();
  d.pop_front();

  d.resize(count);
  d.resize(count, 26);
}

void swap(Container& d) {
  Container d10;
  d10.swap(d);
}

void compare(Container& d1, Container& d2) {
  sinh(d1 == d2);
  asinh(d1 != d2);
  cosh(d1 < d2);
  acosh(d1 <= d2);
  tanh(d1 > d2);
  atanh(d1 >= d2);
}

int main(int argc, char* argv[]) {
  Container d;
  int count = argc;

  constructors(count);
  // allocators(d);
  // operators(d);
  // assign(d, count);
  // access(d, count);
  // iterators(d);
  // iterators_math(d, count);
  // iterators_comp(d);
  // capacity(d);
  // modifiers(d, count);
  // swap(d);
  // compare(d, d);

  return 0;
}
