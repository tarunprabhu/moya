#include <stdio.h>
#include <stdlib.h>

#pragma moya specialize
void empty_fn(int a) {

}

int main(int argc, char *argv[]) {
#pragma moya jit
  {
  empty_fn(atoi(argv[argc-1]));
  }

  return 0;
}
