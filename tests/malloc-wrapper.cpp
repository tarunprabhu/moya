#include <iostream>
#include <cstdlib>

using namespace std;

int* allocate(int size) {
  int *r = new int[size];
  for(int i = 0; i < size; i++)
    r[i] = random();
  return r;
}

void deallocate(int *a) {
  delete[] a;
}

#pragma moya specialize
void vec_add(int *a, int *b, int *c, int n) {
  for(int i = 0; i < n; i++)
    c[i] = a[i] + b[i];
}

int main(int argc, char *argv[]) {
  int n = atoi(argv[1]);
  int *a = allocate(n);
  int *b = allocate(n);
  int *c = allocate(n);

#pragma moya jit
  {
  vec_add(a, b, c, n);
  }

  for(int i = 0; i < n; i++)
    cout << c[i] << " ";
  cout << endl;
  
  deallocate(c);
  deallocate(b);
  deallocate(a);
  
  return 0;
}

