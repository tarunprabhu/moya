module specialize
contains
  !$moya specialize max(10+20)
  subroutine f()
  end subroutine f
end module specialize

program moya_specialize
  use specialize

  !$moya jit
  call f()
  !$moya end jit
end program moya_specialize
