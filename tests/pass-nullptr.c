#include <stdio.h>
#include <stdlib.h>

typedef struct ArrayT {
  int *nums;
  int n;
} Array;

#pragma moya specialize
long sum(Array *a) {
  long s = 0;
  if(a->nums)
    for(int i = 0; i < a->n; i++)
      s += a->nums[i];
  return s;
}

int main(int argc, char *argv[]) {
  int n = atoi(argv[1]);

  long s;
#pragma moya jit
  {
  s = sum(NULL);
  }
  
  return s;
}
