#include <stdio.h>

typedef struct MyStructT {
  long id;
  char c;
} MyStruct;

#pragma moya specialize
void print(MyStruct *s, int n) {
  for(int i = 0; i < n; i++)
    printf("{%ld, %c}", s[i].id, s[i].c);
  printf("\n");
}

int main(int argc, char *argv[]) {
  MyStruct a42 = { 42, 'a' };
  MyStruct z37 = { 37, 'z' };

  MyStruct s[2];
  s[0] = a42;
  s[1] = z37;

#pragma moya jit
  {
  print(s, 2);
  }
  
  return 0;
}
