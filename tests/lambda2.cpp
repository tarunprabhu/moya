#include <iostream>

using namespace std;

auto outer = [](int m) {
  #pragma moya specialize  
  auto inner = [=]() {
    cout << m << endl;
  };
  inner();
};

int main(int argc, char* argv[]) {
  int m = atoi(argv[argc - 1]);

#pragma moya jit
  {
  outer(m);
  }
  return 0;
}
