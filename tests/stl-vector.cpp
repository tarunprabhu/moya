#include <vector>
#include <set>
#include <unordered_set>
#include <cmath>
#include <iostream>

using namespace std;

typedef float E;
typedef E* T;

#define GET(x) *(x)
#define PUT(x) &x

typedef vector<T> Container;
typedef unordered_set<T> Other;

int retval = 32;

void constructors(const Other& other, int count) {
  E local1, local2;
    
  Container v1;
  Container v2(count);
  Container v3(count, PUT(local1));
  Container v4(other.begin(), other.end());
  Container v5(v3);
  Container v6({PUT(local2)});
}

void allocators(const Container& v) {
  v.get_allocator();
}

void operators(const Container& v) {
  T element = *v.begin();
  Container v5;
  Container v6;

  v5 = {element};
  v6 = v5;
}

void assign(const Other& other, int count) {
  E local1, local2;
  
  vector<T> vt;
  
  vt.assign(count, PUT(local1));
  vt.assign(other.begin(), other.end());
  vt.assign({PUT(local2)});
}

void access(const Container& v, int index) {
  Container vt(v);
  E local;
  
  ceil(GET(vt.at(index)));

  vt[index] = PUT(local);
  floor(GET(vt[index]));

  round(GET(vt.front()));
  lrint(GET(vt.back()));
  v.data();
}

void iterators(const Container& v) {
  auto vb = v.begin();
  auto ve = v.end();
  auto vcb = v.cbegin();
  auto vce = v.cend();
  auto vrb = v.rbegin();
  auto vre = v.rend();
  auto vcrb = v.crbegin();
  auto vcre = v.crend();

  std::cout << *vb << " " << *ve << " " << *vcb << " " << *vce << " " << *vrb
            << " " << *vre << " " << *vcrb << " " << *vcre << "\n";
}

void iterators_math(const Container& v, int count) {
  auto it = v.begin();
  auto jt = v.end();

  cos(GET(*it));
  ++it;
  it++;
  --it;
  it--;
  jt = it + count;
  jt = it - count;
  it += count;
  it -= count;
  tan(GET(it[count]));

  it.base();
  log(GET(*jt));
  for(T t : v)
    sin(GET(t));
}

void iterators_comp(const Container& v) {
  auto it = v.begin();
  auto jt = v.end();

  sinh(it == jt);
  asinh(it != jt);
  cosh(it < jt);
  acosh(it <= jt);
  tanh(it > jt);
  atanh(it >= jt);
}

void capacity(Container& v, int count) {
  v.empty();
  v.size();
  v.max_size();

  v.reserve(count);
  v.capacity();
  v.shrink_to_fit();
}

void modifiers(const Other& other, int count) {
  E local1, local2, local3, local4;
  
  vector<T> v;
  
  v.clear();

  v.insert(v.begin(), PUT(local1));
  v.insert(v.begin(), count, PUT(local2));
  v.insert(v.begin(), other.begin(), other.end());
  v.insert(v.begin(), {PUT(local3)});

  v.erase(v.begin());
  v.erase(v.begin(), v.end());

  v.push_back(PUT(local3));
  v.pop_back();

  v.resize(count);
  v.resize(count, PUT(local4));
}

void compare(const Container& x1, Container& x2) {
  sinh(x1 == x2);
  asinh(x1 != x2);
  cosh(x1 < x2);
  acosh(x1 <= x2);
  tanh(x1 > x2);
  atanh(x1 >= x2);
}

int main(int argc, char* argv[]) {
  E element = static_cast<E>(argc);
  Container v({PUT(element)});
  Container target;
  Other other({PUT(element)});
  int count = argc;

  constructors(other, count);
  allocators(v);
  operators(v);
  assign(other, count);
  access(v, count);
  iterators(v);
  iterators_math(v, count);
  iterators_comp(v);
  capacity(v, count);
  modifiers(other, count);
  compare(v, v);

  v.swap(target);
  
  return retval;
}
