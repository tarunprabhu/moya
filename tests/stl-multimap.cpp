#include <map>
#include <cmath>

using namespace std;

typedef float K;
typedef double V;
typedef multimap<K, V> Container;

void constructors() {
  Container m1;
  Container m2({{1, 2}, {3, 4}});
  Container m3(m2);
  Container m4(m3.begin(), m3.end());  
}

void operators(Container& m) {
  Container m5;
  Container m6;

  m6 = {{5, 6}, {7, 8}};
  m5 = m6;
}

void allocators(Container& m) {
  m.get_allocator();
}

void iterators(Container& m) {
  m.begin();
  m.end();
  m.cbegin();
  m.cend();
  m.rbegin();
  m.rend();
  m.crbegin();
  m.crend();
}

void iterators_math(Container& m) {
  Container::iterator it = m.begin();
  Container::iterator jt = it;
  Container::reverse_iterator rit = m.rbegin();
  Container::reverse_iterator rjt = rit;

  cos(it->first);
  sin((*it).second);
  ++it;
  it++;
  --it;
  it--;
  log(jt->first);
  log2((*jt).second);
  for(auto it : m) {
    sinh(it.first);
    cosh(it.second);
  }

  acos(rit->first);
  asin((*rit).second);
  ++rit;
  rit++;
  --rit;
  rit--;
  exp(rjt->first);
  exp2((*rjt).second);
}

void iterators_comp(Container& m) {
  Container::iterator it = m.begin();
  Container::iterator jt = it;
  Container::iterator rit = m.begin();
  Container::iterator rjt = rit;

  asinh(it == jt);
  acosh(it != jt);
  tan(rit == rjt);
  atan(rit != rjt);
}

void capacity(Container& m) {
  m.empty();
  m.size();
  m.max_size();
}

void modifiers(Container& m) {
  Container mt({{50, 51}, {52, 53}});

  m.clear();

  m.insert(pair<K, V>(13, 14));
  m.insert(m.begin(), pair<K, V>(15, 16));
  m.insert(mt.begin(), mt.end());
  m.insert({{9, 10}, {11, 12}});

  m.erase(m.begin());
  m.erase(m.begin(), m.end());
  m.erase(13);
}

void swap(Container& m) {
  Container mt;
  mt.swap(m);
}

void lookups(Container& m, K key) {
  m.count(key);
  m.find(key);
  m.equal_range(key);
  m.lower_bound(key);
  m.upper_bound(key);
}

void observers(Container& m) {
  // m.key_comp();
  // m.value_comp();
}

int main(int argc, char* argv[]) {
  Container m;
  K key = argc;

  constructors();
  allocators(m);
  operators(m);
  iterators(m);
  iterators_math(m);
  iterators_comp(m);
  capacity(m);
  modifiers(m);
  lookups(m, key);
  swap(m);
  observers(m);

  return 0;
}
