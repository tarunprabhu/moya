module Moya

  use iso_c_binding, only: c_funptr, c_int64_t

  ! --------------------------------------------------------------------
  ! These are routines that Pilot will insert into the Fortran code

  interface moya___enter_region
     subroutine moya___enter_region_fort(id) &
          bind(c, name="moya___enter_region_fort")
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int32_t), value, intent(in) :: id
     end subroutine moya___enter_region_fort
  end interface moya___enter_region

  interface moya___exit_region
     subroutine moya___exit_region_fort(id) &
          bind(c, name="moya___exit_region_fort")
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int32_t), value, intent(in) :: id
     end subroutine moya___exit_region_fort
  end interface moya___exit_region

  interface moya___is_in_region
     logical(c_bool) function moya___is_in_region_fort() &
          bind(c, name="moya___is_in_region_fort")
       use, intrinsic :: iso_c_binding
       implicit none
     end function moya___is_in_region_fort
  end interface moya___is_in_region

  ! -----------------------------------------------------------------------
  ! This is a debug function that is used only when debugging Moya. It should
  ! never appear in code
  interface moya___debug
     subroutine moya___debug_print_i8(var, deref) &
          bind(c, name="moya___debug_print_i8_fort_")
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int8_t), intent(in) :: var
       integer(c_int), value, intent(in) :: deref
     end subroutine moya___debug_print_i8

     subroutine moya___debug_print_i16(var, deref) &
          bind(c, name="moya___debug_print_i16_fort_")
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int16_t), intent(in) :: var
       integer(c_int), value, intent(in) :: deref
     end subroutine moya___debug_print_i16

     subroutine moya___debug_print_i32(var, deref) &
          bind(c, name="moya___debug_print_i32_fort_")
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int32_t), intent(in) :: var
       integer(c_int), value, intent(in) :: deref
     end subroutine moya___debug_print_i32

     subroutine moya___debug_print_i64(var, deref) &
          bind(c, name="moya___debug_print_i64_fort_")
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int64_t), intent(in) :: var
       integer(c_int), value, intent(in) :: deref
     end subroutine moya___debug_print_i64

     subroutine moya___debug_print_f32(var, deref) &
          bind(c, name="moya___debug_print_f32_fort_")
       use, intrinsic :: iso_c_binding
       implicit none
       real(c_float), intent(in) :: var
       integer(c_int), value, intent(in) :: deref
     end subroutine moya___debug_print_f32

     subroutine moya___debug_print_f64(var, deref) &
          bind(c, name="moya___debug_print_f64_fort_")
       use, intrinsic :: iso_c_binding
       implicit none
       real(c_double), intent(in) :: var
       integer(c_int), value, intent(in) :: deref
     end subroutine moya___debug_print_f64

     subroutine moya___debug_print_f128(var, deref) &
          bind(c, name="moya___debug_print_f128_fort_")
       use, intrinsic :: iso_c_binding
       implicit none
       real(c_long_double), intent(in) :: var
       integer(c_int), value, intent(in) :: deref
     end subroutine moya___debug_print_f128

     subroutine moya___debug_print_ptr(var) &
          bind(c, name="moya___debug_print_ptr_fort_")
       use, intrinsic :: iso_c_binding
       implicit none
       type(*), intent(in) :: var
     end subroutine moya___debug_print_ptr
  end interface moya___debug
  
end module Moya
