#include "LangConfigC.h"
#include "Set.h"
#include "StringUtils.h"

static const moya::Set<std::string> exts = {"c", "h", "i"};

bool LangConfigC::isValidSourceFile(const std::string& file) {
  return exts.contains(moya::StringUtils::rpartition(file, ".").second);
}
