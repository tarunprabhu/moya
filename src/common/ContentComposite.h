#ifndef MOYA_COMMON_CONTENT_COMPOSITE_H
#define MOYA_COMMON_CONTENT_COMPOSITE_H

#include "Contents.h"
#include "Vector.h"

namespace moya {

// This composite class is only intended to represent temporary contents that
// we get from reading a non-primitive type directly from memory. Otherwise,
// structs and arrays are broken down into their individual components when
// dealing with the abstract memory. This is why this class doesn't have an
// LLVM Constant associated with it because even for compile-time constant
// non-scalar types, we will break it down into individual constants and not
// deal with the constant aggregate as a whole
//
class ContentComposite : public Content {
protected:
  Vector<Contents> elems;

public:
  ContentComposite(const Vector<Contents>&);
  virtual ~ContentComposite() = default;

  unsigned long size() const;
  void insertAt(unsigned long, const Content*);
  void insertAt(unsigned long, const Contents&);
  Contents at(unsigned long) const;
  Contents operator[](unsigned long) const;

  virtual std::string str() const;
  virtual void serialize(Serializer&) const;

public:
  static bool classof(const Content*);
};

} // namespace moya

#endif // MOYA_COMMON_CONTENT_COMPOSITE_H
