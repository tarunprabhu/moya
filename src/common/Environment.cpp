#include "Environment.h"
#include "Diagnostics.h"
#include "Encoder.h"
#include "Hexify.h"
#include "JSONUtils.h"
#include "LLVMUtils.h"
#include "Location.h"
#include "Metadata.h"
#include "PathUtils.h"
#include "RegexUtils.h"
#include "Serializer.h"
#include "StringUtils.h"

#include <llvm/BinaryFormat/Dwarf.h>
#include <llvm/IR/CFG.h>
#include <llvm/IR/DebugInfoMetadata.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Metadata.h>

#include <iomanip>
#include <regex>
#include <sstream>

using llvm::cast;
using llvm::dyn_cast;
using llvm::isa;

namespace moya {

static const Contents emptyContents;
static const Environment::CallSites emptyCallsites;
static const Environment::Callees emptyCallees;

Environment::Environment(const llvm::Module& module,
                         const ClassHeirarchies& heirarchies)
    : module(module), layout(module), heirarchies(heirarchies) {
  for(llvm::StructType* sty : LLVMUtils::getStructTypes(module))
    layout.getStructLayout(sty);
}

bool Environment::isDerivedFrom(llvm::Type* derived, llvm::Type* base) const {
  return heirarchies.isDerivedFrom(derived, base);
}

bool Environment::isBaseOf(llvm::Type* base, llvm::Type* derived) const {
  return heirarchies.isBaseOf(base, derived);
}

bool Environment::contains(const llvm::Value* v) const {
  return env.contains(v);
}

bool Environment::contains(MoyaID id) const {
  return ids.contains(id);
}

const Contents& Environment::lookup(const llvm::Value* v) const {
  if(not contains(v))
    return emptyContents;
  return env.at(v);
}

const Contents& Environment::lookup(MoyaID id) const {
  if(not ids.contains(id))
    return emptyContents;
  return lookup(ids.at(id));
}

const Environment::Address*
Environment::lookupArg(const llvm::Argument* arg) const {
  return fargs.at(arg);
}

const Environment::Address* Environment::lookupArg(MoyaID id) const {
  return fargs.at(cast<llvm::Argument>(ids.at(id)));
}

const llvm::Argument* Environment::lookupArg(const Address* addr) const {
  return fargs.key_at(addr);
}

const Environment::Address*
Environment::lookupFunction(const llvm::Function* f) const {
  return funcs.at(f);
}

const Environment::Address*
Environment::lookupFunction(const llvm::Function& f) const {
  return lookupFunction(&f);
}

const Environment::Address* Environment::lookupFunction(MoyaID id) const {
  return funcs.at(cast<llvm::Function>(ids.at(id)));
}

const llvm::Function* Environment::lookupFunction(const Address* addr) const {
  return funcs.key_at(addr);
}

const Environment::Address*
Environment::lookupGlobal(const llvm::GlobalVariable* g) const {
  return globals.at(g);
}

const Environment::Address*
Environment::lookupGlobal(const llvm::GlobalVariable& g) const {
  return lookupGlobal(&g);
}

const Environment::Address* Environment::lookupGlobal(MoyaID id) const {
  return globals.at(cast<llvm::GlobalVariable>(ids.at(id)));
}

const llvm::GlobalVariable*
Environment::lookupGlobal(const Address* addr) const {
  return globals.key_at(addr);
}

const Environment::Address*
Environment::lookupAlloca(const llvm::AllocaInst* a) const {
  return allocas.at(a);
}

const Environment::Address* Environment::lookupAlloca(MoyaID id) const {
  return allocas.at(cast<llvm::AllocaInst>(ids.at(id)));
}

const llvm::AllocaInst* Environment::lookupAlloca(const Address* addr) const {
  return allocas.key_at(addr);
}

const Environment::Address*
Environment::lookupInsertValue(const llvm::InsertValueInst* inst) const {
  return insvals.at(inst);
}

void Environment::addFile(const std::string& path) {
  files.insert(path);
}

bool Environment::addArg(const llvm::Argument* arg, const Address* addr) {
  if(fargs.contains(arg))
    moya_error("Argument already in environment: " << *arg);

  fargs.emplace(std::make_pair(arg, addr));
  if(Metadata::hasID(arg))
    ids[Metadata::getID(arg)] = arg;
  return true;
}

bool Environment::addFunction(const llvm::Function* f, const Address* addr) {
  if(funcs.contains(f))
    moya_error("Function already in environment: " << f);

  funcs.emplace(std::make_pair(f, addr));
  if(Metadata::hasID(f))
    ids[Metadata::getID(f)] = f;
  return add(f, addr);
}

bool Environment::addGlobal(const llvm::GlobalVariable* g,
                            const Address* addr) {
  if(globals.contains(g))
    moya_error("Global already in environment: " << *g);

  globals.emplace(std::make_pair(g, addr));
  if(Metadata::hasID(g))
    ids[Metadata::getID(g)] = g;
  return add(g, addr);
}

bool Environment::addAlloca(const llvm::AllocaInst* a, const Address* addr) {
  if(allocas.contains(a))
    moya_error("Alloca already in environment: " << *a);

  allocas.emplace(std::make_pair(a, addr));
  return add(a, addr);
}

bool Environment::addInsertValue(const llvm::InsertValueInst* inst,
                                 const Address* addr) {
  if(insvals.contains(inst))
    moya_error("Insert value already in environment: " << *inst);

  insvals[inst] = addr;
  return true;
}

bool Environment::add(const llvm::Value* v) {
  if(contains(v))
    moya_error("Value already in environment: " << *v);

  env[v] = emptyContents;
  if(auto* inst = dyn_cast<llvm::Instruction>(v))
    if(Metadata::hasID(inst))
      ids[Metadata::getID(inst)] = inst;
  return true;
}

bool Environment::add(const llvm::Value* v, const Content* c) {
  if(not c)
    moya_error("Content cannot be null: " << *v);

  bool changed = false;
  if(not env.contains(v))
    add(v);
  changed |= env[v].insert(c);
  return changed;
}

bool Environment::add(const llvm::Value* v, const Contents& contents) {
  bool changed = false;
  for(const Content* c : contents)
    changed |= add(v, c);
  return changed;
}

bool Environment::isFunction(const Address* addr) const {
  return funcs.contains_value(addr);
}

bool Environment::isArg(const Address* addr) const {
  return fargs.contains_value(addr);
}

bool Environment::isGlobal(const Address* addr) const {
  return globals.contains_value(addr);
}

bool Environment::isAlloca(const Address* addr) const {
  return allocas.contains_value(addr);
}

bool Environment::hasInsertValue(const llvm::InsertValueInst* insval) const {
  return insvals.contains(insval);
}

bool Environment::hasCallSites(const llvm::Function* f) const {
  return callSites.contains(f);
}

const Environment::CallSites&
Environment::getCallSites(const llvm::Function* f) const {
  if(not callSites.contains(f))
    return emptyCallsites;
  return callSites.at(f);
}

const Environment::Callees&
Environment::getCallees(const llvm::Instruction* inst) const {
  if(not callees.contains(inst))
    return emptyCallees;
  return callees.at(inst);
}

bool Environment::addCallsite(const llvm::Function* callee,
                              const llvm::Instruction* call) {
  bool changed = false;

  changed |= callSites[callee].insert(call);
  changed |= callees[call].insert(callee);

  return changed;
}

bool Environment::push(const Addresses& rv) {
  retStack.emplace(rv);
  return false;
}

bool Environment::push(const Contents& rv) {
  retStack.emplace(rv);
  return false;
}

bool Environment::push(const Content* c) {
  retStack.emplace(c);
  return false;
}

Contents Environment::pop() {
  if(retStack.empty())
    moya_error("Attempting to pop empty stack\n");
  return retStack.pop();
}

const Contents& Environment::top() {
  return retStack.top();
}

bool Environment::isEmpty() const {
  return retStack.empty();
}

std::string Environment::str() const {
  std::string s;
  llvm::raw_string_ostream ss(s);
  ss << "function {\n";
  for(const llvm::Function& f : module.functions())
    ss << "  " << getDiagnostic(f) << ": " << moya::str(lookup(&f)) << "\n";
  ss << "}\n";
  ss << "\n";
  ss << "globals {\n";
  for(const llvm::GlobalVariable& g : module.globals())
    ss << "  " << getDiagnostic(g) << ": " << moya::str(lookup(&g)) << "\n";
  ss << "}\n";
  return ss.str();
}

void Environment::serializeFunctions(Serializer& s) const {
  moya_message("Serialize functions");

  auto shouldSerialize = [](const llvm::Instruction* inst) {
    if(isa<llvm::LoadInst>(inst) or isa<llvm::StoreInst>(inst)) {
      return true;
    } else if(const auto* call = dyn_cast<llvm::CallInst>(inst)) {
      if(const llvm::Function* f = call->getCalledFunction())
        return not StringUtils::startswith(f->getName(), "llvm.dbg");
      return true;
    } else if(const auto* invoke = dyn_cast<llvm::InvokeInst>(inst)) {
      if(const llvm::Function* f = invoke->getCalledFunction())
        return not StringUtils::startswith(f->getName(), "llvm.dbg");
      return true;
    }
    return false;
  };

  s.mapStart("functions");
  for(const auto& it : funcs) {
    const llvm::Function* f = it.first;
    const ContentAddress* faddr = it.second;
    if((not f->size()) or Metadata::hasNoAnalyze(f))
      continue;
    if(not hasCallSites(f))
      continue;

    s.mapStart(moya::str(f));

    s.add("_class", "Function");
    s.add("id", moya::str(f));
    s.add("llvm", f->getName().str());
    if(Metadata::hasSourceName(f))
      s.add("source", Metadata::getSourceName(f));
    if(Metadata::hasQualifiedName(f))
      s.add("qualified", Metadata::getQualifiedName(f));
    if(Metadata::hasFullName(f))
      s.add("full", Metadata::getFullName(f));
    if(Metadata::hasArtificialName(f))
      s.add("artificial", Metadata::getArtificialName(f));
    s.add("type", JSON::pointer(f->getFunctionType()));
    s.add("address", JSON::pointer(faddr));
    if(Metadata::hasSourceLocation(f))
      s.add("location", Metadata::getSourceLocation(f));
    else if(llvm::DISubprogram* di = f->getSubprogram())
      s.add("location", LLVMUtils::getLocation(di));
    if(Metadata::hasBeginLocation(f) and Metadata::hasEndLocation(f)) {
      s.add("begin", Metadata::getBeginLocation(f));
      s.add("end", Metadata::getEndLocation(f));
    }

    if(Metadata::hasClass(f))
      s.add("class", JSON::pointer(Metadata::getClass(f)));

    if(Metadata::hasEnclosing(f))
      s.add("enclosing", JSON::pointer(Metadata::getEnclosing(f)));

    s.arrStart("flags");
    if(Metadata::hasSpecialize(f))
      s.serialize("MoyaJIT");
    s.arrEnd();

    std::string buf;
    llvm::raw_string_ostream ss(buf);
    ss << *f;
    const std::string ir = ss.str();
    s.add("ir", Encoder::encodeString(ir));

    s.arrStart("args");
    for(const llvm::Argument& arg : f->args()) {
      s.mapStart();

      s.add("_class", "Argument");
      s.add("id", moya::str(&arg));
      if(arg.hasName())
        s.add("llvm", arg.getName().str());
      if(Metadata::hasSourceName(arg))
        s.add("source", Metadata::getSourceName(arg));
      if(Metadata::hasArtificialName(arg))
        s.add("artificial", Metadata::getArtificialName(arg));
      s.add("argno", arg.getArgNo());
      s.add("type", JSON::pointer(LLVMUtils::getAnalysisType(arg)));
      s.add("address", JSON::pointer(lookupArg(&arg)));
      s.add("function", JSON::pointer(f));
      if(Metadata::hasSourceLocation(arg))
        s.add("location", Metadata::getSourceLocation(arg), f);
      if(Metadata::hasFlangLinked(arg))
        s.add("linked",
              JSON::pointer(
                  LLVMUtils::getArgument(f, Metadata::getFlangLinked(arg))));
      s.arrStart("flags");
      if(Metadata::hasFlangArray(arg))
        s.serialize("Array");
      if(Metadata::hasArtificial(arg))
        s.serialize("Artificial");
      if(Metadata::hasIgnore(arg))
        s.serialize("MoyaIgnore");
      s.arrEnd();

      s.mapEnd(); // arg
    }
    s.arrEnd("args");

    s.mapStart("blocks");
    for(const auto& bb : *f) {
      s.mapStart(moya::str(&bb));
      s.add("_class", "Block");
      s.add("id", moya::str(&bb));
      s.arrStart("successors");
      for(const llvm::BasicBlock* succ : llvm::successors(&bb))
        s.serialize(JSON::pointer(succ));
      s.arrEnd("successors");
      s.arrStart("instructions");
      for(auto& inst : bb)
        if(shouldSerialize(&inst))
          s.serialize(JSON::pointer(&inst));
      s.arrEnd("instructions");
      s.mapEnd();
    }
    s.mapEnd("blocks");

    s.mapStart("locals");
    for(auto i = llvm::inst_begin(f); i != llvm::inst_end(f); i++) {
      if(const auto* alloca = dyn_cast<llvm::AllocaInst>(&*i)) {
        // If a function is never called, the allocas will not be in the
        // environment. We probably should fix this so the allocas are
        // always processed regardless of whether or not the function is
        // called
        if(env.contains(alloca)) {
          llvm::Type* type = LLVMUtils::getAllocatedType(alloca);
          s.mapStart(moya::str(&*i));
          s.add("_class", "Local");
          s.add("id", moya::str(&*i));
          if(alloca->hasName())
            s.add("llvm", alloca->getName().str());
          if(Metadata::hasSourceName(alloca))
            s.add("source", Metadata::getSourceName(alloca));

          s.add("type", JSON::pointer(type));
          s.add("address", JSON::pointer(lookupAlloca(alloca)));

          if(Metadata::hasSourceLocation(alloca))
            s.add("location", Metadata::getSourceLocation(alloca), f);
          else if(llvm::DebugLoc loc = alloca->getDebugLoc())
            s.add("location", LLVMUtils::getLocation(loc), f);
          s.mapEnd();
        }
      }
    }
    s.mapEnd("locals");

    const std::regex reEmpty("^\\s*$");
    const std::regex reComment("^\\s*;.*$");
    const std::regex reSwitch("^\\s*\\].*$");
    const Vector<std::string> lines = StringUtils::split(ir, '\n');
    Map<const llvm::Value*, uint64_t> inos;

    s.mapStart("instructions");
    // This is a "fake" instructions. There are times when a cell gets marked
    // with a function but without an actual instructions. We still want to
    // be have an instruction to associate those accesses
    s.mapStart("0");
    s.add("_class", "Dummy");
    s.mapEnd("0");
    uint64_t ino = 0;
    while(not StringUtils::startswith(lines[ino], "define"))
      ino++;
    // ino points to the function definition line. Move to the next
    // instruction
    ino++;
    for(auto i = llvm::inst_begin(f); i != inst_end(f); i++, ino++) {
      const llvm::Instruction* inst = &*i;
      // Skip blank and comment lines
      while(Regex::matches(lines[ino], reEmpty)
            or Regex::matches(lines[ino], reComment))
        ino++;
      inos[inst] = ino;
      // XXX: This is a bug! and a very shitty hack!
      // switch instructions span multiple lines so we should treat it as
      // starting from the "last" line of the instruction. We don't really
      // care about right now, so I'm not going to whinge about it.
      if(isa<llvm::SwitchInst>(inst)) {
        while(not Regex::matches(lines[ino], reSwitch))
          ino++;
        // XXX: This is a hack. Second line of the instruction is a cleanup
      } else if(const auto* pad = dyn_cast<llvm::LandingPadInst>(inst)) {
        if(pad->isCleanup())
          ino++;
        ino += pad->getNumClauses();
      }
      if(shouldSerialize(inst)) {
        s.mapStart(moya::str(inst));
        s.add("id", moya::str(inst));
        if(llvm::DebugLoc dbg = inst->getDebugLoc())
          s.add("location", LLVMUtils::getLocation(dbg), f);
        s.add("location_ir", ino + 1);

        if(const auto* load = dyn_cast<llvm::LoadInst>(inst)) {
          s.add("_class", "Load");
          if(const auto* ptr
             = dyn_cast<llvm::Instruction>(load->getPointerOperand()))
            s.add("pointer", inos.at(ptr) + 1);
          s.arrStart("targets");
          for(const auto* c : lookup(load->getPointerOperand()))
            if(const auto* caddr = dyn_cast<ContentAddress>(c))
              s.serialize(JSON::pointer(caddr));
          s.arrEnd("targets");
        } else if(const auto* store = dyn_cast<llvm::StoreInst>(inst)) {
          s.add("_class", "Store");
          if(const auto* ptr
             = dyn_cast<llvm::Instruction>(store->getPointerOperand()))
            s.add("pointer", inos.at(ptr) + 1);
          s.arrStart("targets");
          for(const auto* c : lookup(store->getPointerOperand()))
            if(const auto* caddr = dyn_cast<ContentAddress>(c))
              s.serialize(JSON::pointer(caddr));
          s.arrEnd("targets");
        } else if(const auto* call = dyn_cast<llvm::CallInst>(&*i)) {
          s.add("_class", "Call");
          s.arrStart("functions");
          for(const llvm::Function* callee : getCallees(inst))
            if(callee->size() and (not moya::Metadata::hasNoAnalyze(callee)))
              s.serialize(JSON::pointer(callee));
          s.arrEnd("functions");
        } else if(const auto* invoke = dyn_cast<llvm::InvokeInst>(&*i)) {
          s.add("_class", "Call");
          s.arrStart("functions");
          for(const llvm::Function* callee : getCallees(inst))
            if(callee->size() and (not moya::Metadata::hasNoAnalyze(callee)))
              s.serialize(JSON::pointer(callee));
          s.arrEnd("functions");
          // Invokes will have a second line with the exception destination
          ino++;
        } else {
          moya_error("Unknown instruction to serialize: " << *inst);
        }

        s.mapEnd();
      }
    }
    s.mapEnd("instructions");

    s.arrStart("callsites");
    if(callSites.contains(f))
      for(const llvm::Instruction* inst : callSites.at(f))
        // The callsite could be null for C++ global constructor functions
        if(inst and inst->getDebugLoc())
          s.serialize(JSON::pointer(inst));
    s.arrEnd("callsites");

    s.mapEnd(); // function
  }

  // Create dummy functions for each region.
  // These can be thought of as functions obtained by outlining the code
  // for each region. Only real purpose is to serve as the root of the
  // callgraph
  //
  // NOTE: The region ID "probably" won't collide with any of the function
  // ID's because the regionID will be tagged so the LSB is odd. On most
  // machines, this should not be a problem. But if someone decides to make
  // one where odd-valued alignments are allowed, we might have trouble
  for(const moya::Region& region : moya::Metadata::getRegions(module)) {
    std::string rid = moya::str(region.getID());
    s.mapStart(rid);
    s.add("_class", "Function");
    s.add("id", rid);
    s.add("artificial", "__moya_wrapper_" + rid);
    s.arrStart("flags");
    s.serialize("Artificial");
    s.arrEnd("flags");
    s.mapEnd(rid);
  }

  s.mapEnd("functions"); // functions
} // namespace moya

void Environment::serializeGlobals(Serializer& s) const {
  moya_message("Serialize globals");

  s.mapStart("globals");
  for(const auto& it : env) {
    if(const auto* g = dyn_cast<llvm::GlobalVariable>(it.first)) {
      if(g->hasName() and (not g->isConstant())
         and (Metadata::hasSourceName(g) or Metadata::hasArtificialName(g))) {
        auto* type = LLVMUtils::getAnalysisTypeAs<llvm::PointerType>(g)
                         ->getElementType();
        s.mapStart(moya::str(g));
        s.add("_class", "Global");
        s.add("id", moya::str(g));
        s.add("llvm", JSON::escape(g->getName().str()));
        if(Metadata::hasSourceName(g))
          s.add("source", Metadata::getSourceName(g));
        if(Metadata::hasQualifiedName(g))
          s.add("qualified", Metadata::getQualifiedName(g));
        if(Metadata::hasFullName(g))
          s.add("full", Metadata::getFullName(g));
        if(Metadata::hasArtificialName(g))
          s.add("artificial", Metadata::getArtificialName(g));
        s.add("type", JSON::pointer(type));
        s.add("size", layout.getTypeAllocSize(type));
        s.add("address", JSON::pointer(lookupGlobal(g)));
        if(Metadata::hasSourceLocation(g))
          s.add("location", Metadata::getSourceLocation(g));
        s.arrStart("flags");
        if(Metadata::hasArtificial(g))
          s.serialize("Artificial");
        s.arrEnd();
        s.mapEnd();
      }
    }
  }
  s.mapEnd();
}

void Environment::serializeFiles(Serializer& s) const {
  moya_message("Serialize files");

  s.mapStart("files");
  for(const std::string& path : files) {
    s.mapStart(moya::str(path));
    s.add("_class", "SourceFile");
    s.add("dir", PathUtils::dirname(path));
    s.add("file", PathUtils::basename(path));
    s.add("contents", Encoder::encodeFile(path));
    s.mapEnd();
  }
  s.mapEnd();
}

} // namespace moya
