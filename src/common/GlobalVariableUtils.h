#ifndef MOYA_COMMON_GLOBAL_VARIABLE_UTILS_H
#define MOYA_COMMON_GLOBAL_VARIABLE_UTILS_H

#include <llvm/IR/GlobalVariable.h>

#include <string>

namespace moya {

namespace LLVMUtils {

llvm::Module* getModule(llvm::GlobalValue*);
llvm::Module* getModule(llvm::GlobalValue&);
const llvm::Module* getModule(const llvm::GlobalValue*);
const llvm::Module* getModule(const llvm::GlobalValue&);

llvm::Type* getAnalysisType(const llvm::GlobalVariable*);
llvm::Type* getAnalysisType(const llvm::GlobalVariable&);
template <typename T>
T* getAnalysisTypeAs(const llvm::GlobalVariable* g) {
  return llvm::dyn_cast_or_null<T>(getAnalysisType(g));
}
template <typename T>
T* getAnalysisTypeAs(const llvm::GlobalVariable& g) {
  return llvm::dyn_cast_or_null<T>(getAnalysisType(g));
}

template <typename T,
          typename std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
T getInitializerAs(const llvm::GlobalVariable*);

template <typename T,
          typename std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
T getInitializerAs(const llvm::GlobalVariable&);

template <typename T,
          typename std::enable_if_t<std::is_class<T>::value, int> = 0>
const T* getInitializerAs(const llvm::GlobalVariable*);

template <typename T,
          typename std::enable_if_t<std::is_class<T>::value, int> = 0>
const T* getInitializerAs(const llvm::GlobalVariable&);

} // namespace LLVMUtils

} // namespace moya

#endif // MOYA_COMMON_GLOBAL_VARIABLE_UTILS_H
