#ifndef MOYA_COMMON_LOCATION_H
#define MOYA_COMMON_LOCATION_H

#include "Hasher.h"

#include <llvm/IR/Function.h>

#include <string>

namespace moya {

class PayloadBase;
class Serializer;
class Deserializer;

class Location {
public:
  enum Cmp { Incomparable = -1, Before, After, Identical };

private:
  std::string path;
  unsigned line;
  unsigned column;
  bool valid;
  Hash hash;

protected:
  bool getValid() const;
  void updateHash();

public:
  Location();
  Location(const std::string&, unsigned, unsigned = 0);
  Location(const std::string&, const std::string&, unsigned, unsigned = 0);
  Location(const Location&) = default;

  const std::string& getFile() const;
  unsigned getLine() const;
  unsigned getColumn() const;
  bool isValid() const;
  Hash getHash() const;

  std::string str() const;
  Serializer& serialize(Serializer&, const llvm::Function* = nullptr) const;
  Deserializer& deserialize(Deserializer&);
  void pickle(PayloadBase&) const;
  Location& unpickle(PayloadBase&);

  Location& operator=(const Location&) = default;
  bool operator==(const Location&) const;
  bool operator!=(const Location&) const;

public:
  static Location::Cmp compare(const Location&, const Location&);
  static bool isBetween(const Location&, const Location&, const Location&);
};

} // namespace moya

#endif // MOYA_COMMON_LOCATION_H
