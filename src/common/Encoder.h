#ifndef MOYA_COMMON_FILE_ENCODER_H
#define MOYA_COMMON_FILE_ENCODER_H

#include "Vector.h"

#include <string>

namespace moya {

namespace Encoder {

std::string encodeFile(const std::string&);
std::string encodeString(const std::string&);
std::string encodeString(const Vector<unsigned char>&);

} // namespace Encoder

} // namespace moya

#endif // MOYA_COMMON_FILE_ENCODER_H
