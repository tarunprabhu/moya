#include "Store.h"
#include "AerynConfig.h"
#include "Config.h"
#include "Diagnostics.h"
#include "Environment.h"
#include "Formatting.h"
#include "Graph.h"
#include "JSONUtils.h"
#include "LLVMUtils.h"
#include "Metadata.h"
#include "StoreCell.h"
#include "StringUtils.h"
#include "Stringify.h"

#include <llvm/IR/InstIterator.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/TypeFinder.h>
#include <llvm/Support/raw_ostream.h>

#include <iomanip>
#include <regex>

using namespace llvm;

namespace moya {

static const Contents emptyContents;
static const Set<StructType*> emptyStructs;
static const Set<ArrayType*> emptyArrays;

Store::Store(const Module& module, const Environment& env)
    : module(module), env(env), layout(module),
      runtimeScalar(new ContentRuntimeScalar()),
      runtimePointer(new ContentRuntimePointer()),
      nullPointer(new ContentNullptr()), undefValue(new ContentUndef()),
      exceptionObject(nullptr),
      nextAvailableAddress(Store::null + layout.getPointerSize()) {
  // To avoid mis-analyses, we check the operands of any inttoptr instructions
  // in the code and set the first abstract store address to be greater
  // than any integer that appears as an operand to inttoptr.
  // LLVM > 3.6 uses inttoptr all over the place because they
  // treat all pointers as 64-bit integers and end up converting
  // everywhere. Another place this appears a lot is in C++ vtables when
  // dealing with virtual inheritance because in that case the offset of a
  // virtually-inherited data member is converted from int to pointer.
  for(const Function& f : module.functions())
    for(auto i = inst_begin(f); i != inst_end(f); i++)
      if(const auto* inttoptr = dyn_cast<IntToPtrInst>(&*i))
        if(const auto* cint = dyn_cast<ConstantInt>(inttoptr->getOperand(0)))
          nextAvailableAddress
              = std::max(nextAvailableAddress,
                         static_cast<RawAddress>(cint->getLimitedValue()));

  // We need to be careful that we have enough addresses available. This can
  // easily break if next is too large
  // FIXME: Ideally, we should use BigInt's or something of the sort for
  // store addresses
  if(nextAvailableAddress > std::numeric_limits<unsigned int>::max())
    moya_error("Starting store address is too large: " << nextAvailableAddress);

  // Allocate a string for exception messages
  const Address* exceptionMsg = allocate(
      Type::getInt8Ty(module.getContext()), CellFlags::Implicit, true);
  exceptionObject.reset(
      new ContentComposite({{exceptionMsg}, {getRuntimeScalar()}}));

  TypeFinder typeFinder;
  typeFinder.run(module, false);
  for(auto it = typeFinder.begin(); it != typeFinder.end(); it++)
    layout.reset(*it);
}

uint64_t Store::getPointerSize() const {
  return layout.getPointerSize();
}

uint64_t Store::getTypeSize(Type* type) const {
  return layout.getTypeSize(type);
}

uint64_t Store::getTypeAllocSize(Type* type) const {
  return layout.getTypeAllocSize(type);
}

uint64_t Store::getTypeAlignment(Type* type) const {
  return layout.getTypeAlignment(type);
}

int64_t Store::getOffsetInType(Type* type,
                               const Vector<const Value*>& indices) const {
  return layout.getOffsetInType(type, indices);
}

const StructLayout& Store::getStructLayout(StructType* sty) const {
  return layout.getStructLayout(sty);
}

bool Store::contains(RawAddress addr) const {
  return cells.contains(addr);
}

bool Store::contains(const Address* addr, int64_t offset) const {
  return cells.contains(addr->getRaw() + offset);
}

Vector<Store::RawAddress> Store::getCellAddresses() const {
  return cells.keys();
}

StoreCell& Store::getCell(RawAddress addr) {
  return *cells.at(addr).get();
}

const StoreCell& Store::getCell(RawAddress addr) const {
  return *cells.at(addr).get();
}

StoreCell& Store::getCell(const Address* addr, int64_t offset) {
  return getCell(addr->getRaw() + offset);
}

const StoreCell& Store::getCell(const Address* addr, int64_t offset) const {
  return getCell(addr->getRaw() + offset);
}

bool Store::hasAllocated(const Address* addr, int64_t offset) const {
  return allocated.contains(addr->getRaw() + offset);
}

Type* Store::getAllocated(const Address* addr, int64_t offset) const {
  return allocated.at(addr->getRaw() + offset);
}

const ContentRuntimeScalar* Store::getRuntimeScalar() const {
  return runtimeScalar.get();
}

const ContentRuntimePointer* Store::getRuntimePointer() const {
  return runtimePointer.get();
}

const ContentNullptr* Store::getNullptr() const {
  return nullPointer.get();
}

const ContentUndef* Store::getUndefValue() const {
  return undefValue.get();
}

const ContentComposite* Store::getExceptionObject() const {
  return exceptionObject.get();
}

// void Store::make(ArrayType* aty, Vector<Contents>& elements) {
//   Type* ety = aty->getElementType();
//   for(unsigned i = 0; i < aty->getNumElements(); i++)
//     if(LLVMUtils::isScalarTy(ety) or ety->LLVMUtils::isPointerTy())
//       elements.push_back(Contents());
//     else
//       elements.push_back({make(ety)});
// }

// void Store::make(StructType* sty, Vector<Contents>& fields) {
//   for(unsigned i = 0; i < sty->getNumElements(); i++) {
//     Type* ety = sty->getElementType(i);
//     if(LLVMUtils::isScalarTy(ety) or ety->LLVMUtils::isPointerTy())
//       fields.push_back(Contents());
//     else
//       fields.push_back({make(ety)});
//   }
// }

const Content* Store::make(Type* ty) {
  if(LLVMUtils::isScalarTy(ty))
    return getRuntimeScalar();
  else if(ty->isPointerTy())
    return getRuntimePointer();
  // else if(auto* aty = dyn_cast<ArrayType>(ty))
  //   make(aty, elements);
  // else if(auto* sty = dyn_cast<StructType>(ty))
  //   make(sty, elements);
  else
    moya_error("Unsupported type for composite object: " << *ty);

  return nullptr;
}

// To lower memory usage, we treat all scalars, nullptrs and runtime ptrs
// as single unknown values even if the values are known.
// Right now, we don't do any analysis that uses these constants or even
// look up the types of the constants, so they don't matter
const ContentNullptr* Store::make(const ConstantPointerNull*) {
  return getNullptr();
}

const ContentScalar* Store::make(const ConstantInt* c) {
  return makeObject<ContentScalar>(c, cints);
  // if(cints.contains(c))
  //   return cast<ContentScalar>(objs.at(cints.at(c)).get());
  // objs.emplace_back(new ContentScalar(c));
  // cints[c] = objs.size() - 1;
  // return cast<ContentScalar>(objs.back().get());
}

const ContentRuntimeScalar* Store::make(const ConstantFP*) {
  // ConstantInt's are used for vtable offsets and GEP address calculations,
  // so we still need to keep their values around in case. There doesn't seem
  // to be any need to keep precise values for constant floating point numbers
  // in the program because we don't do any arithmetic anyway
  return getRuntimeScalar();
}

const ContentCode* Store::make(const Function* f) {
  return makeObject<ContentCode>(f, cfuns);
  // if(cfuns.contains(f))
  //   return cast<ContentCode>(objs.at(cfuns.at(f)).get());
  // objs.emplace_back(new ContentCode(f));
  // cfuns[f] = objs.size() - 1;
  // return cast<ContentCode>(objs.back().get());
}

const Contents& Store::makeContents(const Vector<Contents>& elems) {
  const ContentComposite* ccomp = make(elems);
  if(not ccompContents.contains(ccomp))
    ccompContents[ccomp].insert(ccomp);
  return ccompContents.at(ccomp);
}

const ContentComposite* Store::make(const Vector<Contents>& elems) {
  if(ccomps.contains(elems))
    return cast<ContentComposite>(objs.at(ccomps.at(elems)).get());
  objs.emplace_back(new ContentComposite(elems));
  ccomps[elems] = objs.size() - 1;
  return cast<ContentComposite>(objs.back().get());
}

const ContentComposite* Store::make(const ConstantStruct* cstruct) {
  return makeCompositeObject(cstruct, cstructs);
  // if(cstructs.contains(cstruct))
  //   return cast<ContentComposite>(objs.at(cstructs.at(cstruct)).get());
  // Vector<Contents> elems;
  // for(const Use& u : cstruct->operands())
  //   elems.push_back(make(dyn_cast<Constant>(u.get())));
  // return make(elems);
}

const ContentComposite* Store::make(const ConstantArray* carray) {
  return makeCompositeObject(carray, carrays);
  // if(carrays.contains(carray))
  //   return cast<ContentComposite>(objs.at(carrays.at(carray)).get());
  // Vector<Contents> elems;
  // for(const Use& u : carray->operands())
  //   elems.push_back(make(dyn_cast<Constant>(u.get())));
  // return make(elems);
}

const ContentComposite* Store::make(const ConstantDataArray* cda) {
  return makeCompositeObject(cda, cdas);
  // if(cdas.contains(cda))
  //   return cast<ContentComposite>(objs.at(cdas.at(cda)).get());
  // Vector<Contents> elems;
  // for(unsigned i = 0; i < cda->getNumElements(); i++)
  //   elems.push_back(make(cda->getElementAsConstant(i)));
  // return make(elems);
}

const ContentUndef* Store::make(const UndefValue*) {
  return getUndefValue();
}

const Content* Store::make(const Constant* c) {
  if(const auto* cint = dyn_cast<ConstantInt>(c))
    return make(cint);
  else if(const auto* cfp = dyn_cast<ConstantFP>(c))
    return make(cfp);
  else if(const auto* carray = dyn_cast<ConstantArray>(c))
    return make(carray);
  else if(const auto* cda = dyn_cast<ConstantDataArray>(c))
    return make(cda);
  else if(const auto* cstruct = dyn_cast<ConstantStruct>(c))
    return make(cstruct);
  else if(const auto* cnull = dyn_cast<ConstantPointerNull>(c))
    return make(cnull);
  else if(const auto* cundef = dyn_cast<UndefValue>(c))
    return make(cundef);
  else if(const auto* f = dyn_cast<Function>(c))
    return make(f);
  else
    moya_error("Unsupported constant type to make: " << *c);
  return nullptr;
}

const Store::Address* Store::make(RawAddress addr) {
  if(caddrs.contains(addr))
    return cast<Address>(objs.at(caddrs.at(addr)).get());
  objs.emplace_back(new Address(addr));
  caddrs[addr] = objs.size() - 1;
  return cast<Address>(objs.back().get());
}

const Store::Address* Store::make(const Address* base, int64_t offset) {
  return make(base->getRaw() + offset);
}

const Store::Address* Store::make(const Address* base, int64_t offset) const {
  return cast<Address>(objs.at(caddrs.at(base->getRaw() + offset)).get());
}

bool Store::hasFlag(const Address* addr, CellFlags::Flag flag) const {
  if(contains(addr))
    if(getCell(addr).getFlags().test(flag))
      return true;
  return false;
}

// Once cells are allocated, they are also initialized. We are not interested
// in any uninitialized memory access analyses or the like, so this is ok
void Store::populate(PointerType* pty,
                     const CellFlags& flags,
                     bool,
                     const Instruction* inst,
                     const Function* f,
                     RawAddress base) {
  makeNewCell(base, pty, inst, f, flags);
}

void Store::populate(ArrayType* aty,
                     const CellFlags& flags,
                     bool trunc,
                     const Instruction* inst,
                     const Function* f,
                     RawAddress base) {
  ArrayType* flattened = LLVMUtils::getFlattened(aty);
  bool truncated = false;
  uint64_t nelems = flattened->getNumElements();

  // If trunc is true, it only means that arrays that should be truncated will
  // be truncated. But arrays that should not be truncated, won't be.
  // On the other hand if trunc is false, then nothing will be truncated
  if(trunc and AerynConfig::shouldTruncate(flattened)) {
    truncated = true;
    nelems = AerynConfig::getArrayLength(flattened);
  }
  Type* ety = flattened->getElementType();
  uint64_t size = getTypeAllocSize(ety);

  populate(ety, flags, trunc, inst, f, base);
  for(size_t i = 1, offset = size; i < nelems; i++, offset += size)
    populate(ety, flags, trunc, inst, f, base + offset);
  addArrayAt(base, ety, nelems, truncated);
  if(flags.test(CellFlags::Vtable)) {
    if(nelems > 2) {
      addArrayAt(base + getPointerSize(), ety, nelems - 1, truncated);
      addArrayAt(base + 2 * getPointerSize(), ety, nelems - 2, truncated);
    }
  }
}

void Store::addArrayAt(RawAddress addr,
                       Type* ety,
                       uint64_t nelems,
                       bool truncated) {
  ArrayType* aty = ArrayType::get(ety, nelems);
  arrays[addr].insert(aty);
  arraySpecs[addr][ety] = std::make_tuple(nelems, truncated);
}

void Store::addStructAt(RawAddress addr, StructType* sty) {
  structs[addr].insert(sty);
  if(Metadata::hasBases(sty, module)) {
    StructType* base = Metadata::getBases(sty, module).at(0);
    if(not structs.at(addr).contains(base))
      addStructAt(addr, base);
  }
}

void Store::populate(StructType* sty,
                     const CellFlags& flags,
                     bool trunc,
                     const Instruction* inst,
                     const Function* f,
                     RawAddress base) {
  // Add the structs and all equivalent types to the list of structs at
  // this location. This is here first because we want the first entry
  // in the structs{} map for any address to be the largest type at that
  // address
  addStructAt(base, sty);

  if(sty->getNumElements()) {
    const StructLayout& sl = getStructLayout(sty);
    for(unsigned i = 0; i < sty->getNumElements(); i++) {
      CellFlags cellFlags(flags);
      if(Metadata::hasLastVtableIndex(sty, module)
         and (i <= Metadata::getLastVtableIndex(sty, module)))
        cellFlags.set(CellFlags::Vtable);
      populate(sty->getElementType(i),
               cellFlags,
               trunc,
               inst,
               f,
               base + sl.getElementOffset(i));
    }
  } else {
    // If the struct has no types, we still want to create a cell for it
    // so that there is "something" in the store
    populate(Type::getInt8PtrTy(sty->getContext()),
             CellFlags::Dummy,
             true,
             inst,
             f,
             base);
  }
}

void Store::populate(Type* ty,
                     const CellFlags& flags,
                     bool trunc,
                     const Instruction* inst,
                     const Function* f,
                     RawAddress base) {
  if(LLVMUtils::isScalarTy(ty) or ty->isFunctionTy())
    makeNewCell(base, ty, inst, f, flags);
  else if(auto* pty = dyn_cast<PointerType>(ty))
    populate(pty, flags, trunc, inst, f, base);
  else if(auto* aty = dyn_cast<ArrayType>(ty))
    populate(aty, flags, trunc, inst, f, base);
  else if(auto* sty = dyn_cast<StructType>(ty))
    populate(sty, flags, trunc, inst, f, base);
  else
    moya_error("Cannot populate unknown type: " << *ty);
}

void Store::makeNewCell(RawAddress a,
                        Type* ty,
                        const Instruction* inst,
                        const Function*,
                        const CellFlags& flags) {
  // Create an address object for this cell so we can use it when dealing with
  // constant stores
  make(a);
  cells[a].reset(new StoreCell(a, ty, getTypeAllocSize(ty), flags, *this));
  cells.at(a)->addAllocator(inst);

  // Initialize the cell.
  if(flags.hasInitialized()) {
    AnalysisStatus changed = AnalysisStatus::Unchanged;
    StoreCell* cell = cells.at(a).get();
    if(LLVMUtils::isScalarTy(ty))
      cell->write(getDefaultScalar(ty), inst, changed);
    else if(LLVMUtils::isPointerTy(ty))
      // The default pointer will be a nullptr and we don't want need to
      // write this everywhere because it will all get filtered out during
      // the analysis anyway
      ;
    else
      moya_error("Unsupported type to initialize cell: "
                 << *ty << " at " << getDiagnostic(inst));
  }
}

const Store::Address* Store::reserve(Type* ty) {
  // Adjusts the next pointer so the next allocated object is aligned at the
  // right offset. This is done because for struct types we allocate each
  // element of the struct individually, but the entire struct should still
  // be allocated with the correct alignment
  RawAddress addr = nextAvailableAddress;
  unsigned alignment = getTypeAlignment(ty);
  uint64_t size = getTypeAllocSize(ty);
  if(addr % alignment)
    addr = (addr / alignment + 1) * alignment;

  nextAvailableAddress = addr + std::max(size, getPointerSize());
  return make(addr);
}

const Store::Address* Store::allocate(Type* ty,
                                      const CellFlags& flags,
                                      const Instruction* inst,
                                      const Function* f) {
  const Address* addr = reserve(ty);
  makeNewCell(addr->getRaw(), ty, inst, f, flags);
  return addr;
}

const Store::Address* Store::allocate(PointerType* pty,
                                      const CellFlags& flags,
                                      bool,
                                      const Instruction* inst,
                                      const Function* f) {
  return allocate(pty, flags, inst, f);
}

const Store::Address* Store::allocate(ArrayType* aty,
                                      const CellFlags& flags,
                                      bool trunc,
                                      const Instruction* inst,
                                      const Function* f) {
  const Address* addr = reserve(aty);
  populate(aty, flags, trunc, inst, f, addr->getRaw());
  return addr;
}

const Store::Address* Store::allocate(StructType* sty,
                                      const CellFlags& flags,
                                      bool trunc,
                                      const Instruction* inst,
                                      const Function* f) {
  const Address* addr = reserve(sty);
  populate(sty, flags, trunc, inst, f, addr->getRaw());
  return addr;
}

const Store::Address* Store::allocate(Type* ty,
                                      const CellFlags& flags,
                                      bool trunc,
                                      const Instruction* inst,
                                      const Function* f) {
  const Address* ret = nullptr;

  if(LLVMUtils::isScalarTy(ty) or ty->isFunctionTy() or ty->isMetadataTy())
    ret = allocate(ty, flags, inst, f);
  else if(auto* pty = dyn_cast<PointerType>(ty))
    ret = allocate(pty, flags, trunc, inst, f);
  else if(auto* aty = dyn_cast<ArrayType>(ty))
    ret = allocate(aty, flags, trunc, inst, f);
  else if(auto* sty = dyn_cast<StructType>(ty))
    ret = allocate(sty, flags, trunc, inst, f);
  else
    moya_error("Cannot allocate unsupported type: " << *ty);
  getCell(ret).addFlags(CellFlags::Start);
  allocated[ret->getRaw()] = ty;

  return ret;
}

AnalysisStatus
Store::del(const Address* addr, const Instruction* inst, const Function*) {
  if(contains(addr))
    return static_cast<AnalysisStatus>(getCell(addr).addDeallocator(inst));

  moya_error("Deleting non-existent cell: " << addr->str()
                                            << getDiagnostic(inst));
  return AnalysisStatus::Unchanged;
}

AnalysisStatus Store::deallocate(ArrayType* aty,
                                 const Address* addr,
                                 const Instruction* inst,
                                 const Function* f) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* ety = LLVMUtils::getFlattenedElementType(aty);
  uint64_t size = getTypeAllocSize(ety);
  uint64_t nelems = getArrayLength(addr, ety);

  for(uint64_t i = 0; i < nelems; i++)
    changed |= deallocate(ety, make(addr, i * size), inst, f);

  return changed;
}

AnalysisStatus Store::deallocate(StructType* sty,
                                 const Address* addr,
                                 const Instruction* inst,
                                 const Function* f) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(sty->getNumElements()) {
    const StructLayout& sl = getStructLayout(sty);
    for(unsigned i = 0; i < sty->getNumElements(); i++)
      changed |= deallocate(
          sty->getElementType(i), make(addr, sl.getElementOffset(i)), inst, f);
  } else {
    changed |= del(addr, inst, f);
  }

  return changed;
}

AnalysisStatus Store::deallocate(Type* ty,
                                 const Address* addr,
                                 const Instruction* inst,
                                 const Function* f) {
  if(ty->isIntegerTy() or ty->isFloatTy() or ty->isDoubleTy()
     or ty->isX86_FP80Ty() or ty->isPointerTy())
    // When a pointer is deallocated, it is the pointer variable itself that
    // is being deallocated, not what it is pointing to. If we want to
    // deallocate the actual buffer (or whatever the target of the pointer is),
    // that will have to be deallocated explicitly
    return del(addr, inst, f);
  else if(auto* aty = dyn_cast<ArrayType>(ty))
    return deallocate(aty, addr, inst, f);
  else if(auto* sty = dyn_cast<StructType>(ty))
    return deallocate(sty, addr, inst, f);
  else
    moya_error("Cannot deallocate unsupported type: " << *ty
                                                      << getDiagnostic(inst));

  return AnalysisStatus::Unchanged;
}

void Store::addReader(const Address* addr, const Instruction* inst) {
  getCell(addr).addReader(inst);
}

void Store::addWriter(const Address* addr, const Instruction* inst) {
  getCell(addr).addWriter(inst);
}

void Store::addAllocator(const Address* addr, const Instruction* inst) {
  getCell(addr).addAllocator(inst);
}

void Store::addDeallocator(const Address* addr, const Instruction* inst) {
  getCell(addr).addDeallocator(inst);
}

void Store::addReader(const Address* addr, const Function* f) {
  getCell(addr).addReader(f);
}

void Store::addWriter(const Address* addr, const Function* f) {
  getCell(addr).addWriter(f);
}

void Store::addAllocator(const Address* addr, const Function* f) {
  getCell(addr).addAllocator(f);
}

void Store::addDeallocator(const Address* addr, const Function* f) {
  getCell(addr).addDeallocator(f);
}

const Content* Store::getRuntimeValue(Type* type) {
  if(LLVMUtils::isScalarTy(type)) {
    return getRuntimeScalar();
  } else if(LLVMUtils::isPointerTy(type)) {
    return getRuntimePointer();
  } else if(auto* aty = dyn_cast<ArrayType>(type)) {
    Vector<Contents> elems(aty->getNumElements(),
                           getRuntimeValue(aty->getElementType()));
    return make(elems);
  } else if(auto* sty = dyn_cast<StructType>(type)) {
    Vector<Contents> fields;
    for(Type* elem : sty->elements())
      fields.push_back(getRuntimeValue(elem));
    return make(fields);
  } else {
    moya_error("Unsupported type for runtime value creation: " << *type);
  }
  return nullptr;
}

const Content* Store::getDefaultScalar(Type*) const {
  return getRuntimeScalar();
}

const Content* Store::getDefaultPointer() const {
  return getNullptr();
}

const Content* Store::maybeConvertToAddress(const Address* addr,
                                            const Content* c) {
  // In LLVM 3.9 pointers are largely treated as 64-bit integers which
  // can throw off the analysis which isexpecting pointers. So when we're
  // writing a 64-bit integer into a cell expecting a pointer, we probably
  // do want a pointer
  if(const auto* cscalar = dyn_cast<ContentScalar>(c))
    if(const auto* cint = dyn_cast<ConstantInt>(cscalar->getValue()))
      if(cint->getType()->isIntegerTy(64)
         and getCell(addr).getType()->isPointerTy())
        return make(cint->getLimitedValue());
  return c;
}

bool Store::empty(const Address* addr, const Instruction* inst) const {
  if(not contains(addr))
    moya_error("Check if non-existent cell is empty: "
               << addr->str() << " from " << getDiagnostic(inst));

  return getCell(addr).isEmpty();
}

// We have sanity checks to ensure that we don't read unexpected types
// from the store because we can't really handle these slightly type-unsafe
// accesses. This method should handle those special cases.
Store::AccessKind Store::check_legality_of_read(const Address* addr,
                                                Type* type,
                                                const Instruction* inst) const {
  if(not contains(addr)) {
    moya_error("Reading from non-existent cell: " << addr->str() << " from "
                                                  << getDiagnostic(inst));
    return AccessKind::NonExistentCell;
  } else if(auto* sty = dyn_cast<StructType>(type)) {
    if(not isTypeAt(addr, sty)) {
      // There are some special cases that we need to handle.

      if(LLVMUtils::isScalarTy(getCell(addr).getType())
         and LLVMUtils::isScalarStructTy(sty)) {
        // In general, if a struct consists exclusively of scalars, we can
        // populate it from an array of a single scalar because we are relaxed
        // about scalars and treat all sizes as being equivalent (this is a
        // consequence of some codes allocating arrays of n floats/doubles and
        // treating it as an array of n/2 complex numbers)
        //
        return AccessKind::StructFromScalar;
      } else {
        moya_error("Address " << addr->str()
                              << " does not contain struct: " << *sty << "\n"
                              << getCell(addr).str() << getDiagnostic(inst));
        return AccessKind::Invalid;
      }
    }
  } else if(auto* aty = dyn_cast<ArrayType>(type)) {
    if(not isTypeAt(addr, aty)) {
      moya_error("Cannot read array: " << *aty << " from " << addr->str() << " "
                                       << getDiagnostic(inst));
      return AccessKind::Invalid;
    }
  } else {
    // It is safe to read fewer bytes than the size of the cell
    const StoreCell& cell = getCell(addr);
    if((type->isSized() and cell.getType()->isSized())
       and (getTypeAllocSize(type) > getTypeAllocSize(cell.getType()))
       and not(LLVMUtils::isScalarTy(type)
               and LLVMUtils::isScalarTy(cell.getType()))) {
      moya_error("Read " << *type << " from cell of type " << *cell.getType()
                         << " @ " << addr->str() << " " << getDiagnostic(inst));
      return AccessKind::Invalid;
    }
  }
  return AccessKind::Valid;
}

const Contents& Store::read(const Address* addr) const {
  return getCell(addr).read();
}

const Contents& Store::read(const Address* addr, Type* type) {
  AnalysisStatus changed;
  return read(addr, type, nullptr, changed);
}

const Contents& Store::read(const Address* addr,
                            Type* type,
                            const Instruction* inst,
                            AnalysisStatus& changed) {
  if(type) {
    AccessKind accessKind = check_legality_of_read(addr, type, inst);
    if(auto* sty = dyn_cast<StructType>(type)) {
      Vector<Contents> elems;
      if(accessKind == StructFromScalar) {
        for(unsigned i = 0; i < sty->getNumElements(); i++)
          elems.push_back(getRuntimeScalar());
      } else {
        const StructLayout& sl = getStructLayout(sty);
        for(unsigned i = 0; i < sty->getNumElements(); i++) {
          const Address* next = make(addr, sl.getElementOffset(i));
          Type* ety = sty->getElementType(i);
          elems.push_back(read(next, ety, inst, changed));
        }
      }
      return makeContents(elems);
    } else if(auto* aty = dyn_cast<ArrayType>(type)) {
      Type* ety = aty->getElementType();
      uint64_t size = getTypeAllocSize(ety);
      Vector<Contents> elems;
      for(unsigned i = 0; i < getArrayLength(addr, ety, 1); i++) {
        const Address* next = make(addr, i * size);
        elems.push_back(read(next, ety, inst, changed));
      }
      return makeContents(elems);
    }
  }

  if(contains(addr))
    return getCell(addr).read(inst, changed);

  changed |= AnalysisStatus::Top;
  moya_error("Reading non-existent cell: " << addr->str() << " [" << *type
                                           << "] from "
                                           << LLVMUtils::getFunctionName(inst));

  return emptyContents;
}

// We have sanity checks to ensure that we don't write unexpected types
// from the store because we can't really handle these slightly type-unsafe
// accesses. This method should handle those special cases.
Store::AccessKind
Store::check_legality_of_write(const Address* addr,
                               Type* type,
                               const Instruction* inst) const {
  if(not contains(addr)) {
    moya_error("Writing to non-existent cell:\n"
               << "  addr: " << addr->str() << "\n"
               << "  inst: " << getDiagnostic(inst) << "\n\n"
               << str());
    return AccessKind::NonExistentCell;
  } else if(auto* sty = dyn_cast<StructType>(type)) {
    if(not isTypeAt(addr, sty)) {
      // There is a special case that we need to handle.
      // A common pattern seems to be to allocate an array of n floats/doubles
      // and treat it as an array of n/2 complex numbers.
      // In general, if a struct consists exclusively of scalars, we can
      // populate it from an array of a single scalar because we are relaxed
      // about scalars and treat all sizes as being equivalent.
      if(LLVMUtils::isScalarTy(getCell(addr).getType())
         and LLVMUtils::isScalarStructTy(sty)) {
        return AccessKind::StructFromScalar;
      } else {
        moya_error("Struct not found at address\n"
                   << "  struct: " << *sty << "\n"
                   << "    addr: " << addr->str() << "\n"
                   << "    cell: " << getCell(addr).str() << "\n"
                   << "    inst: " << getDiagnostic(inst) << "\n\n"
                   << str());
        return AccessKind::Invalid;
      }
    }
  } else if(auto* aty = dyn_cast<ArrayType>(type)) {
    if(not isTypeAt(addr, aty))
      moya_error("Cannot write array " << *type << " to " << addr->str()
                                       << getDiagnostic(inst));
  } else {
    // It is safe to write fewer bytes than the size of the cell. But this
    // should only be allowed for scalar types. This is because there seems to
    // explicit code that packs 32-bit ints into a 64-bit int
    const StoreCell& cell = getCell(addr);
    if((getTypeAllocSize(type) > getTypeAllocSize(cell.getType()))
       and not(LLVMUtils::isScalarTy(type)
               and LLVMUtils::isScalarTy(cell.getType()))) {
      moya_error("Writing " << *type << " to " << cell.str() << " at "
                            << getDiagnostic(inst));
      return AccessKind::Invalid;
    }
  }
  return AccessKind::Valid;
}

void Store::write(const Address* base,
                  const Content* c,
                  Type* type,
                  const Instruction* inst,
                  AnalysisStatus& changed) {
  if(isTop(changed))
    return;

  if(type) {
    AccessKind accessKind = check_legality_of_write(base, type, inst);
    if(auto* sty = dyn_cast<StructType>(type)) {
      if(accessKind == AccessKind::StructFromScalar) {
        write(base, getRuntimeScalar(), nullptr, inst, changed);
      } else {
        auto* ccomp = dyn_cast<ContentComposite>(c);
        const StructLayout& sl = getStructLayout(sty);
        for(unsigned i = 0; i < ccomp->size(); i++) {
          const Address* addr = make(base, sl.getElementOffset(i));
          write(addr, ccomp->at(i), sty->getElementType(i), inst, changed);
        }
      }
    } else if(auto* aty = dyn_cast<ArrayType>(type)) {
      // check_legality_of_write() should have failed if it were not
      // possible to write an array to this address
      auto* ccomp = dyn_cast<ContentComposite>(c);
      unsigned long size = getTypeAllocSize(aty->getElementType());
      for(unsigned i = 0; i < ccomp->size(); i++) {
        const Address* addr = make(base, i * size);
        write(addr, ccomp->at(i), aty->getElementType(), inst, changed);
      }
    } else {
      if(not contains(base)) {
        moya_error("Writing to non-existent cell: " << base->str() << " ["
                                                    << *type << "]\n"
                                                    << c->str());
        changed |= AnalysisStatus::Top;
      } else {
        getCell(base).write(maybeConvertToAddress(base, c), inst, changed);
      }
    }
  } else {
    if(not contains(base))
      moya_error("Writing to non-existent cell: " << base->str() << "\n"
                                                  << c->str());
    else {
      StoreCell& cell = getCell(base);
      Type* cellTy = cell.getType();
      if(cellTy->isPointerTy()) {
        if(not(isa<ContentAddress>(c) or isa<ContentRuntimePointer>(c)
               or isa<ContentNullptr>(c)))
          moya_error("Writing non-equivalent types\n"
                     << "  cell: " << *cell.getType() << " @ " << base->str()
                     << "\n"
                     << "  data: " << c->str() << getDiagnostic(inst));
      } else if(LLVMUtils::isScalarTy(cellTy)) {
        if(not(isa<ContentScalar>(c) or isa<ContentRuntimeScalar>(c)
               or isa<ContentCode>(c) or isa<ContentUndef>(c)))
          moya_error("Writing non-equivalent types\n"
                     << "  cell: " << *cellTy << " @ " << base->str() << "\n"
                     << "  data: " << c->str() << getDiagnostic(inst));
      } else if(isa<ContentComposite>(c)) {
        moya_error("Writing composite type without knowing type");
      }
      cell.write(maybeConvertToAddress(base, c), inst, changed);
    }
  }
}

void Store::write(const Address* addr,
                  const Contents& contents,
                  Type* type,
                  const Instruction* inst,
                  AnalysisStatus& changed) {
  // Even if contents is empty, we want to mark the cell as having been
  // written
  if(contains(addr))
    changed |= getCell(addr).addWriter(inst);

  for(const Content* c : contents)
    write(addr, c, type, inst, changed);
}

void Store::write(const Address* addr, const Content* c, Type* type) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  write(addr, c, type, nullptr, changed);
}

void Store::write(const Address* addr, const Contents& contents, Type* type) {
  // Even if contents is empty, we want to mark the cell as having been written
  for(const Content* c : contents)
    write(addr, c, type);
}

uint64_t Store::copy(const Address* dst,
                     const Address* src,
                     Type* type,
                     const Instruction* inst,
                     AnalysisStatus& changed) {
  // FIXME: Deal with the scalar struct madness here
  if(check_legality_of_write(dst, type, inst) == AccessKind::Invalid)
    moya_error("Cannot copy type to cell (" << dst->getRaw() << ") " << *type
                                            << ": " << getDiagnostic(inst));

  if(check_legality_of_read(src, type, inst) == AccessKind::Invalid)
    moya_error("Cannot copy type from cell (" << src->getRaw() << ") " << *type
                                              << ": " << getDiagnostic(inst));

  uint64_t bytes = 0;
  if(LLVMUtils::isScalarTy(type) or LLVMUtils::isPointerTy(type)) {
    StoreCell& dstCell = getCell(dst);
    StoreCell& srcCell = getCell(src);
    for(const Content* c : srcCell.read(inst, changed))
      dstCell.write(c, inst, changed);
    if(not srcCell.isEmpty())
      bytes += srcCell.getSize();
  } else if(auto* aty = dyn_cast<ArrayType>(type)) {
    Type* ety = aty->getElementType();
    uint64_t nelems = getArrayLength(src, ety);
    unsigned long offset = getTypeAllocSize(aty->getElementType());
    for(uint64_t i = 0; i < nelems; i++)
      bytes += copy(
          make(dst, i * offset), make(src, i * offset), ety, inst, changed);
  } else if(auto* sty = dyn_cast<StructType>(type)) {
    const StructLayout& sl = getStructLayout(sty);
    for(unsigned i = 0; i < sty->getNumElements(); i++) {
      Type* field = sty->getElementType(i);
      unsigned long offset = sl.getElementOffset(i);
      bytes += copy(make(dst, offset), make(src, offset), field, inst, changed);
    }
  } else {
    moya_error("Unsupported type to copy: " << *type << " at "
                                            << getDiagnostic(inst));
  }

  return bytes;
}

AnalysisStatus
Store::look(const Address* addr, Type* type, const Instruction* inst) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  if(auto* sty = dyn_cast<StructType>(type)) {
    const StructLayout& sl = getStructLayout(sty);
    for(unsigned i = 0; i < sty->getNumElements(); i++) {
      Type* field = sty->getElementType(i);
      unsigned long offset = sl.getElementOffset(i);
      if(field->isArrayTy() or field->isStructTy())
        changed |= look(make(addr, offset), field, inst);
      else
        changed |= getCell(addr, offset).addReader(inst);
    }
  } else if(auto* aty = dyn_cast<ArrayType>(type)) {
    Type* ety = aty->getElementType();
    unsigned long nelems = getArrayLength(addr, ety, 1);
    unsigned long offset = getTypeAllocSize(ety);
    if(ety->isStructTy() or ety->isArrayTy())
      for(unsigned i = 0; i < nelems; i++)
        changed |= look(make(addr, i * offset), ety, inst);
    else
      for(unsigned i = 0; i < nelems; i++)
        changed |= getCell(addr, i * offset).addReader(inst);
  } else {
    changed |= getCell(addr).addReader(inst);
  }
  return changed;
}

AnalysisStatus
Store::touch(const Address* addr, Type* type, const Instruction* inst) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  if(auto* sty = dyn_cast<StructType>(type)) {
    const StructLayout& sl = getStructLayout(sty);
    for(unsigned i = 0; i < sty->getNumElements(); i++) {
      Type* field = sty->getElementType(i);
      unsigned long offset = sl.getElementOffset(i);
      if(field->isArrayTy() or field->isStructTy())
        changed |= touch(make(addr, offset), field, inst);
      else
        changed |= getCell(addr, offset).addWriter(inst);
    }
  } else if(auto* aty = dyn_cast<ArrayType>(type)) {
    Type* ety = aty->getElementType();
    unsigned long nelems = getArrayLength(addr, ety, 1);
    unsigned long offset = getTypeAllocSize(ety);
    if(ety->isStructTy() or ety->isArrayTy())
      for(unsigned i = 0; i < nelems; i++)
        changed |= touch(make(addr, i * offset), ety, inst);
    else
      for(unsigned i = 0; i < nelems; i++)
        changed |= getCell(addr, i * offset).addWriter(inst);
  } else {
    changed |= getCell(addr).addWriter(inst);
  }
  return changed;
}

uint64_t
Store::getArrayLength(const Address* addr, Type* ty, int64_t dfault) const {
  if(not arrays.contains(addr->getRaw())) {
    if(dfault == -1)
      moya_error("Could not find array at " << addr->str() << "\n\n" << str());
    else
      return dfault;
  }

  if(arraySpecs.at(addr->getRaw()).contains(ty))
    return std::get<uint64_t>(arraySpecs.at(addr->getRaw()).at(ty));

  if(dfault == -1)
    moya_error("Could not find array of " << *ty << " at " << addr->str()
                                          << "\n\n"
                                          << str());
  else
    return dfault;
}

Vector<Type*> Store::getTypesAt(const Address* addr) const {
  Vector<Type*> ret;

  if(contains(addr)) {
    ret.push_back(getCell(addr).getType());

    for(StructType* sty : getStructsAt(addr))
      ret.push_back(sty);

    for(ArrayType* aty : getArraysAt(addr))
      ret.push_back(aty);
  }

  return ret;
}

const Set<ArrayType*>& Store::getArraysAt(const Address* addr) const {
  if(arrays.contains(addr->getRaw()))
    return arrays.at(addr->getRaw());

  return emptyArrays;
}

StructType* Store::getLargestStructAt(const Address* addr) const {
  StructType* largest = nullptr;
  uint64_t size = 0;

  if(structs.contains(addr->getRaw())) {
    for(StructType* sty : structs.at(addr->getRaw())) {
      uint64_t sz = getTypeAllocSize(sty);
      if(sz > size) {
        largest = sty;
        size = sz;
      }
    }
  }

  return largest;
}

const Set<StructType*>& Store::getStructsAt(const Address* addr) const {
  if(structs.contains(addr->getRaw()))
    return structs.at(addr->getRaw());

  return emptyStructs;
}

bool Store::isStructAt(RawAddress addr, StructType* sty) const {
  if(structs.contains(addr) and structs.at(addr).contains(sty))
    return true;

  const StructLayout& sl = getStructLayout(sty);
  for(unsigned i = 0; i < sl.getNumFlattenedElements(); i++)
    if(not isTypeAt(addr + sl.getFlattenedElementOffset(i),
                    sl.getFlattenedElementType(i)))
      return false;

  return true;
}

bool Store::isStructAt(const Address* addr, StructType* sty) const {
  return isStructAt(addr->getRaw(), sty);
}

bool Store::isArrayAt(RawAddress addr, Type* ty) const {
  // Even a single cell is an array of length 1
  if(contains(addr) and getCell(addr).getType() == ty)
    return true;

  if(not arrays.contains(addr))
    return false;

  // All arrays will be flattened before allocation, so we should only have
  // primitive, pointer or struct types stored in the value section of the
  // arrays{} map. This function should also *ONLY* be called with the
  // element type of a flattened array
  if(arraySpecs.at(addr).contains(ty))
    return true;

  return false;
}

bool Store::isArrayAt(const Address* addr, Type* ty) const {
  return isArrayAt(addr->getRaw(), ty);
}

bool Store::isTypeAt(RawAddress addr, Type* ty) const {
  Type* pi8 = Type::getInt8PtrTy(ty->getContext());
  Type* i8 = Type::getInt8Ty(ty->getContext());

  // First do some quick checks to handle the easy cases
  if(not contains(addr))
    return false;

  const StoreCell& cell = getCell(addr);
  Type* cellTy = cell.getType();
  if(cellTy == ty)
    return true;

  // If that didn't work, check for almost identical types
  if(LLVMUtils::isScalarTy(ty)) {
    if(LLVMUtils::isScalarTy(cellTy))
      return true;
    else
      return false;
  } else if(ty->isPointerTy()) {
    if(ty == pi8 and cellTy->isPointerTy())
      return true;
    else if(cellTy == pi8)
      return true;
  } else if(auto* aty = dyn_cast<ArrayType>(ty)) {
    if(isArrayAt(addr, LLVMUtils::getFlattenedElementType(aty)))
      return true;
  } else if(auto* sty = dyn_cast<StructType>(ty)) {
    if(isStructAt(addr, sty))
      return true;
  } else if(ty->isFunctionTy()) {
    if(cellTy->isFunctionTy())
      return true;
  }

  // We couldn't match the types exactly.
  // Now check for inexact matches which implies that we check if the layouts
  // of the types sort of match
  uint64_t tySz = getTypeAllocSize(ty);
  if(LLVMUtils::isScalarTy(ty)) {
    if(getTypeAllocSize(cellTy) == tySz)
      return true;
  } else if(ty->isPointerTy()) {
    // We should do something more intelligent here like check for type
    // equivalence between the pointees of the current type and the type of
    // the pointer
    if(cellTy->isPointerTy())
      return true;
  } else if(auto* aty = dyn_cast<ArrayType>(ty)) {
    Type* ety = aty->getElementType();
    if(ety == i8)
      return true;
  } else if(auto* sty = dyn_cast<StructType>(ty)) {
    if(not sty->isOpaque()) {
      const StructLayout& sl = getStructLayout(sty);
      for(unsigned i = 0; i < sty->getNumElements(); i++)
        if(not isTypeAt(addr + sl.getElementOffset(i), sty->getElementType(i)))
          return false;
    }
    return true;
  }

  return false;
}

bool Store::isTypeAt(const Address* addr, Type* ty) const {
  return isTypeAt(addr->getRaw(), ty);
}

std::string Store::strStructs(int depth) const {
  std::string buf;
  raw_string_ostream ss(buf);
  Vector<RawAddress> addrs;

  for(const auto& it : structs)
    addrs.push_back(it.first);
  llvm::sort(addrs.begin(), addrs.end());

  ss << space(depth) << "structs {\n";
  for(RawAddress addr : addrs)
    for(StructType* sty : structs.at(addr)) {
      ss << space1(depth) << addr << ": ";
      if(sty->hasName())
        ss << sty->getName();
      else
        ss << *sty;
      ss << "\n";
    }
  ss << space(depth) << "}";

  return ss.str();
}

std::string Store::strArrays(int depth) const {
  std::string buf;
  raw_string_ostream ss(buf);
  Vector<RawAddress> addrs;

  for(const auto& it : arraySpecs)
    addrs.push_back(it.first);
  llvm::sort(addrs.begin(), addrs.end());

  ss << space(depth) << "arrays {\n";
  for(RawAddress addr : addrs) {
    for(const auto& jt : arraySpecs.at(addr)) {
      ss << space1(depth) << addr << ":\n";
      ss << space2(depth) << *jt.first << ": " << std::get<uint64_t>(jt.second)
         << " " << (std::get<bool>(jt.second) ? "[Truncated]" : "[Full]")
         << "\n";
    }
  }

  return ss.str();
}

std::string Store::strAllocated(int depth) const {
  std::string buf;
  raw_string_ostream ss(buf);
  Vector<RawAddress> addrs;

  for(const auto& it : allocated)
    addrs.push_back(it.first);
  llvm::sort(addrs.begin(), addrs.end());

  ss << space(depth) << "allocated {\n";
  for(RawAddress addr : addrs)
    if(auto* sty = dyn_cast<StructType>(allocated.at(addr))) {
      ss << space1(depth) << addr << ": ";
      if(sty->hasName())
        ss << sty->getName();
      else
        ss << *sty;
      unsigned long size = getTypeAllocSize(sty);
      ss << " [" << size << ", " << addr + size << "]\n";
    }
  ss << space(depth) << "}";

  return ss.str();
}

std::string Store::str() const {
  std::string buf;
  raw_string_ostream ss(buf);
  Vector<RawAddress> addrs;
  std::string tab1 = space1();
  std::string tab2 = space2();

  for(const auto& it : cells)
    addrs.push_back(it.first);
  llvm::sort(addrs.begin(), addrs.end());

  ss << "store {\n";
  ss << tab1 << "cells {\n";
  for(RawAddress addr : addrs)
    ss << tab2 << addr << ":\n" << cells.at(addr)->str(indent2()) << "\n\n";
  ss << tab1 << "}\n";
  ss << strAllocated(indent1()) << "\n";
  ss << strStructs(indent1()) << "\n";
  ss << strArrays(indent1()) << "\n";
  ss << "}";

  return ss.str();
}

void Store::serialize(Serializer& s) const {
  moya_message("Serialize store");

  env.serializeFiles(s);
  serializeTypes(s);
  env.serializeFunctions(s);
  env.serializeGlobals(s);
  serializeCells(s);
}

static void collectTypes(llvm::Type* type, Set<llvm::Type*>& types) {
  if(type->isLabelTy())
    return;

  if(types.insert(type)) {
    if(LLVMUtils::isScalarTy(type) or LLVMUtils::isVoidTy(type)
       or type->isMetadataTy()) {
      ;
    } else if(auto* pty = dyn_cast<llvm::PointerType>(type)) {
      collectTypes(pty->getElementType(), types);
    } else if(auto* aty = dyn_cast<llvm::ArrayType>(type)) {
      collectTypes(aty->getElementType(), types);
    } else if(auto* sty = dyn_cast<llvm::StructType>(type)) {
      for(llvm::Type* field : sty->elements())
        collectTypes(field, types);
    } else if(auto* fty = dyn_cast<llvm::FunctionType>(type)) {
      collectTypes(fty->getReturnType(), types);
      for(llvm::Type* param : fty->params())
        collectTypes(param, types);
    } else {
      moya_error("Unknown type to serialize: " << *type);
    }
  }
}

void Store::serialize(llvm::PointerType* pty, Serializer& s) const {
  s.add("_class", "PointerType");
  s.add("id", moya::str(pty));
  s.add("llvm", JSON::escape(moya::str(*cast<llvm::Type>(pty))));
  s.add("element", JSON::pointer(pty->getElementType()));
}

void Store::serialize(llvm::ArrayType* aty, Serializer& s) const {
  s.add("_class", "ArrayType");
  s.add("id", moya::str(aty));
  s.add("llvm", JSON::escape(moya::str(*cast<llvm::Type>(aty))));
  s.add("length", aty->getNumElements());
  s.add("element", JSON::pointer(aty->getElementType()));
}

void Store::serialize(llvm::StructType* sty, Serializer& s) const {
  if(Metadata::hasClass(sty, module))
    s.add("_class", "ClassType");
  else if(Metadata::hasUnion(sty, module))
    s.add("_class", "UnionType");
  else
    s.add("_class", "StructType");
  s.add("id", moya::str(sty));
  s.add("llvm", JSON::escape(moya::str(*sty)));

  if(Metadata::hasSourceName(sty, module))
    s.add("source", Metadata::getSourceName(sty, module));

  // if(Metadata::hasSourceLocation(sty, module))
  //   s.add("location", Metadata::getSourceLocation(sty, module));

  s.arrStart("flags");
  if(Metadata::hasArtificial(sty, module))
    s.serialize("Artificial");
  s.arrEnd(); // flags

  const llvm::StructLayout& sl = *module.getDataLayout().getStructLayout(sty);
  Vector<std::string> fields;
  if(Metadata::hasFieldNames(sty, module))
    fields = Metadata::getFieldNames(sty, module);
  s.arrStart("fields");
  for(unsigned i = 0; i < sty->getNumElements(); i++) {
    llvm::Type* ty = sty->getElementType(i);
    s.mapStart();
    if(fields.size() > i)
      s.add("name", fields.at(i));
    s.add("type", JSON::pointer(ty));
    s.add("offset", sl.getElementOffset(i));
    s.mapEnd();
  }
  s.arrEnd();
}

void Store::serialize(llvm::FunctionType* fty, Serializer& s) const {
  s.add("_class", "FunctionType");
  s.add("id", moya::str(fty));
  s.add("llvm", JSON::escape(moya::str(*fty)));

  s.add("return", JSON::pointer(fty->getReturnType()));
  s.arrStart("params");
  for(llvm::Type* param : fty->params())
    s.serialize(JSON::pointer(param));
  s.arrEnd();
}

void Store::serializeTypes(Serializer& s) const {
  moya_message("Serialize types");

  moya::Set<Type*> types;
  for(const auto& it : cells)
    collectTypes(it.second->getType(), types);
  for(const auto& it : structs)
    for(llvm::StructType* sty : it.second)
      collectTypes(sty, types);
  for(const auto& it : arrays)
    for(llvm::ArrayType* aty : it.second)
      collectTypes(aty, types);
  for(const auto& it : allocated)
    collectTypes(it.second, types);

  s.mapStart("types");
  for(Type* type : types) {
    s.mapStart(moya::str(type));
    if(type->isIntegerTy() or type->isHalfTy()) {
      s.add("_class", "IntegerType");
      s.add("id", moya::str(type));
      s.add("llvm", moya::str(*type));
    } else if(type->isFloatTy() or type->isDoubleTy() or type->isX86_FP80Ty()) {
      s.add("_class", "FloatType");
      s.add("id", moya::str(type));
      s.add("llvm", moya::str(*type));
    } else if(type->isVoidTy()) {
      s.add("_class", "VoidType");
      s.add("id", moya::str(type));
      s.add("llvm", moya::str(*type));
    } else if(type->isMetadataTy()) {
      s.add("class", "MetadataType");
      s.add("id", moya::str(type));
      s.add("llvm", moya::str(*type));
    } else if(auto* pty = dyn_cast<llvm::PointerType>(type)) {
      serialize(pty, s);
    } else if(auto* aty = dyn_cast<llvm::ArrayType>(type)) {
      serialize(aty, s);
    } else if(auto* sty = dyn_cast<llvm::StructType>(type)) {
      serialize(sty, s);
    } else if(auto* fty = dyn_cast<llvm::FunctionType>(type)) {
      serialize(fty, s);
    } else {
      moya_error("Unsupported type: " << *type);
    }
    if(type->isSized())
      s.add("size", layout.getTypeStoreSize(type));
    s.mapEnd();
  }
  s.mapEnd();
}

void Store::serializeCells(Serializer& s) const {
  moya_message("Serialize cells");

  Vector<RawAddress> addrs;
  for(const auto& it : cells)
    addrs.push_back(it.first);
  llvm::sort(addrs.begin(), addrs.end());

  s.mapStart("store");
  s.add("_class", "Store");
  for(RawAddress addr : addrs) {
    const StoreCell& cell = *cells.at(addr);
    const Address* caddr = getObjectAs<Address>(caddrs.at(addr));
    if(env.isFunction(caddr)) {
      const llvm::Function* f = env.lookupFunction(caddr);
      if((not moya::Metadata::hasNoAnalyze(f)) and env.hasCallSites(f)
         and f->size())
        s.add(moya::str(addr), cell);
    } else if(env.isArg(caddr)) {
      const llvm::Function* f = LLVMUtils::getFunction(env.lookupArg(caddr));
      if((not moya::Metadata::hasNoAnalyze(f)) and env.hasCallSites(f)
         and f->size())
        s.add(moya::str(addr), cell);
    } else {
      s.add(moya::str(addr), cell);
    }
  }
  s.mapEnd("store");
}

} // namespace moya
