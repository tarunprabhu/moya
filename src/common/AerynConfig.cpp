#include "AerynConfig.h"
#include "LLVMUtils.h"
#include "Metadata.h"

using namespace llvm;

namespace moya {

const std::string AerynConfig::pureFunctionModel = "moya.pure.function";

unsigned AerynConfig::getMaxJITRegions() {
  return max_regions;
}

unsigned AerynConfig::getMaxIterations() {
  return max_iterations;
}

uint64_t AerynConfig::getArrayLength() {
  return analysis_array_length;
}

uint64_t AerynConfig::getArrayLength(ArrayType* aty) {
  if(shouldTruncate(aty))
    return getArrayLength();
  return std::max(aty->getNumElements(), getArrayLength());
}

bool AerynConfig::shouldTruncate(GlobalVariable& g) {
  // Explicitly don't truncate vtables. This would get caught in the
  // test for arrays anyway, but that might change so it doesn't hurt to
  // keep an explicit check for it here
  if(moya::Metadata::hasCXXVtable(g))
    return false;
  return shouldTruncate(
      LLVMUtils::getAnalysisTypeAs<PointerType>(g)->getElementType());
}

bool AerynConfig::shouldTruncate(Type* ty) {
  if(auto* aty = dyn_cast<ArrayType>(ty)) {
    Type* ety = aty->getElementType();
    if(ety->isPointerTy() or ety->isFunctionTy())
      return false;
    return true;
  }
  return false;
}

const std::string& AerynConfig::getPureFunctionModel() {
  return pureFunctionModel;
}

} // namespace moya
