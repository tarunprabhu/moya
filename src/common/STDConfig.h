#ifndef MOYA_COMMON_STD_CONFIG_H
#define MOYA_COMMON_STD_CONFIG_H

#include <string>

namespace moya {

enum STDKind {
  STD_None,

  // STL Containers
  STD_STL_container_first,
  STD_STL_pair,
  STD_STL_array,
  STD_STL_vector,
  STD_STL_deque,
  STD_STL_forward_list,
  STD_STL_list,
  STD_STL_set,
  STD_STL_map,
  STD_STL_multiset,
  STD_STL_multimap,
  STD_STL_unordered_set,
  STD_STL_unordered_map,
  STD_STL_unordered_multiset,
  STD_STL_unordered_multimap,
  STD_STL_stack,
  STD_STL_queue,
  STD_STL_priority_queue,
  STD_STL_bitvector,
  STD_STL_container_last,

  // STL Iterators
  STD_STL_iterator_first,
  STD_STL_iterator_normal,
  STD_STL_iterator_reverse,
  STD_STL_iterator_deque,
  STD_STL_iterator_forward_list,
  STD_STL_iterator_list,
  STD_STL_iterator_tree,
  STD_STL_iterator_hashtable,
  STD_STL_iterator_bucket,
  STD_STL_iterator_bitvector,
  STD_STL_const_iterator_forward_list,
  STD_STL_const_iterator_list,
  STD_STL_const_iterator_tree,
  STD_STL_const_iterator_hashtable,
  STD_STL_const_iterator_bitvector,
  STD_STL_iterator_last,

  // Stream classes
  STD_Stream_istream_first,
  STD_Stream_basic_istream,
  STD_Stream_basic_istringstream,
  STD_Stream_ifstream,
  STD_Stream_istream_last,

  STD_Stream_ostream_first,
  STD_Stream_basic_ostream,
  STD_Stream_basic_ostringstream,
  STD_Stream_ofstream,
  STD_Stream_ostream_last,

  // C++ string
  STD_String,

  // STD implementation classes that are needed

  // std::allocator<T>
  STD_Allocator,

  // std::initializer_list<T>
  STD_Initializer,

  // Other functions and classes in the std namespace which are visible
  // to the user
  STD_Public,

  // Functions and classes in the std namespace that are *NOT* visible to the
  // user
  STD_Impl,
};

namespace STDConfig {

bool isSTLContainer(STDKind);
bool isSTLIterator(STDKind);
bool isSTDAllocator(STDKind);
bool isSTDInitializer(STDKind);
bool isSTDString(STDKind);
bool isSTDStream(STDKind);
bool isSTDIStream(STDKind);
bool isSTDOStream(STDKind);
bool isSTDPublic(STDKind);
bool isSTDImpl(STDKind);

bool isVectorContainer(STDKind);
bool isBitVectorContainer(STDKind);
bool isTreeContainer(STDKind);
bool isHashtableContainer(STDKind);
bool isDequeContainer(STDKind);
bool isForwardListContainer(STDKind);
bool isListContainer(STDKind);

bool isArrayLikeContainer(STDKind);
bool isListLikeContainer(STDKind);
bool isQueueLikeContainer(STDKind);
bool isSetLikeContainer(STDKind);
bool isMapLikeContainer(STDKind);

STDKind getSTDKind(const std::string&);
std::string getSTDName(STDKind);

bool isSTDModel(const std::string&);
std::string getSTDModel(const std::string&);

} // namespace STDConfig

} // namespace moya

#endif // MOYA_COMMON_STD_CONFIG_H
