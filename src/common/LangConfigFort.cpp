#include "LangConfigFort.h"
#include "Set.h"
#include "StringUtils.h"

static const moya::Set<std::string> exts = {"FPP",
                                            "F",
                                            "FOR",
                                            "FTN",
                                            "F90",
                                            "F95",
                                            "F03",
                                            "F08",
                                            "fpp",
                                            "f",
                                            "for",
                                            "ftn",
                                            "f90",
                                            "f95",
                                            "f03",
                                            "f08"};

bool LangConfigFort::isValidSourceFile(const std::string& file) {
  return exts.contains(moya::StringUtils::rpartition(file, ".").second);
}

unsigned LangConfigFort::getMaxDescriptorDimensions() {
  return 7;
}
