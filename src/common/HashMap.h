#ifndef MOYA_COMMON_HASHMAP_H
#define MOYA_COMMON_HASHMAP_H

#include <sstream>
#include <string>
#include <unordered_map>

#include "Deserializer.h"
#include "Serializer.h"
#include "Stringify.h"
#include "Vector.h"

namespace moya {

template <typename K, typename V>
class HashMap {
protected:
  std::unordered_map<K, V> _impl;

public:
  using key_type = typename std::unordered_map<K, V>::key_type;
  using mapped_type = typename std::unordered_map<K, V>::mapped_type;
  using value_type = typename std::unordered_map<K, V>::value_type;
  using size_type = typename std::unordered_map<K, V>::size_type;
  using iterator = typename std::unordered_map<K, V>::iterator;
  using const_iterator = typename std::unordered_map<K, V>::const_iterator;

protected:
  std::unordered_map<K, V>& getImpl() {
    return _impl;
  }

  const std::unordered_map<K, V>& getImpl() const {
    return _impl;
  }

public:
  HashMap() {
    ;
  }

  template <typename InputIt>
  HashMap(InputIt first, InputIt last) : _impl(first, last) {
    ;
  }

  HashMap(const HashMap<K, V>& other) : _impl(other.getImpl()) {
    ;
  }

  HashMap(HashMap&& other) : _impl(other.getImpl()) {
    ;
  }

  HashMap(std::initializer_list<value_type> init) : _impl(init) {
    ;
  }

  virtual ~HashMap() = default;

  HashMap<K, V>& operator=(const HashMap<K, V>& other) {
    _impl = other.getImpl();
    return *this;
  }

  HashMap<K, V>& operator=(HashMap<K, V>&& other) {
    _impl = other.getImpl();
    return *this;
  }

  HashMap<K, V>& operator=(std::initializer_list<value_type> init) {
    _impl = init;
    return *this;
  }

  V& at(const K& key) {
    return _impl.at(key);
  }

  const V& at(const K& key) const {
    return _impl.at(key);
  }

  V& operator[](const K& key) {
    return _impl[key];
  }

  iterator begin() {
    return _impl.begin();
  }

  const_iterator begin() const {
    return _impl.begin();
  }

  iterator end() {
    return _impl.end();
  }

  const_iterator end() const {
    return _impl.end();
  }

  bool empty() const {
    return _impl.empty();
  }

  size_type size() const {
    return _impl.size();
  }

  void clear() {
    _impl.clear();
  }

  template <typename... Args>
  bool emplace(Args&&... args) {
    return _impl.emplace(args...).second;
  }

  bool insert(const value_type& value) {
    return _impl.insert(value).second;
  }

  template <typename InputIt>
  void insert(InputIt first, InputIt last) {
    _impl.insert(first, last);
  }

  bool insert(const HashMap<K, V>& other) {
    bool changed = false;

    for(const auto& kv : other)
      changed |= _impl.insert(kv).second;

    return changed;
  }

  iterator erase(const_iterator pos) {
    return _impl.erase(pos);
  }

  iterator erase(const_iterator first, const_iterator last) {
    return _impl.erase(first, last);
  }

  size_type erase(const K& key) {
    return _impl.erase(key);
  }

  bool contains(const K& k) const {
    return _impl.find(k) != _impl.end();
  }

  moya::Vector<K> keys() const {
    moya::Vector<K> ret;

    for(const auto& it : *this)
      ret.push_back(it.first);

    return ret;
  }

  moya::Vector<V> values() const {
    moya::Vector<V> ret;

    for(const auto& it : *this)
      ret.push_back(it.second);

    return ret;
  }

  bool operator==(const HashMap<K, V>& c2) const {
    return _impl == c2.getImpl();
  }

  bool operator!=(const HashMap<K, V>& c2) const {
    return _impl != c2.getImpl();
  }

  bool operator<(const HashMap<K, V>& c2) const {
    return _impl < c2.getImpl();
  }

  bool operator<=(const HashMap<K, V>& c2) const {
    return _impl <= c2.getImpl();
  }

  bool operator>(const HashMap<K, V>& c2) const {
    return _impl > c2.getImpl();
  }

  bool operator>=(const HashMap<K, V>& c2) const {
    return _impl >= c2.getImpl();
  }

  std::string str() const {
    bool comma = false;
    std::stringstream ss;

    ss << "{";
    for(const auto& it : _impl) {
      if(comma)
        ss << ", ";
      ss << moya::str(it.first) << " : " << moya::str(it.second);
      comma = true;
    }
    ss << "}";

    return ss.str();
  }

  template <typename... ArgsT>
  void serialize(Serializer& s, ArgsT&&... args) const {
    s.mapStart();
    for(const auto& it : _impl)
      s.add(moya::str(it.first), it.second, args...);
    s.mapEnd();
  }

  void deserialize(Deserializer& d) {
    d.mapStart();
    while(d.mapEmpty()) {
      K key;
      V val;
      d.deserialize(key, val);
      _impl[key] = std::move(val);
    }
    d.mapEnd();
  }
};

} // namespace moya

#endif // MOYA_COMMON_HASHMAP_H
