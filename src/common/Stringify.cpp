#include "Stringify.h"
#include "common/Metadata.h"

#include <llvm/IR/GlobalValue.h>
#include <llvm/Support/raw_ostream.h>

namespace moya {

template <>
std::string str(const bool& b) {
  if(b)
    return "true";
  else
    return "false";
}

std::string str(const std::string& s) {
  return s;
}

std::string str(const llvm::Argument& arg) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  if(arg.hasName())
    ss << arg.getName();
  else
    ss << arg.getArgNo();

  return ss.str();
}

std::string str(const llvm::Function& f) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  if(moya::Metadata::hasMangled(f))
    if(moya::Metadata::hasSourceName(f))
      ss << moya::Metadata::getSourceName(f);
    else
      ss << f.getName();
  else
    ss << f.getName();

  return ss.str();
}

std::string str(const llvm::GlobalVariable& g) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  if(moya::Metadata::hasSourceName(g))
    ss << moya::Metadata::getSourceName(g);
  else
    ss << g.getName();

  return ss.str();
}

std::string str(const llvm::Instruction& inst) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  ss << inst;

  return ss.str();
}

std::string str(const llvm::Type& type) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  ss << type;

  return ss.str();
}

std::string str(const llvm::FunctionType& fty) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  ss << fty;

  return ss.str();
}

std::string str(const llvm::StructType& sty) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  if(sty.hasName())
    ss << sty.getName();
  else
    ss << sty;

  return ss.str();
}

std::string str(const llvm::Argument* arg) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  ss << reinterpret_cast<uint64_t>(arg);

  return ss.str();
}

std::string str(const llvm::BasicBlock* bb) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  ss << reinterpret_cast<uint64_t>(bb);

  return ss.str();
}

std::string str(const llvm::Function* f) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  ss << reinterpret_cast<uint64_t>(f);

  return ss.str();
}

std::string str(const llvm::GlobalVariable* g) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  ss << reinterpret_cast<uint64_t>(g);

  return ss.str();
}

std::string str(const llvm::Instruction* inst) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  ss << reinterpret_cast<uint64_t>(inst);

  return ss.str();
}

std::string str(llvm::Type* type) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  ss << reinterpret_cast<uint64_t>(type);

  return ss.str();
}

template <>
std::string str(const llvm::GlobalValue::LinkageTypes& linkage) {
  return str((int64_t)linkage);
}

} // namespace moya
