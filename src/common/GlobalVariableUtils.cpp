#include "APITypes.h"
#include "LLVMUtils.h"
#include "Metadata.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>

using namespace llvm;

namespace moya {

namespace LLVMUtils {

Module* getModule(GlobalValue& g) {
  return g.getParent();
}

Module* getModule(GlobalValue* g) {
  return getModule(*g);
}

const Module* getModule(const GlobalValue& g) {
  return g.getParent();
}

const Module* getModule(const GlobalValue* g) {
  return getModule(*g);
}

template <typename T, typename std::enable_if_t<std::is_class<T>::value, int>>
const T* getInitializerAs(const GlobalVariable& g) {
  return dyn_cast<T>(g.getInitializer());
}

template <typename T, typename std::enable_if_t<std::is_class<T>::value, int>>
const T* getInitializerAs(const GlobalVariable* g) {
  return getInitializerAs<T>(*g);
}

template <>
RegionID getInitializerAs(const GlobalVariable& g) {
  return getInitializerAs<ConstantInt>(g)->getLimitedValue();
}

template <>
RegionID getInitializerAs(const GlobalVariable* g) {
  return getInitializerAs<RegionID>(*g);
}

template const ConstantInt* getInitializerAs(const GlobalVariable*);
template const ConstantInt* getInitializerAs(const GlobalVariable&);
template const Function* getInitializerAs(const GlobalVariable*);
template const Function* getInitializerAs(const GlobalVariable&);
template const ConstantStruct* getInitializerAs(const GlobalVariable*);
template const ConstantStruct* getInitializerAs(const GlobalVariable&);

Type* getAnalysisType(const GlobalVariable* g) {
  return getAnalysisType(*g);
}

Type* getAnalysisType(const GlobalVariable& g) {
  Type* ret = g.getType();
  if(moya::Metadata::hasAnalysisType(g))
    ret = moya::Metadata::getAnalysisType(g);

  if(const Module* module = getModule(g))
    return getCanonicalType(ret, *module);
  return ret;
}

} // namespace LLVMUtils

} // namespace moya
