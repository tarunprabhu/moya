#ifndef MOYA_COMMON_DOTIFIER_H
#define MOYA_COMMON_DOTIFIER_H

#include "Map.h"

#include <fstream>
#include <string>

#include <llvm/Support/raw_ostream.h>

namespace moya {

class Dotifier {
protected:
  std::string buf;
  std::unique_ptr<llvm::raw_fd_ostream> poutf;
  std::unique_ptr<llvm::raw_string_ostream> pouts;
  bool directed;

protected:
  // llvm::raw_ostream& out();
  // llvm::raw_fd_ostream& outf();
  // llvm::raw_string_ostream& outs();
  llvm::raw_ostream& out() const;
  llvm::raw_fd_ostream& outf() const;
  llvm::raw_string_ostream& outs() const;

  std::string getSeparator() const;
  std::string quote(const std::string&) const;
  void addProperties(const Map<std::string, std::string>&);

  void resetf();
  void resets();

public:
  Dotifier();

  void initialize();
  void initialize(const std::string&);
  void finalize();

  bool isInitialized() const;

  void start(bool);
  void end();

  void addVertex(
      const std::string&,
      const Map<std::string, std::string>& = Map<std::string, std::string>());
  void addEdge(
      const std::string&,
      const std::string&,
      const Map<std::string, std::string>& = Map<std::string, std::string>());

  const std::string& str() const;
};

} // namespace moya

#endif // MOYA_COMMON_DOTIFIER_H
