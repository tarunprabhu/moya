#include "STDConfig.h"
#include "Diagnostics.h"
#include "Map.h"
#include "Parse.h"
#include "Set.h"
#include "StringUtils.h"
#include "Vector.h"

#include <string>

namespace moya {

namespace STDConfig {

static std::string stripImplNamespaces(const std::string& name) {
  if(moya::StringUtils::contains(name, "::__cxx11::"))
    return moya::StringUtils::replace(name, "::__cxx11", "");
  else if(moya::StringUtils::contains(name, "::__detail::"))
    return moya::StringUtils::replace(name, "::__detail", "");
  return name;
}

static const moya::Map<std::string, STDKind> stlContainerKinds = {
    {"std::pair", STDKind::STD_STL_pair},
    {"std::array", STDKind::STD_STL_array},
    {"std::vector", STDKind::STD_STL_vector},
    {"std::deque", STDKind::STD_STL_deque},
    {"std::forward_list", STDKind::STD_STL_forward_list},
    {"std::list", STDKind::STD_STL_list},
    {"std::set", STDKind::STD_STL_set},
    {"std::map", STDKind::STD_STL_map},
    {"std::multiset", STDKind::STD_STL_multiset},
    {"std::multimap", STDKind::STD_STL_multimap},
    {"std::unordered_set", STDKind::STD_STL_unordered_set},
    {"std::unordered_map", STDKind::STD_STL_unordered_map},
    {"std::unordered_multiset", STDKind::STD_STL_unordered_multiset},
    {"std::unordered_multimap", STDKind::STD_STL_unordered_multimap},
    {"std::stack", STDKind::STD_STL_stack},
    {"std::queue", STDKind::STD_STL_queue},
    {"std::priority_queue", STDKind::STD_STL_priority_queue},
    {"std::vector<bool>", STDKind::STD_STL_bitvector},
};

static const moya::Map<std::string, STDKind> stlIteratorKinds = {
    {"std::__normal_iterator", STDKind::STD_STL_iterator_normal},
    {"__gnu_cxx::__normal_iterator", STDKind::STD_STL_iterator_normal},
    {"std::reverse_iterator", STDKind::STD_STL_iterator_reverse},
    {"std::_Fwd_list_iterator", STDKind::STD_STL_iterator_forward_list},
    {"std::_List_iterator", STDKind::STD_STL_iterator_list},
    {"std::_Rb_tree_iterator", STDKind::STD_STL_iterator_tree},
    {"std::_Node_iterator", STDKind::STD_STL_iterator_hashtable},
    {"std::_Local_iterator", STDKind::STD_STL_iterator_bucket},
    {"std::_Local_iterator_base", STDKind::STD_STL_iterator_bucket},
    {"std::_Fwd_list_const_iterator",
     STDKind::STD_STL_const_iterator_forward_list},
    {"std::_List_const_iterator", STDKind::STD_STL_const_iterator_list},
    {"std::_Rb_tree_const_iterator", STDKind::STD_STL_const_iterator_tree},
    {"std::_Node_const_iterator", STDKind::STD_STL_const_iterator_hashtable},
    {"std::_Bit_iterator", STDKind::STD_STL_iterator_bitvector},
    {"std::_Bit_const_iterator", STDKind::STD_STL_const_iterator_bitvector},
};

static const moya::Map<std::string, STDKind> stdAllocatorKinds = {
    {"std::allocator", STDKind::STD_Allocator},
};

static const moya::Map<std::string, STDKind> stdInitializerKinds = {
    {"std::initializer_list", STDKind::STD_Initializer},
};

static const moya::Map<std::string, STDKind> stdStringKinds = {
    {"std::basic_string", STDKind::STD_String},
    {"std::string", STDKind::STD_String},
};

static const moya::Map<std::string, STDKind> stdStreamKinds = {
    {"std::basic_istream", STDKind::STD_Stream_basic_istream},
    {"std::basic_istringstream", STDKind::STD_Stream_basic_istringstream},
    {"std::ifstream", STDKind::STD_Stream_ifstream},
    {"std::basic_ostream", STDKind::STD_Stream_basic_ostream},
    {"std::basic_ostringstream", STDKind::STD_Stream_basic_ostringstream},
    {"std::ofstream", STDKind::STD_Stream_ofstream},
};

static const moya::Set<std::string> stdImplPrefixes = {"std::_", "__gnu_cxx::"};

// These are prefixes that look like they should be private but still "bleed"
// into the LLVM code. They are ones for which we need models
static const moya::Set<std::string> stdPublicPrefixes
    = {"__gnu_cxx::operator==",
       "__gnu_cxx::operator!=",
       "__gnu_cxx::operator<",
       "__gnu_cxx::operator<=",
       "__gnu_cxx::operator>",
       "__gnu_cxx::operator>=",
       "__gnu_cxx::operator+",
       "__gnu_cxx::operator-",
       "std::_Bit_reference"};

// static const moya::Set<std::string> stdImplClasses = {
//     "std::allocator_traits",  "std::allocator_arg", "std::pointer_traits",
//     "std::iterator_traits",   "std::basic_stringstream",
//     "std::basic_ostream", "std::basic_istream",     "std::ios_base",
//     "std::basic_ios", "std::basic_streambuf",   "std::basic_stringbuf",
//     "std::type_info", "std::type_index",        "std::exception",
//     "std::logic_error", "std::invalid_argument",  "std::domain_error",
//     "std::length_error", "std::out_of_range",      "std::future_error",
//     "std::runtime_error", "std::range_error",       "std::overflow_error",
//     "std::underflow_error", "std::regex_error",       "std::system_error",
//     "std::bad_typeid", "std::bad_cast",          "std::bad_any_cast",
//     "std::bad_weak_ptr", "std::bad_function_call", "std::bad_alloc",
//     "std::bad_exception",
// };

static const moya::Map<STDKind, std::string> stdKindNames = {
    {STDKind::STD_STL_pair, "std::pair"},
    {STDKind::STD_STL_array, "std::array"},
    {STDKind::STD_STL_vector, "std::vector"},
    {STDKind::STD_STL_deque, "std::deque"},
    {STDKind::STD_STL_list, "std::list"},
    {STDKind::STD_STL_forward_list, "std::forward_list"},
    {STDKind::STD_STL_set, "std::set"},
    {STDKind::STD_STL_map, "std::map"},
    {STDKind::STD_STL_multiset, "std::multiset"},
    {STDKind::STD_STL_multimap, "std::multimap"},
    {STDKind::STD_STL_unordered_set, "std::unordered_set"},
    {STDKind::STD_STL_unordered_map, "std::unordered_map"},
    {STDKind::STD_STL_unordered_multiset, "std::unordered_multiset"},
    {STDKind::STD_STL_unordered_multimap, "std::unordered_multimap"},
    {STDKind::STD_STL_stack, "std::stack"},
    {STDKind::STD_STL_queue, "std::queue"},
    {STDKind::STD_STL_priority_queue, "std::priority_queue"},
    {STDKind::STD_STL_bitvector, "std::vector<bool>"},

    {STDKind::STD_STL_iterator_normal, "__normal_iterator"},
    {STDKind::STD_STL_iterator_reverse, "std::reverse_iterator"},
    {STDKind::STD_STL_iterator_deque, "std::_Deque_iterator"},
    {STDKind::STD_STL_iterator_forward_list, "std::_Fwd_list_iterator"},
    {STDKind::STD_STL_iterator_list, "std::_List_iterator"},
    {STDKind::STD_STL_iterator_tree, "std::_Rb_tree_iterator"},
    {STDKind::STD_STL_iterator_hashtable, "std::_Hashtable::_node_iterator"},
    {STDKind::STD_STL_iterator_bucket, "std::_Hashtable::iterator"},
    {STDKind::STD_STL_iterator_bitvector, "std::_Bit_iterator"},

    {STDKind::STD_STL_const_iterator_forward_list,
     "std::_Fwd_list_const_iterator"},
    {STDKind::STD_STL_const_iterator_list, "std::_List_const_iterator"},
    {STDKind::STD_STL_const_iterator_tree, "std::_Rb_tree_const_iterator"},
    {STDKind::STD_STL_const_iterator_hashtable,
     "std::_Hashtable::const_iterator"},
    {STDKind::STD_STL_const_iterator_bitvector, "std::_Bit_const_iterator"},

    {STDKind::STD_Stream_basic_istream, "std::basic_istream"},
    {STDKind::STD_Stream_basic_istringstream, "std::basic_istringstream"},
    {STDKind::STD_Stream_basic_ostream, "std::basic_ostream"},
    {STDKind::STD_Stream_basic_ostringstream, "std::basic_ostringstream"},
    {STDKind::STD_Stream_ifstream, "std::ifstream"},
    {STDKind::STD_Stream_ofstream, "std::ofstream"},

    {STDKind::STD_String, "std::basic_string"},

    {STDKind::STD_Allocator, "std::allocator"},

    {STDKind::STD_Initializer, "std::initializer_list"},
};

static const moya::Map<std::string, std::string> stdOps = {
    {"std::operator==", "std::operator=="},
    {"std::operator!=", "std::operator!="},
    {"std::operator<", "std::operator<"},
    {"std::operator<=", "std::operator<="},
    {"std::operator>", "std::operator>"},
    {"std::operator>=", "std::operator>="},
    {"__gnu_cxx::operator==", "std::operator=="},
    {"__gnu_cxx::operator!=", "std::operator!="},
    {"__gnu_cxx::operator<", "std::operator<"},
    {"__gnu_cxx::operator>", "std::operator>"},
    {"__gnu_cxx::operator<=", "std::operator<"},
    {"__gnu_cxx::operator>=", "std::operator>="},
};

static const moya::Map<std::string, std::string> stdMath = {
    {"std::abs", "std::abs"},
    {"std::labs", "std::labs"},
    {"std::llabs", "std::llabs"},
    {"std::imaxabs", "std::imaxabs"},
    {"std::fabs", "std::fabs"},
    {"std::div", "std::div"},
    {"std::ldiv", "std::ldiv"},
    {"std::lldiv", "std::lldiv"},
    {"std::imaxdiv", "std::imaxdiv"},
    {"std::fmod", "std::fmod"},
    {"std::remainder", "std::remainder"},
    {"std::remquo", "std::remquo"},
    {"std::fmax", "std::fmax"},
    {"std::fmin", "std::fmin"},
    {"std::fdim", "std::fdim"},
    {"std::pow", "std::pow"},
    {"std::hypot", "std::hypot"},
    {"std::nan", "std::nan"},
    {"std::nanf", "std::nanf"},
    {"std::nanl", "std::nanl"},
    {"std::fma", "std::fma"},
    {"std::exp", "std::exp"},
    {"std::exp2", "std::exp2"},
    {"std::expm1", "std::expm"},
    {"std::log", "std::log"},
    {"std::log10", "std::log10"},
    {"std::log2", "std::log2"},
    {"std::log1p", "std::log1p"},
    {"std::pow", "std::pow"},
    {"std::sqrt", "std::sqrt"},
    {"std::cbrt", "std::cbrt"},
    {"std::hypot", "std::hypot"},
    {"std::sin", "std::sin"},
    {"std::cos", "std::cos"},
    {"std::tan", "std::tan"},
    {"std::asin", "std::asin"},
    {"std::acos", "std::acos"},
    {"std::atan", "std::atan"},
    {"std::atan2", "std::atan2"},
    {"std::sinh", "std::sinh"},
    {"std::cosh", "std::cosh"},
    {"std::tanh", "std::tanh"},
    {"std::asinh", "std::asinh"},
    {"std::acosh", "std::acosh"},
    {"std::atanh", "std::atanh"},
    {"std::erf", "std::erf"},
    {"std::erfc", "std::erfc"},
    {"std::tgamma", "std::tgamma"},
    {"std::lgamma", "std::lgamma"},
    {"std::ceil", "std::ceil"},
    {"std::floor", "std::floor"},
    {"std::trunc", "std::trunc"},
    {"std::round", "std::round"},
    {"std::lround", "std::lround"},
    {"std::llround", "std::llround"},
    {"std::nearbyint", "std::nearbyint"},
    {"std::rint", "std::rint"},
    {"std::lrint", "std::lrint"},
    {"std::llrint", "std::llrint"},
    {"std::ilogb", "std::ilogb"},
    {"std::logb", "std::logb"},
    {"std::frexp", "std::frexp"},
    {"std::ldexp", "std::ldexp"},
    {"std::modf", "std::modf"},
    {"std::scalbn", "std::scalbn"},
    {"std::scalbln", "std::scalbln"},
    {"std::nextafter", "std::nextafter"},
    {"std::nexttoward", "std::nexttoward"},
    {"std::copysign", "std::copysign"},
    {"std::fpclassify", "std::fpclassify"},
    {"std::isfinite", "std::isfinite"},
    {"std::isinf", "std::isinf"},
    {"std::isnan", "std::isnan"},
    {"std::isnormal", "std::isnormal"},
    {"std::signbit", "std::signbit"},
    {"std::isgreater", "std::isgreater"},
    {"std::isgreaterequal", "std::isgreaterequal"},
    {"std::isless", "std::isless"},
    {"std::islessequal", "std::islessequal"},
    {"std::islessgreater", "std::islessgreater"},
    {"std::isunordered", "std::isunordered"},
};

static const moya::Map<std::string, std::string> stdLocale = {
    {"std::isspace", "std::isspace"},
    {"std::isblank", "std::isblank"},
    {"std::iscntrl", "std::iscntrl"},
    {"std::isupper", "std::isupper"},
    {"std::islower", "std::islower"},
    {"std::isalpha", "std::isalpha"},
    {"std::isdigit", "std::isdigit"},
    {"std::ispunct", "std::ispunct"},
    {"std::isxdigit", "std::isxdigit"},
    {"std::isalnum", "std::isalnum"},
    {"std::isprint", "std::isprint"},
    {"std::isgraph", "std::isgraph"},
    {"std::toupper", "std::toupper"},
    {"std::tolower", "std::tolower"},
};

static const moya::Map<std::string, std::string> stdSequenceUnmod = {
    {"std::count", "std::count"},
    {"std::count_if", "std::count_if"},
    {"std::equal", "std::equal"},
    {"std::find", "std::find"},
    {"std::find_if", "std::find_if"},
    {"std::find_if_not", "std::find_if_not"},
    {"std::find_end", "std::find_end"},
    {"std::find_first_of", "std::find_first_of"},
    {"std::search", "std::search"},
    {"std::search_n", "std::search_n"},
    {"std::lexicographical_compare", "std::lexicographical_compare"},
    {"std::all_of", "std::all_of"},
    {"std::any_of", "std::any_of"},
    {"std::none_of", "std::none_of"},
    {"std::mismatch", "std::mismatch"},
    {"std::equal", "std::equal"},
    {"std::adjacent_find", "std::adjacent_find"},
};

static const moya::Map<std::string, std::string> stdSequenceMod = {
    {"std::copy", "std::copy"},
    {"std::copy_if", "std::copy_if"},
    {"std::copy_n", "std::copy_n"},
    {"std::copy_backward", "std::copy_backward"},
    {"std::move", "std::move"},
    {"std::move_backward", "std::move_backward"},
    {"std::fill", "std::fill"},
    {"std::fill_n", "std::fill_n"},
    {"std::remove", "std::remove"},
    {"std::remove_if", "std::remove_if"},
    {"std::remove_copy", "std::remove_copy"},
    {"std::remove_copy_if", "std::remove_copy_if"},
    {"std::replace", "std::replace"},
    {"std::replace_if", "std::replace_if"},
    {"std::replace_copy", "std::replace_copy"},
    {"std::replace_copy_if", "std::replace_copy_if"},
    {"std::swap", "std::swap"},
    {"std::swap_ranges", "std::swap_ranges"},
    {"std::iter_swap", "std::iter_swap"},
    {"std::reverse", "std::reverse"},
    {"std::reverse_copy", "std::reverse_copy"},
    {"std::rotate", "std::rotate"},
    {"std::rotate_copy", "std::rotate_copy"},
    {"std::unique", "std::unique"},
    {"std::unique_copy", "std::unique_copy"}

    // FIXME: TODO: Implement the rest of the models from <algorithm>
    // (modifying sequences)
};

static const moya::Map<std::string, std::string> stdPartition = {
    {"std::partition", "std::partition"},
    {"std::partition_copy", "std::partition_copy"},
    {"std::partition_point", "std::partition_point"},
    {"std::stable_partition", "std::stable_partition"},
    {"std::is_partitioned", "std::is_partitioned"},
};

static const moya::Map<std::string, std::string> stdSort = {
    {"std::is_sorted", "std::is_sorted"},
    {"std::is_sorted_until", "std::is_sorted_until"},
    {"std::sort", "std::sort"},
    {"std::partial_sort", "std::partial_sort"},
    {"std::partial_sort_copy", "std::partial_sort_copy"},
    {"std::stable_sort", "std::stable_sort"},
    {"std::nth_element", "std::nth_element"},
};

static const moya::Map<std::string, std::string> stdBinarySearch = {
    {"std::lower_bound", "std::lower_bound"},
    {"std::upper_bound", "std::upper_bound"},
    {"std::binary_search", "std::binary_search"},
    {"std::equal_range", "std::equal_range"},
};

static const moya::Map<std::string, std::string> stdSet = {
    {"std::merge", "std::merge"},
    {"std::inplace_merge", "std::inplace_merge"},
    {"std::includes", "std::includes"},
    {"std::set_difference", "std::set_difference"},
    {"std::set_intersection", "std::set_intersection"},
    {"std::set_symmetric_difference", "std::set_symmetric_difference"},
    {"std::set_union", "std::set_union"},
};

static const moya::Map<std::string, std::string> stdHeap = {
    {"std::is_heap", "std::is_heap"},
    {"std::is_heap_until", "std::is_heap_until"},
    {"std::make_heap", "std::make_heap"},
    {"std::push_heap", "std::push_heap"},
    {"std::pop_heap", "std::pop_heap"},
    {"std::sort_heap", "std::sort_heap"},
};

static const moya::Map<std::string, std::string> stdMinMax = {
    {"std::max", "std::max"},
    {"std::max_element", "std::max_element"},
    {"std::min", "std::min"},
    {"std::min_element", "std::min_element"},
    {"std::minmax", "std::minmax"},
    {"std::minmax_element", "std::minmax_element"},
    {"std::lexicographic_compare", "std::lexicographic_compare"},
    {"std::is_permutation", "std::is_permutation"},
    {"std::next_permutation", "std::next_permutation"},
    {"std::prev_permutation", "std::prev_permutation"},
};

static const moya::Map<std::string, std::string> stdNumeric = {
    {"std::iota", "std::iota"},
    {"std::accumulate", "std::accumulate"},
    {"std::inner_product", "std::inner_product"},
    {"std::adjacent_difference", "std::adjacent_difference"},
    {"std::partial_sum", "std::partial_sum"},

    // FIXME: TODO: Implement other models from <algorithm> (numeric) for C++17
};

static const moya::Map<std::string, std::string> stdUninitialized = {
    {"std::uninitialized_copy", "std::uninitialized_copy"},
    {"std::uninitialized_copy_n", "std::uninitialized_copy_n"},
    {"std::uninitialized_fill", "std::uninitialized_fill"},
    {"std::uninitialized_fill_n", "std::uninitialized_fill_n"},

    // FIXME: TODO: Implement other models from <algorithm> (uninitialized) for
    // C++17
};

static const moya::Map<std::string, std::string> stdPair = {
    {"std::make_pair", "std::make_pair"},
};

// FIXME: Implement models from <utilities>

static moya::Vector<moya::Map<std::string, std::string>> stdModels
    = {stdOps,
       stdMath,
       stdLocale,
       stdSequenceUnmod,
       stdSequenceMod,
       stdPartition,
       stdSort,
       stdBinarySearch,
       stdSet,
       stdHeap,
       stdMinMax,
       stdNumeric,
       stdUninitialized,
       stdPair};

static bool isSTLContainer(const std::string& name) {
  if(stlContainerKinds.contains(name))
    return stlContainerKinds.contains(name);
  if(moya::StringUtils::startswith(name, "std::vector<bool, "))
    return true;
  return false;
}

static bool isSTLIterator(const std::string& name) {
  return stlIteratorKinds.contains(name);
}

static bool isSTDAllocator(const std::string& name) {
  return stdAllocatorKinds.contains(name);
}

static bool isSTDInitializer(const std::string& name) {
  return stdInitializerKinds.contains(name);
}

static bool isSTDStream(const std::string& name) {
  return stdStreamKinds.contains(name);
}

static bool isSTDString(const std::string& name) {
  return stdStringKinds.contains(name);
}

static bool isSTDImpl(const std::string& name) {
  // if(contains(stdImplClasses, name))
  //   return true;
  for(const std::string& s : stdImplPrefixes)
    if(name.find(s) == 0)
      return true;
  return false;
}

static bool isSTDPublic(const std::string& name) {
  for(const std::string& s : stdPublicPrefixes)
    if(name.find(s) == 0)
      return true;
  if(name.find("std::") == 0)
    return name[5] != '_';
  return false;
}

static STDKind getSTLContainerKind(const std::string& name) {
  if(moya::StringUtils::startswith(name, "std::vector<bool, "))
    return STDKind::STD_STL_bitvector;
  return stlContainerKinds.at(name);
}

static STDKind getSTLIteratorKind(const std::string& name) {
  return stlIteratorKinds.at(name);
}

static STDKind getSTDStreamKind(const std::string& name) {
  return stdStreamKinds.at(name);
}

bool isSTLContainer(STDKind kind) {
  return (kind > STDKind::STD_STL_container_first)
         and (kind < STDKind::STD_STL_container_last);
}

bool isSTLIterator(STDKind kind) {
  return (kind > STDKind::STD_STL_iterator_first)
         and (kind < STDKind::STD_STL_iterator_last);
}

bool isSTDAllocator(STDKind kind) {
  return kind == STD_Allocator;
}

bool isSTDInitializer(STDKind kind) {
  return kind == STD_Initializer;
}

bool isSTDStream(STDKind kind) {
  return isSTDIStream(kind) or isSTDOStream(kind);
}

bool isSTDIStream(STDKind kind) {
  return (kind > STDKind::STD_Stream_istream_first)
         and (kind < STDKind::STD_Stream_ostream_last);
}

bool isSTDOStream(STDKind kind) {
  return (kind > STDKind::STD_Stream_ostream_first)
         and (kind < STDKind::STD_Stream_ostream_last);
}

bool isSTDString(STDKind kind) {
  return kind == STD_String;
}

bool isSTDImpl(STDKind kind) {
  return kind == STD_Impl;
}

bool isSTDPublic(STDKind kind) {
  return kind == STD_Public;
}

bool isVectorContainer(STDKind kind) {
  return ((kind == STDKind::STD_STL_array)
          or (kind == STDKind::STD_STL_vector));
}

bool isBitVectorContainer(STDKind kind) {
  return (kind == STDKind::STD_STL_bitvector);
}

bool isDequeContainer(STDKind kind) {
  return ((kind == STDKind::STD_STL_deque) or (kind == STDKind::STD_STL_stack)
          or (kind == STDKind::STD_STL_queue));
}

bool isTreeContainer(STDKind kind) {
  return ((kind == STDKind::STD_STL_set) or (kind == STDKind::STD_STL_map)
          or (kind == STDKind::STD_STL_multiset)
          or (kind == STDKind::STD_STL_multimap));
}

bool isHashtableContainer(STDKind kind) {
  return ((kind == STDKind::STD_STL_unordered_set)
          or (kind == STDKind::STD_STL_unordered_map)
          or (kind == STDKind::STD_STL_unordered_multiset)
          or (kind == STDKind::STD_STL_unordered_multimap));
}

bool isForwardListContainer(STDKind kind) {
  return kind == STDKind::STD_STL_forward_list;
}

bool isListContainer(STDKind kind) {
  return kind == STDKind::STD_STL_list;
}

bool isArrayLikeContainer(STDKind kind) {
  return ((kind == STDKind::STD_STL_array)
          or (kind == STDKind::STD_STL_vector));
}

bool isListLikeContainer(STDKind kind) {
  return ((kind == STDKind::STD_STL_list)
          or (kind == STDKind::STD_STL_forward_list));
}

bool isQueueLikeContainer(STDKind kind) {
  return ((kind == STDKind::STD_STL_deque) or (kind == STDKind::STD_STL_stack)
          or (kind == STDKind::STD_STL_queue)
          or (kind == STDKind::STD_STL_priority_queue));
}

bool isSetLikeContainer(STDKind kind) {
  return ((kind == STDKind::STD_STL_set) or (kind == STDKind::STD_STL_multiset)
          or (kind == STDKind::STD_STL_unordered_set)
          or (kind == STDKind::STD_STL_unordered_multiset));
}

bool isMapLikeContainer(STDKind kind) {
  return ((kind == STDKind::STD_STL_map) or (kind == STDKind::STD_STL_multimap)
          or (kind == STDKind::STD_STL_unordered_map)
          or (kind == STDKind::STD_STL_unordered_multimap));
}

STDKind getSTDKind(const std::string& s) {
  const std::string name = stripImplNamespaces(s);
  if(isSTLContainer(name))
    return getSTLContainerKind(name);
  else if(isSTLIterator(name))
    return getSTLIteratorKind(name);
  else if(isSTDInitializer(name))
    return STDKind::STD_Initializer;
  else if(isSTDAllocator(name))
    return STDKind::STD_Allocator;
  else if(isSTDString(name))
    return STDKind::STD_String;
  else if(isSTDStream(name))
    return getSTDStreamKind(name);
  else if(isSTDPublic(name))
    return STDKind::STD_Public;
  else if(isSTDImpl(name))
    return STDKind::STD_Impl;
  return STDKind::STD_None;
}

std::string getSTDName(STDKind kind) {
  if(stdKindNames.contains(kind))
    return stdKindNames.at(kind);
  return "";
}

bool isSTDModel(const std::string& fn) {
  for(const auto& m : stdModels)
    if(m.contains(fn))
      return true;
  return false;
}

std::string getSTDModel(const std::string& fn) {
  for(const auto& m : stdModels)
    if(m.contains(fn))
      return m.at(fn);
  return "";
}

} // namespace STDConfig

template <>
std::string str(const STDKind& kind) {
  switch(kind) {
  case STD_None:
    return "std::<none>";

    // STL containers
  case STD_STL_pair:
    return "std::pair";
  case STD_STL_array:
    return "std::array";
  case STD_STL_vector:
    return "std::vector";
  case STD_STL_deque:
    return "std::deque";
  case STD_STL_forward_list:
    return "std::forward_list";
  case STD_STL_list:
    return "std::list";
  case STD_STL_set:
    return "std::set";
  case STD_STL_map:
    return "std::map";
  case STD_STL_multiset:
    return "std::multiset";
  case STD_STL_multimap:
    return "std::multimap";
  case STD_STL_unordered_set:
    return "std::unordered_set";
  case STD_STL_unordered_map:
    return "std::unordered_map";
  case STD_STL_unordered_multiset:
    return "std::unordered_multiset";
  case STD_STL_unordered_multimap:
    return "std::unordered_multimap";
  case STD_STL_stack:
    return "std::stack";
  case STD_STL_queue:
    return "std::queue";
  case STD_STL_priority_queue:
    return "std::priority_queue";
  case STD_STL_bitvector:
    return "std::vector<bool>";

  // STL Iterators
  case STD_STL_iterator_normal:
    return "std::iterator";
  case STD_STL_iterator_reverse:
    return "std::reverse_iterator";
  case STD_STL_iterator_deque:
    return "std::deque::iterator";
  case STD_STL_iterator_forward_list:
    return "std::forward_list::iterator";
  case STD_STL_iterator_list:
    return "std::list::iterator";
  case STD_STL_iterator_tree:
    return "std::tree::iterator";
  case STD_STL_iterator_hashtable:
    return "std::hashtable::iterator";
  case STD_STL_iterator_bucket:
    return "std::hashtable::bucket_iterator";
  case STD_STL_iterator_bitvector:
    return "std::vector<bool>::iterator";
  case STD_STL_const_iterator_forward_list:
    return "std::forward_list::const_iterator";
  case STD_STL_const_iterator_list:
    return "std::list::const_iterator";
  case STD_STL_const_iterator_tree:
    return "std::tree::const_iterator";
  case STD_STL_const_iterator_hashtable:
    return "std::hashtable::const_iterator";
  case STD_STL_const_iterator_bitvector:
    return "std::vector<bool>::const_iterator";

  // Stream
  case STD_Stream_basic_istream:
    return "std::basic_istream";
  case STD_Stream_basic_istringstream:
    return "std::basic_istringstream";
  case STD_Stream_ifstream:
    return "std::ifstream";
  case STD_Stream_basic_ostream:
    return "std::basic_ostream";
  case STD_Stream_basic_ostringstream:
    return "std::basic_ostringstream";
  case STD_Stream_ofstream:
    return "std::ofstream";

  // std::string
  case STD_String:
    return "std::string";

  // std::allocator<T>
  case STD_Allocator:
    return "std::allocator";

  // std::initializer_list<T>
  case STD_Initializer:
    return "std::initializer_list";

  // Other functions and classes in the std namespace which are visible
  // to the user
  case STD_Public:
    return "std::<public>";

  // Functions and classes in the std namespace that are *NOT* visible to the
  // user
  case STD_Impl:
    return "std::__<impl>";

  default:
    moya_error("Unknown STD kind: " << kind);
  }
}

template <>
STDKind parse(const std::string& s) {
  return STDConfig::getSTDKind(s);
}

} // namespace moya
