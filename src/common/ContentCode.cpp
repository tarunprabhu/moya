#include "ContentCode.h"
#include "JSONUtils.h"
#include "Metadata.h"

#include <sstream>

using namespace llvm;

namespace moya {

ContentCode::ContentCode(const Function* f)
    : Content(Content::Kind::Code), function(f) {
  ;
}

const Function* ContentCode::getFunction() const {
  return function;
}

bool ContentCode::classof(const Content* c) {
  return c->getKind() == Content::Kind::Code;
}

std::string ContentCode::str(bool name) const {
  std::string s;
  raw_string_ostream ss(s);

  ss << "CCode(";
  if(function)
    if(name)
      if(moya::Metadata::hasSourceName(function))
        ss << moya::Metadata::getSourceName(function);
      else
        ss << function->getName();
    else
      ss << moya::Metadata::getID(function);
  else
    ss << "_moya.jitted.code_";
  ss << ")";

  return ss.str();
}

std::string ContentCode::str() const {
  return str(true);
}

void ContentCode::serialize(Serializer& s) const {
  if(function) {
    s.mapStart();
    s.add("_class", "Code");
    s.add("function", JSON::pointer(function));
    s.mapEnd();
  }
}

} // namespace moya
