#include "StoreCell.h"
#include "AerynConfig.h"
#include "Assert.h"
#include "Config.h"
#include "Diagnostics.h"
#include "Formatting.h"
#include "JSONUtils.h"
#include "LLVMUtils.h"
#include "Metadata.h"
#include "Serializer.h"
#include "Store.h"
#include "Vector.h"

#include <llvm/Support/raw_ostream.h>

#include <algorithm>
#include <iostream>

using llvm::cast;
using llvm::dyn_cast;
using llvm::isa;

namespace moya {

StoreCell::StoreCell(Address addr,
                     llvm::Type* type,
                     uint64_t size,
                     const CellFlags& flags,
                     const Store& store)
    : store(store), address(addr), type(type), size(size), flags(flags) {
  ;
}

bool StoreCell::addAccessor(std::tuple<Funcs, Insts>& dst,
                            const llvm::Instruction* inst) {
  bool changed = false;

  if(inst) {
    if(const llvm::Function* f = LLVMUtils::getFunction(inst))
      changed |= std::get<Funcs>(dst).insert(f);
    changed |= std::get<Insts>(dst).insert(inst);
  }

  return changed;
}

bool StoreCell::addAccessor(std::tuple<Funcs, Insts>& dst,
                            const llvm::Function* f) {
  bool changed = false;

  changed |= std::get<Funcs>(dst).insert(f);

  return changed;
}

bool StoreCell::addAccessors(std::tuple<Funcs, Insts>& dst,
                             const std::tuple<Funcs, Insts>& src) {
  bool changed = false;

  for(const llvm::Function* f : std::get<Funcs>(src))
    changed |= std::get<Funcs>(dst).insert(f);

  for(const llvm::Instruction* inst : std::get<Insts>(src))
    changed |= std::get<Insts>(dst).insert(inst);

  return changed;
}

bool StoreCell::hasAccessors(const std::tuple<Funcs, Insts>& accessors) const {
  return std::get<Funcs>(accessors).size() or std::get<Insts>(accessors).size();
}

StoreCell::Address StoreCell::getAddress() const {
  return address;
}

llvm::Type* StoreCell::getType() const {
  return type;
}

uint64_t StoreCell::getSize() const {
  return size;
}

bool StoreCell::isEmpty() const {
  return not contents.size();
}

const CellFlags& StoreCell::getFlags() const {
  return flags;
}

void StoreCell::addFlags(const CellFlags& flags) {
  this->flags |= flags;
}

void StoreCell::write(const Content* c,
                      const llvm::Instruction* inst,
                      AnalysisStatus& changed) {
  changed |= addWriter(inst);
  moya_error_if_not(c, "Writing null");
  changed |= contents.insert(c);
}

void StoreCell::write(const Content* c) {
  contents.insert(c);
}

const Contents& StoreCell::read() const {
  return contents;
}

const Contents& StoreCell::read(const llvm::Instruction* inst,
                                AnalysisStatus& changed) {
  changed |= addReader(inst);
  return contents;
}

const Contents& StoreCell::read(const llvm::Function* f,
                                AnalysisStatus& changed) {
  if(Metadata::hasID(f))
    changed |= std::get<Funcs>(readers).insert(f);
  return contents;
}

bool StoreCell::addReader(const llvm::Instruction* inst) {
  return addAccessor(readers, inst);
}

bool StoreCell::addReader(const llvm::Function* f) {
  return addAccessor(readers, f);
}

bool StoreCell::addWriter(const llvm::Instruction* inst) {
  return addAccessor(writers, inst);
}

bool StoreCell::addWriter(const llvm::Function* f) {
  return addAccessor(writers, f);
}

bool StoreCell::addAllocator(const llvm::Instruction* inst) {
  return addAccessor(allocators, inst);
}

bool StoreCell::addAllocator(const llvm::Function* f) {
  return addAccessor(allocators, f);
}

bool StoreCell::addDeallocator(const llvm::Instruction* inst) {
  return addAccessor(deallocators, inst);
}

bool StoreCell::addDeallocator(const llvm::Function* f) {
  return addAccessor(deallocators, f);
}

template <typename T>
const Set<const T*>& StoreCell::getReaders() const {
  return std::get<Set<const T*>>(readers);
}

template <typename T>
const Set<const T*>& StoreCell::getWriters() const {
  return std::get<Set<const T*>>(writers);
}

template <typename T>
const Set<const T*>& StoreCell::getAllocators() const {
  return std::get<Set<const T*>>(allocators);
}

template <typename T>
const Set<const T*>& StoreCell::getDeallocators() const {
  return std::get<Set<const T*>>(deallocators);
}

bool StoreCell::isAccessedByFunction(
    MoyaID id,
    const Set<const llvm::Function*>& accessors) const {
  for(auto* f : accessors)
    if(Metadata::getID(f) == id)
      return true;
  return false;
}

bool StoreCell::isReadByFunction(MoyaID id) const {
  return isAccessedByFunction(id, getReaders<llvm::Function>());
}

bool StoreCell::isWrittenByFunction(MoyaID id) const {
  return isAccessedByFunction(id, getWriters<llvm::Function>());
}

bool StoreCell::isAllocatedByFunction(MoyaID id) const {
  return isAccessedByFunction(id, getAllocators<llvm::Function>());
}

bool StoreCell::isDeallocatedByFunction(MoyaID id) const {
  return isAccessedByFunction(id, getDeallocators<llvm::Function>());
}

bool StoreCell::isUsedByFunction(MoyaID id) const {
  return isReadByFunction(id) or isWrittenByFunction(id)
         or isAllocatedByFunction(id) or isDeallocatedByFunction(id);
}

void StoreCell::merge(const StoreCell& dst, bool accessorsOnly) {
  addAccessors(readers, dst.readers);
  addAccessors(writers, dst.writers);
  addAccessors(allocators, dst.allocators);
  addAccessors(deallocators, dst.deallocators);

  if(not accessorsOnly)
    contents.insert(dst.contents);
}

bool StoreCell::isRead() const {
  return hasAccessors(readers);
}

bool StoreCell::isWritten() const {
  return hasAccessors(writers);
}

bool StoreCell::isAllocated() const {
  return hasAccessors(allocators);
}

bool StoreCell::isDeallocated() const {
  return hasAccessors(deallocators);
}

bool StoreCell::hasConstantContents() const {
  for(const Content* c : contents)
    if(not isa<ContentScalar>(c))
      return false;
  return true;
}

template const Set<const llvm::Function*>& StoreCell::getReaders() const;
template const Set<const llvm::Function*>& StoreCell::getWriters() const;
template const Set<const llvm::Function*>& StoreCell::getAllocators() const;
template const Set<const llvm::Function*>& StoreCell::getDeallocators() const;

template const Set<const llvm::Instruction*>& StoreCell::getReaders() const;
template const Set<const llvm::Instruction*>& StoreCell::getWriters() const;
template const Set<const llvm::Instruction*>& StoreCell::getAllocators() const;
template const Set<const llvm::Instruction*>&
StoreCell::getDeallocators() const;

std::string StoreCell::str(int depth,
                           const Set<const llvm::Function*>& accs) const {
  std::string s;
  llvm::raw_string_ostream ss(s);

  for(const llvm::Function* f : accs)
    ss << "\n" << space(depth) << getDiagnostic(f);

  return ss.str();
}

std::string StoreCell::str(int depth,
                           const Set<const llvm::Instruction*>& accs) const {
  std::string s;
  llvm::raw_string_ostream ss(s);

  for(const llvm::Instruction* inst : accs)
    ss << "\n"
       << space(depth) << *inst << " [" << LLVMUtils::getFunctionName(inst)
       << "]";

  return ss.str();
}

std::string StoreCell::str(int depth, const Contents& contents) const {
  std::string s;
  llvm::raw_string_ostream ss(s);

  for(const Content* c : contents)
    ss << "\n" << space(depth) << c->str();

  return ss.str();
}

std::string StoreCell::str(int depth) const {
  std::string s;
  llvm::raw_string_ostream ss(s);

  ss << space1(depth) << "type        : " << *getType() << "\n"
     << space1(depth) << "flags       : " << getFlags().str() << "\n";
  ss << space1(depth)
     << "readers     :" << str(indent2(depth), getReaders<llvm::Function>())
     << "\n";
  ss << space1(depth)
     << "writers     :" << str(indent2(depth), getWriters<llvm::Function>())
     << "\n";
  ss << space1(depth)
     << "allocators  :" << str(indent2(depth), getAllocators<llvm::Function>())
     << "\n";
  ss << space1(depth) << "deallocators:"
     << str(indent2(depth), getDeallocators<llvm::Function>()) << "\n";
  ss << space1(depth) << "contents    :" << str(indent2(depth), contents);

  return ss.str();
}

void StoreCell::serialize(const std::string& label,
                          const std::tuple<Funcs, Insts>& accessors,
                          Serializer& s) const {
  // // FIXME: This is a shitty hack. Ideally, the functions will have a range
  // of
  // // source locations associated with them and we don't have to associate a
  // // location with a function
  // using SourceLoc = std::tuple<Location, const llvm::Function*>;
  // auto cmp = [](const SourceLoc& l, const SourceLoc& r) {
  //   return Location::compare(std::get<Location>(l), std::get<Location>(r))
  //          != Location::Cmp::Identical;
  // };
  // Set<SourceLoc, decltype(cmp)> locs(cmp);
  // for(const llvm::Instruction* inst : std::get<Insts>(accessors))
  //   if(llvm::DebugLoc dbg = inst->getDebugLoc())
  //     locs.insert(std::make_tuple(LLVMUtils::getLocation(dbg),
  //                                 LLVMUtils::getFunction(inst)));

  s.arrStart(label);
  // if(locs.size()) {
  //   s.arrStart("locations");
  //   for(const SourceLoc& loc : locs)
  //     s.serialize(std::get<Location>(loc), std::get<const
  //     llvm::Function*>(loc));
  //   s.arrEnd("locations");
  // }

  Set<const llvm::Function*> seen;
  // s.arrStart("instructions");
  for(const llvm::Instruction* inst : std::get<Insts>(accessors)) {
    if(const auto* alloca = dyn_cast<llvm::AllocaInst>(inst))
      s.serialize(JSON::pointer(alloca));
    else
      s.serialize(JSON::pointer(inst));
    seen.insert(LLVMUtils::getFunction(inst));
  }
  for(const llvm::Function* f : std::get<Funcs>(accessors))
    if(not seen.contains(f))
      s.serialize(JSON::pointer(nullptr, f));
  // s.arrEnd("instructions");

  s.arrEnd(label);
}

void StoreCell::serialize(Serializer& s) const {
  s.mapStart();
  s.add("_class", "Cell");
  s.add("address", moya::str(getAddress()));
  s.add("type", JSON::pointer(getType()));
  if(contents.size()) {
    s.arrStart("contents");
    for(const Content* c : read())
      c->serialize(s);
    s.arrEnd();
  }
  serialize("readers", readers, s);
  serialize("writers", writers, s);
  serialize("allocators", allocators, s);
  serialize("deallocators", deallocators, s);
  s.add("flags", flags);

  if(store.structs.contains(address)) {
    Vector<llvm::StructType*> structs;
    for(llvm::StructType* sty : store.structs.at(address))
      structs.push_back(sty);

    std::sort(structs.begin(),
              structs.end(),
              [&](llvm::StructType* left, llvm::StructType* right) {
                return store.getTypeSize(left) < store.getTypeSize(right);
              });
    std::reverse(structs.begin(), structs.end());
    s.arrStart("structs");
    for(llvm::StructType* sty : structs)
      s.serialize(JSON::pointer(sty));
    s.arrEnd("structs");
  }

  if(store.allocated.contains(address))
    s.add("allocated", JSON::pointer(store.allocated.at(address)));

  s.mapEnd();
}

} // namespace moya
