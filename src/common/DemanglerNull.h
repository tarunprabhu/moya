#ifndef MOYA_COMMON_DEMANGLER_NULL_H
#define MOYA_COMMON_DEMANGLER_NULL_H

#include "Demangler.h"

namespace moya {

class DemanglerNull : public Demangler {
public:
  DemanglerNull();
  virtual ~DemanglerNull() = default;

  virtual std::string demangle(const llvm::Function&,
                               Demangler::Action) const override;
  virtual std::string demangle(const llvm::GlobalVariable&,
                               Demangler::Action) const override;
};

} // namespace moya

#endif // MOYA_COMMON_DEMANGLER_NULL_H
