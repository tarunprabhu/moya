#ifndef MOYA_COMMON_CONTENT_SCALAR_H
#define MOYA_COMMON_CONTENT_SCALAR_H

#include "Content.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>

namespace moya {

class ContentScalar : public Content {
protected:
  const llvm::Constant* val;

public:
  ContentScalar(const llvm::Constant*);
  virtual ~ContentScalar() = default;

  const llvm::Constant* getValue() const;
  llvm::Type* getType() const;
  llvm::LLVMContext& getContext() const;

  long getIntegerValue() const;
  double getFloatingPointValue() const;

  virtual std::string str() const;
  virtual void serialize(Serializer&) const;

public:
  static bool classof(const Content*);
};

} // namespace moya

#endif // MOYA_COMMON_CONTENT_SCALAR_H
