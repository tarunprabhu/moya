#include "Diagnostics.h"
#include "Graph.h"
#include "LLVMUtils.h"
#include "Metadata.h"
#include "Set.h"

#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;

namespace moya {

namespace LLVMUtils {

// Returns the innermost type of a sequential type - basically if the type is
// an array, pointer or vector type, it finds the innermost scalar or struct
// type
Type* getInnermost(Type* ty) {
  if(auto* pty = dyn_cast_or_null<PointerType>(ty))
    return getInnermost(pty->getElementType());
  else if(auto* aty = dyn_cast_or_null<ArrayType>(ty))
    return getInnermost(aty->getElementType());
  return ty;
}

Type* getInnermost(const Value* v) {
  return getInnermost(v->getType());
}

Type* getInnermost(const llvm::Metadata* m) {
  return getInnermost(dyn_cast<ValueAsMetadata>(m)->getValue());
}

unsigned getTypeDepth(const Type* type, unsigned curr) {
  if(auto* pty = dyn_cast<PointerType>(type))
    return getTypeDepth(pty->getElementType(), curr + 1);
  else if(auto* aty = dyn_cast<ArrayType>(type))
    return getTypeDepth(aty->getElementType(), curr + 1);
  return curr;
}

unsigned getMaxTypeDepth(const Type* type, unsigned curr) {
  if(auto* pty = dyn_cast<PointerType>(type))
    return getMaxTypeDepth(pty->getElementType(), curr + 1);
  else if(auto* aty = dyn_cast<ArrayType>(type))
    return getMaxTypeDepth(aty->getElementType(), curr + 1);
  else if(auto* sty = dyn_cast<StructType>(type))
    return getMaxTypeDepth(sty->getElementType(0), curr + 1);
  return curr;
}

bool isComplexTy(const Type* ty) {
  if(const auto* sty = dyn_cast<StructType>(ty))
    if(not sty->hasName() or isLiteralStructTy(sty))
      if(sty->getNumElements() == 2) {
        Type* f1 = sty->getElementType(0);
        Type* f2 = sty->getElementType(1);
        if(f1->isIntegerTy() or f1->isFloatTy() or f1->isDoubleTy()
           or f1->isX86_FP80Ty())
          return f1 == f2;
      }
  return false;
}

bool isIntTy(const Type* ty) {
  return ty->isIntegerTy();
}

bool isFPTy(const Type* ty) {
  if(ty->isFloatTy() or ty->isDoubleTy() or ty->isX86_FP80Ty()
     or ty->isFP128Ty())
    return true;
  return false;
}

bool isScalarTy(const Type* ty) {
  if(isIntTy(ty) or isFPTy(ty))
    return true;
  return false;
}

bool isPointerTy(const Type* ty) {
  return ty->isPointerTy();
}

bool isArrayTy(const Type* ty) {
  return ty->isArrayTy();
}

bool isStructTy(const Type* ty) {
  return ty->isStructTy();
}

bool isFunctionTy(const Type* ty) {
  return ty->isFunctionTy();
}

bool isVoidTy(const Type* ty) {
  return ty->isVoidTy();
}

bool isPtrToScalarTy(const Type* ty) {
  if(auto* pty = dyn_cast<PointerType>(ty))
    if(isScalarTy(pty->getElementType()))
      return true;
  return false;
}

bool isPtrToIntTy(const Type* ty) {
  if(auto* pty = dyn_cast<PointerType>(ty))
    if(pty->getElementType()->isIntegerTy())
      return true;
  return false;
}

bool isPtrToInt8Ty(const Type* ty) {
  if(auto* pty = dyn_cast<PointerType>(ty))
    if(pty->getElementType()->isIntegerTy(8))
      return true;
  return false;
}

bool isPtrToInt16Ty(const Type* ty) {
  if(auto* pty = dyn_cast<PointerType>(ty))
    if(pty->getElementType()->isIntegerTy(16))
      return true;
  return false;
}

bool isPtrToInt32Ty(const Type* ty) {
  if(auto* pty = dyn_cast<PointerType>(ty))
    if(pty->getElementType()->isIntegerTy(32))
      return true;
  return false;
}

bool isPtrToInt64Ty(const Type* ty) {
  if(auto* pty = dyn_cast<PointerType>(ty))
    if(pty->getElementType()->isIntegerTy(64))
      return true;
  return false;
}

bool isPtrToFloatTy(const Type* ty) {
  if(auto* pty = dyn_cast<PointerType>(ty))
    if(pty->getElementType()->isFloatTy())
      return true;
  return false;
}

bool isPtrToDoubleTy(const Type* ty) {
  if(auto* pty = dyn_cast<PointerType>(ty))
    if(pty->getElementType()->isDoubleTy())
      return true;
  return false;
}

bool isPtrToLongDoubleTy(const Type* ty) {
  if(auto* pty = dyn_cast<PointerType>(ty))
    if(pty->getElementType()->isX86_FP80Ty())
      return true;
  return false;
}

bool isPtrToFPTy(const Type* ty) {
  if(auto* pty = dyn_cast<PointerType>(ty))
    if(isFPTy(pty->getElementType()))
      return true;
  return false;
}

bool isPtrToPtrTy(const Type* ty) {
  if(const auto* pty = dyn_cast_or_null<PointerType>(ty))
    return pty->getElementType()->isPointerTy();
  return false;
}

bool isPtrToFunctionTy(const Type* ty) {
  if(auto* pty = dyn_cast<PointerType>(ty))
    if(pty->getElementType()->isFunctionTy())
      return true;
  return false;
}

bool isPtrToStructTy(const Type* ty) {
  if(auto* pty = dyn_cast<PointerType>(ty))
    if(pty->getElementType()->isStructTy())
      return true;
  return false;
}

bool isPtrToVtableTy(const Type* ty) {
  if(auto* pty = dyn_cast<PointerType>(ty))
    if(isVtableTy(pty->getElementType()))
      return true;
  return false;
}

bool isScalarArrayTy(const Type* ty) {
  if(const auto* aty = dyn_cast<ArrayType>(ty)) {
    if(const auto* bty = dyn_cast<ArrayType>(aty->getElementType())) {
      if(not isScalarArrayTy(bty))
        return false;
    } else if(not isScalarTy(aty->getElementType())) {
      return false;
    }
    return true;
  }
  return false;
}

bool isScalarStructTy(const Type* ty) {
  if(const auto* sty = dyn_cast<StructType>(ty)) {
    for(const Type* ty : sty->elements()) {
      if(const auto* tty = dyn_cast<StructType>(ty)) {
        if(not isScalarStructTy(tty))
          return false;
      } else if(not isScalarTy(ty)) {
        return false;
      }
    }
    return true;
  }
  return false;
}

bool isLiteralStructTy(const Type* type) {
  if(const auto* sty = dyn_cast<StructType>(type)) {
    if(not sty->hasName())
      return true;
    else if(sty->getName().find(".moya.name.") != std::string::npos)
      return true;
  }
  return false;
}

bool is0ArrayTy(const Type* ty) {
  if(const auto* aty = dyn_cast<ArrayType>(ty))
    if(getNumFlattenedElements(aty) == 0)
      return true;
  return false;
}

bool isVtableTy(const Type* ty) {
  if(auto* pty1 = dyn_cast<PointerType>(ty))
    if(auto* pty2 = dyn_cast<PointerType>(pty1->getElementType()))
      if(isFunctionTy(pty2->getElementType()))
        return true;
  return false;
}

std::string getAnonymousStructName(const StructType* sty) {
  std::string s;
  raw_string_ostream ss(s);
  ss << *sty;

  unsigned long hash = 226130351;
  for(char c : ss.str())
    hash = hash * 31 + c;

  std::stringstream out;
  out << "struct.moya.anon." << hash;
  return out.str();
}

static void getLoadableTypes(Type* ty, Set<Type*>& ret) {
  // Don't return array and struct types because they can never be loaded
  // There will be another pass somewhere that checks to make sure that this
  // never happens anywhere in the code
  if(auto* aty = dyn_cast<ArrayType>(ty)) {
    getLoadableTypes(aty->getElementType(), ret);
  } else if(auto* sty = dyn_cast<StructType>(ty)) {
    if(sty->getNumElements())
      getLoadableTypes(sty->getElementType(0), ret);
    if(sty->isLiteral())
      ret.insert(sty);
  } else {
    ret.insert(ty);
  }
}

Set<Type*> getLoadableTypes(Type* ty) {
  Set<Type*> ret;
  if(auto* pty = dyn_cast<PointerType>(ty))
    getLoadableTypes(pty->getElementType(), ret);
  return ret;
}

unsigned getPointerDepth(Type* type, unsigned depth) {
  if(auto* pty = dyn_cast<PointerType>(type))
    return getPointerDepth(pty->getElementType(), depth + 1);
  return depth;
}

Type* getElementType(Type* type) {
  if(auto* pty = dyn_cast<PointerType>(type))
    return pty->getElementType();
  moya_error("Not a pointer type: " << *type);
}

Type* getCanonicalType(Type* ty, const Module& module) {
  if(auto* pty = dyn_cast<PointerType>(ty)) {
    return getCanonicalType(pty->getElementType(), module)->getPointerTo();
  } else if(auto* aty = dyn_cast<ArrayType>(ty)) {
    if(aty->getNumElements() == 0)
      return getCanonicalType(aty->getElementType(), module);
    else
      return ArrayType::get(getCanonicalType(aty->getElementType(), module),
                            aty->getNumElements());
  }
  // We don't look inside structs because they are named and "fixing" them
  // might result in bizarre "type mismatches". The analysis is supposed to
  // be robust and handle these things but in practice, it still depends on
  // the "names" of the types far too much

  return ty;
}

uint64_t getNumFlattenedElements(const ArrayType* aty) {
  if(const auto* bty = dyn_cast<ArrayType>(aty->getElementType()))
    return aty->getNumElements() * getNumFlattenedElements(bty);
  return aty->getNumElements();
}

Type* getFlattenedElementType(const ArrayType* aty) {
  if(const auto* bty = dyn_cast<ArrayType>(aty->getElementType()))
    return getFlattenedElementType(bty);
  return aty->getElementType();
}

ArrayType* getFlattened(const ArrayType* aty) {
  Type* element = getFlattenedElementType(aty);
  uint64_t nelems = getNumFlattenedElements(aty);
  return ArrayType::get(element, nelems);
}

Set<StructType*> getEquivalent(StructType* sty, const llvm::Module& module) {
  if(Metadata::hasEquivalent(sty, module))
    return Metadata::getEquivalent(sty, module);
  return {};
}

static void flatten(const StructType* sty, Vector<Type*>& flattened) {
  // The indices of the elements of the struct that are themselves structs
  for(Type* field : sty->elements())
    if(auto* tty = dyn_cast<StructType>(field))
      if(tty->getNumElements())
        flatten(tty, flattened);
      else
        flattened.push_back(tty);
    else
      flattened.push_back(field);
}

Vector<Type*> getFlattenedTypes(const StructType* sty) {
  Vector<Type*> types;

  flatten(sty, types);

  return types;
}

static void flattenedOffsets(StructType* sty,
                             const Module& module,
                             Vector<uint64_t>& offsets) {
  const llvm::StructLayout& sl = *module.getDataLayout().getStructLayout(sty);
  for(unsigned i = 0; i < sty->getNumElements(); i++)
    if(auto* tty = dyn_cast<StructType>(sty->getElementType(i)))
      flattenedOffsets(tty, module, offsets);
    else
      offsets.push_back(sl.getElementOffset(i));
}

Vector<uint64_t> getFlattenedOffsets(StructType* sty, const Module& module) {
  Vector<uint64_t> offsets;

  flattenedOffsets(sty, module, offsets);

  return offsets;
}

StructType* getFlattened(const StructType* sty, Module& module) {
  return createStruct(
      module, getFlattenedTypes(sty), "moya.struct.flattened", sty->isPacked());
}

static Type* stripPointers(Type* ty, unsigned levels) {
  if(levels) {
    if(auto* pty = dyn_cast<PointerType>(ty))
      return stripPointers(pty->getElementType(), levels - 1);
    moya_error("Non-pointer type to strip: " << *ty);
  }
  return ty;
}

Type* stripPointers1(Type* ty) {
  return stripPointers(ty, 1);
}

Type* stripPointers2(Type* ty) {
  return stripPointers(ty, 2);
}

static Type* stripArrays(ArrayType* aty) {
  if(auto* bty = dyn_cast<ArrayType>(aty->getElementType()))
    return stripArrays(bty);
  return aty->getElementType();
}

Type* stripArrays(Type* ty) {
  if(auto* aty = dyn_cast<ArrayType>(ty))
    return stripArrays(aty);
  moya_error("Expected array type to strip. Got " << *ty);
  return ty;
}

StructType* createStruct(Module& module,
                         const Vector<Type*>& elements,
                         const std::string& name,
                         bool isPacked) {
  StructType* sty = StructType::create(
      module.getContext(), makeArrayRef(elements), name, isPacked);
  Metadata::setArtificial(sty, module);

  return sty;
}

PointerType* createPointer(Type* ety, uint64_t depth) {
  if(depth == 0)
    return nullptr;
  else if(depth == 1)
    return ety->getPointerTo();
  else
    return createPointer(ety->getPointerTo(), depth - 1);
}

static bool areIdentical(Type*, Type*, Set<Type*>&, Set<Type*>&);

static bool areIdentical(PointerType* p1,
                         PointerType* p2,
                         Set<Type*>& seen1,
                         Set<Type*>& seen2) {
  if(seen1.contains(p1))
    return true;
  return areIdentical(p1->getElementType(), p2->getElementType(), seen1, seen2);
}

static bool areIdentical(ArrayType* a1,
                         ArrayType* a2,
                         Set<Type*>& seen1,
                         Set<Type*>& seen2) {
  if(a1->getNumElements() != a2->getNumElements())
    return false;
  return areIdentical(a1->getElementType(), a2->getElementType(), seen1, seen2);
}

static bool areIdentical(StructType* s1,
                         StructType* s2,
                         Set<Type*>& seen1,
                         Set<Type*>& seen2) {
  if(s1->getNumElements() != s2->getNumElements())
    return false;
  for(unsigned i = 0; i < s1->getNumElements(); i++)
    if(not areIdentical(
           s1->getElementType(i), s2->getElementType(i), seen1, seen2))
      return false;
  return true;
}

static bool areIdentical(FunctionType* f1,
                         FunctionType* f2,
                         Set<Type*>& seen1,
                         Set<Type*>& seen2) {
  if(f1->getNumParams() != f2->getNumParams())
    return false;
  if(not areIdentical(f1->getReturnType(), f2->getReturnType(), seen1, seen2))
    return false;
  for(unsigned i = 0; i < f1->getNumParams(); i++)
    if(not areIdentical(f1->getParamType(i), f2->getParamType(i), seen1, seen2))
      return false;
  return true;
}

static bool
areIdentical(Type* t1, Type* t2, Set<Type*>& seen1, Set<Type*>& seen2) {
  if(t1 == t2)
    return true;
  if(seen1.contains(t1))
    return true;

  seen1.insert(t1);
  seen2.insert(t2);
  if(auto* i1 = dyn_cast<IntegerType>(t1)) {
    if(auto* i2 = dyn_cast<IntegerType>(t2))
      return i1->getBitWidth() == i2->getBitWidth();
    return false;
  } else if(t1->isFloatTy()) {
    if(t2->isFloatTy())
      return true;
    return false;
  } else if(t1->isDoubleTy()) {
    if(t2->isDoubleTy())
      return true;
    return false;
  } else if(t1->isX86_FP80Ty()) {
    if(t2->isX86_FP80Ty())
      return true;
    return false;
  } else if(auto* p1 = dyn_cast<PointerType>(t1)) {
    if(auto* p2 = dyn_cast<PointerType>(t2))
      return areIdentical(p1, p2, seen1, seen2);
    return false;
  } else if(auto* a1 = dyn_cast<ArrayType>(t1)) {
    if(auto* a2 = dyn_cast<ArrayType>(t2))
      return areIdentical(a1, a2, seen1, seen2);
    return false;
  } else if(auto* s1 = dyn_cast<StructType>(t1)) {
    if(auto* s2 = dyn_cast<StructType>(t2))
      return areIdentical(s1, s2, seen1, seen2);
    return false;
  } else if(auto* f1 = dyn_cast<FunctionType>(t1)) {
    if(auto* f2 = dyn_cast<FunctionType>(t2))
      return areIdentical(f1, f2, seen1, seen2);
    return false;
  }

  moya_error("Cannot check if types are identical: " << *t1 << ", " << *t2);
  return false;
}

bool areIdentical(Type* t1, Type* t2) {
  if(t1 == t2)
    return true;

  Set<Type*> seen1, seen2;
  return areIdentical(t1, t2, seen1, seen2);
}

Vector<StructType*> getRecursiveStructs(const Module& module,
                                        bool ignoreAnalysisTypes) {
  Graph<StructType*> graph;
  for(StructType* sty : module.getIdentifiedStructTypes()) {
    if(ignoreAnalysisTypes and Metadata::hasArtificial(sty, module))
      continue;

    graph.addNode(sty);
    for(Type* ty : sty->elements())
      if(auto* tty = dyn_cast<StructType>(getInnermost(ty)))
        graph.addEdge(sty, tty);
  }

  Vector<StructType*> recursive;
  const Graph<StructType*>::Components sccs = graph.getSCCs();
  for(size_t i = 0; i < sccs.size(); i++) {
    const Graph<StructType*>::Component& scc = sccs.at(i);
    // if the SCC has more than one struct, then they will definitely be
    // mutually recursive. If there is only one struct in the SCC, then it may
    // still be self-recursive
    if(scc.size() > 1) {
      for(StructType* sty : sccs.at(i))
        recursive.push_back(sty);
    } else {
      StructType* sty = *scc.begin();
      for(Type* ty : sty->elements())
        if(auto* tty = dyn_cast<StructType>(getInnermost(ty)))
          if(sty == tty)
            recursive.push_back(sty);
    }
  }

  return recursive;
}

} // namespace LLVMUtils

} // namespace moya
