#ifndef MOYA_COMMON_LANG_CONFIG_C_H
#define MOYA_COMMON_LANG_CONFIG_C_H

#include <string>

class LangConfigC {
public:
  static bool isValidSourceFile(const std::string&);
};

#endif // MOYA_COMMON_LANG_CONFIG_C_H
