#include "DataLayout.h"
#include "Diagnostics.h"
#include "LLVMUtils.h"

using llvm::dyn_cast;

namespace moya {

DataLayout::DataLayout() : dl(llvm::StringRef()) {
  ;
}

DataLayout::DataLayout(const llvm::Module& module) : dl(llvm::StringRef()) {
  init(module);
}

void DataLayout::reset(const llvm::Module& module) {
  init(module);
}

void DataLayout::reset(llvm::StructType* sty) {
  if(structLayouts.contains(sty))
    structLayouts.erase(sty);
  add(sty);
}

void DataLayout::add(llvm::StructType* sty) {
  structLayouts.emplace(std::make_pair(sty, moya::StructLayout(sty, dl)));
}

void DataLayout::init(const llvm::Module& module) {
  structLayouts.clear();
  dl.reset(module.getDataLayout().getStringRepresentation());
}

const std::string& DataLayout::getStringRepresentation() const {
  return dl.getStringRepresentation();
}

unsigned DataLayout::getStackAlignment() const {
  return dl.getStackAlignment();
}

unsigned DataLayout::getPointerSize() const {
  return dl.getPointerSize();
}

uint64_t DataLayout::getTypeSize(llvm::Type* type) const {
  if(auto* sty = dyn_cast<llvm::StructType>(type))
    if(sty->isOpaque())
      return 1;
    else
      return dl.getTypeSizeInBits(sty) / 8;
  else if(type->isFunctionTy() or type->isMetadataTy())
    return 1;
  else if(not type->isSized())
    moya_error("Cannot get size for type: " << *type);
  else
    return dl.getTypeSizeInBits(type) / 8;
}

uint64_t DataLayout::getTypeStoreSize(llvm::Type* type) const {
  if(auto* sty = dyn_cast<llvm::StructType>(type))
    if(sty->isOpaque())
      return 1;
    else
      return dl.getTypeStoreSize(sty);
  else if(type->isFunctionTy() or type->isMetadataTy())
    return 1;
  else if(not type->isSized())
    moya_error("Cannot get store size for type: " << *type);
  else
    return dl.getTypeStoreSize(type);
}

uint64_t DataLayout::getTypeAllocSize(llvm::Type* type) const {
  if(auto* sty = dyn_cast<llvm::StructType>(type))
    if(sty->isOpaque())
      return 1;
    else
      return dl.getTypeAllocSize(sty);
  else if(type->isFunctionTy() or type->isMetadataTy())
    return 1;
  else if(not type->isSized())
    moya_error("Cannot get alloc size for type: " << *type);
  else
    return dl.getTypeAllocSize(type);
}

uint64_t DataLayout::getTypeAlignment(llvm::Type* type) const {
  llvm::Type* pi8 = llvm::Type::getInt8PtrTy(type->getContext());
  if(type->isFunctionTy() or type->isMetadataTy())
    return dl.getABITypeAlignment(pi8);
  else if(auto* sty = dyn_cast<llvm::StructType>(type))
    if(sty->isOpaque())
      return dl.getABITypeAlignment(pi8);
    else
      return dl.getABITypeAlignment(type);
  else
    return dl.getABITypeAlignment(type);
}

int64_t DataLayout::getOffsetInType(
    llvm::Type* type,
    const moya::Vector<const llvm::Value*>& indices) const {
  llvm::Value** data = const_cast<llvm::Value**>(indices.data());
  llvm::ArrayRef<llvm::Value*> ref(data, indices.size());
  return dl.getIndexedOffsetInType(type, ref);
}

int64_t DataLayout::getOffsetInGEP(llvm::GetElementPtrInst* gep) const {
  llvm::APInt coff(dl.getPointerSize() * 8, 0, false);
  if(gep->accumulateConstantOffset(dl, coff))
    return coff.getLimitedValue();

  moya_error("Cannot get constant offset for GEP: " << *gep);
  return 0;
}

const moya::StructLayout&
DataLayout::getStructLayout(llvm::StructType* sty) const {
  if(not structLayouts.contains(sty))
    moya_error("Layout not found for struct: " << moya::str(*sty));
  return structLayouts.at(sty);
}

const moya::StructLayout& DataLayout::getStructLayout(llvm::StructType* sty) {
  if(not structLayouts.contains(sty))
    add(sty);
  return structLayouts.at(sty);
}

} // namespace moya
