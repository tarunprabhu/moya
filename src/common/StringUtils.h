#ifndef COMMON_MOYA_STRING_UTILS_H
#define COMMON_MOYA_STRING_UTILS_H

#include "Vector.h"

#include <string>

namespace moya {

// This is a class for generic string operations which are not currently
// available in either C++ or in LLVM's StringRef. It would be perhaps be
// better to create Moya's own string class which inherits from the std::string,
// but for now, this will do
namespace StringUtils {

std::string lstrip(const std::string&);
std::string rstrip(const std::string&);
std::string strip(const std::string&);

std::string lower(const std::string&);

Vector<std::string> split(const std::string&, char, bool = false);
Vector<std::string> split(const std::string&, const std::string&, bool = false);
Vector<std::string>
split(const std::string&, char, unsigned long, bool = false);
Vector<std::string>
split(const std::string&, char, unsigned long, unsigned long, bool = false);

std::pair<std::string, std::string> lpartition(const std::string&,
                                               const std::string&);

std::pair<std::string, std::string> rpartition(const std::string&,
                                               const std::string&);

std::string replace(const std::string&,
                    std::string::size_type,
                    std::string::size_type,
                    const std::string&);
std::string replace(const std::string&, const std::string&, const std::string&);

bool contains(const std::string&, const std::string&);

bool startswith(const std::string&, const std::string&);
bool endswith(const std::string&, const std::string&);

std::string remove(const std::string&, const std::string&);

std::string reverse(const std::string&);

} // namespace StringUtils

} // namespace moya

#endif // COMMON_MOYA_STRING_UTILS_H
