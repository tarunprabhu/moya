#ifndef MOYA_COMMON_DESERIALIZER_H
#define MOYA_COMMON_DESERIALIZER_H

#include "Parse.h"
#include "Stack.h"

#include <fstream>
#include <iostream>
#include <memory>
#include <string>

namespace moya {

class Deserializer {
protected:
  std::ifstream in;
  Stack<std::istream::pos_type> checkpoints;

protected:
  void error();
  void error(const std::string&);
  void error(const std::string&, const std::string&);

  // Strings and bools are handled as a special case
  void read(bool&);
  void read(std::string&);

  // These read functions check against an expected value
  void read(const std::string&, bool = true);
  void read(const char&);

  bool peek(const std::string&);
  bool peek(const char&);
  void skipWhitespace();

protected:
  template <typename T,
            typename... ArgsT,
            typename std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
  void read(T& val, ArgsT&&...) {
    skipWhitespace();
    in >> val;
  }

  template <typename T,
            typename... ArgsT,
            typename std::enable_if_t<std::is_enum<T>::value, int> = 0>
  void read(T& val, ArgsT&&...) {
    int i;
    skipWhitespace();
    in >> i;
    val = static_cast<T>(i);
  }

  template <typename T,
            typename... ArgsT,
            typename std::enable_if_t<std::is_class<T>::value, int> = 0>
  void read(T& val, ArgsT&&... args) {
    skipWhitespace();
    val.deserialize(*this, args...);
  }

  template <typename T, typename... ArgsT>
  void read(std::unique_ptr<T>& ptr, ArgsT&&... args) {
    T* obj = new T();
    read(*obj, args...);
    ptr.reset(obj);
  }

  template <typename T,
            typename std::enable_if_t<std::is_arithmetic<T>::value
                                          || std::is_enum<T>::value,
                                      int> = 0>
  void readKey(T& key) {
    skipWhitespace();
    std::string s;
    read(s);
    key = parse<T>(s);
  }

  void readKey(const std::string&);
  void readKey(std::string&);

public:
  Deserializer() = default;
  virtual ~Deserializer() = default;

  void initialize(const std::string&);
  void finalize();

  bool mapEmpty();
  bool arrEmpty();

  void mapStart(const std::string& = "");
  void mapEnd();
  void arrStart(const std::string& = "");
  void arrEnd();
  void pairStart(const std::string& = "");
  void pairEnd();

  // We may need to look-ahead when deserializing objects with virtual
  // methods. This adds a checkpoint so we can revert to actually read
  void pushCheckPoint();
  void popCheckPoint();

  template <typename V, typename... ArgsT>
  void deserialize(const std::string& key, V& val, ArgsT&&... args) {
    read(key);
    read(':');
    read(val, args...);
  }

  template <typename V, typename... ArgsT>
  void deserialize(const char* key, V& val, ArgsT&&... args) {
    const std::string s(key);
    deserialize(s, val, args...);
  }

  template <typename K, typename V, typename... ArgsT>
  void deserialize(K& key, V& val, ArgsT&&... args) {
    readKey(key);
    read(':');
    read(val, args...);
  }

  template <typename T, typename... ArgsT>
  void deserialize(T& val, ArgsT&&... args) {
    read(val, args...);
  }

  template <typename T, typename... ArgsT>
  void deserialize(const T& val, ArgsT&&... args) {
    read(val, args...);
  }
};

} // namespace moya

#endif // MOYA_COMMON_DESERIALIZER_H
