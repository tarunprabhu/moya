#ifndef MOYA_COMMON_STORE_CELL_H
#define MOYA_COMMON_STORE_CELL_H

#include "APITypes.h"
#include "AnalysisStatus.h"
#include "CellFlags.h"
#include "Contents.h"
#include "Status.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Type.h>

namespace moya {

class Store;
class Serializer;

class StoreCell {
public:
  using Address = ContentAddress::Raw;
  using Funcs = Set<const llvm::Function*>;
  using Insts = Set<const llvm::Instruction*>;

private:
  const Store& store;
  Address address;

  // The type of the cell itself. This will always be a primitive or pointer
  // type
  llvm::Type* type;

  // The size in bytes of the type (DataLayout.getTypeAllocSize)
  uint64_t size;

  Contents contents;

  CellFlags flags;

  std::tuple<Funcs, Insts> readers;
  std::tuple<Funcs, Insts> writers;
  std::tuple<Funcs, Insts> allocators;
  std::tuple<Funcs, Insts> deallocators;

private:
  bool addAccessor(std::tuple<Funcs, Insts>&, const llvm::Instruction*);
  bool addAccessor(std::tuple<Funcs, Insts>&, const llvm::Function*);
  bool addAccessors(std::tuple<Funcs, Insts>&, const std::tuple<Funcs, Insts>&);
  bool hasAccessors(const std::tuple<Funcs, Insts>&) const;

  bool isAccessedByFunction(MoyaID, const Set<const llvm::Function*>&) const;

  std::string str(int, const Set<const llvm::Function*>&) const;
  std::string str(int, const Set<const llvm::Instruction*>&) const;
  std::string str(int, const Contents&) const;

  void serialize(const std::string&,
                 const std::tuple<Funcs, Insts>&,
                 Serializer&) const;

public:
  StoreCell(Address, llvm::Type*, uint64_t, const CellFlags&, const Store&);

  void addFlags(const CellFlags&);
  const CellFlags& getFlags() const;

  Address getAddress() const;
  llvm::Type* getType() const;
  uint64_t getSize() const;
  bool isEmpty() const;

  template <typename T>
  const Set<const T*>& getReaders() const;
  template <typename T>
  const Set<const T*>& getWriters() const;
  template <typename T>
  const Set<const T*>& getAllocators() const;
  template <typename T>
  const Set<const T*>& getDeallocators() const;

  bool addReader(const llvm::Instruction*);
  bool addWriter(const llvm::Instruction*);
  bool addAllocator(const llvm::Instruction*);
  bool addDeallocator(const llvm::Instruction*);

  bool addReader(const llvm::Function*);
  bool addWriter(const llvm::Function*);
  bool addAllocator(const llvm::Function*);
  bool addDeallocator(const llvm::Function*);

  const Contents& read() const;
  const Contents& read(const llvm::Function*, AnalysisStatus&);
  const Contents& read(const llvm::Instruction*, AnalysisStatus&);

  void write(const Content*);
  void write(const Content*, const llvm::Instruction*, AnalysisStatus&);

  bool isRead() const;
  bool isWritten() const;
  bool isAllocated() const;
  bool isDeallocated() const;

  bool isReadByFunction(MoyaID) const;
  bool isWrittenByFunction(MoyaID) const;
  bool isAllocatedByFunction(MoyaID) const;
  bool isDeallocatedByFunction(MoyaID) const;
  bool isUsedByFunction(MoyaID) const;

  bool hasConstantContents() const;

  void merge(const StoreCell&, bool = false);

  std::string str(int = 0) const;
  void serialize(Serializer&) const;
};

} // namespace moya

#endif // MOYA_COMMON_STORE_CELL_H
