#ifndef MOYA_COMMON_DEBUG_INFO_UTILS_H
#define MOYA_COMMON_DEBUG_INFO_UTILS_H

#include "Location.h"

#include <llvm/IR/DebugInfoMetadata.h>
#include <llvm/IR/DebugLoc.h>
#include <llvm/IR/Module.h>

namespace moya {

namespace LLVMUtils {

Location getLocation(const llvm::DebugLoc&);
Location getLocation(const llvm::DILocation*);
Location getLocation(const llvm::DISubprogram*);

llvm::Type* getType(const llvm::DIType*, llvm::Module&);

} // namespace LLVMUtils

} // namespace moya

#endif // MOYA_COMMON_DEBUG_INFO_UTILS_H
