#include "Parse.h"
#include "Diagnostics.h"
#include "LLVMUtils.h"
#include "StringUtils.h"
#include "common/Deque.h"

#include <llvm/IR/GlobalValue.h>

namespace moya {

static size_t skipWhitespace(const std::string& s, size_t pos) {
  while(s[pos] == ' ')
    pos++;
  return pos;
}

static int toDigit(char c) {
  return static_cast<int>(c) - static_cast<int>('0');
}

static llvm::Type* parsePrimitive(const std::string& s, llvm::Module& module) {
  llvm::LLVMContext& context = module.getContext();

  if(s == "x86_fp80") {
    return llvm::Type::getX86_FP80Ty(context);
  } else if(s == "double") {
    return llvm::Type::getDoubleTy(context);
  } else if(s == "float") {
    return llvm::Type::getFloatTy(context);
  } else if(s == "i64") {
    return llvm::Type::getInt64Ty(context);
  } else if(s == "i32") {
    return llvm::Type::getInt32Ty(context);
  } else if(s == "i8") {
    return llvm::Type::getInt8Ty(context);
  } else if(s == "void") {
    return llvm::Type::getVoidTy(context);
  } else if(s == "i16") {
    return llvm::Type::getInt16Ty(context);
  } else {
    moya_error("Cannot parse type: " << s);
  }

  return nullptr;
}

static llvm::Type* parsePointer(const std::string& s, llvm::Module& module) {
  return parse(s.substr(0, s.length() - 1), module)->getPointerTo();
}

static llvm::Type* parseArray(const std::string& s, llvm::Module& module) {
  size_t pos = 1;

  unsigned elems = 0;
  for(pos = 1; std::isdigit(s[pos]); pos++)
    elems = elems * 10 + toDigit(s[pos]);

  pos = skipWhitespace(s, pos);
  moya_error_if(s[pos] != 'x', "Expected 'x' when parsing array type");
  pos = skipWhitespace(s, pos + 1);
  moya_error_if(s.back() != ']', "Expected ']' when parsing array type");

  llvm::Type* ety = parse(s.substr(pos, s.length() - pos - 1), module);
  return llvm::ArrayType::get(ety, elems);
}

static llvm::Type* parseStructLiteral(const std::string& s,
                                      llvm::Module& module) {
  moya_error_if(s[1] != '{',
                "Error parsing struct literal\n"
                    << "      In: " << s << "\n"
                    << "  Expect: {\n"
                    << "   Index: " << 1 << "\n"
                    << "     Got: " << s[1]);
  moya_error_if(s[s.length() - 2] != '}',
                "Error parsing struct literal\n"
                    << "      In: " << s << "\n"
                    << "  Expect: }\n"
                    << "   Index: " << (s.length() - 2) << "\n"
                    << "     Got: " << s[s.length() - 2]);
  moya_error_if(s.back() != '>',
                "Error parsing struct literal\n"
                    << "      In: " << s << "\n"
                    << "  Expect: >\n"
                    << "   Index: " << (s.length() - 1) << "\n"
                    << "     Got: " << s.back());

  // Strip the first 2 and the last two characters. These will be '<{' and '}>'
  std::string elements = s.substr(2, s.length() - 4);

  Vector<llvm::Type*> types;
  for(const std::string& t : StringUtils::split(elements, ','))
    types.push_back(parse(t.substr(skipWhitespace(t, 0)), module));

  llvm::StructType* sty = llvm::StructType::get(
      module.getContext(), LLVMUtils::makeArrayRef(types), true);

  return sty;
}

static llvm::Type* parseStruct(const std::string& s, llvm::Module& module) {
  return module.getTypeByName(s.substr(1));
}

static llvm::Type* parseFunction(const std::string& s, llvm::Module& module) {
  // The return type of the function type could be another function type
  // To find the start of the parameters list, start looking from the back
  unsigned pos = s.length() - 1;
  unsigned depth = 0;
  Deque<std::string> params;
  std::stringstream ss;

  do {
    char c = s[pos];
    if(c == ')') {
      depth += 1;
    } else if(c == '(') {
      depth -= 1;
    } else if(c == ',') {
      // If we see a comma, we could still be inside a function type that is
      // a parameter of the current one
      if(depth == 1) {
        params.push_front(StringUtils::reverse(StringUtils::strip(ss.str())));
        ss.str("");
      } else {
        ss << c;
      }
    } else {
      ss << c;
    }
    pos -= 1;
  } while(depth > 0);
  params.push_front(StringUtils::reverse(StringUtils::strip(ss.str())));

  bool varArg = params.back() == "...";
  Vector<llvm::Type*> types;
  for(const std::string& param : params)
    types.push_back(parse(param, module));
  llvm::Type* ret = parse(s.substr(0, pos), module);

  return llvm::FunctionType::get(ret, LLVMUtils::makeArrayRef(types), varArg);
}

llvm::Type* parse(const std::string& s, llvm::Module& module) {
  if(s.back() == '*')
    return parsePointer(s, module);
  else if(s.front() == '[')
    return parseArray(s, module);
  else if(s.front() == '<')
    return parseStructLiteral(s, module);
  else if(llvm::Type* strct = parseStruct(s, module))
    return strct;
  else if((s.find("(") != std::string::npos)
          and (s.rfind(")") == s.length() - 1))
    return parseFunction(s, module);
  else
    return parsePrimitive(s, module);

  return nullptr;
}

std::string parse(const std::string& s) {
  return s;
}

template <>
llvm::GlobalValue::LinkageTypes parse(const std::string& s) {
  int64_t linkage;
  std::stringstream ss(s);
  ss >> linkage;
  return static_cast<llvm::GlobalValue::LinkageTypes>(linkage);
}

} // namespace moya
