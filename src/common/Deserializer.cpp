#include "Deserializer.h"
#include "Diagnostics.h"
#include "Stringify.h"

#include <llvm/Support/FileSystem.h>

#include <sstream>
#include <unistd.h>

namespace moya {

void Deserializer::initialize(const std::string& file) {
  in.open(file, std::ios::in);
  if(not in.is_open())
    moya_error("Could not open file to deserialize: " << file);
}

void Deserializer::finalize() {
  if(not in.is_open())
    moya_error("Finalizing uninitialized deserializer");
  in.close();
}

void Deserializer::error() {
  moya_error("Unspecified error during deserialize() @ " << in.tellg());
}

void Deserializer::error(const std::string& msg) {
  std::stringstream ss;
  ss << "deserializer() error. Got " << msg << " @ " << in.tellg();
  moya_error(ss.str());
}

void Deserializer::error(const std::string& got, const std::string& expected) {
  std::stringstream ss;
  ss << "deserializer() error. Expected \'" << expected << "\'. Got \'" << got
     << "\' @ " << in.tellg();
  llvm::errs() << getBacktrace() << "\n";
  moya_error(ss.str());
}

void Deserializer::skipWhitespace() {
  while(true) {
    if(in.eof())
      error("EOF", " ");
    if(in.bad())
      error();
    char c = in.peek();
    if(std::isspace(c))
      in.get(c);
    else if(c == ',')
      in.get(c);
    else
      return;
  }
}

void Deserializer::read(bool& b) {
  skipWhitespace();
  if(peek("true")) {
    read("true", false);
    b = true;
  } else if(peek("false")) {
    read("false", false);
    b = false;
  } else {
    moya_error("Unexpected serialization of bool");
  }
}

void Deserializer::read(const char& expected) {
  skipWhitespace();
  if(in.eof())
    error("EOF");
  if(in.bad())
    error();
  char c;
  read(c);
  if(c != expected)
    error(str(c), str(expected));
}

void Deserializer::read(const std::string& expected, bool str) {
  std::stringstream ss;
  if(str) {
    std::string s;
    read(s);
    ss << s;
  } else {
    for(size_t i = 0; i < expected.length(); i++) {
      char c;
      in.get(c);
      if(not in.good())
        error("Broken stream", ss.str());
      else if(c != expected[i])
        error("Unexpected character", moya::str(c));
      else
        ss << c;
    }
  }
  if(ss.str() != expected)
    error(ss.str(), expected);
}

void Deserializer::read(std::string& s) {
  std::stringstream ss;
  bool escape = false;
  bool started = false;

  skipWhitespace();
  while(true) {
    char c;
    in.get(c);
    if(in.eof())
      error("EOF", "\"");
    if(in.bad())
      error();
    if(not started) {
      if(c == '"')
        started = true;
      else
        error(str(c), "\"");
    } else {
      if(c == '\\')
        escape = not escape;
      else if(c == '"')
        if(escape)
          escape = false;
        else
          break;
      else
        escape = false;
      ss << c;
    }
  }

  s = ss.str();
}

void Deserializer::readKey(const std::string& key) {
  read(key);
}

void Deserializer::readKey(std::string& key) {
  read(key);
}

bool Deserializer::peek(const char& expected) {
  std::istream::pos_type pos = in.tellg();

  bool found = false;
  while(in.good()) {
    char c;
    in.get(c);
    if(not std::isspace(c)) {
      if(c == expected)
        found = true;
      break;
    }
  }
  // Reset the pointer
  in.seekg(pos, std::ios::beg);
  return found;
}

bool Deserializer::peek(const std::string& expected) {
  std::istream::pos_type pos = in.tellg();

  bool found = true;
  for(size_t i = 0; (i < expected.size()) and found; i++) {
    char c;
    in >> c;
    if(not in.good())
      found = false;
    else if(c != expected[i])
      found = false;
  }

  // Reset the pointer
  in.seekg(pos, std::ios::beg);
  return found;
}

void Deserializer::mapStart(const std::string& label) {
  if(label.length()) {
    read(label);
    read(':');
  }
  read('{');
}

void Deserializer::mapEnd() {
  read('}');
}

bool Deserializer::mapEmpty() {
  return peek('}');
}

void Deserializer::arrStart(const std::string& label) {
  if(label.length()) {
    read(label);
    read(':');
  }
  read('[');
}

void Deserializer::arrEnd() {
  read(']');
}

bool Deserializer::arrEmpty() {
  return peek(']');
}

void Deserializer::pairStart(const std::string& label) {
  if(label.length()) {
    read(label);
    read(':');
  }
  read('<');
}

void Deserializer::pairEnd() {
  read('>');
}

void Deserializer::pushCheckPoint() {
  checkpoints.push(in.tellg());
}

void Deserializer::popCheckPoint() {
  in.seekg(checkpoints.pop());
}

} // namespace moya
