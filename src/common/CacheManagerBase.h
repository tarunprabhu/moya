#ifndef MOYA_COMMON_CACHE_MANAGER_BASE_H
#define MOYA_COMMON_CACHE_MANAGER_BASE_H

#include <string>

namespace moya {

class CacheManagerBase {
private:
  std::string cacheDir;
  std::string prefix;
  std::string sig;
  std::string time;
  std::string path;
  bool enabled;

protected:
  CacheManagerBase(const std::string&,
                   const std::string& = "",
                   const std::string& = "",
                   const std::string& = "",
                   const std::string& = "");

  std::string getFilename(const std::string&, const std::string&) const;
  std::string
  getFilename(const std::string&, const std::string&, const std::string&) const;

  void setCacheDir(const std::string&);

public:
  // This constructor will be called by Talyn because we will not know the
  // executable name until the conf object has been initialized
  CacheManagerBase() = default;
  virtual ~CacheManagerBase() = default;

  void setPrefix(const std::string&);
  void setSignature(const std::string&);
  void setTime(const std::string&);
  void setPath(const std::string&);

  bool isEnabled() const;
  const std::string& getPrefix() const;
  const std::string& getSignature() const;
  const std::string& getTime() const;
  const std::string& getPath() const;
  const std::string& getDirectory() const;
};

} // namespace moya

#endif // MOYA_COMMON_CACHE_MANAGER_BASE_H
