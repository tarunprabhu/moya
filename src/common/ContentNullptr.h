#ifndef MOYA_COMMON_CONTENT_NULLPTR_H
#define MOYA_COMMON_CONTENT_NULLPTR_H

#include "Content.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>

namespace moya {

// We treat ConstantPointerNull as a separate content object because we need
// to ensure that we don't try to dereference it when it is encountered. We
// also want to have it around because there are error checks when constructing
// objects which can fail if we just ignore nulls in the code
class ContentNullptr : public Content {
protected:
  llvm::ConstantPointerNull* cnull;

public:
  ContentNullptr(llvm::ConstantPointerNull* = nullptr);
  virtual ~ContentNullptr() = default;

  llvm::ConstantPointerNull* getNullptr() const;

  virtual std::string str() const;
  virtual void serialize(Serializer&) const;

public:
  static bool classof(const Content*);
};

} // namespace moya

#endif // MOYA_COMMON_CONTENT_NULLPTR_H
