#ifndef MOYA_COMMON_CONTENT_CODE_H
#define MOYA_COMMON_CONTENT_CODE_H

#include "Content.h"

#include <llvm/IR/Function.h>

namespace moya {

// This mostly used to represent functions, but could also perhaps be used for
// JIT'ted code or something like that
class ContentCode : public Content {
protected:
  const llvm::Function* function;

public:
  ContentCode(const llvm::Function*);
  virtual ~ContentCode() = default;

  const llvm::Function* getFunction() const;

  virtual std::string str(bool) const;
  virtual std::string str() const;
  virtual void serialize(Serializer&) const;

public:
  static bool classof(const Content*);
};

} // namespace moya

#endif // MOYA_COMMON_CONTENT_CODE_H
