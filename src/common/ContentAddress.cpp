#include "ContentAddress.h"
#include "JSONUtils.h"

#include <sstream>

using namespace llvm;

namespace moya {

ContentAddress::ContentAddress(ContentAddress::Raw addr)
    : Content(Content::Kind::Address), addr(addr) {
  ;
}

ContentAddress::Raw ContentAddress::getRaw() const {
  return addr;
}

bool ContentAddress::classof(const Content* c) {
  return c->getKind() == Content::Kind::Address;
}

std::string ContentAddress::str() const {
  std::stringstream ss;

  if(getRaw() == ContentAddress::null)
    ss << "CNullptr()";
  else
    ss << "CAddress(" << getRaw() << ")";

  return ss.str();
}

void ContentAddress::serialize(Serializer& s) const {
  s.mapStart();
  s.add("_class", "Pointer");
  if(getRaw() == ContentAddress::null)
    s.add("target", getRaw());
  else
    s.add("target", JSON::pointer(this));
  s.mapEnd();
}

} // namespace moya
