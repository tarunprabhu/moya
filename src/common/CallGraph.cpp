#include "CallGraph.h"
#include "Config.h"
#include "Content.h"
#include "Diagnostics.h"
#include "Dotifier.h"
#include "Environment.h"
#include "Formatting.h"
#include "Hexify.h"
#include "JSONUtils.h"
#include "LLVMUtils.h"
#include "Metadata.h"
#include "Serializer.h"
#include "Store.h"
#include "Stringify.h"

#include <sstream>

#include <llvm/IR/Constants.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/InstrTypes.h>
#include <llvm/IR/Instructions.h>

using llvm::cast;
using llvm::dyn_cast;
using llvm::isa;

namespace moya {

CallGraph::CallGraph(const llvm::Module& module)
    : module(module), region(nullptr), root(nullptr) {
  ;
}

CallGraph::CallGraph(const Region& region, const llvm::Module& module)
    : module(module), region(&region), root(nullptr) {
  ;
}

llvm::const_inst_iterator
CallGraph::getIterator(const llvm::Instruction* inst) {
  const llvm::Function* f = LLVMUtils::getFunction(inst);
  llvm::const_inst_iterator it;
  for(it = inst_begin(f); &*it != inst; it++)
    ;
  return it;
}

void CallGraph::getRegionBoundaryArgs(const llvm::Value* val,
                                      Vector<const llvm::Use*>& calls) const {
  for(const llvm::Use& u : val->uses()) {
    if(const auto* call = dyn_cast<llvm::CallInst>(u.getUser())) {
      if(call->getCalledValue() == val)
        calls.push_back(&call->getArgOperandUse(0));
    } else if(const auto* invoke = dyn_cast<llvm::InvokeInst>(u.getUser())) {
      if(invoke->getCalledValue() == val)
        calls.push_back(&call->getArgOperandUse(0));
    } else if(const auto* cst = dyn_cast<llvm::CastInst>(u.getUser())) {
      getRegionBoundaryArgs(cst, calls);
    }
  }
}

Vector<const llvm::Use*>
CallGraph::getRegionBoundaryArgs(const llvm::Function* f) const {
  Vector<const llvm::Use*> calls;

  getRegionBoundaryArgs(f, calls);

  return calls;
}

const llvm::Instruction*
CallGraph::getRegionBoundaryCall(const llvm::Function* f) const {
  for(const llvm::Use* use : getRegionBoundaryArgs(f)) {
    if(const auto* cint = dyn_cast<llvm::ConstantInt>(use->get())) {
      if(cint->getLimitedValue() == region->getID())
        return dyn_cast<llvm::Instruction>(use->getUser());
    } else if(auto* g = dyn_cast<llvm::GlobalVariable>(use->get())) {
      if(g->hasInitializer()) {
        if(auto* cint = LLVMUtils::getInitializerAs<llvm::ConstantInt>(g)) {
          if(cint->getLimitedValue() == region->getID())
            return dyn_cast<llvm::Instruction>(use->getUser());
        } else {
          moya_error("Unexpected global initializer: " << *g);
        }
      } else {
        moya_error("Global parameter must have initializer: " << *g);
      }
    } else {
      moya_error("Unexpected argument type in call to: " << f->getName());
    }
  }
  return nullptr;
}

const llvm::Instruction* CallGraph::getEnterRegionCall() const {
  if(const llvm::Function* f = LLVMUtils::getEnterRegionFunction(module))
    return getRegionBoundaryCall(f);
  return nullptr;
}

const llvm::Instruction* CallGraph::getExitRegionCall() const {
  if(const llvm::Function* f = LLVMUtils::getExitRegionFunction(module))
    return getRegionBoundaryCall(f);
  return nullptr;
}

void CallGraph::addReachableCall(const llvm::Instruction* inst,
                                 Queue<const llvm::BasicBlock*>& wl,
                                 Set<const llvm::BasicBlock*>& done,
                                 Vector<const llvm::Instruction*>& ret) {
  if(isa<llvm::CallInst>(inst) or isa<llvm::InvokeInst>(inst))
    ret.push_back(inst);
  if(inst->isTerminator())
    for(unsigned i = 0; i < inst->getNumSuccessors(); i++)
      if(not done.contains(inst->getParent()))
        wl.push(inst->getSuccessor(i));
}

Vector<const llvm::Instruction*>
CallGraph::getReachableCalls(llvm::const_inst_iterator begin,
                             llvm::const_inst_iterator end) {
  Vector<const llvm::Instruction*> ret;
  Queue<const llvm::BasicBlock*> wl;
  Set<const llvm::BasicBlock*> done;
  llvm::const_inst_iterator it;
  for(it = begin; it != end; it++)
    addReachableCall(&*it, wl, done, ret);
  addReachableCall(&*it, wl, done, ret);

  while(wl.size()) {
    const llvm::BasicBlock* bb = wl.front();
    if(not done.contains(bb)) {
      for(llvm::BasicBlock::const_iterator it = bb->begin(); it != bb->end();
          it++)
        addReachableCall(&*it, wl, done, ret);
      done.insert(bb);
    }
    wl.pop();
  }
  return ret;
}

void CallGraph::processInstruction(const llvm::Instruction* inst,
                                   const llvm::Function* caller,
                                   const Environment& env,
                                   Queue<const llvm::Function*>& wl) {
  if(not cg.contains(caller))
    cg[caller] = Callees();
  for(const llvm::Function* callee : env.getCallees(inst)) {
    wl.push(callee);
    cg[caller].insert(callee);
  }
}

bool CallGraph::isConstructed() const {
  return cg.contains(root);
}

bool CallGraph::canConstruct() const {
  if(region)
    return getEnterRegionCall();
  return true;
}

CallGraph& CallGraph::construct(const Environment& env, const Store&) {
  Queue<const llvm::Function*> wl;
  if(region) {
    llvm::const_inst_iterator begin = getIterator(getEnterRegionCall());
    llvm::const_inst_iterator end = getIterator(getExitRegionCall());
    for(auto i = begin; i != end; i++)
      rootInsts.insert(&*i);
    for(const llvm::Instruction* inst : getReachableCalls(begin, end))
      processInstruction(inst, root, env, wl);
  } else {
    root = LLVMUtils::getMainFunction(module);
    wl.push(root);
  }

  Set<const llvm::Function*> done;
  while(wl.size()) {
    const llvm::Function* f = wl.front();
    if(done.insert(f))
      for(auto i = inst_begin(f); i != inst_end(f); i++)
        processInstruction(&*i, f, env, wl);
    wl.pop();
  }

  closure = cg;
  bool changed = false;
  do {
    changed = false;
    for(auto& it : closure)
      for(const llvm::Function* f : closure[it.first])
        for(const llvm::Function* g : closure[f])
          changed |= closure[it.first].insert(g);
  } while(changed);

  return *this;
}

bool CallGraph::isInProperClosureOf(const llvm::Function* callee,
                                    const llvm::Function* caller) const {
  if(closure.contains(caller))
    return closure.at(caller).contains(callee);
  return false;
}

bool CallGraph::isInClosureOf(const llvm::Function* callee,
                              const llvm::Function* caller) const {
  return isInProperClosureOf(callee, caller) or (callee == caller);
}

bool CallGraph::isInProperClosureOfRoot(const llvm::Function* callee) const {
  return isInProperClosureOf(callee, root);
}

bool CallGraph::isInClosureOfRoot(const llvm::Function* callee) const {
  return isInClosureOf(callee, root);
}

bool CallGraph::isInProperClosureOfRoot(const llvm::Instruction* inst) const {
  if(const llvm::Function* f = LLVMUtils::getFunction(inst))
    return isInProperClosureOf(f, root);
  else
    moya_error("No parent of instruction: " << *inst);
}

bool CallGraph::isInClosureOfRoot(const llvm::Instruction* inst) const {
  return isInProperClosureOfRoot(inst) or rootInsts.contains(inst);
}

std::string CallGraph::getFunctionName(const llvm::Function* f) const {
  std::stringstream ss;
  if(f)
    ss << f->getName().str();
  else
    ss << "__moya_wrapper_" << region->getName();
  return ss.str();
}

std::string CallGraph::getRegionName() const {
  if(region)
    return moya::str(region->getID());
  else
    return "0";
}

std::string CallGraph::str(const CallGraphT& cg, bool fullnames) const {
  std::stringstream ss;
  if(fullnames) {
    ss << "region." << region << " {\n";
    for(auto& kv : cg) {
      ss << "  " << getFunctionName(kv.first) << " => { ";
      for(const llvm::Function* callee : kv.second)
        ss << getFunctionName(callee) << " ";
      ss << "}" << std::endl;
    }
  } else {
    ss << "region." << region << " {\n";
    for(auto& kv : cg)
      ss << "  " << moya::str(kv.first) << " => " << moya::str(kv.second)
         << "\n";
  }
  ss << "}";
  return ss.str();
}

std::string CallGraph::strCallGraph(bool fullnames) const {
  return str(cg, fullnames);
}

std::string CallGraph::strClosure(bool fullnames) const {
  return str(closure, fullnames);
}

Dotifier& CallGraph::dot(const CallGraphT& cg, Dotifier& dotifier) const {
  dotifier.start(true);
  for(auto& kv : cg) {
    std::string src = getFunctionName(kv.first);
    for(const llvm::Function* callee : kv.second)
      dotifier.addEdge(src, getFunctionName(callee));
  }
  dotifier.end();
  return dotifier;
}

Dotifier& CallGraph::dot(const std::string& mode, Dotifier& dotifier) const {
  if(mode == "callgraph")
    return dot(cg, dotifier);
  else if(mode == "closure")
    return dot(closure, dotifier);
  else
    moya_error("Unsupported serialization mode: " << mode);
  return dotifier;
}

void CallGraph::serialize(Serializer& s) const {
  // XXXXXXX: This is really horrible, and should go away ASAP
  // But the pointer will never be dereferenced and it is only the value
  // that matters. At some point, all of this will use store addresses
  // which will be much better
  std::string fakeRoot = "";
  if(not root)
    fakeRoot = JSON::pointer(
        reinterpret_cast<const llvm::Function*>(region->getID()));

  s.mapStart();
  s.add("_class", "CallGraph");
  if(root)
    s.add("root", JSON::pointer(root));
  else
    s.add("root", fakeRoot);

  HashSet<const llvm::Function*> called;
  for(const auto& kv : cg) {
    const llvm::Function* caller = kv.first;
    if((not caller) or caller->size()) {
      // Caller will be null when region is null (which is the case when this
      // callgraph represents the whole program rather than just a region)
      Set<std::string> callees;
      for(const llvm::Function* callee : kv.second) {
        if(callee->size()) {
          callees.insert(JSON::pointer(callee));
          called.insert(callee);
        }
      }

      if(caller)
        s.add(JSON::pointer(caller), callees);
      else
        s.add(fakeRoot, callees);
    }
  }

  // Add the leaves to the callgraph
  for(const llvm::Function* caller : called)
    if(caller->size() and (not cg.contains(caller)))
      s.add(JSON::pointer(caller), HashSet<std::string>());

  s.mapEnd();
}

} // namespace moya
