#include "ContentRuntimePointer.h"

#include <sstream>

using namespace llvm;

namespace moya {

ContentRuntimePointer::ContentRuntimePointer()
    : Content(Content::Kind::RuntimePointer) {
  ;
}

bool ContentRuntimePointer::classof(const Content* c) {
  return c->getKind() == Content::Kind::RuntimePointer;
}

std::string ContentRuntimePointer::str() const {
  std::stringstream ss;
  ss << "CRuntimePointer()";
  return ss.str();
}

void ContentRuntimePointer::serialize(Serializer& s) const {
  s.mapStart();
  s.add("_class", "Pointer");
  s.mapEnd();
}

} // namespace moya
