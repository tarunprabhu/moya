#ifndef MOYA_COMMON_CONTENT_UNDEF_H
#define MOYA_COMMON_CONTENT_UNDEF_H

#include "Content.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>

namespace moya {

// We treat undef values differently because undef values are "special"
// constants which can be manipulated in ways that a regular constant cannot
// We don't bother associating this with an LLVM value because we don't need
// the type that the LLVM value will contain
class ContentUndef : public Content {
public:
  ContentUndef();
  virtual ~ContentUndef() = default;

  virtual std::string str() const;
  virtual void serialize(Serializer&) const;

public:
  static bool classof(const Content*);
};

} // namespace moya

#endif // MOYA_COMMON_CONTENT_UNDEF_H
