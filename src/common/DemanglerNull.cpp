#include "DemanglerNull.h"
#include "Diagnostics.h"

namespace moya {

DemanglerNull::DemanglerNull() : Demangler(SourceLang::UnknownLang) {
  ;
}

std::string DemanglerNull::demangle(const llvm::Function& f,
                                    Demangler::Action action) const {
  switch(action) {
  case Action::Full:
  case Action::Source:
  case Action::Qualified:
  case Action::Model:
    return f.getName();
    break;
  default:
    moya_error("Unsupported demangler action: " << action);
    break;
  }

  return f.getName().str();
}

std::string DemanglerNull::demangle(const llvm::GlobalVariable& g,
                                    Demangler::Action action) const {
  switch(action) {
  case Action::Full:
  case Action::Source:
  case Action::Qualified:
    return g.getName();
    break;
  case Action::Model:
    moya_error("Unsupported demangler action for global: " << action);
    break;
  }

  return g.getName().str();
}

} // namespace moya
