#include "Config.h"
#include "LLVMUtils.h"
#include "Metadata.h"
#include "Set.h"
#include "Vector.h"

#include <llvm/IR/Module.h>

using llvm::cast;
using llvm::dyn_cast;
using llvm::isa;

namespace moya {

namespace LLVMUtils {

llvm::Function* getMainFunction(llvm::Module& module) {
  for(llvm::Function& f : module.functions())
    if(moya::Metadata::hasMain(f))
      return &f;
  return nullptr;
}

llvm::Function* getMainFunction(llvm::Module* module) {
  return getMainFunction(*module);
}

const llvm::Function* getMainFunction(const llvm::Module& module) {
  for(const llvm::Function& f : module.functions())
    if(moya::Metadata::hasMain(f))
      return &f;
  return nullptr;
}

const llvm::Function* getMainFunction(const llvm::Module* module) {
  return getMainFunction(*module);
}

llvm::GlobalVariable* getPayloadRegionGlobal(llvm::Module& module) {
  std::string name = Config::getPayloadRegionGlobalName();
  llvm::Type* type = getRegionIDType(module.getContext());
  return dyn_cast<llvm::GlobalVariable>(module.getOrInsertGlobal(name, type));
}

llvm::GlobalVariable* getPayloadFunctionGlobal(llvm::Module& module) {
  std::string name = Config::getPayloadFunctionGlobalName();
  llvm::Type* type = llvm::Type::getInt8PtrTy(module.getContext());
  return dyn_cast<llvm::GlobalVariable>(module.getOrInsertGlobal(name, type));
}

llvm::Function* getEnterRegionFunction(llvm::Module& module) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* tyVoid = llvm::Type::getVoidTy(context);
  llvm::Type* tyRegion = getRegionIDType(context);

  llvm::Type* params[] = {tyRegion};
  llvm::FunctionType* ftype = llvm::FunctionType::get(tyVoid, params, false);
  std::string fname = Config::getEnterRegionFunctionName();

  // Fortran screws things up and might return aa function which is bitcast
  llvm::Constant* c = module.getOrInsertFunction(fname, ftype);
  if(auto* cexpr = dyn_cast<llvm::ConstantExpr>(c))
    return dyn_cast<llvm::Function>(cexpr->getOperand(0));
  else
    return dyn_cast<llvm::Function>(c);
}

const llvm::Function* getEnterRegionFunction(const llvm::Module& module) {
  return module.getFunction(Config::getEnterRegionFunctionName());
}

llvm::Function* getExitRegionFunction(llvm::Module& module) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* tyVoid = llvm::Type::getVoidTy(context);
  llvm::Type* tyRegion = getRegionIDType(context);

  llvm::Type* params[] = {tyRegion};
  llvm::FunctionType* ftype = llvm::FunctionType::get(tyVoid, params, false);
  std::string fname = Config::getExitRegionFunctionName();

  // Fortran screws things up and might return a function which is bitcast
  llvm::Constant* c = module.getOrInsertFunction(fname, ftype);
  if(auto* cexpr = dyn_cast<llvm::ConstantExpr>(c))
    return dyn_cast<llvm::Function>(cexpr->getOperand(0));
  else
    return dyn_cast<llvm::Function>(c);
}

const llvm::Function* getExitRegionFunction(const llvm::Module& module) {
  return module.getFunction(Config::getExitRegionFunctionName());
}

llvm::Function* getIsInRegionFunction(llvm::Module& module) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* i32 = llvm::Type::getInt32Ty(context);

  llvm::FunctionType* fty = llvm::FunctionType::get(i32, {}, false);
  std::string fname = Config::getIsInRegionFunctionName();

  return dyn_cast<llvm::Function>(module.getOrInsertFunction(fname, fty));
}

llvm::Function* getCompileFunction(llvm::Module& module) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* i8 = llvm::Type::getInt8Ty(context);
  llvm::Type* keyTy = getJITIDType(context);

  llvm::Type* params = {keyTy};
  llvm::FunctionType* fty
      = llvm::FunctionType::get(i8->getPointerTo(), params, false);
  std::string fname = Config::getCompileFunctionName();

  return dyn_cast<llvm::Function>(module.getOrInsertFunction(fname, fty));
}

llvm::Function* getBeginCallFunction(llvm::Module& module) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* voidTy = llvm::Type::getVoidTy(context);
  llvm::Type* keyTy = getJITIDType(context);

  llvm::Type* params[] = {keyTy};
  llvm::FunctionType* fty = llvm::FunctionType::get(voidTy, params, false);
  std::string fname = Config::getBeginCallFunctionName();

  return dyn_cast<llvm::Function>(module.getOrInsertFunction(fname, fty));
}

llvm::Function* getRegisterArgFunction(llvm::Module& module, llvm::Type* ty) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* voidTy = llvm::Type::getVoidTy(context);
  llvm::Type* keyTy = getJITIDType(context);
  llvm::Type* i32 = llvm::Type::getInt32Ty(context);

  llvm::Type* params[] = {keyTy, i32, ty, i32};
  llvm::FunctionType* fty = llvm::FunctionType::get(voidTy, params, false);
  std::string fname = Config::getRegisterArgFunctionName(ty);

  auto* f = dyn_cast<llvm::Function>(module.getOrInsertFunction(fname, fty));
  f->addFnAttr(llvm::Attribute::AttrKind::AlwaysInline);
  for(llvm::Argument& arg : f->args())
    if(arg.getType()->isPointerTy())
      f->setAttributes(f->getAttributes().addAttribute(
          context, arg.getArgNo() + 1, llvm::Attribute::AttrKind::ReadOnly));

  return f;
}

llvm::Function* getRegisterArgFunction(llvm::Module& module, llvm::ArrayType* aty) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* voidTy = llvm::Type::getVoidTy(context);
  llvm::Type* keyTy = getJITIDType(context);
  llvm::Type* i32 = llvm::Type::getInt32Ty(context);
  llvm::Type* pty = aty->getElementType()->getPointerTo();

  llvm::Type* params[] = {keyTy, i32, pty, i32, i32};
  llvm::FunctionType* fty = llvm::FunctionType::get(voidTy, params, false);
  std::string fname = Config::getRegisterArgFunctionName(aty);

  auto* f = dyn_cast<llvm::Function>(module.getOrInsertFunction(fname, fty));
  f->addFnAttr(llvm::Attribute::AttrKind::AlwaysInline);
  for(llvm::Argument& arg : f->args())
    if(arg.getType()->isPointerTy())
      f->setAttributes(f->getAttributes().addAttribute(
          context, arg.getArgNo() + 1, llvm::Attribute::AttrKind::ReadOnly));

  return f;
}

llvm::Function* getRegisterArgPtrFunction(llvm::Module& module) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* voidTy = llvm::Type::getVoidTy(context);
  llvm::Type* keyTy = getJITIDType(context);
  llvm::Type* pi8 = llvm::Type::getInt8Ty(context)->getPointerTo();
  llvm::Type* i32 = llvm::Type::getInt32Ty(context);

  llvm::Type* params[] = {keyTy, i32, pi8, i32};
  llvm::FunctionType* fty = llvm::FunctionType::get(voidTy, params, false);
  std::string fname = Config::getRegisterArgPtrFunctionName();

  auto* f = dyn_cast<llvm::Function>(module.getOrInsertFunction(fname, fty));
  f->addFnAttr(llvm::Attribute::AttrKind::AlwaysInline);
  for(llvm::Argument& arg : f->args())
    if(arg.getType()->isPointerTy())
      f->setAttributes(f->getAttributes().addAttribute(
          context, arg.getArgNo() + 1, llvm::Attribute::AttrKind::ReadOnly));

  return f;
}

// llvm::Function* getStatsStartCallFunction(llvm::Module& module) {
//   llvm::LLVMContext& context = module.getContext();

//   Type* tyVoid = Type::getVoidTy(context);
//   Type* tyKey = getJITIDType(context);

//   Type* params[] = {tyKey};
//   FunctionType* ftype = FunctionType::get(tyVoid, params, false);
//   std::string fname = Config::getStatsStartCallFunctionName();

//   return dyn_cast<llvm::Function>(module.getOrInsertFunction(fname, ftype));
// }

// llvm::Function* getStatsStopCallFunction(llvm::Module& module) {
//   llvm::LLVMContext& context = module.getContext();

//   Type* tyVoid = Type::getVoidTy(context);
//   Type* tyKey = getJITIDType(context);

//   Type* params[] = {tyKey};
//   FunctionType* ftype = FunctionType::get(tyVoid, params, false);
//   std::string fname = Config::getStatsStopCallFunctionName();

//   return dyn_cast<llvm::Function>(module.getOrInsertFunction(fname, ftype));
// }

// llvm::Function* getStatsStartCompileFunction(llvm::Module& module) {
//   llvm::LLVMContext& context = module.getContext();

//   Type* tyVoid = Type::getVoidTy(context);
//   Type* tyKey = getJITIDType(context);
//   Type* tySig = getFunctionSignatureBasicType(context);

//   Type* params[] = {tyKey, tySig};
//   FunctionType* ftype = FunctionType::get(tyVoid, params, false);
//   std::string fname = Config::getStatsStartCompileFunctionName();

//   return dyn_cast<llvm::Function>(module.getOrInsertFunction(fname, ftype));
// }

// llvm::Function* getStatsStopCompileFunction(llvm::Module& module) {
//   llvm::LLVMContext& context = module.getContext();

//   Type* tyVoid = Type::getVoidTy(context);
//   Type* tyKey = getJITIDType(context);
//   Type* tySig = getFunctionSignatureBasicType(context);

//   Type* params[] = {tyKey, tySig};
//   FunctionType* ftype = FunctionType::get(tyVoid, params, false);
//   std::string fname = Config::getStatsStopCompileFunctionName();

//   return dyn_cast<llvm::Function>(module.getOrInsertFunction(fname, ftype));
// }

llvm::Function* getStatsStartExecuteFunction(llvm::Module& module) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* tyVoid = llvm::Type::getVoidTy(context);
  llvm::Type* tyKey = getJITIDType(context);
  llvm::Type* tySig = getFunctionSignatureBasicType(context);
  llvm::Type* tyVer = getFunctionSignatureTuningType(context);

  llvm::Type* params[] = {tyKey, tySig, tyVer};
  llvm::FunctionType* ftype = llvm::FunctionType::get(tyVoid, params, false);
  std::string fname = Config::getStatsStartExecuteFunctionName();

  return dyn_cast<llvm::Function>(module.getOrInsertFunction(fname, ftype));
}

llvm::Function* getStatsStopExecuteFunction(llvm::Module& module) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* tyVoid = llvm::Type::getVoidTy(context);
  llvm::Type* tyKey = getJITIDType(context);
  llvm::Type* tySig = getFunctionSignatureBasicType(context);
  llvm::Type* tyVer = getFunctionSignatureTuningType(context);

  llvm::Type* params[] = {tyKey, tySig, tyVer};
  llvm::FunctionType* ftype = llvm::FunctionType::get(tyVoid, params, false);
  std::string fname = Config::getStatsStopExecuteFunctionName();

  return dyn_cast<llvm::Function>(module.getOrInsertFunction(fname, ftype));
}

llvm::Function* getTopErrorFunction(llvm::Module& module) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* tyVoid = llvm::Type::getVoidTy(context);

  llvm::ArrayRef<llvm::Type*> params;
  llvm::FunctionType* ftype = llvm::FunctionType::get(tyVoid, params, false);
  std::string fname = Config::getTopErrorFunctionName();

  return dyn_cast<llvm::Function>(module.getOrInsertFunction(fname, ftype));
}

llvm::StructType* getMoyaStdStringType(llvm::Module& module) {
  return module.getTypeByName("moya.std.string");
}

llvm::StructType* getMoyaStdStringType(const llvm::Module& module) {
  return module.getTypeByName("moya.std.string");
}

static void addType(llvm::Type* type,
                    bool onlyNamed,
                    Set<llvm::Type*>& seen,
                    Set<llvm::StructType*>& structs) {
  if(seen.insert(type)) {
    if(type->isIntegerTy() or type->isFloatTy() or type->isDoubleTy()
       or type->isX86_FP80Ty() or type->isVoidTy() or type->isLabelTy()
       or type->isMetadataTy()) {
      ;
    } else if(auto* pty = dyn_cast<llvm::PointerType>(type)) {
      addType(pty->getElementType(), onlyNamed, seen, structs);
    } else if(auto* aty = dyn_cast<llvm::ArrayType>(type)) {
      addType(aty->getElementType(), onlyNamed, seen, structs);
    } else if(auto* fty = dyn_cast<llvm::FunctionType>(type)) {
      addType(fty->getReturnType(), onlyNamed, seen, structs);
      for(llvm::Type* element : fty->params())
        addType(element, onlyNamed, seen, structs);
    } else if(auto* sty = dyn_cast<llvm::StructType>(type)) {
      if((not onlyNamed) or sty->hasName())
        structs.insert(sty);
      for(auto* element : sty->elements())
        addType(element, onlyNamed, seen, structs);
    } else {
      moya_error("Unknown type to collect: " << *type);
    }
  }
}

Vector<llvm::StructType*> getStructTypes(const llvm::Module& module,
                                         bool onlyNamed) {
  Set<llvm::Type*> seen;
  Set<llvm::StructType*> structs;

  for(const llvm::Function& f : module.functions()) {
    addType(f.getFunctionType(), onlyNamed, seen, structs);
    for(const llvm::Argument& arg : f.args()) {
      addType(arg.getType(), onlyNamed, seen, structs);
      addType(LLVMUtils::getAnalysisType(arg), onlyNamed, seen, structs);
    }
    for(const llvm::BasicBlock& bb : f) {
      for(const llvm::Instruction& inst : bb) {
        addType(inst.getType(), onlyNamed, seen, structs);
        addType(LLVMUtils::getAnalysisType(inst), onlyNamed, seen, structs);
        for(const llvm::Use& op : inst.operands()) {
          addType(op->getType(), onlyNamed, seen, structs);
          addType(
              LLVMUtils::getAnalysisType(op.get()), onlyNamed, seen, structs);
        }
      }
    }
  }

  for(const llvm::GlobalVariable& g : module.globals()) {
    addType(g.getType(), onlyNamed, seen, structs);
    addType(LLVMUtils::getAnalysisType(g), onlyNamed, seen, structs);
  }

  return Vector<llvm::StructType*>(structs.begin(), structs.end());
}

Vector<llvm::StructType*> getStructTypes(const llvm::Module* module,
                                         bool onlyNamed) {
  return getStructTypes(*module, onlyNamed);
}

} // namespace LLVMUtils

} // namespace moya
