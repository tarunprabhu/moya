#ifndef MOYA_COMMON_GRAPH_H
#define MOYA_COMMON_GRAPH_H

#include "Map.h"
#include "Set.h"
#include "Stack.h"
#include "Vector.h"

#include <memory>

namespace moya {

template <typename T>
class Graph {
public:
  using Component = Set<T>;
  using Components = Vector<Component>;

protected:
  template <typename N>
  struct GraphNode {
    N data;

    // These fields are used to compute SCC's
    int index;
    int lowlink;
    bool inwl;

    GraphNode(N data) : data(data), index(-1), lowlink(-1), inwl(false) {
      ;
    }
  };

protected:
  Map<T, Set<T>> edges;
  Map<T, std::unique_ptr<struct GraphNode<T>>> nodes;

protected:
  void getSCC(T, int&, Stack<T>&, Components&);

public:
  Graph();

  void addNode(T);
  void addEdge(T, T);
  Components getSCCs();

  std::string str() const;
};

} // namespace moya

#endif // MOYA_COMMON_GRAPH_H
