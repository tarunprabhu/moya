#ifndef MOYA_COMMON_CLASS_HEIRARCHIES_H
#define MOYA_COMMON_CLASS_HEIRARCHIES_H

#include "common/Map.h"
#include "common/Vector.h"

#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>

namespace moya {

class ClassHeirarchiesNode;
class Dotifier;
class Serializer;

class ClassHeirarchies {
private:
  Map<llvm::StructType*, ClassHeirarchiesNode*> nodes;

private:
  ClassHeirarchiesNode* getNode(llvm::StructType*, const llvm::Module&);
  bool hasNode(llvm::StructType*) const;
  const ClassHeirarchiesNode* getNode(llvm::StructType*) const;

  bool isBase(const ClassHeirarchiesNode*, llvm::StructType*) const;
  bool isVirtualBase(const ClassHeirarchiesNode*, llvm::StructType*) const;

public:
  ~ClassHeirarchies();
  void initialize(const llvm::Module&);

  bool isDerivedFrom(llvm::StructType*, llvm::StructType*) const;
  bool isDerivedFrom(llvm::Type*, llvm::Type*) const;
  bool isBaseOf(llvm::StructType*, llvm::StructType*) const;
  bool isBaseOf(llvm::Type*, llvm::Type*) const;
  bool isVirtualBaseOf(llvm::StructType*, llvm::StructType*) const;
  bool isVirtualBaseOf(llvm::Type*, llvm::Type*) const;

  void dotify(Dotifier&) const;
  void serialize(Serializer&) const;
};

} // namespace moya

#endif // MOYA_COMMON_CLASS_HEIRARCHIES_H
