#include "PayloadBase.h"
#include "Config.h"
#include "Diagnostics.h"
#include "Hexify.h"
#include "Location.h"
#include "Region.h"
#include "Status.h"
#include "SummaryBase.h"

#include <iostream>
#include <sstream>

namespace moya {

PayloadBase::PayloadBase(const std::string& file,
                         std::ios_base::openmode flags) {
  if(flags & std::ios_base::in) {
    inf.open(file, std::ios::binary | flags);
    if(not inf.is_open())
      moya_error("Could not open file: " << file);
  } else if((flags & std::ios_base::out) or (flags & std::ios_base::app)) {
    outf.open(file, std::ios::binary | flags);
    if(not outf.is_open())
      moya_error("Could not open file: " << file);
  } else {
    moya_error("Invalid payload flags: " << flags);
  }
}

PayloadBase::~PayloadBase() {
  if(inf.is_open())
    inf.close();
  if(outf.is_open())
    outf.close();
}

std::ifstream& PayloadBase::fin() {
  return inf;
}

std::ofstream& PayloadBase::fout() {
  return outf;
}

// Pickle functions
template <typename T,
          typename std::enable_if_t<std::is_arithmetic<T>::value, int>>
void PayloadBase::write(const T& val) {
  outf.write(reinterpret_cast<const char*>(&val), sizeof(T));
}

void PayloadBase::write(const std::string& s, bool terminate) {
  outf.write(s.c_str(), s.length());
  if(terminate)
    outf.put(0);
}

template void PayloadBase::write(const bool&);
template void PayloadBase::write(const int8_t&);
template void PayloadBase::write(const uint8_t&);
template void PayloadBase::write(const int32_t&);
template void PayloadBase::write(const uint32_t&);
template void PayloadBase::write(const int64_t&);
template void PayloadBase::write(const uint64_t&);

// Read functions
template <typename T,
          typename std::enable_if_t<std::is_arithmetic<T>::value, int>>
void PayloadBase::read(T& val) {
  inf.read(reinterpret_cast<char*>(&val), sizeof(T));
}

void PayloadBase::read(std::string& s) {
  std::stringstream ss;
  char c = 0;
  inf.read(&c, 1);
  while(c != '\0') {
    ss << c;
    inf.read(&c, 1);
  }
  s = ss.str();
}

template void PayloadBase::read(bool&);
template void PayloadBase::read(int8_t&);
template void PayloadBase::read(uint8_t&);
template void PayloadBase::read(int32_t&);
template void PayloadBase::read(uint32_t&);
template void PayloadBase::read(int64_t&);
template void PayloadBase::read(uint64_t&);

} // namespace moya
