#include "Trace.h"
#include "Diagnostics.h"
#include "Formatting.h"
#include "LLVMUtils.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Instructions.h>
#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;

namespace moya {

Trace::Trace(Value* v) {
  if(not genTrace(v))
    moya_error("Could not generate trace: " << *v);
  for(Vector<Value*>& trace : traces)
    reverse(trace.begin(), trace.end());
}

Trace::~Trace() {
  for(Instruction* inst : todelete)
    inst->deleteValue();
}

bool Trace::genTrace(Value* v, Vector<Value*>& trace) {
  if(auto* cexpr = dyn_cast<ConstantExpr>(v)) {
    Instruction* inst = cexpr->getAsInstruction();
    todelete.emplace(inst);
    v = inst;
  }

  trace.push_back(v);

  if(isa<AllocaInst>(v) or isa<Argument>(v) or isa<GlobalVariable>(v)
     or isa<Function>(v))
    return true;

  if(isa<ConstantPointerNull>(v) or isa<UndefValue>(v))
    return true;

  if(isa<Constant>(v))
    return false;

  if(isa<CallInst>(v) or isa<InvokeInst>(v))
    return true;

  if(auto* gep = dyn_cast<GetElementPtrInst>(v))
    return genTrace(gep->getPointerOperand(), trace);
  else if(auto* cst = dyn_cast<CastInst>(v))
    return genTrace(cst->getOperand(0), trace);
  else if(auto* load = dyn_cast<LoadInst>(v))
    return genTrace(load->getPointerOperand(), trace);
  else if(auto* ret = dyn_cast<ReturnInst>(v))
    return genTrace(ret->getReturnValue(), trace);
  else if(auto* select = dyn_cast<SelectInst>(v)) {
    Vector<Value*> newTrace(trace);
    for(Value* op : {select->getFalseValue(), select->getTrueValue()})
      if(not trace.contains(op))
        if(genTrace(op, newTrace))
          if(newTrace.size())
            traces.push_back(newTrace);
    trace.clear();
    return true;
  } else if(auto* phi = dyn_cast<PHINode>(v)) {
    for(unsigned i = 0; i < phi->getNumIncomingValues(); i++) {
      Vector<Value*> newTrace(trace);
      if(not trace.contains(phi->getIncomingValue(i)))
        if(genTrace(phi->getIncomingValue(i), newTrace))
          if(newTrace.size())
            traces.push_back(newTrace);
    }
    trace.clear();
    return true;
  } else if(auto* bin = dyn_cast<BinaryOperator>(v)) {
    for(unsigned i = 0; i < bin->getNumOperands(); i++) {
      Vector<Value*> newTrace(trace);
      if(not trace.contains(bin->getOperand(i)))
        if(genTrace(bin->getOperand(i), newTrace))
          if(newTrace.size())
            traces.push_back(newTrace);
    }
    trace.clear();
    return true;
  }

  std::string s;
  raw_string_ostream ss(s);
  if(auto* inst = dyn_cast<Instruction>(v)) {
    ss << "Function: " << LLVMUtils::getFunctionName(inst) << "\n";
    if(BasicBlock* bb = inst->getParent())
      ss << *bb->getParent() << "\n";
  }
  ss << *v << "\n";
  ss << str() << "\n";
  moya_error("Unknown value type in trace: " << ss.str());
  return false;
}

bool Trace::genTrace(Value* v) {
  Vector<Value*> trace;
  if(genTrace(v, trace)) {
    if(trace.size())
      traces.push_back(trace);
    return true;
  }
  return false;
}

Vector<Value*> Trace::fronts() const {
  Vector<Value*> ret;
  for(const Vector<Value*>& trace : traces)
    ret.push_back(trace.front());
  return ret;
}

Vector<Value*> Trace::backs() const {
  Vector<Value*> ret;
  for(const Vector<Value*>& trace : traces)
    ret.push_back(trace.back());
  return ret;
}

Vector<Value*> Trace::rests() const {
  Vector<Value*> ret;
  for(const Vector<Value*>& trace : traces)
    for(unsigned i = 1; i < trace.size(); i++)
      ret.push_back(trace.at(i));
  return ret;
}

std::string Trace::str(const Vector<Value*>& trace, int depth) const {
  std::string s;
  raw_string_ostream ss(s);

  ss << space(depth) << "trace {\n";
  for(Value* v : trace)
    ss << space1(depth) << *v << "\n";
  ss << space(depth) << "}";

  return ss.str();
}

std::string Trace::str() const {
  std::stringstream ss;

  ss << "traces {" << std::endl;
  for(const Vector<Value*>& trace : traces)
    ss << str(trace, 1) << std::endl;
  ss << "}";

  return ss.str();
}

} // namespace moya
