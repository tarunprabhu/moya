#include "ConfigCBindings.h"
#include "Config.h"

// These are functions that are used by the Python front-end so we don't have
// to hardcode these in the Python scripts
extern "C" {

const char* MoyaConfig_getEnvVarDisabled() {
  return moya::Config::getEnvVarDisabled();
}

const char* MoyaConfig_getEnvVarVerbose() {
  return moya::Config::getEnvVarVerbose();
}

const char* MoyaConfig_getEnvVarSaveDir() {
  return moya::Config::getEnvVarSaveDir();
}

const char* MoyaConfig_getEnvVarWorkingDir() {
  return moya::Config::getEnvVarWorkingDir();
}

const char* MoyaConfig_getEnvVarPayloadDir() {
  return moya::Config::getEnvVarPayloadDir();
}

const char* MoyaConfig_getEnvVarDebugFns() {
  return moya::Config::getEnvVarDebugFns();
}

const char* MoyaConfig_getEnvVarDebugStore() {
  return moya::Config::getEnvVarDebugStore();
}

uint64_t MoyaConfig_getPayloadMagicNumber() {
  return moya::Config::getPayloadMagicNumber();
}

const char* MoyaConfig_getELFBitcodeSectionName() {
  return moya::Config::getELFBitcodeSectionName();
}

} // extern "C"
