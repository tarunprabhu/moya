#include "Hasher.h"

namespace moya {

namespace Hasher {

// This implements the FNV hash algorithm

static const uint64_t FNV_BASIS = 14695981039346656037ULL;
static const uint64_t FNV_PRIME = 1099511628211ULL;

Hash& initHash(Hash& hash) {
  hash = FNV_BASIS;

  return hash;
}

template <typename T>
static Hash& updateHashImpl(const T* val, Hash& hash) {
  const uint8_t* arr = reinterpret_cast<const uint8_t*>(val);
  for(unsigned i = 0; i < sizeof(T); i++) {
    hash ^= arr[i];
    hash *= FNV_PRIME;
  }

  return hash;
}

template <typename T, std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
static Hash getHashImpl(const T* val) {
  Hash hash;
  initHash(hash);
  updateHashImpl(val, hash);

  return hash;
}

Hash get(const std::string& s) {
  Hash hash;
  initHash(hash);
  for(char c : s)
    updateHashImpl(&c, hash);
  return hash;
}

Hash get(uint8_t value) {
  return getHashImpl(&value);
}

Hash get(uint16_t value) {
  return getHashImpl(&value);
}

Hash get(uint32_t value) {
  return getHashImpl(&value);
}

Hash get(uint64_t value) {
  return getHashImpl(&value);
}

Hash get(float value) {
  return getHashImpl(&value);
}

Hash get(double value) {
  return getHashImpl(&value);
}

Hash get(long double value) {
  return getHashImpl(&value);
}

Hash& update(uint8_t value, Hash& hash) {
  return updateHashImpl(&value, hash);
}

Hash& update(uint16_t value, Hash& hash) {
  return updateHashImpl(&value, hash);
}

Hash& update(uint32_t value, Hash& hash) {
  return updateHashImpl(&value, hash);
}

Hash& update(uint64_t value, Hash& hash) {
  return updateHashImpl(&value, hash);
}

Hash& update(float value, Hash& hash) {
  return updateHashImpl(&value, hash);
}

Hash& update(double value, Hash& hash) {
  return updateHashImpl(&value, hash);
}

Hash& update(long double value, Hash& hash) {
  return updateHashImpl(&value, hash);
}

Hash& update(const std::string& s, Hash& hash) {
  for(char c : s)
    updateHashImpl(&c, hash);
  return hash;
}

} // namespace Hasher

} // namespace moya
