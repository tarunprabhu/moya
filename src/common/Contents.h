#ifndef MOYA_COMMON_CONTENTS_H
#define MOYA_COMMON_CONTENTS_H

#include "Content.h"
#include "Diagnostics.h"
#include "Set.h"

#include <llvm/IR/Instruction.h>

#include <tuple>

namespace moya {

class ContentAddress;
class ContentCode;
class ContentComposite;
class ContentNullptr;
class ContentRuntimePointer;
class ContentRuntimeScalar;
class ContentScalar;
class ContentUndef;
class Serializer;

template <typename T>
using ContentsImpl = moya::Set<const T*>;

class Contents {
public:
  using iterator = ContentsImpl<Content>::iterator;
  using const_iterator = ContentsImpl<Content>::const_iterator;

protected:
  std::tuple<ContentsImpl<Content>,
             ContentsImpl<ContentAddress>,
             ContentsImpl<ContentCode>,
             ContentsImpl<ContentComposite>,
             ContentsImpl<ContentNullptr>,
             ContentsImpl<ContentRuntimePointer>,
             ContentsImpl<ContentRuntimeScalar>,
             ContentsImpl<ContentScalar>,
             ContentsImpl<ContentUndef>>
      contents;

protected:
  template <typename C>
  ContentsImpl<C>& get();
  template <typename C>
  const ContentsImpl<C>& get() const;
  bool insertImpl(const Content*);

public:
  Contents();
  Contents(const Content*);
  Contents(const Contents&);
  Contents(Contents&&);
  template <typename C = Content>
  Contents(const ContentsImpl<C>&);

  Contents& operator=(const Contents&);
  Contents& operator=(Contents&&);

  template <typename C = Content>
  const ContentsImpl<C>& getAs(const llvm::Instruction*) const;

  bool insert(const Contents&);
  bool insert(const Content*);
  template <typename C>
  bool insert(const ContentsImpl<C>&);

  Contents::iterator begin();
  Contents::iterator end();
  Contents::const_iterator begin() const;
  Contents::const_iterator end() const;

  bool empty() const;
  size_t size() const;

  bool operator==(const Contents&) const;
  bool operator!=(const Contents&) const;
  bool operator<(const Contents&) const;
  bool operator<=(const Contents&) const;
  bool operator>(const Contents&) const;
  bool operator>=(const Contents&) const;

  bool disjoint(const Contents&) const;

  std::string str() const;
  Serializer& serialize(Serializer&) const;
};

using Addresses = ContentsImpl<ContentAddress>;

} // namespace moya

#include "ContentAddress.h"
#include "ContentCode.h"
#include "ContentComposite.h"
#include "ContentNullptr.h"
#include "ContentRuntimePointer.h"
#include "ContentRuntimeScalar.h"
#include "ContentScalar.h"
#include "ContentUndef.h"

#endif // MOYA_COMMON_CONTENTS_H
