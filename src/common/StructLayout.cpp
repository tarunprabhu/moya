#include "StructLayout.h"

namespace moya {

void StructLayout::init(llvm::StructType* sty,
                        const llvm::DataLayout& dl,
                        uint64_t first) {
  const llvm::StructLayout& sl = *dl.getStructLayout(sty);
  for(unsigned i = 0; i < sty->getNumElements(); i++) {
    llvm::Type* ety = sty->getElementType(i);
    uint64_t offset = first + sl.getElementOffset(i);
    if(auto* tty = llvm::dyn_cast<llvm::StructType>(ety)) {
      init(tty, dl, offset);
    } else {
      flattenedTypes.push_back(ety);
      flattenedOffsets.push_back(offset);
    }
  }
}

StructLayout::StructLayout(llvm::StructType* sty, const llvm::DataLayout& dl)
    : sty(sty), sl(*dl.getStructLayout(sty)) {
  init(sty, dl, 0);
}

uint64_t StructLayout::getSizeInBits() const {
  return sl.getSizeInBits();
}

uint64_t StructLayout::getSizeInBytes() const {
  return sl.getSizeInBytes();
}

unsigned StructLayout::getAlignment() const {
  return sl.getAlignment();
}

bool StructLayout::hasPadding() const {
  return sl.hasPadding();
}

unsigned StructLayout::getElementContainingOffset(uint64_t offset) const {
  return sl.getElementContainingOffset(offset);
}

uint64_t StructLayout::getElementOffset(unsigned idx) const {
  return sl.getElementOffset(idx);
}

uint64_t StructLayout::getElementOffsetInBits(unsigned idx) const {
  return sl.getElementOffsetInBits(idx);
}

unsigned StructLayout::getNumElements() const {
  return sty->getNumElements();
}

unsigned StructLayout::getNumFlattenedElements() const {
  return flattenedOffsets.size();
}

llvm::Type* StructLayout::getFlattenedElementType(unsigned idx) const {
  return flattenedTypes.at(idx);
}

uint64_t StructLayout::getFlattenedElementOffset(unsigned idx) const {
  return flattenedOffsets.at(idx);
}

} // namespace moya
