#ifndef MOYA_COMMON_CONTENT_ADDRESS_H
#define MOYA_COMMON_CONTENT_ADDRESS_H

#include "Content.h"
#include "Set.h"

#include <stdint.h>

namespace moya {

// We treat the addresses as a special case because we want to avoid
// accidentally using a compile-time constant as an address if possible. This
// makes it slightly easier to catch those kinds of cases
class ContentAddress : public Content {
public:
  using Raw = int64_t;

protected:
  Raw addr;

public:
  ContentAddress(Raw);
  virtual ~ContentAddress() = default;

  Raw getRaw() const;

  virtual std::string str() const;
  virtual void serialize(Serializer&) const;

public:
  static const Raw null = 0;

public:
  static bool classof(const Content*);
};

using Address = ContentAddress;

} // namespace moya

#endif // MOYA_COMMON_CONTENT_ADDRESS_H
