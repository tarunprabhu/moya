#include "Diagnostics.h"
#include "Config.h"
#include "LLVMUtils.h"
#include "Metadata.h"

#include <cstdlib>
#include <execinfo.h>

using namespace llvm;

namespace moya {

std::string getBacktrace() {
  void* buffer[256];
  std::string buf;
  raw_string_ostream ss(buf);

  int size = backtrace(buffer, 256);
  char** strings = backtrace_symbols(buffer, size);
  ss << "Backtrace:"
     << "\n";
  for(int i = 0; i < size; i++)
    ss << "  " << strings[i] << "\n";
  ss << "\n";
  free(strings);

  return ss.str();
}

std::string getDiagnostic(const Instruction* inst) {
  std::string buf;
  raw_string_ostream ss(buf);
  const Function* f = LLVMUtils::getFunction(inst);

  ss << *inst << " at " << getDiagnostic(f);

  return ss.str();
}

std::string getDiagnostic(const Function* f) {
  return getDiagnostic(*f);
}

std::string getDiagnostic(const Function& f) {
  std::string buf;
  raw_string_ostream ss(buf);

  if(moya::Metadata::hasQualifiedName(f))
    ss << moya::Metadata::getQualifiedName(f) << " [" << f.getName() << "]";
  else if(moya::Metadata::hasSourceName(f))
    ss << moya::Metadata::getSourceName(f) << " [" << f.getName() << "]";
  else
    ss << f.getName();

  return ss.str();
}

std::string getDiagnostic(const GlobalVariable* g) {
  return getDiagnostic(*g);
}

std::string getDiagnostic(const GlobalVariable& g) {
  std::string buf;
  raw_string_ostream ss(buf);

  if(g.hasName())
    if(moya::Metadata::hasSourceName(g))
      ss << moya::Metadata::getSourceName(g);
    else
      ss << g.getName();
  else
    ss << g;

  return ss.str();
}

int isVerboseEnabled() {
  return moya::Config::getEnvVerbose();
}

} // namespace moya
