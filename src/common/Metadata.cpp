#include "Metadata.h"
#include "ArgumentUtils.h"
#include "Diagnostics.h"
#include "LLVMUtils.h"
#include "Parse.h"
#include "Set.h"
#include "Status.h"
#include "Stringify.h"
#include "Vector.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Instruction.h>
#include <llvm/Support/raw_ostream.h>

#include <cctype>
#include <sstream>
#include <vector>

using llvm::dyn_cast;
using llvm::dyn_cast_or_null;
using llvm::isa;

static bool shouldInsertDot(const std::string& in, size_t pos, size_t len) {
  // Touching uppercase letters should not be converted because they are
  // intended to be that way. If the current letter is uppercase, check the
  // letter on either side. If both are uppercase, don't convert it.
  if(isupper(in.at(pos))) {
    // This is not the last character. Check the character on either side
    if(len > pos + 1) {
      if(isupper(in.at(pos - 1)) and isupper(in.at(pos + 1)))
        return false;
    } else {
      if(isupper(in.at(pos - 1)))
        return false;
    }
    return true;
  }
  return false;
}

static std::string fixCasing(const std::string in) {
  std::stringstream ss;
  size_t len = in.length();
  if(len == 0)
    return ss.str();

  ss << static_cast<char>(tolower(in.at(0)));
  for(unsigned i = 1; i < in.size(); i++) {
    if(shouldInsertDot(in, i, len))
      ss << ".";
    ss << static_cast<char>(tolower(in.at(i)));
  }

  return ss.str();
}

static std::string getMoyaPrefix() {
  return "moya.";
}

static std::string getMoyaAttrName(const std::string& base) {
  // It would be nice to have the casing be the same for attributes
  return getMoyaPrefix() + base;
}

static std::string getMoyaNodeName(const std::string& base) {
  return getMoyaPrefix() + fixCasing(base);
}

static std::string getMoyaGlobalName(const std::string& base) {
  return getMoyaPrefix() + fixCasing(base);
}

template <typename T>
static llvm::Metadata* getMetadata(llvm::LLVMContext&, const T&);
template <typename T>
static llvm::Metadata* getMetadata(llvm::LLVMContext&, T*);
template <typename T>
static bool setAttr(llvm::Function&, const std::string&, const T&);
template <typename T>
static bool setAttr(llvm::Function&, const std::string&, T*);
template <typename T>
static T getAttr(const llvm::Function&, const std::string&);
template <typename T>
static bool setAttr(llvm::Argument&, const std::string&, const T&);
template <typename T>
static T getAttr(const llvm::Argument&, const std::string&);
template <typename T>
static bool setAttr(llvm::Instruction*, const std::string&, const T&);
template <typename T>
static T getAttr(const llvm::Instruction*, const std::string&);
template <typename T>
static bool setAttr(llvm::GlobalVariable&, const std::string&, const T&);
template <typename T>
static T getAttr(const llvm::GlobalVariable&, const std::string&);
template <typename T>
static bool setAttr(llvm::Type*, const std::string&, const T&, llvm::Module&);
template <typename T>
static T getAttr(llvm::Type*, const std::string&, const llvm::Module&);
template <typename T>
static moya::Vector<T> getMDEntry(const llvm::NamedMDNode*);
template <typename T>
static bool setMDEntry(llvm::NamedMDNode*, const T&);

template <>
llvm::Type* moya::Metadata::getAs(const llvm::Metadata* md) {
  if(const auto* cm = dyn_cast<llvm::ConstantAsMetadata>(md))
    return cm->getType();
  return nullptr;
}

template <>
llvm::StructType* moya::Metadata::getAs(const llvm::Metadata* md) {
  return dyn_cast<llvm::StructType>(getAs<llvm::Type*>(md));
}

template <>
std::string moya::Metadata::getAs(const llvm::Metadata* md) {
  return dyn_cast<llvm::MDString>(md)->getString();
}

template <>
moya::Vector<llvm::Type*> moya::Metadata::getAs(const llvm::Metadata* md) {
  moya::Vector<llvm::Type*> types;
  for(const llvm::MDOperand& op : dyn_cast<llvm::MDNode>(md)->operands())
    if(auto* cm = dyn_cast_or_null<llvm::ConstantAsMetadata>(op))
      types.push_back(cm->getValue()->getType());
    else
      types.push_back(nullptr);
  return types;
}

template <>
moya::Set<llvm::Type*> moya::Metadata::getAs(const llvm::Metadata* md) {
  moya::Set<llvm::Type*> types;
  for(const llvm::MDOperand& op : dyn_cast<llvm::MDNode>(md)->operands())
    if(auto* cm = dyn_cast_or_null<llvm::ConstantAsMetadata>(op))
      types.insert(cm->getValue()->getType());
    else
      types.insert(nullptr);
  return types;
}

template <>
moya::Vector<llvm::StructType*>
moya::Metadata::getAs(const llvm::Metadata* md) {
  moya::Vector<llvm::StructType*> types;
  for(llvm::Type* ty : moya::Metadata::getAs<moya::Vector<llvm::Type*>>(md))
    types.push_back(dyn_cast<llvm::StructType>(ty));
  return types;
}

template <>
moya::Set<llvm::StructType*> moya::Metadata::getAs(const llvm::Metadata* md) {
  moya::Set<llvm::StructType*> types;
  for(llvm::Type* ty : moya::Metadata::getAs<moya::Vector<llvm::Type*>>(md))
    types.insert(dyn_cast<llvm::StructType>(ty));
  return types;
}

template <>
moya::Vector<int64_t> moya::Metadata::getAs(const llvm::Metadata* md) {
  moya::Vector<int64_t> ints;
  for(const llvm::MDOperand& op : dyn_cast<llvm::MDNode>(md)->operands())
    ints.push_back(moya::Metadata::getAs<int64_t>(op.get()));
  return ints;
}

template <>
moya::Vector<std::string> moya::Metadata::getAs(const llvm::Metadata* md) {
  moya::Vector<std::string> strs;
  for(const llvm::MDOperand& op : dyn_cast<llvm::MDNode>(md)->operands())
    strs.push_back(moya::Metadata::getAs<std::string>(op.get()));
  return strs;
}

template <>
moya::Set<unsigned> moya::Metadata::getAs(const llvm::Metadata* md) {
  moya::Set<unsigned> ints;
  for(const llvm::MDOperand& op : dyn_cast<llvm::MDNode>(md)->operands())
    ints.insert(moya::Metadata::getAs<unsigned>(op.get()));
  return ints;
}

template <>
llvm::Function* moya::Metadata::getAs(const llvm::Metadata* md) {
  if(const auto* cm = dyn_cast<llvm::ConstantAsMetadata>(md))
    if(auto* f = dyn_cast<llvm::Function>(cm->getValue()))
      return f;
  return nullptr;
}

template <>
llvm::GlobalVariable* moya::Metadata::getAs(const llvm::Metadata* md) {
  if(const auto* cm = dyn_cast<llvm::ConstantAsMetadata>(md))
    if(auto* g = dyn_cast<llvm::GlobalVariable>(cm->getValue()))
      return g;
  return nullptr;
}

template <>
moya::Location moya::Metadata::getAs(const llvm::Metadata* md) {
  const auto* node = dyn_cast<llvm::MDNode>(md);
  auto file = moya::Metadata::getAs<std::string>(node->getOperand(0));
  auto line = moya::Metadata::getAs<unsigned>(node->getOperand(1));
  auto column = moya::Metadata::getAs<unsigned>(node->getOperand(2));

  return Location(file, line, column);
}

template <>
moya::Region moya::Metadata::getAs(const llvm::Metadata* md) {
  const auto* node = dyn_cast<llvm::MDNode>(md);
  auto id = moya::Metadata::getAs<RegionID>(node->getOperand(0));
  auto name = moya::Metadata::getAs<std::string>(node->getOperand(1));
  auto loc = moya::Metadata::getAs<Location>(node->getOperand(2));
  auto beginLoc = moya::Metadata::getAs<Location>(node->getOperand(3));
  auto endLoc = moya::Metadata::getAs<Location>(node->getOperand(4));
  auto lang = moya::Metadata::getAs<SourceLang>(node->getOperand(5));

  return moya::Region(id, name, loc, beginLoc, endLoc, lang);
}

template <>
moya::Status moya::Metadata::getAs(const llvm::Metadata* md) {
  if(const auto* cm = dyn_cast<llvm::ConstantAsMetadata>(md))
    if(const auto* cint = dyn_cast<llvm::ConstantInt>(cm->getValue()))
      return moya::Status(cint->getLimitedValue());
  return moya::Status::getUnused();
}

template <typename T>
T moya::Metadata::getAs(const llvm::Metadata* md) {
  if(const auto* cm = dyn_cast<llvm::ConstantAsMetadata>(md))
    if(auto* cint = dyn_cast<llvm::ConstantInt>(cm->getValue()))
      return static_cast<T>(cint->getLimitedValue());
  return static_cast<T>(0);
}

template <>
llvm::Metadata* getMetadata(llvm::LLVMContext& context,
                            const std::string& str) {
  return llvm::MDString::get(context, str);
}

template <>
llvm::Metadata* getMetadata(llvm::LLVMContext& context,
                            const moya::Status& status) {
  llvm::IntegerType* ity = llvm::IntegerType::get(context, 32);
  return llvm::ConstantAsMetadata::get(
      llvm::ConstantInt::get(ity, static_cast<int32_t>(status.getRaw())));
}

template <>
llvm::Metadata* getMetadata(llvm::LLVMContext&, llvm::Type* val) {
  if(val)
    return llvm::ConstantAsMetadata::get(llvm::UndefValue::get(val));
  return nullptr;
}

template <>
llvm::Metadata* getMetadata(llvm::LLVMContext&, llvm::StructType* val) {
  if(val)
    return llvm::ConstantAsMetadata::get(llvm::UndefValue::get(val));
  return nullptr;
}

template <>
llvm::Metadata* getMetadata(llvm::LLVMContext& context,
                            const moya::Vector<std::string>& strs) {
  std::vector<llvm::Metadata*> mds;
  for(const std::string& s : strs)
    mds.push_back(getMetadata(context, s));
  return llvm::MDNode::get(context, mds);
}

template <>
llvm::Metadata* getMetadata(llvm::LLVMContext& context,
                            const moya::Vector<llvm::StructType*>& types) {
  std::vector<llvm::Metadata*> mds;
  for(llvm::StructType* sty : types)
    mds.push_back(llvm::ConstantAsMetadata::get(llvm::UndefValue::get(sty)));
  return llvm::MDNode::get(context, mds);
}

template <>
llvm::Metadata* getMetadata(llvm::LLVMContext& context,
                            const moya::Set<llvm::StructType*>& types) {
  std::vector<llvm::Metadata*> mds;
  for(llvm::StructType* sty : types)
    mds.push_back(llvm::ConstantAsMetadata::get(llvm::UndefValue::get(sty)));
  return llvm::MDNode::get(context, mds);
}

template <>
llvm::Metadata* getMetadata(llvm::LLVMContext& context,
                            const moya::Vector<int64_t>& ints) {
  std::vector<llvm::Metadata*> mds;
  for(int i : ints)
    mds.push_back(getMetadata(context, i));
  return llvm::MDNode::get(context, mds);
}

template <>
llvm::Metadata* getMetadata(llvm::LLVMContext& context,
                            const moya::Set<unsigned>& ints) {
  std::vector<llvm::Metadata*> mds;
  for(int i : ints)
    mds.push_back(getMetadata(context, i));
  return llvm::MDNode::get(context, mds);
}

template <>
llvm::Metadata* getMetadata(llvm::LLVMContext& context,
                            const moya::Location& loc) {
  std::vector<llvm::Metadata*> mds;
  mds.push_back(getMetadata(context, loc.getFile()));
  mds.push_back(getMetadata(context, loc.getLine()));
  mds.push_back(getMetadata(context, loc.getColumn()));

  return llvm::MDNode::get(context, mds);
}

template <>
llvm::Metadata* getMetadata(llvm::LLVMContext& context,
                            const moya::Region& region) {
  std::vector<llvm::Metadata*> mds;
  mds.push_back(getMetadata(context, region.getID()));
  mds.push_back(getMetadata(context, region.getName()));
  mds.push_back(getMetadata(context, region.getLocation()));
  mds.push_back(getMetadata(context, region.getBeginLoc()));
  mds.push_back(getMetadata(context, region.getEndLoc()));
  mds.push_back(getMetadata(context, region.getSourceLang()));

  return llvm::MDNode::get(context, mds);
}

template <>
llvm::Metadata* getMetadata(llvm::LLVMContext&, llvm::Function* f) {
  return llvm::ConstantAsMetadata::get(f);
}

template <typename T>
static llvm::Metadata* getMetadata(llvm::LLVMContext&, T*) {
  moya_error("Should never be called");
  return nullptr;
}

template <typename T>
static llvm::Metadata* getMetadata(llvm::LLVMContext& context, const T& val) {
  llvm::IntegerType* ity = llvm::IntegerType::get(context, sizeof(T) * 8);
  return llvm::ConstantAsMetadata::get(
      llvm::ConstantInt::get(ity, static_cast<int64_t>(val)));
}

static llvm::MDNode* getEntry(llvm::NamedMDNode* nmd, llvm::Type* key) {
  for(llvm::MDNode* md : nmd->operands())
    if(auto* cm = dyn_cast<llvm::ConstantAsMetadata>(md->getOperand(0)))
      if(cm->getType() == key)
        return md;
  return nullptr;
}

static const llvm::MDNode* getEntry(const llvm::NamedMDNode* nmd,
                                    llvm::Type* key) {
  for(const llvm::MDNode* md : nmd->operands())
    if(const auto* cm = dyn_cast<llvm::ConstantAsMetadata>(md->getOperand(0)))
      if(cm->getType() == key)
        return md;
  return nullptr;
}

static llvm::MDNode* getEntry(llvm::NamedMDNode* nmd, const llvm::Value* key) {
  for(llvm::MDNode* md : nmd->operands())
    if(auto* vm = dyn_cast_or_null<llvm::ValueAsMetadata>(md->getOperand(0)))
      if(vm->getValue() == key)
        return md;
  return nullptr;
}

static const llvm::MDNode* getEntry(const llvm::NamedMDNode* nmd,
                                    const llvm::Value* key) {
  for(const llvm::MDNode* md : nmd->operands())
    if(const auto* vm
       = dyn_cast_or_null<llvm::ValueAsMetadata>(md->getOperand(0)))
      if(vm->getValue() == key)
        return md;
  return nullptr;
}

static llvm::MDNode*
getOrSetEntry(llvm::NamedMDNode* nmd, llvm::Type* key, unsigned slots) {
  if(llvm::MDNode* md = getEntry(nmd, key))
    return md;

  llvm::LLVMContext& context = key->getContext();

  std::vector<llvm::Metadata*> mds(
      slots + 1, llvm::MDNode::get(context, llvm::ArrayRef<llvm::Metadata*>()));
  mds[0] = llvm::ConstantAsMetadata::get(llvm::UndefValue::get(key));

  llvm::MDNode* md = llvm::MDNode::get(context, mds);
  nmd->addOperand(md);

  return md;
}

static llvm::MDNode*
getOrSetEntry(llvm::NamedMDNode* nmd, llvm::Value* key, unsigned slots) {
  if(llvm::MDNode* md = getEntry(nmd, key))
    return md;

  llvm::LLVMContext& context = key->getContext();

  std::vector<llvm::Metadata*> mds(
      slots + 1, llvm::MDNode::get(context, llvm::ArrayRef<llvm::Metadata*>()));
  mds[0] = isa<llvm::Constant>(key) ? llvm::ValueAsMetadata::getConstant(key)
                                    : llvm::ValueAsMetadata::get(key);

  llvm::MDNode* md = llvm::MDNode::get(context, mds);
  nmd->addOperand(md);

  return md;
}

static llvm::Metadata*
getRawAttr(const llvm::MDNode* entry, unsigned argno, const std::string& attr) {
  for(const llvm::MDOperand& op :
      dyn_cast<llvm::MDNode>(entry->getOperand(argno))->operands())
    if(dyn_cast<llvm::MDString>(dyn_cast<llvm::MDNode>(op)->getOperand(0))
           ->getString()
       == attr)
      return dyn_cast<llvm::MDNode>(op)->getOperand(1);
  return nullptr;
}

static std::vector<llvm::Metadata*>
getAttrs(const llvm::MDNode* entry, unsigned argno, const std::string& attr) {
  std::vector<llvm::Metadata*> mds;
  for(const llvm::MDOperand& op :
      dyn_cast<llvm::MDNode>(entry->getOperand(argno))->operands())
    if(dyn_cast<llvm::MDString>(dyn_cast<llvm::MDNode>(op)->getOperand(0))
           ->getString()
       != attr)
      mds.push_back(op.get());
  return mds;
}

static bool
hasAttr(const llvm::MDNode* entry, unsigned slot, const std::string& attr) {
  return getRawAttr(entry, slot + 1, attr);
}

template <typename T>
static bool setAttr(llvm::MDNode* entry,
                    unsigned slot,
                    const std::string& attr,
                    const T& val) {
  llvm::LLVMContext& context = entry->getContext();
  std::vector<llvm::Metadata*> mds = getAttrs(entry, slot + 1, attr);
  llvm::Metadata* mdAttr = llvm::MDString::get(context, attr);
  llvm::Metadata* mdVal = getMetadata(context, val);
  mds.push_back(llvm::MDNode::get(context, {mdAttr, mdVal}));
  entry->replaceOperandWith(slot + 1, llvm::MDNode::get(context, mds));
  return true;
}

static bool
setAttr(llvm::MDNode* entry, unsigned slot, const std::string& attr) {
  llvm::LLVMContext& context = entry->getContext();
  llvm::IntegerType* i32 = llvm::IntegerType::get(context, sizeof(int) * 8);
  std::vector<llvm::Metadata*> mds = getAttrs(entry, slot + 1, attr);
  llvm::Metadata* mdAttr = llvm::MDString::get(context, attr);
  llvm::Metadata* cm
      = llvm::ConstantAsMetadata::get(llvm::ConstantInt::get(i32, 1));
  mds.push_back(llvm::MDNode::get(context, {mdAttr, cm}));
  entry->replaceOperandWith(slot + 1, llvm::MDNode::get(context, mds));
  return true;
}

template <>
bool setAttr(llvm::Function& f, const std::string& attr, llvm::Type* val) {
  llvm::NamedMDNode* funcs
      = moya::Metadata::getOrInsertFunctionsNode(moya::LLVMUtils::getModule(f));
  unsigned slots = f.getFunctionType()->getNumParams() + 1;
  return setAttr(getOrSetEntry(funcs, &f, slots), 0, attr, val);
}

template <>
bool setAttr(llvm::Function& f,
             const std::string& attr,
             llvm::StructType* val) {
  return setAttr(f, attr, llvm::cast<llvm::Type>(val));
}

template <>
bool setAttr(llvm::Function& f,
             const std::string& attr,
             const moya::Location& val) {
  llvm::NamedMDNode* funcs
      = moya::Metadata::getOrInsertFunctionsNode(moya::LLVMUtils::getModule(f));
  unsigned slots = f.getFunctionType()->getNumParams() + 1;
  return setAttr(getOrSetEntry(funcs, &f, slots), 0, attr, val);
}

static bool
setAttr(llvm::Function& f, const std::string& attr, llvm::Function* val) {
  llvm::NamedMDNode* funcs
      = moya::Metadata::getOrInsertFunctionsNode(moya::LLVMUtils::getModule(f));
  unsigned slots = f.getFunctionType()->getNumParams() + 1;
  return setAttr(getOrSetEntry(funcs, &f, slots), 0, attr, val);
}

static bool setAttr(llvm::Function& f, const std::string& attr) {
  if(not f.hasFnAttribute(attr)) {
    f.addFnAttr(attr);
    return true;
  }
  return false;
}

template <typename T>
static bool setAttr(llvm::Function& f, const std::string& attr, const T& val) {
  f.addFnAttr(attr, moya::str(val));
  return true;
}

static bool setBackupAttr(llvm::Function& f, const std::string& attr) {
  llvm::NamedMDNode* backups
      = moya::Metadata::getOrInsertBackupFunctionAttributesNode(
          moya::LLVMUtils::getModule(f));
  // We are not likely to set properties to arguments of functions without
  // a definition, so we only need the one slot
  return setAttr(getOrSetEntry(backups, &f, 1), 0, attr);
}

template <typename T>
static bool
setBackupAttr(llvm::Function& f, const std::string& attr, const T& val) {
  llvm::NamedMDNode* backups
      = moya::Metadata::getOrInsertBackupFunctionAttributesNode(
          moya::LLVMUtils::getModule(f));
  // We are not likely to set properties to arguments of functions without
  // a definition, so we only need the one slot
  return setAttr(getOrSetEntry(backups, &f, 1), 0, attr, val);
}

static bool resetAttr(llvm::Function& f, const std::string& key) {
  if(f.hasFnAttribute(key)) {
    f.removeFnAttr(key);
    return true;
  }
  return false;
}

static bool setAttr(llvm::Argument& arg, const std::string& attr) {
  llvm::Function& f = *moya::LLVMUtils::getFunction(arg);
  llvm::LLVMContext& context = f.getContext();
  f.setAttributes(
      f.getAttributes().addAttribute(context, arg.getArgNo() + 1, attr));
  return true;
}

template <typename T>
static bool
setAttr(llvm::Argument& arg, const std::string& attr, const T& val) {
  llvm::Function& f = *moya::LLVMUtils::getFunction(arg);
  llvm::Module& module = *moya::LLVMUtils::getModule(arg);
  llvm::NamedMDNode* funcs = moya::Metadata::getOrInsertFunctionsNode(module);
  unsigned slots = f.getFunctionType()->getNumParams() + 1;
  return setAttr(
      getOrSetEntry(funcs, &f, slots), arg.getArgNo() + 1, attr, val);
}

static bool setAttr(llvm::Instruction* inst, const std::string& attr) {
  return moya::LLVMUtils::setMetadata(inst, attr);
}

static bool resetAttr(llvm::Instruction* inst, const std::string& attr) {
  return moya::LLVMUtils::resetMetadata(inst, attr);
}

template <typename T>
static bool
setAttr(llvm::Instruction* inst, const std::string& attr, const T& val) {
  llvm::LLVMContext& context = inst->getContext();
  inst->setMetadata(attr,
                    llvm::MDNode::get(context, getMetadata(context, val)));
  return true;
}

static bool setAttr(llvm::GlobalVariable& g, const std::string& attr) {
  llvm::Module& module = *moya::LLVMUtils::getModule(g);
  llvm::NamedMDNode* globals = moya::Metadata::getOrInsertGlobalsNode(module);
  return setAttr(getOrSetEntry(globals, &g, 1), 0, attr);
}

template <typename T>
static bool
setAttr(llvm::GlobalVariable& g, const std::string& attr, const T& val) {
  llvm::Module& module = *moya::LLVMUtils::getModule(g);
  llvm::NamedMDNode* globals = moya::Metadata::getOrInsertGlobalsNode(module);
  return setAttr(getOrSetEntry(globals, &g, 1), 0, attr, val);
}

static bool
setAttr(llvm::Type* type, const std::string& attr, llvm::Module& module) {
  llvm::NamedMDNode* structs = moya::Metadata::getOrInsertTypesNode(module);
  return setAttr(getOrSetEntry(structs, type, 1), 0, attr);
}

template <typename T>
static bool setAttr(llvm::Type* type,
                    const std::string& attr,
                    const T& val,
                    llvm::Module& module) {
  llvm::NamedMDNode* types = moya::Metadata::getOrInsertTypesNode(module);
  return setAttr(getOrSetEntry(types, type, 1), 0, attr, val);
}

static llvm::GlobalVariable* getOrSetGlobal(llvm::Module& module,
                                            const std::string& gname) {
  llvm::Type* gtype = llvm::Type::getInt64Ty(module.getContext());
  return dyn_cast<llvm::GlobalVariable>(module.getOrInsertGlobal(gname, gtype));
}

template <>
llvm::Type* getAttr(const llvm::Function& f, const std::string& attr) {
  const llvm::NamedMDNode* fns
      = moya::Metadata::getFunctionsNode(*moya::LLVMUtils::getModule(f));
  return moya::Metadata::getAs<llvm::Type*>(
      getRawAttr(getEntry(fns, &f), 1, attr));
}

template <>
llvm::StructType* getAttr(const llvm::Function& f, const std::string& attr) {
  return dyn_cast<llvm::StructType>(getAttr<llvm::Type*>(f, attr));
}

template <>
llvm::Function* getAttr(const llvm::Function& f, const std::string& attr) {
  const llvm::NamedMDNode* fns
      = moya::Metadata::getFunctionsNode(*moya::LLVMUtils::getModule(f));
  return moya::Metadata::getAs<llvm::Function*>(
      getRawAttr(getEntry(fns, &f), 1, attr));
}

template <>
moya::Location getAttr(const llvm::Function& f, const std::string& attr) {
  const llvm::NamedMDNode* fns
      = moya::Metadata::getFunctionsNode(*moya::LLVMUtils::getModule(f));
  return moya::Metadata::getAs<moya::Location>(
      getRawAttr(getEntry(fns, &f), 1, attr));
}

template <>
std::string getAttr(const llvm::Function& f, const std::string& attr) {
  return f.getFnAttribute(attr).getValueAsString().str();
}

template <typename T>
static T getAttr(const llvm::Function& f, const std::string& attr) {
  return moya::parse<T>(f.getFnAttribute(attr).getValueAsString().str());
}

template <typename T>
static T getAttr(const llvm::Instruction* inst, const std::string& attr) {
  return moya::Metadata::getAs<T>(inst->getMetadata(attr)->getOperand(0));
}

template <typename T>
static T getAttr(const llvm::GlobalVariable& g, const std::string& attr) {
  llvm::NamedMDNode* globals
      = moya::Metadata::getGlobalsNode(moya::LLVMUtils::getModule(g));
  return moya::Metadata::getAs<T>(getRawAttr(getEntry(globals, &g), 1, attr));
}

template <typename T>
static T
getAttr(llvm::Type* type, const std::string& attr, const llvm::Module& module) {
  const llvm::NamedMDNode* types = moya::Metadata::getTypesNode(module);
  return moya::Metadata::getAs<T>(getRawAttr(getEntry(types, type), 1, attr));
}

template <typename T>
static T getAttr(const llvm::Argument& arg, const std::string& attr) {
  const llvm::Function& f = *moya::LLVMUtils::getFunction(arg);
  const llvm::Module& module = *moya::LLVMUtils::getModule(f);
  const llvm::NamedMDNode* funcs = moya::Metadata::getFunctionsNode(module);
  return moya::Metadata::getAs<T>(
      getRawAttr(getEntry(funcs, &f), arg.getArgNo() + 2, attr));
}

template <typename T>
static bool setMDEntry(llvm::NamedMDNode* nmd, const T& entry) {
  llvm::LLVMContext& context = nmd->getParent()->getContext();
  nmd->addOperand(llvm::cast<llvm::MDNode>(getMetadata(context, entry)));

  return true;
}

template <typename T>
moya::Vector<T> getMDEntry(const llvm::NamedMDNode* nmd) {
  moya::Vector<T> ret;
  if(nmd)
    for(const llvm::MDNode* op : nmd->operands())
      ret.push_back(moya::Metadata::getAs<T>(op));
  return ret;
}

static llvm::NamedMDNode* getNode(const llvm::Module& module,
                                  const std::string& attr) {
  return module.getNamedMetadata(attr);
}

static const llvm::GlobalVariable* getGlobal(const llvm::Module& module,
                                             const std::string& gname) {
  return module.getNamedGlobal(gname);
}

static bool hasNode(const llvm::Module& module, const std::string& name) {
  return module.getNamedMetadata(name);
}

static bool hasGlobal(const llvm::Module& module, const std::string& name) {
  return module.getNamedGlobal(name);
}

static bool hasAttr(const llvm::Function& f, const std::string& attr) {
  // Function attributes could either be here or in the metadata node if
  // they have a value
  if(f.hasFnAttribute(attr))
    return true;

  const llvm::Module& module = *moya::LLVMUtils::getModule(f);
  if(const llvm::NamedMDNode* funcs = moya::Metadata::getFunctionsNode(module))
    if(const llvm::MDNode* entry = getEntry(funcs, &f))
      return hasAttr(entry, 0, attr);

  return false;
}

static bool hasAttr(const llvm::Argument& arg, const std::string& attr) {
  const llvm::Function& f = *moya::LLVMUtils::getFunction(arg);
  if(f.getAttributes().hasAttribute(arg.getArgNo() + 1, attr))
    return true;

  const llvm::Module& module = *moya::LLVMUtils::getModule(f);
  if(const llvm::NamedMDNode* funcs = moya::Metadata::getFunctionsNode(module))
    if(const llvm::MDNode* entry = getEntry(funcs, &f))
      return hasAttr(entry, arg.getArgNo() + 1, attr);

  return false;
}

static bool hasAttr(const llvm::Instruction* inst, const std::string& attr) {
  return inst->getMetadata(attr);
}

static bool hasAttr(const llvm::GlobalVariable& g, const std::string& attr) {
  const llvm::Module& module = *moya::LLVMUtils::getModule(g);
  if(const llvm::NamedMDNode* globals = moya::Metadata::getGlobalsNode(module))
    if(const llvm::MDNode* entry = getEntry(globals, &g))
      return hasAttr(entry, 0, attr);
  return false;
}

static bool
hasAttr(llvm::Type* type, const std::string& attr, const llvm::Module& module) {
  if(const llvm::NamedMDNode* types = moya::Metadata::getTypesNode(module))
    if(const llvm::MDNode* entry = getEntry(types, type))
      return hasAttr(entry, 0, attr);
  return false;
}

// Definitions of Metadata methods
#undef MOYA_METADATA_NODE
#undef MOYA_METADATA_NODE_VAL
#undef MOYA_METADATA_FN_ATTR_COMMON
#undef MOYA_METADATA_FN_ATTR_VAL_COMMON
#undef MOYA_METADATA_FN_ATTR
#undef MOYA_METADATA_FN_ATTR_VAL
#undef MOYA_METADATA_FN_ATTR_VALP
#undef MOYA_METADATA_ARG_ATTR_COMMON
#undef MOYA_METADATA_ARG_ATTR_VAL_COMMON
#undef MOYA_METADATA_ARG_ATTR
#undef MOYA_METADATA_ARG_ATTR_VAL
#undef MOYA_METADATA_ARG_ATTR_VALP
#undef MOYA_METADATA_INSTR_ATTR_COMMON
#undef MOYA_METADATA_INSTR_ATTR_VAL_COMMON
#undef MOYA_METADATA_INSTR_ATTR
#undef MOYA_METADATA_INSTR_ATTR_VAL
#undef MOYA_METADATA_INSTR_ATTR_VALP
#undef MOYA_METADATA_GLOBAL_ATTR_COMMON
#undef MOYA_METADATA_GLOBAL_ATTR_VAL_COMMON
#undef MOYA_METADATA_GLOBAL_ATTR
#undef MOYA_METADATA_GLOBAL_ATTR_VAL
#undef MOYA_METADATA_GLOBAL_ATTR_VALP
#undef MOYA_METADATA_TYPE_ATTR_COMMON
#undef MOYA_METADATA_TYPE_ATTR_VAL_COMMON
#undef MOYA_METADATA_TYPE_ATTR
#undef MOYA_METADATA_TYPE_ATTR_VAL
#undef MOYA_METADATA_TYPE_ATTR_VALP
#undef MOYA_METADATA_GLOBAL

#define MOYA_METADATA_NODE(BASE)                                     \
  std::string moya::Metadata::get##BASE##NodeName() {                \
    return getMoyaNodeName(#BASE);                                   \
  }                                                                  \
                                                                     \
  bool moya::Metadata::has##BASE##Node(const llvm::Module& module) { \
    return hasNode(module, get##BASE##NodeName());                   \
  }                                                                  \
                                                                     \
  bool moya::Metadata::has##BASE##Node(const llvm::Module* module) { \
    return has##BASE##Node(*module);                                 \
  }                                                                  \
                                                                     \
  llvm::NamedMDNode* moya::Metadata::getOrInsert##BASE##Node(        \
      llvm::Module& module) {                                        \
    return module.getOrInsertNamedMetadata(get##BASE##NodeName());   \
  }                                                                  \
                                                                     \
  llvm::NamedMDNode* moya::Metadata::getOrInsert##BASE##Node(        \
      llvm::Module* module) {                                        \
    return getOrInsert##BASE##Node(*module);                         \
  }                                                                  \
                                                                     \
  llvm::NamedMDNode* moya::Metadata::get##BASE##Node(                \
      const llvm::Module& module) {                                  \
    return getNode(module, get##BASE##NodeName());                   \
  }                                                                  \
                                                                     \
  llvm::NamedMDNode* moya::Metadata::get##BASE##Node(                \
      const llvm::Module* module) {                                  \
    return get##BASE##Node(*module);                                 \
  }

#define MOYA_METADATA_NODE_VAL(BASE, TYPE)                                   \
  std::string moya::Metadata::get##BASE##NodeName() {                        \
    return getMoyaNodeName(#BASE);                                           \
  }                                                                          \
                                                                             \
  bool moya::Metadata::has##BASE##Node(const llvm::Module& module) {         \
    return hasNode(module, get##BASE##NodeName());                           \
  }                                                                          \
                                                                             \
  bool moya::Metadata::has##BASE##Node(const llvm::Module* module) {         \
    return has##BASE##Node(*module);                                         \
  }                                                                          \
                                                                             \
  llvm::NamedMDNode* moya::Metadata::getOrInsert##BASE##Node(                \
      llvm::Module& module) {                                                \
    return module.getOrInsertNamedMetadata(get##BASE##NodeName());           \
  }                                                                          \
                                                                             \
  llvm::NamedMDNode* moya::Metadata::getOrInsert##BASE##Node(                \
      llvm::Module* module) {                                                \
    return getOrInsert##BASE##Node(*module);                                 \
  }                                                                          \
                                                                             \
  llvm::NamedMDNode* moya::Metadata::get##BASE##Node(                        \
      const llvm::Module& module) {                                          \
    return getNode(module, get##BASE##NodeName());                           \
  }                                                                          \
                                                                             \
  llvm::NamedMDNode* moya::Metadata::get##BASE##Node(                        \
      const llvm::Module* module) {                                          \
    return get##BASE##Node(*module);                                         \
  }                                                                          \
                                                                             \
  bool moya::Metadata::add##BASE(llvm::Module& module, const TYPE& entry) {  \
    return setMDEntry(getOrInsert##BASE##Node(module), entry);               \
  }                                                                          \
                                                                             \
  bool moya::Metadata::add##BASE(llvm::Module* module, const TYPE& entry) {  \
    return add##BASE(*module, entry);                                        \
  }                                                                          \
                                                                             \
  moya::Vector<TYPE> moya::Metadata::get##BASE(const llvm::Module& module) { \
    return getMDEntry<TYPE>(get##BASE##Node(module));                        \
  }                                                                          \
                                                                             \
  moya::Vector<TYPE> moya::Metadata::get##BASE(const llvm::Module* module) { \
    return get##BASE(*module);                                               \
  }

// Configuration macro for llvm::Function attributes which don't have a value
// This is for those attributes that are essentially flags
//
// Functions without a definition lose their attributes when written to
// and read from disk. So we stash it in another NamedMetadata node so we
// can restore the when needed
//
#define MOYA_METADATA_FN_ATTR_COMMON(BASE)                          \
  bool moya::Metadata::is##BASE##FnAttrName(llvm::StringRef name) { \
    return name == get##BASE##FnAttrName();                         \
  }                                                                 \
                                                                    \
  std::string moya::Metadata::get##BASE##FnAttrName() {             \
    return getMoyaAttrName(#BASE);                                  \
  }                                                                 \
                                                                    \
  bool moya::Metadata::has##BASE(const llvm::Function& f) {         \
    return hasAttr(f, get##BASE##FnAttrName());                     \
  }                                                                 \
                                                                    \
  bool moya::Metadata::has##BASE(const llvm::Function* f) {         \
    return hasAttr(*f, get##BASE##FnAttrName());                    \
  }

#define MOYA_METADATA_FN_ATTR(BASE)                     \
  MOYA_METADATA_FN_ATTR_COMMON(BASE)                    \
                                                        \
  bool moya::Metadata::set##BASE(llvm::Function& f) {   \
    if(not f.size())                                    \
      setBackupAttr(f, get##BASE##FnAttrName());        \
    return setAttr(f, get##BASE##FnAttrName());         \
  }                                                     \
                                                        \
  bool moya::Metadata::set##BASE(llvm::Function* f) {   \
    return setAttr(*f, get##BASE##FnAttrName());        \
  }                                                     \
                                                        \
  bool moya::Metadata::reset##BASE(llvm::Function& f) { \
    return resetAttr(f, get##BASE##FnAttrName());       \
  }                                                     \
                                                        \
  bool moya::Metadata::reset##BASE(llvm::Function* f) { \
    return resetAttr(*f, get##BASE##FnAttrName());      \
  }

// Configuration macro for Function attributes that have a value.
//
// TYPE specifies the type of the value of the attribute.
//
// Functions without a definition lose their attributes when written to
// and read from disk. So we stash it in another NamedMetadata node so we
// can restore the when needed
//
#define MOYA_METADATA_FN_ATTR_VAL_COMMON(BASE, VAL_TYPE)        \
  MOYA_METADATA_FN_ATTR_COMMON(BASE)                            \
                                                                \
  VAL_TYPE moya::Metadata::get##BASE(const llvm::Function& f) { \
    return getAttr<VAL_TYPE>(f, get##BASE##FnAttrName());       \
  }                                                             \
                                                                \
  VAL_TYPE moya::Metadata::get##BASE(const llvm::Function* f) { \
    return getAttr<VAL_TYPE>(*f, get##BASE##FnAttrName());      \
  }

#define MOYA_METADATA_FN_ATTR_VAL(BASE, VAL_TYPE)                 \
  MOYA_METADATA_FN_ATTR_VAL_COMMON(BASE, VAL_TYPE)                \
                                                                  \
  bool moya::Metadata::set##BASE(                                 \
      llvm::Function& f, const VAL_TYPE& val, bool check) {       \
    if(check and has##BASE(f) and (get##BASE(f) != val))          \
      moya_error("Overwriting existing function attribute:\n"     \
                 << "  attr: " << get##BASE##FnAttrName() << "\n" \
                 << "  func: " << f.getName() << "\n"             \
                 << "   old: " << moya::str(get##BASE(f)) << "\n" \
                 << "   new: " << moya::str(val));                \
    if(not f.size())                                              \
      setBackupAttr(f, get##BASE##FnAttrName(), val);             \
    if((f.getName() == "_GLOBAL__sub_I_pc2fileinfo.C")            \
       and (std::string(#BASE) == "Model"))                       \
      moya_error("Boom!");                                        \
    return setAttr(f, get##BASE##FnAttrName(), val);              \
  }                                                               \
                                                                  \
  bool moya::Metadata::set##BASE(                                 \
      llvm::Function* f, const VAL_TYPE& val, bool check) {       \
    return set##BASE(*f, val, check);                             \
  }

#define MOYA_METADATA_FN_ATTR_VALP(BASE, VAL_TYPE)                      \
  MOYA_METADATA_FN_ATTR_VAL_COMMON(BASE, VAL_TYPE)                      \
                                                                        \
  bool moya::Metadata::set##BASE(                                       \
      llvm::Function& f, VAL_TYPE val, bool check) {                    \
    if(check and has##BASE(f) and (get##BASE(f) != val))                \
      moya_error("Overwriting existing attribute '"                     \
                 << get##BASE##FnAttrName() << "' in " << f.getName()); \
    if(not f.size())                                                    \
      setBackupAttr(f, get##BASE##FnAttrName(), val);                   \
    return setAttr(f, get##BASE##FnAttrName(), val);                    \
  }                                                                     \
                                                                        \
  bool moya::Metadata::set##BASE(                                       \
      llvm::Function* f, VAL_TYPE val, bool check) {                    \
    return set##BASE(*f, val, check);                                   \
  }

// Configuration macro for Argument attributes
// This is for those attributes that are just flags
//
#define MOYA_METADATA_ARG_ATTR_COMMON(BASE)                   \
  std::string moya::Metadata::get##BASE##ArgAttrName() {      \
    return getMoyaAttrName(#BASE);                            \
  }                                                           \
                                                              \
  bool moya::Metadata::has##BASE(const llvm::Argument& arg) { \
    return hasAttr(arg, getMoyaAttrName(#BASE));              \
  }                                                           \
                                                              \
  bool moya::Metadata::has##BASE(const llvm::Argument* arg) { \
    return hasAttr(*arg, getMoyaAttrName(#BASE));             \
  }

#define MOYA_METADATA_ARG_ATTR(BASE)                    \
  MOYA_METADATA_ARG_ATTR_COMMON(BASE)                   \
                                                        \
  bool moya::Metadata::set##BASE(llvm::Argument& arg) { \
    return setAttr(arg, getMoyaAttrName(#BASE));        \
  }                                                     \
                                                        \
  bool moya::Metadata::set##BASE(llvm::Argument* arg) { \
    return setAttr(*arg, getMoyaAttrName(#BASE));       \
  }

#define MOYA_METADATA_ARG_ATTR_VAL_COMMON(BASE, VAL_TYPE)         \
  MOYA_METADATA_ARG_ATTR_COMMON(BASE)                             \
                                                                  \
  VAL_TYPE moya::Metadata::get##BASE(const llvm::Argument& arg) { \
    return getAttr<VAL_TYPE>(arg, getMoyaAttrName(#BASE));        \
  }                                                               \
                                                                  \
  VAL_TYPE moya::Metadata::get##BASE(const llvm::Argument* arg) { \
    return getAttr<VAL_TYPE>(*arg, getMoyaAttrName(#BASE));       \
  }

#define MOYA_METADATA_ARG_ATTR_VAL(BASE, VAL_TYPE)                        \
  MOYA_METADATA_ARG_ATTR_VAL_COMMON(BASE, VAL_TYPE)                       \
                                                                          \
  bool moya::Metadata::set##BASE(                                         \
      llvm::Argument& arg, const VAL_TYPE& val, bool check) {             \
    if(check and has##BASE(arg) and (get##BASE(arg) != val))              \
      moya_error("Overwriting existing argument attribute:\n"             \
                 << "  attr: " << get##BASE##ArgAttrName() << "\n"        \
                 << "   arg: " << arg.getArgNo() << "\n"                  \
                 << "  func: " << moya::LLVMUtils::getFunctionName(arg)); \
    return setAttr(arg, getMoyaAttrName(#BASE), val);                     \
  }                                                                       \
                                                                          \
  bool moya::Metadata::set##BASE(                                         \
      llvm::Argument* arg, const VAL_TYPE& val, bool check) {             \
    return set##BASE(*arg, val, check);                                   \
  }

#define MOYA_METADATA_ARG_ATTR_VALP(BASE, VAL_TYPE)              \
  MOYA_METADATA_ARG_ATTR_VAL_COMMON(BASE, VAL_TYPE)              \
                                                                 \
  bool moya::Metadata::set##BASE(                                \
      llvm::Argument& arg, VAL_TYPE val, bool check) {           \
    if(check and has##BASE(arg) and (get##BASE(arg) != val))     \
      moya_error("Overwriting existing attribute '"              \
                 << get##BASE##ArgAttrName() << "' in argument " \
                 << arg.getArgNo() << " of "                     \
                 << moya::LLVMUtils::getFunctionName(arg));      \
    return setAttr(arg, getMoyaAttrName(#BASE), val);            \
  }                                                              \
                                                                 \
  bool moya::Metadata::set##BASE(                                \
      llvm::Argument* arg, VAL_TYPE val, bool check) {           \
    return set##BASE(*arg, val, check);                          \
  }

// Configuration macro for Instruction attributes
//
#define MOYA_METADATA_INSTR_ATTR_COMMON(BASE)                     \
  std::string moya::Metadata::get##BASE##InstrAttrName() {        \
    return getMoyaAttrName(#BASE);                                \
  }                                                               \
                                                                  \
  bool moya::Metadata::has##BASE(const llvm::Instruction& inst) { \
    return hasAttr(&inst, get##BASE##InstrAttrName());            \
  }                                                               \
                                                                  \
  bool moya::Metadata::has##BASE(const llvm::Instruction* inst) { \
    return hasAttr(inst, get##BASE##InstrAttrName());             \
  }                                                               \
                                                                  \
  bool moya::Metadata::reset##BASE(llvm::Instruction& inst) {     \
    return resetAttr(&inst, get##BASE##InstrAttrName());          \
  }                                                               \
                                                                  \
  bool moya::Metadata::reset##BASE(llvm::Instruction* inst) {     \
    return resetAttr(inst, get##BASE##InstrAttrName());           \
  }

#define MOYA_METADATA_INSTR_ATTR(BASE)                      \
  MOYA_METADATA_INSTR_ATTR_COMMON(BASE)                     \
                                                            \
  bool moya::Metadata::set##BASE(llvm::Instruction& inst) { \
    return setAttr(&inst, get##BASE##InstrAttrName());      \
  }                                                         \
                                                            \
  bool moya::Metadata::set##BASE(llvm::Instruction* inst) { \
    return setAttr(inst, get##BASE##InstrAttrName());       \
  }

#define MOYA_METADATA_INSTR_ATTR_VAL_COMMON(BASE, VAL_TYPE)           \
  MOYA_METADATA_INSTR_ATTR_COMMON(BASE)                               \
                                                                      \
  VAL_TYPE moya::Metadata::get##BASE(const llvm::Instruction& inst) { \
    return getAttr<VAL_TYPE>(&inst, get##BASE##InstrAttrName());      \
  }                                                                   \
                                                                      \
  VAL_TYPE moya::Metadata::get##BASE(const llvm::Instruction* inst) { \
    return getAttr<VAL_TYPE>(inst, get##BASE##InstrAttrName());       \
  }

#define MOYA_METADATA_INSTR_ATTR_VAL(BASE, VAL_TYPE)                     \
  MOYA_METADATA_INSTR_ATTR_VAL_COMMON(BASE, VAL_TYPE)                    \
                                                                         \
  bool moya::Metadata::set##BASE(                                        \
      llvm::Instruction& inst, const VAL_TYPE& val, bool check) {        \
    return set##BASE(&inst, val, check);                                 \
  }                                                                      \
                                                                         \
  bool moya::Metadata::set##BASE(                                        \
      llvm::Instruction* inst, const VAL_TYPE& val, bool check) {        \
    if(check and has##BASE(inst) and (get##BASE(inst) != val))           \
      moya_error("Overwriting existing instruction attribute:\n"         \
                 << "  attr: " << get##BASE##InstrAttrName() << "\n"     \
                 << "  inst: " << *inst << "\n"                          \
                 << "  func: " << moya::LLVMUtils::getFunctionName(inst) \
                 << "\n"                                                 \
                 << "   old: " << moya::str(get##BASE(inst)) << "\n"     \
                 << "   new: " << moya::str(val));                       \
    return setAttr(inst, get##BASE##InstrAttrName(), val);               \
  }

#define MOYA_METADATA_INSTR_ATTR_VALP(BASE, VAL_TYPE)                    \
  MOYA_METADATA_INSTR_ATTR_VAL_COMMON(BASE, VAL_TYPE)                    \
                                                                         \
  bool moya::Metadata::set##BASE(                                        \
      llvm::Instruction& inst, VAL_TYPE val, bool check) {               \
    return set##BASE(&inst, val, check);                                 \
  }                                                                      \
                                                                         \
  bool moya::Metadata::set##BASE(                                        \
      llvm::Instruction* inst, VAL_TYPE val, bool check) {               \
    if(check and has##BASE(inst) and (get##BASE(inst) != val))           \
      moya_error("Overwriting existing attribute\n"                      \
                 << "  attr: " << get##BASE##InstrAttrName() << "\n"     \
                 << "  inst: " << *inst << "\n"                          \
                 << "  func: " << moya::LLVMUtils::getFunctionName(inst) \
                 << "\n"                                                 \
                 << "   old: " << *get##BASE(inst) << "\n"               \
                 << "   new: " << *val);                                 \
    return setAttr(inst, get##BASE##InstrAttrName(), val);               \
  }

// Configuration macros for global variable attributes
//
#define MOYA_METADATA_GLOBAL_ATTR_COMMON(BASE)                    \
  std::string moya::Metadata::get##BASE##GlobalAttrName() {       \
    return getMoyaAttrName(#BASE);                                \
  }                                                               \
                                                                  \
  bool moya::Metadata::has##BASE(const llvm::GlobalVariable& g) { \
    return hasAttr(g, getMoyaAttrName(#BASE));                    \
  }                                                               \
                                                                  \
  bool moya::Metadata::has##BASE(const llvm::GlobalVariable* g) { \
    return hasAttr(*g, getMoyaAttrName(#BASE));                   \
  }

#define MOYA_METADATA_GLOBAL_ATTR(BASE)                     \
  MOYA_METADATA_GLOBAL_ATTR_COMMON(BASE)                    \
                                                            \
  bool moya::Metadata::set##BASE(llvm::GlobalVariable& g) { \
    return setAttr(g, getMoyaAttrName(#BASE));              \
  }                                                         \
                                                            \
  bool moya::Metadata::set##BASE(llvm::GlobalVariable* g) { \
    return setAttr(*g, getMoyaAttrName(#BASE));             \
  }

#define MOYA_METADATA_GLOBAL_ATTR_VAL_COMMON(BASE, VAL_TYPE)          \
  MOYA_METADATA_GLOBAL_ATTR_COMMON(BASE)                              \
                                                                      \
  VAL_TYPE moya::Metadata::get##BASE(const llvm::GlobalVariable& g) { \
    return getAttr<VAL_TYPE>(g, getMoyaAttrName(#BASE));              \
  }                                                                   \
                                                                      \
  VAL_TYPE moya::Metadata::get##BASE(const llvm::GlobalVariable* g) { \
    return getAttr<VAL_TYPE>(*g, getMoyaAttrName(#BASE));             \
  }

#define MOYA_METADATA_GLOBAL_ATTR_VAL(BASE, VAL_TYPE)                 \
  MOYA_METADATA_GLOBAL_ATTR_VAL_COMMON(BASE, VAL_TYPE)                \
                                                                      \
  bool moya::Metadata::set##BASE(                                     \
      llvm::GlobalVariable& g, const VAL_TYPE& val, bool check) {     \
    if(check and has##BASE(g) and (get##BASE(g) != val))              \
      moya_error("Overwriting existing global attribute:\n"           \
                 << "  attr: " << get##BASE##GlobalAttrName() << "\n" \
                 << "  glob: " << g.getName() << "\n"                 \
                 << "   old: " << moya::str(get##BASE(g)) << "\n"     \
                 << "   new: " << moya::str(val));                    \
    return setAttr(g, getMoyaAttrName(#BASE), val);                   \
  }                                                                   \
                                                                      \
  bool moya::Metadata::set##BASE(                                     \
      llvm::GlobalVariable* g, const VAL_TYPE& val, bool check) {     \
    return set##BASE(*g, val, check);                                 \
  }

#define MOYA_METADATA_GLOBAL_ATTR_VALP(BASE, VAL_TYPE)                      \
  MOYA_METADATA_GLOBAL_ATTR_VAL_COMMON(BASE, VAL_TYPE)                      \
                                                                            \
  bool moya::Metadata::set##BASE(                                           \
      llvm::GlobalVariable& g, VAL_TYPE val, bool check) {                  \
    if(check and has##BASE(g) and (get##BASE(g) != val))                    \
      moya_error("Overwriting existing attribute '"                         \
                 << get##BASE##GlobalAttrName() << "' in " << g.getName()); \
    return setAttr(g, getMoyaAttrName(#BASE), val);                         \
  }                                                                         \
                                                                            \
  bool moya::Metadata::set##BASE(                                           \
      llvm::GlobalVariable* g, VAL_TYPE val, bool check) {                  \
    return set##BASE(*g, val, check);                                       \
  }

// Configuration macro for Type attributes
//
#define MOYA_METADATA_TYPE_ATTR_COMMON(BASE)                   \
  std::string moya::Metadata::get##BASE##TypeAttrName() {      \
    return getMoyaAttrName(#BASE);                             \
  }                                                            \
                                                               \
  bool moya::Metadata::has##BASE(llvm::Type* type,             \
                                 const llvm::Module& module) { \
    return hasAttr(type, getMoyaAttrName(#BASE), module);      \
  }

#define MOYA_METADATA_TYPE_ATTR(BASE)                                      \
  MOYA_METADATA_TYPE_ATTR_COMMON(BASE)                                     \
                                                                           \
  bool moya::Metadata::set##BASE(llvm::Type* type, llvm::Module& module) { \
    return setAttr(type, getMoyaAttrName(#BASE), module);                  \
  }

#define MOYA_METADATA_TYPE_ATTR_VAL_COMMON(BASE, VAL_TYPE)          \
  MOYA_METADATA_TYPE_ATTR_COMMON(BASE)                              \
                                                                    \
  VAL_TYPE moya::Metadata::get##BASE(llvm::Type* type,              \
                                     const llvm::Module& module) {  \
    return getAttr<VAL_TYPE>(type, getMoyaAttrName(#BASE), module); \
  }

#define MOYA_METADATA_TYPE_ATTR_VAL(BASE, VAL_TYPE)                            \
  MOYA_METADATA_TYPE_ATTR_VAL_COMMON(BASE, VAL_TYPE)                           \
                                                                               \
  bool moya::Metadata::set##BASE(llvm::Type* type,                             \
                                 const VAL_TYPE& val,                          \
                                 llvm::Module& module,                         \
                                 bool check) {                                 \
    if(check and has##BASE(type, module) and (get##BASE(type, module) != val)) \
      moya_error("Overwriting existing attribute '"                            \
                 << get##BASE##TypeAttrName() << "' in " << *type);            \
    return setAttr(type, getMoyaAttrName(#BASE), val, module);                 \
  }

#define MOYA_METADATA_TYPE_ATTR_VALP(BASE, VAL_TYPE)                           \
  MOYA_METADATA_TYPE_ATTR_VAL_COMMON(BASE, VAL_TYPE)                           \
                                                                               \
  bool moya::Metadata::set##BASE(                                              \
      llvm::Type* type, VAL_TYPE val, llvm::Module& module, bool check) {      \
    if(check and has##BASE(type, module) and (get##BASE(type, module) != val)) \
      moya_error("Overwriting existing attribute '"                            \
                 << get##BASE##TypeAttrName() << "' in " << *type);            \
    return setAttr(type, getMoyaAttrName(#BASE), val, module);                 \
  }

// Configuration macro for Moya-specific global variables. These are not
// dummy variables used for the analysis but actually used by the runtime
// system
//
#define MOYA_METADATA_GLOBAL(BASE)                                     \
  std::string moya::Metadata::get##BASE##GlobalName() {                \
    return getMoyaGlobalName(#BASE);                                   \
  }                                                                    \
                                                                       \
  bool moya::Metadata::has##BASE##Global(const llvm::Module& module) { \
    return hasGlobal(module, get##BASE##GlobalName());                 \
  }                                                                    \
                                                                       \
  llvm::GlobalVariable* moya::Metadata::getOrInsert##BASE##Global(     \
      llvm::Module& module) {                                          \
    return getOrSetGlobal(module, get##BASE##GlobalName());            \
  }                                                                    \
                                                                       \
  const llvm::GlobalVariable* moya::Metadata::get##BASE##Global(       \
      const llvm::Module& module) {                                    \
    return getGlobal(module, get##BASE##GlobalName());                 \
  }

#include "MoyaAttributes.inc"

#undef MOYA_METADATA_NODE
#undef MOYA_METADATA_NODE_VAL
#undef MOYA_METADATA_FN_ATTR_COMMON
#undef MOYA_METADATA_FN_ATTR_VAL_COMMON
#undef MOYA_METADATA_FN_ATTR
#undef MOYA_METADATA_FN_ATTR_VAL
#undef MOYA_METADATA_FN_ATTR_VALP
#undef MOYA_METADATA_ARG_ATTR_COMMON
#undef MOYA_METADATA_ARG_ATTR_VAL_COMMON
#undef MOYA_METADATA_ARG_ATTR
#undef MOYA_METADATA_ARG_ATTR_VAL
#undef MOYA_METADATA_ARG_ATTR_VALP
#undef MOYA_METADATA_INSTR_ATTR_COMMON
#undef MOYA_METADATA_INSTR_ATTR_VAL_COMMON
#undef MOYA_METADATA_INSTR_ATTR
#undef MOYA_METADATA_INSTR_ATTR_VAL
#undef MOYA_METADATA_INSTR_ATTR_VALP
#undef MOYA_METADATA_GLOBAL_ATTR_COMMON
#undef MOYA_METADATA_GLOBAL_ATTR_VAL_COMMON
#undef MOYA_METADATA_GLOBAL_ATTR
#undef MOYA_METADATA_GLOBAL_ATTR_VAL
#undef MOYA_METADATA_GLOBAL_ATTR_VALP
#undef MOYA_METADATA_TYPE_ATTR_COMMON
#undef MOYA_METADATA_TYPE_ATTR_VAL_COMMON
#undef MOYA_METADATA_TYPE_ATTR
#undef MOYA_METADATA_TYPE_ATTR_VAL
#undef MOYA_METADATA_TYPE_ATTR_VALP
#undef MOYA_METADATA_GLOBAL

moya::Set<std::string> moya::Metadata::getOptimizationAttributeNames() {
  return {getSummaryAddressInstrAttrName(),
          getAnalysisTypeInstrAttrName(),
          getAnalysisTypeArgAttrName(),
          getAnalysisTypeGlobalAttrName(),
          getGEPIndexInstrAttrName(),
          getIDInstrAttrName()};
}

bool moya::Metadata::isMoyaAttributeName(const std::string& attr) {
  return attr.find(getMoyaPrefix()) == 0;
}

template int32_t moya::Metadata::getAs(const llvm::Metadata*);
template uint32_t moya::Metadata::getAs(const llvm::Metadata*);
template int64_t moya::Metadata::getAs(const llvm::Metadata*);
template uint64_t moya::Metadata::getAs(const llvm::Metadata*);
