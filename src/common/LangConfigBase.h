#ifndef MOYA_COMMON_LANG_CONFIG_BASE_H
#define MOYA_COMMON_LANG_CONFIG_BASE_H

#include "moya/Set.h"

class LangConfigBase {
protected:
  moya::Set<std::string> exts;

protected:
  LangConfigBase(const std::moya::Set<std::string>&);

public:
  LangConfigBase() = delete;

  static bool hasValidExtension(const std::string&);
};

#endif // MOYA_COMMON_LANG_CONFIG_BASE_H
