#ifndef MOYA_COMMON_DEMANGLER_H
#define MOYA_COMMON_DEMANGLER_H

#include "SourceLang.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/GlobalVariable.h>

#include <string>

namespace moya {

// We create a separate class here because the GNU demangler has lousy
// memory management so it's better to do it ourselves for C++. If we ever need
// one for Fortran, we can subclass this
class Demangler {
public:
  enum Action {
    // Gets the full demangled name with templates, namespaces and arguments
    Full,

    // Gets the qualified name without templates
    Qualified,

    // Gets the source name as it appears in the text without any namespaces
    // or class/module names
    Source,

    // Gets the name suitable for a model. This typically strips namespaces
    // but keeps arguments
    Model,
  };

protected:
  SourceLang lang;

protected:
  Demangler(SourceLang);

public:
  virtual ~Demangler() = default;

  SourceLang getLang() const;

  virtual std::string demangle(const llvm::Function&,
                               Demangler::Action) const = 0;
  virtual std::string demangle(const llvm::GlobalVariable&,
                               Demangler::Action) const = 0;
};

// Demangle the function name. Makes life a lot easier when debugging C++
std::string demangle(const llvm::Function*,
                     Demangler::Action = Demangler::Action::Full);
std::string demangle(const llvm::Function&,
                     Demangler::Action = Demangler::Action::Full);
std::string demangle(const llvm::GlobalVariable*,
                     Demangler::Action = Demangler::Action::Full);
std::string demangle(const llvm::GlobalVariable&,
                     Demangler::Action = Demangler::Action::Full);

} // namespace moya

#endif // MOYA_COMMON_DEMANGLER_H
