#ifndef MOYA_COMMON_FILESYSTEM_UTILS_H
#define MOYA_COMMON_FILESYSTEM_UTILS_H

#include "Vector.h"

#include <string>

namespace moya {

namespace FileSystemUtils {

unsigned long getFileSize(const std::string&);
Vector<std::string> ls(const std::string&);
std::string readlink(const std::string&);
std::string cwd();
void rm(const std::string&);
void mkdir(const std::string&);
std::string getTmpDir();
bool exists(const std::string&);

} // namespace FileSystemUtils

} // namespace moya

#endif // MOYA_COMMON_FILESYSTEM_UTILS_H
