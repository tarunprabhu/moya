#include "SourceLang.h"
#include "Diagnostics.h"
#include "Map.h"
#include "Stringify.h"

static const moya::Map<std::string, SourceLang> str2lang
    = {{"c", SourceLang::C},
       {"cxx", SourceLang::CXX},
       {"fort", SourceLang::Fort},
       {"f77", SourceLang::F77},
       {"objc", SourceLang::ObjC},
       {"objcxx", SourceLang::ObjCXX},
       {"cu", SourceLang::Cuda},
       {"<unknown>", SourceLang::UnknownLang}};

SourceLang parseSourceLang(const std::string& s) {
  if(str2lang.contains(s))
    return str2lang.at(s);
  return SourceLang::UnknownLang;
}

void operator>>(std::stringstream& ss, SourceLang& lang) {
  lang = parseSourceLang(ss.str());
}

bool isC(SourceLang lang) {
  return lang == SourceLang::C;
}

bool isCXX(SourceLang lang) {
  return lang == SourceLang::CXX;
}

bool isFortran(SourceLang lang) {
  return (lang == SourceLang::F77) or (lang == SourceLang::Fort);
}

bool isCuda(SourceLang lang) {
  return lang == SourceLang::Cuda;
}

namespace moya {

template <>
std::string str(const SourceLang& lang) {
  for(const auto& it : str2lang)
    if(it.second == lang)
      return it.first;
  moya_error("Unsupported language: " << lang);
  return str(SourceLang::UnknownLang);
}

template <>
SourceLang parse(const std::string& s) {
  if(str2lang.contains(s))
    return str2lang.at(s);
  moya_error("Unknown language: " << s);
  return SourceLang::UnknownLang;
}

} // namespace moya
