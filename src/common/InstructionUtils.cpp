#include "Diagnostics.h"
#include "LLVMUtils.h"
#include "Metadata.h"

#include <llvm/IR/Constants.h>
#include <llvm/IR/Instructions.h>

using namespace llvm;

namespace moya {

namespace LLVMUtils {

// We need this function because for some reasons, there are bitcast
// instructions in the callsite which make it tricky to get the actual
// function out of it.
template <typename T>
Function* getCalledFunction(T* call) {
  if(auto* f = dyn_cast<Function>(call->getCalledValue()))
    return f;
  Value* called = call->getCalledValue();
  if(auto* cexpr = dyn_cast<ConstantExpr>(called))
    if(auto* f = dyn_cast<Function>(cexpr->getOperand(0)))
      return f;
  if(auto* cst = dyn_cast<CastInst>(called))
    if(auto* f = dyn_cast<Function>(cst->getOperand(0)))
      return f;
  return nullptr;
}

template <typename T>
const Function* getCalledFunction(const T* call) {
  if(const auto* f = dyn_cast<Function>(call->getCalledValue()))
    return f;
  const Value* called = call->getCalledValue();
  if(const auto* cexpr = dyn_cast<ConstantExpr>(called))
    if(const auto* f = dyn_cast<Function>(cexpr->getOperand(0)))
      return f;
  if(const auto* cst = dyn_cast<CastInst>(called))
    if(const auto* f = dyn_cast<Function>(cst->getOperand(0)))
      return f;
  return nullptr;
}

template Function* getCalledFunction(CallInst*);
template Function* getCalledFunction(InvokeInst*);
template const Function* getCalledFunction(const CallInst*);
template const Function* getCalledFunction(const InvokeInst*);

Function* getCalledFunction(Instruction* inst, bool strict) {
  if(auto* call = dyn_cast<CallInst>(inst))
    return getCalledFunction(call);
  else if(auto* invoke = dyn_cast<InvokeInst>(inst))
    return getCalledFunction(invoke);
  else if(not strict)
    return nullptr;

  moya_error("Not a call instruction: " << *inst);
}

const Function* getCalledFunction(const Instruction* inst, bool strict) {
  if(const auto* call = dyn_cast<CallInst>(inst))
    return getCalledFunction(call);
  else if(const auto* invoke = dyn_cast<InvokeInst>(inst))
    return getCalledFunction(invoke);
  else if(not strict)
    return nullptr;

  moya_error("Not a call instruction: " << *inst);
}

Value* getArgOperand(Instruction* inst, unsigned op) {
  if(auto* call = dyn_cast<CallInst>(inst))
    return call->getArgOperand(op);
  else if(auto* invoke = dyn_cast<InvokeInst>(inst))
    return invoke->getArgOperand(op);
  return nullptr;
}

const Value* getArgOperand(const Instruction* inst, unsigned op) {
  if(const auto* call = dyn_cast<CallInst>(inst))
    return call->getArgOperand(op);
  else if(const auto* invoke = dyn_cast<InvokeInst>(inst))
    return invoke->getArgOperand(op);
  return nullptr;
}

Type* getArgOperandType(const Instruction* inst, unsigned op) {
  return getAnalysisType(getArgOperand(inst, op));
}

Type* getAnalysisType(const Instruction* inst) {
  Type* ret = inst->getType();
  if(moya::Metadata::hasAnalysisType(inst))
    ret = moya::Metadata::getAnalysisType(inst);

  if(const Module* module = getModule(inst))
    return getCanonicalType(ret, *module);
  return ret;
}

const Module* getModule(const Instruction* inst) {
  if(not inst)
    return nullptr;
  if(const Function* f = getFunction(inst))
    return f->getParent();
  return nullptr;
}

Module* getModule(Instruction* inst) {
  if(not inst)
    return nullptr;
  if(Function* f = getFunction(inst))
    return f->getParent();
  return nullptr;
}

const Function* getFunction(const Instruction* inst) {
  if(const BasicBlock* bb = inst->getParent())
    return bb->getParent();
  return nullptr;
}

Function* getFunction(Instruction* inst) {
  if(BasicBlock* bb = inst->getParent())
    return bb->getParent();
  return nullptr;
}

StringRef getFunctionName(const Instruction* inst) {
  if(not inst)
    return "Cannot get function name: null instruction";
  else if(not inst->getParent())
    return "Cannot get function name: non-embedded instruction";
  return getFunction(inst)->getName();
}

long getArgOperandAsI64(const CallInst* call, unsigned argno) {
  return cast<ConstantInt>(call->getArgOperand(argno))->getLimitedValue();
}

long getArgOperandAsI64(const InvokeInst* call, unsigned argno) {
  return cast<ConstantInt>(call->getArgOperand(argno))->getLimitedValue();
}

bool arePHINodeIncomingValuesEqual(const PHINode* phi) {
  auto* op0 = dyn_cast<Instruction>(phi->getIncomingValue(0));
  if(not op0)
    return false;
  unsigned opcode = op0->getOpcode();
  Vector<Value*> ops;
  for(unsigned j = 0; j < op0->getNumOperands(); j++)
    ops.push_back(op0->getOperand(j));
  for(unsigned i = 1; i < phi->getNumIncomingValues(); i++) {
    auto* opi = dyn_cast<Instruction>(phi->getIncomingValue(i));
    if(not opi)
      return false;
    if(opi->getOpcode() != opcode)
      return false;
    for(unsigned j = 0; j < opi->getNumOperands(); j++)
      if(opi->getOperand(j) != ops[j])
        return false;
  }
  return true;
}

bool setMetadata(Instruction* inst, const std::string& attr) {
  if(inst->getMetadata(attr))
    return false;

  // We just need some metadata node because we are only concerned with the
  // presence of the attribute and not it's value. 1 is as good a value as any
  LLVMContext& context = inst->getContext();
  Type* i32 = Type::getInt32Ty(context);
  llvm::Metadata* one
      = ConstantAsMetadata::get(ConstantInt::get(i32, 1, false));
  inst->setMetadata(attr, MDNode::get(context, one));

  return true;
}

bool resetMetadata(Instruction* inst, const std::string& attr) {
  if(not inst->getMetadata(attr))
    return false;
  inst->setMetadata(attr, nullptr);
  return true;
}

Type* getAllocatedType(const Instruction* inst) {
  const Module& module = *getModule(inst);
  if(auto* alloca = dyn_cast<AllocaInst>(inst)) {
    if(moya::Metadata::hasAnalysisType(alloca))
      if(auto* pty = getAnalysisTypeAs<PointerType>(alloca))
        return getCanonicalType(pty->getElementType(), module);
    return getCanonicalType(alloca->getAllocatedType(), module);
  }
  return nullptr;
}

Location getLocation(const Instruction* inst) {
  return getLocation(inst->getDebugLoc());
}

} // namespace LLVMUtils

} // namespace moya
