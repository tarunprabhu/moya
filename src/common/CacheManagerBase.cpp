#include "CacheManagerBase.h"
#include "PathUtils.h"

#include <sstream>

namespace moya {

CacheManagerBase::CacheManagerBase(const std::string& cacheDir,
                                   const std::string& prefix,
                                   const std::string& sig,
                                   const std::string& time,
                                   const std::string& path)
    : cacheDir(cacheDir), prefix(prefix), sig(sig), time(time), path(path),
      enabled(cacheDir.length()) {
  ;
}

std::string CacheManagerBase::getFilename(const std::string& label,
                                          const std::string& ext) const {
  std::stringstream name;

  if(ext == "moya")
    name << prefix << "." << ext;
  else if(prefix.length())
    name << prefix << "_moya_" << label << "." << ext;
  else
    name << label << "." << ext;

  return moya::PathUtils::construct(cacheDir, name.str());
}

std::string CacheManagerBase::getFilename(const std::string& subPath,
                                          const std::string& filename,
                                          const std::string& ext) const {
  std::stringstream name;
  name << filename << "." << ext;

  return moya::PathUtils::join(cacheDir, subPath, name.str());
}

void CacheManagerBase::setPrefix(const std::string& prefix) {
  this->prefix = prefix;
}

void CacheManagerBase::setSignature(const std::string& sig) {
  this->sig = sig;
}

void CacheManagerBase::setTime(const std::string& time) {
  this->time = time;
}

void CacheManagerBase::setPath(const std::string& path) {
  this->path = path;
}

void CacheManagerBase::setCacheDir(const std::string& cacheDir) {
  this->cacheDir = cacheDir;
}

bool CacheManagerBase::isEnabled() const {
  return enabled;
}

const std::string& CacheManagerBase::getPrefix() const {
  return prefix;
}

const std::string& CacheManagerBase::getSignature() const {
  return sig;
}

const std::string& CacheManagerBase::getTime() const {
  return time;
}

const std::string& CacheManagerBase::getPath() const {
  return path;
}

const std::string& CacheManagerBase::getDirectory() const {
  return cacheDir;
}

} // namespace moya
