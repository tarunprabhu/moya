#ifndef MOYA_COMMON_DATA_LAYOUT_H
#define MOYA_COMMON_DATA_LAYOUT_H

#include <llvm/IR/DataLayout.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>

#include "Map.h"
#include "StructLayout.h"
#include "Vector.h"

namespace moya {

// This is a wrapper around a llvm::DataLayout object because we need some
// additional functionality that the llvm::DataLayout class does not provide
class DataLayout {
protected:
  llvm::DataLayout dl;
  moya::Map<llvm::StructType*, moya::StructLayout> structLayouts;

protected:
  void init(const llvm::Module&);
  void add(llvm::StructType*);

public:
  DataLayout();
  DataLayout(const llvm::Module&);

  void reset(const llvm::Module&);
  void reset(llvm::StructType*);
  const std::string& getStringRepresentation() const;

  unsigned getStackAlignment() const;
  unsigned getPointerSize() const;
  uint64_t getTypeSize(llvm::Type*) const;
  uint64_t getTypeStoreSize(llvm::Type*) const;
  uint64_t getTypeAllocSize(llvm::Type*) const;
  uint64_t getTypeAlignment(llvm::Type*) const;
  int64_t getOffsetInType(llvm::Type*,
                          const moya::Vector<const llvm::Value*>&) const;
  int64_t getOffsetInGEP(llvm::GetElementPtrInst*) const;
  const moya::StructLayout& getStructLayout(llvm::StructType*) const;
  const moya::StructLayout& getStructLayout(llvm::StructType*);
};

} // namespace moya

#endif // MOYA_COMMON_DATA_LAYOUT_H
