#ifndef MOYA_COMMON_REGION_H
#define MOYA_COMMON_REGION_H

#include "APITypes.h"
#include "Location.h"
#include "Serializer.h"
#include "SourceLang.h"

#include <string>

class PayloadBase;

namespace moya {

class Region {
protected:
  RegionID id;
  std::string name;
  Location loc;
  Location beginLoc;
  Location endLoc;
  SourceLang lang;

public:
  Region();
  Region(RegionID,
         const std::string&,
         const Location&,
         const Location&,
         const Location&,
         SourceLang);

  RegionID getID() const;
  const std::string& getName() const;
  const Location& getLocation() const;
  const Location& getBeginLoc() const;
  const Location& getEndLoc() const;
  SourceLang getSourceLang() const;

  std::string str() const;
  Serializer& serialize(Serializer&) const;
  void pickle(PayloadBase&) const;
  Region& unpickle(PayloadBase&);
};

} // namespace moya

#endif // MOYA_COMMON_REGION_H
