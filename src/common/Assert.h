#ifndef MOYA_COMMON_ASSERT_H
#define MOYA_COMMON_ASSERT_H

#include <cassert>

// Might be nice to print an error message or work around what to do if
// NDEBUG is defined
#define moya_assert(cond, msg) assert(cond)

#endif // MOYA_COMMON_ASSERT_H
