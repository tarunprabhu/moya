#ifndef MOYA_COMMON_JSON_UTILS_H
#define MOYA_COMMON_JSON_UTILS_H

#include "APITypes.h"

#include <string>

#include <llvm/IR/Function.h>
#include <llvm/IR/GlobalVariable.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Type.h>

namespace moya {

class Location;
class ContentAddress;
class Region;

namespace JSON {

std::string escape(const std::string&);

std::string pointer(llvm::Type*);
std::string pointer(const llvm::Argument*);
std::string pointer(const llvm::BasicBlock*);
std::string pointer(const llvm::Function*);
std::string pointer(const llvm::GlobalVariable*);
std::string pointer(const llvm::AllocaInst*);
std::string pointer(const llvm::Instruction*, const llvm::Function* = nullptr);
std::string pointer(const ContentAddress*);
std::string pointer(const Region*);

} // namespace JSON

} // namespace moya

#endif
