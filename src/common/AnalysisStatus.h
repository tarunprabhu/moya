#ifndef MOYA_COMMON_ANALYSIS_STATUS_H
#define MOYA_COMMON_ANALYSIS_STATUS_H

#include <llvm/IR/Function.h>
#include <llvm/IR/Instruction.h>

namespace moya {

enum AnalysisStatus {
  Unchanged = 0,
  Changed = 1,
  Top = ~0,
};

AnalysisStatus operator|=(AnalysisStatus&, bool);
AnalysisStatus operator|=(AnalysisStatus&, AnalysisStatus);

bool isChanged(AnalysisStatus);
bool isTop(AnalysisStatus);

AnalysisStatus setTop(const llvm::Instruction*);
AnalysisStatus setTop();
// AnalysisStatus setTop(const std::string&, const llvm::Function*);

} // namespace moya

#endif // MOYA_COMMON_ANALYSIS_STATUS_H
