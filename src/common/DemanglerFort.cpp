#include "DemanglerFort.h"
#include "Diagnostics.h"
#include "Metadata.h"

namespace moya {

DemanglerFort::DemanglerFort() : Demangler(SourceLang::Fort) {
  ;
}

std::string DemanglerFort::getModelName(const std::string& in) const {
  return in;
}

std::string DemanglerFort::getSourceName(const std::string& in) const {
  moya_error("UNIMPLEMENTED: DemanglerFort::getFunctionName()");

  return in;
}

std::string DemanglerFort::getQualifiedName(const std::string& in) const {
  moya_error("UNIMPLEMENTED: DemanglerFort::getQualifiedName()");

  return in;
}

std::string DemanglerFort::getFullName(const std::string& in) const {
  if(in.back() == '_')
    return in.substr(0, in.length() - 1);
  return in;
}

std::string DemanglerFort::demangle(const llvm::Function& f,
                                    Demangler::Action action) const {
  switch(action) {
  case Demangler::Action::Full:
  case Demangler::Action::Model:
  case Demangler::Action::Qualified:
    return getFullName(f.getName().str());
    break;
  case Demangler::Action::Source:
    if(moya::Metadata::hasSourceName(f))
      return moya::Metadata::getSourceName(f);
    return getSourceName(f.getName().str());
    break;
  default:
    moya_error("Unsupported demangler action: " << action);
  }

  return f.getName();
}

std::string DemanglerFort::demangle(const llvm::GlobalVariable& g,
                                    Demangler::Action action) const {
  switch(action) {
  case Demangler::Action::Full:
  case Demangler::Action::Qualified:
    return getFullName(g.getName().str());
    break;
  case Demangler::Action::Source:
    if(moya::Metadata::hasSourceName(g))
      return moya::Metadata::getSourceName(g);
    return getSourceName(g.getName());
    break;
  case Demangler::Action::Model:
    moya_error("Unsupported demangle action for global variable: " << action);
    break;
  }

  return g.getName();
}

} // namespace moya
