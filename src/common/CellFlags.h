#ifndef MOYA_COMMON_CELL_FLAGS_H
#define MOYA_COMMON_CELL_FLAGS_H

#include <initializer_list>
#include <stdint.h>
#include <string>

namespace moya {

class Serializer;

// The checks for the types are adapted from
// https://stackoverflow.com/a/20066694
template <bool... b>
struct all_of;

// Recurse if the first argument is true
template <bool... tail>
struct all_of<true, tail...> : all_of<tail...> {};

// Terminate if the first argument is false
template <bool... tail>
struct all_of<false, tail...> : std::false_type {};

// If we get to the point where all the arguments are processed, everything
// was the same
template <>
struct all_of<> : std::true_type {};

class CellFlags {
protected:
  uint64_t flags;

public:
  enum Flag {
    // The cell is part of a statically allocated object.
    // These are typically local variables in functions.
    // Global variables are not considered to be statically allocated.
    Stack = 0x1,

    // The cell is part of a dynamically allocated object.
    Heap = 0x2,

    // The cell represents a function, a function argument, a global variable,
    // a global constant or an exception object (we don't explicitly model
    // exceptions, but we want to do something sane. If we ever do start
    // modeling them, the cell will almsot certainly not be implicit).
    // This weakly represents  memory that has been reserved before the program
    // begins running and has a constant address at runtime.
    Implicit = 0x8,

    // This cell corresponds to a function or other form of executable code.
    // In this sense, the cell is "fake" because the store obviously does not
    // contain any code. But having a cell for a function lets us deal with
    // function pointers by allowing the address of a function to be taken
    Code = 0x10,

    // The cell is part of a "fake" stack
    // through which function arguments are passed
    Argument = 0x20,

    // The cell is part of a global variable
    Global = 0x40,

    // The cell is part of a global constant
    Constant = 0x80,

    // The cell is the first cell in an allocated object.
    Start = 0x100,

    // The vtable is a flagged separately because it is a special case of a
    // static array which is guaranteed to not be truncated
    Vtable = 0x200,

    // The cell was initialized with a default value. This is typically
    // done for stack allocated variables.
    Initialized = 0x400,

    // The cell should not "strictly" be here but is used as a workaround for
    // certain situations. Some of them are listed here, but there could be
    // other reasons. The point of having a catch-all flag like this is because
    // the cell is unlikely to ever be used for anything once the analysis
    // has finished.
    //
    //   - InsertValue instructions occasionally have a pattern:
    //
    //       %result = insertvalue UNDEF_TYPE undef, TYPE value, idx
    //
    //     This happens in Fortran in some cases for complex numbers and in C++
    //     when constructing exception objects. UNDEF_TYPE is usually a
    //     StructType in those cases and constructing a Content object for it
    //     gets messy. So we allocate some memory in the store and read the
    //     object from there because it is easier that way
    //
    //   - Empty have no fields, but we still need something in the store. So
    //     we create a cell of type i8*
    //
    Dummy = 0x800,

    // The cell is "implicitly" allocated through a call to a library function
    // that isn't necessarily intended to be a memory allocation function
    // For instance, calling fopen() will result in the allocation of a FILE
    // object and a pointer to the object will be returned. But fopen() is not
    // a memory allocation function.
    LibAlloc = 0x1000,

    // This is *NOT* a valid flag
    _Invalid
  };

protected:
  void init(std::initializer_list<Flag>&& flags);

public:
  CellFlags();

  template <typename... T>
  CellFlags(T&&... fs) : CellFlags() {
    static_assert(all_of<std::is_same<T, Flag>::value...>::value,
                  "Constructor to CellFlags can "
                  "only accept values with type "
                  "enum CellFlags::Flag");
    init({std::forward<Flag>(fs)...});
  }

  CellFlags& set(Flag);
  CellFlags& reset(Flag);
  bool test(Flag) const;

  CellFlags& operator|=(const CellFlags&);

  bool hasArgument() const;
  bool hasCode() const;
  bool hasConstant() const;
  bool hasDummy() const;
  bool hasGlobal() const;
  bool hasHeap() const;
  bool hasImplicit() const;
  bool hasInitialized() const;
  bool hasLibAlloc() const;
  bool hasStack() const;
  bool hasStart() const;
  bool hasVtable() const;

  std::string str() const;
  void serialize(Serializer&) const;
};

} // namespace moya

#endif // MOYA_COMMON_CELL_FLAGS_H
