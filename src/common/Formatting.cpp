#include "Formatting.h"

#include <sstream>

namespace moya {

// How deep of an indent to make
static int shift = 2;

unsigned indent1(unsigned depth) {
  return depth + shift;
}

unsigned indent2(unsigned depth) {
  return indent1(indent1(depth));
}

unsigned indent3(unsigned depth) {
  return indent1(indent2(depth));
}

unsigned indent4(unsigned depth) {
  return indent1(indent3(depth));
}

unsigned indent5(unsigned depth) {
  return indent1(indent4(depth));
}

unsigned unindent(unsigned depth) {
  return depth - shift;
}

// Generates an indent when printing types
std::string space(unsigned depth) {
  std::stringstream ss;
  for(unsigned i = 0; i < depth; i++)
    ss << " ";
  return ss.str();
}

std::string space1(unsigned indent) {
  return space(indent1(indent));
}

std::string space2(unsigned indent) {
  return space(indent2(indent));
}

std::string space3(unsigned indent) {
  return space(indent3(indent));
}

std::string space4(unsigned indent) {
  return space(indent4(indent));
}

} // namespace moya
