#ifndef MOYA_COMMON_PASS_MANAGER_H
#define MOYA_COMMON_PASS_MANAGER_H

#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Target/TargetMachine.h>

namespace moya {

class PassManager {
private:
  std::unique_ptr<llvm::legacy::PassManager> modulePasses;
  std::unique_ptr<llvm::legacy::FunctionPassManager> functionPasses;
  std::unique_ptr<llvm::TargetMachine> machine;

  void init(llvm::Module&, bool);

public:
  PassManager(llvm::Module&, bool = true);
  virtual ~PassManager() = default;

  llvm::legacy::PassManager& getModulePassManager();
  llvm::legacy::FunctionPassManager& getFunctionPassManager();
  void addEmitAssemblerFilePass(llvm::raw_pwrite_stream&);
  void addEmitMCFilePass(llvm::raw_pwrite_stream&);

  void addOptimizationPasses(bool, bool, unsigned = 3, unsigned = 1);
  void addConstPropPasses();

  void addPass(llvm::Pass*);
  void addModulePass(llvm::Pass*);
  void addFunctionPass(llvm::Pass*);
  void addRegionPass(llvm::Pass*);

  bool run(llvm::Module&);
  bool run(llvm::Function&);
};

} // namespace moya

#endif // MOYA_COMMON_PASS_MANAGER_H
