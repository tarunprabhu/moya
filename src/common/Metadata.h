#ifndef MOYA_COMMON_METADATA_H
#define MOYA_COMMON_METADATA_H

#include "APITypes.h"
#include "Location.h"
#include "Region.h"
#include "STDConfig.h"
#include "Set.h"
#include "SourceLang.h"
#include "Status.h"
#include "Vector.h"

#include <llvm/ADT/DenseMap.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Metadata.h>
#include <llvm/IR/Module.h>

#undef MOYA_METADATA_NODE
#undef MOYA_METADATA_NODE_VAL
#undef MOYA_METADATA_FN_ATTR_COMMON
#undef MOYA_METADATA_FN_ATTR_VAL_COMMON
#undef MOYA_METADATA_FN_ATTR
#undef MOYA_METADATA_FN_ATTR_VAL
#undef MOYA_METADATA_FN_ATTR_VALP
#undef MOYA_METADATA_ARG_ATTR_COMMON
#undef MOYA_METADATA_ARG_ATTR_VAL_COMMON
#undef MOYA_METADATA_ARG_ATTR
#undef MOYA_METADATA_ARG_ATTR_VAL
#undef MOYA_METADATA_ARG_ATTR_VALP
#undef MOYA_METADATA_INSTR_ATTR_COMMON
#undef MOYA_METADATA_INSTR_ATTR_VAL_COMMON
#undef MOYA_METADATA_INSTR_ATTR
#undef MOYA_METADATA_INSTR_ATTR_VAL
#undef MOYA_METADATA_INSTR_ATTR_VALP
#undef MOYA_METADATA_GLOBAL_ATTR_COMMON
#undef MOYA_METADATA_GLOBAL_ATTR_VAL_COMMON
#undef MOYA_METADATA_GLOBAL_ATTR
#undef MOYA_METADATA_GLOBAL_ATTR_VAL
#undef MOYA_METADATA_GLOBAL_ATTR_VALP
#undef MOYA_METADATA_TYPE_ATTR_COMMON
#undef MOYA_METADATA_TYPE_ATTR_VAL_COMMON
#undef MOYA_METADATA_TYPE_ATTR
#undef MOYA_METADATA_TYPE_ATTR_VAL
#undef MOYA_METADATA_TYPE_ATTR_VALP
#undef MOYA_METADATA_GLOBAL

//
// Metadata nodes that hold Moya-specific metadata that was added
//
#define MOYA_METADATA_NODE(BASE)                                    \
  static std::string get##BASE##NodeName();                         \
  static bool has##BASE##Node(const llvm::Module&);                 \
  static bool has##BASE##Node(const llvm::Module*);                 \
  static llvm::NamedMDNode* getOrInsert##BASE##Node(llvm::Module&); \
  static llvm::NamedMDNode* getOrInsert##BASE##Node(llvm::Module*); \
  static llvm::NamedMDNode* get##BASE##Node(const llvm::Module&);   \
  static llvm::NamedMDNode* get##BASE##Node(const llvm::Module*);

#define MOYA_METADATA_NODE_VAL(BASE, TYPE)                          \
  static std::string get##BASE##NodeName();                         \
  static bool has##BASE##Node(const llvm::Module&);                 \
  static bool has##BASE##Node(const llvm::Module*);                 \
  static llvm::NamedMDNode* getOrInsert##BASE##Node(llvm::Module&); \
  static llvm::NamedMDNode* getOrInsert##BASE##Node(llvm::Module*); \
  static llvm::NamedMDNode* get##BASE##Node(const llvm::Module&);   \
  static llvm::NamedMDNode* get##BASE##Node(const llvm::Module*);   \
  static bool add##BASE(llvm::Module&, const TYPE&);                \
  static bool add##BASE(llvm::Module*, const TYPE&);                \
  static moya::Vector<TYPE> get##BASE(const llvm::Module&);         \
  static moya::Vector<TYPE> get##BASE(const llvm::Module*);

//
// Function attributes
//
#define MOYA_METADATA_FN_ATTR_COMMON(BASE)           \
  static bool is##BASE##FnAttrName(llvm::StringRef); \
  static std::string get##BASE##FnAttrName();        \
  static bool has##BASE(const llvm::Function&);      \
  static bool has##BASE(const llvm::Function*);

#define MOYA_METADATA_FN_ATTR_VAL_COMMON(BASE, VAL_TYPE) \
  MOYA_METADATA_FN_ATTR_COMMON(BASE)                     \
  static VAL_TYPE get##BASE(const llvm::Function&);      \
  static VAL_TYPE get##BASE(const llvm::Function*);

#define MOYA_METADATA_FN_ATTR(BASE)         \
  MOYA_METADATA_FN_ATTR_COMMON(BASE)        \
  static bool set##BASE(llvm::Function&);   \
  static bool set##BASE(llvm::Function*);   \
  static bool reset##BASE(llvm::Function&); \
  static bool reset##BASE(llvm::Function*);

#define MOYA_METADATA_FN_ATTR_VAL(BASE, VAL_TYPE)                       \
  MOYA_METADATA_FN_ATTR_VAL_COMMON(BASE, VAL_TYPE)                      \
  static bool set##BASE(llvm::Function*, const VAL_TYPE&, bool = true); \
  static bool set##BASE(llvm::Function&, const VAL_TYPE&, bool = true);

#define MOYA_METADATA_FN_ATTR_VALP(BASE, VAL_TYPE)               \
  MOYA_METADATA_FN_ATTR_VAL_COMMON(BASE, VAL_TYPE)               \
  static bool set##BASE(llvm::Function*, VAL_TYPE, bool = true); \
  static bool set##BASE(llvm::Function&, VAL_TYPE, bool = true);

//
// Argument attributes
//
#define MOYA_METADATA_ARG_ATTR_COMMON(BASE)     \
  static std::string get##BASE##ArgAttrName();  \
  static bool has##BASE(const llvm::Argument&); \
  static bool has##BASE(const llvm::Argument*);

#define MOYA_METADATA_ARG_ATTR_VAL_COMMON(BASE, VAL_TYPE) \
  MOYA_METADATA_ARG_ATTR_COMMON(BASE)                     \
  static VAL_TYPE get##BASE(const llvm::Argument&);       \
  static VAL_TYPE get##BASE(const llvm::Argument*);

#define MOYA_METADATA_ARG_ATTR(BASE)      \
  MOYA_METADATA_ARG_ATTR_COMMON(BASE)     \
  static bool set##BASE(llvm::Argument&); \
  static bool set##BASE(llvm::Argument*);

#define MOYA_METADATA_ARG_ATTR_VAL(BASE, VAL_TYPE)                      \
  MOYA_METADATA_ARG_ATTR_VAL_COMMON(BASE, VAL_TYPE)                     \
  static bool set##BASE(llvm::Argument&, const VAL_TYPE&, bool = true); \
  static bool set##BASE(llvm::Argument*, const VAL_TYPE&, bool = true);

#define MOYA_METADATA_ARG_ATTR_VALP(BASE, VAL_TYPE)              \
  MOYA_METADATA_ARG_ATTR_VAL_COMMON(BASE, VAL_TYPE)              \
  static bool set##BASE(llvm::Argument&, VAL_TYPE, bool = true); \
  static bool set##BASE(llvm::Argument*, VAL_TYPE, bool = true);

//
// Instruction attributes
//
#define MOYA_METADATA_INSTR_ATTR_COMMON(BASE)      \
  static std::string get##BASE##InstrAttrName();   \
  static bool has##BASE(const llvm::Instruction&); \
  static bool has##BASE(const llvm::Instruction*); \
  static bool reset##BASE(llvm::Instruction&);     \
  static bool reset##BASE(llvm::Instruction*);

#define MOYA_METADATA_INSTR_ATTR_VAL_COMMON(BASE, VAL_TYPE)    \
  MOYA_METADATA_INSTR_ATTR_COMMON(BASE)                        \
  static llvm::MDNode* getRaw##BASE(const llvm::Instruction&); \
  static llvm::MDNode* getRaw##BASE(const llvm::Instruction*); \
  static VAL_TYPE get##BASE(const llvm::Instruction&);         \
  static VAL_TYPE get##BASE(const llvm::Instruction*);

#define MOYA_METADATA_INSTR_ATTR(BASE)       \
  MOYA_METADATA_INSTR_ATTR_COMMON(BASE)      \
  static bool set##BASE(llvm::Instruction&); \
  static bool set##BASE(llvm::Instruction*);

#define MOYA_METADATA_INSTR_ATTR_VAL(BASE, VAL_TYPE)                       \
  MOYA_METADATA_INSTR_ATTR_VAL_COMMON(BASE, VAL_TYPE)                      \
  static bool set##BASE(llvm::Instruction&, const VAL_TYPE&, bool = true); \
  static bool set##BASE(llvm::Instruction*, const VAL_TYPE&, bool = true);

#define MOYA_METADATA_INSTR_ATTR_VALP(BASE, VAL_TYPE)               \
  MOYA_METADATA_INSTR_ATTR_VAL_COMMON(BASE, VAL_TYPE)               \
  static bool set##BASE(llvm::Instruction&, VAL_TYPE, bool = true); \
  static bool set##BASE(llvm::Instruction*, VAL_TYPE, bool = true);

//
// Global attributes
//
#define MOYA_METADATA_GLOBAL_ATTR_COMMON(BASE)        \
  static std::string get##BASE##GlobalAttrName();     \
  static bool has##BASE(const llvm::GlobalVariable&); \
  static bool has##BASE(const llvm::GlobalVariable*);

#define MOYA_METADATA_GLOBAL_ATTR_VAL_COMMON(BASE, VAL_TYPE) \
  MOYA_METADATA_GLOBAL_ATTR_COMMON(BASE)                     \
  static VAL_TYPE get##BASE(const llvm::GlobalVariable&);    \
  static VAL_TYPE get##BASE(const llvm::GlobalVariable*);

#define MOYA_METADATA_GLOBAL_ATTR(BASE)         \
  MOYA_METADATA_GLOBAL_ATTR_COMMON(BASE)        \
  static bool set##BASE(llvm::GlobalVariable&); \
  static bool set##BASE(llvm::GlobalVariable*);

#define MOYA_METADATA_GLOBAL_ATTR_VAL(BASE, VAL_TYPE)                         \
  MOYA_METADATA_GLOBAL_ATTR_VAL_COMMON(BASE, VAL_TYPE)                        \
  static bool set##BASE(llvm::GlobalVariable&, const VAL_TYPE&, bool = true); \
  static bool set##BASE(llvm::GlobalVariable*, const VAL_TYPE&, bool = true);

#define MOYA_METADATA_GLOBAL_ATTR_VALP(BASE, VAL_TYPE)                 \
  MOYA_METADATA_GLOBAL_ATTR_VAL_COMMON(BASE, VAL_TYPE)                 \
  static bool set##BASE(llvm::GlobalVariable&, VAL_TYPE, bool = true); \
  static bool set##BASE(llvm::GlobalVariable*, VAL_TYPE, bool = true);

//
// Struct type attributes
//
#define MOYA_METADATA_TYPE_ATTR_COMMON(BASE)    \
  static std::string get##BASE##TypeAttrName(); \
  static bool has##BASE(llvm::Type*, const llvm::Module&);

#define MOYA_METADATA_TYPE_ATTR_VAL_COMMON(BASE, VAL_TYPE) \
  MOYA_METADATA_TYPE_ATTR_COMMON(BASE)                     \
  static VAL_TYPE get##BASE(llvm::Type*, const llvm::Module&);

#define MOYA_METADATA_TYPE_ATTR(BASE)  \
  MOYA_METADATA_TYPE_ATTR_COMMON(BASE) \
  static bool set##BASE(llvm::Type*, llvm::Module&);

#define MOYA_METADATA_TYPE_ATTR_VAL(BASE, VAL_TYPE)   \
  MOYA_METADATA_TYPE_ATTR_VAL_COMMON(BASE, VAL_TYPE); \
  static bool set##BASE(                              \
      llvm::Type*, const VAL_TYPE&, llvm::Module&, bool = true);

#define MOYA_METADATA_TYPE_ATTR_VALP(BASE, VAL_TYPE)  \
  MOYA_METADATA_TYPE_ATTR_VAL_COMMON(BASE, VAL_TYPE); \
  static bool set##BASE(llvm::Type*, VAL_TYPE, llvm::Module&, bool = true);

//
// Moya-specific global variables
//
#define MOYA_METADATA_GLOBAL(BASE)                                       \
  static std::string get##BASE##GlobalName();                            \
  static bool has##BASE##Global(const llvm::Module&);                    \
  static llvm::GlobalVariable* getOrInsert##BASE##Global(llvm::Module&); \
  static const llvm::GlobalVariable* get##BASE##Global(const llvm::Module&);

namespace moya {

class Metadata {
public:
  Metadata() = delete;

public:
  template <typename T>
  static T getAs(const llvm::Metadata*);

  static bool isMoyaAttributeName(const std::string&);

  static moya::Set<std::string> getAnalysisAttributeNames();
  static moya::Set<std::string> getOptimizationAttributeNames();

// Everything below this should be calls to the CONF_* macros to generate
// all the getters and setters that we need
#include "MoyaAttributes.inc"
};

} // namespace moya

#undef MOYA_METADATA_NODE
#undef MOYA_METADATA_NODE_VAL
#undef MOYA_METADATA_FN_ATTR_COMMON
#undef MOYA_METADATA_FN_ATTR_VAL_COMMON
#undef MOYA_METADATA_FN_ATTR
#undef MOYA_METADATA_FN_ATTR_VAL
#undef MOYA_METADATA_FN_ATTR_VALP
#undef MOYA_METADATA_ARG_ATTR_COMMON
#undef MOYA_METADATA_ARG_ATTR_VAL_COMMON
#undef MOYA_METADATA_ARG_ATTR
#undef MOYA_METADATA_ARG_ATTR_VAL
#undef MOYA_METADATA_ARG_ATTR_VALP
#undef MOYA_METADATA_INSTR_ATTR_COMMON
#undef MOYA_METADATA_INSTR_ATTR_VAL_COMMON
#undef MOYA_METADATA_INSTR_ATTR
#undef MOYA_METADATA_INSTR_ATTR_VAL
#undef MOYA_METADATA_INSTR_ATTR_VALP
#undef MOYA_METADATA_GLOBAL_ATTR_COMMON
#undef MOYA_METADATA_GLOBAL_ATTR_VAL_COMMON
#undef MOYA_METADATA_GLOBAL_ATTR
#undef MOYA_METADATA_GLOBAL_ATTR_VAL
#undef MOYA_METADATA_GLOBAL_ATTR_VALP
#undef MOYA_METADATA_TYPE_ATTR_COMMON
#undef MOYA_METADATA_TYPE_ATTR_VAL_COMMON
#undef MOYA_METADATA_TYPE_ATTR
#undef MOYA_METADATA_TYPE_ATTR_VAL
#undef MOYA_METADATA_TYPE_ATTR_VALP
#undef MOYA_METADATA_GLOBAL

#endif // MOYA_COMMON_METADATA_H
