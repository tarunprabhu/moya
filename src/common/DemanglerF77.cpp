#include "DemanglerF77.h"
#include "Diagnostics.h"

namespace moya {

DemanglerF77::DemanglerF77() : Demangler(SourceLang::F77) {
  ;
}

std::string DemanglerF77::stripTrailingUnderscore(const std::string& in) const {
  if(in.back() == '_')
    return in.substr(0, in.length() - 1);
  return in;
}

std::string DemanglerF77::demangle(const llvm::Function& f,
                                   Demangler::Action action) const {
  switch(action) {
  case Demangler::Action::Full:
  case Demangler::Action::Qualified:
  case Demangler::Action::Source:
  case Demangler::Action::Model:
    return stripTrailingUnderscore(f.getName().str());
    break;
  default:
    moya_error("Unsupported demangler action: " << action);
  }

  return stripTrailingUnderscore(f.getName());
}

std::string DemanglerF77::demangle(const llvm::GlobalVariable& g,
                                   Demangler::Action action) const {
  moya_error("Unimplemented: demangle() global variables for Fortran 77");

  return g.getName();
}

} // namespace moya
