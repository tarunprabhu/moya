#ifndef MOYA_COMMON_TRACE_H
#define MOYA_COMMON_TRACE_H

#include "Set.h"
#include "Vector.h"

#include <llvm/IR/Value.h>

namespace moya {

class Trace {
private:
  Vector<Vector<llvm::Value*>> traces;
  Set<llvm::Instruction*> todelete;

private:
  bool genTrace(llvm::Value*);
  bool genTrace(llvm::Value*, Vector<llvm::Value*>&);
  std::string str(const Vector<llvm::Value*>&, int = 0) const;

public:
  Trace(llvm::Value*);
  ~Trace();

  Vector<llvm::Value*> fronts() const;
  Vector<llvm::Value*> backs() const;
  Vector<llvm::Value*> rests() const;

  std::string str() const;
};

} // namespace moya

#endif // MOYA_COMMON_TRACE_H
