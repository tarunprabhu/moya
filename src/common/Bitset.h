#ifndef MOYA_COMMON_BITSET_H
#define MOYA_COMMON_BITSET_H

#include "Deserializer.h"
#include "Diagnostics.h"
#include "Serializer.h"
#include "Stringify.h"

#include <bitset>
#include <sstream>
#include <string>

namespace moya {

template <std::size_t N>
class Bitset {
protected:
  std::bitset<N> _impl;

public:
  using reference = typename std::bitset<N>::reference;

protected:
  std::bitset<N>& getImpl() {
    return _impl;
  }

  const std::bitset<N>& getImpl() const {
    return _impl;
  }

public:
  constexpr Bitset() {
    ;
  }

  constexpr Bitset(unsigned long long val) noexcept : _impl(val) {
    ;
  }

  template <class CharT, class Traits, class Alloc>
  explicit Bitset(
      const std::basic_string<CharT, Traits, Alloc>& str,
      typename std::basic_string<CharT, Traits, Alloc>::size_type pos = 0,
      typename std::basic_string<CharT, Traits, Alloc>::size_type n
      = std::basic_string<CharT, Traits, Alloc>::npos,
      CharT zero = CharT('0'),
      CharT one = CharT('1'))
      : _impl(str, pos, n, zero, one) {
    ;
  }

  template <class CharT>
  explicit Bitset(const CharT* str,
                  typename std::basic_string<CharT>::size_type n
                  = std::basic_string<CharT>::npos,
                  CharT zero = CharT('0'),
                  CharT one = CharT('1'))
      : _impl(str, n, zero, one) {
    ;
  }

  bool operator==(const Bitset<N>& rhs) const {
    return _impl == rhs.getImpl();
  }

  bool operator!=(const Bitset<N>& rhs) const {
    return _impl != rhs.getImpl();
  }

  constexpr bool operator[](std::size_t pos) const {
    return _impl[pos];
  }

  reference operator[](std::size_t pos) {
    return _impl[pos];
  }

  bool test(size_t pos) const {
    return _impl.test(pos);
  }

  bool all() const noexcept {
    return _impl.all();
  }

  bool any() const noexcept {
    return _impl.any();
  }

  bool none() const noexcept {
    return _impl.none();
  }

  std::size_t count() const noexcept {
    return _impl.count();
  }

  constexpr std::size_t size() const noexcept {
    return _impl.size();
  }

  Bitset<N>& operator&=(const Bitset<N>& other) noexcept {
    _impl &= other.getImpl();
    return *this;
  }

  Bitset<N>& operator|=(const Bitset<N>& other) noexcept {
    _impl |= other.getImpl();
    return *this;
  }

  Bitset<N>& operator^=(const Bitset<N>& other) noexcept {
    _impl ^= other.getImpl();
    return *this;
  }

  Bitset<N> operator~() const noexcept {
    Bitset<N> tmp;
    tmp._impl = ~_impl;
    return tmp;
  }

  Bitset<N> operator<<(std::size_t pos) const noexcept {
    Bitset<N> tmp;
    tmp._impl << pos;
    return tmp;
  }

  Bitset<N>& operator<<=(std::size_t pos) noexcept {
    _impl <<= pos;
    return *this;
  }

  Bitset<N> operator>>(std::size_t pos) const noexcept {
    Bitset<N> tmp;
    tmp._impl >> pos;
    return tmp;
  }

  Bitset<N>& operator>>=(std::size_t pos) noexcept {
    _impl >>= pos;
    return *this;
  }

  Bitset<N>& set() noexcept {
    _impl.set();
    return *this;
  }

  Bitset<N>& set(std::size_t pos, bool value = true) {
    _impl.set(pos, value);
    return *this;
  }

  Bitset<N>& reset() noexcept {
    _impl.reset();
    return *this;
  }

  Bitset<N>& reset(std::size_t pos) {
    _impl.reset(pos);
    return *this;
  }

  Bitset<N>& flip() noexcept {
    _impl.flip();
    return *this;
  }

  Bitset<N>& flip(std::size_t pos) {
    _impl.flip(pos);
    return *this;
  }

  template <class CharT = char,
            class Traits = std::char_traits<CharT>,
            class Allocator = std::allocator<CharT>>
  std::basic_string<CharT, Traits, Allocator>
  to_string(CharT zero = CharT('0'), CharT one = CharT('1')) const {
    return _impl.to_string(zero, one);
  }

  unsigned long to_ulong() const {
    return _impl.to_ulong();
  }

  unsigned long long to_ullong() const {
    return _impl.to_ullong();
  }

  void serialize(Serializer& s) const {
    s.serialize(_impl.to_string());
  }

  void deserialize(Deserializer& d) {
    std::string s;
    d.deserialize(s);
    _impl = std::bitset<N>(s);
  }
};

} // namespace moya

#endif // MOYA_COMMON_BITSET_H
