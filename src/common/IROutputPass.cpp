#include "Diagnostics.h"
#include "common/StringUtils.h"

#include <llvm/Bitcode/BitcodeWriter.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Support/raw_ostream.h>

#include <system_error>

using namespace llvm;

enum class OutputMode {
  BC,
  LL,
};

class IROutputPass : public ModulePass {
public:
  static char ID;

private:
  std::string fname;

public:
  IROutputPass();

  void setFilename(const std::string&);

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

IROutputPass::IROutputPass() : ModulePass(ID) {
  ;
}

StringRef IROutputPass::getPassName() const {
  return "Moya IR Output";
}

void IROutputPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesAll();
}

void IROutputPass::setFilename(const std::string& fname) {
  this->fname = fname;
}

bool IROutputPass::runOnModule(Module& module) {
  moya_message(getPassName());

  std::string ext = moya::StringUtils::lower(
      moya::StringUtils::rpartition(fname, ".").second);
  OutputMode mode = ext == "ll" ? OutputMode::LL : OutputMode::BC;

  std::error_code ec;
  raw_fd_ostream f(fname.c_str(), ec, sys::fs::F_None);
  if(ec.value()) {
    moya_warning("Could not write to " << fname << ": " << ec.message());
  } else {
    if(mode == OutputMode::LL) {
      f << module << "\n";
    } else {
      WriteBitcodeToFile(module, f);
      f.close();
    }
  }

  return false;
}

char IROutputPass::ID = 0;

static RegisterPass<IROutputPass>
    X("moya-write-ir", "Writes the module to file", true, true);

Pass* createIROutputPass(const std::string& fname) {
  auto* pass = new IROutputPass();
  pass->setFilename(fname);
  return pass;
}
