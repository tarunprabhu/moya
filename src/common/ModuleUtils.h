#ifndef MOYA_COMMON_MODULE_UTILS_H
#define MOYA_COMMON_MODULE_UTILS_H

#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>

// Utilities for LLVM modules

namespace moya {

namespace LLVMUtils {

llvm::Function* getMainFunction(llvm::Module*);
llvm::Function* getMainFunction(llvm::Module&);
const llvm::Function* getMainFunction(const llvm::Module*);
const llvm::Function* getMainFunction(const llvm::Module&);

const llvm::Function* getEnterRegionFunction(const llvm::Module&);
const llvm::Function* getExitRegionFunction(const llvm::Module&);

llvm::GlobalVariable* getPayloadRegionGlobal(llvm::Module&);
llvm::GlobalVariable* getPayloadFunctionGlobal(llvm::Module&);
llvm::Function* getEnterRegionFunction(llvm::Module&);
llvm::Function* getExitRegionFunction(llvm::Module&);
llvm::Function* getIsInRegionFunction(llvm::Module&);
llvm::Function* getCompileFunction(llvm::Module&);
llvm::Function* getBeginCallFunction(llvm::Module&);
llvm::Function* getRegisterArgFunction(llvm::Module&, llvm::Type*);
llvm::Function* getRegisterArgFunction(llvm::Module&, llvm::ArrayType*);
llvm::Function* getRegisterArgPtrFunction(llvm::Module&);
// llvm::Function* getStatsStartCallFunction(llvm::Module&);
// llvm::Function* getStatsStopCallFunction(llvm::Module&);
// llvm::Function* getStatsStartCompileFunction(llvm::Module&);
// llvm::Function* getStatsStopCompileFunction(llvm::Module&);
llvm::Function* getStatsStartExecuteFunction(llvm::Module&);
llvm::Function* getStatsStopExecuteFunction(llvm::Module&);
llvm::Function* getTopErrorFunction(llvm::Module&);

llvm::StructType* getMoyaStdStringType(llvm::Module&);
llvm::StructType* getMoyaStdStringType(const llvm::Module&);

moya::Vector<llvm::StructType*> getStructTypes(const llvm::Module&,
                                               bool = false);
moya::Vector<llvm::StructType*> getStructTypes(const llvm::Module*,
                                               bool = false);

} // namespace LLVMUtils

} // namespace moya

#endif // MOYA_COMMON_MODULE_UTILS_H
