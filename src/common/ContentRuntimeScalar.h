#ifndef MOYA_COMMON_CONTENT_RUNTIME_SCALAR_H
#define MOYA_COMMON_CONTENT_RUNTIME_SCALAR_H

#include "Content.h"

namespace moya {

// This class is used to represent a runtime scalar i.e. some value which is
// only known at runtime. I don't want to overload some constant and use it
// to represent runtime values in case it collides with a legitimate value
// during the analysis. There's only really one instance of this class that
// will ever exist.
class ContentRuntimeScalar : public Content {
public:
  ContentRuntimeScalar();
  virtual ~ContentRuntimeScalar() = default;

  virtual std::string str() const;
  virtual void serialize(Serializer&) const;

public:
  static bool classof(const Content*);
};

} // namespace moya

#endif // MOYA_COMMON_CONTENT_RUNTIME_SCALAR_H
