#include "Content.h"
#include "ContentAddress.h"
#include "ContentCode.h"
#include "ContentComposite.h"
#include "ContentNullptr.h"
#include "ContentRuntimePointer.h"
#include "ContentRuntimeScalar.h"
#include "ContentScalar.h"
#include "ContentUndef.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;

namespace moya {

Content::Content(Content::Kind kind) : kind(kind) {
  ;
}

Content::Kind Content::getKind() const {
  return kind;
}

bool Content::isAddress() const {
  return isa<ContentAddress>(this);
}

bool Content::isCode() const {
  return isa<ContentCode>(this);
}

bool Content::isComposite() const {
  return isa<ContentComposite>(this);
}

bool Content::isNullptr() const {
  return isa<ContentNullptr>(this);
}

bool Content::isRuntimePointer() const {
  return isa<ContentRuntimePointer>(this);
}

bool Content::isRuntimeScalar() const {
  return isa<ContentRuntimeScalar>(this);
}

bool Content::isScalar() const {
  return isa<ContentScalar>(this);
}

bool Content::isUndef() const {
  return isa<ContentUndef>(this);
}

} // namespace moya
