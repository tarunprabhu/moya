#ifndef MOYA_COMMON_DEQUE_H
#define MOYA_COMMON_DEQUE_H

#include "Deserializer.h"
#include "Diagnostics.h"
#include "Serializer.h"
#include "Stringify.h"

#include <deque>
#include <sstream>
#include <string>

namespace moya {

template <typename T>
class Deque {
protected:
  std::deque<T> _impl;

public:
  using value_type = typename std::deque<T>::value_type;
  using size_type = typename std::deque<T>::size_type;
  using reference = typename std::deque<T>::reference;
  using const_reference = typename std::deque<T>::const_reference;
  using pointer = typename std::deque<T>::pointer;
  using const_pointer = typename std::deque<T>::const_pointer;
  using iterator = typename std::deque<T>::iterator;
  using const_iterator = typename std::deque<T>::const_iterator;
  using reverse_iterator = typename std::deque<T>::reverse_iterator;
  using const_reverse_iterator = typename std::deque<T>::const_reverse_iterator;

public:
  std::deque<T>& getImpl() {
    return _impl;
  }

  const std::deque<T>& getImpl() const {
    return _impl;
  }

public:
  Deque() {
    ;
  }

  explicit Deque(size_type count) : _impl(count) {
    ;
  }

  explicit Deque(size_type count, const T& value) : _impl(count, value) {
    ;
  }

  template <typename Iterator>
  Deque(Iterator first, Iterator last) : _impl(first, last) {
    ;
  }

  Deque(const Deque& other) : _impl(other.geImpl()) {
    ;
  }

  Deque(Deque&& other) : _impl(other.getImpl()) {
    ;
  }

  Deque(std::initializer_list<T> init) : _impl(init) {
    ;
  }

  virtual ~Deque() = default;

  Deque<T>& operator=(const Deque<T>& other) {
    _impl = other.getImpl();
    return *this;
  }

  Deque<T>& operator=(Deque<T>&& other) {
    _impl = other.getImpl();
    return *this;
  }

  Deque<T>& operator=(std::initializer_list<T> init) {
    _impl = init;
    return *this;
  }

  reference at(size_type i) {
    return _impl.at(i);
  }

  const_reference at(size_type i) const {
    return _impl.at(i);
  }

  reference operator[](size_type i) {
    return _impl[i];
  }

  const_reference operator[](size_type i) const {
    return _impl[i];
  }

  reference front() {
    return _impl.front();
  }

  const_reference front() const {
    return _impl.front();
  }

  reference back() {
    return _impl.back();
  }

  const_reference back() const {
    return _impl.back();
  }

  iterator begin() {
    return _impl.begin();
  }

  const_iterator begin() const {
    return _impl.begin();
  }

  reverse_iterator rbegin() {
    return _impl.rbegin();
  }

  const_reverse_iterator rbegin() const {
    return _impl.rbegin();
  }

  iterator end() {
    return _impl.end();
  }

  const_iterator end() const {
    return _impl.end();
  }

  reverse_iterator rend() {
    return _impl.rend();
  }

  const_reverse_iterator rend() const {
    return _impl.rend();
  }

  bool empty() const {
    return _impl.empty();
  }

  size_type size() const {
    return _impl.size();
  }

  void clear() {
    return _impl.clear();
  }

  iterator erase(const_iterator pos) {
    return _impl.erase(pos);
  }

  iterator erase(const_iterator first, const_iterator last) {
    return _impl.erase(first, last);
  }

  template <typename... Args>
  reference emplace_back(Args&&... args) {
    _impl.emplace_back(args...);
    return _impl.back();
  }

  void push_back(const T& value) {
    _impl.push_back(value);
  }

  void push_back(const Deque<T>& other) {
    for(const T& e : other)
      push_back(e);
  }

  template <typename... Args>
  reference emplace_front(Args&&... args) {
    _impl.emplace_front(args...);
    return _impl.front();
  }

  void push_front(const T& value) {
    _impl.push_front(value);
  }

  void push_front(const Deque<T>& other) {
    for(const T& e : other)
      push_front(e);
  }

  value_type pop_front() {
    T ret = std::move(front());
    _impl.pop_front();
    return std::move(ret);
  }

  value_type pop_back() {
    T ret = std::move(back());
    _impl.pop_back();
    return std::move(ret);
  }

  bool contains(const T& e) const {
    for(auto i : *this)
      if(i == e)
        return true;
    return false;
  }

  bool operator==(const Deque<T>& c2) const {
    return _impl == c2._impl;
  }

  bool operator!=(const Deque<T>& c2) const {
    return _impl != c2._impl;
  }

  bool operator<(const Deque<T>& c2) const {
    return _impl < c2._impl;
  }

  bool operator<=(const Deque<T>& c2) const {
    return _impl <= c2._impl;
  }

  bool operator>(const Deque<T>& c2) const {
    return _impl > c2._impl;
  }

  bool operator>=(const Deque<T>& c2) const {
    return _impl >= c2._impl;
  }

  std::string str() const {
    bool comma = false;
    std::stringstream ss;

    ss << "[";
    for(const auto& e : _impl) {
      if(comma)
        ss << ", ";
      ss << moya::str(e);
      comma = true;
    }
    ss << "]";

    return ss.str();
  }

  void serialize(Serializer& s) const {
    s.arrStart();
    for(const auto& e : _impl)
      s.serialize(e);
    s.arrEnd();
  }

  template <typename U = T>
  typename std::enable_if_t<!std::is_pointer<U>::value>
  deserialize(Deserializer& d) {
    d.arrStart();
    while(not d.arrEmpty()) {
      _impl.emplace_back();
      d.deserialize(_impl.back());
    }
    d.arrEnd();
  }

  template <typename U = T>
  typename std::enable_if_t<std::is_pointer<U>::value
                            && std::is_class<std::remove_pointer<U>>::value>
  deserialize(Deserializer& d) {
    d.arrStart();
    while(not d.arrEmpty())
      _impl.push_back(std::remove_pointer<U>::type::deserializeFrom(d));
    d.arrEnd();
  }

  template <typename U = T>
  typename std::enable_if_t<std::is_pointer<U>::value
                            && !std::is_class<std::remove_pointer<U>>::value>
  deserialize(Deserializer& d) {
    moya_error("Cannot deserialize deque with pointers to non-class types");
  }
};

} // namespace moya

#endif // MOYA_COMMON_DEQUE_H
