#include "LLVMUtils.h"
#include "Metadata.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>

using namespace llvm;

namespace moya {

namespace LLVMUtils {

Function* getPersonalityFn(const Function& f) {
  if(not f.hasPersonalityFn())
    return nullptr;
  Constant* p = f.getPersonalityFn();
  if(ConstantExpr* cexpr = dyn_cast<ConstantExpr>(p))
    return cast<Function>(cexpr->getOperand(0));
  return cast<Function>(p);
}

Function* getPersonalityFn(const Function* f) {
  return getPersonalityFn(*f);
}

// Gets the function argument at the specified index
const Argument* getArgument(const Function* f, unsigned argno) {
  return getArgument(*f, argno);
}

const Argument* getArgument(const Function& f, unsigned argno) {
  for(const Argument& a : f.args())
    if(a.getArgNo() == argno)
      return &a;
  return nullptr;
}

Argument* getArgument(Function* f, unsigned argno) {
  return getArgument(*f, argno);
}

Argument* getArgument(Function& f, unsigned argno) {
  for(Argument& a : f.args())
    if(a.getArgNo() == argno)
      return &a;
  return nullptr;
}

Type* getArgumentType(const Function& f, unsigned argno) {
  if(argno < f.getFunctionType()->getNumParams())
    return f.getFunctionType()->getParamType(argno);
  return nullptr;
}

Type* getArgumentType(const Function* f, unsigned argno) {
  return getArgumentType(*f, argno);
}

unsigned getNumParams(const Function& f) {
  return f.getFunctionType()->getNumParams();
}

unsigned getNumParams(const Function* f) {
  return getNumParams(*f);
}

bool hasReturn(const Function& f) {
  return not f.getReturnType()->isVoidTy();
}

bool hasReturn(const Function* f) {
  return hasReturn(*f);
}

Module* getModule(Function& f) {
  return f.getParent();
}

Module* getModule(Function* f) {
  return getModule(*f);
}

const Module* getModule(const Function& f) {
  return f.getParent();
}

const Module* getModule(const Function* f) {
  return getModule(*f);
}

// A pure function is one that does not take any pointers as arguments or
// return any pointers. We assume here that the function does not modify any
// global variables that are visible to the containing program
bool isPureFunction(const Function& f) {
  FunctionType* fty = f.getFunctionType();
  if(fty->isVarArg())
    return false;
  Type* retTy = f.getFunctionType()->getReturnType();
  if(not(isScalarTy(retTy) or isScalarStructTy(retTy) or isVoidTy(retTy)))
    return false;
  for(const Argument& arg : f.args())
    if(not isScalarTy(getAnalysisType(arg)))
      return false;

  return true;
}

bool isPureFunction(const Function* f) {
  return isPureFunction(*f);
}

} // namespace LLVMUtils

} // namespace moya
