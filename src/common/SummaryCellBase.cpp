#include "SummaryCellBase.h"
#include "SummaryBase.h"

using namespace llvm;

namespace moya {

SummaryCellBase::SummaryCellBase() : addr(null), status(Status::getUnused()) {
  ;
}

SummaryCellBase::Address SummaryCellBase::getAddress() const {
  return addr;
}

const Status& SummaryCellBase::getStatus() const {
  return status;
}

const SummaryCellBase::Targets& SummaryCellBase::getTargets() const {
  return targets;
}

} // namespace moya
