#ifndef MOYA_COMMON_ENVIRONMENT_H
#define MOYA_COMMON_ENVIRONMENT_H

#include "APITypes.h"
#include "ClassHeirarchies.h"
#include "Contents.h"
#include "DataLayout.h"
#include "HashMap.h"
#include "HashMap2.h"
#include "HashSet.h"
#include "Map.h"
#include "Set.h"
#include "SourceLang.h"
#include "Stack.h"

#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Value.h>

namespace moya {

class Location;
class Serializer;

class Environment {
public:
  // Call sites can be both CallInst and InvokeInst in C++
  using Address = ContentAddress;
  using CallSites = HashSet<const llvm::Instruction*>;
  using Callees = HashSet<const llvm::Function*>;

private:
  const llvm::Module& module;

  DataLayout layout;

  HashMap<const llvm::Value*, Contents> env;
  HashMap<MoyaID, const llvm::Value*> ids;
  Stack<Contents> retStack;

  // This contains a map of all the call-sites for each defined function
  // This can handle the case where multiple functions can be called at any
  // one callsite if a function pointer is used
  HashMap<const llvm::Function*, CallSites> callSites;

  // Reverse of callSites
  HashMap<const llvm::Instruction*, Callees> callees;

  // We pretend as if each function has its own "private" stack, so anytime
  // the function is called, the arguments are "pushed" onto its private
  // stack. This just makes it easier to summarize the arguments because we
  // can just pass it the cells. Also, because they are their own cell, we can
  // fiddle with the status a little more without affecting other things. Since
  // we are specializing on the basis of arguments anyway, this is helpful.
  // The analysis itself will never examine these cells directly.
  HashMap2<const llvm::Argument*, const Address*> fargs;

  // Functions will have a unique address
  HashMap2<const llvm::Function*, const Address*> funcs;

  // This is just for quick lookups of global because they are "special" in
  // the sense that we need to look for them explicitly the way we look for
  // function arguments
  HashMap2<const llvm::GlobalVariable*, const Address*> globals;

  // Alloca's too will have a unique address because they are allocated in
  // each function.
  HashMap2<const llvm::AllocaInst*, const Address*> allocas;

  // InsertValueInst's have a unique address because they modify objects
  // "in memory" as opposed to through the store. To prevent this, we route
  // all the modifications through the store, but obviously don't want to
  // pollute it
  HashMap<const llvm::InsertValueInst*, const Address*> insvals;

  // Debug info for arguments and local variables
  Set<std::string> files;

  // Class heirarchies. This is a better place to have this than the store
  const ClassHeirarchies& heirarchies;

public:
  Environment(const llvm::Module&, const ClassHeirarchies&);
  virtual ~Environment() = default;

  bool contains(const llvm::Value*) const;
  bool contains(MoyaID) const;

  const Contents& lookup(const llvm::Value*) const;
  const Contents& lookup(MoyaID) const;
  const Address* lookupArg(const llvm::Argument*) const;
  const Address* lookupArg(MoyaID) const;
  const llvm::Argument* lookupArg(const Address*) const;
  const Address* lookupFunction(const llvm::Function*) const;
  const Address* lookupFunction(const llvm::Function&) const;
  const Address* lookupFunction(MoyaID) const;
  const llvm::Function* lookupFunction(const Address*) const;
  const Address* lookupGlobal(const llvm::GlobalVariable*) const;
  const Address* lookupGlobal(const llvm::GlobalVariable&) const;
  const Address* lookupGlobal(MoyaID) const;
  const llvm::GlobalVariable* lookupGlobal(const Address*) const;
  const Address* lookupAlloca(const llvm::AllocaInst*) const;
  const Address* lookupAlloca(MoyaID) const;
  const llvm::AllocaInst* lookupAlloca(const Address*) const;
  const Address* lookupInsertValue(const llvm::InsertValueInst*) const;

  bool add(const llvm::Value*);
  bool add(const llvm::Value*, const Contents&);
  bool add(const llvm::Value*, const Content*);
  bool addArg(const llvm::Argument*, const Address*);
  bool addFunction(const llvm::Function*, const Address*);
  bool addGlobal(const llvm::GlobalVariable*, const Address*);
  bool addAlloca(const llvm::AllocaInst*, const Address*);
  bool addInsertValue(const llvm::InsertValueInst*, const Address*);
  void addFile(const std::string&);

  bool isFunction(const Address*) const;
  bool isArg(const Address*) const;
  bool isGlobal(const Address*) const;
  bool isAlloca(const Address*) const;
  bool hasInsertValue(const llvm::InsertValueInst*) const;

  // Call-site specific functions
  bool hasCallSites(const llvm::Function*) const;
  const CallSites& getCallSites(const llvm::Function*) const;
  const Callees& getCallees(const llvm::Instruction*) const;
  bool addCallsite(const llvm::Function*, const llvm::Instruction*);

  // Return values from the stack
  bool push(const Addresses&);
  bool push(const Contents&);
  bool push(const Content*);
  Contents pop();
  const Contents& top();
  bool isEmpty() const;

  bool isDerivedFrom(llvm::Type*, llvm::Type*) const;
  bool isBaseOf(llvm::Type*, llvm::Type*) const;

  std::string str() const;
  void serializeFiles(Serializer&) const;
  void serializeFunctions(Serializer&) const;
  void serializeGlobals(Serializer&) const;
};

} // namespace moya

#endif // MOYA_COMMON_ENVIRONMENT_H
