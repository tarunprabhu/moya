#ifndef MOYA_COMMON_PATH_UTILS_H
#define MOYA_COMMON_PATH_UTILS_H

#include "common/Vector.h"

#include <string>

namespace moya {

// To work with file system paths. I can internally Boost or something, but
// I don't need much, so I'll just do things the hard way
// These currently only work for Linux and likely will never support anything
// else
//
namespace PathUtils {

// Constructs a path from the given entities. The first entity is the root
// The returned path will be an absolute path
std::string joinImpl(const Vector<std::string>&);

// Get the absolute path from the given path
std::string abspath(const std::string&);

// The name of the file in the given path
std::string basename(const std::string&);

// Path to the directory containing the file, or the directory itself
// if the path is to a directory
std::string dirname(const std::string&);

// Check if the path exists
bool exists(const std::string&);

// Check if the path is absolute
bool isAbsolute(const std::string&);

// Construct the path from the given components
template <typename... Args>
std::string construct(Args... args) {
  return abspath(joinImpl({args...}));
}

template <typename... Args>
std::string join(Args... args) {
  return joinImpl({args...});
}

} // namespace PathUtils

} // namespace moya

#endif // MOYA_COMMON_PATH_UTILS_H
