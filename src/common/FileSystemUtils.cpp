#include "FileSystemUtils.h"
#include "Diagnostics.h"
#include "PathUtils.h"

#include <cstdio>
#include <dirent.h>
#include <linux/limits.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

namespace moya {

namespace FileSystemUtils {

unsigned long getFileSize(const std::string& path) {
  std::ifstream in(path, std::ios::ate | std::ios::binary);
  return in.tellg();
}

moya::Vector<std::string> ls(const std::string& path) {
  moya::Vector<std::string> files;

  DIR* dir = opendir(path.c_str());
  while(dir) {
    errno = 0;
    struct dirent* file = readdir(dir);
    if(file) {
      files.push_back(file->d_name);
    } else if(errno == 0) {
      // If nothing was returned and errno == 0, then we have read all the
      // files in the directory, so we can just return
      closedir(dir);
      dir = nullptr;
    } else {
      // If file was null and errno was not 0, then there was a read error of
      // some sort
      moya_error("An error occurred while reading directory: " << path);
    }
  }

  return files;
}

std::string readlink(const std::string& path) {
  char result[PATH_MAX];
  ssize_t count = ::readlink(path.c_str(), result, PATH_MAX);
  if(count < 0)
    moya_error("Could not get current executable");

  return std::string(result, count);
}

std::string cwd() {
  char result[PATH_MAX];
  ::getcwd(result, PATH_MAX);
  return std::string(result);
}

void rm(const std::string& path) {
  ::remove(path.c_str());
}

void mkdir(const std::string& path) {
  if(DIR* dir = opendir(path.c_str())) {
    closedir(dir);
  } else if(errno == ENOENT) {
    ::mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  } else {
    moya_error("Error occurred while making directory: " << path);
  }
}

std::string getTmpDir() {
  const char* tmp = getenv("TMPDIR");
  if(tmp)
    return std::string(tmp);
  return "/tmp";
}

bool exists(const std::string& file) {
  struct stat stats;
  return stat(file.c_str(), &stats) == 0;
}

} // namespace FileSystemUtils

} // namespace moya
