#ifndef MOYA_COMMON_DEMANGLER_FORT_H
#define MOYA_COMMON_DEMANGLER_FORT_H

#include "Demangler.h"

namespace moya {

class DemanglerFort : public Demangler {
protected:
  std::string getFullName(const std::string&) const;
  std::string getSourceName(const std::string&) const;
  std::string getModelName(const std::string&) const;
  std::string getQualifiedName(const std::string&) const;

public:
  DemanglerFort();
  virtual ~DemanglerFort() = default;

  virtual std::string demangle(const llvm::Function&,
                               Demangler::Action) const override;
  virtual std::string demangle(const llvm::GlobalVariable&,
                               Demangler::Action) const override;
};

} // namespace moya

#endif // MOYA_COMMON_DEMANGLER_FORT_H
