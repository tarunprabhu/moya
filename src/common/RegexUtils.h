#ifndef MOYA_COMMON_REGEX_UTILS_H
#define MOYA_COMMON_REGEX_UTILS_H

#include <regex>

namespace moya {

namespace Regex {

bool matches(const std::string&, const std::regex&);
bool matches(const std::string&, const std::string&);
bool startswith(const std::string&, const std::regex&);
bool startswith(const std::string&, const std::string&);

} // namespace Regex

} // namespace moya

#endif // MOYA_COMMON_REGEX_UTILS_H
