#ifndef MOYA_COMMON_HEXIFY_H
#define MOYA_COMMON_HEXIFY_H

#include <string>

namespace moya {

std::string hex(uint64_t, bool = false);
std::string hex(uint32_t, bool = false);
std::string hex(uint16_t, bool = false);
std::string hex(uint8_t, bool = false);

} // namespace moya

#endif // MOYA_COMMON_HEXIFY_H
