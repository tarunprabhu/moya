#include "Location.h"
#include "Hexify.h"
#include "JSONUtils.h"
#include "PathUtils.h"
#include "PayloadBase.h"
#include "Stringify.h"

#include <sstream>

namespace moya {

Location::Location() : path(""), line(0), column(0), valid(false), hash(0) {
  ;
}

Location::Location(const std::string& file, unsigned line, unsigned column)
    : path(file), line(line), column(column), valid(line), hash(0) {
  if(not PathUtils::isAbsolute(path))
    moya_error("Need absolute file path. Got " << path << " " << line << " "
                                               << column);
  updateHash();
}

Location::Location(const std::string& dir,
                   const std::string& file,
                   unsigned line,
                   unsigned col)
    : path(""), line(line), column(col), valid(line), hash(0) {
  path = file;
  if(dir.size() and not moya::PathUtils::isAbsolute(path))
    path = moya::PathUtils::construct(dir, file);
  if(not PathUtils::isAbsolute(path))
    moya_error("Need absolute file path");
  updateHash();
}

void Location::updateHash() {
  hash = Hasher::get(path);
  hash = Hasher::update(line, hash);
  hash = Hasher::update(column, hash);
}

bool Location::getValid() const {
  return valid;
}

bool Location::isValid() const {
  return valid or (path != "" and line);
}

Hash Location::getHash() const {
  return hash;
}

const std::string& Location::getFile() const {
  return path;
}

unsigned Location::getLine() const {
  return line;
}

unsigned Location::getColumn() const {
  return column;
}

std::string Location::str() const {
  std::stringstream ss;
  if(not isValid())
    ss << "*:*:*";
  else
    ss << path << ":" << line << ":" << column;
  return ss.str();
}

void Location::pickle(PayloadBase& p) const {
  p.pickle(path);
  p.pickle(line);
  p.pickle(column);
  p.pickle(hash);
  p.pickle(getValid());
}

Location& Location::unpickle(PayloadBase& p) {
  path = p.unpickle<std::string>();
  line = p.unpickle<unsigned>();
  column = p.unpickle<unsigned>();
  hash = p.unpickle<Hash>();
  valid = p.unpickle<bool>();

  return *this;
}

Serializer& Location::serialize(Serializer& s, const llvm::Function* f) const {
  s.mapStart();
  s.add("file", path);
  s.add("line", line);
  s.add("column", column);
  if(f)
    s.add("function", JSON::pointer(f));
  s.mapEnd();

  return s;
}

Deserializer& Location::deserialize(Deserializer& d) {
  d.mapStart();
  d.deserialize("file", path);
  d.deserialize("line", line);
  d.deserialize("column", column);
  d.mapEnd();
  valid = line;
  updateHash();

  return d;
}

Location::Cmp Location::compare(const Location& locL, const Location& locR) {
  unsigned lineL = locL.getLine();
  unsigned lineR = locR.getLine();
  unsigned colL = locL.getColumn();
  unsigned colR = locR.getColumn();

  if(not locL.isValid() or not locR.isValid())
    return Location::Cmp::Incomparable;

  if(locL.getFile() != locR.getFile())
    return Location::Cmp::Incomparable;

  if(lineL > lineR)
    return Location::Cmp::After;
  else if(lineL < lineR)
    return Location::Cmp::Before;

  // The line numbers are the same
  if(colL > colR)
    return Location::Cmp::After;
  else if(colL < colR)
    return Location::Cmp::Before;

  return Location::Cmp::Identical;
}

bool Location::isBetween(const Location& loc,
                         const Location& locBefore,
                         const Location& locAfter) {
  return (Location::compare(loc, locBefore) == Location::Cmp::After)
         and (Location::compare(loc, locAfter) == Location::Cmp::Before);
}

bool Location::operator==(const Location& other) const {
  return Location::compare(*this, other) == Location::Cmp::Identical;
}

bool Location::operator!=(const Location& other) const {
  return Location::compare(*this, other) != Location::Cmp::Identical;
}

} // namespace moya
