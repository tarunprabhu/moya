#include "PassManager.h"
#include "common/Diagnostics.h"

#include <llvm/ADT/Triple.h>
#include <llvm/Analysis/TargetLibraryInfo.h>
#include <llvm/Analysis/TargetTransformInfo.h>
#include <llvm/IR/IRPrintingPasses.h>
#include <llvm/IR/Verifier.h>
#include <llvm/LinkAllPasses.h>
#include <llvm/MC/SubtargetFeature.h>
#include <llvm/Pass.h>
#include <llvm/Support/CodeGen.h>
#include <llvm/Support/PrettyStackTrace.h>
#include <llvm/Support/Signals.h>
#include <llvm/Support/TargetRegistry.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Target/TargetMachine.h>
#include <llvm/Target/TargetOptions.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

#ifdef ENABLED_POLLY
#include <polly/RegisterPasses.h>
#endif // ENABLED_POLLY

using namespace llvm;

namespace moya {

static TargetOptions getTargetOptions() {
  TargetOptions options;

  // This is blatantly ripped from llvm/CodeGen/CommandFlags.h
  // We basically juse set all the options to the default values of the
  // command line options. We don't just include that header here because
  // then we get stuck with all sorts of duplicate-definition errors
  // because the command line options there are all global variables.
  options.AllowFPOpFusion = FPOpFusion::Standard;
  options.UnsafeFPMath = false;
  options.NoInfsFPMath = false;
  options.NoNaNsFPMath = false;
  options.NoSignedZerosFPMath = false;
  options.NoTrappingFPMath = false;
  options.FPDenormalMode = FPDenormal::IEEE;
  options.HonorSignDependentRoundingFPMathOption = false;
  options.FloatABIType = FloatABI::Default;
  options.NoZerosInBSS = false;
  options.GuaranteedTailCallOpt = false;
  options.StackAlignmentOverride = 0;
  options.StackSymbolOrdering = true;
  options.UseInitArray = !false;
  options.RelaxELFRelocations = false;
  options.DataSections = false;
  options.FunctionSections = false;
  options.UniqueSectionNames = true;
  options.EmulatedTLS = false;
  options.ExceptionModel = ExceptionHandling::None;
  // FIXME: Might need to change this depending on the program being compiled
  // especially if it is OpenMP
  options.ThreadModel = ThreadModel::POSIX;
  options.EABIVersion = EABI::Default;
  options.DebuggerTuning = DebuggerKind::Default;

  // This is blatantly ripped from llvm/MC/MCTargetOptionsCommandFlags.inc
  options.MCOptions.SanitizeAddress = false;
  options.MCOptions.MCRelaxAll = false;
  options.MCOptions.MCIncrementalLinkerCompatible = false;
  options.MCOptions.MCPIECopyRelocations = false;
  options.MCOptions.DwarfVersion = 0;
  options.MCOptions.ShowMCInst = false;
  options.MCOptions.ABIName = "";
  options.MCOptions.MCFatalWarnings = false;
  options.MCOptions.MCNoWarn = false;
  options.MCOptions.MCNoDeprecatedWarn = false;

  return options;
}

// Returns the TargetMachine instance or zero if no triple is provided.
// This is a combination of code copied from llvm/tools/opt.cpp and other
// header files with some parameters set to be deliberately aggressive
static TargetMachine* getTargetMachine(Triple triple) {
  std::string err;
  const Target* target = TargetRegistry::lookupTarget(triple.normalize(), err);

  // This is always going to be the host, so we leave it unset
  std::string mcpu = "";

  // This will always be the features available on the host
  // This is copied from llvm/CodeGen/CommandFlags.h
  SubtargetFeatures features;
  StringMap<bool> hostFeatures;
  if(sys::getHostCPUFeatures(hostFeatures))
    for(auto& it : hostFeatures)
      features.AddFeature(it.first(), it.second);
  std::string mattrs = features.getString();

  // This has to be PIC_ because it must be linked at runtime (duh!)
  Reloc::Model modelReloc = Reloc::PIC_;

  // The code model is from the SystemV ABI. Explanations are here:
  // https://software.intel.com/sites/default/files/article/402129/mpx-linux64-abi.pdf
  CodeModel::Model modelCode = CodeModel::Small;

  CodeGenOpt::Level levelOpt = CodeGenOpt::Aggressive;

  return target->createTargetMachine(triple.getTriple(),
                                     mcpu,
                                     mattrs,
                                     getTargetOptions(),
                                     modelReloc,
                                     modelCode,
                                     levelOpt);
}

void PassManager::init(Module& module, bool initializeRegistry) {
  InitializeAllTargets();

  // If we are using this in clang to bypass the normal codegen mechanisms,
  // we don't need to initialize the registry because clang will have done
  // that for us. This only needs to be done explicitly at JIT time

  if(initializeRegistry) {
    // Initialize passes (copied from llvm/tools/opt.cpp)
    PassRegistry& registry = *PassRegistry::getPassRegistry();
    initializeCore(registry);
    initializeCoroutines(registry);
    initializeScalarOpts(registry);
    initializeObjCARCOpts(registry);
    initializeVectorization(registry);
    initializeIPO(registry);
    initializeAnalysis(registry);
    initializeTransformUtils(registry);
    initializeInstCombine(registry);
    initializeInstrumentation(registry);
    initializeTarget(registry);

    initializeScalarizeMaskedMemIntrinPass(registry);
    initializeCodeGenPreparePass(registry);
    initializeAtomicExpandPass(registry);
    initializeRewriteSymbolsLegacyPassPass(registry);
    initializeWinEHPreparePass(registry);
    initializeDwarfEHPreparePass(registry);
    initializeSafeStackLegacyPassPass(registry);
    initializeSjLjEHPreparePass(registry);
    initializePreISelIntrinsicLoweringLegacyPassPass(registry);
    initializeGlobalMergePass(registry);
    initializeInterleavedAccessPass(registry);
    initializeUnreachableBlockElimLegacyPassPass(registry);
    initializeExpandReductionsPass(registry);

#ifdef ENABLED_POLLY
    polly::initializePollyPasses(registry);
#endif // ENABLED_POLLY
  }

  Triple triple(module.getTargetTriple());
  if(triple.getArch())
    machine.reset(getTargetMachine(Triple(triple)));
}

PassManager::PassManager(Module& module, bool initializeRegistry)
    : modulePasses(new legacy::PassManager()),
      functionPasses(new legacy::FunctionPassManager(&module)),
      machine(nullptr) {

  init(module, initializeRegistry);
  modulePasses->add(
      new TargetLibraryInfoWrapperPass(Triple(module.getTargetTriple())));
  if(machine) {
    modulePasses->add(
        createTargetTransformInfoWrapperPass(machine->getTargetIRAnalysis()));
    functionPasses->add(
        createTargetTransformInfoWrapperPass(machine->getTargetIRAnalysis()));
  }
}

void PassManager::addPass(Pass* p) {
  if(p->getPassKind() == PassKind::PT_Module)
    addModulePass(p);
  else if(p->getPassKind() == PassKind::PT_Function)
    addFunctionPass(p);
  else
    moya_error("Unsupported pass kind");
}

void PassManager::addModulePass(Pass* p) {
  modulePasses->add(p);
}

void PassManager::addFunctionPass(Pass* p) {
  functionPasses->add(p);
}

void PassManager::addRegionPass(Pass* p) {
  functionPasses->add(p);
}

void PassManager::addOptimizationPasses(bool unroll,
                                        bool vectorize,
                                        unsigned levelOpt,
                                        unsigned levelSize) {
  functionPasses->add(createVerifierPass());

  PassManagerBuilder builder;
  builder.OptLevel = levelOpt;
  builder.SizeLevel = levelSize;

  if(levelOpt > 1) {
    unsigned threshold = 225;
    if(levelSize == 1) // -Os
      threshold = 75;
    else if(levelSize == 2) // -Oz
      threshold = 25;
    if(levelOpt > 2)
      threshold = 275;
    builder.Inliner = createFunctionInliningPass(threshold);
  } else {
    builder.Inliner = createAlwaysInlinerLegacyPass();
  }

  builder.DisableUnrollLoops = not unroll;
  builder.LoopVectorize = vectorize;
  builder.SLPVectorize = vectorize;

  builder.populateFunctionPassManager(*functionPasses);
  builder.populateModulePassManager(*modulePasses);
}

void PassManager::addConstPropPasses() {
  // -simplifycfg -instcombine -inline -globaldce -instcombine -simplifycfg
  // -adce -scalarrepl -mem2reg -adce -verify -sccp -adce -instcombine -dce
  // -simplifycfg -deadargelim -globaldce
  addModulePass(createCFGSimplificationPass());
  addModulePass(createInstructionCombiningPass());
  addModulePass(createGlobalDCEPass());
  addModulePass(createInstructionCombiningPass());
  addModulePass(createCFGSimplificationPass());
  addModulePass(createAggressiveDCEPass());
  addModulePass(createSROAPass());
  addModulePass(createPromoteMemoryToRegisterPass());
  addModulePass(createAggressiveDCEPass());
  addModulePass(createVerifierPass());
  addModulePass(createSCCPPass());
  addModulePass(createAggressiveDCEPass());
  addModulePass(createInstructionCombiningPass());
  addModulePass(createDeadCodeEliminationPass());
  addModulePass(createCFGSimplificationPass());
  addModulePass(createDeadArgEliminationPass());
  addModulePass(createGlobalDCEPass());
}

legacy::PassManager& PassManager::getModulePassManager() {
  return *modulePasses.get();
}

legacy::FunctionPassManager& PassManager::getFunctionPassManager() {
  return *functionPasses.get();
}

void PassManager::addEmitAssemblerFilePass(raw_pwrite_stream& out) {
  bool error = machine->addPassesToEmitFile(
      getModulePassManager(),
      out,
      nullptr,
      TargetMachine::CodeGenFileType::CGFT_AssemblyFile);
  if(error)
    moya_error("Could not add passes to emit assembly file");
}

void PassManager::addEmitMCFilePass(raw_pwrite_stream& out) {
  bool error = machine->addPassesToEmitFile(
      getModulePassManager(),
      out,
      nullptr,
      TargetMachine::CodeGenFileType::CGFT_ObjectFile);
  if(error)
    moya_error("Could not add passes to emit object file");
}

bool PassManager::run(Function& F) {
  functionPasses->doInitialization();
  bool rv = functionPasses->run(F);
  functionPasses->doFinalization();

  return rv;
}

bool PassManager::run(Module& M) {
  return modulePasses->run(M);
}

} // namespace moya
