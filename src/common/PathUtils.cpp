#include "PathUtils.h"
#include "Diagnostics.h"
#include "StringUtils.h"

#include <sstream>

#include <sys/stat.h>

namespace moya {

namespace PathUtils {

std::string abspath(const std::string& p) {
  char abspath[PATH_MAX];
  realpath(p.c_str(), abspath);
  return abspath;
}

std::string dirname(const std::string& p) {
  Vector<std::string> dirs = StringUtils::split(p, '/');
  if(dirs.size() == 1)
    return ".";
  std::stringstream ss;
  ss << dirs[0];
  for(size_t i = 1; i < dirs.size() - 1; i++)
    ss << "/" << dirs[i];
  return ss.str();
}

std::string basename(const std::string& p) {
  return StringUtils::split(p, '/').back();
}

bool exists(const std::string& p) {
  struct stat info;
  if(stat(p.c_str(), &info) != 0)
    return false;
  return true;
}

// Return a copy of the string with the trailing slashes removed
static std::string stripTrailingSlashes(const std::string& s) {
  std::string t = s;

  // Find the index of the last character of the string that is not a '/'
  size_t pos = t.size() - 1;
  while(t[pos] == '/')
    pos--;
  // pos now points to the first character that *IS NOT* a '/'.
  // Increment it so that it points to the first slash.
  pos++;

  // This will erase everything from pos to the end of the string
  t.erase(pos);

  return t;
}

std::string joinImpl(const Vector<std::string>& elems) {
  if(elems.size() == 0)
    return "";

  std::stringstream ss;
  ss << stripTrailingSlashes(elems[0]);
  for(size_t i = 1; i < elems.size(); i++) {
    if(elems[i][0] == '/')
      moya_error("Cannot construct path from: " << str(elems));
    ss << "/" << stripTrailingSlashes(elems[i]);
  }

  return ss.str();
}

bool isAbsolute(const std::string& path) {
  if(path.size())
    return path[0] == '/';
  return false;
}

} // namespace PathUtils

} // namespace moya
