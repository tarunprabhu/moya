#ifndef MOYA_COMMON_STRINGIFY_H
#define MOYA_COMMON_STRINGIFY_H

#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Type.h>

#include <sstream>
#include <string>

// The str() functions behave like Python's str() and always return a string

namespace moya {

std::string str(const std::string&);
std::string str(const llvm::Argument&);
std::string str(const llvm::Function&);
std::string str(const llvm::GlobalVariable&);
std::string str(const llvm::Instruction&);
std::string str(const llvm::Type&);
std::string str(const llvm::FunctionType&);
std::string str(const llvm::StructType&);

std::string str(llvm::Type*);
std::string str(const llvm::Argument*);
std::string str(const llvm::BasicBlock*);
std::string str(const llvm::Function*);
std::string str(const llvm::GlobalVariable*);
std::string str(const llvm::Instruction*);

template <typename T1, typename T2>
inline std::string str(const std::pair<T1, T2>& p) {
  std::stringstream ss;
  ss << "{" << str(p.first) << ", " << str(p.second) << "}";
  return ss.str();
}

// For primitive types (integers, floats, etc.)
template <typename T,
          typename std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
inline std::string str(const T& e) {
  std::stringstream ss;
  ss << e;
  return ss.str();
}

// For enums. We expect that a separate str() function is defined to deal
// with the enum
template <typename T,
          typename std::enable_if_t<std::is_enum<T>::value, int> = 0>
std::string str(const T& e);

// For class types. We assume that the class contains a str() member function
template <typename T,
          typename std::enable_if_t<std::is_class<T>::value, int> = 0>
std::string str(const T& e) {
  return e.str();
}

// For pointers to classes
template <typename T,
          typename std::enable_if_t<
              std::is_pointer<T>::value
                  && std::is_class<std::remove_pointer<T>>::value,
              int> = 0>
std::string str(const T* e) {
  return e->str();
}

// For all other pointers
template <typename T,
          typename std::enable_if_t<
              std::is_pointer<T>::value
                  && !std::is_class<std::remove_pointer<T>>::value,
              int> = 0>
std::string str(const T& e) {
  std::stringstream ss;
  ss << reinterpret_cast<const void*>(e);
  return ss.str();
}

} // namespace moya

#endif // MOYA_COMMON_STRINGIFY_H
