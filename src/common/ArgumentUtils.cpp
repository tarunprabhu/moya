#include "LLVMUtils.h"
#include "Metadata.h"

using namespace llvm;

namespace moya {

namespace LLVMUtils {

const Function* getFunction(const Argument& arg) {
  return arg.getParent();
}

const Function* getFunction(const Argument* arg) {
  return getFunction(*arg);
}

Function* getFunction(Argument& arg) {
  return arg.getParent();
}

Function* getFunction(Argument* arg) {
  return getFunction(*arg);
}

StringRef getFunctionName(const Argument& arg) {
  return getFunction(arg)->getName();
}

StringRef getFunctionName(const Argument* arg) {
  return getFunctionName(*arg);
}

const Module* getModule(const Argument& arg) {
  return arg.getParent()->getParent();
}

const Module* getModule(const Argument* arg) {
  return getModule(*arg);
}

Module* getModule(Argument& arg) {
  return arg.getParent()->getParent();
}

Module* getModule(Argument* arg) {
  return getModule(*arg);
}

Type* getAnalysisType(const Argument* arg) {
  return getAnalysisType(*arg);
}

Type* getAnalysisType(const Argument& arg) {
  Type* ret = arg.getType();
  if(moya::Metadata::hasAnalysisType(arg))
    ret = moya::Metadata::getAnalysisType(arg);

  if(const Module* module = getModule(arg))
    return getCanonicalType(ret, *module);
  return ret;
}

} // namespace LLVMUtils

} // namespace moya
