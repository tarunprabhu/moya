#ifndef MOYA_COMMON_PAIR_H
#define MOYA_COMMON_PAIR_H

#include "Deserializer.h"
#include "Diagnostics.h"
#include "Serializer.h"
#include "Stringify.h"

#include <utility>

namespace moya {

template <typename T1, typename T2>
class Pair {
private:
  std::pair<T1, T2> _impl;

public:
  constexpr Pair() {
    ;
  }

  constexpr Pair(const T1& x, const T2& y) : _impl(x, y) {
    ;
  }

  template <class U1, class U2>
  constexpr Pair(U1&& x, U2&& y) : _impl(x, y) {
    ;
  }

  template <class U1, class U2>
  constexpr Pair(const Pair<U1, U2>& p) : _impl(p._impl) {
    ;
  }

  template <class U1, class U2>
  constexpr Pair(const std::pair<U1, U2>& p) : _impl(p) {
    ;
  }

  template <class U1, class U2>
  constexpr Pair(Pair<U1, U2>&& p) : _impl(p._impl) {
    ;
  }

  template <class U1, class U2>
  constexpr Pair(std::pair<U1, U2>&& p) : _impl(p) {
    ;
  }

  Pair(const Pair&) = default;
  Pair(Pair&&) = default;

  constexpr Pair& operator=(const Pair& other) {
    this->_impl = other._impl;
    return *this;
  }

  template <class U1, class U2>
  constexpr Pair& operator=(const Pair<U1, U2>& other) {
    this->_impl = other._impl;
    return *this;
  }

  template <class U1, class U2>
  constexpr Pair& operator=(const std::pair<U1, U2>& other) {
    this->_impl = other;
    return *this;
  }

  constexpr void swap(Pair& other) {
    this->_impl.swap(other._impl);
  }

  T1& car() {
    return this->_impl.first;
  }

  const T1& car() const {
    return this->_impl.first;
  }

  T2& cdr() {
    return this->_impl.second;
  }

  const T2& cdr() const {
    return this->_impl.second;
  }

  std::string str() const {
    std::stringstream ss;

    ss << "<" << moya::str(this->_impl.first) << ", "
       << moya::str(this->_impl.second) << ">";

    return ss.str();
  }

  template <typename... ArgsT>
  void serialize(Serializer& s, ArgsT&&...) const {
    s.pairStart();
    s.serialize(this->_impl.first);
    s.serialize(this->_impl.second);
    s.pairEnd();
  }

  void deserialize(Deserializer& d) {
    d.pairStart();
    d.deserialize(this->_impl.first);
    d.deserialize(this->_impl.second);
    d.pairEnd();
  }
};

} // namespace moya

#endif // MOYA_COMMON_PAIR_H
