#ifndef MOYA_COMMON_HASHER_H
#define MOYA_COMMON_HASHER_H

#include <stdint.h>
#include <string>

namespace moya {

using Hash = uint64_t;

namespace Hasher {

Hash get(uint8_t);
Hash get(uint16_t);
Hash get(uint32_t);
Hash get(uint64_t);
Hash get(float);
Hash get(double);
Hash get(long double);
Hash get(const std::string&);

Hash& update(uint8_t, Hash&);
Hash& update(uint16_t, Hash&);
Hash& update(uint32_t, Hash&);
Hash& update(uint64_t, Hash&);
Hash& update(float, Hash&);
Hash& update(double, Hash&);
Hash& update(long double, Hash&);
Hash& update(const std::string&, Hash&);

} // namespace Hasher

} // namespace moya

#endif // MOYA_COMMON_HASHER_H
