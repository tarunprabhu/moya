#ifndef MOYA_AERYN_AERYN_CONFIG_H
#define MOYA_AERYN_AERYN_CONFIG_H

// This class keeps track of parameters which affect Moya's static analyses
// We separate this from MoyaConfig because this is limited only to the static
// analysis

#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/GlobalVariable.h>
#include <llvm/IR/Type.h>

namespace moya {

class AerynConfig {
private:
  // FIXME: Support multiple regions. Right now, for simplicity, this is
  // restricted to 1. But most of those bugs have probably gone away so it
  // might be safe to remove this limitation
  static const int32_t max_regions = 1;

  // The maximum number of iterations that the mutability analysis should
  // run for.
  static const uint32_t max_iterations = 50;

  // In some cases, it might be helpful to track an entire array (for instance,
  // C++ vtables), but in most cases it doesn't produce anything useful and is
  // very expensive. This restricts the maximum size of an array that we track.
  // For dynamically allocated arrays, we will assume that it is of length
  // 1 just to keep things simple. This handles the annoying case of
  // 0-length arrays too
  static const uint64_t analysis_array_length = 1;

  // For consistency, we still need a model for pure functions.
  static const std::string pureFunctionModel;

public:
  static uint32_t getMaxJITRegions();
  static uint32_t getMaxIterations();

  // The default array length to assume when we don't know the length of the
  // array (almost the only use for this is for dynamically allocated data)
  static uint64_t getArrayLength();

  static uint64_t getArrayLength(llvm::ArrayType*);

  static bool shouldTruncate(llvm::Type*);
  static bool shouldTruncate(llvm::GlobalVariable&);

  static const std::string& getPureFunctionModel();
};

} // namespace moya

#endif // MOYA_AERYN_AERYN_CONFIG_H
