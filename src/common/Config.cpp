#include "Config.h"
#include "APIDecls.h"
#include "Diagnostics.h"
#include "Hasher.h"
#include "Metadata.h"
#include "PathUtils.h"
#include "StringUtils.h"
// This is a generated file, so it will be in the install directory. If we
// don't put the common/ prefix, the compiler will complain that it cannot
// find the file
#include "common/MoyaVersion.h"

#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Support/raw_ostream.h>

#include <sstream>

namespace moya {

namespace Config {

const char* getMoyaAPIPrefix() {
  return MOYA_QUOTE(MOYA_PREFIX);
}

bool isMoyaFunction(const llvm::Function& f) {
  return f.getName().find(getMoyaAPIPrefix()) == 0;
}

const char* getVersionString() {
  return MOYA_VERSION_STR;
}

unsigned getVersionMajor() {
  return MOYA_VERSION_MAJOR;
}

unsigned getVersionMinor() {
  return MOYA_VERSION_MINOR;
}

unsigned getVersionPatch() {
  return MOYA_VERSION_PATCH;
}

const char* getEnterRegionFunctionName() {
  return MOYA_QUOTE(MOYA_FN_ENTER_REGION);
}

const char* getExitRegionFunctionName() {
  return MOYA_QUOTE(MOYA_FN_EXIT_REGION);
}

const char* getIsInRegionFunctionName() {
  return MOYA_QUOTE(MOYA_FN_IS_IN_REGION);
}

const char* getCompileFunctionName() {
  return MOYA_QUOTE(MOYA_FN_COMPILE);
}

const char* getBeginCallFunctionName() {
  return MOYA_QUOTE(MOYA_FN_BEGIN_CALL);
}

const char* getRegisterArgPtrFunctionName() {
  return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(ptr));
}

const char* getRegisterArgFunctionName(llvm::Type* ty) {
  if(auto* pty = llvm::dyn_cast<llvm::PointerType>(ty)) {
    llvm::Type* ety = pty->getElementType();
    if(ety->isIntegerTy(1))
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(pi1));
    else if(ety->isIntegerTy(8))
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(pi8));
    else if(ety->isIntegerTy(16))
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(pi16));
    else if(ety->isIntegerTy(32))
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(pi32));
    else if(ety->isIntegerTy(64))
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(pi64));
    else if(ety->isFloatTy())
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(pf32));
    else if(ety->isDoubleTy())
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(pf64));
    else if(ety->isX86_FP80Ty())
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(pf80));
    else if(ety->isPointerTy())
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(pptr));
  } else if(auto* aty = llvm::dyn_cast<llvm::ArrayType>(ty)) {
    llvm::Type* ety = aty->getElementType();
    if(ety->isIntegerTy(1))
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(ai1));
    else if(ety->isIntegerTy(8))
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(ai8));
    else if(ety->isIntegerTy(16))
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(ai16));
    else if(ety->isIntegerTy(32))
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(ai32));
    else if(ety->isIntegerTy(64))
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(ai64));
    else if(ety->isFloatTy())
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(af32));
    else if(ety->isDoubleTy())
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(af64));
    else if(ety->isX86_FP80Ty())
      return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(af80));
  } else if(ty->isIntegerTy(1)) {
    return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(i1));
  } else if(ty->isIntegerTy(8)) {
    return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(i8));
  } else if(ty->isIntegerTy(16)) {
    return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(i16));
  } else if(ty->isIntegerTy(32)) {
    return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(i32));
  } else if(ty->isIntegerTy(64)) {
    return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(i64));
  } else if(ty->isFloatTy()) {
    return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(f32));
  } else if(ty->isDoubleTy()) {
    return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(f64));
  } else if(ty->isX86_FP80Ty()) {
    return MOYA_QUOTE(MOYA_FN_REGISTER_ARG(f80));
  }

  moya_error("Unsupported type for argument registration: " << *ty);
  return nullptr;
}

const char* getExecuteCudaFunctionName() {
  return MOYA_QUOTE(MOYA_FN_EXECUTE_CUDA);
}

// const char* getStatsStartCallFunctionName() {
//   return MOYA_QUOTE(MOYA_FN_STATS_START_CALL);
// }

// const char* getStatsStopCallFunctionName() {
//   return MOYA_QUOTE(MOYA_FN_STATS_STOP_CALL);
// }

// const char* getStatsStartCompileFunctionName() {
//   return MOYA_QUOTE(MOYA_FN_STATS_START_COMPILE);
// }

// const char* getStatsStopCompileFunctionName() {
//   return MOYA_QUOTE(MOYA_FN_STATS_STOP_COMPILE);
// }

const char* getStatsStartExecuteFunctionName() {
  return MOYA_QUOTE(MOYA_FN_STATS_START_EXECUTE);
}

const char* getStatsStopExecuteFunctionName() {
  return MOYA_QUOTE(MOYA_FN_STATS_STOP_EXECUTE);
}

const char* getTopErrorFunctionName() {
  return MOYA_QUOTE(MOYA_FN_TOP_ERROR);
}

static const char* getRedirecterFunctionNameBase() {
  return MOYA_QUOTE(MOYA_FN_REDIRECTER_BASE);
}

bool isRedirecterFunction(const std::string& fname) {
  return fname.find(getRedirecterFunctionNameBase()) == 0;
}

// Sentinel functions
const char* getSentinelStartFunctionName() {
  return MOYA_QUOTE(MOYA_FN_SENTINEL_START);
}

const char* getSentinelStopFunctionName() {
  return MOYA_QUOTE(MOYA_FN_SENTINEL_STOP);
}

bool isSentinelFunction(const llvm::Function& f) {
  if((f.getName() == getSentinelStartFunctionName())
     or (f.getName() == getSentinelStopFunctionName()))
    return true;
  return false;
}

bool isSentinelFunction(const llvm::Function* f) {
  return isSentinelFunction(*f);
}

// Function signatures
static std::string getIntegerTypeSignature(unsigned size, bool sign = false) {
  std::stringstream ss;

  if(not sign)
    ss << "unsigned ";
  switch(size) {
  case 4:
    ss << "int";
    break;
  case 8:
    ss << "long long";
    break;
  default:
    moya_error("Unsupported integer type size: " << size);
    break;
  }

  return ss.str();
}

static std::string getFunctionSignature(const std::string& fname,
                                        const std::string& ret,
                                        const Vector<std::string>& params) {
  std::stringstream ss;

  ss << ret << " " << fname << "(";
  if(params.size()) {
    ss << params[0];
    for(unsigned i = 1; i < params.size(); i++)
      ss << ", " << params[i];
  }
  ss << ")";

  return ss.str();
}

std::string getEnterRegionFunctionSignature() {
  return getFunctionSignature(getEnterRegionFunctionName(),
                              "void",
                              {getIntegerTypeSignature(sizeof(RegionID))});
}

std::string getExitRegionFunctionSignature() {
  return getFunctionSignature(getExitRegionFunctionName(),
                              "void",
                              {getIntegerTypeSignature(sizeof(RegionID))});
}

std::string getSentinelStartFunctionSignature() {
  return getFunctionSignature(getSentinelStartFunctionName(),
                              "void",
                              {getIntegerTypeSignature(sizeof(MoyaID))});
}

std::string getSentinelStopFunctionSignature() {
  return getFunctionSignature(getSentinelStopFunctionName(),
                              "void",
                              {getIntegerTypeSignature(sizeof(MoyaID))});
}

const char* getEnvVarDisabled() {
  return "MOYA_OFF";
}

const char* getEnvVarVerbose() {
  return "MOYA_VERBOSE";
}

const char* getEnvVarSaveDir() {
  return "MOYA_SAVE_DIR";
}

const char* getEnvVarCacheDir() {
  return "MOYA_CACHE_DIR";
}

const char* getEnvVarNoAggrConstProp() {
  return "MOYA_NO_ADCP";
}

const char* getEnvVarUsePolly() {
  return "MOYA_USE_POLLY";
}

const char* getEnvVarSaveCode() {
  return "MOYA_SAVE_CODE";
}

const char* getEnvVarSaveOptReport() {
  return "MOYA_SAVE_OPT_REPORT";
}

const char* getEnvVarSaveStats() {
  return "MOYA_SAVE_STATS";
}

const char* getEnvVarWorkingDir() {
  return "MOYA_WORKING_DIR";
}

const char* getEnvVarPayloadDir() {
  return "MOYA_PAYLOAD_DIR";
}

const char* getEnvVarDebugFns() {
  return "MOYA_DEBUG_FNS";
}

const char* getEnvVarDebugFnsImmediate() {
  return "MOYA_DEBUG_FNS_IMM";
}

const char* getEnvVarDebugStore() {
  return "MOYA_DEBUG_STORE";
}

const char* getEnvVarErrorOnTop() {
  return "MOYA_ERROR_ON_TOP";
}

const char* getEnvVarTempfileFlang1() {
  return "MOYA_TEMPFILE_FLANG1";
}

const char* getEnvVarTempfileFlang2() {
  return "MOYA_TEMPFILE_FLANG2";
}

const char* getELFBitcodeSectionName() {
  return ".moya";
}

const char* getBitcodeGlobalNamePrefix() {
  return MOYA_QUOTE(MOYA_PREFIX) "bitcode_global_";
}

std::string getFileBitcodeFileName(const std::string& filename) {
  std::stringstream ss;

  auto p = StringUtils::rpartition(filename, ".");
  std::string base = p.first;
  for(unsigned i = 0; i < base.length(); i++)
    if(ispunct(base[i]))
      ss << '_';
    else
      ss << base[i];
  ss << ".bc";

  return ss.str();
}

std::string getFileBitcodeGlobalName(const std::string& filename) {
  std::stringstream ss;

  ss << getBitcodeGlobalNamePrefix();
  for(unsigned i = 0; i < filename.length(); i++)
    if(ispunct(filename[i]))
      ss << '_';
    else
      ss << filename[i];

  return ss.str();
}

uint64_t getPayloadMagicNumber() {
  return 0x301AFEED;
}

std::string getPayloadFunctionGlobalName() {
  std::stringstream ss;

  ss << MOYA_QUOTE(MOYA_PREFIX) << "payload_function";

  return ss.str();
}

std::string getPayloadRegionGlobalName() {
  std::stringstream ss;

  ss << MOYA_QUOTE(MOYA_PREFIX) << "payload_region";

  return ss.str();
}

std::string getPickledSummaryFilename(RegionID region, JITID key) {
  std::stringstream ss;

  ss << MOYA_QUOTE(MOYA_PREFIX) << "summary_" << region << "_" << key << ".pkl";

  return ss.str();
}

std::string getPickledModuleFilename(RegionID region, const llvm::Function& f) {
  std::stringstream ss;

  ss << "payload_" << region << "_" << Metadata::getID(f);

  return ss.str();
}

std::string generateGetterName(const llvm::GlobalValue& g) {
  std::stringstream ss;
  ss << MOYA_QUOTE(MOYA_PREFIX) << "getter_" << g.getName().str() << "_" << &g;
  return ss.str();
}

std::string generateRedirecterName(const std::string& fn, const Location& loc) {
  std::string s;
  llvm::raw_string_ostream ss(s);

  ss << getRedirecterFunctionNameBase() << generateJITID(fn, loc);

  return ss.str();
}

// Environment variables controlling Moya
bool getEnvVarValueAsBool(const char* val) {
  if(not val)
    return false;

  std::string s = StringUtils::lower(val);
  if(s == "0" or s == "false")
    return false;
  else if(s == "1" or s == "true")
    return true;
  return false;
}

static std::string getEnvVarValueAsPath(const char* envVarname, bool strict) {
  if(const char* out = getenv(envVarname)) {
    std::string path = PathUtils::abspath(out);
    if(PathUtils::exists(path))
      return path;
    else if(not strict)
      return path;
    else
      moya_warning("Path does not exist: " << path);
  }
  return "";
}

bool getEnvDisabled() {
  return getEnvVarValueAsBool(getenv(getEnvVarDisabled()));
}

bool getEnvVerbose() {
  return getEnvVarValueAsBool(getenv(getEnvVarVerbose()));
}

std::string getEnvSaveDir() {
  return getEnvVarValueAsPath(getEnvVarSaveDir(), true);
}

std::string getEnvCacheDir() {
  return getEnvVarValueAsPath(getEnvVarCacheDir(), true);
}

bool isEnvSaveEnabled() {
  return (getEnvSaveDir().size() > 0);
}

bool isEnvCacheEnabled() {
  return (getEnvCacheDir().size() > 0);
}

bool isEnvNoAggrConstProp() {
  return getEnvVarValueAsBool(getenv(getEnvVarNoAggrConstProp()));
}

bool isEnvUsePolly() {
  return getEnvVarValueAsBool(getenv(getEnvVarUsePolly()));
}

bool isEnvSaveCode() {
  return getEnvVarValueAsBool(getenv(getEnvVarSaveCode()));
}

bool isEnvSaveOptReport() {
  return getEnvVarValueAsBool(getenv(getEnvVarSaveOptReport()));
}

bool isEnvSaveStats() {
  return getEnvVarValueAsBool(getenv(getEnvVarSaveStats()));
}

bool isEnvErrorOnTop() {
  return getEnvVarValueAsBool(getenv(getEnvVarErrorOnTop()));
}

Set<std::string> getEnvDebugFns() {
  if(const char* out = getenv(getEnvVarDebugFns())) {
    Vector<std::string> v = StringUtils::split(out, ':');
    return Set<std::string>(v.begin(), v.end());
  }
  return {};
}

Set<std::string> getEnvDebugFnsImmediate() {
  if(const char* out = getenv(getEnvVarDebugFnsImmediate())) {
    Vector<std::string> v = StringUtils::split(out, ':');
    return Set<std::string>(v.begin(), v.end());
  }
  return {};
}

unsigned getEnvDebugStore() {
  if(const char* out = getenv(getEnvVarDebugStore())) {
    std::string s = StringUtils::lower(out);
    if(s == "before")
      return 0x1;
    else if(s == "after")
      return 0x2;
    else if(s == "both")
      return 0x3;
  }
  return 0x0;
}

// ID-related functions

// This should be kept consistent with the comments in APITypes.h
// These are here because there is no good reason to expose them at all
static const uint32_t IDKindBits = 4;
static const MoyaID IDKindMask = 0xFULL;
static const MoyaID IDValueMask = ~IDKindMask;

static MoyaID getIDBase(MoyaID id) {
  return static_cast<MoyaID>(id & IDValueMask);
}

static MoyaID getID(MoyaID id, IDKind kind) {
  return static_cast<MoyaID>((id & IDValueMask) | static_cast<MoyaID>(kind));
}

IDKind getIDKind(MoyaID id) {
  return static_cast<IDKind>(id & IDKindMask);
}

MoyaID getFirstValidMoyaID(IDKind kind) {
  return getID(static_cast<uint64_t>(IDKindMask) + 1, kind);
}

MoyaID getNextMoyaID(MoyaID id) {
  MoyaID base = getIDBase(id);
  return getID(((base >> IDKindBits) + 1) << IDKindBits, getIDKind(id));
}

RegionID generateRegionID(const Location& loc) {
  return getID(loc.getHash(), IDKind::JITRegion);
}

LoopID generateLoopID(const Location& loc) {
  return getID(loc.getHash(), IDKind::Loop);
}

JITID generateJITID(const std::string& fn, const Location& loc) {
  Hash hash = loc.getHash();
  Hasher::update(fn, hash);

  return getID(hash, IDKind::Function);
}

bool isInvalidJITID(JITID key) {
  return key == getInvalidJITID();
}

bool isInvalidRegionID(RegionID id) {
  return id == getInvalidRegionID();
}

bool isInvalidLoopID(LoopID id) {
  return id == getInvalidLoopID();
}

JITID getInvalidJITID() {
  return 0;
}

RegionID getInvalidRegionID() {
  return getID(0, IDKind::JITRegion);
}

LoopID getInvalidLoopID() {
  return getID(0, IDKind::Loop);
}

FunctionSignatureTuning getUntunedFunctionSignature() {
  return 0;
}

// Not really Moya-related but necessary
const char* getCudaStreamStructName() {
  return "struct.CUstream_st";
}

} // namespace Config

} // namespace moya
