#ifndef MOYA_COMMON_DEMANGLER_CXX_H
#define MOYA_COMMON_DEMANGLER_CXX_H

#include "Demangler.h"
#include "Set.h"

#include <string>

namespace moya {

class DemangledFunction;

class DemanglerCXX : public Demangler {
protected:
  Set<std::string> special;

protected:
  std::string stripTemplates(const std::string&) const;
  std::string stripTemplates(const std::string&, long, long) const;
  std::string stripCppImplNamespaces(const std::string&) const;
  std::string stripABISpecifications(const std::string&) const;

  std::string parseType(const std::string&) const;
  void parseArgs(const std::string&, DemangledFunction&) const;
  void parseName(const std::string&, DemangledFunction&) const;
  void parse(const std::string&, DemangledFunction&) const;

  std::string demangle(const std::string&, int) const;

protected:
  std::string getSourceName(const std::string&) const;
  std::string getFullName(const std::string&) const;
  std::string getModelName(const std::string&) const;
  std::string getQualifiedName(const std::string&) const;

public:
  DemanglerCXX();
  virtual ~DemanglerCXX() = default;

  virtual std::string demangle(const llvm::Function&,
                               Demangler::Action) const override;
  virtual std::string demangle(const llvm::GlobalVariable&,
                               Demangler::Action) const override;
};

} // namespace moya

#endif // MOYA_COMMON_DEMANGLER_CXX_H
