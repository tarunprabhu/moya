// This contains utilities to encode a file before adding it to the JSON
// output of Moya's results. Currently, this compresses the file using
// zlib and then base64 encodes the compressed result

#include "APITypes.h"
#include "Diagnostics.h"
#include "FileSystemUtils.h"

#include <llvm/Support/raw_ostream.h>

#include <fstream>
#include <sstream>
#include <zlib.h>

namespace moya {

namespace Encoder {

static const char tblEncode[]
    = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
       'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
       'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
       'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
       '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'};

static const unsigned lmask[] = {0xFC, 0xF0, 0xC0};
static const unsigned rmask[] = {0x3, 0xF, 0x3F};
static const unsigned lshift[] = {2, 4, 6};
static const unsigned rshift[] = {4, 2, 0};

static std::string base64(const Vector<byte>& bytes, unsigned long size) {
  std::stringstream ss;
  unsigned num = 0;
  unsigned char prev = 0;

  for(unsigned long i = 0; i < size; i++, num = (num + 1) % 3) {
    unsigned char c = bytes[i];
    unsigned char curr = (c & lmask[num]) >> lshift[num];
    ss << tblEncode[prev | curr];
    prev = (c & rmask[num]) << rshift[num];
    if(num == 2) {
      ss << tblEncode[prev];
      prev = 0;
    }
  }

  if(num % 3) {
    ss << tblEncode[prev];
    for(unsigned j = num + 1; j < 4; j++)
      ss << "=";
  }

  return ss.str();
}

std::string encodeFile(const std::string& path) {
  std::ifstream in(path);
  if(not in.is_open())
    moya_error("Could not open file: " << path);

  unsigned long inflatedSize = moya::FileSystemUtils::getFileSize(path);
  unsigned long deflatedSize = compressBound(inflatedSize);
  Vector<byte> srcBuf(inflatedSize);
  Vector<byte> dstBuf(deflatedSize);

  char c;
  for(unsigned i = 0; in.get(c); i++)
    srcBuf[i] = c;

  int result = compress2(dstBuf.data(),
                         &deflatedSize,
                         srcBuf.data(),
                         inflatedSize,
                         Z_BEST_COMPRESSION);
  if(result != Z_OK)
    moya_error("Could not compress file: " << path << ". Got " << result);

  if(in.is_open())
    in.close();

  return base64(dstBuf, deflatedSize);
}

std::string encodeString(const std::string& s) {
  unsigned long deflatedSize = compressBound(s.length());
  Vector<byte> dstBuf(deflatedSize);
  int result = compress2(dstBuf.data(),
                         &deflatedSize,
                         reinterpret_cast<const byte*>(s.c_str()),
                         s.length(),
                         Z_BEST_COMPRESSION);
  if(result != Z_OK)
    moya_error("Could not compress LLVM. Got" << result);

  return base64(dstBuf, deflatedSize);
}

std::string encodeString(const Vector<unsigned char>& bytes) {
  return base64(bytes, bytes.size());
}

} // namespace Encoder

} // namespace moya
