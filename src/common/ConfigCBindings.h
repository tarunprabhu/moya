#ifndef MOYA_COMMON_CONFIG_C_BINDINGS_H
#define MOYA_COMMON_CONFIG_C_BINDINGS_H

#include "APITypes.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

// These functions are part of the C interface to the MoyaConfig class
const char* MoyaConfig_getEnvVarDisabled();
const char* MoyaConfig_getEnvVarVerbose();
const char* MoyaConfig_getEnvVarVerbose();
const char* MoyaConfig_getEnvVarSaveDir();
const char* MoyaConfig_getEnvVarWorkingDir();
const char* MoyaConfig_getEnvVarPayloadDir();
const char* MoyaConfig_getEnvVarDebugFns();
const char* MoyaConfig_getEnvVarDebugStore();
const char* MoyaConfig_getELFBitcodeSectionName();

uint64_t MoyaConfig_getPayloadMagicNumber();

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // MOYA_COMMON_CONFIG_C_BINDINGS_H
