#ifndef MOYA_COMMON_SUMMARY_CELL_BASE_H
#define MOYA_COMMON_SUMMARY_CELL_BASE_H

#include "APITypes.h"
#include "Contents.h"
#include "Status.h"
#include "Vector.h"

#include <llvm/IR/Type.h>

namespace moya {

// This class represents cells in the summary. These have fewer fields than
// the full-blown cells used in the store for the analysis.
class SummaryCellBase {
public:
  using Address = ContentAddress::Raw;
  using Targets = Vector<Address>;

protected:
  Address addr;

  // Each cell is guaranteed to "belong" to a single JIT'ed function, so we
  // just need the one status field
  Status status;

  // In the summaries, a cell that corresponds to a pointer type will have
  // a single entry to the target of the pointer. All other cells will be
  // empty
  Targets targets;

protected:
  SummaryCellBase();

public:
  SummaryCellBase(const SummaryCellBase&) = delete;
  SummaryCellBase(const SummaryCellBase&&) = delete;
  virtual ~SummaryCellBase() = default;

  Address getAddress() const;
  llvm::Type* getType() const;
  const Status& getStatus() const;
  const Targets& getTargets() const;

public:
  static const Address null = ContentAddress::null;
};

} // namespace moya

#endif // MOYA_COMMON_SUMMARY_CELL_BASE_H
