#ifndef MOYA_COMMON_INSTRUCTION_UTILS_H
#define MOYA_COMMON_INSTRUCTION_UTILS_H

#include "Location.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>

namespace moya {

namespace LLVMUtils {

// These are utilities that operate on LLVM instructions.
template <typename T>
llvm::Function* getCalledFunction(T*);
template <typename T>
const llvm::Function* getCalledFunction(const T*);
llvm::Function* getCalledFunction(llvm::Instruction*, bool = true);
const llvm::Function* getCalledFunction(const llvm::Instruction*, bool = true);

llvm::Value* getArgOperand(llvm::Instruction*, unsigned);
const llvm::Value* getArgOperand(const llvm::Instruction*, unsigned);

template <typename T,
          typename std::enable_if_t<std::is_integral<T>::value, int> = 0>
T getArgOperandAs(const llvm::Instruction* inst, unsigned op) {
  return llvm::dyn_cast<llvm::ConstantInt>(getArgOperand(inst, op))
      ->getLimitedValue();
}

llvm::Type* getArgOperandType(const llvm::Instruction*, unsigned);
template <typename T>
T* getArgOperandTypeAs(const llvm::Instruction* inst, unsigned op) {
  return llvm::dyn_cast_or_null<T>(getArgOperandType(inst, op));
}

llvm::Type* getAnalysisType(const llvm::Instruction*);
template <typename T>
T* getAnalysisTypeAs(const llvm::Instruction* inst) {
  return llvm::dyn_cast_or_null<T>(getAnalysisType(inst));
}

// Get the module in which this instruction belongs
const llvm::Module* getModule(const llvm::Instruction*);
llvm::Module* getModule(llvm::Instruction*);

// Get the function in which this instruction belongs
const llvm::Function* getFunction(const llvm::Instruction*);
llvm::Function* getFunction(llvm::Instruction*);

// Returns the parent of an instruction. Just so I have to type less
llvm::StringRef getFunctionName(const llvm::Instruction*);

bool arePHINodeIncomingValuesEqual(const llvm::PHINode*);

bool setMetadata(llvm::Instruction*, const std::string&);
bool resetMetadata(llvm::Instruction*, const std::string&);

llvm::Type* getAllocatedType(const llvm::Instruction*);

Location getLocation(const llvm::Instruction*);

} // namespace LLVMUtils

} // namespace moya

#endif // MOYA_COMMON_INSTRUCTION_UTILS_H
