#ifndef MOYA_COMMON_FUNCTION_UTILS_H
#define MOYA_COMMON_FUNCTION_UTILS_H

#include <llvm/IR/Argument.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>

namespace moya {

namespace LLVMUtils {

llvm::Function* getPersonalityFn(const llvm::Function&);
llvm::Function* getPersonalityFn(const llvm::Function*);

// Returns the function argument at the specified index
const llvm::Argument* getArgument(const llvm::Function*, unsigned);
const llvm::Argument* getArgument(const llvm::Function&, unsigned);
llvm::Argument* getArgument(llvm::Function*, unsigned);
llvm::Argument* getArgument(llvm::Function&, unsigned);

llvm::Type* getArgumentType(const llvm::Function*, unsigned);
llvm::Type* getArgumentType(const llvm::Function&, unsigned);
template <typename T>
T* getArgumentTypeAs(const llvm::Function* f, unsigned argNo) {
  return llvm::dyn_cast_or_null<T>(getArgumentType(f, argNo));
}
template <typename T>
T* getArgumentTypeAS(const llvm::Function& f, unsigned argNo) {
  return llvm::dyn_cast_or_null<T>(getArgumentType(f, argNo));
}

unsigned getNumParams(const llvm::Function*);
unsigned getNumParams(const llvm::Function&);

bool hasReturn(const llvm::Function*);
bool hasReturn(const llvm::Function&);

llvm::Module* getModule(llvm::Function&);
llvm::Module* getModule(llvm::Function*);
const llvm::Module* getModule(const llvm::Function&);
const llvm::Module* getModule(const llvm::Function*);

bool isPureFunction(const llvm::Function&);
bool isPureFunction(const llvm::Function*);

} // namespace LLVMUtils

} // namespace moya

#endif // MOYA_COMMON_FUNCTION_UTILS_H
