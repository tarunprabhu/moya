#ifndef MOYA_COMMON_STATUS_H
#define MOYA_COMMON_STATUS_H

#include <string>

namespace moya {

class PayloadBase;
class Serializer;

class Status final {
public:
  using Raw = uint32_t;

private:
  //          +-----------------------------------------+
  //          | . 9 . 8 . 7 . 6 . 5 . 4 . 3 . 2 . 1 . 0 |
  //          |-----------------------------------------|
  //          |                       K . R . _ . C . U |
  //          +-----------------------------------------+
  //
  // _ = Unused
  // U = Used
  // C = Constant
  // R = The function argument that this status summarizes should be deferenced
  //     before being used in the key (the key is the set of constants used to
  //     uniquely identify a run-time specialized function). This is only
  //     relevant when the status is associated with a function argument
  // K = The value at the cell is used to compute the key. This way, even if
  //     the cell is marked as variable, it can be used to propagate a constant
  //
  Raw status;

private:
  static const unsigned Unused = 0x0;
  static const unsigned Constant = 0x1;
  static const unsigned Variable = 0x3;

  static const unsigned DerefInKey = 0x8;
  static const unsigned UsedInKey = 0x10;

public:
  Raw getRaw() const;

public:
  Status(bool, bool);
  Status(unsigned = 0);
  Status(const Status&);
  Status(const Status&&);

  Status& operator=(const Status&);

  Status& setConstant(bool = false);
  Status& setVariable(bool = false);
  Status& setDerefInKey();
  Status& setUsedInKey();

  Status& reset(const Status&);
  Status& update(const Status&);

  bool isUsed() const;
  bool isConstant() const;
  bool isVariable() const;
  bool isDerefInKey() const;
  bool isUsedInKey() const;

  std::string str() const;
  void serialize(Serializer&) const;
  void pickle(PayloadBase&) const;
  void unpickle(PayloadBase&);

public:
  static Status getUnused();
  static Status getConstant();
  static Status getVariable();
};

} // namespace moya

bool operator==(const moya::Status&, const moya::Status&);
bool operator!=(const moya::Status&, const moya::Status&);

#endif // MOYA_COMMON_STATUS_H
