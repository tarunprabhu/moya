#ifndef MOYA_COMMON_SERIALIZER_H
#define MOYA_COMMON_SERIALIZER_H

#include "Formatting.h"
#include "Stringify.h"

#include <llvm/Support/raw_ostream.h>

#include <string>

namespace moya {

class Serializer {
protected:
  std::string buf;
  std::unique_ptr<llvm::raw_fd_ostream> poutf;
  std::unique_ptr<llvm::raw_string_ostream> pouts;

  // Current indentation level
  int tab;

  // Does the entry need to be preceded by a comma and newline
  bool comma;

protected:
  llvm::raw_ostream& out();
  llvm::raw_fd_ostream& outf();
  llvm::raw_string_ostream& outs();
  llvm::raw_string_ostream& outs() const;

  void next();

  std::string quote(const std::string&, bool = true);

  // Special cases for certain types
  template <typename... ArgsT>
  void write(const void* ptr, ArgsT&&...) {
    out() << ptr;
  }

  template <typename... ArgsT>
  void write(const bool& b, ArgsT&&...) {
    if(b)
      out() << "true";
    else
      out() << "false";
  }

  template <typename... ArgsT>
  void write(const std::string& s, ArgsT&&... args) {
    out() << quote(s, args...);
  }

  template <typename... ArgsT>
  void write(const char* s, ArgsT&... args) {
    out() << quote(s, args...);
  }

  template <typename T,
            typename std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
  void write(const T& val) {
    out() << val;
  }

  template <typename T,
            typename std::enable_if_t<std::is_enum<T>::value, int> = 0>
  void write(const T& val) {
    out() << static_cast<int>(val);
  }

  template <typename T,
            typename... ArgsT,
            typename std::enable_if_t<std::is_class<T>::value, int> = 0>
  void write(const T& val, ArgsT&&... args) {
    val.serialize(*this, args...);
  }

  template <typename T,
            typename... ArgsT,
            typename std::enable_if_t<std::is_class<T>::value, int> = 0>
  void write(const T* ptr, ArgsT&&... args) {
    ptr->serialize(*this, args...);
  }

  template <typename T, typename... ArgsT>
  void write(const std::unique_ptr<T>& ptr, ArgsT&&... args) {
    serialize(*ptr.get(), args...);
  }

public:
  Serializer();
  virtual ~Serializer() = default;

  void initialize();
  void initialize(const std::string&);
  void finalize();

  bool isInitialized() const;
  const std::string& str() const;

  void mapStart(const std::string& = "");
  void mapEnd(const std::string& = "");
  void arrStart(const std::string& = "");
  void arrEnd(const std::string& = "");
  void pairStart(const std::string& = "");
  void pairEnd(const std::string& = "");

  void endl();

  // This writes to JSON and all the keys must be strings
  //
  template <typename V, typename... ArgsT>
  void add(const std::string& key, const V& val, ArgsT&&... args) {
    if(comma)
      next();
    endl();
    write(key);
    comma = false;
    out() << ": ";
    write(val, args...);
    comma = true;
  }

  template <typename T, typename... ArgsT>
  void serialize(const T& val, ArgsT&&... args) {
    if(comma)
      next();
    endl();
    write(val, args...);
    comma = true;
  }
};

} // namespace moya

#endif // MOYA_COMMON_SERIALIZER_H
