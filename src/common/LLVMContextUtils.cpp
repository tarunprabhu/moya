#include "LLVMUtils.h"

using namespace llvm;

namespace moya {

namespace LLVMUtils {

Type* getMoyaIDType(LLVMContext& context) {
  return IntegerType::get(context, sizeof(MoyaID) * 8);
}

Type* getJITIDType(LLVMContext& context) {
  return IntegerType::get(context, sizeof(JITID) * 8);
}

Constant* getJITIDConstant(LLVMContext& context, JITID id) {
  return ConstantInt::get(getJITIDType(context), id, false);
}

Type* getRegionIDType(LLVMContext& context) {
  return IntegerType::get(context, sizeof(RegionID) * 8);
}

Constant* getRegionIDConstant(LLVMContext& context, RegionID id) {
  return ConstantInt::get(getRegionIDType(context), id, false);
}

Type* getFunctionSignatureTuningType(LLVMContext& context) {
  return IntegerType::get(context, sizeof(FunctionSignatureTuning) * 8);
}

Type* getFunctionSignatureBasicType(LLVMContext& context) {
  return IntegerType::get(context, sizeof(FunctionSignatureBasic) * 8);
}

} // namespace LLVMUtils

} // namespace moya
