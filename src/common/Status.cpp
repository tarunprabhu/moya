#include "Status.h"
#include "PayloadBase.h"
#include "Serializer.h"

#include <sstream>

namespace moya {

Status::Status(bool read, bool written) : status(Unused) {
  if(read)
    setConstant();
  if(written)
    setVariable();
}

Status::Status(unsigned raw) : status(raw) {
  ;
}

Status::Status(const Status& o) : status(o.getRaw()) {
  ;
}

Status::Status(const Status&& o) : status(o.getRaw()) {
  ;
}

Status& Status::operator=(const Status& o) {
  this->status = o.status;
  return *this;
}

bool Status::isUsed() const {
  return status != Unused;
}

bool Status::isConstant() const {
  return (status & Variable) == Constant;
}

bool Status::isVariable() const {
  return (status & Variable) == Variable;
}

bool Status::isDerefInKey() const {
  return status & DerefInKey;
}

bool Status::isUsedInKey() const {
  return status & UsedInKey;
}

unsigned Status::getRaw() const {
  return status;
}

Status& Status::setConstant(bool force) {
  if(force)
    status &= ~Variable;
  status |= Constant;
  return *this;
}

Status& Status::setVariable(bool force) {
  status |= Variable;
  return *this;
}

Status& Status::setDerefInKey() {
  status |= DerefInKey;
  return *this;
}

Status& Status::setUsedInKey() {
  status |= UsedInKey;
  return *this;
}

Status& Status::reset(const Status& nw) {
  status = nw.getRaw();
  return *this;
}

Status& Status::update(const Status& nw) {
  status = status | nw.getRaw();
  return *this;
}

std::string Status::str() const {
  std::stringstream ss;

  ss << "-";
  ss << (isUsedInKey() ? "K" : "-");
  ss << (isDerefInKey() ? "D" : "-");
  ss << "-";
  ss << (isConstant() ? "C" : "-");
  ss << (isUsed() ? "U" : "-");

  return ss.str();
}

void Status::serialize(Serializer& s) const {
  s.serialize(str());
}

void Status::pickle(PayloadBase& p) const {
  p.pickle(getRaw());
}

void Status::unpickle(PayloadBase& p) {
  status = p.unpickle<Raw>();
}

Status Status::getVariable() {
  return Status(Variable);
}

Status Status::getConstant() {
  return Status(Constant);
}

Status Status::getUnused() {
  return Status(Unused);
}

} // namespace moya

bool operator==(const moya::Status& s1, const moya::Status& s2) {
  return s1.getRaw() == s2.getRaw();
}

bool operator!=(const moya::Status& s1, const moya::Status& s2) {
  return s1.getRaw() != s2.getRaw();
}
