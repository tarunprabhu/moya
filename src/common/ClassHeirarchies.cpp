#include "ClassHeirarchies.h"
#include "Diagnostics.h"
#include "Dotifier.h"
#include "Formatting.h"
#include "Metadata.h"
#include "Serializer.h"
#include "StringUtils.h"
#include "common/Set.h"

#include <llvm/IR/Constants.h>

using namespace llvm;

namespace moya {

class ClassHeirarchiesNode {
private:
  const Module& module;
  StructType* sty;
  std::string name;
  Set<const ClassHeirarchiesNode*> bases;
  Set<const ClassHeirarchiesNode*> vbases;
  Set<const ClassHeirarchiesNode*> derived;

private:
  void addDerived(ClassHeirarchiesNode* derived) {
    if(not derived or not this->derived.insert(derived))
      return;
    derived->addBase(this);
  }

public:
  ClassHeirarchiesNode(llvm::StructType* sty, const llvm::Module& module)
      : module(module), sty(sty), name(sty->getName()) {
    if(Metadata::hasSourceName(sty, module))
      name = Metadata::getSourceName(sty, module);
  }

  const Module& getModule() const {
    return module;
  }

  StructType* getStructType() const {
    return sty;
  }

  const std::string& getName() const {
    return name;
  }

  bool isDerived(const ClassHeirarchiesNode* node) const {
    return derived.contains(node);
  }

  bool isBase(const ClassHeirarchiesNode* node) const {
    return bases.contains(node);
  }

  bool isVirtualBase(const ClassHeirarchiesNode* node) const {
    return vbases.contains(node);
  }

  const Set<const ClassHeirarchiesNode*>& getDerived() const {
    return derived;
  }

  const Set<const ClassHeirarchiesNode*>& getBases() const {
    return bases;
  }

  const Set<const ClassHeirarchiesNode*>& getVirtualBases() const {
    return vbases;
  }

  void addBase(ClassHeirarchiesNode* base) {
    // FIXME: I have no idea why this happening, but it is for some reason
    if(base->getStructType() == sty)
      return;
    if(not base or not this->bases.insert(base))
      return;
    base->addDerived(this);
  }

  void addVirtualBase(ClassHeirarchiesNode* base) {
    // FIXME: I have no idea why this happening, but it is for some reason
    if(base->getStructType() == sty)
      return;
    if(not base or not this->vbases.insert(base))
      return;
    base->addDerived(this);
  }
};

// ClassHeriarchies method definitions

ClassHeirarchies::~ClassHeirarchies() {
  for(auto& it : nodes)
    delete it.second;
}

ClassHeirarchiesNode* ClassHeirarchies::getNode(StructType* s,
                                                const Module& module) {
  if(nodes.contains(s))
    return nodes.at(s);
  nodes[s] = new ClassHeirarchiesNode(s, module);
  return nodes.at(s);
}

const ClassHeirarchiesNode* ClassHeirarchies::getNode(StructType* s) const {
  return nodes.at(s);
}

bool ClassHeirarchies::hasNode(StructType* s) const {
  return nodes.contains(s);
}

bool ClassHeirarchies::isBase(const ClassHeirarchiesNode* node,
                              StructType* sty) const {
  for(const ClassHeirarchiesNode* base : node->getBases())
    if((base->getStructType() == sty) or isBase(base, sty))
      return true;
  for(const ClassHeirarchiesNode* base : node->getVirtualBases())
    if((base->getStructType() == sty) or isBase(base, sty))
      return true;
  return false;
}

bool ClassHeirarchies::isVirtualBase(const ClassHeirarchiesNode* node,
                                     StructType* sty) const {
  for(const ClassHeirarchiesNode* base : node->getVirtualBases())
    if((base->getStructType() == sty) or isVirtualBase(base, sty))
      return true;
  for(const ClassHeirarchiesNode* base : node->getBases())
    if(isVirtualBase(base, sty))
      return true;
  return false;
}

bool ClassHeirarchies::isDerivedFrom(StructType* derivedTy,
                                     StructType* baseTy) const {
  if(hasNode(baseTy) and hasNode(derivedTy))
    return isBase(getNode(derivedTy), baseTy);
  return false;
}

bool ClassHeirarchies::isDerivedFrom(Type* derivedTy, Type* baseTy) const {
  if(auto* b = dyn_cast<StructType>(baseTy))
    if(auto* d = dyn_cast<StructType>(derivedTy))
      return isDerivedFrom(d, b);
  return false;
}

bool ClassHeirarchies::isBaseOf(Type* baseTy, Type* derivedTy) const {
  return isDerivedFrom(derivedTy, baseTy);
}

bool ClassHeirarchies::isBaseOf(StructType* baseTy,
                                StructType* derivedTy) const {
  return isDerivedFrom(derivedTy, baseTy);
}

bool ClassHeirarchies::isVirtualBaseOf(Type* baseTy, Type* derivedTy) const {
  if(auto* b = dyn_cast<StructType>(baseTy))
    if(auto* d = dyn_cast<StructType>(derivedTy))
      return isVirtualBaseOf(b, d);
  return false;
}

bool ClassHeirarchies::isVirtualBaseOf(StructType* baseTy,
                                       StructType* derivedTy) const {
  if(hasNode(baseTy) and hasNode(derivedTy))
    return isVirtualBase(getNode(derivedTy), baseTy);
  return false;
}

void ClassHeirarchies::initialize(const Module& module) {
  for(StructType* sty : module.getIdentifiedStructTypes()) {
    if(Metadata::hasBases(sty, module))
      for(StructType* base : Metadata::getBases(sty, module))
        getNode(sty, module)->addBase(getNode(base, module));
    if(Metadata::hasVirtualBases(sty, module))
      for(StructType* base : Metadata::getVirtualBases(sty, module))
        getNode(sty, module)->addVirtualBase(getNode(base, module));
  }
}

void ClassHeirarchies::dotify(Dotifier& dot) const {
  dot.start(true);
  for(auto& it : nodes) {
    const ClassHeirarchiesNode* node = it.second;
    for(const ClassHeirarchiesNode* derived : node->getDerived()) {
      Map<std::string, std::string> properties;
      if(derived->isVirtualBase(node))
        properties["label"] = "virtual";
      dot.addEdge(node->getName(), derived->getName(), properties);
    }
  }
  dot.end();
}

void ClassHeirarchies::serialize(Serializer& s) const {
  s.mapStart();
  for(auto& it : nodes) {
    const ClassHeirarchiesNode* base = it.second;
    s.arrStart(str(*base->getStructType()));
    for(const ClassHeirarchiesNode* derived : base->getDerived())
      s.serialize(str(*derived->getStructType()));
    s.arrEnd();
  }
  s.mapEnd();
}

} // namespace moya
