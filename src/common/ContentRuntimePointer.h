#ifndef MOYA_COMMON_CONTENT_RUNTIME_POINTER_H
#define MOYA_COMMON_CONTENT_RUNTIME_POINTER_H

#include "Content.h"

namespace moya {

// This class is used to represent a runtime pointer i.e. some value which is
// only known at runtime. We create a class which is separate from the scalar
// value because this value will be handled differently - if this is
// encountered, the analysis might immediately hit top
class ContentRuntimePointer : public Content {
public:
  ContentRuntimePointer();
  virtual ~ContentRuntimePointer() = default;

  virtual std::string str() const;
  virtual void serialize(Serializer&) const;

public:
  static bool classof(const Content*);
};

} // namespace moya

#endif // MOYA_COMMON_CONTENT_RUNTIME_POINTER_H
