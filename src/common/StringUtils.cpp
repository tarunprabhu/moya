#include "StringUtils.h"
#include "Set.h"

#include <algorithm>
#include <iostream>
#include <sstream>

namespace moya {

namespace StringUtils {

std::string lstrip(const std::string& s) {
  for(size_t i = 0; i < s.size(); i++)
    if(not isspace(s[i]))
      return s.substr(i);
  return std::string();
}

std::string rstrip(const std::string& s) {
  for(size_t i = s.size(); i > 0; i--)
    if(not isspace(s[i - 1]))
      return s.substr(0, i);
  return std::string();
}

std::string strip(const std::string& s) {
  return lstrip(rstrip(s));
}

std::string lower(const std::string& s) {
  std::string copy(s.length(), '\0');

  transform(s.begin(), s.end(), copy.begin(), ::tolower);

  return copy;
}

Vector<std::string> split(const std::string& s, char sep, bool stripElements) {
  Vector<std::string> rv;
  size_t pos = s.find_first_of(sep);
  std::string first = "";
  std::string rest = s;
  while(pos != std::string::npos) {
    first = rest.substr(0, pos);
    rest = rest.substr(pos + 1);
    rv.push_back(stripElements ? strip(first) : first);
    pos = rest.find_first_of(sep);
  }
  rv.push_back(stripElements ? strip(rest) : rest);
  return rv;
}

Vector<std::string>
split(const std::string& s, const std::string& sep, bool stripElements) {
  Vector<std::string> rv;
  size_t pos = s.find(sep);
  unsigned long sepSz = sep.length();
  std::string first = "";
  std::string rest = s;
  while(pos != std::string::npos) {
    first = rest.substr(0, pos);
    rest = rest.substr(pos + sepSz);
    rv.push_back(stripElements ? strip(first) : first);
    pos = rest.find(sep);
  }
  rv.push_back(stripElements ? strip(rest) : rest);
  return rv;
}

Vector<std::string>
split(const std::string& s, char sep, unsigned long begin, bool strip) {
  return split(s.substr(begin), sep, strip);
}

Vector<std::string> split(const std::string& s,
                          char sep,
                          unsigned long begin,
                          unsigned long end,
                          bool strip) {
  return split(s.substr(begin, end), sep, strip);
}

std::string replace(const std::string& in,
                    const std::string& search,
                    const std::string& repl) {
  std::string ret(in);
  size_t lenSearch = search.size();
  size_t lenRepl = repl.size();
  size_t pos = ret.find(search);
  while(pos != std::string::npos) {
    ret.erase(pos, lenSearch);
    ret.insert(pos, repl);
    pos += lenRepl;
    pos = ret.find(search, pos);
  }
  return ret;
}

std::string replace(const std::string& in,
                    std::string::size_type size,
                    std::string::size_type count,
                    const std::string& repl) {
  std::string t(in);
  t.replace(size, count, repl);

  return t;
}

std::pair<std::string, std::string> lpartition(const std::string& in,
                                               const std::string& sep) {
  Vector<std::string> elems = split(in, sep);
  if(elems.size() == 1)
    return make_pair(in, "");
  std::stringstream ss;
  ss << elems[1];
  for(size_t i = 2; i < elems.size(); i++)
    ss << sep << elems[i];
  return std::make_pair(elems.front(), ss.str());
}

std::pair<std::string, std::string> rpartition(const std::string& in,
                                               const std::string& sep) {
  Vector<std::string> elems = split(in, sep);
  if(elems.size() == 1)
    return make_pair("", in);
  std::stringstream ss;
  ss << elems[0];
  for(size_t i = 1; i < elems.size() - 1; i++)
    ss << sep << elems[i];
  return std::make_pair(ss.str(), elems.back());
}

bool contains(const std::string& s, const std::string& key) {
  return s.find(key) != std::string::npos;
}

bool startswith(const std::string& s, const std::string& key) {
  size_t i = 0;
  while(1) {
    // We compile with C++11 or higher, so
    // s[s.size()] == '\0'

    // If we got this far without breaking and we are done with the key, then
    // obviously the string starts with the key
    if(key[i] == '\0')
      return true;

    // If the string to be searched ended, then it is shorter than key and
    // therefore cannot possibly match.
    if((s[i] == '\0') or (s[i] != key[i]))
      return false;

    i += 1;
  }
}

bool endswith(const std::string& s, const std::string& key) {
  long si = s.size();
  long ki = key.size();
  while(1) {
    // We compile with C++11 or higher, so
    // s[s.size()] == '\0'

    // If we got this far without breaking, we obviously found the key at the
    // end of the string
    if(ki == -1L)
      return true;

    // If the string ended before the key, then it was shorter than the key
    // and therefore cannot possibly match
    if((si == -1L) or (s[si] != key[ki]))
      return false;

    si -= 1;
    ki -= 1;
  }
}

std::string reverse(const std::string& s) {
  std::string t = s;
  std::reverse(t.begin(), t.end());
  return t;
}

} // namespace StringUtils

} // namespace moya
