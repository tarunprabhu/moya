#ifndef MOYA_COMMON_IR_UTILS_H
#define MOYA_COMMON_IR_UTILS_H

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>

#include <string>

namespace moya {

namespace LLVMUtils {

// Reads the a module from the specified file
std::unique_ptr<llvm::Module> readModuleFromFile(const std::string&,
                                                 llvm::LLVMContext&);

} // namespace LLVMUtils

} // namespace moya

#endif // MOYA_COMMON_IR_UTILS_H
