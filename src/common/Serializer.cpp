#include "Serializer.h"
#include "Diagnostics.h"
#include "Stringify.h"

#include <llvm/Support/FileSystem.h>

#include <sstream>

namespace moya {

Serializer::Serializer()
    : poutf(nullptr), pouts(nullptr), tab(0), comma(false) {
  ;
}

void Serializer::initialize() {
  pouts.reset(new llvm::raw_string_ostream(buf));
}

void Serializer::initialize(const std::string& file) {
  std::error_code ec;
  poutf.reset(
      new llvm::raw_fd_ostream(file, ec, llvm::sys::fs::OpenFlags::F_Text));
  if(ec)
    moya_error("Could not open serialization file: " << file);
  tab = 0;
  comma = false;
}

llvm::raw_ostream& Serializer::out() {
  if(poutf)
    return outf();
  else if(pouts)
    return outs();

  moya_error("Serializer not initialized");
  // Just to stop the compiler from warning about an unreturned value
  return outs();
}

llvm::raw_fd_ostream& Serializer::outf() {
  return *poutf.get();
}

llvm::raw_string_ostream& Serializer::outs() {
  return *pouts.get();
}

llvm::raw_string_ostream& Serializer::outs() const {
  return *pouts.get();
}

void Serializer::finalize() {
  if(poutf) {
    outf().flush();
    outf().close();
    poutf.reset(nullptr);
  } else if(pouts) {
    pouts.reset(nullptr);
  } else {
    moya_error("Finalizing uninitialized serialization object");
  }
}

bool Serializer::isInitialized() const {
  if(poutf or pouts)
    return true;
  return false;
}

static const std::string emptyStr = "";
const std::string& Serializer::str() const {
  if(poutf)
    moya_warning("<writing to file>");
  else if(pouts)
    return outs().str();
  else
    moya_error("Dotifier not initialized");

  return emptyStr;
}

std::string Serializer::quote(const std::string& s, bool quote) {
  std::stringstream ss;

  if(quote)
    ss << "\"" << s << "\"";
  else
    ss << s;

  return ss.str();
}

void Serializer::next() {
  out() << ",";
  comma = false;
}

void Serializer::endl() {
  out() << "\n" << space(tab);
}

void Serializer::mapStart(const std::string& label) {
  if(comma)
    next();
  if(label.size())
    endl();
  if(label.size())
    out() << quote(label) << ": ";
  out() << "{";
  tab = indent1(tab);
  comma = false;
}

void Serializer::mapEnd(const std::string&) {
  tab = unindent(tab);
  endl();
  out() << "}";
  comma = true;
}

void Serializer::arrStart(const std::string& label) {
  if(comma)
    next();
  if(label.size())
    endl();
  if(label.size())
    out() << quote(label) << ": ";
  out() << "[";
  tab = indent1(tab);
  comma = false;
}

void Serializer::arrEnd(const std::string&) {
  tab = unindent(tab);
  endl();
  out() << "]";
  comma = true;
}

void Serializer::pairStart(const std::string& label) {
  if(comma)
    next();
  if(label.size())
    endl();
  if(label.size())
    out() << quote(label) << ": ";
  out() << "<";
  tab = indent1(tab);
  comma = false;
}

void Serializer::pairEnd(const std::string&) {
  tab = unindent(tab);
  endl();
  out() << ">";
  comma = true;
}

template <>
void Serializer::write(const long double& g) {
  std::stringstream ss;
  ss << g;
  out() << ss.str();
}

template void Serializer::write(const int8_t&);
template void Serializer::write(const int16_t&);
template void Serializer::write(const int32_t&);
template void Serializer::write(const int64_t&);
template void Serializer::write(const uint8_t&);
template void Serializer::write(const uint16_t&);
template void Serializer::write(const uint32_t&);
template void Serializer::write(const uint64_t&);
template void Serializer::write(const float&);
template void Serializer::write(const double&);

} // namespace moya
