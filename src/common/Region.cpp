#include "Region.h"
#include "Config.h"
#include "PayloadBase.h"

#include <llvm/Support/raw_ostream.h>

#include <sstream>

namespace moya {

Region::Region()
    : id(moya::Config::getInvalidRegionID()), name("<null>"),
      lang(SourceLang::UnknownLang) {
  ;
}

Region::Region(RegionID id,
               const std::string& name,
               const Location& loc,
               const Location& beginLoc,
               const Location& endLoc,
               SourceLang lang)
    : id(id), name(name), loc(loc), beginLoc(beginLoc), endLoc(endLoc),
      lang(lang) {
  ;
}

RegionID Region::getID() const {
  return id;
}

const std::string& Region::getName() const {
  return name;
}

const Location& Region::getLocation() const {
  return loc;
}

const Location& Region::getBeginLoc() const {
  return beginLoc;
}

const Location& Region::getEndLoc() const {
  return endLoc;
}

SourceLang Region::getSourceLang() const {
  return lang;
}

std::string Region::str() const {
  std::stringstream ss;

  ss << getName() << " (" << getID() << ") @ " << getLocation().str();

  return ss.str();
}

Serializer& Region::serialize(Serializer& s) const {
  s.mapStart();
  s.add("id", getID());
  s.add("name", getName());
  if(getLocation().isValid())
    s.add("location", getLocation());
  if(getBeginLoc().isValid())
    s.add("begin", getBeginLoc());
  if(getEndLoc().isValid())
    s.add("end", getEndLoc());
  s.add("lang", lang);
  s.mapEnd();

  return s;
}

void Region::pickle(PayloadBase& p) const {
  p.pickle(getID());
  p.pickle(getName());
  p.pickle(getLocation());
  p.pickle(getBeginLoc());
  p.pickle(getEndLoc());
  p.pickle(getSourceLang());
}

Region& Region::unpickle(PayloadBase& p) {
  id = p.unpickle<RegionID>();
  name = p.unpickle<std::string>();
  loc = p.unpickle<Location>();
  beginLoc = p.unpickle<Location>();
  endLoc = p.unpickle<Location>();
  lang = p.unpickle<SourceLang>();

  return *this;
}

} // namespace moya
