#include "ContentNullptr.h"

#include <sstream>

using namespace llvm;

namespace moya {

ContentNullptr::ContentNullptr(ConstantPointerNull* cnull)
    : Content(Content::Kind::Nullptr), cnull(cnull) {
  ;
}

ConstantPointerNull* ContentNullptr::getNullptr() const {
  return cnull;
}

bool ContentNullptr::classof(const Content* c) {
  return c->getKind() == Content::Kind::Nullptr;
}

std::string ContentNullptr::str() const {
  std::stringstream ss;
  ss << "CNullptr()";
  return ss.str();
}

void ContentNullptr::serialize(Serializer& s) const {
  s.mapStart();
  s.add("_class", "Pointer");
  s.add("target", 0);
  s.mapEnd();
}

} // namespace moya
