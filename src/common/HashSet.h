#ifndef MOYA_COMMON_HASHSET_H
#define MOYA_COMMON_HASHSET_H

#include <sstream>
#include <string>
#include <unordered_set>

#include "Deserializer.h"
#include "Serializer.h"
#include "Stringify.h"

namespace moya {

template <typename T>
class HashSet {
protected:
  std::unordered_set<T> _impl;

public:
  using key_type = typename std::unordered_set<T>::key_type;
  using value_type = typename std::unordered_set<T>::value_type;
  using size_type = typename std::unordered_set<T>::size_type;
  using reference = typename std::unordered_set<T>::reference;
  using const_reference = typename std::unordered_set<T>::const_reference;
  using pointer = typename std::unordered_set<T>::pointer;
  using const_pointer = typename std::unordered_set<T>::const_pointer;
  using iterator = typename std::unordered_set<T>::iterator;
  using const_iterator = typename std::unordered_set<T>::const_iterator;

protected:
  std::unordered_set<T>& getImpl() {
    return _impl;
  }

  const std::unordered_set<T>& getImpl() const {
    return _impl;
  }

public:
  HashSet() {
    ;
  }

  explicit HashSet(const T& value) : _impl(value) {
    ;
  }

  template <typename InputIt>
  HashSet(InputIt first, InputIt last) : _impl(first, last) {
    ;
  }

  HashSet(const HashSet& other) : _impl(other.getImpl()) {
    ;
  }

  HashSet(HashSet&& other) : _impl(other.getImpl()) {
    ;
  }

  HashSet(std::initializer_list<T> init) : _impl(init) {
    ;
  }

  virtual ~HashSet() = default;

  HashSet<T>& operator=(const HashSet& other) {
    _impl = other.getImpl();
    return *this;
  }

  HashSet<T>& operator=(HashSet&& other) {
    _impl = other.getImpl();
    return *this;
  }

  HashSet<T>& operator=(std::initializer_list<T> other) {
    _impl = other;
    return *this;
  }

  iterator begin() {
    return _impl.begin();
  }

  const_iterator begin() const {
    return _impl.begin();
  }

  iterator end() {
    return _impl.end();
  }

  const_iterator end() const {
    return _impl.end();
  }

  bool empty() const {
    return _impl.empty();
  }

  size_type size() const {
    return _impl.size();
  }

  void clear() {
    return _impl.clear();
  }

  template <typename... Args>
  bool emplace(Args&&... args) {
    return _impl.emplace(args...).second;
  }

  bool insert(const T& value) {
    return _impl.insert(value).second;
  }

  template <typename InputIt>
  void insert(InputIt first, InputIt last) {
    _impl.insert(first, last);
  }

  bool insert(const HashSet<T>& other) {
    bool changed = false;

    for(const T& e : other)
      changed |= _impl.insert(e).second;

    return changed;
  }

  iterator erase(const_iterator pos) {
    return _impl.erase(pos);
  }

  iterator erase(const_iterator first, const_iterator last) {
    return _impl.erase(first, last);
  }

  size_type erase(const T& key) {
    return _impl.erase(key);
  }

  bool contains(const T& e) const {
    return (_impl.find(e) != _impl.end());
  }

  bool equals(const HashSet<T>& other) const {
    if(this->size() != other.size())
      return false;
    for(auto e : other)
      if(not contains(e))
        return false;
    return true;
  }

  bool disjoint(const HashSet<T>& other) const {
    for(const T& e : other)
      if(contains(e))
        return false;
    return true;
  }

  bool operator==(const HashSet<T>& c2) const {
    return _impl == c2.getImpl();
  }

  bool operator!=(const HashSet<T>& c2) const {
    return _impl != c2.getImpl();
  }

  bool operator<(const HashSet<T>& c2) const {
    return _impl < c2.getImpl();
  }

  bool operator<=(const HashSet<T>& c2) const {
    return _impl <= c2.getImpl();
  }

  bool operator>(const HashSet<T>& c2) const {
    return _impl > c2.getImpl();
  }

  bool operator>=(const HashSet<T>& c2) const {
    return _impl >= c2.getImpl();
  }

  std::string str() const {
    bool comma = false;
    std::stringstream ss;

    ss << "{";
    for(const auto& e : _impl) {
      if(comma)
        ss << ", ";
      ss << moya::str(e);
      comma = true;
    }
    ss << "}";

    return ss.str();
  }

  template <typename... ArgsT>
  void serialize(Serializer& s, ArgsT&&... args) const {
    s.arrStart();
    for(const auto& e : _impl)
      s.serialize(e, args...);
    s.arrEnd();
  }

  void deserialize(Deserializer& d) {
    d.arrStart();
    while(not d.arrEmpty()) {
      T t;
      d.deserialize(t);
      _impl.emplace(std::move(t));
    }
    d.arrEnd();
  }
};

} // namespace moya

#endif // MOYA_COMMON_HASHSET_H
