#ifndef MOYA_COMMON_SOURCE_LANG_H
#define MOYA_COMMON_SOURCE_LANG_H

#include <sstream>
#include <string>

enum SourceLang {
  // All C dialects
  C,

  // All C++ dialects
  CXX,

  // All dialects of Fortran except Fortran 77
  Fort,

  // Fortran77
  F77,

  // Objective-C
  ObjC,

  // Objective-C++
  ObjCXX,

  // Cuda
  Cuda,

  UnknownLang
};

SourceLang parseSourceLang(const std::string&);
void operator>>(std::stringstream&, SourceLang&);

bool isFortran(SourceLang);
bool isC(SourceLang);
bool isCXX(SourceLang);
bool isCuda(SourceLang);

#endif // MOYA_COMMON_SOURCE_LANG_H
