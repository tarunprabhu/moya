#ifndef MOYA_COMMON_VALUE_UTILS_H
#define MOYA_COMMON_VALUE_UTILS_H

#include <llvm/IR/Value.h>

namespace moya {

namespace LLVMUtils {

llvm::Module* getModule(llvm::Value*);
const llvm::Module* getModule(const llvm::Value*);

llvm::Function* getFunction(llvm::Value*);
const llvm::Function* getFunction(const llvm::Value*);

bool hasUsesInFunction(const llvm::Value*, const llvm::Function*);
bool hasUsesInFunction(const llvm::Value*, const llvm::Function&);
bool hasUsesInFunction(const llvm::Value&, const llvm::Function*);
bool hasUsesInFunction(const llvm::Value&, const llvm::Function&);

llvm::Type* getAnalysisType(const llvm::Value*);
llvm::Type* getAnalysisType(const llvm::Value&);

template <typename T>
T* getAnalysisTypeAs(const llvm::Value* v);
template <typename T>
T* getAnalysisTypeAs(const llvm::Value& v);

} // namespace LLVMUtils

} // namespace moya

#endif // MOYA_COMMON_VALUE_UTILS_H
