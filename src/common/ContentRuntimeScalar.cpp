#include "ContentRuntimeScalar.h"

#include <sstream>

using namespace llvm;

namespace moya {

ContentRuntimeScalar::ContentRuntimeScalar()
    : Content(Content::Kind::RuntimeScalar) {
  ;
}

bool ContentRuntimeScalar::classof(const Content* c) {
  return c->getKind() == Content::Kind::RuntimeScalar;
}

std::string ContentRuntimeScalar::str() const {
  std::stringstream ss;
  ss << "CRuntimeScalar()";
  return ss.str();
}

void ContentRuntimeScalar::serialize(Serializer& s) const {
  s.mapStart();
  s.add("_class", "Scalar");
  s.mapEnd();
}

} // namespace moya
