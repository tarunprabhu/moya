#ifndef MOYA_COMMON_DEMANGLER_F77_H
#define MOYA_COMMON_DEMANGLER_F77_H

#include "Demangler.h"

namespace moya {

class DemanglerF77 : public Demangler {
protected:
  std::string stripTrailingUnderscore(const std::string&) const;

public:
  DemanglerF77();
  virtual ~DemanglerF77() = default;

  virtual std::string demangle(const llvm::Function&,
                               Demangler::Action) const override;
  virtual std::string demangle(const llvm::GlobalVariable&,
                               Demangler::Action) const override;
};

} // namespace moya

#endif // MOYA_COMMON_DEMANGLER_F77_H
