#include "Graph.h"
#include "Stringify.h"

using namespace llvm;

namespace moya {

template <typename T>
Graph<T>::Graph() {
  ;
}

template <typename T>
void Graph<T>::addNode(T data) {
  if(not nodes.contains(data)) {
    nodes[data].reset(new GraphNode<T>(data));
    edges[data];
  }
}

template <typename T>
void Graph<T>::addEdge(T src, T dst) {
  addNode(src);
  addNode(dst);

  edges.at(src).insert(dst);
}

template <typename T>
void Graph<T>::getSCC(T v, int& index, moya::Stack<T>& wl, Components& sccs) {
  auto* vn = nodes.at(v).get();
  vn->index = index;
  vn->lowlink = index;
  index += 1;
  wl.push(v);
  vn->inwl = true;

  for(T w : edges.at(v)) {
    auto* wn = nodes.at(w).get();
    if(wn->index == -1) {
      getSCC(w, index, wl, sccs);
      vn->lowlink = std::min(vn->lowlink, wn->lowlink);
    } else if(wn->inwl) {
      vn->lowlink = std::min(vn->lowlink, wn->index);
    }
  }

  if(vn->lowlink == vn->index) {
    Component scc;
    T w;
    do {
      w = wl.pop();
      nodes.at(w)->inwl = false;
      scc.insert(w);
    } while(w != v);
    sccs.push_back(scc);
  }
}

template <typename T>
typename Graph<T>::Components Graph<T>::getSCCs() {
  int index = 0;
  moya::Stack<T> wl;
  Components sccs;
  for(const auto& it : nodes)
    if(it.second->index == -1)
      getSCC(it.first, index, wl, sccs);
  return sccs;
}

template <typename T>
std::string Graph<T>::str() const {
  std::stringstream ss;

  for(const auto& it : nodes) {
    ss << moya::str(*it.first) << ": { ";
    for(const auto& jt : edges.at(it.first))
      ss << moya::str(*jt) << " ";
    ss << "}\n\n";
  }

  return ss.str();
}

template class Graph<StructType*>;

} // namespace moya
