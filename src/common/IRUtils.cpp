#include "Diagnostics.h"
#include "LLVMUtils.h"

#include <llvm/IRReader/IRReader.h>
#include <llvm/Support/SourceMgr.h>

using namespace llvm;

namespace moya {

namespace LLVMUtils {

std::unique_ptr<Module> readModuleFromFile(const std::string& filename,
                                           LLVMContext& ctxt) {
  SMDiagnostic error;
  std::unique_ptr<Module> module(parseIRFile(filename, error, ctxt));
  if(not module.get())
    moya_error("getModule: could not parse IR file");
  return module;
}

} // namespace LLVMUtils

} // namespace moya
