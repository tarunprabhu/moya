#include "Diagnostics.h"
#include "LLVMUtils.h"
#include "PathUtils.h"

using namespace llvm;

namespace moya {

namespace LLVMUtils {

Location getLocation(const DebugLoc& loc) {
  if(loc)
    return getLocation(loc.get());
  return Location();
}

Location getLocation(const DILocation* di) {
  if(di) {
    if(DIScope* scope = di->getScope()) {
      std::string path = scope->getFilename();
      if(not PathUtils::isAbsolute(path))
        path = PathUtils::construct(scope->getDirectory(), path);
      return Location(path, di->getLine(), di->getColumn());
    }
  }
  return Location();
}

Location getLocation(const DISubprogram* di) {
  if(di) {
    std::string path = di->getFilename();
    if(not PathUtils::isAbsolute(path))
      path = PathUtils::construct(di->getDirectory(), path);
    return Location(path, di->getLine());
  }
  return Location();
}

static Type* getBasicType(const DIBasicType* di, LLVMContext& context) {
  unsigned bits = di->getSizeInBits();
  switch(di->getEncoding()) {
  case dwarf::DW_ATE_signed:
  case dwarf::DW_ATE_unsigned:
  case dwarf::DW_ATE_boolean:
  case dwarf::DW_ATE_signed_char:
  case dwarf::DW_ATE_unsigned_char:
    switch(bits) {
    case 8:
      return Type::getInt8Ty(context);
    case 16:
      return Type::getInt16Ty(context);
    case 32:
      return Type::getInt32Ty(context);
    case 64:
      return Type::getInt64Ty(context);
    default:
      moya_error("Unsupported integer size: " << bits);
    }
  case dwarf::DW_ATE_float:
    switch(bits) {
    case 32:
      return Type::getFloatTy(context);
    case 64:
      return Type::getDoubleTy(context);
    default:
      moya_error("Unsupported floating point size: " << bits);
    }
  case dwarf::DW_ATE_complex_float:
    switch(bits) {
    case 64:
      return StructType::get(Type::getFloatTy(context),
                             Type::getFloatTy(context));
    case 128:
      return StructType::get(Type::getDoubleTy(context),
                             Type::getDoubleTy(context));
    default:
      moya_error("Unsupported complex size: " << bits / 2);
    }
  default:
    moya_error("Unsupported basic type: " << di->getName());
  }

  return nullptr;
}

static Type* getDerivedType(const DIDerivedType* di, Module& module) {
  switch(di->getTag()) {
  case dwarf::DW_TAG_pointer_type:
    return getType(di->getBaseType().resolve(), module)->getPointerTo();
  case dwarf::DW_TAG_member:
    return getType(di->getBaseType().resolve(), module);
  default:
    moya_error("Unsupported derived type: " << di->getTag());
  }

  return nullptr;
}

static Type* getCompositeType(const DICompositeType* di, Module& module) {
  switch(di->getTag()) {
  case dwarf::DW_TAG_array_type: {
    const DataLayout& dl = module.getDataLayout();
    Type* ety = getType(di->getBaseType().resolve(), module);
    size_t size = di->getSizeInBits();
    size_t elemSize = dl.getTypeSizeInBits(ety);
    if(size % elemSize)
      moya_error("Array size is not a multiple of element size: "
                 << size << ", " << elemSize);
    size_t count = size / elemSize;
    if(count <= 1)
      return ety;
    else
      return ArrayType::get(ety, count);
  }
  case dwarf::DW_TAG_structure_type: {
    moya::Vector<Type*> elements;
    for(const MDOperand& md :
        dyn_cast<MDNode>(di->getElements().get())->operands())
      if(auto* diType = dyn_cast<DIType>(md))
        elements.push_back(getType(diType, module));
    return StructType::create(makeArrayRef(elements), di->getName());
  }
  default:
    moya_error("Unsupported composite type tag: " << di->getTag());
  }

  return nullptr;
}

Type* getType(const DIType* di, Module& module) {
  if(const auto* diBasic = dyn_cast<DIBasicType>(di))
    return getBasicType(diBasic, module.getContext());
  else if(const auto* diDerived = dyn_cast<DIDerivedType>(di))
    return getDerivedType(diDerived, module);
  else if(const auto* diComposite = dyn_cast<DICompositeType>(di))
    return getCompositeType(diComposite, module);
  else
    moya_error("Unsupported debug info type: " << di->getTag());

  return nullptr;
}

} // namespace LLVMUtils

} // namespace moya
