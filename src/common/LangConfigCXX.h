#ifndef MOYA_COMMON_LANG_CONFIG_CXX_H
#define MOYA_COMMON_LANG_CONFIG_CXX_H

#include "Vector.h"

#include <string>

// This is a configuration class for C++-specific features.
class LangConfigCXX {
public:
  // The comparison operators in C++. These are there to implement the
  // operator overloads for the STL containers
  static const moya::Vector<std::string>& getComparisonOperators();

  static bool isValidSourceFile(const std::string&);
};

#endif // MOYA_COMMON_LANG_CONFIG_CXX_H
