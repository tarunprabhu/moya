#ifndef MOYA_COMMON_PARSE_H
#define MOYA_COMMON_PARSE_H

#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>

#include <sstream>
#include <stdint.h>
#include <string>

namespace moya {

llvm::Type* parse(const std::string& s, llvm::Module&);

// For primitive types (integers, floats, etc.)
template <typename T,
          typename std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
T parse(const std::string& s) {
  T t;
  std::stringstream ss(s);
  ss >> t;
  return t;
}

// For enums. We expect that a separate parse() function is defined to deal
// with the enum
template <typename T,
          typename std::enable_if_t<std::is_enum<T>::value, int> = 0>
T parse(const std::string& s);

std::string parse(const std::string& s);

} // namespace moya

#endif // MOYA_COMMON_PARSE_H
