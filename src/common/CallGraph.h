#ifndef MOYA_COMMON_CALLGRAPH_H
#define MOYA_COMMON_CALLGRAPH_H

#include "APITypes.h"
#include "HashMap.h"
#include "HashSet.h"
#include "Queue.h"
#include "Region.h"
#include "Set.h"
#include "Vector.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Module.h>

namespace moya {

class Dotifier;
class Environment;
class Serializer;
class Store;

// A callgraph may be specific to a particular region, but it doens't have
// to be. If it is not specific to a region, then the querying functions
// are all invalid
class CallGraph {
private:
  using Callees = HashSet<const llvm::Function*>;
  using CallGraphT = HashMap<const llvm::Function*, Callees>;

private:
  const llvm::Module& module;

  // If this callgraph represents the whole program, this->region will be null
  const Region* region;

  // The root will be the main() function (or equivalent in other languages)
  // when this callgraph is for the whole program (when this->region is null)
  // Otherwise, it wil be null
  //
  // FIXME: Would be nice to have assertion here because exactly one of
  // this->region and this->root should be null
  const llvm::Function* root;

  // If this callgraph is for a region, these are the instructions in the
  // region
  Set<const llvm::Instruction*> rootInsts;

  CallGraphT cg;
  CallGraphT closure;

private:
  std::string getRegionName() const;
  std::string getFunctionName(const llvm::Function*) const;
  void getRegionBoundaryArgs(const llvm::Value*,
                             Vector<const llvm::Use*>&) const;
  Vector<const llvm::Use*> getRegionBoundaryArgs(const llvm::Function*) const;
  const llvm::Instruction* getRegionBoundaryCall(const llvm::Function*) const;
  const llvm::Instruction* getEnterRegionCall() const;
  const llvm::Instruction* getExitRegionCall() const;
  llvm::const_inst_iterator getIterator(const llvm::Instruction*);
  Vector<const llvm::Instruction*> getReachableCalls(llvm::const_inst_iterator,
                                                     llvm::const_inst_iterator);
  void processInstruction(const llvm::Instruction*,
                          const llvm::Function*,
                          const Environment&,
                          Queue<const llvm::Function*>&);
  void addReachableCall(const llvm::Instruction*,
                        Queue<const llvm::BasicBlock*>&,
                        Set<const llvm::BasicBlock*>&,
                        Vector<const llvm::Instruction*>&);

  std::string str(const CallGraphT&, bool) const;
  Dotifier& dot(const CallGraphT&, Dotifier&) const;

public:
  CallGraph(const llvm::Module&);
  CallGraph(const Region&, const llvm::Module&);
  virtual ~CallGraph() = default;

  bool isInProperClosureOf(const llvm::Function*, const llvm::Function*) const;
  bool isInClosureOf(const llvm::Function*, const llvm::Function*) const;
  bool isInProperClosureOfRoot(const llvm::Function*) const;
  bool isInClosureOfRoot(const llvm::Function*) const;
  bool isInProperClosureOfRoot(const llvm::Instruction*) const;
  bool isInClosureOfRoot(const llvm::Instruction*) const;

  bool isConstructed() const;
  bool canConstruct() const;
  CallGraph& construct(const Environment&, const Store&);

  // If the second argument is true, prints the callgraph, else prints the
  // closure
  std::string strCallGraph(bool = true) const;
  std::string strClosure(bool = true) const;

  Dotifier& dot(const std::string&, Dotifier&) const;
  void serialize(Serializer&) const;
};

} // namespace moya

#endif // MOYA_COMMON_CALLGRAPH_H
