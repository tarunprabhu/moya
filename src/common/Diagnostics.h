#ifndef MOYA_COMMON_DIAGNOSTICS_H
#define MOYA_COMMON_DIAGNOSTICS_H

// This macro raises a segmentation fault because, for some reason, I don't
// want to use C++ exceptions which would be the more sensible thing to do
#define SEGFAULT() (*((volatile int*)0x0))

#ifdef __cplusplus

#include <llvm/Support/raw_ostream.h>

namespace llvm {
class Function;
class GlobalVariable;
class Instruction;
} // namespace llvm

namespace moya {

std::string getBacktrace();
std::string getDiagnostic(const llvm::Function*);
std::string getDiagnostic(const llvm::Function&);
std::string getDiagnostic(const llvm::GlobalVariable&);
std::string getDiagnostic(const llvm::GlobalVariable*);
std::string getDiagnostic(const llvm::Instruction*);

int isVerboseEnabled();

} // namespace moya

// FIXME: It would be good if these macros became proper functions

// We raise a segfault on the errors so we have a stacktrace to use with GDB
#define moya_error(s)                                               \
  do {                                                              \
    llvm::errs() << "\n"                                            \
                 << " *** ERROR: " << s << "\n"                     \
                 << " in " << __FILE__ << ":" << __LINE__ << "\n\n" \
                 << moya::getBacktrace();                           \
    SEGFAULT();                                                     \
    exit(1);                                                        \
  } while(0)

#define moya_error_if_not(cond, s) \
  do {                             \
    if(not(cond)) {                \
      moya_error(s);               \
    }                              \
  } while(0)

#define moya_error_if(cond, s) \
  do {                         \
    if((cond)) {               \
      moya_error(s);           \
    }                          \
  } while(0)

#define moya_warning(s)                                             \
  do {                                                              \
    llvm::errs() << " *** WARNING (" << __FILE__ << ":" << __LINE__ \
                 << "): " << s << "\n";                             \
  } while(0)

#define moya_message(s)                         \
  do {                                          \
    if(moya::isVerboseEnabled())                \
      llvm::errs() << "MESSAGE: " << s << "\n"; \
  } while(0)

#define moya_debug(s)                                                      \
  do {                                                                     \
    llvm::errs() << "DEBUG (" << __FILE__ << ":" << __LINE__ << "): " << s \
                 << "\n";                                                  \
  } while(0)

#else // !__cplusplus

#include <stdarg.h>
#include <stdio.h>

#define moya_error(...)                                    \
  do {                                                     \
    fprintf(stderr, " *** ERROR: ");                       \
    fprintf(stderr, __VA_ARGS__);                          \
    fprintf(stderr, " -- in %s:%d\n", __FILE__, __LINE__); \
    SEGFAULT();                                            \
    exit(1);                                               \
  } while(0)

#define moya_error_if_not(cond, ...) \
  do {                               \
    if(not(cond)) {                  \
      moya_error(__VA_ARGS__);       \
    }                                \
  } while(0)

#define moya_error_if(cond, ...) \
  do {                           \
    if((cond)) {                 \
      moya_error(__VA_ARGS__);   \
    }                            \
  } while(0)

#define moya_warning(...)                                  \
  do {                                                     \
    fprintf(stderr, " *** ERROR: ");                       \
    fprintf(stderr, __VA_ARGS__);                          \
    fprintf(stderr, " -- in %s:%d\n", __FILE__, __LINE__); \
  } while(0)

#define moya_message(...)           \
  do {                              \
    if(isVerboseEnabled()) {        \
      fprintf(stderr, "MESSAGE: "); \
      fprintf(stderr, __VA_ARGS__); \
      fprintf(stderr, "\n");        \
    }                               \
  } while(0);

#define moya_debug(...)           \
  do {                            \
    fprintf(stderr, "DEBUG: ");   \
    fprintf(stderr, __VA_ARGS__); \
    fprintf(stderr, "\n");        \
  } while(0);

#endif // __cplusplus

#endif // MOYA_COMMON_DIAGNOSTICS_H
