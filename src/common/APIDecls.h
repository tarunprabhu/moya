#ifndef MOYA_COMMON_MOYA_API_DECLS_H
#define MOYA_COMMON_MOYA_API_DECLS_H

#include "APITypes.h"

// Enum for all the Moya runtime functions
enum MoyaAPIFunctions {
  moya_EnterRegion,
  moya_ExitRegion,
  moya_IsInRegion,
  moya_GetCompiled,
  moya_BeginCall,
  moya_EndCall,
  moya_RegisterArgI1,
  moya_RegisterArgI8,
  moya_RegisterArgI16,
  moya_RegisterArgI32,
  moya_RegisterArgI64,
  moya_RegisterArgF32,
  moya_RegisterArgF64,
  moya_RegisterArgF80,
  moya_RegisterArgPtr,
  moya_ExecuteCuda,
};

#define MOYA_QUOTE_IMPL(STR) #STR
#define MOYA_QUOTE(STR) MOYA_QUOTE_IMPL(STR)

#define MOYA_FN_IMPL_(PREF, FN, SUFF) PREF##FN##SUFF
#define MOYA_FN_IMPL(PREF, FN, SUFF) MOYA_FN_IMPL_(PREF, FN, SUFF)

#define MOYA_PREFIX __moya_

#define MOYA_FN(FN, SUFFIX) MOYA_FN_IMPL(MOYA_PREFIX, FN, SUFFIX)

#define MOYA_FN_REDIRECTER_BASE MOYA_FN(redirecter, _)

#define MOYA_FN_ENTER_REGION MOYA_FN(enter_region, )
#define MOYA_FN_EXIT_REGION MOYA_FN(exit_region, )
#define MOYA_FN_IS_IN_REGION MOYA_FN(is_in_region, )
#define MOYA_FN_IS_COMPILED MOYA_FN(is_compiled, )
#define MOYA_FN_GET_COMPILED MOYA_FN(get_compiled, )
#define MOYA_FN_COMPILE MOYA_FN(compile, )
#define MOYA_FN_EXECUTE_CUDA MOYA_FN(execute_cuda, )
#define MOYA_FN_BEGIN_CALL MOYA_FN(begin_call, )
#define MOYA_FN_REGISTER_ARG(TYPE) MOYA_FN(register_arg_, TYPE)

// #define MOYA_FN_STATS_START_CALL MOYA_FN(stats_start_, call)
// #define MOYA_FN_STATS_STOP_CALL MOYA_FN(stats_stop_, call)
// #define MOYA_FN_STARTS_START_COMPILE MOYA_FN(stats_start_, compile)
// #define MOYA_FN_STATS_STOP_COMPILE MOYA_FN(stats_stop_, compile)
// #define MOYA_FN_STATS_START_OPTIMIZE MOYA_FN(stats_start_, optimize)
// #define MOYA_FN_STATS_STOP_OPTIMIZE MOYA_FN(stats_stop_, optimize)
// #define MOYA_FN_STATS_START_CODEGEN MOYA_FN(stats_start_, codegen)
// #define MOYA_FN_STATS_STOP_CODEGEN MOYA_FN(stats_stop_, codegen)
#define MOYA_FN_STATS_START_EXECUTE MOYA_FN(stats_start_, execute)
#define MOYA_FN_STATS_STOP_EXECUTE MOYA_FN(stats_stop_, execute)

#define MOYA_FN_TOP_ERROR MOYA_FN(top_error, )

#define MOYA_FN_SENTINEL_START MOYA_FN(sentinel_, start)
#define MOYA_FN_SENTINEL_STOP MOYA_FN(sentinel_, stop)

#ifdef __cplusplus
namespace moya {

extern "C" {
#endif // __cplusplus

// Commonly used runtime functions. These do the bulk of the work of redirecting
// and compiling code
void MOYA_FN_ENTER_REGION(RegionID);
void MOYA_FN_EXIT_REGION(RegionID);
int MOYA_FN_IS_IN_REGION(void);
void MOYA_FN_COMPILE(JITID);
void MOYA_FN_EXECUTE_CUDA(JITID,
                          FunctionSignatureBasic,
                          unsigned,
                          unsigned,
                          unsigned,
                          unsigned,
                          unsigned,
                          unsigned,
                          unsigned,
                          CUstream,
                          void**);

// This is called before the first argument is registered
// There is no corresponding end_call exposed because the runtime handles it
// internally
void MOYA_FN_BEGIN_CALL(JITID);

// Argument registration functions. These are called
// whenever a function to be JIT'ed is called
void MOYA_FN_REGISTER_ARG(i1)(JITID, unsigned, const bool, bool);
void MOYA_FN_REGISTER_ARG(i8)(JITID, unsigned, const int8_t, bool);
void MOYA_FN_REGISTER_ARG(i16)(JITID, unsigned, const int16_t, bool);
void MOYA_FN_REGISTER_ARG(i32)(JITID, unsigned, const int32_t, bool);
void MOYA_FN_REGISTER_ARG(i64)(JITID, unsigned, const int64_t, bool);
void MOYA_FN_REGISTER_ARG(f32)(JITID, unsigned, const float, bool);
void MOYA_FN_REGISTER_ARG(f64)(JITID, unsigned, const double, bool);
void MOYA_FN_REGISTER_ARG(f80)(JITID, unsigned, const long double, bool);
void MOYA_FN_REGISTER_ARG(ptr)(JITID, unsigned, const void*, bool);
void MOYA_FN_REGISTER_ARG(pi1)(JITID, unsigned, const bool*, bool);
void MOYA_FN_REGISTER_ARG(pi8)(JITID, unsigned, const int8_t*, bool);
void MOYA_FN_REGISTER_ARG(pi16)(JITID, unsigned, const int16_t*, bool);
void MOYA_FN_REGISTER_ARG(pi32)(JITID, unsigned, const int32_t*, bool);
void MOYA_FN_REGISTER_ARG(pi64)(JITID, unsigned, const int64_t*, bool);
void MOYA_FN_REGISTER_ARG(pf32)(JITID, unsigned, const float*, bool);
void MOYA_FN_REGISTER_ARG(pf64)(JITID, unsigned, const double*, bool);
void MOYA_FN_REGISTER_ARG(pf80)(JITID, unsigned, const long double*, bool);
void MOYA_FN_REGISTER_ARG(pptr)(JITID, unsigned, const void**, bool);
void MOYA_FN_REGISTER_ARG(ai1)(JITID, unsigned, const bool*, int32_t, bool);
void MOYA_FN_REGISTER_ARG(ai8)(JITID, unsigned, const int8_t*, int32_t, bool);
void MOYA_FN_REGISTER_ARG(a16)(JITID, unsigned, const int16_t*, int32_t, bool);
void MOYA_FN_REGISTER_ARG(ai32)(JITID, unsigned, const int32_t*, int32_t, bool);
void MOYA_FN_REGISTER_ARG(ai64)(JITID, unsigned, const int64_t*, int32_t, bool);
void MOYA_FN_REGISTER_ARG(af32)(JITID, unsigned, const float*, int32_t, bool);
void MOYA_FN_REGISTER_ARG(af64)(JITID, unsigned, const double*, int32_t, bool);
void MOYA_FN_REGISTER_ARG(
    af80)(JITID, unsigned, const long double*, int32_t, bool);

// Other runtime functions, for book-keeping, overheads and the like
void MOYA_FN_STATS_START_CALL(JITID);
void MOYA_FN_STATS_STOP_CALL(JITID);
void MOYA_FN_STATS_START_COMPILE(JITID, FunctionSignatureBasic);
void MOYA_FN_STATS_STOP_COMPILE(JITID, FunctionSignatureBasic);
void MOYA_FN_STATS_START_EXECUTE(JITID,
                                 FunctionSignatureBasic,
                                 FunctionSignatureTuning);
void MOYA_FN_STATS_STOP_EXECUTE(JITID,
                                FunctionSignatureBasic,
                                FunctionSignatureTuning);

// Other sentinels function which are only used during the analysis and
// should be stripped during before codegen
void MOYA_FN_SENTINEL_START(MoyaID);
void MOYA_FN_SENTINEL_STOP(MoyaID);

#ifdef __cplusplus
} // namespace moya

} // extern "C"
#endif // __cplusplus

#endif // MOYA_COMMON_MOYA_API_DECLS_H
