#ifndef MOYA_COMMON_TYPE_UTILS_H
#define MOYA_COMMON_TYPE_UTILS_H

// #include "Metadata.h"
#include "Set.h"
#include "Vector.h"

#include <llvm/IR/Metadata.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/Value.h>

namespace moya {

namespace LLVMUtils {

// Returns the innermost non-sequential (array/pointer/vector) type within
// the given type
llvm::Type* getInnermost(llvm::Type*);
llvm::Type* getInnermost(const llvm::Value*);
llvm::Type* getInnermost(const llvm::Metadata*);

// Returns the depth of an LLVM type - the depth is the number of pointer or
// array types that wrap a primitive or struct type
unsigned getTypeDepth(const llvm::Type*, unsigned = 0);

// Returns the maximum depth of an LLVM type. Looks into the first element
// of a struct and goes down through that as well because it could be in
// the same address
unsigned getMaxTypeDepth(const llvm::Type*, unsigned = 0);

// Check if the type is a complex type
bool isComplexTy(const llvm::Type*);

bool isVtableTy(const llvm::Type*);

// Check if the type is a floating-point type
bool isFPTy(const llvm::Type*);
bool isIntTy(const llvm::Type*);
bool isScalarTy(const llvm::Type*);
bool isPointerTy(const llvm::Type*);
bool isArrayTy(const llvm::Type*);
bool isFunctionTy(const llvm::Type*);
bool isStructTy(const llvm::Type*);
bool isVoidTy(const llvm::Type*);

bool isPtrToScalarTy(const llvm::Type*);
bool isPtrToIntTy(const llvm::Type*);
bool isPtrToFPTy(const llvm::Type*);
bool isPtrToInt8Ty(const llvm::Type*);
bool isPtrToInt16Ty(const llvm::Type*);
bool isPtrToInt32Ty(const llvm::Type*);
bool isPtrToInt64Ty(const llvm::Type*);
bool isPtrToFloatTy(const llvm::Type*);
bool isPtrToDoubleTy(const llvm::Type*);
bool isPtrToLongDoubleTy(const llvm::Type*);
bool isPtrToPtrTy(const llvm::Type*);
bool isPtrToStructTy(const llvm::Type*);
bool isPtrToFunctionTy(const llvm::Type*);
bool isPtrToVtableTy(const llvm::Type*);

// Check if the struct is a literal. We cannot use the llvm::StructType method
// because in one of the pre-Aeryn passes, we name all the structs in the module
bool isLiteralStructTy(const llvm::Type*);

// Check if the struct consists only of scalars. This is typically used in
// cases where complex numbers are read from dynamically allocated arrays of
// primitive types
bool isScalarStructTy(const llvm::Type*);

// Check if the array element is a scalar type
bool isScalarArrayTy(const llvm::Type*);

// Check if the type is a 0-length array
bool is0ArrayTy(const llvm::Type*);

// Gets an appropriately sized integer type
llvm::IntegerType* getIntegerType(llvm::LLVMContext&, size_t);

std::string getAnonymousStructName(const llvm::StructType*);

// Get all the types that can be loaded from the current type. This
// essentially returns all the types that are correctly aligned and begin
// at the same offset as this type
moya::Set<llvm::Type*> getLoadableTypes(llvm::Type*);

// Returns the depth of pointers in the type
unsigned getPointerDepth(llvm::Type*, unsigned = 0);

// Convience function to get the element type of a pointer type. The argument
// *MUST* be a pointer type
llvm::Type* getElementType(llvm::Type*);

llvm::Type* getCanonicalType(llvm::Type*, const llvm::Module&);
template <typename T>
T* getCanonicalTypeAs(llvm::Type* ty, const llvm::Module& module) {
  return llvm::dyn_cast<T>(getCanonicalType(ty, module));
}

llvm::ArrayType* getFlattened(const llvm::ArrayType*);
llvm::Type* getFlattenedElementType(const llvm::ArrayType*);
uint64_t getNumFlattenedElements(const llvm::ArrayType*);

moya::Set<llvm::StructType*> getEquivalent(llvm::StructType*,
                                           const llvm::Module&);
moya::Vector<llvm::Type*> getFlattenedTypes(const llvm::StructType*);
moya::Vector<uint64_t> getFlattenedOffsets(llvm::StructType*,
                                           const llvm::Module&);
llvm::StructType* getFlattened(const llvm::StructType*, llvm::Module&);

llvm::Type* stripPointers1(llvm::Type*);
llvm::Type* stripPointers2(llvm::Type*);

llvm::Type* stripArrays(llvm::Type*);

bool areIdentical(llvm::Type*, llvm::Type*);

// Creates a struct type for use in the analysis. This is a convenince wrapper
// because we want to flag these structs so that they are not used in any of
// the subsequent type metadata analyses
//
llvm::StructType* createStruct(llvm::Module&,
                               const moya::Vector<llvm::Type*>&,
                               const std::string&,
                               bool = false);

// template <typename... Types>
// llvm::StructType* createStruct(llvm::Module& module,
//                                const std::string& name,
//                                llvm::Type* ty1,
//                                Types*... tys) {
//   llvm::StructType* sty = llvm::StructType::create(name, ty1, tys...);
//   moya::Metadata::setAnalysisType(sty, module);

//   return sty;
// }

llvm::PointerType* createPointer(llvm::Type*, uint64_t);

moya::Vector<llvm::StructType*> getRecursiveStructs(const llvm::Module&,
                                                    bool = true);

} // namespace LLVMUtils

} // namespace moya

#endif // MOYA_COMMON_TYPE_UTILS_H
