#ifndef MOYA_COMMON_SUMMARY_BASE_H
#define MOYA_COMMON_SUMMARY_BASE_H

#include "APITypes.h"
#include "Content.h"
#include "DataLayout.h"
#include "HashMap.h"
#include "HashSet.h"
#include "Map.h"
#include "Region.h"
#include "Status.h"
#include "SummaryCellBase.h"
#include "Vector.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/GlobalVariable.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>

#include <tuple>

namespace moya {

// This looks like the store used to do the analysis with the exception that
// each cell contains only a status object and nothing else. This is what will
// get serialized and used in Talyn for the dynamic optimizations. Each summary
// object will correspond to a single function to be JIT'ed. The global
// variables will be duplicated in each summary object. This might get
// optimized in the future.
class SummaryBase {
public:
  using Address = ContentAddress::Raw;
  using Addresses = Vector<Address>;
  using Targets = SummaryCellBase::Targets;

protected:
  // The function to for which this is a summary
  JITID function;

  // The region for which this is a summary
  Region region;

  // This is true when the analysis hit Top. In this case, all runtime
  // optimizations should be disabled
  bool top;

  // The summary may be empty if the function for which the summary belongs
  // was never called within a region, or if the region itself is not
  // reachable
  bool summarized;

  DataLayout layout;

  // We use the function keys because the status for the function arguments
  // is most likely to be used when determining the signature for the
  // function call.
  // At that point, the fastest and most convenient to use is the function key
  // We don't use the FunctionID because the ID's are not available when
  // dealing with Cuda because we have to separately compile the host and
  // device code and don't generate the ID's for the device
  //
  // This is a separate structure because the cells used here are "special"
  // Their status contains additional information that can be used for key
  // construction and a few optimizations that exploit the fact that function
  // arguments can sometimes be treated differently. For example, when an
  // integer is passed by pointer in Fortran, the address might be variable if
  // it is a loop counter variable, but we can treat it as constant in the
  // function if it is not modified by any of the callees
  Vector<std::tuple<Address, Status>> args;

  // The string is really the only way to reliably get a handle on the global
  // This is ok because it only ever gets used once when compiling the function
  // so it's acceptable if it's a little more expensive than it needs to be
  //
  // This status is what we should look at when getting the status of the
  // global variable as a whole. For any individual element (in case of a
  // static array) or field (in case of a struct), the appropriate cell should
  // be looked at
  Map<std::string, std::tuple<Address, Status>> globals;

  // The cells have to be created using createCell() which has to be specialized
  // by every derived class.
  HashMap<Address, std::unique_ptr<SummaryCellBase>> cells;

  // These are the targets of load instructions, the sources of stores,
  // the pointers to the memory allocated in alloca instructions. It's
  // essentially the environment but a significantly stripped down one with
  // only those entries that we need in the analysis
  HashMap<MoyaID, Addresses> addresses;

protected:
  SummaryCellBase& getCellBase(Address);
  const SummaryCellBase& getCellBase(Address) const;

  uint64_t getMagicNumber() const;

protected:
  SummaryBase();
  SummaryBase(JITID, const Region&, bool);

public:
  virtual ~SummaryBase() = default;

  const Region& getRegion() const;
  bool isTop() const;
  bool isSummarized() const;
  JITID getFunction() const;
  Vector<std::string> getGlobals() const;

  void setLayout(const llvm::Module&);

  bool contains(Address) const;

  // Look up function argument address
  Address lookup(unsigned) const;

  // Lookup global variable address
  Address lookup(const std::string&) const;

  // A cell could have multiple targets in C++ when we have a base
  // class pointer because it could point to several derived class objects
  // which can't be folded onto one another.
  const Targets& getTargets(Address) const;

  const Addresses& getAddresses(MoyaID) const;
  Address getUniqueAddress(MoyaID) const;
  bool hasUniqueAddress(MoyaID) const;

  // Status of the function argument
  const Status& getStatus(unsigned) const;

  // Status of the global variable
  const Status& getStatus(const std::string&) const;

  // Status of the cell
  const Status& getStatus(Address) const;

  // Convenience functions for multiple addresses
  Targets getTargets(const Vector<Address>&) const;
  bool isStatusConstant(const Vector<Address>&) const;
  bool isStatusUsedInKey(const Vector<Address>&) const;

  Address propagate(const llvm::GetElementPtrInst*,
                    llvm::Type*,
                    unsigned,
                    Vector<const llvm::Value*>,
                    Address) const;

public:
  static const Address null = ContentAddress::null;
};

} // namespace moya

#endif // MOYA_COMMON_SUMMARY_BASE_H
