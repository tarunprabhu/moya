#ifndef MOYA_COMMON_QUEUE_H
#define MOYA_COMMON_QUEUE_H

#include <queue>

namespace moya {

template <typename T>
class Queue {
protected:
  std::queue<T> _impl;

public:
  using value_type = typename std::queue<T>::value_type;
  using size_type = typename std::queue<T>::size_type;
  using reference = typename std::queue<T>::reference;
  using const_reference = typename std::queue<T>::const_reference;

protected:
  std::queue<T>& getImpl() {
    return _impl;
  }

  const std::queue<T>& getImpl() const {
    return _impl;
  }

public:
  Queue() {
    ;
  }

  template <typename InputIt>
  Queue(InputIt first, InputIt last) : _impl(first, last) {
    ;
  }

  Queue(const Queue<T>& other) : _impl(other.getImpl()) {
    ;
  }

  Queue(Queue<T>&& other) : _impl(other.getImpl()) {
    ;
  }

  Queue(std::initializer_list<T> init) : _impl(init) {
    ;
  }

  virtual ~Queue() = default;

  Queue<T>& operator=(const Queue<T>& other) {
    _impl = other.getImpl();
    return *this;
  }

  Queue<T>& operator=(Queue<T>&& other) {
    _impl = other.getImpl();
    return *this;
  }

  reference front() {
    return _impl.front();
  }

  const_reference front() const {
    return _impl.front();
  }

  reference back() {
    return _impl.back();
  }

  const_reference back() const {
    return _impl.back();
  }

  bool empty() const {
    return _impl.empty();
  }

  size_type size() const {
    return _impl.size();
  }

  template <typename... Args>
  reference emplace(Args&&... args) {
    _impl.emplace(args...);
    return back();
  }

  void push(const T& value) {
    _impl.push(value);
  }

  T pop() {
    T value = std::move(_impl.front());
    _impl.pop();
    return value;
  }

  bool operator==(const Queue<T>& c2) const {
    return _impl == c2._impl;
  }

  bool operator!=(const Queue<T>& c2) const {
    return _impl != c2._impl;
  }

  bool operator<(const Queue<T>& c2) const {
    return _impl < c2._impl;
  }

  bool operator<=(const Queue<T>& c2) const {
    return _impl <= c2._impl;
  }

  bool operator>(const Queue<T>& c2) const {
    return _impl > c2._impl;
  }

  bool operator>=(const Queue<T>& c2) const {
    return _impl >= c2._impl;
  }
};

} // namespace moya

#endif // MOYA_COMMON_QUEUE_H
