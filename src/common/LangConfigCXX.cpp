#include "LangConfigCXX.h"
#include "Set.h"
#include "StringUtils.h"

static const moya::Set<std::string> exts = {"cc",
                                            "C",
                                            "cpp",
                                            "CPP",
                                            "c++",
                                            "cp",
                                            "cxx",
                                            "h",
                                            "hh",
                                            "hpp",
                                            "H",
                                            "tcc",
                                            "ii"};

bool LangConfigCXX::isValidSourceFile(const std::string& file) {
  return exts.contains(moya::StringUtils::rpartition(file, ".").second);
}

static const moya::Vector<std::string>
    cxxOps({"==", "!=", "<", "<=", ">", ">="});
const moya::Vector<std::string>& LangConfigCXX::getComparisonOperators() {
  return cxxOps;
}
