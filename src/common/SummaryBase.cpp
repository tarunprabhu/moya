#include "SummaryBase.h"
#include "Config.h"
#include "Diagnostics.h"
#include "Stringify.h"

using namespace llvm;

namespace moya {

SummaryBase::SummaryBase()
    : function(moya::Config::getInvalidJITID()), top(true), summarized(false) {
  ;
}

SummaryBase::SummaryBase(JITID function, const moya::Region& region, bool top)
    : function(function), region(region), top(top), summarized(false) {
  ;
}

uint64_t SummaryBase::getMagicNumber() const {
  return 0x51133a1e4;
}

void SummaryBase::setLayout(const Module& module) {
  layout.reset(module);
}

const moya::Region& SummaryBase::getRegion() const {
  return region;
}

JITID SummaryBase::getFunction() const {
  return function;
}

bool SummaryBase::isTop() const {
  return top;
}

bool SummaryBase::isSummarized() const {
  return summarized;
}

moya::Vector<std::string> SummaryBase::getGlobals() const {
  moya::Vector<std::string> ret;
  for(const auto& it : globals)
    ret.push_back(it.first);
  return ret;
}

bool SummaryBase::contains(Address addr) const {
  return cells.contains(addr);
}

const SummaryBase::Addresses& SummaryBase::getAddresses(MoyaID id) const {
  return addresses.at(id);
}

bool SummaryBase::hasUniqueAddress(MoyaID id) const {
  if(addresses.contains(id) and (addresses.at(id).size() == 1))
    return true;
  return false;
}

SummaryBase::Address SummaryBase::getUniqueAddress(MoyaID id) const {
  if(not hasUniqueAddress(id))
    moya_error("ID does not have a unique address: " << id);

  return *addresses.at(id).begin();
}

const Status& SummaryBase::getStatus(unsigned argno) const {
  return std::get<Status>(args.at(argno));
}

const Status& SummaryBase::getStatus(const std::string& g) const {
  return std::get<Status>(globals.at(g));
}

const Status& SummaryBase::getStatus(Address addr) const {
  return cells.at(addr)->getStatus();
}

const SummaryBase::Targets& SummaryBase::getTargets(Address addr) const {
  return cells.at(addr)->getTargets();
}

bool SummaryBase::isStatusConstant(const Vector<Address>& addrs) const {
  for(Address addr : addrs)
    if(not getStatus(addr).isConstant())
      return false;
  return true;
}

bool SummaryBase::isStatusUsedInKey(const Vector<Address>& addrs) const {
  for(Address addr : addrs)
    if(not getStatus(addr).isUsedInKey())
      return false;
  return true;
}

SummaryBase::Targets
SummaryBase::getTargets(const Vector<Address>& addrs) const {
  SummaryBase::Targets targets;
  for(Address addr : addrs)
    targets.push_back(getTargets(addr));
  return targets;
}

SummaryBase::Address SummaryBase::lookup(unsigned argno) const {
  return std::get<Address>(args.at(argno));
}

SummaryBase::Address SummaryBase::lookup(const std::string& gname) const {
  return std::get<Address>(globals.at(gname));
}

SummaryBase::Address SummaryBase::propagate(const GetElementPtrInst* gep,
                                            Type* type,
                                            unsigned op,
                                            moya::Vector<const Value*> indices,
                                            Address base) const {
  int64_t idx = 0;
  Value* vidx = gep->getOperand(op);
  Type* nextTy = nullptr;

  if(auto* cint = dyn_cast<ConstantInt>(vidx)) {
    int64_t val = cint->getLimitedValue();
    if(auto* pty = dyn_cast<PointerType>(type)) {
      nextTy = pty->getElementType();
    } else if(auto* aty = dyn_cast<ArrayType>(type)) {
      nextTy = aty->getElementType();
    } else if(auto* sty = dyn_cast<StructType>(type)) {
      idx = val;
      nextTy = sty->getElementType(val);
    } else {
      moya_error("Unexpected type in GEP: " << *gep);
    }
  } else {
    if(auto* pty = dyn_cast<PointerType>(type))
      nextTy = pty->getElementType();
    else if(auto* aty = dyn_cast<ArrayType>(type))
      nextTy = aty->getElementType();
    else if(isa<StructType>(type))
      moya_error("Expected constant operand for struct field: " << *gep);
    else
      moya_error("Unexpected type in GEP: " << *gep);
  }

  indices.push_back(ConstantInt::getSigned(vidx->getType(), idx));
  if(op == gep->getNumIndices())
    return base
           + layout.getOffsetInType(
               dyn_cast<PointerType>(gep->getPointerOperandType())
                   ->getElementType(),
               indices);
  else
    return propagate(gep, nextTy, op + 1, indices, base);
}

} // namespace moya
