#include "RegexUtils.h"

namespace moya {

namespace Regex {

bool matches(const std::string& s, const std::string& regex) {
  return matches(s, std::regex(regex));
}

bool matches(const std::string& s, const std::regex& re) {
  return std::regex_match(s, re);
}

bool startswith(const std::string& s, const std::string& regex) {
  return startswith(s, std::regex(regex));
}

bool startswith(const std::string& s, const std::regex& re) {
  std::smatch m;
  if(std::regex_match(s, m, re))
    return m.prefix().length() == 0;
  return false;
}

} // namespace Regex

} // namespace moya
