#ifndef MOYA_COMMON_TIMER_H
#define MOYA_COMMON_TIMER_H

#include <chrono>

class Timer {
public:
  using Secs = double;
  using MSecs = double;
  using USecs = double;
  using NSecs = int64_t;

private:
  enum State {
    Stopped,
    Running,
    Paused,
  };

private:
  bool used;
  State state;

  // The accumulated time so far. This will get updated when the timer is
  // paused and will be reset when the timer is started
  NSecs accum;

  // The total time measured by this timer object. This will be set to zero
  // when the object is first created and will only be reset with a call to
  // the reset() function
  NSecs total;

private:
  NSecs now();

public:
  Timer();

  void start();
  NSecs stop();
  NSecs pause();
  void resume();
  void reset();

  bool isUsed() const;
  bool isRunning() const;
  bool isPaused() const;

  Secs secs() const;
  MSecs msecs() const;
  USecs usecs() const;
  NSecs nsecs() const;
};

#endif
