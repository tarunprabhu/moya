#ifndef MOYA_COMMON_STORE_H
#define MOYA_COMMON_STORE_H

#include "AnalysisStatus.h"
#include "CellFlags.h"
#include "Contents.h"
#include "DataLayout.h"
#include "HashMap.h"
#include "HashSet.h"
#include "Map.h"
#include "Vector.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>

namespace moya {

class Environment;
class Serializer;
class StoreCell;

class Store {
public:
  using RawAddress = ContentAddress::Raw;
  using Address = ContentAddress;

  enum AccessKind {
    Valid = 0,

    // This could be for any number of reasons including unimplemented accesses
    // (reading an array from the store), type mismatches (non-existent struct
    // at address)
    Invalid,

    // Accessing a non-existent cell
    NonExistentCell,

    // When reading/writing a struct from a scalar and all elements of the
    // struct are also scalars
    StructFromScalar
  };

private:
  // The module that this store is used with
  const llvm::Module& module;

  // The environment that this store is used with
  const Environment& env;

  DataLayout layout;

  HashMap<RawAddress, std::unique_ptr<StoreCell>> cells;

  // This is the largest type allocated at a given address. For instance,
  // if the type:
  //
  // struct C {
  //   int[20];
  //   float;
  // };
  //
  // struct A {
  //   struct B {
  //     struct C[4];
  //     int;
  //   };
  //   const char*;
  // };
  //
  // were allocated at address a0, then allocated would contain an entry:
  //
  // allocated[a0] = struct A
  //
  // even though the cell at a0 is of type int.
  //
  HashMap<RawAddress, llvm::Type*> allocated;

  // These are all the structs and arrays that begin at a given address
  // For the example above,
  //
  // structs[a0] = [struct B, struct A]
  // arrays[a0] = [int, struct C]
  //
  // The values in the map are vectors because they are unlikely to be
  // very large except for pathological cases which are again unlikely to occur
  // in real code. And we need to preseve the order because the entries will
  // be in the order from the "innermost" array or struct to the outermost
  //
  HashMap<RawAddress, Set<llvm::StructType*>> structs;
  HashMap<RawAddress, Set<llvm::ArrayType*>> arrays;
  HashMap<RawAddress, HashMap<llvm::Type*, std::tuple<uint64_t, bool>>>
      arraySpecs;

  Vector<std::unique_ptr<const Content>> objs;
  HashMap<const llvm::ConstantPointerNull*, uint64_t> cnulls;
  HashMap<const llvm::ConstantInt*, uint64_t> cints;
  HashMap<const llvm::ConstantFP*, uint64_t> cfps;
  HashMap<const llvm::ConstantArray*, uint64_t> carrays;
  HashMap<const llvm::ConstantStruct*, uint64_t> cstructs;
  HashMap<const llvm::ConstantDataArray*, uint64_t> cdas;
  HashMap<const llvm::Function*, uint64_t> cfuns;
  Map<const Vector<Contents>, uint64_t> ccomps;
  HashMap<RawAddress, uint64_t> caddrs;
  HashMap<const ContentComposite*, Contents> ccompContents;

  std::unique_ptr<const ContentRuntimeScalar> runtimeScalar;
  std::unique_ptr<const ContentRuntimePointer> runtimePointer;
  std::unique_ptr<const ContentNullptr> nullPointer;
  std::unique_ptr<const ContentUndef> undefValue;
  std::unique_ptr<const ContentComposite> exceptionObject;

  // The next available store address to use when allocating
  RawAddress nextAvailableAddress;

private:
  const Address* reserve(llvm::Type*);

  void makeNewCell(RawAddress,
                   llvm::Type*,
                   const llvm::Instruction*,
                   const llvm::Function*,
                   const CellFlags&);

  AnalysisStatus
  del(const Address*, const llvm::Instruction*, const llvm::Function*);

  const Contents& makeContents(const Vector<Contents>&);

  void populate(llvm::Type*,
                const CellFlags&,
                bool,
                const llvm::Instruction*,
                const llvm::Function*,
                RawAddress);
  void populate(llvm::PointerType*,
                const CellFlags&,
                bool,
                const llvm::Instruction*,
                const llvm::Function*,
                RawAddress);
  void populate(llvm::ArrayType*,
                const CellFlags&,
                bool,
                const llvm::Instruction*,
                const llvm::Function*,
                RawAddress);
  void populate(llvm::StructType*,
                const CellFlags&,
                bool,
                const llvm::Instruction*,
                const llvm::Function*,
                RawAddress);

  const Address* allocate(llvm::Type*,
                          const CellFlags&,
                          const llvm::Instruction*,
                          const llvm::Function*);
  const Address* allocate(llvm::PointerType*,
                          const CellFlags&,
                          bool,
                          const llvm::Instruction*,
                          const llvm::Function*);
  const Address* allocate(llvm::ArrayType*,
                          const CellFlags&,
                          bool,
                          const llvm::Instruction*,
                          const llvm::Function*);
  const Address* allocate(llvm::StructType*,
                          const CellFlags&,
                          bool,
                          const llvm::Instruction*,
                          const llvm::Function*);

  AnalysisStatus deallocate(llvm::ArrayType*,
                            const Address*,
                            const llvm::Instruction*,
                            const llvm::Function*);
  AnalysisStatus deallocate(llvm::StructType*,
                            const Address*,
                            const llvm::Instruction*,
                            const llvm::Function*);

  AccessKind check_legality_of_read(const Address*,
                                    llvm::Type*,
                                    const llvm::Instruction*) const;
  AccessKind check_legality_of_write(const Address*,
                                     llvm::Type*,
                                     const llvm::Instruction*) const;

  void serialize(llvm::PointerType*, Serializer&) const;
  void serialize(llvm::ArrayType*, Serializer&) const;
  void serialize(llvm::StructType*, Serializer&) const;
  void serialize(llvm::FunctionType*, Serializer&) const;

  bool isTypeAt(RawAddress, llvm::Type*) const;
  bool isStructAt(RawAddress, llvm::StructType*) const;
  bool isArrayAt(RawAddress, llvm::Type*) const;

  bool contains(RawAddress) const;

  StoreCell& getCell(RawAddress);

public:
  Store(const llvm::Module&, const Environment&);
  Store(const Store&&) = delete;

  uint64_t getPointerSize() const;
  uint64_t getTypeSize(llvm::Type*) const;
  uint64_t getTypeAllocSize(llvm::Type*) const;
  uint64_t getTypeAlignment(llvm::Type*) const;
  int64_t getOffsetInType(llvm::Type*, const Vector<const llvm::Value*>&) const;
  const StructLayout& getStructLayout(llvm::StructType*) const;

  bool contains(const Address*, long = 0) const;

  // void make(llvm::ArrayType*, Vector<Contents>&);
  // void make(llvm::StructType*, Vector<Contents>&);
  const Content* make(llvm::Type*);
  const ContentScalar* makeScalar(const llvm::ConstantInt*);
  const Content* make(const llvm::Constant*);
  const ContentScalar* make(const llvm::ConstantInt*);
  const ContentRuntimeScalar* make(const llvm::ConstantFP*);
  const ContentNullptr* make(const llvm::ConstantPointerNull*);
  const ContentComposite* make(const llvm::ConstantArray*);
  const ContentComposite* make(const llvm::ConstantDataArray*);
  const ContentComposite* make(const llvm::ConstantStruct*);
  const ContentUndef* make(const llvm::UndefValue*);
  const ContentCode* make(const llvm::Function*);
  const ContentComposite* make(const Vector<Contents>&);

  const Address* make(RawAddress);
  const Address* make(const Address*, int64_t = 0);
  const Address* make(const Address*, int64_t = 0) const;

  const ContentNullptr* getNullptr() const;
  const ContentRuntimeScalar* getRuntimeScalar() const;
  const ContentRuntimePointer* getRuntimePointer() const;
  const ContentUndef* getUndefValue() const;
  const ContentComposite* getExceptionObject() const;
  const Content* getRuntimeValue(llvm::Type*);

  const Content* getDefaultScalar(llvm::Type*) const;
  const Content* getDefaultPointer() const;

  Vector<RawAddress> getCellAddresses() const;
  StoreCell& getCell(const Address*, int64_t = 0);
  const StoreCell& getCell(const Address*, int64_t = 0) const;
  const StoreCell& getCell(RawAddress) const;

  bool isTypeAt(const Address*, llvm::Type*) const;
  bool isStructAt(const Address*, llvm::StructType*) const;
  bool isArrayAt(const Address*, llvm::Type*) const;

  void addArrayAt(RawAddress, llvm::Type*, uint64_t, bool);
  void addStructAt(RawAddress, llvm::StructType*);
  llvm::StructType* getLargestStructAt(const Address*) const;
  const Set<llvm::StructType*>& getStructsAt(const Address*) const;
  const Set<llvm::ArrayType*>& getArraysAt(const Address*) const;
  Vector<llvm::Type*> getTypesAt(const Address*) const;

  bool hasFlag(const Address*, CellFlags::Flag) const;

  const Address* allocate(llvm::Type*,
                          const CellFlags&,
                          bool,
                          const llvm::Instruction* = nullptr,
                          const llvm::Function* = nullptr);
  AnalysisStatus deallocate(llvm::Type*,
                            const Address*,
                            const llvm::Instruction*,
                            const llvm::Function*);

  // If an object was allocated at this address, return true
  // This has to be the starting address of the object. It doesn't matter
  // whether the object was statically or dynamically allocated
  bool hasAllocated(const Address*, int64_t = 0) const;

  // Gets the allocated type at the address
  llvm::Type* getAllocated(const Address*, int64_t = 0) const;

  // Given an address and an array type, returns the length of the array of that
  // type that begins at that address
  uint64_t getArrayLength(const Address*, llvm::Type*, int64_t = -1) const;

  // If pointers are represented as 64-bit integers, we might need to convert
  // it to an address every now again
  const Content* maybeConvertToAddress(const Address*, const Content*);

  bool empty(const Address*, const llvm::Instruction*) const;

  const Contents& read(const Address*) const;
  const Contents& read(const Address*, llvm::Type*);
  const Contents&
  read(const Address*, llvm::Type*, const llvm::Instruction*, AnalysisStatus&);

  template <typename Out = Content>
  ContentsImpl<Out> readAs(const Addresses& addrs,
                           llvm::Type* type,
                           const llvm::Instruction* call,
                           AnalysisStatus& changed) {
    Contents contents;
    for(const Address* addr : addrs)
      contents.insert(read(addr, type, call, changed));
    return contents.getAs<Out>(call);
  }

  template <typename Out = Content>
  ContentsImpl<Out> readAs(const Addresses& addrs,
                           unsigned long offset,
                           llvm::Type* type,
                           const llvm::Instruction* call,
                           AnalysisStatus& changed) {
    Contents contents;
    for(const Address* addr : addrs)
      contents.insert(read(make(addr, offset), type, call, changed));
    return contents.getAs<Out>(call);
  }

  template <typename Out = Content>
  ContentsImpl<Out> readAs(const Contents& ptrs,
                           llvm::Type* type,
                           const llvm::Instruction* call,
                           AnalysisStatus& changed) {
    return readAs<Out>(ptrs.template getAs<Address>(call), type, call, changed);
  }

  template <typename Out = Content>
  ContentsImpl<Out> readAs(const Contents& ptrs,
                           unsigned long offset,
                           llvm::Type* type,
                           const llvm::Instruction* call,
                           AnalysisStatus& changed) {
    return readAs<Out>(
        ptrs.template getAs<Address>(call), offset, type, call, changed);
  }

  void write(const Address*, const Content*, llvm::Type*);
  void write(const Address*,
             const Content*,
             llvm::Type*,
             const llvm::Instruction*,
             AnalysisStatus&);
  void write(const Address*, const Contents&, llvm::Type*);
  void write(const Address*,
             const Contents&,
             llvm::Type*,
             const llvm::Instruction*,
             AnalysisStatus&);

  // This could just as easily be done using reads and writes, but if we
  // have to copy structs, it can get expensive because ContentImpl<*> objects
  // will have to be created and copied around. Returns the number of objects
  // actually copied.
  uint64_t copy(const Address*,
                const Address*,
                llvm::Type*,
                const llvm::Instruction*,
                AnalysisStatus&);

  // These are "dummy" reads and writes respectively where we want to mark
  // cells in the store as having been read or written without modifying them
  // In the case of dummy reads, this is much more efficient because it doesn't
  // attempt to create a Content object to return which can make it much
  // more efficient than using a read and then discarding the results.
  // touch() is present for symmetry since there is a look method but it is no
  // better than using a write with no contents
  AnalysisStatus look(const Address*, llvm::Type*, const llvm::Instruction*);
  AnalysisStatus touch(const Address*, llvm::Type*, const llvm::Instruction*);

  // Functions for debugging
  std::string strAllocated(int = 0) const;
  std::string strArrays(int = 0) const;
  std::string strStructs(int = 0) const;
  std::string str() const;

  // Functions for serialization
  void serialize(Serializer&) const;
  void serializeTypes(Serializer&) const;
  void serializeCells(Serializer&) const;

  // These are only to be used in special cases. For instance, in realloc(),
  // we don't really gain anything by reallocating a buffer, but we want to
  // add that function as an allocator to the cell.
  void addReader(const Address*, const llvm::Instruction*);
  void addWriter(const Address*, const llvm::Instruction*);
  void addAllocator(const Address*, const llvm::Instruction*);
  void addDeallocator(const Address*, const llvm::Instruction*);

  void addReader(const Address*, const llvm::Function*);
  void addWriter(const Address*, const llvm::Function*);
  void addAllocator(const Address*, const llvm::Function*);
  void addDeallocator(const Address*, const llvm::Function*);

private:
  template <typename ContentT>
  const ContentT* getObjectAs(uint64_t idx) const {
    return llvm::cast<ContentT>(objs.at(idx).get());
  }

  template <typename ContentT, typename LLVMT>
  const ContentT* makeObject(const LLVMT* llvm,
                             HashMap<const LLVMT*, uint64_t>& cobjs) {
    if(cobjs.contains(llvm))
      return getObjectAs<ContentT>(cobjs.at(llvm));
    objs.emplace_back(new ContentT(llvm));
    cobjs[llvm] = objs.size() - 1;
    return getObjectAs<ContentT>(cobjs.at(llvm));
  }

  template <typename LLVMT>
  const ContentComposite*
  makeCompositeObject(const LLVMT* llvm,
                      HashMap<const LLVMT*, uint64_t>& cobjs) {
    if(cobjs.contains(llvm))
      return getObjectAs<ContentComposite>(cobjs.at(llvm));
    Vector<Contents> elems;
    for(const llvm::Use& u : llvm->operands())
      elems.push_back(make(llvm::dyn_cast<llvm::Constant>(u.get())));
    return make(elems);
  }

public:
  static const RawAddress null = ContentAddress::null;

public:
  friend class StoreCell;
};

} // namespace moya

#endif // MOYA_COMMON_STORE_H
