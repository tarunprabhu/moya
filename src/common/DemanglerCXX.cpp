#include "DemanglerCXX.h"
#include "Diagnostics.h"
#include "Metadata.h"
#include "StringUtils.h"
#include "Vector.h"

#include <sstream>
#define HAVE_DECL_BASENAME 1
#include <libiberty/demangle.h>

#include <llvm/Support/raw_ostream.h>

extern "C" char* cplus_demangle(const char*, int);

namespace moya {

class DemangledFunction {
private:
  std::string name;
  std::string ns;
  std::string ret;
  moya::Vector<std::string> args;

public:
  void setNamespace(const std::string& s) {
    ns = s;
  }

  void setName(const std::string& s) {
    name = s;
  }

  void setRet(const std::string& s) {
    ret = s;
  }

  void addArg(const std::string&& s) {
    args.push_back(s);
  }

  const std::string& getNamespace() const {
    return ns;
  }

  const std::string& getName() const {
    return name;
  }

  const std::string& getRet() const {
    return ret;
  }

  moya::Vector<std::string> getArgs() const {
    return args;
  }

  std::string str() const {
    std::stringstream ss;

    if(ns.length())
      ss << ns << "::";
    ss << name;
    ss << "(";
    if(args.size()) {
      ss << args.at(0);
      for(unsigned long i = 1; i < args.size(); i++)
        ss << ", " << args.at(i);
    }
    ss << ")";
    return ss.str();
  }
};

DemanglerCXX::DemanglerCXX() : Demangler(SourceLang::CXX) {
  special = {"TLS wrapper"};
}

std::string
DemanglerCXX::stripTemplates(const std::string& s, long begin, long end) const {
  int depth = 0;
  std::stringstream ss;
  for(long i = begin; i != end; i++) {
    char c = s[i];
    if(c == '<')
      depth += 1;
    else if(c == '>')
      depth -= 1;
    else if(depth == 0)
      ss << c;
  }
  return ss.str();
}

std::string DemanglerCXX::stripTemplates(const std::string& s) const {
  size_t ignoreBegin = std::string::npos;
  size_t ignoreEnd = std::string::npos;
  size_t opLT = s.find("operator<");
  size_t opGT = s.find("operator>");
  size_t opPtr = s.find("operator->");
  if(opLT != std::string::npos) {
    ignoreBegin = opLT;
    ignoreEnd = ignoreBegin + 9;
    if(s[ignoreEnd] == '<')
      ignoreEnd += 1;
  } else if(opGT != std::string::npos) {
    ignoreBegin = opGT;
    ignoreEnd = ignoreBegin + 9;
    if(s[ignoreEnd] == '>')
      ignoreEnd += 1;
  } else if(opPtr != std::string::npos) {
    ignoreBegin = opPtr;
    ignoreEnd = ignoreBegin + 10;
  }

  std::stringstream ss;
  if(ignoreBegin != std::string::npos) {
    ss << stripTemplates(s, 0, ignoreBegin);
    for(unsigned long i = ignoreBegin; i < ignoreEnd; i++)
      ss << s[i];
    ss << stripTemplates(s, ignoreEnd, s.size());
  } else {
    ss << stripTemplates(s, 0, s.size());
  }

  return ss.str();
}

std::string DemanglerCXX::parseType(const std::string& s) const {
  std::string t = moya::StringUtils::strip(s);
  t = moya::StringUtils::replace(t, " const", "");
  t = stripCppImplNamespaces(t);
  return t;
}

std::string DemanglerCXX::stripCppImplNamespaces(const std::string& s) const {
  std::string t = s;
  t = moya::StringUtils::replace(t, "__cxx11::", "");
  t = moya::StringUtils::replace(t, "__detail::", "");
  return t;
}

std::string DemanglerCXX::stripABISpecifications(const std::string& s) const {
  // FIXME: Make this more general
  return moya::StringUtils::replace(s, "[abi:cxx11]", "");
}

void DemanglerCXX::parseArgs(const std::string& s, DemangledFunction& f) const {
  moya::Vector<std::string> ret;
  std::stringstream ss;

  // Since all the templates are removed, all the arguments will be separated
  // by commas
  for(char c : moya::StringUtils::strip(s)) {
    if(c == ',') {
      f.addArg(parseType(ss.str()));
      ss.str(std::string());
    } else {
      ss << c;
    }
  }
  if(ss.str().size() != 0)
    f.addArg(parseType(ss.str()));
}

void DemanglerCXX::parseName(const std::string& s, DemangledFunction& f) const {
  std::pair<std::string, std::string> p
      = moya::StringUtils::rpartition(s, "::");
  if(p.first.size())
    f.setNamespace(p.first);
  f.setName(p.second);
}

void DemanglerCXX::parse(const std::string& orig, DemangledFunction& f) const {
  std::string s = stripTemplates(orig);
  long i = s.size() - 1;
  // Strip any trailing characters until the first right parenthesis.
  while(s[i] != ')')
    i--;

  // The function arguments will be within a pair of balanced parentheses
  long argsEnd = i - 1;
  i--;
  for(int depth = 1; depth != 0; i--)
    if(s[i] == ')')
      depth += 1;
    else if(s[i] == '(')
      depth -= 1;
  parseArgs(s.substr(i + 2, argsEnd - i - 1), f);

  // Remove any spaces between the function name and the parenthesis
  while(s[i] == ' ')
    i--;

  // All the template goo is gone.
  // If this is an overloaded operator, the name might have a space.
  // If not, then anything until the first space is the function name
  long nameEnd = i;
  if(s.find("operator ") != std::string::npos) {
    while(i >= 0 and s[i] != ' ')
      i--;
    i--;
    while(i >= 0 and s[i] != ' ')
      i--;
  } else {
    while(i >= 0 and s[i] != ' ')
      i--;
  }

  std::string h = stripCppImplNamespaces(s.substr(i + 1, nameEnd - i));
  h = stripABISpecifications(h);

  // We have to use StringUtils::lpartition here because rpartition might split
  // the string in the middle of an operator overload function name
  std::pair<std::string, std::string> p = moya::StringUtils::lpartition(h, " ");

  if(h.find("operator ") != std::string::npos) {
    // If this is an operator overload, splitting on the first space should
    // give the return type if there is one. If there isn't, then it will
    // split on the operator keyword
    if(p.second.size()) {
      if(p.first == "operator") {
        parseName(h, f);
      } else {
        f.setRet(moya::StringUtils::strip(p.first));
        parseName(p.second, f);
      }
    } else {
      parseName(p.first, f);
    }
  } else {
    // If this is not an operator overload, then splitting on the first space
    // may have a return type
    if(p.second.size()) {
      f.setRet(moya::StringUtils::strip(p.first));
      parseName(p.second, f);
    } else {
      parseName(p.first, f);
    }
  }
}

std::string DemanglerCXX::demangle(const std::string& s, int flags) const {
  char* cstr = cplus_demangle(s.c_str(), flags);
  if(not cstr)
    return s;

  std::string ret(cstr);
  free(cstr);
  return ret;
}

std::string DemanglerCXX::getFullName(const std::string& in) const {
  return demangle(in, DMGL_PARAMS);
}

std::string DemanglerCXX::getSourceName(const std::string& in) const {
  std::string full = demangle(in, 0);

  if((full == in) or special.contains(full))
    return in;

  DemangledFunction f;
  parse(full, f);
  return f.getName();
}

std::string DemanglerCXX::getQualifiedName(const std::string& in) const {
  std::string full = demangle(in, 0);

  if((full == in) or special.contains(full))
    return in;

  std::stringstream ss;
  DemangledFunction f;
  parse(full, f);
  ss << f.getNamespace() << "::" << f.getName();
  return ss.str();
}

std::string DemanglerCXX::getModelName(const std::string& in) const {
  std::string full = demangle(in, DMGL_PARAMS);

  if((full == in) or special.contains(full))
    return full;

  DemangledFunction f;
  parse(full, f);
  return f.str();
}

std::string DemanglerCXX::demangle(const llvm::Function& f,
                                   Demangler::Action action) const {
  switch(action) {
  case Demangler::Action::Full:
    if(moya::Metadata::hasFullName(f))
      return moya::Metadata::getFullName(f);
    return getFullName(f.getName().str());
    break;
  case Demangler::Action::Qualified:
    if(moya::Metadata::hasQualifiedName(f))
      return moya::Metadata::getQualifiedName(f);
    return getQualifiedName(f.getName().str());
    break;
  case Demangler::Action::Source:
    if(moya::Metadata::hasSourceName(f))
      return moya::Metadata::getSourceName(f);
    return getSourceName(f.getName().str());
  case Demangler::Action::Model:
    if(moya::Metadata::hasModel(f))
      return moya::Metadata::getModel(f);
    return getModelName(f.getName().str());
    break;
  default:
    moya_error("Unsupported demangler action: " << action);
  }

  return f.getName().str();
}

std::string DemanglerCXX::demangle(const llvm::GlobalVariable& g,
                                   Demangler::Action action) const {
  switch(action) {
  case Demangler::Action::Full:
    if(moya::Metadata::hasFullName(g))
      return moya::Metadata::getFullName(g);
    return getFullName(g.getName().str());
    break;
  case Demangler::Action::Qualified:
    if(moya::Metadata::hasQualifiedName(g))
      return moya::Metadata::getQualifiedName(g);
    return getQualifiedName(g.getName().str());
    break;
  case Demangler::Action::Source:
    if(moya::Metadata::hasSourceName(g))
      return moya::Metadata::getSourceName(g);
    return getSourceName(g.getName().str());
  case Demangler::Action::Model:
    moya_error("Unsupported demangler action for global: " << action);
    break;
  default:
    moya_error("Unsupported demangler action: " << action);
  }

  return g.getName().str();
}

} // namespace moya
