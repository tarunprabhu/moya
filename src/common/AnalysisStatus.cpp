#include "AnalysisStatus.h"
#include "Config.h"
#include "Diagnostics.h"
#include "LLVMUtils.h"
#include "Metadata.h"

namespace moya {

AnalysisStatus operator|=(AnalysisStatus& lhs, bool b) {
  lhs = static_cast<AnalysisStatus>(static_cast<unsigned>(lhs) | b);
  return lhs;
}

AnalysisStatus operator|=(AnalysisStatus& lhs, AnalysisStatus rhs) {
  lhs = static_cast<AnalysisStatus>(static_cast<unsigned>(lhs)
                                    | static_cast<unsigned>(rhs));
  return lhs;
}

bool isChanged(AnalysisStatus status) {
  return status != AnalysisStatus::Unchanged;
}

bool isTop(AnalysisStatus status) {
  return status == AnalysisStatus::Top;
}

AnalysisStatus setTop(const llvm::Instruction* inst) {
  const llvm::Function* f = LLVMUtils::getFunction(inst);
  if(moya::Metadata::hasMemSafe(f))
    return AnalysisStatus::Unchanged;

  if(moya::Config::isEnvErrorOnTop())
    moya_error("Setting top\n"
               << "  inst: " << getDiagnostic(inst) << "\n\n"
               << getBacktrace());
  else
    moya_warning("Setting top\n"
                 << "  inst: " << getDiagnostic(inst) << "\n\n"
                 << getBacktrace());

  return AnalysisStatus::Top;
}

AnalysisStatus setTop() {
  if(moya::Config::isEnvErrorOnTop())
    moya_error("Setting top during initialization\n\n" << getBacktrace());
  else
    moya_warning("Setting top during initialization\n\n" << getBacktrace());

  return AnalysisStatus::Top;
}

// AnalysisStatus setTop(const std::string& model, const Function* f) {
//   moya_error("Setting top in " << model << " from " << getDiagnosticName(f));
//   return AnalysisStatus::Top;
// }

std::string str(AnalysisStatus status) {
  switch(status) {
  case AnalysisStatus::Unchanged:
    return "Unchanged";
    break;
  case AnalysisStatus::Changed:
    return "Changed";
    break;
  case AnalysisStatus::Top:
    return "TOP";
    break;
  default:
    return "<<< UNKNOWN ANALYSIS STATUS >>>";
    break;
  }
  return "";
}

} // namespace moya
