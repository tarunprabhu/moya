#ifndef MOYA_COMMON_STRUCT_LAYOUT_H
#define MOYA_COMMON_STRUCT_LAYOUT_H

#include "Vector.h"

#include <llvm/IR/DataLayout.h>
#include <llvm/IR/Type.h>

namespace moya {

// A wrapper around an LLVM StructLayout object that extends it by getting
// the flattend layout of a struct as well
class StructLayout {
protected:
  llvm::StructType* sty;
  const llvm::StructLayout& sl;
  moya::Vector<uint64_t> flattenedOffsets;
  moya::Vector<llvm::Type*> flattenedTypes;

protected:
  void init(llvm::StructType*, const llvm::DataLayout&, uint64_t);

public:
  StructLayout(llvm::StructType*, const llvm::DataLayout&);

  uint64_t getSizeInBits() const;
  uint64_t getSizeInBytes() const;
  unsigned getAlignment() const;
  bool hasPadding() const;
  unsigned getNumElements() const;
  unsigned getElementContainingOffset(uint64_t) const;
  uint64_t getElementOffset(unsigned) const;
  uint64_t getElementOffsetInBits(unsigned) const;
  unsigned getNumFlattenedElements() const;
  llvm::Type* getFlattenedElementType(unsigned) const;
  uint64_t getFlattenedElementOffset(unsigned) const;
};

} // namespace moya

#endif // MOYA_COMMON_STRUCT_LAYOUT_H
