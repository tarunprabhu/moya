#ifndef MOYA_COMMON_ARRAY_REF_UTILS_H
#define MOYA_COMMON_ARRAY_REF_UTILS_H

#include "Vector.h"

#include <llvm/ADT/ArrayRef.h>

namespace moya {

namespace LLVMUtils {

template <typename T>
llvm::ArrayRef<T> makeArrayRef(const Vector<T>& vec) {
  return llvm::ArrayRef<T>(vec.data(), vec.size());
}

} // namespace LLVMUtils

} // namespace moya

#endif // MOYA_COMMON_ARRAY_REF_UTILS_H
