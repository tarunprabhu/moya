#ifndef MOYA_COMMON_HASHMAP2_H
#define MOYA_COMMON_HASHMAP2_H

#include <sstream>
#include <string>
#include <unordered_map>

#include "Deserializer.h"
#include "Serializer.h"
#include "Stringify.h"
#include "Vector.h"

namespace moya {

template <typename K, typename V>
class HashMap2 {
protected:
  std::unordered_map<K, V> _impl;
  std::unordered_map<V, K> _rev_impl;

public:
  using key_type = typename std::unordered_map<K, V>::key_type;
  using mapped_type = typename std::unordered_map<K, V>::mapped_type;
  using value_type = typename std::unordered_map<K, V>::value_type;
  using size_type = typename std::unordered_map<K, V>::size_type;
  using iterator = typename std::unordered_map<K, V>::iterator;
  using value_iterator = typename std::unordered_map<V, K>::iterator;
  using const_iterator = typename std::unordered_map<K, V>::const_iterator;
  using const_value_iterator =
      typename std::unordered_map<V, K>::const_iterator;

protected:
  std::unordered_map<K, V>& getImpl() {
    return _impl;
  }

  std::unordered_map<V, K>& getReverseImpl() {
    return _rev_impl;
  }

  const std::unordered_map<K, V>& getImpl() const {
    return _impl;
  }

  const std::unordered_map<V, K>& getReverseImpl() const {
    return _rev_impl;
  }

  void update() {
    for(const auto& it : _impl)
      _rev_impl.emplace(std::make_pair(it.second, it.first));
  }

  void _erase_key(iterator pos) {
    _impl.erase(pos);
  }

  void _erase_value(value_iterator pos) {
    _rev_impl.erase(pos);
  }

public:
  HashMap2() {
    ;
  }

  template <typename InputIt>
  HashMap2(InputIt first, InputIt last) : _impl(first, last) {
    update();
  }

  HashMap2(const HashMap2<K, V>& other) : _impl(other.getImpl()) {
    update();
  }

  HashMap2(HashMap2&& other) : _impl(other.getImpl()) {
    update();
  }

  HashMap2(std::initializer_list<value_type> init) : _impl(init) {
    update();
  }

  virtual ~HashMap2() = default;

  HashMap<K, V>& operator=(const HashMap<K, V>& other) {
    _impl = other.getImpl();
    update();
    return *this;
  }

  HashMap<K, V>& operator=(HashMap<K, V>&& other) {
    _impl = other.getImpl();
    update();
    return *this;
  }

  HashMap<K, V>& operator=(std::initializer_list<value_type> init) {
    _impl = init;
    update();
    return *this;
  }

  V& at(const K& key) {
    return _impl.at(key);
  }

  K& key_at(const V& value) {
    return _rev_impl.at(value);
  }

  const V& at(const K& key) const {
    return _impl.at(key);
  }

  const K& key_at(const V& value) const {
    return _rev_impl.at(value);
  }

  iterator begin() {
    return _impl.begin();
  }

  value_iterator value_begin() {
    return _rev_impl.begin();
  }

  const_iterator begin() const {
    return _impl.begin();
  }

  const_value_iterator value_begin() const {
    return _rev_impl.begin();
  }

  iterator end() {
    return _impl.end();
  }

  value_iterator value_end() {
    return _rev_impl.end();
  }

  const_iterator end() const {
    return _impl.end();
  }

  const_value_iterator value_end() const {
    return _rev_impl.end();
  }

  bool contains(const K& k) const {
    return _impl.find(k) != _impl.end();
  }

  bool contains_value(const V& value) const {
    return _rev_impl.find(value) != _rev_impl.end();
  }

  bool empty() const {
    return _impl.empty();
  }

  size_type size() const {
    return _impl.size();
  }

  void clear() {
    _impl.clear();
    _rev_impl.clear();
  }

  template <typename... Args>
  bool emplace(Args&&... args) {
    bool ret = _impl.emplace(args...).second;
    update();
    return ret;
  }

  bool insert(const value_type& value) {
    bool ret = _impl.insert(value).second;
    update();
    return ret;
  }

  template <typename InputIt>
  void insert(InputIt first, InputIt last) {
    _impl.insert(first, last);
    update();
  }

  bool insert(const HashMap<K, V>& other) {
    bool changed = false;

    for(const auto& kv : other)
      changed |= _impl.insert(kv).second;

    return changed;
  }

  iterator erase(const_iterator pos) {
    V& val = pos.second;
    iterator ret = _impl.erase(pos);
    if(contains_value(val))
      erase_value(val);
    return ret;
  }

  iterator erase(const_iterator first, const_iterator last) {
    for(auto it = first; it != last; it++)
      erase_value(it.second);
    return _impl.erase(first, last);
  }

  size_type erase(const K& key) {
    if(contains(key))
      erase_value(at(key));
    return _impl.erase(key);
  }

  value_iterator erase_value(const_value_iterator pos) {
    K& key = pos.second;
    value_iterator ret = _rev_impl.erase(pos);
    if(contains(key))
      erase(key);
    return ret;
  }

  value_iterator erase_value(const_value_iterator first,
                             const_value_iterator last) {
    for(auto it = first; it != last; it++)
      erase(it.second);
    return _rev_impl.erase(first, last);
  }

  size_type erase(const V& value) {
    if(contains_value(value))
      erase(key_at(value));
    return _rev_impl.erase(value);
  }

  moya::Vector<K> keys() const {
    moya::Vector<K> ret;

    for(const auto& it : *this)
      ret.push_back(it.first);

    return ret;
  }

  moya::Vector<V> values() const {
    moya::Vector<V> ret;

    for(const auto& it : *this)
      ret.push_back(it.second);

    return ret;
  }

  bool operator==(const HashMap<K, V>& c2) const {
    return _impl == c2.getImpl();
  }

  bool operator!=(const HashMap<K, V>& c2) const {
    return _impl != c2.getImpl();
  }

  bool operator<(const HashMap<K, V>& c2) const {
    return _impl < c2.getImpl();
  }

  bool operator<=(const HashMap<K, V>& c2) const {
    return _impl <= c2.getImpl();
  }

  bool operator>(const HashMap<K, V>& c2) const {
    return _impl > c2.getImpl();
  }

  bool operator>=(const HashMap<K, V>& c2) const {
    return _impl >= c2.getImpl();
  }

  std::string str() const {
    bool comma = false;
    std::stringstream ss;

    ss << "{";
    for(const auto& it : _impl) {
      if(comma)
        ss << ", ";
      ss << moya::str(it.first) << " : " << moya::str(it.second);
      comma = true;
    }
    ss << "}";

    return ss.str();
  }

  template <typename... ArgsT>
  void serialize(Serializer& s, ArgsT&&... args) const {
    s.mapStart();
    for(const auto& it : _impl)
      s.add(moya::str(it.first), it.second, args...);
    s.mapEnd();
  }

  void deserialize(Deserializer& d) {
    d.mapStart();
    while(d.mapEmpty()) {
      K key;
      V val;
      d.deserialize(key, val);
      _impl[key] = std::move(val);
    }
    d.mapEnd();
  }
};

} // namespace moya

#endif // MOYA_COMMON_HASHMAP2_H
