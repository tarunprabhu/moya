#ifndef MOYA_COMMON_CONFIG_H
#define MOYA_COMMON_CONFIG_H

#include "APITypes.h"
#include "Location.h"
#include "Set.h"
#include "Vector.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>

#include <string>

namespace moya {

namespace Config {

const unsigned DebugFnBefore = 0x1;
const unsigned DebugFnAfter = 0x2;

const char* getMoyaAPIPrefix();

// Actual Moya functions
const char* getEnterRegionFunctionName();
const char* getExitRegionFunctionName();
const char* getIsInRegionFunctionName();
const char* getCompileFunctionName();
const char* getExecuteCudaFunctionName();
const char* getBeginCallFunctionName();
// Argument registration functions
const char* getRegisterArgPtrFunctionName();
const char* getRegisterArgFunctionName(llvm::Type*);

// Moya stats recording functions
// const char* getStatsStartCallFunctionName();
// const char* getStatsStopCallFunctionName();
// const char* getStatsStartCompileFunctionName();
// const char* getStatsStopCompileFunctionName();
const char* getStatsStartExecuteFunctionName();
const char* getStatsStopExecuteFunctionName();

// Actual Moya error function
const char* getTopErrorFunctionName();

// Sentinel functions used during preprocessing/analysis
const char* getSentinelStartFunctionName();
const char* getSentinelStopFunctionName();

// Signatures for Moya functions. We only implement those that we actually
// need.
std::string getEnterRegionFunctionSignature();
std::string getExitRegionFunctionSignature();
std::string getSentinelStartFunctionSignature();
std::string getSentinelStopFunctionSignature();

bool isSentinelFunction(const llvm::Function*);
bool isSentinelFunction(const llvm::Function&);
bool isMoyaFunction(const llvm::Function&);

// Generated functions
bool isRedirecterFunction(const std::string&);

// Hidden getter functions to get the address of variables and
// functions at JIT-time
std::string generateGetterName(const llvm::GlobalValue&);

// Utilities used for ID generation
RegionID generateRegionID(const Location&);
JITID generateJITID(const std::string&, const Location&);
LoopID generateLoopID(const Location&);

std::string generateRedirecterName(const std::string&, const Location&);

// Global variables containing bitcode
const char* getBitcodeGlobalNamePrefix();
std::string getFileBitcodeFileName(const std::string&);
std::string getFileBitcodeGlobalName(const std::string&);

// Payload-related functions
uint64_t getPayloadMagicNumber();
std::string getPayloadRegionGlobalName();
std::string getPayloadFunctionGlobalName();
std::string getPickledSummaryFilename(RegionID, JITID);
std::string getPickledModuleFilename(RegionID, const llvm::Function&);
std::string getPayloadModuleSaveFile(RegionID, const llvm::Function&);

// ELF section config
const char* getELFBitcodeSectionName();

// Names of the environment variables
const char* getEnvVarDisabled();
const char* getEnvVarVerbose();
const char* getEnvVarSaveDir();
const char* getEnvVarCacheDir();
const char* getEnvNoAggrConstProp();
const char* getEnvVarWorkingDir();
const char* getEnvVarPayloadDir();
const char* getEnvVarDebugFns();
const char* getEnvVarDebugFnsImmediate();
const char* getEnvVarDebugStore();
const char* getEnvVarSaveOptReport();
const char* getEnvVarSaveSpecialized();
const char* getEnvVarErrorOnTop();

// Environment variables to control Moya's behaviour
bool getEnvDisabled();
bool getEnvVerbose();
bool isEnvErrorOnTop();
bool isEnvSaveEnabled();
bool isEnvCacheEnabled();
bool isEnvNoAggrConstProp();
bool isEnvUsePolly();
bool isEnvSaveCode();
bool isEnvSaveOptReport();
bool isEnvSaveStats();

// The save directory is where intermediate values are saved so they can be
// examined later. Unlike the working directory, these values are likely to
// persist across invocations of Moya (both at compile-time and runtime).
//
// - At compile-time, intermediate results will be written saved once a
//   valid directory has been specified.
//
// - At runtime, both a valid save directory AND additional environment
//   variables need to be set which indicate what to save.
//    MOYA_SAVE_CODE must be set for the LLVM IR of the JIT'ed code is to be
//    saved. MOYA_SAVE_STATS must be set for execution stats to be saved
//
std::string getEnvSaveDir();

// The cache directory is where the object files of the JIT'ed code are
// saved. These may be used in subsequent invocations of the application to
// avoid JIT'ing and to reuse JIT'ed code. If MOYA_CACHE_DIR is set to a valid
// directory, the object files will be written automatically
//
// WARNING: This feature is experimental.
//
std::string getEnvCacheDir();
moya::Set<std::string> getEnvDebugFns();
moya::Set<std::string> getEnvDebugFnsImmediate();
unsigned getEnvDebugStore();

// Miscellaneous stuff to do with environment variables
bool getEnvVarValueAsBool(const char*);

// Miscellaneous stuff
const char* getVersionString();
unsigned getVersionMajor();
unsigned getVersionMinor();
unsigned getVersionPatch();

// There's no real reason to ifdef this out.
const char* getCudaStreamStructName();

// ID related methods
IDKind getIDKind(MoyaID);

MoyaID getFirstValidMoyaID(IDKind);
MoyaID getNextMoyaID(MoyaID);

bool isInvalidJITID(JITID);
bool isInvalidRegionID(RegionID);
bool isInvalidLoopID(LoopID);

JITID getInvalidJITID();
RegionID getInvalidRegionID();
LoopID getInvalidLoopID();

FunctionSignatureTuning getUntunedFunctionSignature();

} // namespace Config

} // namespace moya

#endif // MOYA_COMMON_MOYA_CONFIG_H
