#include "Demangler.h"
#include "DemanglerCXX.h"
#include "DemanglerF77.h"
#include "DemanglerFort.h"
#include "DemanglerNull.h"
#include "Diagnostics.h"
#include "Metadata.h"

#include <stdlib.h>
#include <string.h>

namespace moya {

// Global demanglers. These are all constant anyway, so it doesn't really
// matter that they are global
static const DemanglerNull demanglerNull;
static const DemanglerCXX demanglerCXX;
static const DemanglerFort demanglerFort;
static const DemanglerF77 demanglerF77;

Demangler::Demangler(SourceLang lang) : lang(lang) {
  ;
}

SourceLang Demangler::getLang() const {
  return lang;
}

template <typename T>
static std::string demangleImpl(const T& obj, Demangler::Action action) {
  if(moya::Metadata::hasSourceLang(obj)) {
    SourceLang lang = moya::Metadata::getSourceLang(obj);
    switch(lang) {
    case SourceLang::CXX:
      return demanglerCXX.demangle(obj, action);
      break;
    case SourceLang::Fort:
      return demanglerFort.demangle(obj, action);
      break;
    case SourceLang::F77:
      return demanglerF77.demangle(obj, action);
      break;
    case SourceLang::C:
      return demanglerNull.demangle(obj, action);
      break;
    default:
      moya_error("Unimplemented demangler for language: " << moya::str(lang));
      break;
    }
  }
  return obj.getName().str();
}

std::string demangle(const llvm::Function& f, Demangler::Action action) {
  return demangleImpl(f, action);
}

std::string demangle(const llvm::Function* f, Demangler::Action action) {
  return demangleImpl(*f, action);
}

std::string demangle(const llvm::GlobalVariable& g, Demangler::Action action) {
  return demangleImpl(g, action);
}

std::string demangle(const llvm::GlobalVariable* g, Demangler::Action action) {
  return demangleImpl(*g, action);
}

} // namespace moya
