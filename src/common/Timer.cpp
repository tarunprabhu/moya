#include "Timer.h"

#include <cmath>

#include <iostream>

Timer::Timer() : used(false), state(Stopped), accum(0), total(0) {
  ;
}

Timer::NSecs Timer::now() {
  return std::chrono::time_point_cast<std::chrono::nanoseconds>(
             std::chrono::high_resolution_clock::now())
      .time_since_epoch()
      .count();
}

void Timer::start() {
  if(state == Stopped) {
    used = true;
    accum = 0;
    accum -= now();
    state = Running;
  }
}

Timer::NSecs Timer::stop() {
  NSecs duration = 0;
  if(state == Paused) {
    duration = accum;
  } else if(state == Running) {
    accum += now();
    duration = accum;
  }
  total += duration;
  accum = 0;
  state = Stopped;

  return duration;
}

Timer::NSecs Timer::pause() {
  if(state == Running) {
    accum += now();
    state = Paused;
    return accum;
  }
  return 0;
}

void Timer::resume() {
  if(state == Paused) {
    accum -= now();
    state = Running;
  }
}

void Timer::reset() {
  accum = 0;
  total = 0;
  state = Stopped;
}

bool Timer::isUsed() const {
  return used;
}

bool Timer::isPaused() const {
  return state == Paused;
}

bool Timer::isRunning() const {
  return state == Running;
}

Timer::Secs Timer::secs() const {
  return (double)total / pow(10, 9);
}

Timer::MSecs Timer::msecs() const {
  return (double)total / pow(10, 6);
}

Timer::USecs Timer::usecs() const {
  return (double)total / pow(10, 3);
}

Timer::NSecs Timer::nsecs() const {
  return total;
}
