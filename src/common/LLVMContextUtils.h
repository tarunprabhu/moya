#ifndef MOYA_COMMON_LLVMCONTEXT_UTILS_H
#define MOYA_COMMON_LLVMCONTEXT_UTILS_H

#include "APITypes.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Type.h>

namespace moya {

namespace LLVMUtils {

llvm::Type* getMoyaIDType(llvm::LLVMContext&);
llvm::Type* getJITIDType(llvm::LLVMContext&);
llvm::Constant* getJITIDConstant(llvm::LLVMContext&, JITID);
llvm::Type* getRegionIDType(llvm::LLVMContext&);
llvm::Constant* getRegionIDConstant(llvm::LLVMContext&, RegionID);
llvm::Type* getFunctionSignatureTuningType(llvm::LLVMContext&);
llvm::Type* getFunctionSignatureBasicType(llvm::LLVMContext&);

} // namespace LLVMUtils

} // namespace moya

#endif // MOYA_COMMON_LLVMCONTEXT_UTILS_H
