#include "Hexify.h"

#include <algorithm>
#include <sstream>

namespace moya {

template <typename T>
static std::string hexify(T n, bool prepend) {
  std::stringstream ss;
  unsigned len = 0;
  if(n) {
    for(; n; n >>= 4, len++)
      ss << std::hex << (n & 0xf);
  } else {
    len += 1;
    ss << 0;
  }
  if(prepend)
    for(unsigned i = len; i < sizeof(T) * 2; i++)
      ss << 0;

  std::string s = ss.str();
  std::reverse(s.begin(), s.end());

  return s;
}

std::string hex(uint64_t n, bool prepend) {
  return hexify(n, prepend);
}

std::string hex(uint32_t n, bool prepend) {
  return hexify(n, prepend);
}

std::string hex(uint16_t n, bool prepend) {
  return hexify(n, prepend);
}

std::string hex(uint8_t n, bool prepend) {
  return hexify(n, prepend);
}

} // namespace moya
