#include "JSONUtils.h"
#include "ContentAddress.h"
#include "LLVMUtils.h"
#include "Location.h"
#include "Map.h"
#include "Region.h"
#include "StringUtils.h"

namespace moya {

namespace JSON {

std::string escape(const std::string& s) {
  moya::Map<std::string, std::string> repl = {{"\n", "\\n"}, {"\"", "\\\""}};
  std::string t = StringUtils::strip(s);
  for(const auto& it : repl) {
    const std::string& find = it.first;
    const std::string& repl = it.second;
    if(t.find(find) != std::string::npos)
      t = StringUtils::replace(t, find, repl);
  }

  return t;
}

static std::string pointer(const std::string& target, const std::string& base) {
  std::stringstream ss;

  if(base.size())
    ss << base;
  ss << "/" << target;

  return ss.str();
}

std::string pointer(MoyaID id) {
  return pointer(moya::str(id), "/functions");
}

std::string pointer(llvm::Type* type) {
  return pointer(moya::str(type), "/types");
}

std::string pointer(const llvm::Function* f) {
  return pointer(moya::str(f), "/functions");
}

std::string pointer(const llvm::GlobalVariable* g) {
  return pointer(moya::str(g), "/globals");
}

std::string pointer(const llvm::BasicBlock* bb) {
  std::stringstream ss;
  ss << pointer(bb->getParent()) << "/blocks";
  return pointer(moya::str(bb), ss.str());
}

std::string pointer(const llvm::AllocaInst* alloca) {
  std::stringstream ss;
  ss << pointer(LLVMUtils::getFunction(alloca)) << "/locals";
  return pointer(moya::str(alloca), ss.str());
}

std::string pointer(const llvm::Instruction* inst, const llvm::Function* f) {
  if(not f)
    f = LLVMUtils::getFunction(inst);
  std::string istr = "0";
  if(inst)
    istr = moya::str(inst);
  std::stringstream ss;
  ss << pointer(f) << "/instructions";
  return pointer(istr, ss.str());
}

std::string pointer(const llvm::Argument* a) {
  std::stringstream ss;
  ss << "/functions/" << moya::str(LLVMUtils::getFunction(a)) << "/args";

  return pointer(moya::str(a->getArgNo()), ss.str());
}

std::string pointer(const ContentAddress* addr) {
  return pointer(moya::str(addr->getRaw()), "/store");
}

std::string pointer(const Region* region) {
  return pointer(moya::str(region->getID()), "/regions");
}

} // namespace JSON

} // namespace moya
