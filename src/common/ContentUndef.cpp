#include "ContentUndef.h"

#include <sstream>

using namespace llvm;

namespace moya {

ContentUndef::ContentUndef() : Content(Content::Kind::Undef) {
  ;
}

bool ContentUndef::classof(const Content* c) {
  return c->getKind() == Content::Kind::Undef;
}

std::string ContentUndef::str() const {
  std::stringstream ss;
  ss << "CUndef()";
  return ss.str();
}

void ContentUndef::serialize(Serializer& s) const {
  s.mapStart();
  s.add("_class", "Undef");
  s.mapEnd();
}

} // namespace moya
