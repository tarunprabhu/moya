#ifndef MOYA_COMMON_FORMATTING_H
#define MOYA_COMMON_FORMATTING_H

#include <string>

namespace moya {

unsigned indent1(unsigned = 0);
unsigned indent2(unsigned = 0);
unsigned indent3(unsigned = 0);
unsigned indent4(unsigned = 0);
unsigned indent5(unsigned = 0);

unsigned unindent(unsigned);

// Generates an indent when printing types
std::string space(unsigned);

std::string space1(unsigned = 0);
std::string space2(unsigned = 0);
std::string space3(unsigned = 0);
std::string space4(unsigned = 0);

} // namespace moya

#endif // MOYA_COMMON_FORMATTING_H
