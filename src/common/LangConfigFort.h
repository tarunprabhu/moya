#ifndef MOYA_COMMON_LANG_CONFIG_FORTRAN_H
#define MOYA_COMMON_LANG_CONFIG_FORTRAN_H

#include <string>

// This is a configuration class for Fortran-specific features
class LangConfigFort {
public:
  static unsigned getMaxDescriptorDimensions();
  static bool isValidSourceFile(const std::string&);
};

#endif // MOYA_COMMON_LANG_CONFIG_FORTRAN_H
