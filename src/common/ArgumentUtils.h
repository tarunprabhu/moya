#ifndef MOYA_COMMON_ARGUMENT_UTILS_H
#define MOYA_COMMON_ARGUMENT_UTILS_H

#include <llvm/IR/Function.h>

// These utilities deal with LLVM function arguments

namespace moya {

namespace LLVMUtils {

llvm::Type* getAnalysisType(const llvm::Argument*);
llvm::Type* getAnalysisType(const llvm::Argument&);

const llvm::Function* getFunction(const llvm::Argument*);
const llvm::Function* getFunction(const llvm::Argument&);
llvm::Function* getFunction(llvm::Argument*);
llvm::Function* getFunction(llvm::Argument&);

llvm::StringRef getFunctionName(const llvm::Argument*);
llvm::StringRef getFunctionName(const llvm::Argument&);

const llvm::Module* getModule(const llvm::Argument*);
const llvm::Module* getModule(const llvm::Argument&);
llvm::Module* getModule(llvm::Argument*);
llvm::Module* getModule(llvm::Argument&);

} // namespace LLVMUtils

} // namespace moya

#endif // MOYA_COMMON_ARGUMENT_UTILS_H
