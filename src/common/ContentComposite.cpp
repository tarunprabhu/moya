#include "ContentComposite.h"
#include "Formatting.h"

#include <sstream>

using namespace llvm;

namespace moya {

ContentComposite::ContentComposite(const Vector<Contents>& elems)
    : Content(Content::Kind::Composite), elems(elems) {
  ;
}

unsigned long ContentComposite::size() const {
  return elems.size();
}

void ContentComposite::insertAt(unsigned long i, const Content* c) {
  elems.at(i).insert(c);
}

void ContentComposite::insertAt(unsigned long i, const Contents& c) {
  elems.at(i).insert(c);
}

Contents ContentComposite::at(unsigned long i) const {
  return elems.at(i);
}

Contents ContentComposite::operator[](unsigned long i) const {
  return elems.at(i);
}

bool ContentComposite::classof(const Content* c) {
  return c->getKind() == Content::Kind::Composite;
}

std::string ContentComposite::str() const {
  bool comma = false;
  std::stringstream ss;

  ss << "CComposite(";
  for(unsigned i = 0; i < elems.size(); i++) {
    if(comma)
      ss << ", ";
    ss << "[" << i << "] => " << elems.at(i).str();
    comma = true;
  }
  ss << ")";

  return ss.str();
}

void ContentComposite::serialize(Serializer& s) const {
  s.mapStart();
  s.add("_class", "Composite");
  s.mapEnd();
}

} // namespace moya
