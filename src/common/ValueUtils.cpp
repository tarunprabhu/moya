#include "LLVMUtils.h"
#include "Metadata.h"

using namespace llvm;

namespace moya {

namespace LLVMUtils {

Module* getModule(Value* v) {
  if(auto* inst = dyn_cast<Instruction>(v))
    return getModule(inst);
  else if(auto* g = dyn_cast<GlobalValue>(v))
    return getModule(g);

  return nullptr;
}

const Module* getModule(const Value* v) {
  if(const auto* inst = dyn_cast<Instruction>(v))
    return getModule(inst);
  else if(const auto* g = dyn_cast<GlobalValue>(v))
    return getModule(g);

  return nullptr;
}

Function* getFunction(Value* v) {
  if(auto* inst = dyn_cast<Instruction>(v))
    return getFunction(inst);
  else if(auto* arg = dyn_cast<Argument>(v))
    return getFunction(arg);
  return nullptr;
}

const Function* getFunction(const Value* v) {
  if(const auto* inst = dyn_cast<Instruction>(v))
    return getFunction(inst);
  else if(const auto* arg = dyn_cast<Argument>(v))
    return getFunction(arg);
  return nullptr;
}

bool hasUsesInFunction(const Value* v, const Function* f) {
  for(const Use& u : v->uses())
    if(const Function* func = getFunction(u.getUser()))
      if(func == f)
        return true;
  return false;
}

bool hasUsesInFunction(const Value* v, const Function& f) {
  return hasUsesInFunction(v, &f);
}

bool hasUsesInFunction(const Value& v, const Function& f) {
  return hasUsesInFunction(&v, &f);
}

bool hasUsesInFunction(const Value& v, const Function* f) {
  return hasUsesInFunction(&v, f);
}

Type* getAnalysisType(const Value* v) {
  if(auto* g = dyn_cast<GlobalVariable>(v))
    return getAnalysisType(g);
  else if(auto* arg = dyn_cast<Argument>(v))
    return getAnalysisType(arg);
  else if(auto* inst = dyn_cast<Instruction>(v))
    return getAnalysisType(inst);
  else if(const Module* module = getModule(v))
    return getCanonicalType(v->getType(), *module);

  return v->getType();
}

Type* getAnalysisType(const Value& v) {
  return getAnalysisType(&v);
}

template <typename T>
T* getAnalysisTypeAs(const Value* v) {
  return dyn_cast<T>(getAnalysisType(v));
}

template <typename T>
T* getAnalysisTypeAs(const Value& v) {
  return dyn_cast<T>(getAnalysisType(v));
}

template PointerType* getAnalysisTypeAs(const Value* v);
template PointerType* getAnalysisTypeAs(const Value& v);

} // namespace LLVMUtils

} // namespace moya
