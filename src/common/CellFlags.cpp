#include "CellFlags.h"
#include "Diagnostics.h"
#include "Map.h"
#include "Serializer.h"

#include <sstream>
#include <string>

#include <llvm/Support/raw_ostream.h>

namespace moya {

static const Map<CellFlags::Flag, std::string> shortNames = {
    {CellFlags::Flag::Argument, "a"},
    {CellFlags::Flag::Code, "p"},
    {CellFlags::Flag::Constant, "c"},
    {CellFlags::Flag::Dummy, "x"},
    {CellFlags::Flag::Heap, "h"},
    {CellFlags::Flag::Global, "g"},
    {CellFlags::Flag::Implicit, "i"},
    {CellFlags::Flag::Initialized, "s"},
    {CellFlags::Flag::LibAlloc, "l"},
    {CellFlags::Flag::Start, "b"},
    {CellFlags::Flag::Stack, "k"},
    {CellFlags::Flag::Vtable, "v"},
};

static const Map<CellFlags::Flag, std::string> longNames = {
    {CellFlags::Flag::Argument, "Argument"},
    {CellFlags::Flag::Code, "Code"},
    {CellFlags::Flag::Constant, "Constant"},
    {CellFlags::Flag::Dummy, "Dummy"},
    {CellFlags::Flag::Global, "Global"},
    {CellFlags::Flag::Heap, "Heap"},
    {CellFlags::Flag::Implicit, "Implicit"},
    {CellFlags::Flag::Initialized, "Initialized"},
    {CellFlags::Flag::LibAlloc, "LibAlloc"},
    {CellFlags::Flag::Start, "Start"},
    {CellFlags::Flag::Stack, "Stack"},
    {CellFlags::Flag::Vtable, "Vtable"},
};

CellFlags::CellFlags() : flags(0) {
  ;
}

void CellFlags::init(std::initializer_list<Flag>&& flags) {
  for(const Flag& flag : flags)
    set(flag);
}

CellFlags& CellFlags::reset(Flag flag) {
  flags &= ~static_cast<uint64_t>(flag);
  return *this;
}

CellFlags& CellFlags::set(Flag flag) {
  flags |= static_cast<uint64_t>(flag);
  return *this;
}

bool CellFlags::test(Flag flag) const {
  return flags & static_cast<uint64_t>(flag);
}

CellFlags& CellFlags::operator|=(const CellFlags& other) {
  flags |= other.flags;
  return *this;
}

bool CellFlags::hasArgument() const {
  return test(Flag::Argument);
}

bool CellFlags::hasCode() const {
  return test(Flag::Code);
}

bool CellFlags::hasConstant() const {
  return test(Flag::Constant);
}

bool CellFlags::hasDummy() const {
  return test(Flag::Dummy);
}

bool CellFlags::hasGlobal() const {
  return test(Flag::Global);
}

bool CellFlags::hasHeap() const {
  return test(Flag::Heap);
}

bool CellFlags::hasImplicit() const {
  return test(Flag::Implicit);
}

bool CellFlags::hasInitialized() const {
  return test(Flag::Initialized);
}

bool CellFlags::hasLibAlloc() const {
  return test(Flag::LibAlloc);
}

bool CellFlags::hasStack() const {
  return test(Flag::Stack);
}

bool CellFlags::hasStart() const {
  return test(Flag::Start);
}

bool CellFlags::hasVtable() const {
  return test(Flag::Vtable);
}

std::string CellFlags::str() const {
  std::stringstream ss;
  bool pipe = false;

  for(uint64_t f = 0x1; f < static_cast<uint64_t>(Flag::_Invalid); f <<= 1) {
    auto flag = static_cast<Flag>(f);
    if(test(flag)) {
      if(pipe)
        ss << " | ";
      pipe = true;
      ss << longNames.at(flag);
    }
  }

  return ss.str();
}

void CellFlags::serialize(Serializer& s) const {
  s.arrStart();
  for(uint64_t f = 0x1; f < static_cast<uint64_t>(Flag::_Invalid); f <<= 1) {
    auto flag = static_cast<Flag>(f);
    if(test(flag)) {
      if(longNames.contains(flag))
        s.serialize(longNames.at(flag));
      else
        moya_error("Unsupported cell flag (" << f << "): serialize()");
    }
  }
  s.arrEnd();
}

} // namespace moya
