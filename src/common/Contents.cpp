#include "Contents.h"
#include "AnalysisStatus.h"
#include "Diagnostics.h"

namespace moya {

Contents::Contents()
    : contents(std::make_tuple(ContentsImpl<Content>(),
                               ContentsImpl<ContentAddress>(),
                               ContentsImpl<ContentCode>(),
                               ContentsImpl<ContentComposite>(),
                               ContentsImpl<ContentNullptr>(),
                               ContentsImpl<ContentRuntimePointer>(),
                               ContentsImpl<ContentRuntimeScalar>(),
                               ContentsImpl<ContentScalar>(),
                               ContentsImpl<ContentUndef>())) {
  ;
}

Contents::Contents(const Content* c) : Contents() {
  insertImpl(c);
}

Contents::Contents(const Contents& contents) : Contents() {
  for(const Content* c : contents)
    insertImpl(c);
}

Contents::Contents(Contents&& contents) : Contents() {
  for(const Content* c : contents)
    insertImpl(c);
}

template <typename C>
Contents::Contents(const ContentsImpl<C>& contents) : Contents() {
  for(const C* c : contents)
    insertImpl(c);
}

Contents& Contents::operator=(const Contents& other) {
  for(const Content* c : other)
    insertImpl(c);
  return *this;
}

Contents& Contents::operator=(Contents&& other) {
  for(const Content* c : other)
    insertImpl(c);
  return *this;
}

template <typename C>
ContentsImpl<C>& Contents::get() {
  return std::get<ContentsImpl<C>>(contents);
}

template <typename C>
const ContentsImpl<C>& Contents::get() const {
  return std::get<ContentsImpl<C>>(contents);
}

template <typename C>
const ContentsImpl<C>& Contents::getAs(const llvm::Instruction* inst) const {
  for(const Content* c : get<Content>())
    if(not llvm::isa<C>(c))
      moya_error("Expected " << typeid(c).name() << " object: "
                             << getDiagnostic(inst) << ". Got " << c->str());

  return get<C>();
}

template <>
const ContentsImpl<ContentAddress>&
Contents::getAs(const llvm::Instruction* inst) const {
  for(const Content* c : get<Content>())
    if(c->isAddress() or c->isNullptr() or c->isUndef())
      ;
    else if(c->isRuntimePointer())
      setTop(inst);
    else
      moya_error("Expected address object: " << getDiagnostic(inst) << ". Got "
                                             << c->str());

  return get<ContentAddress>();
}

bool Contents::insertImpl(const Content* c) {
  bool changed = false;

  changed |= get<Content>().insert(c);
  if(const auto* addr = llvm::dyn_cast<ContentAddress>(c))
    changed |= get<ContentAddress>().insert(addr);
  else if(const auto* code = llvm::dyn_cast<ContentCode>(c))
    changed |= get<ContentCode>().insert(code);
  else if(const auto* ccomp = llvm::dyn_cast<ContentComposite>(c))
    changed |= get<ContentComposite>().insert(ccomp);
  else if(const auto* null = llvm::dyn_cast<ContentNullptr>(c))
    changed |= get<ContentNullptr>().insert(null);
  else if(const auto* rp = llvm::dyn_cast<ContentRuntimePointer>(c))
    changed |= get<ContentRuntimePointer>().insert(rp);
  else if(const auto* rc = llvm::dyn_cast<ContentRuntimeScalar>(c))
    changed |= get<ContentRuntimeScalar>().insert(rc);
  else if(const auto* sc = llvm::dyn_cast<ContentScalar>(c))
    changed |= get<ContentScalar>().insert(sc);
  else if(const auto* undef = llvm::dyn_cast<ContentUndef>(c))
    changed |= get<ContentUndef>().insert(undef);
  else
    moya_error("Unsupported content kind to insert: " << c->getKind());

  return changed;
}

bool Contents::insert(const Contents& contents) {
  bool changed = false;
  for(const Content* c : contents)
    changed |= insertImpl(c);
  return changed;
}

bool Contents::insert(const Content* c) {
  return insertImpl(c);
}

template <typename C>
bool Contents::insert(const ContentsImpl<C>& contents) {
  bool changed = false;
  for(const C* c : contents)
    changed |= insertImpl(c);
  return changed;
}

Contents::iterator Contents::begin() {
  return get<Content>().begin();
}

Contents::const_iterator Contents::begin() const {
  return get<Content>().begin();
}

Contents::iterator Contents::end() {
  return get<Content>().end();
}

Contents::const_iterator Contents::end() const {
  return get<Content>().end();
}

bool Contents::empty() const {
  return get<Content>().empty();
}

size_t Contents::size() const {
  return get<Content>().size();
}

bool Contents::operator==(const Contents& other) const {
  return get<Content>() == other.get<Content>();
}

bool Contents::operator!=(const Contents& other) const {
  return get<Content>() != other.get<Content>();
}

bool Contents::operator<(const Contents& other) const {
  return get<Content>() < other.get<Content>();
}

bool Contents::operator<=(const Contents& other) const {
  return get<Content>() <= other.get<Content>();
}

bool Contents::operator>(const Contents& other) const {
  return get<Content>() > other.get<Content>();
}

bool Contents::operator>=(const Contents& other) const {
  return get<Content>() >= other.get<Content>();
}

bool Contents::disjoint(const Contents& other) const {
  return get<Content>().disjoint(other.get<Content>());
}

std::string Contents::str() const {
  std::stringstream ss;
  bool comma = false;

  ss << "{ ";
  for(const Content* c : get<Content>()) {
    if(comma)
      ss << ", ";
    ss << c->str();
    comma = true;
  }
  ss << " }";

  return ss.str();
}

Serializer& Contents::serialize(Serializer& s) const {
  s.arrStart();
  for(const Content* c : get<Content>())
    c->serialize(s);
  s.arrEnd();

  return s;
}

// Explicitly instantiate a function which accepts a const Content*
#define FUNC_1(RET, NAME, TYPE) template RET Contents::NAME(const TYPE*);

// Explicitly instantiate a function which accepts a ContentsImpl object
#define FUNC_2(RET, NAME, TYPE) \
  template RET Contents::NAME(const ContentsImpl<TYPE>&);

// Instantiate a function that returns a ContentsImpl object
#define FUNC_3(RET, NAME, TYPE) \
  template const ContentsImpl<TYPE>& Contents::NAME() const;

#define INSTANTIATE_IMPL(FN) FN;

#define INSTANTIATE(FN, RET, NAME)                       \
  INSTANTIATE_IMPL(FN(RET, NAME, Content))               \
  INSTANTIATE_IMPL(FN(RET, NAME, ContentAddress))        \
  INSTANTIATE_IMPL(FN(RET, NAME, ContentCode))           \
  INSTANTIATE_IMPL(FN(RET, NAME, ContentComposite))      \
  INSTANTIATE_IMPL(FN(RET, NAME, ContentNullptr))        \
  INSTANTIATE_IMPL(FN(RET, NAME, ContentRuntimePointer)) \
  INSTANTIATE_IMPL(FN(RET, NAME, ContentRuntimeScalar))  \
  INSTANTIATE_IMPL(FN(RET, NAME, ContentScalar))         \
  INSTANTIATE_IMPL(FN(RET, NAME, ContentUndef))

// Instantiate the functions we need
INSTANTIATE(FUNC_2, , Contents)
INSTANTIATE(FUNC_2, bool, insert)

// Because we have had to explicitly specialize getAs<ContentAddress>, we can't
// use the macros
template const ContentsImpl<Content>&
Contents::getAs(const llvm::Instruction*) const;
template const ContentsImpl<ContentCode>&
Contents::getAs(const llvm::Instruction*) const;
template const ContentsImpl<ContentComposite>&
Contents::getAs(const llvm::Instruction*) const;

} // namespace moya
