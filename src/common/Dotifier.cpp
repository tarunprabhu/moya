#include "Dotifier.h"
#include "Diagnostics.h"
#include "Formatting.h"

#include <llvm/Support/FileSystem.h>

#include <sstream>

namespace moya {

Dotifier::Dotifier() : poutf(nullptr), pouts(nullptr), directed(false) {
  ;
}

// llvm::raw_ostream& Dotifier::out() {
//   return *pout.get();
// }

// llvm::raw_fd_ostream& Dotifier::outf() {
//   return *llvm::dyn_cast<llvm::raw_fd_ostream>(pout.get());
// }

// llvm::raw_string_ostream& Dotifier::outs() {
//   return *llvm::dyn_cast<llvm::raw_string_ostream>(pout.get());
// }

llvm::raw_ostream& Dotifier::out() const {
  if(poutf)
    return outf();
  else if(pouts)
    return outs();

  moya_error("Dotifier not initialized");
}

llvm::raw_fd_ostream& Dotifier::outf() const {
  return *poutf;
}

llvm::raw_string_ostream& Dotifier::outs() const {
  return *pouts;
}

void Dotifier::resetf() {
  if(poutf)
    poutf->close();
  poutf.reset(nullptr);
}

void Dotifier::resets() {
  pouts.reset(nullptr);
  buf = "";
}

void Dotifier::initialize() {
  resetf();
  resets();
  pouts.reset(new llvm::raw_string_ostream(buf));
}

void Dotifier::initialize(const std::string& file) {
  resets();
  resetf();
  std::error_code ec;
  poutf.reset(
      new llvm::raw_fd_ostream(file, ec, llvm::sys::fs::OpenFlags::F_Text));
  if(ec)
    moya_error("Could not open dot file: " << file);
}

void Dotifier::finalize() {
  if(poutf)
    resetf();
  else if(pouts)
    resets();
  else
    moya_error("Dotifier not initialized");
}

bool Dotifier::isInitialized() const {
  if(poutf or pouts)
    return true;
  return false;
}

std::string Dotifier::quote(const std::string& s) const {
  std::stringstream ss;
  ss << "\"" << s << "\"";
  return ss.str();
}

std::string Dotifier::getSeparator() const {
  if(directed)
    return " -> ";
  else
    return " -- ";
}

void Dotifier::start(bool directed) {
  this->directed = directed;
  if(directed)
    out() << "digraph G {\n";
  else
    out() << "graph G {\n";
}

void Dotifier::end() {
  out() << "}";
}

void Dotifier::addProperties(const Map<std::string, std::string>& properties) {
  if(properties.size()) {
    out() << " [";
    bool comma = false;
    for(const auto& it : properties) {
      if(comma)
        out() << ", ";
      out() << it.first << "=" << quote(it.second);
      comma = true;
    }
    out() << "]";
  }
}

void Dotifier::addVertex(const std::string& v,
                         const Map<std::string, std::string>& properties) {
  out() << space1() << quote(v);
  addProperties(properties);
  out() << ";\n";
}

void Dotifier::addEdge(const std::string& src,
                       const std::string& dst,
                       const Map<std::string, std::string>& properties) {
  out() << space1() << quote(src) << getSeparator() << quote(dst);
  addProperties(properties);
  out() << ";\n";
}

static const std::string emptyStr = "";
const std::string& Dotifier::str() const {
  if(poutf)
    moya_warning("<writing to file>");
  else if(pouts)
    return outs().str();
  else
    moya_error("Dotifier not initialized");

  return emptyStr;
}

} // namespace moya
