#ifndef MOYA_COMMON_CONTENT_H
#define MOYA_COMMON_CONTENT_H

#include "Serializer.h"
#include "Set.h"

namespace moya {

class Content {
public:
  enum Kind {
    Address,
    Code,
    Composite,
    Nullptr,
    RuntimePointer,
    RuntimeScalar,
    Scalar,
    Undef,
  };

protected:
  Kind kind;

public:
  Content(Kind);
  virtual ~Content() = default;

  Kind getKind() const;

  bool isAddress() const;
  bool isCode() const;
  bool isComposite() const;
  bool isNullptr() const;
  bool isRuntimePointer() const;
  bool isRuntimeScalar() const;
  bool isScalar() const;
  bool isUndef() const;

  virtual std::string str() const = 0;
  virtual void serialize(Serializer&) const = 0;
};

} // namespace moya

#endif // MOYA_COMMON_CONTENT_H
