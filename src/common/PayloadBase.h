#ifndef MOYA_COMMON_PAYLOAD_BASE_H
#define MOYA_COMMON_PAYLOAD_BASE_H

#include <llvm/IR/Module.h>
#include <llvm/Support/raw_ostream.h>

#include <fstream>

namespace moya {

class SummaryBase;
class SummaryCellBase;

class PayloadBase {
private:
  std::ifstream inf;
  std::ofstream outf;

protected:
  void write(const std::string&, bool = true);

  template <typename T,
            typename std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
  void write(const T&);

  template <typename T,
            typename std::enable_if_t<std::is_enum<T>::value, int> = 0>
  void write(const T& val) {
    write(static_cast<uint64_t>(val));
  }

  template <typename T,
            typename std::enable_if_t<std::is_class<T>::value, int> = 0>
  void write(const T& obj) {
    obj.pickle(*this);
  }

protected:
  void read(std::string&);

  template <typename T,
            typename std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
  void read(T&);

  template <typename T,
            typename std::enable_if_t<std::is_enum<T>::value, int> = 0>
  void read(T& val) {
    uint64_t ival = 0;
    read(ival);
    val = static_cast<T>(ival);
  }

  template <typename T,
            typename std::enable_if_t<std::is_class<T>::value, int> = 0>
  void read(T& obj) {
    obj.unpickle(*this);
  }

protected:
  PayloadBase(const std::string&,
              std::ios_base::openmode = std::ios_base::openmode());

  std::ifstream& fin();
  std::ofstream& fout();

public:
  PayloadBase() = delete;
  PayloadBase(const PayloadBase&) = delete;
  PayloadBase(const PayloadBase&&) = delete;
  virtual ~PayloadBase();

  template <typename T>
  void pickle(const T& v) {
    write(v);
  }

  template <typename T>
  T unpickle() {
    T val;
    read(val);
    return val;
  }
};

} // namespace moya

#endif // MOYA_COMMON_PAYLOAD_BASE_H
