#include "ContentScalar.h"
#include "Diagnostics.h"

#include <llvm/Support/raw_ostream.h>

using llvm::dyn_cast;

namespace moya {

ContentScalar::ContentScalar(const llvm::Constant* val)
    : Content(Content::Kind::Scalar), val(val) {
  ;
}

const llvm::Constant* ContentScalar::getValue() const {
  return val;
}

llvm::Type* ContentScalar::getType() const {
  return val->getType();
}

llvm::LLVMContext& ContentScalar::getContext() const {
  return val->getContext();
}

double ContentScalar::getFloatingPointValue() const {
  if(const auto* cfp = dyn_cast<llvm::ConstantFP>(val))
    return cfp->getValueAPF().convertToDouble();

  moya_error("Invalid floating point value: " << *val);
  return 0.0;
}

long ContentScalar::getIntegerValue() const {
  if(const auto* cint = dyn_cast<llvm::ConstantInt>(val))
    return cint->getLimitedValue();

  moya_error("Invalid integer value: " << *val);
  return 0;
}

bool ContentScalar::classof(const Content* c) {
  return c->getKind() == Content::Kind::Scalar;
}

std::string ContentScalar::str() const {
  std::string s;
  llvm::raw_string_ostream ss(s);

  ss << "CScalar(" << *val << ")";

  return ss.str();
}

void ContentScalar::serialize(Serializer& s) const {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  s.mapStart();
  s.add("_class", "Scalar");
  if(const auto* cint = dyn_cast<llvm::ConstantInt>(val))
    cint->getValue().print(ss, true);
  else if(const auto* cfp = dyn_cast<llvm::ConstantFP>(val))
    cfp->getValueAPF().print(ss);
  s.add("value", ss.str(), false);
  s.mapEnd();
}

} // namespace moya
