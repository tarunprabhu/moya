#include <llvm/Pass.h>

#include "common/DataLayout.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/Set.h"

using namespace llvm;

class PropagateAnalysisTypesPass : public FunctionPass {
protected:
  JITID fid;
  moya::DataLayout layout;

protected:
  Type* getLoadedType(Type*);
  int64_t getAlignedOffset(StructType*, int64_t, int64_t);
  moya::Set<Type*> getAlignedTypes(StructType*, uint64_t);

  bool allUsesLoads(Value*);
  bool allUsesCasts(Value*);

  bool propagate(GetElementPtrInst*, StructType*);
  bool propagate(GetElementPtrInst*, ArrayType*);

  bool propagate(LoadInst*, Value*, Type*);
  bool propagate(GetElementPtrInst*, Value*, Type*);
  bool propagate(CastInst*, Value*, Type*);
  bool propagate(PHINode*, Value*, Type*);
  bool propagate(Value*, Value*, Type*);

public:
  explicit PropagateAnalysisTypesPass(JITID);

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnFunction(Function&);

public:
  static char ID;
};

PropagateAnalysisTypesPass::PropagateAnalysisTypesPass(JITID fid)
    : FunctionPass(ID), fid(fid) {
  ;
}

StringRef PropagateAnalysisTypesPass::getPassName() const {
  return "Moya Propagate Analysis Types (Talyn)";
}

void PropagateAnalysisTypesPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool PropagateAnalysisTypesPass::allUsesLoads(Value* v) {
  for(Use& u : v->uses())
    if(not isa<LoadInst>(u.getUser()))
      return false;
  return true;
}

bool PropagateAnalysisTypesPass::allUsesCasts(Value* v) {
  for(Use& u : v->uses())
    if(not isa<CastInst>(u.getUser()))
      return false;
  return true;
}

// A GEP instruction could have an offset that is computed into the "middle"
// of an array contained within a struct. This can happen, for instance,
// when the struct is cast to an array of bytes and the offset computed into
// the array of bytes. For the analysis, we need to readjust the offset so
// that it points to the start of the array.
//
// gOffset: Offset from the start of the outermost struct
//          (may not necessarily be sty because this is a recursive function)
// currOffset: Offset within sty.
int64_t PropagateAnalysisTypesPass::getAlignedOffset(StructType* sty,
                                                     int64_t gOffset,
                                                     int64_t currOffset) {
  const moya::StructLayout& sl = layout.getStructLayout(sty);
  unsigned idx = sl.getElementContainingOffset(currOffset);
  int64_t elemOffset = sl.getElementOffset(idx);
  Type* elem = sty->getElementType(idx);
  if(elemOffset == currOffset)
    // This will only be called when we are in a GEP with constant offsets,
    // so we don't have to worry about a mismatch in the number of indices
    // and the type contained in the array
    return gOffset + currOffset;

  if(auto* aty = dyn_cast<ArrayType>(elem)) {
    ArrayType* flattened = moya::LLVMUtils::getFlattened(aty);

    // We are in the middle of an array. If this is an array of structs, we
    // may be looking at the element of a struct
    if(auto* ety = dyn_cast<StructType>(flattened->getElementType()))
      return getAlignedOffset(
          ety, gOffset + elemOffset, currOffset - elemOffset);
    else
      // Since this is Fortran coming from Flang, we will never have a situation
      // where we have non-trunctated arrays, so we just return the offset
      // to the first element of the array
      return gOffset + elemOffset;
  } else if(auto* ety = dyn_cast<StructType>(elem)) {
    return getAlignedOffset(ety, gOffset + elemOffset, currOffset - elemOffset);
  } else if(moya::LLVMUtils::isScalarTy(elem)) {
    // For some reason, we could still be looking in the "middle" of some
    // primitive type. For instance, the programmer could have packed two
    // 32-bit ints inside a 64-bit int and is trying to get at the second one.
    return gOffset + elemOffset;
  }

  return 0;
}

// <offset> is the offset inside the struct
// If the offset does not line up with the start of the idx'th element,
// it may be that the element is a struct and we will find something that
// aligns inside the struct. Then we recursively look inside it
//
moya::Set<Type*> PropagateAnalysisTypesPass::getAlignedTypes(StructType* sty,
                                                             uint64_t offset) {
  const moya::StructLayout& sl = layout.getStructLayout(sty);
  unsigned idx = sl.getElementContainingOffset(offset);
  Type* elem = sty->getElementType(idx);
  if(sl.getElementOffset(idx) == offset)
    // We are looking for the type that will be loaded, but the argument to
    // getLoadableTypes() must be a pointer type
    return moya::LLVMUtils::getLoadableTypes(elem->getPointerTo());

  if(auto* aty = dyn_cast<ArrayType>(elem)) {
    // This will only be called when we are in a GEP with constant offsets,
    // so we don't have to worry about a mismatch in the number of indices
    // and the type contained in the array
    ArrayType* flattened = moya::LLVMUtils::getFlattened(aty);

    // We are in the middle of an array. If this is an array of structs, we
    // may be looking at the element of a struct
    if(auto* ety = dyn_cast<StructType>(flattened->getElementType()))
      return getAlignedTypes(ety, offset - sl.getElementOffset(idx));

    // At this point, we will surely be returning the element of the array
    return {flattened->getElementType()};
  } else if(auto* ety = dyn_cast<StructType>(elem)) {
    return getAlignedTypes(ety, offset - sl.getElementOffset(idx));
  } else if(moya::LLVMUtils::isScalarTy(elem)) {
    // For some reason, we could still be looking in the "middle" of some
    // primitive type. For instance, the programmer could have packed two
    // 32-bit ints inside a 64-bit int and is trying to get at the second one.
    return {elem};
  }

  return {};
}

Type* PropagateAnalysisTypesPass::getLoadedType(Type* loaded) {
  if(auto* sty = dyn_cast<StructType>(loaded))
    return getLoadedType(sty->getElementType(0));
  else if(auto* aty = dyn_cast<ArrayType>(loaded))
    return getLoadedType(moya::LLVMUtils::getFlattenedElementType(aty));

  return loaded;
}

bool PropagateAnalysisTypesPass::propagate(LoadInst* load,
                                           Value* src,
                                           Type* type) {
  bool changed = false;

  if(auto* pty = dyn_cast<PointerType>(type)) {
    // Becuase this is Fortran, we will not ever load a struct or an array
    // We will only ever load pointers and primitive types. At this point,
    // if the element type of the pointer is a struct, then we are loading
    // the first element of the struct, if it is an array, we are looking
    // at the array element
    Type* loaded = getLoadedType(pty->getElementType());

    changed |= moya::Metadata::setAnalysisType(load, loaded);
    for(Use& u : load->uses())
      changed |= propagate(u.getUser(), load, loaded);
  } else {
    moya_error("Expected pointer type passed to load:\n"
               << "  load: " << *load << "\n"
               << "  type: " << *type);
  }

  return changed;
}

bool PropagateAnalysisTypesPass::propagate(GetElementPtrInst* gep,
                                           StructType* sty) {
  bool changed = false;
  int64_t offset = layout.getOffsetInGEP(gep);

  // FIXME: This is not exactly right. The rest of this code assumes that
  // the offset of the GEP is either a field or array element lookup.
  // But in the latter case, it also includes an adjustment made for the
  // bounds of the array. Specifically. instead of adjusting the indices when
  // performing an array lookup, the offset will be adjusted so the indices
  // can be used "as is". This means the offset could be negative (before the
  // start of the struct) or greater than sizeof(struct).

  if(offset > 0) {
    moya::Set<Type*> types = getAlignedTypes(sty, offset);

    // We may not not be able to find a unique type at the offset
    // because we may have arrays of structs and it may get difficult
    // to figure out whether we are looking at an element of the
    // $i_{th}$ struct in the array or the struct itself. The only
    // way we can resolve this is to look ahead and see what is being
    // done with the result of the instruction
    //
    if(types.size() == 1) {
      // The aligned types will be the actual types present, but
      // obviously the type to be propagated will be a pointer to that
      // type
      //
      Type* next = (*types.begin())->getPointerTo();
      for(Use& u : gep->uses())
        changed |= propagate(u.getUser(), gep, next);
      changed |= moya::Metadata::setAnalysisType(gep, next);
    } else if(types.size() == 0) {
      moya_error("Could not get next type of gep:\n"
                 << "  inst: " << *gep << "\n"
                 << "  type: " << *sty);
    } else {
      // // Try and limit the number of types this could be by looking
      // // ahead.
      // if(allUsesLoads(gep)) {
      //   // Because this comes from Fortran, if the only uses of
      //   // this instruction are other loads, it cannot be a struct.
      // } else if(allUsesCasts(gep)) {
      //   // If all the uses are casts,
      // }

      llvm::errs() << "USES: " << *gep << "\n";
      for(Use& u : gep->uses())
        llvm::errs() << "    : " << *u.getUser() << "\n";

      if(types.size() > 0) {
        std::string buf;
        raw_string_ostream ss(buf);
        for(Type* t : types)
          ss << "        " << *t << "\n";
        moya_warning("Could not determine unique analysis types\n"
                     << "  inst: " << *gep << "\n"
                     << ss.str());
      }
    }

    changed
        |= moya::Metadata::setGEPIndex(gep, getAlignedOffset(sty, 0, offset));
  } else if(offset == 0) {
    moya_error("Unexpected zero offset in struct type\n"
               << "  inst: " << *gep << "\n"
               << "  type: " << *sty);
  } else if(offset < 0) {
    moya_error("Unexpected negative offset in struct type\n"
               << "  inst: " << *gep << "\n"
               << "  type: " << *sty);
  }

  return changed;
}

bool PropagateAnalysisTypesPass::propagate(GetElementPtrInst* gep,
                                           ArrayType* aty) {
  bool changed = false;
  Type* ety = aty->getElementType();

  if(not moya::LLVMUtils::isScalarTy(ety))
    moya_error("Expected scalar element type in array passed to GEP:\n"
               << "  inst: " << *gep << "\n"
               << "  elem: " << *ety);

  // FIXME: This is not exactly right.
  // The offset also includes an adjustment made for the
  // bounds of the array. Specifically. instead of adjusting the indices when
  // performing an array lookup, the offset will be adjusted so the indices
  // can be used "as is". This means the offset could be negative (before the
  // start of the array) or greater than sizeof(array).

  changed |= moya::Metadata::setAnalysisType(gep, ety->getPointerTo());
  changed |= moya::Metadata::setGEPIndex(gep, 0);
  for(Use& u : gep->uses())
    changed |= propagate(u.getUser(), gep, ety->getPointerTo());

  return changed;
}

bool PropagateAnalysisTypesPass::propagate(GetElementPtrInst* gep,
                                           Value* src,
                                           Type* type) {
  bool changed = false;

  if(gep->getPointerOperand() != src)
    return changed;

  if(auto* pty = dyn_cast<PointerType>(type)) {
    if(gep->hasAllConstantIndices()) {
      Type* ety = pty->getElementType();
      if(auto* sty = dyn_cast<StructType>(ety)) {
        changed |= propagate(gep, sty);
      } else if(auto* aty = dyn_cast<ArrayType>(ety)) {
        changed |= propagate(gep, aty);
      } else if(moya::LLVMUtils::isScalarTy(ety)) {
        changed |= moya::Metadata::setGEPIndex(gep, 0);
        changed |= moya::Metadata::setAnalysisType(gep, ety->getPointerTo());
        for(Use& u : gep->uses())
          changed |= propagate(u.getUser(), gep, ety->getPointerTo());
      } else {
        moya_error("Unexpected pointer type in GEP:\n"
                   << "  inst: " << *gep << "\n"
                   << "  type: " << *type);
      }
    } else {
      changed |= moya::Metadata::setGEPIndex(gep, 0);
      changed |= moya::Metadata::setAnalysisType(gep, pty);
      for(Use& u : gep->uses())
        changed |= propagate(u.getUser(), gep, pty);
    }
  } else if(auto* sty = dyn_cast<StructType>(type)) {
    moya_error("Unimplemented struct type passed to gep:\n"
               << "   gep: " << *gep << "\n"
               << "  type: " << *type);
  } else {
    moya_error("Expected pointer type passed to gep:\n"
               << "   gep: " << *gep << "\n"
               << "  type: " << *type);
  }

  return changed;
}

bool PropagateAnalysisTypesPass::propagate(CastInst* cst,
                                           Value* src,
                                           Type* type) {
  bool changed = false;

  // Since this Fortran coming from Flang, if an i8* is cast to an i8**, then
  // it is a prelude to loading the first element of the struct which is
  // a pointer
  Type* dstTy = type;
  if(auto* pty = dyn_cast<PointerType>(type)) {
    if((moya::LLVMUtils::getPointerDepth(type) == 1)
       and (moya::LLVMUtils::getPointerDepth(cst->getDestTy()) == 2)) {
      if(auto* sty = dyn_cast<StructType>(pty->getElementType())) {
        if(isa<PointerType>(sty->getElementType(0)))
          dstTy = sty->getElementType(0)->getPointerTo();
        else
          moya_error("Expected pointer type in cast:\n"
                     << "  inst: " << *cst << "\n"
                     << "  type: " << *type);
      } else {
        moya_error("Expected struct type in cast:\n"
                   << "  inst: " << *cst << "\n"
                   << "  type: " << *type);
      }
    }
  }

  changed |= moya::Metadata::setAnalysisType(cst, dstTy);
  for(Use& u : cst->uses())
    changed |= propagate(u.getUser(), cst, dstTy);

  return changed;
}

bool PropagateAnalysisTypesPass::propagate(PHINode* phi,
                                           Value* src,
                                           Type* type) {
  bool changed = false;

  // All the incoming values should have the same analysis type. But they
  // may not have been propagated yet. There could be a use cycle here because
  // the phi node may get used by the instruction that will be one of the
  // incoming values of this node
  //
  if(not moya::Metadata::hasAnalysisType(phi)) {
    changed |= moya::Metadata::setAnalysisType(phi, type);
  } else {
    if(moya::Metadata::getAnalysisType(phi) != type)
      moya_error("Inconsistent propagated types\n"
                 << "  inst: " << *phi << "\n"
                 << "   old: " << *moya::Metadata::getAnalysisType(phi) << "\n"
                 << "   new: " << *type);
  }

  for(Use& u : phi->uses())
    changed |= propagate(u.getUser(), phi, type);

  return changed;
}

bool PropagateAnalysisTypesPass::propagate(Value* user,
                                           Value* src,
                                           Type* type) {
  bool changed = false;

  if(auto* load = dyn_cast<LoadInst>(user))
    return propagate(load, src, type);
  else if(auto* gep = dyn_cast<GetElementPtrInst>(user))
    return propagate(gep, src, type);
  else if(auto* cst = dyn_cast<CastInst>(user))
    return propagate(cst, src, type);
  else if(auto* phi = dyn_cast<PHINode>(user))
    if(not moya::Metadata::hasAnalysisType(phi))
      return propagate(phi, src, type);

  return changed;
}

bool PropagateAnalysisTypesPass::runOnFunction(Function& f) {
  moya_message(getPassName());

  bool changed = false;

  if(moya::Metadata::getJITID(f) != fid)
    return changed;

  Module& module = *moya::LLVMUtils::getModule(f);
  layout.reset(module);

  // for(StringRef sname :
  //     {"struct.t_region", "struct.t_grid", "struct.t_mixt_input"}) {
  //   if(StructType* sty = module.getTypeByName(sname)) {
  //     const StructLayout& sl = *module.getDataLayout().getStructLayout(sty);
  //     llvm::errs() << sname << ":\n";
  //     for(unsigned i = 0; i < sty->getNumElements(); i++)
  //       llvm::errs() << "  " << i << ": " << sl.getElementOffset(i) << "\n"
  //                    << "      : " << *sty->getElementType(i) << "\n";
  //   }
  //   llvm::errs() << "\n";
  // }

  for(Argument& arg : f.args())
    if(moya::Metadata::hasAnalysisType(arg))
      for(Use& u : arg.uses())
        changed |= propagate(
            u.getUser(), &arg, moya::Metadata::getAnalysisType(arg));

  for(GlobalVariable& g : module.globals())
    if(not g.isConstant())
      if(moya::Metadata::hasAnalysisType(g))
        for(Use& u : g.uses())
          changed
              |= propagate(u.getUser(), &g, moya::Metadata::getAnalysisType(g));

  return false;
}

char PropagateAnalysisTypesPass::ID = 0;

Pass* createPropagateAnalysisTypesPass(JITID fid) {
  return new PropagateAnalysisTypesPass(fid);
}
