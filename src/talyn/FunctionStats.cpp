#include "FunctionStats.h"
#include "common/Config.h"
#include "common/Stringify.h"

#ifdef ENABLED_PAPI
#include <papi.h>
#endif // ENABLED_PAPI

namespace moya {

FunctionStats::FunctionStats(const std::string& fname,
                             const Vector<std::string>& papiEvents)
    : name(fname), papiEvents(papiEvents), recording(false) {
  for(unsigned i = 0; i < papiEvents.size(); i++)
    counters.push_back(0);
  // This will be used when Moya is turned off at runtime
  variants[0].emplace(0, Variant(Config::getUntunedFunctionSignature(), ""));
}

const std::string& FunctionStats::getName() const {
  return name;
}

void FunctionStats::addVariant(FunctionSignatureBasic sig,
                               FunctionSignatureTuning variant,
                               const std::string& variantStr) {
  variants[sig].emplace(variant, Variant(variant, variantStr));
}

void FunctionStats::startCall() {
  call.start();
}

void FunctionStats::stopCall() {
  call.stop();
}

void FunctionStats::startLoad(FunctionSignatureBasic, FunctionSignatureTuning) {
  load.start();
}

void FunctionStats::stopLoad(FunctionSignatureBasic, FunctionSignatureTuning) {
  load.stop();
}

void FunctionStats::startOptimize(FunctionSignatureBasic,
                                  FunctionSignatureTuning) {
  optimize.start();
}

void FunctionStats::stopOptimize(FunctionSignatureBasic,
                                 FunctionSignatureTuning) {
  optimize.stop();
}

void FunctionStats::startCodegen(FunctionSignatureBasic,
                                 FunctionSignatureTuning) {
  codegen.start();
}

void FunctionStats::stopCodegen(FunctionSignatureBasic,
                                FunctionSignatureTuning) {
  codegen.stop();
}

void FunctionStats::startExecute(FunctionSignatureBasic sig,
                                 FunctionSignatureTuning variant,
                                 const long long* counters) {
  startRecording(sig, variant, counters);
  variants.at(sig).at(variant).calls += 1;
  recording = true;
}

void FunctionStats::stopExecute(FunctionSignatureBasic sig,
                                FunctionSignatureTuning variant,
                                const long long* counters) {
  recording = false;
  Timer::NSecs exec = stopRecording(sig, variant, counters);

  // Note the variable is a reference
  Timer::NSecs& minExecute = variants.at(sig).at(variant).minExecute;
  Timer::NSecs& maxExecute = variants.at(sig).at(variant).maxExecute;

  minExecute = std::min(minExecute, exec);
  maxExecute = std::max(maxExecute, exec);
}

void FunctionStats::startRecording(const long long* counters) {
  for(unsigned i = 0; i < this->counters.size(); i++)
    this->counters[i] -= counters[i];
}

void FunctionStats::stopRecording(const long long* counters) {
  for(unsigned i = 0; i < this->counters.size(); i++)
    this->counters[i] += counters[i];
}

void FunctionStats::startRecording(FunctionSignatureBasic sig,
                                   FunctionSignatureTuning variant,
                                   const long long* counters) {
  // FIXME: If this is a recursive function, restarting the timer here will
  // cause all sorts of havoc
  getVariant(sig, variant).execute.start();
  startRecording(counters);
}

Timer::NSecs FunctionStats::stopRecording(FunctionSignatureBasic sig,
                                          FunctionSignatureTuning variant,
                                          const long long* counters) {
  stopRecording(counters);
  return getVariant(sig, variant).execute.stop();
}

void FunctionStats::pauseRecording(FunctionSignatureBasic,
                                   const long long* counters) {
  stopRecording(counters);
  for(auto& i : variants.values())
    for(Variant& variant : i.values())
      if(variant.execute.isRunning())
        variant.execute.pause();
}

void FunctionStats::resumeRecording(FunctionSignatureBasic,
                                    const long long* counters) {
  for(auto& i : variants.values())
    for(Variant& variant : i.values())
      if(variant.execute.isPaused())
        variant.execute.resume();
  startRecording(counters);
}

bool FunctionStats::isRecording() const {
  return recording;
}

bool FunctionStats::isExecuted() const {
  for(const auto& i : variants)
    for(const auto& j : i.second)
      if(j.second.execute.isUsed())
        return true;
  return false;
}

FunctionSignatureTuning
FunctionStats::getBestVariant(FunctionSignatureBasic sig) const {
  Timer::NSecs bestTime = std::numeric_limits<Timer::NSecs>::max();
  FunctionSignatureTuning bestSig = 0;
  for(const Variant& v : variants.at(sig).values()) {
    if(v.minExecute < bestTime) {
      bestTime = v.minExecute;
      bestSig = v.sig;
    }
  }

  return bestSig;
}

unsigned FunctionStats::getNumSpecialized() const {
  return variants.size();
}

unsigned FunctionStats::getNumVariants(FunctionSignatureBasic sig) const {
  if(variants.contains(sig))
    return variants.at(sig).size();
  return 0;
}

unsigned long FunctionStats::getNumCalls(FunctionSignatureBasic sig) const {
  unsigned long calls = 0;
  if(variants.contains(sig))
    for(const auto& i : variants.at(sig))
      calls += i.second.calls;
  return calls;
}

unsigned long
FunctionStats::getNumCalls(FunctionSignatureBasic sig,
                           FunctionSignatureTuning variant) const {
  if(variants.contains(sig) and variants.at(sig).contains(variant))
    return variants.at(sig).at(variant).calls;
  return 0;
}

FunctionStats::Variant&
FunctionStats::getVariant(FunctionSignatureBasic sig,
                          FunctionSignatureTuning variant) {
  return variants.at(sig).at(variant);
}

Timer::NSecs FunctionStats::getBestTime(FunctionSignatureBasic sig,
                                        FunctionSignatureTuning variant) const {
  if(variants.contains(sig) and variants.at(sig).contains(variant))
    return variants.at(sig).at(variant).minExecute;
  return 0;
}

template <typename T>
static T mean(T total, unsigned long count) {
  return (T)ceil((double)total / count);
}

Serializer& FunctionStats::serialize(Serializer& s) const {
  // The total number of calls to all specialized versions of the function
  // across all tuned variants
  unsigned long calls = 0;
  Timer::NSecs execute = 0;
  Timer::NSecs minExecute = std::numeric_limits<Timer::NSecs>::max();
  Timer::NSecs maxExecute = 0;
  for(auto i : variants) {
    const Map<FunctionSignatureTuning, Variant>& specialized = i.second;
    for(const auto& j : specialized) {
      const Variant& stats = j.second;
      calls += stats.calls;
      execute += stats.execute.nsecs();
      minExecute = std::min(minExecute, stats.minExecute);
      maxExecute = std::max(maxExecute, stats.maxExecute);
    }
  }

  s.mapStart();

  s.add("Total execute", execute);
  s.add("Mean execute", mean(execute, calls));
  s.add("Min execute", minExecute);
  s.add("Max execute", maxExecute);
  s.add("Total calls", calls);

  s.mapStart("Overheads");
  s.add("Call", call.nsecs());
  s.add("Load", load.nsecs());
  s.add("Optimize", optimize.nsecs());
  s.add("Codegen", codegen.nsecs());
  s.add("Compile", optimize.nsecs() + codegen.nsecs());
  s.add("Total",
        call.nsecs() + load.nsecs() + optimize.nsecs() + codegen.nsecs());
  s.mapEnd("Overheads");

  s.add("Specialized",
        std::count_if(variants.begin(), variants.end(), [&](const auto& p) {
          if(this->getNumCalls(p.first))
            return true;
          return false;
        }));
  s.mapStart("Calls");
  for(const auto& i : variants) {
    FunctionSignatureBasic sig = i.first;
    if(getNumCalls(sig))
      s.add(str(sig), getNumCalls(sig));
  }
  s.mapEnd("Calls");

  s.mapStart("Tuned");
  for(const auto& i : variants) {
    FunctionSignatureBasic sig = i.first;
    const Map<FunctionSignatureTuning, Variant>& variants = i.second;
    if(sig) {
      s.mapStart(str(sig));
      s.add("Best", getBestVariant(sig));
      for(const Variant& v : variants.values()) {
        if(v.calls) {
          s.mapStart(str(v.sig));
          s.add("Params", v.params);
          s.add("Calls", v.calls);
          s.add("Total Execute", v.execute.nsecs());
          s.add("Mean execute", mean(v.execute.nsecs(), v.calls));
          s.add("Min execute", v.minExecute);
          s.add("Max execute", v.maxExecute);
          s.mapEnd(str(v.sig));
        }
      }
      s.mapEnd(str(sig));
    }
  }
  s.mapEnd("Tuned");

  for(unsigned i = 0; i < papiEvents.size(); i++)
    s.add(papiEvents.at(i), counters.at(i));

  s.mapEnd();

  return s;
}

} // namespace moya
