#include "RegionStats.h"

namespace moya {

RegionStats::RegionStats(const Region& region,
                         const JITContext& jitContext,
                         const Vector<std::string>& papiEvents)
    : region(region) {
  for(JITID key : jitContext.getJITIDs())
    funcs.emplace(key, FunctionStats(jitContext.getUserName(key), papiEvents));
}

const Region& RegionStats::getRegion() const {
  return region;
}

void RegionStats::addVariant(JITID key,
                             FunctionSignatureBasic sig,
                             FunctionSignatureTuning variant,
                             const std::string& variantStr) {
  funcs.at(key).addVariant(sig, variant, variantStr);
}

void RegionStats::startTotal() {
  total.start();
}

void RegionStats::stopTotal() {
  total.stop();
}

void RegionStats::startCall(JITID key) {
  funcs.at(key).startCall();
}

void RegionStats::stopCall(JITID key) {
  funcs.at(key).stopCall();
}

void RegionStats::startCompile(JITID,
                               FunctionSignatureBasic sig,
                               const long long* counters) {
  // We don't want to measure the compilation time when measuring the time
  // spent executing the function. This can happen when a function being that is
  // JIT'ted itself calls functions that are JIT'ted. To get around this, we
  // turn off the timer and the counters as soon as we start compiling
  for(FunctionStats& f : funcs.values())
    if(f.isRecording())
      f.pauseRecording(sig, counters);
}

void RegionStats::stopCompile(JITID,
                              FunctionSignatureBasic sig,
                              const long long* counters) {
  for(FunctionStats& f : funcs.values())
    if(f.isRecording())
      f.resumeRecording(sig, counters);
}

void RegionStats::startLoad(JITID key,
                            FunctionSignatureBasic sig,
                            FunctionSignatureTuning variant) {
  funcs.at(key).startLoad(sig, variant);
}

void RegionStats::stopLoad(JITID key,
                           FunctionSignatureBasic sig,
                           FunctionSignatureTuning variant) {
  funcs.at(key).stopLoad(sig, variant);
}

void RegionStats::startOptimize(JITID key,
                                FunctionSignatureBasic sig,
                                FunctionSignatureTuning variant) {
  funcs.at(key).startOptimize(sig, variant);
}

void RegionStats::stopOptimize(JITID key,
                               FunctionSignatureBasic sig,
                               FunctionSignatureTuning variant) {
  funcs.at(key).stopOptimize(sig, variant);
}

void RegionStats::startCodegen(JITID key,
                               FunctionSignatureBasic sig,
                               FunctionSignatureTuning variant) {
  funcs.at(key).startCodegen(sig, variant);
}

void RegionStats::stopCodegen(JITID key,
                              FunctionSignatureBasic sig,
                              FunctionSignatureTuning variant) {
  funcs.at(key).stopCodegen(sig, variant);
}

void RegionStats::startExecute(JITID key,
                               FunctionSignatureBasic sig,
                               FunctionSignatureTuning version,
                               const long long* counters) {
  funcs.at(key).startExecute(sig, version, counters);
}

void RegionStats::stopExecute(JITID key,
                              FunctionSignatureBasic sig,
                              FunctionSignatureTuning version,
                              const long long* counters) {
  funcs.at(key).stopExecute(sig, version, counters);
}

bool RegionStats::isExecuted() const {
  return total.isUsed();
}

FunctionSignatureTuning
RegionStats::getBestVariant(JITID key, FunctionSignatureBasic sig) const {
  return funcs.at(key).getBestVariant(sig);
}

unsigned RegionStats::getNumSpecialized(JITID key) const {
  return funcs.at(key).getNumSpecialized();
}

unsigned long RegionStats::getNumCalls(JITID key,
                                       FunctionSignatureBasic sig,
                                       FunctionSignatureTuning version) const {
  return funcs.at(key).getNumCalls(sig, version);
}

Serializer& RegionStats::serialize(Serializer& s) const {
  s.mapStart();

  s.add("ID", region.getID());
  s.add("Total", total.nsecs());

  s.mapStart("Execute");
  for(const FunctionStats& f : funcs.values())
    if(f.isExecuted())
      s.add(f.getName(), f);
  s.mapEnd();

  s.mapEnd();

  return s;
}

} // namespace moya
