#include "Call.h"
#include "JITContext.h"
#include "Summary.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/Analysis/AssumptionCache.h>
#include <llvm/Analysis/LoopInfo.h>
#include <llvm/Analysis/ScalarEvolution.h>
#include <llvm/Analysis/ScalarEvolutionExpressions.h>
#include <llvm/Config/llvm-config.h>
#include <llvm/IR/Dominators.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Metadata.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/Scalar.h>
#include <llvm/Transforms/Utils/UnrollLoop.h>

using namespace llvm;
using namespace moya;

class DynamicLoopReductionPass : public FunctionPass {
public:
  static char ID;

private:
  std::string function;
  Vector<Call::Arg> args;
  const Summary* store;
  const JITContext* conf;
  Set<Instruction*> constgeps;

private:
  bool replaceArg(Function&, unsigned);
  bool
  propagate(Function&, Value*, Summary::Address, const void*, const Summary&);

public:
  DynamicLoopReductionPass();

  void setFunction(const std::string&);
  void setArgs(const Vector<Call::Arg>&);
  void setStore(const Summary*);
  void setJITContext(const JITContext*);

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnFunction(Function&);
};

DynamicLoopReductionPass::DynamicLoopReductionPass() : FunctionPass(ID) {
  ;
}

StringRef DynamicLoopReductionPass::getPassName() const {
  return "Moya Dynamic Loop Reduction";
}

void DynamicLoopReductionPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
  AU.addRequired<ScalarEvolutionWrapperPass>();
  AU.addRequiredID(LCSSAID);
  AU.addPreservedID(LCSSAID);
  AU.addPreserved<DominatorTreeWrapperPass>();
  AU.addRequired<AssumptionCacheTracker>();
}

void DynamicLoopReductionPass::setFunction(const std::string& f) {
  this->function = f;
}

void DynamicLoopReductionPass::setArgs(const Vector<Call::Arg>& args) {
  this->args = args;
}

void DynamicLoopReductionPass::setStore(const Summary* store) {
  this->store = store;
}

void DynamicLoopReductionPass::setJITContext(const JITContext* conf) {
  this->conf = conf;
}

// The status of the descriptor may be variable, but even so, we will
// fold the descriptor values in becuase we will generate a function
// for each descriptor size.
bool DynamicLoopReductionPass::propagate(Function& f,
                                         Value* inst,
                                         Summary::Address addr,
                                         const void* ptr,
                                         const Summary& store) {
  bool changed = false;

  if(auto* load = dyn_cast<LoadInst>(inst)) {
    Type* ptrTy = load->getPointerOperand()->getType();
    Type* elemTy = dyn_cast<PointerType>(ptrTy)->getElementType();
    // If it is a struct, we deference it regardless of the status
    if(store.getStatus(addr).isConstant()) {
      if(ptr and elemTy->isPointerTy()) {
        const void* nextPtr = *reinterpret_cast<const void* const*>(ptr);
        const Summary::Targets& targets = store.getTargets(addr);
        if(targets.size() == 1) {
          Summary::Address nextAddr = targets.at(0);
          unsigned i = 0;
          for(Use& u : load->uses()) {
            changed |= propagate(f, u.getUser(), nextAddr, nextPtr, store);
            // This is here because replaceAllUsesWith will
            // invalidate the iterator here
            if(i++ > load->getNumUses())
              break;
          }
        } else {
          moya_error("Unsupported: multiple targets");
        }
      }
    }
  } else if(auto* gep = dyn_cast<GetElementPtrInst>(inst)) {
    Type* ptrTy = gep->getPointerOperandType();
    auto* elemTy = dyn_cast<PointerType>(ptrTy)->getElementType();
    // if(isDescriptorType(elemTy)) {
    //   const DataLayout& DL = getDataLayout(f);
    //   APInt coff(DL.getPointerSize() * 8, 0, false);
    //   if(ptr and gep->accumulateConstantOffset(DL, coff)) {
    //     uint64_t offset = coff.getLimitedValue();
    //     const byte* nextPtr = ptr + offset;
    //     Summary::Address nextAddr = addr + offset;
    //     unsigned i = 0;
    //     for(Use& u : gep->uses()) {
    //       changed |=
    //           propagate(f, u.getUser(), nextAddr, nextPtr, true, store);
    //       if(i++ > gep->getNumUses())
    //         break;
    //     }
    //   } else {
    //     if(store.getStatus(addr).isConstant())
    //       constgeps.insert(gep);
    //   }
    // }
  } else if(auto* cst = dyn_cast<CastInst>(inst)) {
    unsigned i = 0;
    for(Use& u : cst->uses()) {
      changed |= propagate(f, u.getUser(), addr, ptr, store);
      if(i++ > cst->getNumUses())
        break;
    }
  } else if(auto* phi = dyn_cast<PHINode>(inst)) {
    if(LLVMUtils::arePHINodeIncomingValuesEqual(phi)) {
      unsigned i = 0;
      for(Use& u : phi->uses()) {
        // FIXME? : Is this correct?
        changed |= propagate(f, u.getUser(), addr, ptr, store);
        if(i++ > phi->getNumUses())
          break;
      }
    }
  }

  // I probably don't need to do propagate into the call arguments because
  // the subsequent passes will take care of it for me, but I should check
  // NOTE: The comment above might only apply for Fortran because everything
  // gets passed by pointer in Fortran
  return changed;
}

bool DynamicLoopReductionPass::replaceArg(Function& f, unsigned argno) {
  bool changed = false;
  Argument* arg = LLVMUtils::getArgument(f, argno);
  if((not moya::Metadata::hasIgnore(arg)) and arg->getType()->isPointerTy()) {
    Summary::Address addr = store->lookup(argno);
    const auto* ptr = reinterpret_cast<const void*>(args.at(argno));
    for(Use& u : arg->uses())
      changed |= propagate(f, u.getUser(), addr, ptr, *store);
  }
  return changed;
}

static bool doesNotWrite(Loop* loop) {
  for(auto bb = loop->block_begin(); bb != loop->block_end(); bb++)
    for(auto i = (*bb)->begin(); i != (*bb)->end(); i++)
      if(isa<StoreInst>(&*i))
        return false;
  return true;
}

bool DynamicLoopReductionPass::runOnFunction(Function& f) {
  if(f.getName().str() != function)
    return false;
  constgeps.clear();
  moya_message(getPassName());

  bool changed = false;
  for(unsigned i = 0; i < args.size(); i++)
    changed |= replaceArg(f, i);

  for(GlobalVariable& g : f.getParent()->globals()) {
    if(not g.isConstant()) {
      const void* ptr = conf->getGlobalPtr(g.getName());
      Summary::Address addr = store->lookup(g.getName());
      for(Use& u : g.uses())
        if(auto* inst = dyn_cast<Instruction>(u.getUser()))
          if(LLVMUtils::getFunctionName(inst) == function)
            changed |= propagate(f, inst, addr, ptr, *store);
    }
  }

  LoopInfo& li = getAnalysis<LoopInfoWrapperPass>().getLoopInfo();
  ScalarEvolution& se = getAnalysis<ScalarEvolutionWrapperPass>().getSE();

  Map<Loop*, unsigned> tounroll;
  for(Instruction* gep : constgeps)
    if(Loop* loop = li.getLoopFor(gep->getParent()))
      if(doesNotWrite(loop))
        if(isa<const SCEVAddRecExpr>(se.getSCEV(gep)))
          if(BasicBlock* exiting = loop->getExitingBlock())
            tounroll[loop] = se.getSmallConstantTripCount(loop, exiting);

  AssumptionCache& AC
      = getAnalysis<AssumptionCacheTracker>().getAssumptionCache(f);
  for(auto it : tounroll) {
    Loop* loop = it.first;
    unsigned count = it.second;
    // FIXME?: Can't assume that all these values will be the same
    bool PreserveLCSSA = mustPreserveAnalysisID(LCSSAID);
    UnrollLoop(loop,
               count,
               count,
               true,
               false,
               false,
               true,
               false,
               count,
               count,
               0,
               &li,
               &se,
               nullptr,
               &AC,
               nullptr,
               PreserveLCSSA);
    changed |= true;
  }

  return changed;
}

char DynamicLoopReductionPass::ID = 0;
static RegisterPass<DynamicLoopReductionPass>
    X("moya-jit-reduction",
      "Fully unrolls a loop which performs reductions on "
      "constant memory (should ONLY be used at runtime)",
      false,
      false);

Pass* createDynamicLoopReductionPass(const std::string& function,
                                     const Vector<Call::Arg>& args,
                                     const Summary* store,
                                     const JITContext* conf) {
  auto* pass = new DynamicLoopReductionPass();
  pass->setFunction(function);
  pass->setArgs(args);
  pass->setStore(store);
  pass->setJITContext(conf);
  return pass;
}
