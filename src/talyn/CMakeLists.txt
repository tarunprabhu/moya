set(SOURCES
  AAResult.cpp
  APIDefns.cpp
  CallArg.cpp
  Call.cpp
  ConstantGenerator.cpp
  ConstantGeneratorHost.cpp
  # DynamicConstantPointersPass.cpp
  DynamicConstantPropagationPass.cpp
  # DynamicLoopReductionPass.cpp
  FunctionStats.cpp
  DyldMemoryManager.cpp
  JITContext.cpp
  LibTalyn.cpp
  MachineCodeCacheManager.cpp
  ObjectCache.cpp
  Payload.cpp
  PrepareJITPass.cpp
  PropagateAnalysisTypesPass.cpp
  RegionStats.cpp
  Runtime.cpp
  RuntimeCacheManager.cpp
  Search.cpp
  SearchConfig.cpp
  SearchExhaustive.cpp
  SearchSpace.cpp
  SearchStrategy.cpp
  Signature.cpp
  Stats.cpp
  Summary.cpp
  SummaryCell.cpp
  System.cpp
  TalynBase.cpp
  TalynC.cpp
  TalynCXX.cpp
  TalynFort.cpp
  TalynF77.cpp)

if(CUDA_FOUND)
  set(SOURCES ${SOURCES}
    ConstantGeneratorCuda.cpp
    TalynCuda.cpp)
endif()

if(POLLY_FOUND)
  set(SOURCES ${SOURCES}
    PollyASTPrinterPass.cpp)
endif()

include_directories(SYSTEM ${OPENSSL_INCLUDEDIR})

set(TARGET ${MOYA_LIB_NAME_TALYN})
set(INSTALL_RPATH "${INSTALL_RPATH}:${MOYA_INSTALL_LIBDIR}")

# The target to build
add_library(${TARGET} SHARED ${SOURCES})

# Add other libraries to be linked
if(MPI_FOUND)
  set_target_properties(${TARGET} PROPERTIES
    LINK_FLAGS "${MPI_C_LINK_FLAGS}")
  target_link_libraries(${TARGET} ${MPI_C_LIBRARIES})
endif()

if(CUDA_FOUND)
  target_link_libraries(${TARGET} ${CUDA_LIBRARIES})
endif()

if(PAPI_FOUND)
  target_link_libraries(${TARGET} ${PAPI_LIBRARIES})
endif()

if(POLLY_FOUND)
  # The Polly shared library is usually named LLVMPolly.so
  # For some reason, CMake won't just include the full path of the shared object
  # in the link line but insists on converting it to -lLLVMPolly which obviously
  # is a problem because the linker starts looking for libLLVMPolly.so
  # Setting CMP0060 to NEW doesn't seem to do the trick, so we have to use
  # this workaround courtesy: http://stackoverflow.com/a/33202591
  include_directories(/tools/moya/include/polly)
  add_library(polly UNKNOWN IMPORTED)
  set_property(TARGET polly
    PROPERTY IMPORTED_LOCATION ${POLLY_LIBRARIES})
  target_link_libraries(${TARGET} polly)
endif()

target_link_directories(${TARGET} PUBLIC ${OPENSSL_LIBDIR})
target_link_libraries(${TARGET} ${OPENSSL_LIBRARIES})

# Target properties and library dependencies
target_link_options(${TARGET} PUBLIC -flto)

set_target_properties(${TARGET} PROPERTIES
  INSTALL_RPATH ${INSTALL_RPATH}
  BUILD_WITH_INSTALL_RPATH FALSE)

target_link_libraries(${TARGET}
  ${MOYA_LIB_NAME_COMMON} ${CMAKE_LLVM_SHARED})


install(TARGETS ${TARGET}
  LIBRARY
  DESTINATION ${MOYA_INSTALL_LIBDIR})
