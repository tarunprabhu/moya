#ifndef MOYA_TALYN_LIB_TALYN_H
#define MOYA_TALYN_LIB_TALYN_H

namespace moya {

class Runtime;

Runtime& talyn_get_runtime();

} // namespace moya

#endif // MOYA_TALYN_LIB_TALYN_H
