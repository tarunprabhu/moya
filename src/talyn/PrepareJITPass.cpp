#include "common/APITypes.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/Pass.h>

using namespace llvm;

class PrepareJITPass : public FunctionPass {
protected:
  JITID id;

public:
  explicit PrepareJITPass(JITID);

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnFunction(Function&);

public:
  static char ID;
};

PrepareJITPass::PrepareJITPass(JITID id) : FunctionPass(ID), id(id) {
  ;
}

StringRef PrepareJITPass::getPassName() const {
  return "Moya Prepare JIT (Talyn)";
}

void PrepareJITPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool PrepareJITPass::runOnFunction(Function& f) {
  bool changed = false;

  if(moya::Metadata::getJITID(f) != id)
    return changed;

  LLVMContext& context = f.getContext();
  unsigned id
      = context.getMDKindID(moya::Metadata::getAnalysisTypeInstrAttrName());

  // For now, all we need to do is get rid of the analysis types
  for(inst_iterator i = inst_begin(f); i != inst_end(f); i++)
    i->setMetadata(id, nullptr);

  return changed;
}

char PrepareJITPass::ID = 0;

Pass* createPrepareJITPass(JITID id) {
  return new PrepareJITPass(id);
}
