#include "Payload.h"
#include "common/Config.h"
#include "common/Encoder.h"
#include "common/Hexify.h"

#include <llvm/Bitcode/BitcodeReader.h>
#include <llvm/Bitcode/BitcodeWriter.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/raw_ostream.h>

#include <openssl/md5.h>

namespace moya {

static std::string computeHash(const char* start, size_t size) {
  Vector<unsigned char> md5(MD5_DIGEST_LENGTH, 0);

  MD5(reinterpret_cast<const unsigned char*>(start), size, md5.data());

  return Encoder::encodeString(md5);
}

Payload::Payload(const std::string& file)
    : PayloadBase(file, std::ios::in | std::ios::ate),
      moyaDisabled(Config::getEnvDisabled()) {
  ;
}

const Vector<Summary*>& Payload::getSummaries() const {
  return summaries;
}

Payload::module_range Payload::getModules() {
  return module_range(modules.begin(), modules.end());
}

const std::string& Payload::getHash(llvm::Module* mod) const {
  return modules.at(mod);
}

// The format of the payload is as follows
//
// <magic number> [8 byte]
// <number of regions> [8 byte]
//   <summary blob>
//   ...
// <number of bitcode modules> [8 byte]
//   <size of bitcode module> [8 byte]
//   <bitcode module>
//   ...
// <offset of magic number from start of executable file> [8 byte]
// <magic number> [8 byte]
//
// The magic numbers are present as sanity checks
//
bool Payload::parse() {
  uint64_t magic = 0;

  // Footer
  fin().seekg(-8, std::ios::end);
  magic = unpickle<uint64_t>();
  if(magic != Config::getPayloadMagicNumber()) {
    if(not moyaDisabled)
      moya_warning(
          "Possibly corrupt payload. Could not get footer: " << hex(magic));
    return false;
  }

  // Offset from start of executable
  fin().seekg(-16, std::ios::end);
  size_t pos = unpickle<size_t>();

  // Reposition cursor to header of payload
  fin().seekg(pos, std::ios::beg);
  magic = unpickle<uint64_t>();
  if(magic != Config::getPayloadMagicNumber()) {
    if(not moyaDisabled)
      moya_warning(
          "Possibly corrupt payload. Could not get header: " << hex(magic));
    return false;
  }

  // Summaries
  size_t numSummaries = unpickle<size_t>();
  for(size_t i = 0; i < numSummaries; i++) {
    summaries.push_back(new Summary());
    summaries.back()->unpickle(*this);
  }

  // Modules
  size_t numModules = unpickle<size_t>();
  for(size_t i = 0; i < numModules; i++) {
    size_t size = unpickle<size_t>();

    std::unique_ptr<char[]> raw(new char[size]);
    fin().read(raw.get(), size);

    llvm::StringRef ref(raw.get(), size);
    std::unique_ptr<llvm::MemoryBuffer> buffer
        = llvm::MemoryBuffer::getMemBuffer(ref, "", false);

    std::string hash
        = computeHash(buffer->getBufferStart(), buffer->getBufferSize());

    llvm::LLVMContext* context = new llvm::LLVMContext();
    std::unique_ptr<llvm::Module> module
        = move(parseBitcodeFile(buffer->getMemBufferRef(), *context).get());

    modules[module.release()] = hash;
  }

  fin().close();

  return true;
}

} // namespace moya
