#include "common/Diagnostics.h"

#include <llvm/Analysis/RegionPass.h>
#include <llvm/Pass.h>
#include <polly/CodeGen/IslAst.h>
#include <polly/LinkAllPasses.h>

using namespace llvm;

class PollyASTPrinterPass : public RegionPass {
public:
  static char ID;

private:
  raw_ostream& out;

public:
  explicit PollyASTPrinterPass(raw_ostream&);

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnRegion(Region*, RGPassManager&);
};

PollyASTPrinterPass::PollyASTPrinterPass(raw_ostream& out)
    : RegionPass(ID), out(out) {
  ;
}

StringRef PollyASTPrinterPass::getPassName() const {
  return "Moya Polly AST Printer";
}

void PollyASTPrinterPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequiredID(&polly::IslAstInfoWrapperPass::ID);
  AU.setPreservesAll();
}

bool PollyASTPrinterPass::runOnRegion(Region* region, RGPassManager&) {
  moya_message(getPassName());

  getAnalysisID<Pass>(&polly::IslAstInfoWrapperPass::ID)
      .print(out, region->getEntry()->getParent()->getParent());

  return false;
}

char PollyASTPrinterPass::ID = 0;

Pass* createPollyASTPrinterPass(raw_ostream& stream) {
  return new PollyASTPrinterPass(stream);
}
