#include "SearchStrategy.h"

namespace moya {

SearchStrategy::SearchStrategy(SearchSpace& space)
    : curr(SearchSpace::getInvalidEnumeration()), space(space) {
  ;
}

void SearchStrategy::setCurr(SearchSpace::Enumeration state) {
  this->curr = state;
  this->currStr = space.getEnumerationStr(state);
}

bool SearchStrategy::isStarted() const {
  return curr != SearchSpace::getInvalidEnumeration();
}

SearchSpace::Enumeration SearchStrategy::getEnumeration() const {
  return curr;
}

const std::string& SearchStrategy::getEnumerationStr() const {
  return currStr;
}

Vector<char*>& SearchStrategy::getPollyArgs() {
  return space.getPollyArgs(vals);
}

} // namespace moya
