#ifndef MOYA_TALYN_SEARCH_SPACE_H
#define MOYA_TALYN_SEARCH_SPACE_H

#include "common/Map.h"
#include "common/Vector.h"
#include <memory>
#include <string>

namespace moya {

class System;

// This is a class that represents the state space that is to be searched
// while tuning. There will be only one instance of this class.
// Any search algorithms will be separate from the space, so we can have
// different search strategies to explore it.
//
class SearchSpace {
public:
  using Key = unsigned;
  using Val = unsigned;
  using KeyVal = uint64_t;
  using Enumeration = uint64_t;

private:
  const System& system;

  // These are the options that will be passed to polly
  Vector<std::string> keys;

  // These are the values that each of the options might take
  Vector<Vector<std::string>> vals;

  Map<KeyVal, std::unique_ptr<char>> strings;

  // A cache of the polly option strings that have been generated so far
  Map<Vector<Val>, Vector<char*>> args;

private:
  char* makeCStr(const std::string&, const std::string&);
  void createCache(unsigned, const std::string&, unsigned long);

  KeyVal getKeyVal(Key, Val);

public:
  SearchSpace(const System&);

  unsigned getNumKeys() const;
  unsigned getNumVals(unsigned) const;
  Vector<char*>& getPollyArgs(const Vector<SearchSpace::Val>&);

  // This returns an enumeration of the state. All the states in the search
  // space are enumerable
  Enumeration getEnumeration(const Vector<Val>&) const;
  std::string getEnumerationStr(Enumeration) const;

  bool isValid(SearchSpace::Enumeration) const;

  // This returns the first key that is actually a tunable parameter.
  // The first n keys in the space are not really tunable but are fixed
  // parameters that depend on the system on which we are JITting.
  Key getMinKey() const;

  // The last key that is a tunable parameter
  Key getMaxKey() const;

  // The minimum value and maximum values for a given key
  Val getMinVal(Key) const;
  Val getMaxVal(Key) const;

public:
  static Enumeration getInvalidEnumeration();
};

} // namespace moya

#endif // MOYA_TALYN_SEARCH_SPACE_H
