#ifndef MOYA_TALYN_SUMMARY_CELL_H
#define MOYA_TALYN_SUMMARY_CELL_H

#include "common/SummaryCellBase.h"

namespace moya {

class Payload;

// This has absolutely nothing new to add the SummaryCellBase

class SummaryCell : public SummaryCellBase {
public:
  SummaryCell();
  virtual ~SummaryCell() = default;

  SummaryCell& unpickle(Payload&);
};

} // namespace moya

#endif // MOYA_TALYN_SUMMARY_CELL_H
