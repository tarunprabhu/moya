#include "ConstantGeneratorHost.h"

using namespace llvm;

namespace moya {

ConstantGeneratorHost::ConstantGeneratorHost(const Module& module)
    : ConstantGenerator(module) {
  ;
}

// Everything is local and accessible, so no need to copy anything
const void* ConstantGeneratorHost::getIntPointer(const void* gptr,
                                                 unsigned long) {
  return gptr;
}

const void* ConstantGeneratorHost::getFloatPointer(const void* gptr) {
  return gptr;
}

const void* ConstantGeneratorHost::getDoublePointer(const void* gptr) {
  return gptr;
}

void* ConstantGeneratorHost::getArrayPointer(const void* gptr, unsigned long) {
  // This will only be used by ConstantGeneratorHost and we are definitely
  // not doing anything evil with this pointer there
  return const_cast<void*>(gptr);
}

// No need to deallocate anything because everything is local and we don't need
// to copy anything
void ConstantGeneratorHost::deallocateHostBuffer(void*) {
  return;
}

} // namespace moya
