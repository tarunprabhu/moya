#include "TalynFort.h"
#include "Passes.h"
#include "common/LLVMUtils.h"
#include "common/PassManager.h"

using namespace llvm;

namespace moya {

TalynFort::TalynFort(const JITContext& jitContext,
                     MachineCodeCacheManager& mcCacheMgr,
                     RuntimeCacheManager& rtCacheMgr,
                     Stats& stats,
                     SearchSpace& space)
    : TalynBase(jitContext,
                mcCacheMgr,
                rtCacheMgr,
                stats,
                space,
                SourceLang::Fort) {
  ;
}

void TalynFort::populatePreparePasses(PassManager& pm, JITID id) {
  // XXX: This is stupid!
  // The only reason this is being done is because the analysis types from
  // the frontend are broken. Ideally, those should be fixed instead.
  pm.addFunctionPass(createPrepareJITPass(id));
}

void TalynFort::populateConstantPropagationPasses(PassManager& pm,
                                                  JITID key,
                                                  const Vector<CallArg>& args) {
  pm.addFunctionPass(createPropagateAnalysisTypesPass(key));
  pm.addFunctionPass(
      createDynamicConstantPropagationPass(key, args, region, jitContext));
}

template <typename T>
void TalynFort::registerArgImpl(JITID key,
                                unsigned i,
                                ArgType type,
                                const T* ptr,
                                ArgDeref deref) {
  const Summary& store = jitContext.getStore(region, key);
  Status status = store.getStatus(static_cast<unsigned>(i));
  if(status.isDerefInKey())
    if(ptr)
      call.at(key).registerArg(i,
                               type,
                               reinterpret_cast<const void*>(ptr),
                               *ptr,
                               deref == ArgDeref::Ignore ? ArgDeref::Ignore
                                                         : ArgDeref::One);
    else
      call.at(key).registerArg(i,
                               type,
                               reinterpret_cast<const void*>(ptr),
                               static_cast<T>(0),
                               deref);
  else
    // We still need the argument in the call args, but it will not be used to
    // compute the signature.
    call.at(key).registerArg(
        i, type, reinterpret_cast<const void*>(ptr), deref);
}

void TalynFort::registerArg(JITID key,
                            unsigned i,
                            ArgType type,
                            const bool* arg,
                            ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynFort::registerArg(JITID key,
                            unsigned i,
                            ArgType type,
                            const int8_t* arg,
                            ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynFort::registerArg(JITID key,
                            unsigned i,
                            ArgType type,
                            const int16_t* arg,
                            ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynFort::registerArg(JITID key,
                            unsigned i,
                            ArgType type,
                            const int32_t* arg,
                            ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynFort::registerArg(JITID key,
                            unsigned i,
                            ArgType type,
                            const int64_t* arg,
                            ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynFort::registerArg(JITID key,
                            unsigned i,
                            ArgType type,
                            const float* arg,
                            ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynFort::registerArg(JITID key,
                            unsigned i,
                            ArgType type,
                            const double* arg,
                            ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynFort::registerArg(JITID key,
                            unsigned i,
                            ArgType type,
                            const long double* arg,
                            ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynFort::registerArg(JITID key,
                            unsigned i,
                            ArgType type,
                            const void** arg,
                            ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

} // namespace moya
