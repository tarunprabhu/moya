#ifndef MOYA_TALYN_PAYLOAD_H
#define MOYA_TALYN_PAYLOAD_H

#include "Summary.h"
#include "common/PayloadBase.h"
#include "common/Vector.h"

#include <llvm/IR/Module.h>

namespace moya {

class Payload : public PayloadBase {
protected:
  Vector<Summary*> summaries;
  Map<llvm::Module*, std::string> modules;

  bool moyaDisabled;

public:
  using module_iterator = decltype(modules)::key_iterator;
  using module_range = decltype(modules)::key_range;

public:
  Payload(const std::string&);

  const Vector<Summary*>& getSummaries() const;
  module_range getModules();
  const std::string& getHash(llvm::Module*) const;

  bool parse();
};

} // namespace moya

#endif // MOYA_TALYN_PAYLOAD_H
