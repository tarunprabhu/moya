#include "System.h"

#include <fstream>
#include <sstream>

#include <sched.h>
#include <sys/types.h>
#include <unistd.h>

namespace moya {

// The tuner is helped if it has more accurate information about the hardware
// on which it is executing. So figure out the size of the caches here.
// We do this by first figuring out which CPU we are running on and then
// looking up the cpuinfo in /sys. Of course, there is a chance that we will
// get migrated to another CPU with different characteristics, but that is
// something that we won't deal with for the moment
static unsigned long readCacheSize(unsigned cache) {
  int cpu = sched_getcpu();
  unsigned long size = 0;
  std::stringstream fname;
  fname << "/sys/devices/cpu/cpu" << cpu << "/cache/index" << cache << "/size";
  std::ifstream f(fname.str());
  if(f.is_open()) {
    char c;
    unsigned long decsize = 0;
    f.get(c);
    while(c >= '0' and c <= '9') {
      decsize = decsize * 10 + (c - '0');
      f.get(c);
    }
    if(c == 'K')
      decsize *= 1024;
    else if(c == 'M')
      decsize *= (1024 * 1024);

    // The size should be the nearest power of two above this number
    size = 1;
    while(size <= decsize)
      size *= 2;
  }

  return size;
}

System::System() {
  for(unsigned i = 0; i < 3; i++)
    cacheSizes[i] = readCacheSize(i + 1);
}

unsigned long System::getCacheSize(unsigned idx) const {
  if(idx >= 1 and idx <= 3)
    return cacheSizes[idx - 1];
  return 0;
}

} // namespace moya
