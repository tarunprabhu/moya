#include "Search.h"
#include "SearchExhaustive.h"

#include "common/Config.h"

namespace moya {

Search::Search(unsigned tuning, llvm::Module& module, SearchSpace& space)
    : space(space), config(tuning), strategy(new SearchExhaustive(space)),
      module(module) {
  ;
}

llvm::Module& Search::getModule() const {
  return module;
}

unsigned Search::getThresholdNumCalls() const {
  return config.getThresholdNumCalls();
}

bool Search::canExplore() const {
  return not strategy->isFinished();
}

FunctionSignatureTuning Search::getCurr() const {
  SearchSpace::Enumeration curr = strategy->getEnumeration();
  if(not space.isValid(curr))
    return Config::getUntunedFunctionSignature();
  return curr;
}

const std::string& Search::getCurrStr() const {
  return strategy->getEnumerationStr();
}

Search::State Search::getNext() {
  // The search space has been exhausted
  if(not strategy->next())
    return {Config::getUntunedFunctionSignature(), 0, nullptr};

  Vector<char*>& args = strategy->getPollyArgs();
  return {static_cast<FunctionSignatureTuning>(strategy->getEnumeration()),
          args.size(),
          args.data()};
}

} // namespace moya
