#ifndef MOYA_TALYN_TALYN_BASE_H
#define MOYA_TALYN_TALYN_BASE_H

#include "Call.h"
#include "Search.h"
#include "Signature.h"
#include "Stats.h"
#include "common/APITypes.h"
#include "common/Map.h"
#include "common/Set.h"
#include "common/Vector.h"

#include <llvm/Analysis/AliasAnalysis.h>
#include <llvm/ExecutionEngine/OrcMCJITReplacement.h>
#include <llvm/IR/DataLayout.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>

namespace moya {

class JITContext;
class SearchSpace;
class Stats;
class MachineCodeCacheManager;
class RuntimeCacheManager;
class PassManager;

class TalynBase {
protected:
  const JITContext& jitContext;
  MachineCodeCacheManager& mcCacheMgr;
  RuntimeCacheManager& rtCacheMgr;
  Stats& stats;
  SearchSpace& searchSpace;

  SourceLang lang;

  // This is the "best" version of the function that has been discovered by
  // the autotuner. If the function is not being autotuned, this is just the
  // JIT'ted code for the function. We have a map for each region because the
  // status of the data that the function uses may be different in each
  // region
  Map<RegionID, Map<JITID, Map<FunctionSignatureBasic, void*>>> specialized;

  // All the tuned versions that are generated
  Map<RegionID,
      Map<JITID,
          Map<FunctionSignatureBasic, Map<FunctionSignatureTuning, void*>>>>
      tuned;

  // Objects to manage autotuning
  Map<RegionID, Map<JITID, Map<FunctionSignatureBasic, Search>>> searches;

  Map<RegionID, Map<JITID, Set<FunctionSignatureBasic>>> doneSearching;

  // Each new module module has its own ExecutionEngine. We only use the pointer
  // to the compiled function, but the execution engine and the module both
  // need to be around for that pointer to be valid.
  //
  // Deleting the execution engine also deletes the module with which the
  // execution engine was associated.
  Set<llvm::Module*> newms;
  Set<llvm::ExecutionEngine*> ees;

  // Current region
  RegionID region;

  // Cached values of environment variables
  bool saveCode;
  bool usePolly;
  bool saveOptReport;

  // The arguments for the current call
  Map<JITID, Call> call;

protected:
  void maybeSaveOptReport(JITID,
                          FunctionSignatureBasic,
                          FunctionSignatureTuning,
                          const std::string&,
                          const std::string&);
  void maybeSaveSpecializedModule(llvm::Module&,
                                  JITID,
                                  FunctionSignatureBasic,
                                  FunctionSignatureTuning);
  void maybeSaveSpecializedArgs(const moya::Vector<CallArg>&,
                                JITID,
                                FunctionSignatureBasic,
                                FunctionSignatureTuning);
  void maybeSaveOptimizedModule(llvm::Module&,
                                JITID,
                                FunctionSignatureBasic,
                                FunctionSignatureTuning);
  void maybeSaveAssembler(llvm::Module&,
                          JITID,
                          FunctionSignatureBasic,
                          FunctionSignatureTuning);
  void maybeCacheMachineCode(llvm::Module&,
                             JITID,
                             FunctionSignatureBasic,
                             FunctionSignatureTuning);

  // Calls to this function must be protected by a critical region
  virtual void
  update(JITID, FunctionSignatureBasic, FunctionSignatureTuning, void*);
  virtual void setBest(JITID, FunctionSignatureBasic);

  virtual llvm::ExecutionEngine& createExecutionEngine(llvm::Module&);
  virtual llvm::ExecutionEngine& getExecutionEngine(llvm::Module&, JITID);
  virtual llvm::Module& getClonedModule(llvm::Module&);

  virtual bool shouldExplore(JITID, FunctionSignatureBasic) const;

  virtual bool setMoyaGlobals(llvm::Module&,
                              FunctionSignatureBasic,
                              FunctionSignatureTuning);

  virtual void populatePreparePasses(PassManager&, JITID);

  virtual void populateConstantPropagationPasses(PassManager&,
                                                 JITID,
                                                 const Vector<CallArg>&);

#ifdef ENABLED_POLLY
  // This is the function used when autotuning. We don't need the arguments
  // because we start working with the specialized module
  virtual void* compileTuned(JITID, FunctionSignatureBasic);
  virtual bool runPollyPasses(llvm::Module&,
                              JITID,
                              FunctionSignatureBasic,
                              FunctionSignatureTuning,
                              const std::string&,
                              unsigned,
                              char**);
  virtual void populatePollyPreparePasses(PassManager&, JITID);
  virtual void populatePollyPasses(PassManager&, JITID, llvm::raw_ostream&);
#endif // ENABLED_POLLY

  virtual void* loadCached(JITID, FunctionSignatureBasic);
  virtual void* compileBasic(JITID, FunctionSignatureBasic);

  virtual bool isCompiled(JITID, FunctionSignatureBasic) const;
  virtual void* getCompiled(JITID, FunctionSignatureBasic) const;
  virtual bool isCached(JITID, FunctionSignatureBasic) const;

  virtual bool runMoyaPasses(llvm::Module&,
                             JITID,
                             FunctionSignatureBasic,
                             const Vector<CallArg>&);
  virtual void runOptimizationPasses(llvm::Module&,
                                     JITID,
                                     FunctionSignatureBasic,
                                     FunctionSignatureTuning);

  virtual void* emitMachineCode(llvm::Module&,
                                JITID,
                                FunctionSignatureBasic,
                                FunctionSignatureTuning);

  TalynBase(const JITContext&,
            MachineCodeCacheManager&,
            RuntimeCacheManager&,
            Stats&,
            SearchSpace&,
            SourceLang);

public:
  virtual ~TalynBase() = default;

  virtual void enterRegion(RegionID);
  virtual void exitRegion(RegionID);
  virtual bool isInRegion() const;
  virtual RegionID getCurrentRegion() const;

  // This is the entry point to JIT compile the function
  virtual void* compile(JITID);

  // These are the rules for hashing functions. The only distinction needs to
  // be made when there's a pointer to a primitive being passed. In Fortran,
  // it is just a pointer to a primitive, but in C/C++, it may be an array
  // being passed. In Fortran, then, we use the contents pointed to by the
  // pointer to compute the key, but in C/C++ we use the pointer itself
  //
  //   Primitive: Just hash it
  //
  //   Pointer to primitive:
  //
  //     Fortran: Use the primitive value
  //
  //     In the case of Fortran, this is a way of
  //     implementing the call-by-value semantics that we see in C/C++. The
  //     odds are that the function is being used in this way, but because
  //     everything is passed by pointer in Fortran, any loop index being
  //     passed, for instance, will end up being marked as variable
  //     unnecessarily.
  //
  //     C:       Use the pointer
  //
  //     In C, a pointer to an integer is either an array, or is actually
  //     the address of the integer. Since we can't be sure which of these it
  //     is, we just use the pointer value itself in computing the signature.
  //     This is true for C++ as well when passing values by pointer.
  //
  //     C++:     Use the primitive value if the argument is passed by
  //              reference
  //
  //     Variables passed by reference in C++ are passed by pointer in LLVM.
  //     But if it is passed by reference, then we treat it like Fortran
  //     variables passed by reference and use the primitive value.
  //
  //
  //   Pointer to array: This happens in all languages when an array of known
  //     length is passed as a parameter. The types will be canonicalized, so
  //     we will never get 0-length arrays here.
  //
  //     Use the elements of the array as a key.
  //
  //     TODO: Ideally, we should only use the elements of the array that
  //     are actually used in JIT'ting in the key instead of all the elements
  //
  //
  //   Pointer to struct: Use the fields of the struct which are semiconstant.
  //     (ideal). Use the pointer value (first implementation).
  //
  //     If any of the fields is a pointer, just treat the contents of the
  //     pointer as a primitive value. The idea here is that any variable field
  //     will be ignored by the optimizer, so there's no need to include it in
  //     the key. If any of the fields in the struct is a pointer,
  //     if it is variable, again, the optimizer
  //     will not look at the object which the pointer points to. If the
  //     pointer is semi-constant, then it will look at the object and fold any
  //     semi-constants it finds there. For the purpose of determining a key,
  //     the actual pointer is what matters.
  //
  //     If we examine the actual value of the struct, we can probably get
  //     additional optimizations because, like the Fortran case where we are
  //     passing primitives by pointer when we ought to be passing them by
  //     value, we can check if the fields are constant in the closure of the
  //     function and fold them in. But as a first implementation, we won't
  //     do that.
  //
  //     TODO: Examine the fields and mark them as constant in closure for
  //     extra optimization possibilities
  //
  //
  //   Pointer to pointer:
  //
  //     Fortran:  Use contents of the pointer
  //
  //     This is seen, for instance, when a pointer to a derived type is passed
  //     to a function. In this case, we need to create a new version of the
  //     function for each pointee that is passed in. If the pointee is a
  //     derived type, then the optimizer will not fold in any fields which
  //     are not semi-constant anyway.
  //
  //     C/C++: This is just a pointer
  //

  template <typename T,
            typename std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
  void registerArg(JITID, unsigned, ArgType, const T, ArgDeref);

  template <typename T,
            typename std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
  void registerArg(JITID, unsigned, ArgType, const T*, int32_t, ArgDeref);

  // In C, we can't tell whether or not a poiner to something is a pointer to
  // a single object or to an array. In that case, we should never try to
  // dereference it and just use the pointer value in signature generation
  //
  void registerArg(JITID, unsigned, ArgType, const void*, ArgDeref);

  // These are intended for Fortran and C++
  //
  // Fortran: Arguments are passed by pointer by default and we ought to
  //          use the values when possible in signature generation
  //
  // C++ : When arguments are passed by reference, they will look like they are
  //       passed by pointer and we should use their values in signature gen.
  //
  virtual void registerArg(JITID, unsigned, ArgType, const bool*, ArgDeref) = 0;
  virtual void registerArg(JITID, unsigned, ArgType, const int8_t*, ArgDeref)
      = 0;
  virtual void registerArg(JITID, unsigned, ArgType, const int16_t*, ArgDeref)
      = 0;
  virtual void registerArg(JITID, unsigned, ArgType, const int32_t*, ArgDeref)
      = 0;
  virtual void registerArg(JITID, unsigned, ArgType, const int64_t*, ArgDeref)
      = 0;
  virtual void registerArg(JITID, unsigned, ArgType, const float*, ArgDeref)
      = 0;
  virtual void registerArg(JITID, unsigned, ArgType, const double*, ArgDeref)
      = 0;
  virtual void
  registerArg(JITID, unsigned, ArgType, const long double*, ArgDeref)
      = 0;
  virtual void registerArg(JITID, unsigned, ArgType, const void**, ArgDeref)
      = 0;

  // This is called before we begin to register the call arguments
  // This will explicitly be called from the Moya API's. The corresponding
  // endCall is handled internally.
  virtual void beginCall(JITID);
};

} // namespace moya

#endif // MOYA_TALYN_TALYN_BASE_H
