#ifndef MOYA_TALYN_RUNTIME_CACHE_MANAGER_H
#define MOYA_TALYN_RUNTIME_CACHE_MANAGER_H

#include "CallArg.h"
#include "common/CacheManagerBase.h"
#include "common/Vector.h"

#include <llvm/IR/Module.h>
#include <llvm/Support/raw_ostream.h>

namespace moya {

class Stats;

class RuntimeCacheManager : public CacheManagerBase {
public:
  RuntimeCacheManager();
  virtual ~RuntimeCacheManager() = default;

  void setSubdir(const std::string&);

  void writeStats(const Stats&, const std::string&);
  void writeAssembler(llvm::Module&, const std::string&);
  void writeModule(llvm::Module&, const std::string&);
  void writeSpecializedArgs(const Vector<CallArg>&, const std::string&);
  void writeOptReport(const std::string&, const std::string&);
};

} // namespace moya

#endif // MOYA_TALYN_RUNTIME_CACHE_MANAGER_H
