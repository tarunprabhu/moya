#include "ObjectCache.h"

#include "common/Diagnostics.h"

namespace moya {

ObjectCache::ObjectCache(llvm::Module* mod, llvm::MemoryBuffer* mbuf)
    : mod(mod), mbuf(mbuf) {
  ;
}

void ObjectCache::notifyObjectCompiled(const llvm::Module*,
                                       llvm::MemoryBufferRef) {
  moya_error("This should never be called\n");
}

std::unique_ptr<llvm::MemoryBuffer>
ObjectCache::getObject(const llvm::Module* mod) {
  if(mod != this->mod)
    return nullptr;
  return std::move(mbuf);
}

} // namespace moya
