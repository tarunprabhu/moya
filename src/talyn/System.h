#ifndef MOYA_TALYN_SYSTEM_H
#define MOYA_TALYN_SYSTEM_H

namespace moya {

// Properties of the system on which the code is running. This should be
// subclassed for different operating systems.
//
class System {
protected:
  // The cache sizes on the CPU on which this object lives.
  unsigned long cacheSizes[3];

public:
  System();
  virtual ~System() = default;

  // Return the requested cache size for the CPU on which this is running.
  // idx should be 1, 2 or 3.
  // TODO: This is only set during the constructor, so it is possible that the
  // process will have migrated. Need to handle that case.
  unsigned long getCacheSize(unsigned) const;
};

} // namespace moya

#endif // MOYA_TALYN_SYSTEM_H
