#include "SearchConfig.h"

#include <limits>

SearchConfig::SearchConfig(unsigned tuningLevel) {
  // TODO: Set different thresholds based on the tuning level
  switch(tuningLevel) {
  case 1:
    thresholdNumCalls = 15;
    break;
  case 2:
    thresholdNumCalls = 15;
    break;
  case 3:
    thresholdNumCalls = 10;
    break;
  default:
    thresholdNumCalls = std::numeric_limits<unsigned>::max();
    // Effectively turn off tuning but don't cause errors if this does happen
    // accidentally
  }
}

unsigned SearchConfig::getThresholdNumCalls() const {
  return thresholdNumCalls;
}
