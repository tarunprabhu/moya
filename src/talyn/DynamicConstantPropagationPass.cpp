#include "Call.h"
#include "JITContext.h"
#include "Summary.h"
#include "common/APITypes.h"
#include "common/Config.h"
#include "common/DataLayout.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/Analysis/LoopInfo.h>
#include <llvm/Config/llvm-config.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class DynamicConstantPropagationPass : public FunctionPass {
private:
  JITID fid;
  const moya::Vector<moya::CallArg> args;
  const moya::JITContext& conf;
  const moya::Summary& summary;
  moya::ConstantGenerator& cgen;
  moya::DataLayout layout;

private:
  bool replaceArg(Function&, unsigned);
  bool propagate(Value*, const moya::Summary::Targets&, const void*);

public:
  static char ID;

public:
  explicit DynamicConstantPropagationPass(JITID,
                                          const moya::Vector<moya::CallArg>&,
                                          RegionID,
                                          const moya::JITContext&);

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnFunction(Function&);
};

DynamicConstantPropagationPass::DynamicConstantPropagationPass(
    JITID fid,
    const moya::Vector<moya::CallArg>& args,
    RegionID region,
    const moya::JITContext& conf)
    : FunctionPass(ID), fid(fid), args(args), conf(conf),
      summary(conf.getStore(region, fid)),
      cgen(conf.getConstantGenerator(fid)) {
  ;
}

StringRef DynamicConstantPropagationPass::getPassName() const {
  return "Moya Dynamic Constant Propagation";
}

void DynamicConstantPropagationPass::getAnalysisUsage(AnalysisUsage&) const {
  ;
}

bool DynamicConstantPropagationPass::propagate(
    Value* inst,
    const moya::Summary::Targets& addrs,
    const void* ptr) {
  bool changed = false;

  if(auto* load = dyn_cast<LoadInst>(inst)) {
    Type* ptrTy = load->getPointerOperand()->getType();
    Type* elemTy = dyn_cast<PointerType>(ptrTy)->getElementType();

    if(summary.isStatusConstant(addrs) or summary.isStatusUsedInKey(addrs)) {
      if(ptr and elemTy->isPointerTy()) {
        const auto* nextPtr = *reinterpret_cast<const void* const*>(ptr);
        const moya::Summary::Targets targets = summary.getTargets(addrs);

        unsigned i = 0;
        for(Use& u : load->uses()) {
          changed |= propagate(u.getUser(), targets, nextPtr);
          // This is here because RAUW will invalidate the iterator
          if(i++ > load->getNumUses())
            break;
        }
      } else if(ptr) {
        Value* repl = nullptr;
        if(auto* cst = dyn_cast<CastInst>(load->getPointerOperand())) {
          Constant* c = cgen.getConstant(
              dyn_cast<PointerType>(cst->getOperand(0)->getType())
                  ->getElementType(),
              ptr);
          repl = CastInst::CreateBitOrPointerCast(c, elemTy, "", load);
        } else {
          repl = cgen.getConstant(elemTy, ptr);
        }
        // Should find the number of uses before they are replaced.
        changed |= (load->getNumUses() > 0);
        load->replaceAllUsesWith(repl);
        // moya_message("Replacing: " << *load << "\n  ==> " << *repl);
      }
    }
  } else if(auto* gep = dyn_cast<GetElementPtrInst>(inst)) {
    if(ptr and gep->hasAllConstantIndices()) {
      int64_t offset = layout.getOffsetInGEP(gep);
      const void* nextPtr = &reinterpret_cast<const byte*>(ptr)[offset];

      moya::Summary::Targets nextAddrs;
      if(moya::Metadata::hasGEPIndex(gep)) {
        for(moya::Summary::Address addr : addrs)
          nextAddrs.push_back(addr + moya::Metadata::getGEPIndex(gep));
      } else {
        for(moya::Summary::Address addr : addrs)
          nextAddrs.push_back(summary.propagate(
              gep, gep->getPointerOperandType(), 1, {}, addr));
      }

      unsigned i = 0;
      for(Use& u : gep->uses()) {
        changed |= propagate(u.getUser(), nextAddrs, nextPtr);
        if(i++ > gep->getNumUses())
          break;
      }
    }
  } else if(auto* cst = dyn_cast<CastInst>(inst)) {
    unsigned i = 0;
    for(Use& u : cst->uses()) {
      changed |= propagate(u.getUser(), addrs, ptr);
      if(i++ > cst->getNumUses())
        break;
    }
  } else if(auto* phi = dyn_cast<PHINode>(inst)) {
    if(moya::LLVMUtils::arePHINodeIncomingValuesEqual(phi)) {
      unsigned i = 0;
      for(Use& u : phi->uses()) {
        // FIXME?: Is this correct?
        changed |= propagate(u.getUser(), addrs, ptr);
        if(i++ > phi->getNumUses())
          break;
      }
    }
  }

  return changed;
}

bool DynamicConstantPropagationPass::replaceArg(Function& f, unsigned argno) {
  bool changed = false;
  Argument* arg = moya::LLVMUtils::getArgument(f, argno);

  if(not moya::Metadata::hasIgnore(arg)) {
    if(arg->getType()->isPointerTy()) {
      // The arguments to the call in args are actually points to the stack
      // where the arguments are saved. So even if a scalar were being passed
      // args[argno] would be a pointer. This is equivalent to passing
      // arguments to the function via memory (stack) instead of registers.
      // This is simulated in the summary as well with each argument having an
      // address associated with it. This way, we will get a single address
      // for the function argument, and the depth of the addresses will match
      // The summary address could have more than one target associated with it
      //
      const moya::Summary::Targets& addrs
          = summary.getTargets(summary.lookup(argno));
      const auto* ptr = reinterpret_cast<const void*>(args.at(argno).getArg());
      for(Use& u : arg->uses())
        changed |= propagate(u.getUser(), addrs, ptr);
    } else {
      const void* ptr = &args.at(argno).getArg();
      Constant* c = cgen.getConstant(arg->getType(), ptr);
      changed |= arg->getNumUses();
      // moya_message("Replacing: " << *arg << "\n  ==> " << *c);
      arg->replaceAllUsesWith(c);
    }
  }

  return changed;
}

bool DynamicConstantPropagationPass::runOnFunction(Function& f) {
  if(moya::Metadata::getJITID(f) != fid)
    return false;

  moya_message(getPassName());

  bool changed = false;

  layout.reset(*moya::LLVMUtils::getModule(f));

  for(unsigned i = 0; i < args.size(); i++)
    changed |= replaceArg(f, i);

  for(GlobalVariable& g : f.getParent()->globals()) {
    if(not g.isConstant()) {
      auto* pty = moya::LLVMUtils::getAnalysisTypeAs<PointerType>(g);
      Type* gtype = pty->getElementType();
      moya::Summary::Address addr = summary.lookup(g.getName());
      // If the global variable is not used, there will be no cell at addr
      if(moya::LLVMUtils::isScalarTy(gtype) and summary.contains(addr)
         and summary.getStatus(addr).isConstant()) {
        g.setConstant(true);
      } else {
        const void* ptr = conf.getGlobalPtr(g.getName());
        moya::Summary::Targets addrs = {addr};
        for(Use& u : g.uses())
          if(auto* inst = dyn_cast<Instruction>(u.getUser()))
            if(moya::LLVMUtils::getFunction(inst) == &f)
              changed |= propagate(inst, addrs, ptr);
      }
    }
  }

  return changed;
}

char DynamicConstantPropagationPass::ID = 0;

// static const char* name = "moya-jit-constants";
// static const char* descr
//     = "Plugs semi-constants (should ONLY be used at runtime)";
// static const bool cfg = false;
// static const bool analysis = false;

// static RegisterPass<DynamicConstantPropagationPass>
//     X(name, descr, cfg, analysis);

Pass* createDynamicConstantPropagationPass(
    JITID fid,
    const moya::Vector<moya::CallArg>& args,
    RegionID region,
    const moya::JITContext& conf) {
  return new DynamicConstantPropagationPass(fid, args, region, conf);
}
