#ifndef MOYA_TALYN_MACHINE_CODE_CACHE_MANAGER_H
#define MOYA_TALYN_MACHINE_CODE_CACHE_MANAGER_H

#include "common/APITypes.h"
#include "common/CacheManagerBase.h"
#include "common/Vector.h"

#include <llvm/IR/Module.h>
#include <llvm/Support/MemoryBuffer.h>

namespace moya {

class MachineCodeCacheManager : public CacheManagerBase {
public:
  MachineCodeCacheManager();
  virtual ~MachineCodeCacheManager() = default;

  void setSubdir(const std::string&);
  void writePTX(const std::string&, const std::string&);
  bool hasELF(RegionID,
              JITID,
              FunctionSignatureBasic,
              FunctionSignatureTuning,
              const std::string&) const;
  void writeELF(llvm::Module&,
                RegionID,
                JITID,
                FunctionSignatureBasic,
                FunctionSignatureTuning,
                const std::string&);
  llvm::MemoryBuffer* getELF(RegionID,
                             JITID,
                             FunctionSignatureBasic,
                             FunctionSignatureTuning,
                             const std::string&);
};

} // namespace moya

#endif // MOYA_TALYN_MACHINE_CODE_CACHE_MANAGER_H
