#ifndef MOYA_DYLD_MEMORY_MANAGER_H
#define MOYA_DYLD_MEMORY_MANAGER_H

#include "JITContext.h"

#include <llvm/ExecutionEngine/SectionMemoryManager.h>

#include <string>

namespace moya {

class DyldMemoryManager : public llvm::SectionMemoryManager {
private:
  const JITContext& jitContext;

public:
  DyldMemoryManager(const JITContext&);

  // FIXME: This function has been deprecated.
  // Need to override findSymbol instead
  uint64_t getSymbolAddress(const std::string&);
};

} // namespace moya

#endif // MOYA_DYLD_MEMORY_MANAGER_H
