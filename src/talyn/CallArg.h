#ifndef MOYA_TALYN_CALL_ARG_H
#define MOYA_TALYN_CALL_ARG_H

#include "common/APITypes.h"

#include <cstring>

namespace moya {

enum class ArgType {
  Bool,
  Int8,
  Int16,
  Int32,
  Int64,
  Float,
  Double,
  LongDouble,

  // Generic pointer type. Typically, this is a pointer to a struct/class
  Ptr,
  BoolPtr,
  Int8Ptr,
  Int16Ptr,
  Int32Ptr,
  Int64Ptr,
  FloatPtr,
  DoublePtr,
  LongDoublePtr,

  // Pointer to pointer type. Often appears in Fortran code when structs are
  // passed as pointer-to-pointer
  PtrPtr,
};

enum class ArgDeref {
  // The argument will not be used to specialize the function or to in
  // computing the signature
  Ignore,

  // The argument is used as-is in computing the signature and may also
  // be used in specializing the function. The argument may be a pointer
  // in which case the value of the pointer (the address) gets used in
  // computing the signature
  None,

  // Only valid for pointer types. In this case, the pointer is dereferenced
  // and the dereferenced value is used in the signature.
  One,

  // Only valid for array types. In this case, the argument itself will be
  // a pointer to the array. But the contents of the array will be used
  // in computing the signature
  Many,
};

class CallArg {
public:
  using Raw = uint128_t;

private:
  Raw arg;
  ArgType type;
  ArgDeref deref;
  unsigned count;

public:
  CallArg() = default;

  template<typename T>
  void reset(const T, ArgType, ArgDeref, unsigned = 0);

  template<typename T>
  T getArgAs() const;

  const Raw& getArg() const;
  ArgType getType() const;
  ArgDeref getDeref() const;
  unsigned getCount() const;
};

} // namespace moya

#endif // MOYA_TALYN_CALL_ARG_H
