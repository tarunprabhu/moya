#include "SearchExhaustive.h"
#include "common/Diagnostics.h"

namespace moya {

SearchExhaustive::SearchExhaustive(SearchSpace& space)
    : SearchStrategy(space), finished(false) {
  ;
}

bool SearchExhaustive::isFinished() const {
  return finished;
}

bool SearchExhaustive::start() {
  for(SearchSpace::Key i = 0; i < space.getMinKey(); i++)
    vals.push_back(space.getMinVal(i));
  for(SearchSpace::Key i = space.getMinKey(); i <= space.getMaxKey(); i++)
    vals.push_back(space.getMinVal(i));

  setCurr(space.getEnumeration(vals));

  return vals.size();
}

bool SearchExhaustive::next() {
  if(isFinished())
    return false;

  if(not isStarted()) {
    bool started = start();
    if(not started)
      return false;
  } else {
    for(SearchSpace::Key key = space.getMaxKey(); key >= space.getMinKey();
        key--) {
      vals[key] += 1;
      if(vals[key] > space.getMaxVal(key)) {
        if(key == space.getMinKey()) {
          finished = true;
          break;
        } else {
          vals[key] = space.getMinVal(key);
        }
      } else {
        break;
      }
    }
  }

  setCurr(space.getEnumeration(vals));

  return not isFinished();
}

} // namespace moya
