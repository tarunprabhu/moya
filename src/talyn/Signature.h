#ifndef MOYA_TALYN_SIGNATURE_H
#define MOYA_TALYN_SIGNATURE_H

#include "common/APITypes.h"
#include "common/Vector.h"

#include <type_traits>

namespace moya {

class Signature {
private:
  FunctionSignatureBasic signature;

public:
  Signature();

  // Non-pointer types are used to compute the signature
  template <typename T,
            typename std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
  void addComponent(T);

  // Signature components which are arrays. We need to treat them differently
  // from just pointers to those datatypes with length 1.
  // TODO: Arrays of structs are not supported
  template <typename T,
            typename std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
  void addComponent(const T*, int32_t);

  // Pointers are used directly in the component
  void addComponent(const void*);

  void reset();
  FunctionSignatureBasic get() const;
};

} // namespace moya

#endif // MOYA_TALYN_SIGNATURE_H
