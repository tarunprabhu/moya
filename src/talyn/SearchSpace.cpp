#include "SearchSpace.h"
#include "System.h"

#include <llvm/Support/raw_ostream.h>

#include <sstream>

namespace moya {

// Polly arguments. These have to passed as char* (not const char*) and they
// need an address, so keep them here
static char pollyUnprofitable[] = "-polly-process-unprofitable";
static char pollyTimeout[] = "-polly-dependences-computeout=0";

SearchSpace::SearchSpace(const System& system) : system(system) {
  keys = {"polly-target-1st-cache-level-size",
          "polly-target-2nd-cache-level-size",
          "polly-opt-fusion",
          "polly-tiling",
          "polly-2nd-level-tiling",
          "polly-vectorizer"};
  vals = {{},
          {},
          {"min", "max"},
          {"false", "true"},
          {"false", "true"},
          {"none", "stripmine"}};

  // The default values here are what Polly initializes itself with
  createCache(1, keys[0], 32768);
  createCache(2, keys[1], 262144);
  for(unsigned key = getMinKey(); key <= getMaxKey(); key++)
    for(unsigned val = getMinVal(key); val <= getMaxVal(key); val++)
      strings[getKeyVal(key, val)].reset(makeCStr(keys[key], vals[key][val]));
}

SearchSpace::KeyVal SearchSpace::getKeyVal(SearchSpace::Key key,
                                           SearchSpace::Val val) {
  SearchSpace::KeyVal kv = key;
  kv <<= 32;
  kv |= val;
  kv |= (1L << 63);
  return kv;
}

char* SearchSpace::makeCStr(const std::string& key, const std::string& val) {
  std::stringstream ss;
  ss << "-" << key << "=" << val;

  std::string s = ss.str();
  char* cstr = new char[s.size() + 1];
  memcpy(cstr, s.c_str(), s.size());
  cstr[s.size()] = '\0';

  return cstr;
}

void SearchSpace::createCache(unsigned cache,
                              const std::string& key,
                              unsigned long def) {
  SearchSpace::KeyVal idx = getKeyVal(cache - 1, 0);
  unsigned long size = system.getCacheSize(cache);
  if(size)
    strings[idx].reset(makeCStr(key, str(size)));
  else
    strings[idx].reset(makeCStr(key, str(def)));
}

Vector<char*>& SearchSpace::getPollyArgs(const Vector<SearchSpace::Val>& vals) {
  if(not args.contains(vals)) {
    args[vals].push_back(pollyUnprofitable);
    args[vals].push_back(pollyTimeout);
    for(SearchSpace::Key key = 0; key < vals.size(); key++)
      args[vals].push_back(strings[getKeyVal(key, vals[key])].get());
  }
  return args[vals];
}

SearchSpace::Enumeration
SearchSpace::getEnumeration(const Vector<SearchSpace::Val>& vals) const {
  // This basically takes each key, and uses as few bits as are necessary to
  // represent the maximum value of the key and OR's it into the result
  SearchSpace::Enumeration ret = 1L << 63;
  for(SearchSpace::Key key = getMinKey(), shift = 0; key <= getMaxKey();
      key++) {
    ret |= (vals[key] << shift);
    for(unsigned long p = 1; p <= getMaxVal(key); p *= 2)
      shift += 1;
  }
  return ret;
}

std::string
SearchSpace::getEnumerationStr(SearchSpace::Enumeration state) const {
  std::stringstream ss;
  for(SearchSpace::Key key = getMinKey(); key <= getMaxKey(); key++) {
    unsigned bits = 0;
    for(unsigned long p = 1; p <= getMaxVal(key); p *= 2)
      bits += 1;
    unsigned long mask = 1;
    for(unsigned i = 1; i < bits; i++)
      mask = (mask << 1) | 1;
    unsigned val = state & mask;
    ss << keys[key] << "=" << vals[key][val] << " ";
    state >>= bits;
  }
  return ss.str();
}

bool SearchSpace::isValid(SearchSpace::Enumeration state) const {
  return state != SearchSpace::getInvalidEnumeration();
}

SearchSpace::Enumeration SearchSpace::getInvalidEnumeration() {
  return 0;
}

SearchSpace::Key SearchSpace::getMinKey() const {
  return 2;
}

SearchSpace::Key SearchSpace::getMaxKey() const {
  return keys.size() - 1;
}

SearchSpace::Val SearchSpace::getMinVal(SearchSpace::Key key) const {
  return 0;
}

SearchSpace::Val SearchSpace::getMaxVal(SearchSpace::Key key) const {
  return vals.at(key).size() - 1;
}

} // namespace moya
