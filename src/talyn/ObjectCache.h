#ifndef MOYA_TALYN_OBJECT_CACHE_H
#define MOYA_TALYN_OBJECT_CACHE_H

#include <llvm/ExecutionEngine/ObjectCache.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/MemoryBuffer.h>

namespace moya {

// Because of the way this the runtime is currently implemented, there is a
// single LLVM module for every function that is JIT'ted. The object cache
// just contains a reference to a single module and the memory buffer
// containing the object code.
class ObjectCache : public llvm::ObjectCache {
protected:
  llvm::Module* mod;
  std::unique_ptr<llvm::MemoryBuffer> mbuf;

public:
  ObjectCache(llvm::Module* mod, llvm::MemoryBuffer* mbuf);
  virtual ~ObjectCache() = default;

  virtual void notifyObjectCompiled(const llvm::Module* mod,
                                    llvm::MemoryBufferRef obj);

  virtual std::unique_ptr<llvm::MemoryBuffer>
  getObject(const llvm::Module* mod);
};

} // namespace moya

#endif // MOYA_TALYN_OBJECT_CACHE_H
