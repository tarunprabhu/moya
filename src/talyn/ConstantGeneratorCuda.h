#ifndef MOYA_TALYN_CONSTANT_GENERATOR_CUDA_H
#define MOYA_TALYN_CONSTANT_GENERATOR_CUDA_H

#include "ConstantGenerator.h"

namespace moya {

class ConstantGeneratorCuda : public ConstantGenerator {
protected:
  uint64_t ival;
  float fval;
  double gval;

protected:
  virtual const void* getIntPointer(const void*, unsigned long);
  virtual const void* getFloatPointer(const void*);
  virtual const void* getDoublePointer(const void*);
  virtual void* getArrayPointer(const void*, unsigned long);
  virtual void deallocateHostBuffer(void*);

public:
  ConstantGeneratorCuda(const llvm::Module&);

  virtual ~ConstantGeneratorCuda() = default;
};

} // namespace moya

#endif // MOYA_TALYN_CONSTANT_GENERATOR_CUDA_H
