#include "MachineCodeCacheManager.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/FileSystemUtils.h"
#include "common/PassManager.h"
#include "common/PathUtils.h"

#include <llvm/Support/FileSystem.h>
#include <llvm/Support/raw_ostream.h>

#include <sstream>

namespace moya {

MachineCodeCacheManager::MachineCodeCacheManager()
    : CacheManagerBase(Config::getEnvCacheDir()) {
  ;
}

void MachineCodeCacheManager::setSubdir(const std::string& subdir) {
  setCacheDir(PathUtils::join(getDirectory(), subdir));
}

void MachineCodeCacheManager::writePTX(const std::string& code,
                                       const std::string& label) {
  std::error_code ec;
  std::string filename = getFilename(label, "ptx");
  llvm::raw_fd_ostream fs(filename, ec, llvm::sys::fs::OpenFlags::F_None);

  fs << code;

  fs.close();
}

static std::string getFileBase(FunctionSignatureBasic sig,
                               FunctionSignatureTuning variant) {
  std::stringstream name;
  name << sig << "_" << variant;

  return name.str();
}

static std::string
getSubpath(RegionID region, JITID key, const std::string& hash) {
  return PathUtils::join(moya::str(region), moya::str(key), hash);
}

bool MachineCodeCacheManager::hasELF(RegionID region,
                                     JITID key,
                                     FunctionSignatureBasic sig,
                                     FunctionSignatureTuning variant,
                                     const std::string& hash) const {
  return FileSystemUtils::exists(getFilename(
      getSubpath(region, key, hash), getFileBase(sig, variant), "o"));
}

llvm::MemoryBuffer*
MachineCodeCacheManager::getELF(RegionID region,
                                JITID key,
                                FunctionSignatureBasic sig,
                                FunctionSignatureTuning variant,
                                const std::string& hash) {
  std::string filename = getFilename(
      getSubpath(region, key, hash), getFileBase(sig, variant), "o");

  auto buf = llvm::MemoryBuffer::getFile(filename);
  if(not buf) {
    moya_error("Error opening ELF file: " << filename << ". "
                                          << buf.getError().message());
    return nullptr;
  }

  return buf.get().release();
}

void MachineCodeCacheManager::writeELF(llvm::Module& module,
                                       RegionID region,
                                       JITID key,
                                       FunctionSignatureBasic sig,
                                       FunctionSignatureTuning variant,
                                       const std::string& hash) {
  std::error_code ec;
  std::string filename = getFilename(
      getSubpath(region, key, hash), getFileBase(sig, variant), "o");
  llvm::raw_fd_ostream fs(filename, ec, llvm::sys::fs::OpenFlags::F_None);
  if(ec)
    moya_error("Error opening output object file: " << filename);

  PassManager writer(module);
  writer.addEmitMCFilePass(fs);
  writer.run(module);
}

} // namespace moya
