#include "TalynCuda.h"
#include "Globals.h"
#include "PassManager.h"
#include "common/Diagnostics.h"

#include <llvm/ADT/SmallStringUtils.h>

using namespace llvm;

static void check(CUresult err) {
  if(err != CUDA_SUCCESS)
    moya_error("Cuda function failed");
}

TalynCuda::TalynCuda(const JITContext& jitContext,
                     CacheMgr& cacheMgr,
                     Stats& stats,
                     SearchSpace& moya::space)
    : Talyn(jitContext,
            cacheMgr,
            stats,
            moya::space,
            SourceLang::CXX,
            new DemanglerCXX()) {
  check(cuInit(0));
  check(cuDeviceGet(&device, 0));
  check(cuCtxCreate(&context, 0, device));
}

std::string TalynCuda::getPtx(Module& module) {
  SmallString<1024> ptxBuf;
  raw_svector_ostream ptxOut(ptxBuf);
  moya::PassManager ptxPM(module);
  ptxPM.addEmitAssemblerFilePass(ptxOut);
  ptxPM.run(module);

  return ptxBuf.str();
}

CUmodule TalynCuda::createCudaModule() {
  newms.emplace_back();
  return newms.back();
}

CUfunction TalynCuda::createCudaFunction() {
  newfs.emplace_back();
  return newfs.back();
}

void TalynCuda::compile(JITID key, FunctionSig sig) {
  moya_error("Tuning not implemented for Cuda");
}

void TalynCuda::compile(JITID key,
                        FunctionSig sig,
                        const moya::Vector<const byte*>& args) {
  std::string fname = jitContext.getName(key);
  Module& module = getClonedModule(jitContext.getModule(key));
  FunctionVersion version = moya::Config::getUntunedFunctionVersion();

  runMoyaPasses(module, key, sig, args);
  runOptimizationPasses(module, key, sig, version);

  std::string ptx = getPtx(module);
  writePTXToDiskCache(ptx, key, sig, version);

  CUmodule cudaModule = createCudaModule();
  CUfunction cudaFunction = createCudaFunction();
  check(cuModuleLoadDataEx(&cudaModule, ptx.c_str(), 0, 0, 0));
  check(cuModuleGetFunction(&cudaFunction, cudaModule, fname.c_str()));

  update(key, sig, version, cudaFunction);
}

FunctionSignatureBasic
TalynCuda::getSignature(JITID key,
                        const moya::Vector<const byte*>& args) const {
  Signature signature;
  for(unsigned argno = 0; argno < nargs; argno++) {
    if(::contains(fargsToIgnore.at(key), argno))
      continue;

    Type* ty = getArgumentType(f, argno);
    const byte* ptr = args.at(argno);
    if(PointerType* pty = dyn_cast<PointerType>(ty)) {
      const void* arg = *reinterpret_cast<const void**>(ptr);
      signature.addComponent(arg);
    } else if(isScalarTy(ty)) {
      if(ty->isIntegerTy(1))
        signature.addComponent(*reinterpret_cast<const bool*>(ptr));
      else if(ty->isIntegerTy(8))
        signature.addComponent(*reinterpret_cast<const int8_t*>(ptr));
      else if(ty->isIntegerTy(16))
        signature.addComponent(*reinterpret_cast<const int16_t*>(ptr));
      else if(ty->isIntegerTy(32))
        signature.addComponent(*reinterpret_cast<const int32_t*>(ptr));
      else if(ty->isIntegerTy(64))
        signature.addComponent(*reinterpret_cast<const int64_t*>(ptr));
      else if(ety->isFloatTy())
        signature.addComponent(*reinterpret_cast<const float*>(ptr));
      else if(ety->isDoubleTy())
        signature.addComponent(*reinterpret_cast<const double*>(ptr));
      else if(ety->isX86_FP80Ty())
        signature.addComponent(*reinterpret_cast<const long double*>(ptr));
      else
        moya_error("Cannot dereference type for signature: " << *ty);
    } else {
      moya_error("Unsupported type in key generation: " << farg);
    }
  }

  return signature.get();
}

bool TalynCuda::isCompiled(JITID key, FunctionSig sig) const {
  if(::contains(specialized, key))
    return ::contains(specialized.at(key), sig);
  return false;
}

void* TalynCuda::getCompiled(JITID key, FunctionSig sig) const {
  FunctionVersion version = moya::Config::getUntunedFunctionVersion();
  return specialized.at(key).at(sig).at(version);
}

void TalynCuda::executeCuda(CUfunction cudaFunction,
                            unsigned gridX,
                            unsigned gridY,
                            unsigned gridZ,
                            unsigned blockX,
                            unsigned blockY,
                            unsigned blockZ,
                            unsigned sharedMemBytes,
                            CUstream hstream,
                            void** kernelParams) {
  check(cuLaunchKernel(cudaFunction,
                       gridX,
                       gridY,
                       gridZ,
                       blockX,
                       blockY,
                       blockZ,
                       sharedMemBytes,
                       hstream,
                       kernelParams,
                       NULL));
}

void TalynCuda::update(JITID key,
                       FunctionSig sig,
                       FunctionVersion version,
                       void* fptr) {
  specialized[key][sig][version] = static_cast<CUfunction>(fptr);
}

void TalynCuda::writePTXToDiskCache(std::string ptx,
                                    JITID key,
                                    FunctionSig sig,
                                    FunctionVersion version) {
  if(save and saveSpecialized) {
    std::stringstream name;
    name << "emitted_" << jitContext.getName(key) << "_" << jitted.at(key)
         << "_" << version << "_" << global_get_runtime().getID();
    cacheMgr.writePTX(ptx, name.str());
  }
}
