#include "TalynCXX.h"
#include "common/LLVMUtils.h"

using namespace llvm;

namespace moya {

TalynCXX::TalynCXX(const JITContext& jitContext,
                   MachineCodeCacheManager& mcCacheMgr,
                   RuntimeCacheManager& rtCacheMgr,
                   Stats& stats,
                   SearchSpace& space)
    : TalynBase(jitContext,
                mcCacheMgr,
                rtCacheMgr,
                stats,
                space,
                SourceLang::CXX) {
  ;
}

template <typename T>
void TalynCXX::registerArgImpl(JITID key,
                               unsigned i,
                               ArgType argType,
                               const T* ptr,
                               ArgDeref deref) {
  // This will only be used on arguments that are passed by reference, so
  // it is guaranteed to never be NULL and it is always safe to use it in the
  // key
  const Summary& store = jitContext.getStore(region, key);
  Status status = store.getStatus(static_cast<unsigned>(i));
  if(status.isDerefInKey())
    // This will only be done when the parameter is passed by reference
    // in which case it will always be safe to dereference the pointer
    call.at(key).registerArg(i,
                             argType,
                             reinterpret_cast<const void*>(ptr),
                             *ptr,
                             deref == ArgDeref::Ignore ? ArgDeref::Ignore
                                                       : ArgDeref::One);
  else
    call.at(key).registerArg(
        i, argType, reinterpret_cast<const void*>(ptr), deref);
}

void TalynCXX::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const bool* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynCXX::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const int8_t* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynCXX::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const int16_t* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynCXX::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const int32_t* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynCXX::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const int64_t* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynCXX::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const float* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynCXX::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const double* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynCXX::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const long double* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynCXX::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const void** arg,
                           ArgDeref deref) {
  TalynBase::registerArg(
      key, i, type, reinterpret_cast<const void*>(arg), deref);
}

} // namespace moya
