#include "AAResult.h"
#include "common/Diagnostics.h"
#include "common/Metadata.h"

#include <llvm/IR/InstIterator.h>

namespace moya {

AAResult::AAResult(const Summary& summary)
    : llvm::AAResultBase<AAResult>(), summary(summary) {
  ;
}

Summary::Address AAResult::lookup(const llvm::Value* v) const {
  if(const auto* inst = llvm::dyn_cast<llvm::Instruction>(v)) {
    if(Metadata::hasID(inst)) {
      MoyaID id = Metadata::getID(inst);
      if(summary.hasUniqueAddress(id))
        return summary.getUniqueAddress(id);
    }
  } else if(const auto* arg = llvm::dyn_cast<llvm::Argument>(v)) {
    if(arg->getType()->isPointerTy()) {
      Summary::Address addr = summary.lookup(arg->getArgNo());
      const Summary::Targets& targets = summary.getTargets(addr);
      if(targets.size() == 1)
        return targets.at(0);
      // else
      //   moya_error("Unsupported: multiple targets in " << *v);
    }
  }
  return Summary::null;
}

llvm::AliasResult AAResult::alias(const llvm::MemoryLocation& loc1,
                                  const llvm::MemoryLocation& loc2) {
  Summary::Address addr1 = lookup(loc1.Ptr);
  Summary::Address addr2 = lookup(loc2.Ptr);

  if(addr1 == Summary::null or addr2 == Summary::null)
    return llvm::AliasResult::MayAlias;
  else if(addr1 != addr2)
    return llvm::AliasResult::NoAlias;
  return llvm::AliasResult::MayAlias;
}

} // namespace moya
