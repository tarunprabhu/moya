#include "LibTalyn.h"
#include "Runtime.h"
#include "common/APIDecls.h"
#include "common/Diagnostics.h"
#include "common/StringUtils.h"

#include <llvm/Support/raw_ostream.h>

using moya::ArgType;
using moya::ArgDeref;

// Moya interface functions

extern "C" void MOYA_FN_ENTER_REGION(RegionID id) {
  moya_message("moya_enter_region(" << id << ")");
  moya::talyn_get_runtime().enterRegion(id);
}

extern "C" void MOYA_FN_EXIT_REGION(RegionID id) {
  moya_message("moya_exit_region(" << id << ")");
  moya::talyn_get_runtime().exitRegion(id);
}

extern "C" int MOYA_FN_IS_IN_REGION(void) {
  return moya::talyn_get_runtime().isInRegion();
}

extern "C" void MOYA_FN_COMPILE(JITID key) {
  moya::talyn_get_runtime().compile(key);
}

extern "C" void MOYA_FN_BEGIN_CALL(JITID key) {
  moya_message("moya_call(" << key << ")");
  moya::talyn_get_runtime().beginCall(key);
}

extern "C" void
MOYA_FN_REGISTER_ARG(i1)(JITID key, unsigned i, const bool arg, bool ignore) {
  moya_message("moya_register_arg<i1>(" << key << ", " << arg << ")");
  moya::talyn_get_runtime().registerArg(
      key, i, ArgType::Bool, arg, ignore ? ArgDeref::Ignore : ArgDeref::None);
}

extern "C" void
MOYA_FN_REGISTER_ARG(i8)(JITID key, unsigned i, const int8_t arg, bool ignore) {
  moya_message("moya_register_arg<i8>(" << key << ", " << arg << ")");
  moya::talyn_get_runtime().registerArg(
      key, i, ArgType::Int8, arg, ignore ? ArgDeref::Ignore : ArgDeref::None);
}

extern "C" void MOYA_FN_REGISTER_ARG(i16)(JITID key,
                                          unsigned i,
                                          const int16_t arg,
                                          bool ignore) {
  moya_message("moya_register_arg<i16>(" << key << ", " << arg << ")");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::Int16,
                                        arg,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::None);
}

extern "C" void MOYA_FN_REGISTER_ARG(i32)(JITID key,
                                          unsigned i,
                                          const int32_t arg,
                                          bool ignore) {
  moya_message("moya_register_arg<i32>(" << key << ", " << arg << ")");
  moya::talyn_get_runtime().registerArg(
      key, i, ArgType::Int32, arg, ignore ? ArgDeref::Ignore : ArgDeref::None);
}

extern "C" void MOYA_FN_REGISTER_ARG(i64)(JITID key,
                                          unsigned i,
                                          const int64_t arg,
                                          bool ignore) {
  moya_message("moya_register_arg<i64>(" << key << ", " << arg << ")");
  moya::talyn_get_runtime().registerArg(
      key, i, ArgType::Int64, arg, ignore ? ArgDeref::Ignore : ArgDeref::None);
}

extern "C" void
MOYA_FN_REGISTER_ARG(f32)(JITID key, unsigned i, const float arg, bool ignore) {
  moya_message("moya_register_arg<float>(" << key << ", " << arg << ")");
  moya::talyn_get_runtime().registerArg(
      key, i, ArgType::Float, arg, ignore ? ArgDeref::Ignore : ArgDeref::None);
}

extern "C" void MOYA_FN_REGISTER_ARG(f64)(JITID key,
                                          unsigned i,
                                          const double arg,
                                          bool ignore) {
  moya_message("moya_register_arg<double>(" << key << ", " << arg << ")");
  moya::talyn_get_runtime().registerArg(
      key, i, ArgType::Double, arg, ignore ? ArgDeref::Ignore : ArgDeref::None);
}

extern "C" void MOYA_FN_REGISTER_ARG(f80)(JITID key,
                                          unsigned i,
                                          const long double arg,
                                          bool ignore) {
  moya_message("moya_register_arg<long double>(" << key << ", "
                                                 << moya::str(arg) << ")");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::LongDouble,
                                        arg,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::None);
}

extern "C" void
MOYA_FN_REGISTER_ARG(ptr)(JITID key, unsigned i, const void* arg, bool ignore) {
  moya_message("moya_register_arg<void*>(" << key << ", " << arg << ")");
  moya::talyn_get_runtime().registerArg(
      key, i, ArgType::Ptr, arg, ignore ? ArgDeref::Ignore : ArgDeref::None);
}

extern "C" void
MOYA_FN_REGISTER_ARG(pi1)(JITID key, unsigned i, const bool* arg, bool ignore) {
  moya_message("moya_register_arg<bool*>("
               << key << ", " << arg << " => "
               << (arg ? moya::str(*arg) : std::string("<null>")) << ")");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::BoolPtr,
                                        arg,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::None);
}

extern "C" void MOYA_FN_REGISTER_ARG(pi8)(JITID key,
                                          unsigned i,
                                          const int8_t* arg,
                                          bool ignore) {
  moya_message("moya_register_arg<i8*>("
               << key << ", " << arg << " => "
               << (arg ? moya::str(*arg) : std::string("<null>")) << ")");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::Int8Ptr,
                                        arg,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::None);
}

extern "C" void MOYA_FN_REGISTER_ARG(pi16)(JITID key,
                                           unsigned i,
                                           const int16_t* arg,
                                           bool ignore) {
  moya_message("moya_register_arg<i16*>("
               << key << ", " << arg << " => "
               << (arg ? moya::str(*arg) : std::string("<null>")) << ")");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::Int16Ptr,
                                        arg,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::None);
}

extern "C" void MOYA_FN_REGISTER_ARG(pi32)(JITID key,
                                           unsigned i,
                                           const int32_t* arg,
                                           bool ignore) {
  moya_message("moya_register_arg<i32*>("
               << key << ", " << arg << " => "
               << (arg ? moya::str(*arg) : std::string("<null>")) << ")");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::Int32Ptr,
                                        arg,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::None);
}

extern "C" void MOYA_FN_REGISTER_ARG(pi64)(JITID key,
                                           unsigned i,
                                           const int64_t* arg,
                                           bool ignore) {
  moya_message("moya_register_arg<i64*>("
               << key << ", " << arg << " => "
               << (arg ? moya::str(*arg) : std::string("<null>")) << ")");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::Int64Ptr,
                                        arg,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::None);
}

extern "C" void MOYA_FN_REGISTER_ARG(pf32)(JITID key,
                                           unsigned i,
                                           const float* arg,
                                           bool ignore) {
  moya_message("moya_register_arg<float*>("
               << key << ", " << arg << " => "
               << (arg ? moya::str(*arg) : std::string("<null>")) << ")");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::FloatPtr,
                                        arg,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::None);
}

extern "C" void MOYA_FN_REGISTER_ARG(pf64)(JITID key,
                                           unsigned i,
                                           const double* arg,
                                           bool ignore) {
  moya_message("moya_register_arg<double*>("
               << key << ", " << arg << " => "
               << (arg ? moya::str(*arg) : std::string("<null>")) << ")");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::DoublePtr,
                                        arg,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::None);
}

extern "C" void MOYA_FN_REGISTER_ARG(pf80)(JITID key,
                                           unsigned i,
                                           const long double* arg,
                                           bool ignore) {
  moya_message("moya_register_arg<long double*>("
               << key << ", " << arg << " => "
               << (arg ? moya::str(*arg) : std::string("<null>")) << ")");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::LongDoublePtr,
                                        arg,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::None);
}

extern "C" void MOYA_FN_REGISTER_ARG(pptr)(JITID key,
                                           unsigned i,
                                           const void** arg,
                                           bool ignore) {
  moya_message("moya_register_arg<void**>(" << key << ", " << arg << " => "
                                            << *arg << ")");
  moya::talyn_get_runtime().registerArg(
      key, i, ArgType::PtrPtr, arg, ignore ? ArgDeref::Ignore : ArgDeref::None);
}

extern "C" void MOYA_FN_REGISTER_ARG(
    ai1)(JITID key, unsigned i, const bool* ptr, int32_t num, bool ignore) {
  moya_message("moya_register_arg<bool[]>(" << key << ", " << ptr << "[" << num
                                            << "])");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::Bool,
                                        ptr,
                                        num,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::Many);
}

extern "C" void MOYA_FN_REGISTER_ARG(
    ai8)(JITID key, unsigned i, const int8_t* ptr, int32_t num, bool ignore) {
  moya_message("moya_register_arg<i8[]>(" << key << ", " << ptr << "[" << num
                                          << "])");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::Int8,
                                        ptr,
                                        num,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::Many);
}

extern "C" void MOYA_FN_REGISTER_ARG(
    ai16)(JITID key, unsigned i, const int16_t* ptr, int32_t num, bool ignore) {
  moya_message("moya_register_arg<i16[]>(" << key << ", " << ptr << "[" << num
                                           << "])");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::Int16,
                                        ptr,
                                        num,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::Many);
}

extern "C" void MOYA_FN_REGISTER_ARG(
    ai32)(JITID key, unsigned i, const int32_t* ptr, int32_t num, bool ignore) {
  moya_message("moya_register_arg<i32[]>(" << key << ", " << ptr << "[" << num
                                           << "])");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::Int32,
                                        ptr,
                                        num,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::Many);
}

extern "C" void MOYA_FN_REGISTER_ARG(
    ai64)(JITID key, unsigned i, const int64_t* ptr, int32_t num, bool ignore) {
  moya_message("moya_register_arg<i64[]>(" << key << ", " << ptr << "[" << num
                                           << "])");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::Int64,
                                        ptr,
                                        num,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::Many);
}

extern "C" void MOYA_FN_REGISTER_ARG(
    af32)(JITID key, unsigned i, const float* ptr, int32_t num, bool ignore) {
  moya_message("moya_register_arg<float[]>(" << key << ", " << ptr << "[" << num
                                             << "])");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::Float,
                                        ptr,
                                        num,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::Many);
}

extern "C" void MOYA_FN_REGISTER_ARG(
    af64)(JITID key, unsigned i, const double* ptr, int32_t num, bool ignore) {
  moya_message("moya_register_arg<double[]>(" << key << ", " << ptr << "["
                                              << num << "])");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::Double,
                                        ptr,
                                        num,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::Many);
}

extern "C" void MOYA_FN_REGISTER_ARG(af80)(JITID key,
                                           unsigned i,
                                           const long double* ptr,
                                           int32_t num,
                                           bool ignore) {
  moya_message("moya_register_arg<long double[]>(" << key << ", " << ptr << "["
                                                   << num << "])");
  moya::talyn_get_runtime().registerArg(key,
                                        i,
                                        ArgType::LongDouble,
                                        ptr,
                                        num,
                                        ignore ? ArgDeref::Ignore
                                               : ArgDeref::Many);
}

#ifdef ENABLED_CUDA
extern "C" void MOYA_FN_EXECUTE_CUDA(JITID key,
                                     FunctionSignatureBasic sig,
                                     unsigned grid_x,
                                     unsigned grid_y,
                                     unsigned grid_z,
                                     unsigned block_x,
                                     unsigned block_y,
                                     unsigned block_z,
                                     unsigned sharedMemBytes,
                                     CUstream stream,
                                     void** params) {
  moya::talyn_get_runtime().executeCuda(key,
                                        sig,
                                        grid_x,
                                        grid_y,
                                        grid_z,
                                        block_x,
                                        block_y,
                                        block_z,
                                        sharedMemBytes,
                                        stream,
                                        params);
}
#endif // ENABLED_CUDA

// extern "C" void MOYA_FN_STATS_START_CALL(JITID key) {
//   moya::talyn_get_runtime().statsStartCall(key);
// }

// extern "C" void MOYA_FN_STATS_STOP_CALL(JITID key) {
//   moya::talyn_get_runtime().statsStopCall(key);
// }

// extern "C" void MOYA_FN_STATS_START_OPTIMIZE(JITID key,
//                                             FunctionSignatureBasic sig) {
//   moya::talyn_get_runtime().statsStartOptimize(key, sig);
// }

// extern "C" void MOYA_FN_STATS_STOP_OPTIMIZE(JITID key,
//                                            FunctionSignatureBasic sig) {
//   moya::talyn_get_runtime().statsStopOptimize(key, sig);
// }

// extern "C" void MOYA_FN_STATS_START_CODEGEN(JITID key,
//                                             FunctionSignatureBasic sig) {
//   moya::talyn_get_runtime().startsStartCodegen(key, sig);
// }

// extern "C" void MOYA_FN_STATS_STOP_CODEGEN(JITID key,
//                                            FunctionSignatureBasic sig) {
//   moya::talyn_get_runtime().startsStartCodegen(key, sig);
// }

extern "C" void MOYA_FN_STATS_START_EXECUTE(JITID key,
                                            FunctionSignatureBasic sig,
                                            FunctionSignatureTuning version) {
  moya::talyn_get_runtime().statsStartExecute(key, sig, version);
}

extern "C" void MOYA_FN_STATS_STOP_EXECUTE(JITID key,
                                           FunctionSignatureBasic sig,
                                           FunctionSignatureTuning version) {
  moya::talyn_get_runtime().statsStopExecute(key, sig, version);
}

extern "C" void MOYA_FN_TOP_ERROR() {
  moya_error("Dummy redirect function called");
}

extern "C" void MOYA_FN_SENTINEL_START(MoyaID) {
  moya_error("Internal compiler error: Sentinel start function called");
}

extern "C" void MOYA_FN_SENTINEL_STOP(MoyaID) {
  moya_error("Internal compiler error: Sentintel stop function called");
}

extern "C" void __moya_debug_int(int64_t i) {
  llvm::errs() << "DEBUG: " << i << "\n";
}
