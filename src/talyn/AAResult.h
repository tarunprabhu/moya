#ifndef MOYA_TALYN_AA_RESULT_H
#define MOYA_TALYN_AA_RESULT_H

#include <llvm/Analysis/AliasAnalysis.h>
#include <llvm/IR/Function.h>

#include "common/Map.h"

#include "Summary.h"

namespace moya {

// Everything that we need to perform alias analysis is already in the source
// because we have annotated it all.
//
class AAResult : public llvm::AAResultBase<AAResult> {
protected:
  const Summary& summary;

protected:
  Summary::Address lookup(const llvm::Value*) const;

public:
  AAResult(const Summary&);

  llvm::AliasResult alias(const llvm::MemoryLocation&,
                          const llvm::MemoryLocation&);

  friend llvm::AAResultBase<AAResult>;
};

} // namespace moya

#endif // MOYA_TALYN_AA_RESULT_H
