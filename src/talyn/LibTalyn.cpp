#include "LibTalyn.h"
#include "Runtime.h"
#include "common/Config.h"
#include "common/Diagnostics.h"

#include <llvm/Support/TargetSelect.h>
#include <llvm/Support/raw_ostream.h>

namespace moya {

// This is the sole global moya object that is present and contains all of the
// state that needs to be maintained through the duration of the application.
thread_local static Runtime* runtimeSingleton;

static Runtime& talyn_new_runtime() {
  moya_message("Create Moya runtime");
  runtimeSingleton = new moya::Runtime();
  return talyn_get_runtime();
}

static void talyn_delete_runtime() {
  moya_message("Destroy Moya runtime");
  delete runtimeSingleton;
}

// This is only here to keep all the global variables in one place.
// Eventually, we're going to want to make Moya thread-safe, so everything
// that needs protection will be found here. The accessor methods should
// contain all the locks
Runtime& talyn_get_runtime() {
  return *runtimeSingleton;
}

} // namespace moya

__attribute__((constructor)) static void libTalynInit(void) {
  moya_message("Initialize libTalyn");
  if(not moya::Config::getEnvDisabled()) {
    moya_message("Moya enabled");
    // FIXME: We shouldn't initialize everything because we only support a few
    // backends and LLVM could have been built to support many targets
    llvm::InitializeAllTargets();
    llvm::InitializeAllTargetInfos();
    llvm::InitializeAllTargetMCs();
    llvm::InitializeAllAsmParsers();
    llvm::InitializeAllAsmPrinters();
  }

  moya::talyn_new_runtime();
  moya::talyn_get_runtime().initialize();
}

__attribute__((destructor)) static void libTalynFini(void) {
  moya_message("Finalize libTalyn");
  moya::talyn_get_runtime().finalize();
  moya::talyn_delete_runtime();
}
