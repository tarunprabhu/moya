#include "ConstantGeneratorCuda.h"
#include "common/Config.h"
#include "common/Diagnostics.h"

#include <cuda_runtime_api.h>

using namespace llvm;

namespace moya {

ConstantGeneratorCuda::ConstantGeneratorCuda(const Module& module)
    : ConstantGenerator(module) {
  ;
}

const void* ConstantGeneratorCuda::getIntPointer(const void* gptr,
                                                 unsigned long size) {
  ival = 0;
  cudaMemcpy(&ival, gptr, size, cudaMemcpyDeviceToHost);
  return &ival;
}

const void* ConstantGeneratorCuda::getFloatPointer(const void* gptr) {
  cudaMemcpy(&fval, gptr, sizeof(float), cudaMemcpyDeviceToHost);
  return &fval;
}

const void* ConstantGeneratorCuda::getDoublePointer(const void* gptr) {
  cudaMemcpy(&gval, gptr, sizeof(double), cudaMemcpyDeviceToHost);
  return &gval;
}

void* ConstantGeneratorCuda::getArrayPointer(const void* gptr,
                                             unsigned long size) {
  void* lptr = new byte[size];
  cudaMemcpy(lptr, gptr, size, cudaMemcpyDeviceToHost);
  return lptr;
}

void ConstantGeneratorCuda::deallocateHostBuffer(void* gptr) {
  delete[] static_cast<byte*>(gptr);
}

} // namespace moya
