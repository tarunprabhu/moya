#include "TalynC.h"
#include "common/DemanglerNull.h"
#include "common/LLVMUtils.h"

using namespace llvm;

namespace moya {

TalynC::TalynC(const JITContext& jitContext,
               MachineCodeCacheManager& mcCacheMgr,
               RuntimeCacheManager& rtCacheMgr,
               Stats& stats,
               SearchSpace& space)
    : TalynBase(jitContext,
                mcCacheMgr,
                rtCacheMgr,
                stats,
                space,
                SourceLang::C) {
  ;
}

template <typename T>
void TalynC::registerArgImpl(JITID key,
                             unsigned i,
                             ArgType type,
                             const T* ptr,
                             ArgDeref deref) {
  call.at(key).registerArg(i, type, reinterpret_cast<const void*>(ptr), deref);
}

void TalynC::registerArg(JITID key,
                         unsigned i,
                         ArgType type,
                         const bool* arg,
                         ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynC::registerArg(JITID key,
                         unsigned i,
                         ArgType type,
                         const int8_t* arg,
                         ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynC::registerArg(JITID key,
                         unsigned i,
                         ArgType type,
                         const int16_t* arg,
                         ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynC::registerArg(JITID key,
                         unsigned i,
                         ArgType type,
                         const int32_t* arg,
                         ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynC::registerArg(JITID key,
                         unsigned i,
                         ArgType type,
                         const int64_t* arg,
                         ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynC::registerArg(JITID key,
                         unsigned i,
                         ArgType type,
                         const float* arg,
                         ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynC::registerArg(JITID key,
                         unsigned i,
                         ArgType type,
                         const double* arg,
                         ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynC::registerArg(JITID key,
                         unsigned i,
                         ArgType type,
                         const long double* arg,
                         ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynC::registerArg(JITID key,
                         unsigned i,
                         ArgType type,
                         const void** arg,
                         ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

} // namespace moya
