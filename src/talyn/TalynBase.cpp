#include "TalynBase.h"
#include "AAResult.h"
#include "DyldMemoryManager.h"
#include "JITContext.h"
#include "MachineCodeCacheManager.h"
#include "ObjectCache.h"
#include "Passes.h"
#include "RuntimeCacheManager.h"
#include "SearchSpace.h"
#include "Stats.h"
#include "common/APIDecls.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/PassManager.h"
#include "common/PathUtils.h"

#include <llvm/Analysis/BasicAliasAnalysis.h>
#include <llvm/Analysis/Passes.h>
#include <llvm/Analysis/ScalarEvolution.h>
#include <llvm/LinkAllPasses.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/Utils/Cloning.h>

#ifdef ENABLED_POLLY
#include <polly/ForwardOpTree.h>
#include <polly/LinkAllPasses.h>
#include <polly/ScopPass.h>
#endif // ENABLED_POLLY

#include <cstdlib>
#include <sstream>

using llvm::cast;
using llvm::dyn_cast;
using llvm::isa;
namespace legacy = llvm::legacy;

namespace moya {

// Polly arguments. These have to passed as char* (not const char*) and they
// need an address, so keep them here
static char pollyUnprofitable[] = "-polly-process-unprofitable";
static char pollyTimeout[] = "-polly-dependences-computeout=0";

TalynBase::TalynBase(const JITContext& jitContext,
                     MachineCodeCacheManager& mcCacheMgr,
                     RuntimeCacheManager& rtCacheMgr,
                     Stats& stats,
                     SearchSpace& searchSpace,
                     SourceLang lang)
    : jitContext(jitContext), mcCacheMgr(mcCacheMgr), rtCacheMgr(rtCacheMgr),
      stats(stats), searchSpace(searchSpace), lang(lang),
      region(Config::getInvalidRegionID()), saveCode(Config::isEnvSaveCode()),
      usePolly(Config::isEnvUsePolly()),
      saveOptReport(Config::isEnvSaveOptReport()) {
  for(JITID id : jitContext.getJITIDs())
    call.emplace(id, Call(id, jitContext.getNumArgs(id)));
}

llvm::ExecutionEngine& TalynBase::createExecutionEngine(llvm::Module& module) {
  std::string errStr;
  llvm::TargetOptions options;
  llvm::ExecutionEngine* ee
      = llvm::EngineBuilder(std::unique_ptr<llvm::Module>(&module))
            .setErrorStr(&errStr)
            .setOptLevel(llvm::CodeGenOpt::Aggressive)
            .setEngineKind(llvm::EngineKind::JIT)
            .setTargetOptions(options)
            .setMCJITMemoryManager(std::unique_ptr<llvm::RTDyldMemoryManager>(
                new DyldMemoryManager(jitContext)))
            .create();
  if(!ee)
    moya_error("Could not create execution engine: " << errStr);
  return *ee;
}

void TalynBase::enterRegion(RegionID id) {
  region = id;
}

void TalynBase::exitRegion(RegionID) {
  region = Config::getInvalidRegionID();
  specialized.clear();
}

bool TalynBase::isInRegion() const {
  return not Config::isInvalidRegionID(getCurrentRegion());
}

RegionID TalynBase::getCurrentRegion() const {
  return region;
}

llvm::Module& TalynBase::getClonedModule(llvm::Module& orig) {
  llvm::Module* pnew = CloneModule(orig).release();
  newms.insert(pnew);
  return *pnew;
}

llvm::ExecutionEngine& TalynBase::getExecutionEngine(llvm::Module& module,
                                                     JITID key) {
  llvm::ExecutionEngine& ee = createExecutionEngine(module);
  ConstantGenerator& cgen = jitContext.getConstantGenerator(key);

  const Summary& store = jitContext.getStore(region, key);
  for(std::string gname : store.getGlobals()) {
    if(llvm::GlobalVariable* gv = module.getNamedGlobal(gname)) {
      llvm::Type* type = gv->getType()->getElementType();
      const void* gptr = jitContext.getGlobalPtr(gname);

      if(gv->hasInternalLinkage() and gv->isConstant()) {
        if(not isa<llvm::PointerType>(type)
           and not isa<llvm::StructType>(type)) {
          if(llvm::Constant* init = cgen.getConstant(type, gptr)) {
            gv->setLinkage(llvm::GlobalValue::LinkageTypes::InternalLinkage);
            gv->setInitializer(init);
            if(store.getStatus(gname).isConstant())
              gv->setConstant(true);
            continue;
          }
        }
      }

      if(not gptr)
        moya_error("Problems initializing and linking global: " << *gv);

      std::string name = gv->getName();
      gv->setName(name + ".old");
      llvm::GlobalVariable* newGV = new llvm::GlobalVariable(
          module,
          gv->getType()->getElementType(),
          false,
          llvm::GlobalValue::LinkageTypes::ExternalLinkage,
          nullptr,
          name);
      gv->replaceAllUsesWith(newGV);
      gv->eraseFromParent();
      // The pointer will get cast to uint64_t immediately by ExecutionEngine
      // anyway, so why bother. Better than having to deal with the name
      // mangling nonsense if I use the other function:
      // addGlobalMapping(StringRef, uint64_t);
      ee.addGlobalMapping(newGV, const_cast<void*>(gptr));
    }
  }

  ees.insert(&ee);
  return ee;
}

bool TalynBase::setMoyaGlobals(llvm::Module& module,
                               FunctionSignatureBasic sig,
                               FunctionSignatureTuning version) {
  llvm::LLVMContext& context = module.getContext();
  llvm::Type* verTy = LLVMUtils::getFunctionSignatureTuningType(context);
  llvm::Type* sigTy = LLVMUtils::getFunctionSignatureBasicType(context);
  llvm::Constant* cver = llvm::ConstantInt::get(verTy, version, false);
  llvm::Constant* csig = llvm::ConstantInt::get(sigTy, sig, false);

  // If the function version has been optimized out,
  // find the calls to the stats functions because it will have been used
  // there

  for(llvm::Use& u : LLVMUtils::getStatsStartExecuteFunction(module)->uses())
    if(auto* call = dyn_cast<llvm::CallInst>(u.getUser())) {
      call->setArgOperand(1, csig);
      call->setArgOperand(2, cver);
    }

  for(llvm::Use& u : LLVMUtils::getStatsStopExecuteFunction(module)->uses())
    if(auto* call = dyn_cast<llvm::CallInst>(u.getUser())) {
      call->setArgOperand(1, csig);
      call->setArgOperand(2, cver);
    }

  return true;
}

#ifdef ENABLED_POLLY
void TalynBase::populatePollyPreparePasses(PassManager& pm, JITID) {
  pm.addModulePass(polly::createPollyCanonicalizePass());
}

void TalynBase::populatePollyPasses(PassManager& pm,
                                    JITID key,
                                    llvm::raw_ostream& os) {
  AAResult& aaResult = jitContext.getAAResult(region, key);
  std::function<void(llvm::Pass&, llvm::Function&, llvm::AAResults&)> cb
      = [&](llvm::Pass&, llvm::Function&, llvm::AAResults& aar) {
          aar.addAAResult(aaResult);
        };

  pm.addFunctionPass(createExternalAAWrapperPass(cb));
  pm.addFunctionPass(polly::createCodePreparationPass());
  pm.addFunctionPass(polly::createScopDetectionWrapperPassPass());
  pm.addFunctionPass(polly::createScopInfoRegionPassPass());
  pm.addFunctionPass(polly::createSimplifyPass(0));
  pm.addRegionPass(polly::createForwardOpTreePass());
  pm.addFunctionPass(polly::createDeLICMPass());
  pm.addFunctionPass(polly::createSimplifyPass(1));
  pm.addFunctionPass(polly::createDeadCodeElimPass());
  pm.addFunctionPass(polly::createPruneUnprofitablePass());
  pm.addFunctionPass(polly::createIslScheduleOptimizerPass());
  pm.addFunctionPass(polly::createIslAstInfoWrapperPassPass());
  pm.addFunctionPass(polly::createCodeGenerationPass());

  pm.addFunctionPass(createPollyASTPrinterPass(os));
}

bool TalynBase::runPollyPasses(llvm::Module& module,
                               JITID key,
                               FunctionSignatureBasic sig,
                               FunctionSignatureTuning variant,
                               const std::string& variantStr,
                               unsigned pollyArgc,
                               char** pollyArgv) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);
  llvm::Function& f = *module.getFunction(jitContext.getName(key));

  setMoyaGlobals(module, sig, variant);
  llvm::cl::ParseCommandLineOptions(pollyArgc, pollyArgv);

  PassManager pollyPrep(module);
  populatePollyPreparePasses(pollyPrep, key);
  pollyPrep.run(module);

  PassManager pollyPM(module);
  populatePollyPasses(pollyPM, key, ss);
  pollyPM.run(f);

  maybeSaveOptReport(key, sig, variant, variantStr, ss.str());

  return true;
}
#endif // ENABLED_POLLY

void TalynBase::populateConstantPropagationPasses(PassManager& pm,
                                                  JITID key,
                                                  const Vector<CallArg>& args) {
  pm.addFunctionPass(
      createDynamicConstantPropagationPass(key, args, region, jitContext));
}

void TalynBase::populatePreparePasses(PassManager&, JITID) {
  // Nothing to be done at this point, but it may be needed in the future
}

bool TalynBase::runMoyaPasses(llvm::Module& module,
                              JITID key,
                              FunctionSignatureBasic sig,
                              const Vector<CallArg>& args) {
  std::string fname = jitContext.getName(key);
  llvm::Function& f = *module.getFunction(fname);
  std::string optReportBuf;
  llvm::raw_string_ostream optReport(optReportBuf);

  FunctionSignatureTuning version = Config::getUntunedFunctionSignature();
  setMoyaGlobals(module, sig, version);

  PassManager dycpPreparePassMgr(module);
  populatePreparePasses(dycpPreparePassMgr, key);

  // XXX: When I run the default function passes, the return value is always
  // true even if the function hasn't appreciably changed. So I have to create
  // a separate pass manager for my constant propagation pass and another
  // which runs the default function passes
  PassManager dycpAuxPassMgr(module);
#ifdef ENABLED_POLLY
  // Because the tuner currently uses Polly, if we are ever tuning the function,
  // use the "polly-friendly" auxiliary passes. The default aggressive
  // dynamic constant propagation pass runs all the -O2 passes mainly to
  // maximize the number of dynamic constants that can be found. But this can
  // cause Polly and maybe even the compiler's regular optimization passes to
  // fail to optimize code that it really should.
  if(usePolly or jitContext.getTuningLevel(key)
     or Config::isEnvNoAggrConstProp())
    dycpAuxPassMgr.addConstPropPasses();
  else
    dycpAuxPassMgr.addOptimizationPasses(true, false, 2);
#else  // !ENABLED_POLLY
  if(Config::isEnvNoAggrConstProp())
    dycpAuxPassMgr.addConstPropPasses();
  else
    dycpAuxPassMgr.addOptimizationPasses(true, false, 2);
#endif // ENABLED_POLLY

  unsigned dycpIters = 0;
  PassManager dycpPassMgr(module);
  populateConstantPropagationPasses(dycpPassMgr, key, args);

  // Do this once outside. There may be functions where constants are only
  // found once some loop has been unrolled.
  dycpPreparePassMgr.run(f);
  while(dycpPassMgr.run(f))
    dycpAuxPassMgr.run(f);
  dycpAuxPassMgr.run(module);
  do {
    dycpIters = 0;
    while(dycpPassMgr.run(f)) {
      dycpIters += 1;
      dycpAuxPassMgr.run(f);
    }
    dycpAuxPassMgr.run(module);
  } while(dycpIters > 0);

  maybeSaveSpecializedArgs(args, key, sig, version);
  maybeSaveSpecializedModule(module, key, sig, version);

  return true;
}

void TalynBase::runOptimizationPasses(llvm::Module& module,
                                      JITID key,
                                      FunctionSignatureBasic sig,
                                      FunctionSignatureTuning version) {
  PassManager o3pm(module);
  o3pm.addOptimizationPasses(true, true, 3);
  o3pm.run(module);

  maybeSaveOptimizedModule(module, key, sig, version);
}

void* TalynBase::emitMachineCode(llvm::Module& module,
                                 JITID key,
                                 FunctionSignatureBasic sig,
                                 FunctionSignatureTuning version) {
  moya_message("Emit machine code");
  std::string fname = jitContext.getName(key);

  llvm::ExecutionEngine& ee = getExecutionEngine(module, key);
  ee.lock.lock();
  ee.finalizeObject();
  void* fptr = (void*)(ee.getFunctionAddress(fname));
  ee.lock.unlock();

  update(key, sig, version, fptr);
  moya_message("Done emit machine code");

  // FIXME: FunctionSignatureBasic is not usable as a cache key
  maybeSaveAssembler(module, key, sig, version);
  maybeCacheMachineCode(module, key, sig, version);

  return fptr;
}

void TalynBase::setBest(JITID key, FunctionSignatureBasic sig) {
  specialized[region][key][sig]
      = tuned[region][key][sig][stats.getBestVariant(region, key, sig)];
  doneSearching[region][key].insert(sig);
}

bool TalynBase::shouldExplore(JITID key, FunctionSignatureBasic sig) const {
  if(jitContext.getTuningLevel(key)) {
    // The function may not have been seen yet, so no search context has
    // been created. It doesn't make sense to explore anything in this case
    if(not(searches.contains(region) and searches.at(region).contains(key)
           and searches.at(region).at(key).contains(sig)))
      return false;

    // If the search space has been exhausted, there is nothing to explore.
    // Don't check for search.canExplore() because we still need to call
    // compileTuned() after the search space has been exhausted so the best
    // variant can be selected and set for future use
    if(doneSearching.contains(region) and doneSearching.at(region).contains(key)
       and doneSearching.at(region).at(key).contains(sig))
      return false;

    const Search& search = searches.at(region).at(key).at(sig);
    unsigned numCalls = stats.getNumCalls(region, key, sig, search.getCurr());
    if((numCalls < search.getThresholdNumCalls()))
      return false;
    return true;
  }
  return false;
}

bool TalynBase::isCompiled(JITID key, FunctionSignatureBasic sig) const {
#ifdef ENABLED_POLLY
  // If we should explore a new tuned option, then this should return false
  // so that the new compilation will be triggered in the redirecter function
  if(shouldExplore(key, sig))
    return false;
#endif // ENABLED_POLLY

  if(specialized.contains(region) and specialized.at(region).contains(key))
    return specialized.at(region).at(key).contains(sig);
  return false;
}

bool TalynBase::isCached(JITID key, FunctionSignatureBasic sig) const {
  if(mcCacheMgr.isEnabled())
    // FIXME: If the function has been tuned, retrieve the best version from
    // the cache
    return mcCacheMgr.hasELF(region,
                             key,
                             sig,
                             Config::getUntunedFunctionSignature(),
                             jitContext.getHash(key));

  return false;
}

void* TalynBase::getCompiled(JITID key, FunctionSignatureBasic sig) const {
  // #ifdef ENABLED_POLLY
  //   if(jitContext.getTuningLevel(key)) {
  //     const Search& search = searches.at(region).at(key).at(sig);
  //     FunctionSignatureTuning version = search.getCurr();
  //     if(version != Config::getUntunedFunctionSignature())
  //       return tuned.at(region).at(key).at(sig).at(version);
  //   }
  // #endif // ENABLED_POLLY

  return specialized.at(region).at(key).at(sig);
}

#ifdef ENABLED_POLLY
void* TalynBase::compileTuned(JITID key, FunctionSignatureBasic sig) {
  moya_message("Tuning compile: " << jitContext.getName(key));

  Search& search = searches.at(region).at(key).at(sig);
  // If there's nothing left to explore, find the best variant and set a flag
  // so that variant can be used in all subsequent uses
  if(not search.canExplore()) {
    setBest(key, sig);
    return getCompiled(key, sig);
  }

  stats.startCompile(key, sig);

  // If we aren't done, get the next version and the polly arguments
  // There is no easy way to pass parameters to the polly passes because the
  // functions to set the arguments are not exposed. So we create a fake
  // command line and parse it. It's really stupid.
  //
  Search::State opts = search.getNext();
  FunctionSignatureTuning version = opts.sig;
  unsigned pollyArgc = opts.argc;
  char** pollyArgv = opts.argv;

  // Either the entire search space has been searched, or there was an error
  // in either case, now is the time to pick the best function seen so far
  // and stick with it
  if(pollyArgc == 0) {
    setBest(key, sig);
    return getCompiled(key, sig);
  }

  llvm::Module& module = getClonedModule(search.getModule());

  stats.addVariant(key, sig, version, search.getCurrStr());
  stats.startOptimize(key, sig, version);

  runPollyPasses(
      module, key, sig, version, search.getCurrStr(), pollyArgc, pollyArgv);
  runOptimizationPasses(module, key, sig, version);
  moya_message("Done tuning compile");

  stats.stopOptimize(key, sig, version);
  stats.startCodegen(key, sig, version);

  void* fptr = emitMachineCode(module, key, sig, version);

  stats.stopCodegen(key, sig, version);
  stats.stopCompile(key, sig);

  return fptr;
}
#endif // ENABLED_POLLY

void* TalynBase::loadCached(JITID key, FunctionSignatureBasic sig) {
  moya_message("Load from cache: " << jitContext.getName(key));

  stats.startCompile(key, sig);

  // FIXME: Load the best variant if the function is tuned
  FunctionSignatureTuning variant = Config::getUntunedFunctionSignature();

  stats.startLoad(key, sig, variant);
  stats.addVariant(key, sig, variant, "");

  llvm::Module& mod = getClonedModule(jitContext.getModule(region, key));
  llvm::MemoryBuffer* buf
      = mcCacheMgr.getELF(region, key, sig, variant, jitContext.getHash(key));

  ObjectCache* cache = new ObjectCache(&mod, buf);
  llvm::ExecutionEngine& ee = getExecutionEngine(mod, key);
  ee.setObjectCache(cache);

  void* fptr = (void*)ee.getFunctionAddress(jitContext.getName(key));

  update(key, sig, variant, fptr);

  stats.stopLoad(key, sig, variant);
  stats.stopCompile(key, sig);

  return fptr;
}

void* TalynBase::compileBasic(JITID key, FunctionSignatureBasic sig) {
  moya_message("Normal compile: " << jitContext.getName(key));

  stats.startCompile(key, sig);

  FunctionSignatureTuning variant = Config::getUntunedFunctionSignature();

  llvm::Module& module = getClonedModule(jitContext.getModule(region, key));

  stats.addVariant(key, sig, variant, "");
  stats.startOptimize(key, sig, variant);

  runMoyaPasses(module, key, sig, call.at(key).getArgs());

#ifdef ENABLED_POLLY
  if(unsigned tuning = jitContext.getTuningLevel(key))
    searches[region][key].insert(
        std::make_pair(sig, Search(tuning, module, searchSpace)));
  if(usePolly) {
    Vector<char*> pollyArgv = {&pollyTimeout[0]};
    pollyArgv.push_back(&pollyUnprofitable[0]);
    runPollyPasses(
        module, key, sig, variant, "", pollyArgv.size(), pollyArgv.data());
  }
#endif // ENABLED_POLLY
  runOptimizationPasses(module, key, sig, variant);
  moya_message("Done normal compile");

  stats.stopOptimize(key, sig, variant);
  stats.startCodegen(key, sig, variant);

  void* fptr = emitMachineCode(module, key, sig, variant);

  stats.stopCodegen(key, sig, variant);
  stats.stopCompile(key, sig);

  return fptr;
}

void* TalynBase::compile(JITID key) {
  FunctionSignatureBasic sig = call.at(key).getSignature().get();
  void* fn = nullptr;

  moya_message("compile: " << isCompiled(key, sig) << " "
                           << isCached(key, sig));

  if(isCompiled(key, sig)) {
    fn = getCompiled(key, sig);
    stats.stopCall(key);
    call.at(key).reset();
  } else if(isCached(key, sig)) {
    stats.stopCall(key);
    fn = loadCached(key, sig);
    call.at(key).reset();
  }
#ifdef ENABLED_POLLY
  // A key will only be present if the function is to be tuned and it has
  // already been compiled. Also, before getting here, we will have checked
  // if there is anything in the search space left to explore. If there is
  // nothing left, then we will never get here
  else if(searches.contains(region) and searches.at(region).contains(key)
          and searches.at(region).at(key).contains(sig)) {
    stats.stopCall(key);
    fn = compileTuned(key, sig);
    call.at(key).reset();
  }
#endif // ENABLED_POLLY
  // If we aren't tuning, then we need to compile the basic version
  else {
    stats.stopCall(key);
    fn = compileBasic(key, sig);
    call.at(key).reset();
  }

  return fn;
}

void TalynBase::beginCall(JITID key) {
  stats.startCall(key);
  call.at(key).reset();
}

template <typename T,
          typename std::enable_if_t<std::is_arithmetic<T>::value, int>>
void TalynBase::registerArg(JITID key,
                            unsigned i,
                            ArgType type,
                            const T arg,
                            ArgDeref deref) {
  call.at(key).registerArg(i, type, arg, deref);
}

template void
TalynBase::registerArg(JITID, unsigned, ArgType, const bool, ArgDeref);
template void
TalynBase::registerArg(JITID, unsigned, ArgType, const int8_t, ArgDeref);
template void
TalynBase::registerArg(JITID, unsigned, ArgType, const int16_t, ArgDeref);
template void
TalynBase::registerArg(JITID, unsigned, ArgType, const int32_t, ArgDeref);
template void
TalynBase::registerArg(JITID, unsigned, ArgType, const int64_t, ArgDeref);
template void
TalynBase::registerArg(JITID, unsigned, ArgType, const float, ArgDeref);
template void
TalynBase::registerArg(JITID, unsigned, ArgType, const double, ArgDeref);
template void
TalynBase::registerArg(JITID, unsigned, ArgType, const long double, ArgDeref);

template <typename T,
          typename std::enable_if_t<std::is_arithmetic<T>::value, int>>
void TalynBase::registerArg(JITID key,
                            unsigned i,
                            ArgType type,
                            const T* ptr,
                            int32_t num,
                            ArgDeref deref) {
  call.at(key).registerArg(i, type, ptr, num, deref);
}

template void TalynBase::registerArg(JITID,
                                     unsigned,
                                     ArgType,
                                     const bool*,
                                     int32_t,
                                     ArgDeref);
template void TalynBase::registerArg(JITID,
                                     unsigned,
                                     ArgType,
                                     const int8_t*,
                                     int32_t,
                                     ArgDeref);
template void TalynBase::registerArg(JITID,
                                     unsigned,
                                     ArgType,
                                     const int16_t*,
                                     int32_t,
                                     ArgDeref);
template void TalynBase::registerArg(JITID,
                                     unsigned,
                                     ArgType,
                                     const int32_t*,
                                     int32_t,
                                     ArgDeref);
template void TalynBase::registerArg(JITID,
                                     unsigned,
                                     ArgType,
                                     const int64_t*,
                                     int32_t,
                                     ArgDeref);
template void TalynBase::registerArg(JITID,
                                     unsigned,
                                     ArgType,
                                     const float*,
                                     int32_t,
                                     ArgDeref);
template void TalynBase::registerArg(JITID,
                                     unsigned,
                                     ArgType,
                                     const double*,
                                     int32_t,
                                     ArgDeref);
template void TalynBase::registerArg(JITID,
                                     unsigned,
                                     ArgType,
                                     const long double*,
                                     int32_t,
                                     ArgDeref);

void TalynBase::registerArg(JITID key,
                            unsigned i,
                            ArgType type,
                            const void* arg,
                            ArgDeref deref) {
  call.at(key).registerArg(i, type, arg, deref);
}

void TalynBase::update(JITID key,
                       FunctionSignatureBasic sig,
                       FunctionSignatureTuning variant,
                       void* fptr) {
  specialized[region][key][sig] = fptr;
  tuned[region][key][sig][variant] = fptr;
}

void TalynBase::maybeSaveOptReport(JITID key,
                                   FunctionSignatureBasic sig,
                                   FunctionSignatureTuning variant,
                                   const std::string& variantStr,
                                   const std::string& report) {
  if(rtCacheMgr.isEnabled() and saveOptReport) {
    std::string optReportBuf;
    llvm::raw_string_ostream optReport(optReportBuf);
    if(saveOptReport) {
      optReport
          << " ---------------------------- MOYA -----------------------\n";
      optReport << "\n";
      optReport << " Signature: " << sig << "\n";
      optReport << " Variant:   " << variant << "\n";
      optReport << " Descr:     " << variantStr << "\n\n";
      optReport << report << "\n\n";
    }

    std::stringstream name;
    name << jitContext.getName(key) << "_" << sig << "_" << variant;

    rtCacheMgr.writeOptReport(
        optReport.str(),
        PathUtils::join(moya::str(region), "reports", name.str()));
  }
}

void TalynBase::maybeSaveSpecializedModule(llvm::Module& module,
                                           JITID key,
                                           FunctionSignatureBasic sig,
                                           FunctionSignatureTuning variant) {
  if(rtCacheMgr.isEnabled() and saveCode) {
    std::stringstream name;
    name << jitContext.getName(key) << std::hex << "_" << sig << "_" << variant;

    rtCacheMgr.writeModule(
        module, PathUtils::join(moya::str(region), "specialized", name.str()));
  }
}

void TalynBase::maybeSaveSpecializedArgs(const Vector<CallArg>& args,
                                         JITID key,
                                         FunctionSignatureBasic sig,
                                         FunctionSignatureTuning variant) {
  if(rtCacheMgr.isEnabled() and saveCode) {
    std::stringstream name;
    name << jitContext.getName(key) << std::hex << "_" << sig << "_" << variant;

    rtCacheMgr.writeSpecializedArgs(
        args, PathUtils::join(moya::str(region), "specialized", name.str()));
  }
}

void TalynBase::maybeSaveOptimizedModule(llvm::Module& module,
                                         JITID key,
                                         FunctionSignatureBasic sig,
                                         FunctionSignatureTuning variant) {
  if(rtCacheMgr.isEnabled() and saveCode) {
    std::stringstream name;
    name << jitContext.getName(key) << std::hex << "_" << sig << "_" << variant;

    rtCacheMgr.writeModule(
        module, PathUtils::join(moya::str(region), "optimized", name.str()));
  }
}

void TalynBase::maybeSaveAssembler(llvm::Module& module,
                                   JITID key,
                                   FunctionSignatureBasic sig,
                                   FunctionSignatureTuning variant) {
  if(rtCacheMgr.isEnabled() and saveCode) {
    std::stringstream name;
    name << jitContext.getName(key) << std::hex << "_" << sig << "_" << variant;

    rtCacheMgr.writeAssembler(
        module, PathUtils::join(moya::str(region), "asm", name.str()));
  }
}

void TalynBase::maybeCacheMachineCode(llvm::Module& module,
                                      JITID key,
                                      FunctionSignatureBasic sig,
                                      FunctionSignatureTuning variant) {
  if(mcCacheMgr.isEnabled())
    mcCacheMgr.writeELF(
        module, region, key, sig, variant, jitContext.getHash(key));
}

} // namespace moya
