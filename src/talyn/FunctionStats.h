#ifndef MOYA_TALYN_FUNCTION_STATS_H
#define MOYA_TALYN_FUNCTION_STATS_H

#include "JITContext.h"
#include "common/APITypes.h"
#include "common/Map.h"
#include "common/Serializer.h"
#include "common/Timer.h"
#include "common/Vector.h"

#include <string>

namespace moya {

// Statistics collected during the execution of functions
class FunctionStats {
private:
  // This struct collects the statistics for a single tuned variant
  struct Variant {
    FunctionSignatureTuning sig;

    // A string representation of the tuning parameters
    std::string params;

    // The number of calls made to each tuned variant
    unsigned long calls;

    // The total amount of time spent executing in this variant
    Timer execute;

    Timer::NSecs minExecute;
    Timer::NSecs maxExecute;

    Variant(FunctionSignatureTuning sig, const std::string& params)
        : sig(sig), params(params), calls(0),
          minExecute(std::numeric_limits<Timer::NSecs>::max()), maxExecute(0) {
      ;
    }
  };

private:
  // The function for which these stats are being collected
  // Since this will be used for statistics that the user will look at,
  // there's no point in returning the key
  std::string name;

  // The names of the PAPI events that we can record on this platform
  const Vector<std::string>& papiEvents;

  // Statics for the different tuned variants of the function.
  // If tuning is disabled, there will be a single variant with an
  // "untuned function" signature
  Map<FunctionSignatureBasic, Map<FunctionSignatureTuning, Variant>>
      variants;

  // Call overhead. Computing function signature, checking and retrieving
  // the signature, etc.
  Timer call;

  // Load overhead. This is the time spent in loading an object file that
  // was saved in a previous invocation of the code
  Timer load;

  // Total optimization time for all variants of this function. This is the
  // time spent running LLVM passes on the IR
  Timer optimize;

  // Total time spent in machine code generation for all variants of this
  // function. This time is started after the optimize timer has been stopped
  Timer codegen;

  // Hardware counters for PAPI statistics
  Vector<long long> counters;

  // This is the time when the function started executing.
  bool recording;

private:
  Timer::NSecs getBestTime(FunctionSignatureBasic,
                           FunctionSignatureTuning) const;

  Variant& getVariant(FunctionSignatureBasic, FunctionSignatureTuning);
  void startRecording(const long long*);
  void stopRecording(const long long*);

public:
  FunctionStats(const std::string&, const Vector<std::string>&);

  const std::string& getName() const;

  void addVariant(FunctionSignatureBasic,
                  FunctionSignatureTuning,
                  const std::string&);
  void startCall();
  void stopCall();
  void startLoad(FunctionSignatureBasic, FunctionSignatureTuning);
  void stopLoad(FunctionSignatureBasic, FunctionSignatureTuning);
  void startOptimize(FunctionSignatureBasic, FunctionSignatureTuning);
  void stopOptimize(FunctionSignatureBasic, FunctionSignatureTuning);
  void startCodegen(FunctionSignatureBasic, FunctionSignatureTuning);
  void stopCodegen(FunctionSignatureBasic, FunctionSignatureTuning);
  void startExecute(FunctionSignatureBasic,
                    FunctionSignatureTuning,
                    const long long*);
  void stopExecute(FunctionSignatureBasic,
                   FunctionSignatureTuning,
                   const long long*);

  bool isRecording() const;
  void startRecording(FunctionSignatureBasic,
                      FunctionSignatureTuning,
                      const long long*);
  Timer::NSecs stopRecording(FunctionSignatureBasic,
                             FunctionSignatureTuning,
                             const long long*);
  void pauseRecording(FunctionSignatureBasic,
                      const long long*);
  void resumeRecording(FunctionSignatureBasic,
                       const long long*);

  bool isExecuted() const;
  FunctionSignatureTuning getBestVariant(FunctionSignatureBasic) const;
  unsigned getNumSpecialized() const;
  unsigned getNumVariants(FunctionSignatureBasic) const;
  unsigned long getNumCalls(FunctionSignatureBasic) const;
  unsigned long getNumCalls(FunctionSignatureBasic,
                            FunctionSignatureTuning) const;

  Serializer& serialize(Serializer&) const;
};

} // namespace moya

#endif // MOYA_TALYN_FUNCTION_STATS_H
