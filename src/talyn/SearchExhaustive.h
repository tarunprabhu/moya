#ifndef MOYA_TALYN_SEARCH_EXHAUSTIVE_H
#define MOYA_TALYN_SEARCH_EXHAUSTIVE_H

#include "SearchStrategy.h"

namespace moya {

// A simple "brute-force" search which simply walks over the whole state space
// without doing anything clever
class SearchExhaustive : public SearchStrategy {
protected:
  bool finished;

public:
  SearchExhaustive(SearchSpace&);
  virtual ~SearchExhaustive() = default;

  virtual bool isFinished() const;
  virtual bool start();
  virtual bool next();
};

} // namespace moya

#endif // MOYA_TALYN_SEARCH_EXHAUSTIVE_H
