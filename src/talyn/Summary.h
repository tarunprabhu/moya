#ifndef MOYA_TALYN_SUMMARY_H
#define MOYA_TALYN_SUMMARY_H

#include "SummaryCell.h"
#include "common/HashMap.h"
#include "common/Set.h"
#include "common/SummaryBase.h"
#include "common/Vector.h"

#include <llvm/IR/Instructions.h>

namespace moya {

// This is a specialization of the summary class intended for use in the
// runtime. The only functionality that it adds is processing GEP
// instructions

class Payload;

class Summary : public SummaryBase {
public:
  using Address = SummaryBase::Address;

protected:
  int64_t getOffsetInType(llvm::Type*, const moya::Vector<llvm::Value*>&) const;
  moya::Set<Summary::Address> processGEP(llvm::GetElementPtrInst*,
                                         llvm::Type*,
                                         unsigned,
                                         moya::Vector<llvm::Value*>,
                                         Summary::Address) const;

  SummaryCell& getCell(Address);
  const SummaryCell& getCell(Address) const;

public:
  Summary();
  virtual ~Summary() = default;

  Summary& unpickle(Payload&);
  std::string str() const;
};

} // namespace moya

#endif // MOYA_TALYN_SUMMARY_H
