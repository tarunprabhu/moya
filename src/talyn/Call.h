#ifndef MOYA_TALYN_CALL_H
#define MOYA_TALYN_CALL_H

#include "CallArg.h"
#include "Signature.h"
#include "common/APITypes.h"
#include "common/Vector.h"

namespace moya {

class Call {
protected:
  JITID key;
  Signature signature;
  Vector<CallArg> args;

public:
  Call(JITID, unsigned);

  void reset();

  const JITID& getKey() const;
  const Signature& getSignature() const;
  const Vector<CallArg>& getArgs() const;

  // Used when passing pointers to arrays of primitive types as parameter
  // In this case, the contents of the array should be used in the signature
  // but the pointer will be the argument
  //
  template <typename T,
            typename std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
  void registerArg(unsigned, ArgType, const T*, int32_t, ArgDeref);

  // This is used for both primitive types as well as pointers when the
  // address itself is used in the signature
  //
  template <typename T>
  void registerArg(unsigned, ArgType, const T, ArgDeref);

  // Used when the argument is passed by pointer/reference and we need to use
  // the pointer in the argument and the contents at the address in the
  // signature
  //
  template <typename T>
  void registerArg(unsigned, ArgType, const void*, T, ArgDeref);
};

} // namespace moya

#endif // MOYA_TALYN_CALL_H
