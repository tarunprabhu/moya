#ifndef MOYA_TALYN_CONSTANT_GENERATOR_H
#define MOYA_TALYN_CONSTANT_GENERATOR_H

#include "common/DataLayout.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>

namespace moya {

// This class deals with the mechanics of generating LLVM constants given
// raw pointers. We wrap it all up in a class because when JIT'ting for GPU's,
// we might need to explicitly copy data from the device back to the host
// since the pointers we have might be for the device memory which is not
// directly accessible from the host
class ConstantGenerator {
protected:
  DataLayout layout;

protected:
  // When dealing with remote data (say on a GPU), we have to copy it back
  // to the host before we can use it to construct a constant. This function
  // deallocates the buffer on the host that was allocated during the copy.
  // When dealing with local memory, this is a nop
  virtual void deallocateHostBuffer(void*) = 0;

  // When dealing with remote data (say on a GPU), we have to copy it back to
  // the host before we can use it to construct a constant. This function
  // copies the data back to the host if needed and returns a pointer to the
  // copy of the data. If no data needed to be copied, it just returns the
  // pointer that was passed in
  virtual void* getArrayPointer(const void*, unsigned long) = 0;

  // These functions will never allocate buffers on the host. If data must
  // be copied, it will be copied to pre-allocated buffers
  virtual const void* getIntPointer(const void*, unsigned long) = 0;
  virtual const void* getFloatPointer(const void*) = 0;
  virtual const void* getDoublePointer(const void*) = 0;

protected:
  template <typename T>
  llvm::Constant* getConstantInt(llvm::IntegerType*, const void*);

  llvm::Constant* getConstantFloat(llvm::Type*, const void*);
  llvm::Constant* getConstantDouble(llvm::Type*, const void*);

  template <typename T>
  llvm::Constant* getConstantDataArray(llvm::Type*, const void*, uint64_t);

  llvm::Constant* getConstantPrimitive(llvm::Type*, const void*);
  llvm::Constant* getConstantPointer(llvm::PointerType*, const void*);
  llvm::Constant* getConstantArray(llvm::ArrayType*, const void*);
  llvm::Constant* getConstantStruct(llvm::StructType*, const void*);

protected:
  ConstantGenerator(const llvm::Module&);

public:
  virtual ~ConstantGenerator() = default;

  llvm::Constant* getConstant(llvm::Type*, const void*);
};

} // namespace moya

#endif // MOYA_TALYN_CONSTANT_GENERATOR_H
