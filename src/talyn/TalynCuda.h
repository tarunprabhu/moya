#ifndef MOYA_TALYN_CUDA_H
#define MOYA_TALYN_CUDA_H

#include "Talyn.h"
#include "common/DemanglerCXX.h"

#include "common/Map.h"
#include "common/Set.h"
#include <string>

#include <cuda.h>

namespace moya {

class TalynCuda : public Talyn {
protected:
  // We currently only support running on a single GPU even if more than one
  // may be avaiable. I'm not sure how you do multiple GPU launches from a
  // Cuda program
  CUdevice device;

  // The Talyn objects are all thread-local, so a single context should
  // suffice for the current CPU thread.
  // This could get very unpleasant if Moya is run inside threading libraries
  // like AMPI, but for the moment that is not supported
  CUcontext context;

  // These serve the same purpose as the corresponding fields in the parent
  // class. Currently, we don't support JIT'ting host functions from Cuda
  // files. If we do so, this object won't get called anyway, so we're
  // still ok
  moya::Vector<CUmodule> newms;
  moya::Vector<CUfunction> newfs;
  moya::Map<JITID,
            moya::Map<FunctionSig, moya::Map<FunctionVersion, CUfunction>>>
      specialized;

protected:
  void writePTXToDiskCache(std::string ptx,
                           JITID key,
                           FunctionSig sig,
                           FunctionVersion version);

public:
  TalynCuda(const JITContext& conf,
            CacheManager& cacheMgr,
            Stats& stats,
            SearchSpace& space);
  virtual ~TalynCuda() = default;

  virtual void update(JITID, FunctionSig, FunctionVersion, void*);

  FunctionSignatureBasic getSignature(JITID key,
                                      const vector<const byte*>& args) const;
  virtual void compile(JITID, FunctionSig);
  virtual void compile(JITID, FunctionSig, const moya::Vector<const byte*>&);
  virtual bool isCompiled(JITID key, FunctionSig sig) const;
  virtual void* getCompiled(JITID key, FunctionSig sig) const;

  std::string getPtx(llvm::Module&);
  CUmodule createCudaModule();
  CUfunction createCudaFunction();

  void executeCuda(CUfunction,
                   unsigned grid_x,
                   unsigned grid_y,
                   unsigned grid_z,
                   unsigned block_x,
                   unsigned block_y,
                   unsigned block_z,
                   unsigned sharedMemBytes,
                   CUstream hstream,
                   void** kernelParams);
};

} // namespace moya

#endif // MOYA_TALYN_CUDA_H
