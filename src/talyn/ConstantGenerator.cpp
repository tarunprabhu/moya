#include "ConstantGenerator.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

#include <llvm/IR/Constants.h>

using namespace llvm;

namespace moya {

// We never need to deallocate scalars because the object itself will
// pre-allocate some space in which to store the scalars and will return
// the address of that pre-allocated space
template <typename T>
Constant* ConstantGenerator::getConstantInt(IntegerType* itype,
                                            const void* gptr) {
  const T* lptr = static_cast<const T*>(getIntPointer(gptr, sizeof(T)));
  uint64_t val = *lptr;
  return ConstantInt::get(itype, val, true);
}

Constant* ConstantGenerator::getConstantFloat(Type* type, const void* gptr) {
  LLVMContext& context = type->getContext();
  const float* lptr = static_cast<const float*>(getFloatPointer(gptr));
  APFloat val(*lptr);
  return ConstantFP::get(context, val);
}

Constant* ConstantGenerator::getConstantDouble(Type* type, const void* gptr) {
  LLVMContext& context = type->getContext();
  const double* lptr = static_cast<const double*>(getDoublePointer(gptr));
  APFloat val(*lptr);
  return ConstantFP::get(context, val);
}

// Arrays are always copied into freshly allocated memory on the host, so we
// do need to free the allocate host buffer
template <typename T>
Constant* ConstantGenerator::getConstantDataArray(Type* ety,
                                                  const void* gptr,
                                                  uint64_t n) {
  LLVMContext& context = ety->getContext();
  T* lptr = static_cast<T*>(getArrayPointer(gptr, sizeof(T) * n));
  ArrayRef<T> elems(lptr, n);
  Constant* c = ConstantDataArray::get(context, elems);
  deallocateHostBuffer(lptr);
  return c;
}

Constant* ConstantGenerator::getConstantPrimitive(Type* type,
                                                  const void* gptr) {
  if(IntegerType* ity = dyn_cast<IntegerType>(type))
    switch(ity->getBitWidth()) {
    case 64:
      return getConstantInt<uint64_t>(ity, gptr);
      break;
    case 32:
      return getConstantInt<uint32_t>(ity, gptr);
      break;
    case 8:
      return getConstantInt<uint8_t>(ity, gptr);
      break;
    case 16:
      return getConstantInt<uint16_t>(ity, gptr);
      break;
    default:
      moya_error("Unknown integer type: " << *ity);
      break;
    }
  else if(type->isFloatTy())
    return getConstantFloat(type, gptr);
  else if(type->isDoubleTy())
    return getConstantDouble(type, gptr);
  else
    moya_error("Unknown primitive type: " << *type);

  return nullptr;
}

Constant* ConstantGenerator::getConstantPointer(PointerType* pty,
                                                const void* gptr) {
  LLVMContext& context = pty->getContext();
  Constant* cint = ConstantInt::get(Type::getInt64Ty(context), (uint64_t)gptr);
  return ConstantExpr::getIntToPtr(cint, pty);
}

Constant* ConstantGenerator::getConstantArray(ArrayType* aty,
                                              const void* gptr) {
  Type* ety = aty->getElementType();
  uint64_t n = aty->getNumElements();
  if(IntegerType* ity = dyn_cast<IntegerType>(ety)) {
    switch(ity->getBitWidth()) {
    case 64:
      return getConstantDataArray<uint64_t>(ity, gptr, n);
      break;
    case 32:
      return getConstantDataArray<uint32_t>(ity, gptr, n);
      break;
    case 8:
      return getConstantDataArray<uint8_t>(ity, gptr, n);
      break;
    case 16:
      return getConstantDataArray<uint16_t>(ity, gptr, n);
      break;
    default:
      moya_error("Unexpected integer type: " << *aty);
      break;
    }
  } else if(ety->isFloatTy()) {
    return getConstantDataArray<float>(ety, gptr, n);
  } else if(ety->isFloatTy() or ety->isDoubleTy()) {
    return getConstantDataArray<double>(ety, gptr, n);
  } else {
    const byte* ptr = static_cast<const byte*>(gptr);
    uint64_t size = layout.getTypeAllocSize(ety);
    Vector<Constant*> vec;
    for(uint64_t i = 0; i < n; i++, ptr += size)
      vec.push_back(getConstant(ety, ptr));
    return ConstantArray::get(aty, LLVMUtils::makeArrayRef(vec));
  }
  return nullptr;
}

Constant* ConstantGenerator::getConstantStruct(StructType* stype,
                                               const void* gptr) {
  Vector<Constant*> consts;
  const StructLayout& sl = layout.getStructLayout(stype);
  for(unsigned i = 0; i < stype->getNumElements(); i++)
    consts.push_back(getConstant(stype->getElementType(i),
                                 (const byte*)gptr + sl.getElementOffset(i)));
  return ConstantStruct::get(stype, LLVMUtils::makeArrayRef(consts));
}

Constant* ConstantGenerator::getConstant(Type* type, const void* gptr) {
  if(type->getPrimitiveSizeInBits())
    return getConstantPrimitive(type, gptr);
  else if(PointerType* ptype = dyn_cast<PointerType>(type))
    return getConstantPointer(ptype, gptr);
  else if(ArrayType* atype = dyn_cast<ArrayType>(type))
    return getConstantArray(atype, gptr);
  else if(StructType* stype = dyn_cast<StructType>(type))
    return getConstantStruct(stype, gptr);
  return nullptr;
}

ConstantGenerator::ConstantGenerator(const Module& module) : layout(module) {
  ;
}

} // namespace moya
