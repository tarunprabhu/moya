#ifndef MOYA_TALYN_RUNTIME_H
#define MOYA_TALYN_RUNTIME_H

#include "JITContext.h"
#include "MachineCodeCacheManager.h"
#include "RuntimeCacheManager.h"
#include "Stats.h"
#include "System.h"
#include "TalynBase.h"
#include "common/Map.h"

#ifdef ENABLED_CUDA
#include <cuda.h>
#endif // ENABLED_CUDA

namespace moya {

// A single object of this runtime class will be present as a global variable.
// It might be thread_local or not.
class Runtime {
private:
  JITContext jitContext;
  System system;
  Stats stats;
  SearchSpace searchSpace;
  RuntimeCacheManager rtCacheMgr;
  MachineCodeCacheManager mcCacheMgr;
  bool moyaDisabled;
  bool setupSaveDir, setupCacheDir;
  bool saveStats, usePolly;
  RegionID currentRegion;

  // Talyn objects for the various languages
  moya::Map<SourceLang, std::unique_ptr<TalynBase>> talyn;

  // This shutdown object manages any global LLVM objects
  llvm::llvm_shutdown_obj shutdown_obj;

private:
  void clearTalyn();
  void initJITContext();
  void initStats();
  void initTalyn();
  void setupSaveDirs();
  void setupCacheDirs();
  int getRank() const;

public:
  Runtime();
  ~Runtime() = default;

  void initialize();
  void finalize();

  const JITContext& getJITContext() const;
  const Stats& getStats() const;

  bool isInRegion();
  void enterRegion(RegionID);
  void exitRegion(RegionID);
  void* compile(JITID);
  void beginCall(JITID);

  template <typename T>
  void registerArg(JITID, unsigned, ArgType, const T, ArgDeref);

  template <typename T>
  void registerArg(JITID, unsigned, ArgType, const T*, int32_t, ArgDeref);

#ifdef ENABLED_CUDA
  void executeCuda(JITID,
                   FunctionSignatureBasic,
                   unsigned,
                   unsigned,
                   unsigned,
                   unsigned,
                   unsigned,
                   unsigned,
                   unsigned,
                   CUstream,
                   void**);
#endif // ENABLED_CUDA

  // Stats
  void statsStartCall(JITID);
  void statsStopCall(JITID);
  // void statsStartCompile(JITID, FunctionSignatureBasic);
  // void statsStopCompile(JITID, FunctionSignatureBasic);
  // void statsStartOptimize(JITID, FunctionSignatureBasic);
  // void statsStopOptimize(JITID, FunctionSignatureBasic);
  void
      statsStartExecute(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
  void statsStopExecute(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
};

} // namespace moya

#endif // MOYA_TALYN_RUNTIME_H
