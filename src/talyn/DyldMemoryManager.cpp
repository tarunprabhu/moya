#include "DyldMemoryManager.h"

using namespace llvm;

namespace moya {

DyldMemoryManager::DyldMemoryManager(const JITContext& jitContext)
    : SectionMemoryManager(), jitContext(jitContext) {
  ;
}

uint64_t DyldMemoryManager::getSymbolAddress(const std::string& name) {
  uint64_t addr = SectionMemoryManager::getSymbolAddress(name);
  if(addr)
    return addr;

  if(jitContext.hasFunctionPtr(name))
    return reinterpret_cast<uint64_t>(jitContext.getFunctionPtr(name));
  return 0;
}

} // namespace moya
