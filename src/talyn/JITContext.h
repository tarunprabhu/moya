#ifndef MOYA_TALYN_CONF_H
#define MOYA_TALYN_CONF_H

#include "ConstantGeneratorHost.h"
#ifdef ENABLED_CUDA
#include "ConstantGeneratorCuda.h"
#endif // ENABLED_CUDA
#include "AAResult.h"
#include "Summary.h"
#include "common/APITypes.h"
#include "common/Map.h"
#include "common/Region.h"
#include "common/Set.h"
#include "common/SourceLang.h"
#include "common/Status.h"
#include "common/Vector.h"

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/ManagedStatic.h>

#include <string>

namespace moya {

class JITContext {
public:
  struct FuncMeta {
    std::string llvmName;
    std::string userName;
    unsigned numParams;
    SourceLang lang;
    unsigned tuningLevel;
    std::string hash;

    FuncMeta(const std::string& llvmName,
             const std::string& userName,
             unsigned numParams,
             SourceLang lang,
             unsigned tuningLevel,
             const std::string& hash)
        : llvmName(llvmName), userName(userName), numParams(numParams),
          lang(lang), tuningLevel(tuningLevel), hash(hash) {
      ;
    }
  };

private:
  Map<JITID, FuncMeta> funcs;

  // Addresses of the global variables in the program. These need to get passed
  // to the dynamic loader so everything gets linked correctly
  Map<std::string, const void*> gPtrs;

  // Addresses of the functions in the program. These need to get passed to
  // the dynamic loader so everything gets linked correctly
  Map<std::string, const void*> fPtrs;

  // There is one constant generator for each device. Each function to be
  // JIT'ed will use one of these
  std::unique_ptr<ConstantGenerator> ptrConstGenHost;
  std::unique_ptr<ConstantGenerator> ptrConstGenCuda;
  Map<JITID, ConstantGenerator*> cgens;

  // There is one modules and one summary (store) for every JIT'ed function
  // for each region
  Map<RegionID, Map<JITID, std::unique_ptr<llvm::Module>>> modules;
  Map<RegionID, Map<JITID, std::unique_ptr<Summary>>> stores;

  Vector<Region> regions;

  // There are alias objects for each region because the callgraph for each
  // region may be different and cause different aliasing behaviors
  Map<RegionID, Map<JITID, std::unique_ptr<AAResult>>> aaResults;

  // The executable name is used in generating the output files
  std::string exe;

  // Several initializations can only be performed after JITContext has been
  // initialized. We don't initialize in the constructor, so we need a flag
  // to prevent unpleasantness
  bool initialized;

public:
  using jitid_const_iterator = decltype(funcs)::key_const_iterator;
  using jitid_const_range = decltype(funcs)::key_const_range;

public:
  JITContext();

  // Returns false if Moya was not disabled.
  // Moya can be disabled explicitly by setting MOYA_OFF in the environment
  // or if we hit TOP during the analysis
  bool initialize();
  bool isInitialized() const;

  const Vector<Region>& getRegions() const;
  jitid_const_range getJITIDs() const;

  bool hasFunctionPtr(const std::string&) const;
  bool hasGlobalPtr(const std::string&) const;

  const void* getFunctionPtr(const std::string&) const;
  const void* getGlobalPtr(const std::string&) const;

  AAResult& getAAResult(RegionID, JITID) const;
  // We need a pointer to the store because the LLVM passes must have a
  // default constructor, so the only way out is to create the pass first
  // and then pass it any other parameters that it needs
  const Summary& getStore(RegionID, JITID) const;
  llvm::Module& getModule(RegionID, JITID) const;

  const std::string& getName(JITID) const;
  const std::string& getUserName(JITID) const;
  unsigned getNumArgs(JITID) const;
  unsigned getTuningLevel(JITID) const;
  SourceLang getLang(JITID) const;
  const std::string& getHash(JITID) const;

  ConstantGenerator& getConstantGenerator(JITID) const;

  const std::string& getExe() const;
};

} // namespace moya

#endif // MOYA_TALYN_CONF_H
