#include "Summary.h"
#include "Payload.h"
#include "common/Diagnostics.h"

using llvm::cast;
using llvm::dyn_cast;
using llvm::isa;

namespace moya {

Summary::Summary() : SummaryBase() {
  ;
}

SummaryCell& Summary::getCell(Address addr) {
  return *static_cast<SummaryCell*>(cells.at(addr).get());
}

const SummaryCell& Summary::getCell(Address addr) const {
  return *static_cast<SummaryCell*>(cells.at(addr).get());
}

Summary& Summary::unpickle(Payload& p) {
  uint64_t magic = p.unpickle<long>();
  if(magic != getMagicNumber())
    moya_error("Cannot unpickle stream");

  region = p.unpickle<Region>();
  function = p.unpickle<JITID>();
  top = p.unpickle<bool>();
  summarized = p.unpickle<bool>();

  if(summarized) {
    // Function arguments
    size_t numArgs = p.unpickle<size_t>();
    for(unsigned i = 0; i < numArgs; i++) {
      Address addr = p.unpickle<Address>();
      Status status = p.unpickle<Status>();

      args.push_back(std::make_tuple(addr, status));
    }

    // Globals
    size_t numGlobals = p.unpickle<size_t>();
    for(unsigned i = 0; i < numGlobals; i++) {
      const std::string& gname = p.unpickle<std::string>();
      Address addr = p.unpickle<Address>();
      Status status = p.unpickle<Status>();

      globals.insert(std::make_pair(gname, std::make_tuple(addr, status)));
    }

    // Cells
    size_t numCells = p.unpickle<size_t>();
    for(unsigned i = 0; i < numCells; i++) {
      SummaryCell* cell = new SummaryCell();
      cell->unpickle(p);
      cells[cell->getAddress()].reset(cell);
    }
  }

  return *this;
}

std::string Summary::str() const {
  std::string s;
  llvm::raw_string_ostream ss(s);

  ss << "top: " << top << "\n";
  ss << "summarized: " << summarized << "\n";
  ss << "region: " << region.getID() << "\n";
  ss << "function: " << function << "\n";

  unsigned argno = 0;
  for(const auto& it : args) {
    ss << "  " << argno++ << ": " << std::get<Address>(it) << "\n";
  }

  ss << "globals:\n";
  for(const auto& it : globals) {
    const std::string& gname = it.first;
    Address addr = std::get<Address>(it.second);
    Status status = std::get<Status>(it.second);

    ss << gname << ": " << addr << " (" << status.str() << ")\n";
  }

  ss << "cells:\n";
  Vector<Address> addrs = cells.keys();
  std::sort(addrs.begin(), addrs.end());

  for(Address addr : addrs) {
    const SummaryCell& cell = getCell(addr);
    ss << addr << ":\n";
    ss << "  status: " << cell.getStatus().str() << "\n";
    ss << "  targets: " << cell.getTargets().str() << "\n";
  }

  return ss.str();
}

} // namespace moya
