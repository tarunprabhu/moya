#include "TalynF77.h"
#include "common/DemanglerF77.h"
#include "common/LLVMUtils.h"

using namespace llvm;

namespace moya {

TalynF77::TalynF77(const JITContext& jitContext,
                   MachineCodeCacheManager& mcCacheMgr,
                   RuntimeCacheManager& rtCacheMgr,
                   Stats& stats,
                   SearchSpace& space)
    : TalynBase(jitContext,
                mcCacheMgr,
                rtCacheMgr,
                stats,
                space,
                SourceLang::F77) {
  ;
}

template <typename T>
void TalynF77::registerArgImpl(JITID key,
                               unsigned i,
                               ArgType type,
                               const T* ptr,
                               ArgDeref deref) {
  const Summary& store = jitContext.getStore(region, key);
  Status status = store.getStatus(static_cast<unsigned>(i));
  if(status.isDerefInKey())
    if(ptr)
      call.at(key).registerArg(i,
                               type,
                               reinterpret_cast<const void*>(ptr),
                               *ptr,
                               deref == ArgDeref::Ignore ? ArgDeref::Ignore
                                                         : ArgDeref::One);
    else
      call.at(key).registerArg(i,
                               type,
                               reinterpret_cast<const void*>(ptr),
                               static_cast<T>(0),
                               deref == ArgDeref::Ignore ? ArgDeref::Ignore
                                                         : ArgDeref::One);
  else
    call.at(key).registerArg(
        i, type, reinterpret_cast<const void*>(ptr), deref);
}

void TalynF77::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const bool* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynF77::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const int8_t* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynF77::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const int16_t* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynF77::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const int32_t* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynF77::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const int64_t* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynF77::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const float* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynF77::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const double* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynF77::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const long double* arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

void TalynF77::registerArg(JITID key,
                           unsigned i,
                           ArgType type,
                           const void** arg,
                           ArgDeref deref) {
  registerArgImpl(key, i, type, arg, deref);
}

} // namespace moya
