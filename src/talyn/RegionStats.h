#ifndef MOYA_TALYN_REGION_STATS_H
#define MOYA_TALYN_REGION_STATS_H

#include "FunctionStats.h"
#include "common/APITypes.h"
#include "common/Map.h"
#include "common/Region.h"
#include "common/Serializer.h"
#include "common/Timer.h"
#include "common/Vector.h"

#include <string>

namespace moya {

// Statistics collected for each region
class RegionStats {
private:
  // The name of the region. Since this is intended for the user, no point
  // throwing back a silly region ID which may not mean anything
  const Region& region;

  // Time spent in the JIT region
  Timer total;

  // Execution stats of the functions
  Map<JITID, FunctionStats> funcs;

public:
  RegionStats(const Region&, const JITContext&, const Vector<std::string>&);

  const Region& getRegion() const;

  void addVariant(JITID,
                  FunctionSignatureBasic,
                  FunctionSignatureTuning,
                  const std::string&);
  void startTotal();
  void stopTotal();
  void startCall(JITID);
  void stopCall(JITID);
  void startCompile(JITID, FunctionSignatureBasic, const long long*);
  void stopCompile(JITID, FunctionSignatureBasic, const long long*);
  void startLoad(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
  void stopLoad(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
  void startOptimize(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
  void stopOptimize(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
  void startCodegen(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
  void stopCodegen(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
  void startExecute(JITID,
                    FunctionSignatureBasic,
                    FunctionSignatureTuning,
                    const long long*);
  void stopExecute(JITID,
                   FunctionSignatureBasic,
                   FunctionSignatureTuning,
                   const long long*);

  bool isExecuted() const;
  FunctionSignatureTuning getBestVariant(JITID, FunctionSignatureBasic) const;
  unsigned getNumSpecialized(JITID) const;
  unsigned long
      getNumCalls(JITID, FunctionSignatureBasic, FunctionSignatureTuning) const;

  Serializer& serialize(Serializer&) const;
};

} // namespace moya

#endif // MOYA_TALYN_REGION_STATS_H
