#ifndef MOYA_TALYN_SEARCH_H
#define MOYA_TALYN_SEARCH_H

#include "SearchConfig.h"
#include "SearchSpace.h"
#include "SearchStrategy.h"
#include "common/APITypes.h"
#include "common/Map.h"

#include <llvm/IR/Module.h>

namespace moya {

// This is a class that contains the various objects that control how the
// searching is done. There will be one such object for each function
class Search {
private:
  SearchSpace& space;
  SearchConfig config;

  std::shared_ptr<SearchStrategy> strategy;
  llvm::Module& module;

public:
  struct State {
    FunctionSignatureTuning sig;
    uint64_t argc;
    char** argv;
  };

public:
  Search(unsigned, llvm::Module&, SearchSpace&);

  llvm::Module& getModule() const;
  unsigned getThresholdNumCalls() const;
  bool canExplore() const;

  FunctionSignatureTuning getCurr() const;
  const std::string& getCurrStr() const;
  State getNext();
};

} // namespace moya

#endif // MOYA_TALYN_SEARCH_H
