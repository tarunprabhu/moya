#ifndef MOYA_TALYN_CXX_H
#define MOYA_TALYN_CXX_H

#include "TalynBase.h"

namespace moya {

class TalynCXX : public TalynBase {
protected:
  template <typename T>
  void registerArgImpl(JITID, unsigned, ArgType, const T*, ArgDeref);

public:
  TalynCXX(const JITContext&,
           MachineCodeCacheManager&,
           RuntimeCacheManager&,
           Stats&,
           SearchSpace&);
  virtual ~TalynCXX() = default;

  virtual void
  registerArg(JITID, unsigned, ArgType, const bool*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const int8_t*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const int16_t*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const int32_t*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const int64_t*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const float*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const double*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const long double*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const void**, ArgDeref) override;
};

} // namespace moya

#endif // MOYA_TALYN_CXX_H
