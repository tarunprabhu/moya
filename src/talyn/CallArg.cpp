#include "CallArg.h"

namespace moya {

template <typename T>
void CallArg::reset(const T arg,
                    ArgType type,
                    ArgDeref deref,
                    unsigned count) {
  std::memcpy(&this->arg, &arg, sizeof(T));
  this->type = type;
  this->deref = deref;
  this->count = count;
}

template void CallArg::reset(const bool, ArgType, ArgDeref, unsigned);
template void CallArg::reset(const int8_t, ArgType, ArgDeref, unsigned);
template void CallArg::reset(const int16_t, ArgType, ArgDeref, unsigned);
template void CallArg::reset(const int32_t, ArgType, ArgDeref, unsigned);
template void CallArg::reset(const int64_t, ArgType, ArgDeref, unsigned);
template void CallArg::reset(const float, ArgType, ArgDeref, unsigned);
template void CallArg::reset(const double, ArgType, ArgDeref, unsigned);
template void CallArg::reset(const long double, ArgType, ArgDeref, unsigned);
template void CallArg::reset(const void*, ArgType, ArgDeref, unsigned);

template <typename T>
T CallArg::getArgAs() const {
  return *reinterpret_cast<const T*>(&arg);
}

template bool CallArg::getArgAs() const;
template int8_t CallArg::getArgAs() const;
template int16_t CallArg::getArgAs() const;
template int32_t CallArg::getArgAs() const;
template int64_t CallArg::getArgAs() const;
template float CallArg::getArgAs() const;
template double CallArg::getArgAs() const;
template long double CallArg::getArgAs() const;
template const bool* CallArg::getArgAs() const;
template const int8_t* CallArg::getArgAs() const;
template const int16_t* CallArg::getArgAs() const;
template const int32_t* CallArg::getArgAs() const;
template const int64_t* CallArg::getArgAs() const;
template const float* CallArg::getArgAs() const;
template const double* CallArg::getArgAs() const;
template const long double* CallArg::getArgAs() const;
template const void* CallArg::getArgAs() const;
template const void** CallArg::getArgAs() const;

const CallArg::Raw& CallArg::getArg() const {
  return arg;
}

ArgType CallArg::getType() const {
  return type;
}

ArgDeref CallArg::getDeref() const {
  return deref;
}

unsigned CallArg::getCount() const {
  return count;
}

} // namespace moya
