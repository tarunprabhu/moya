#ifndef MOYA_TALYN_CONSTANT_GENERATOR_HOST_H
#define MOYA_TALYN_CONSTANT_GENERATOR_HOST_H

#include "ConstantGenerator.h"

namespace moya {

class ConstantGeneratorHost : public ConstantGenerator {
protected:
  virtual const void* getIntPointer(const void*, unsigned long);
  virtual const void* getFloatPointer(const void*);
  virtual const void* getDoublePointer(const void*);
  virtual void* getArrayPointer(const void*, unsigned long);
  virtual void deallocateHostBuffer(void*);

public:
  ConstantGeneratorHost(const llvm::Module&);

  virtual ~ConstantGeneratorHost() = default;
};

} // namespace moya

#endif // MOYA_TALYN_CONSTANT_GENERATOR_HOST_H
