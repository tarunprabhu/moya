#ifndef MOYA_TALYN_STATS_H
#define MOYA_TALYN_STATS_H

#include "JITContext.h"
#include "RegionStats.h"
#include "common/APITypes.h"
#include "common/Map.h"
#include "common/Serializer.h"
#include "common/Timer.h"

#ifdef ENABLED_PAPI
#include <papi.h>
#endif // ENABLED_PAPI

namespace moya {

class Stats {
private:
  // This object has to be initialized properly before we can start up the
  // counters and things
  bool initialized;

  // The current region
  RegionID region;

  // The PAPI event set.
  int papiEventSet;

  // The names of the PAPI events that we can record
  moya::Vector<std::string> papiEvents;

  // This is the total progam wall-clock time
  Timer total;

  // The time to read the LLVM bitcode and perform other initializations.
  // PAPI for instance will be initialized here and the number of hardware
  // events that are tracked are set up
  Timer initJITContext;

  // The time to initialize the statistics collecting object.
  Timer initStats;

  // The time to initialize the Talyn objects. This will normally be only done
  // once, but if this is used with AMPI for instance, the Talyn objects will
  // be reinitialized after every migration
  Timer initTalyn;

  // The dynamic memory usage of the application
  long long memMax;

  // The size of the heap
  long long memHeap;

  // Temporary array used when reading counters
  moya::Vector<long long> snapshot;

  // The counters for the whole
  moya::Vector<long long> counters;

  moya::Map<RegionID, RegionStats> regionStats;

private:
  void readPAPICounters();

public:
  Stats();

  void initialize(const JITContext&);

  void startCounters();
  void stopCounters();

  void addVariant(JITID,
                  FunctionSignatureBasic,
                  FunctionSignatureTuning,
                  const std::string&);
  void startTotal();
  void stopTotal();
  void startInitStats();
  void stopInitStats();
  void startInitJITContext();
  void stopInitJITContext();
  void startInitTalyn();
  void stopInitTalyn();
  void startCall(JITID);
  void stopCall(JITID);
  void startCompile(JITID, FunctionSignatureBasic);
  void stopCompile(JITID, FunctionSignatureBasic);
  void startLoad(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
  void stopLoad(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
  void startOptimize(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
  void stopOptimize(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
  void startCodegen(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
  void stopCodegen(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
  void startExecute(JITID, FunctionSignatureBasic, FunctionSignatureTuning);
  void stopExecute(JITID, FunctionSignatureBasic, FunctionSignatureTuning);

  void enterRegion(RegionID);
  void beginProgram();
  void endProgram();

  unsigned getNumRegions() const;

  FunctionSignatureTuning
      getBestVariant(RegionID, JITID, FunctionSignatureBasic) const;

  unsigned long getNumCalls(RegionID,
                            JITID,
                            FunctionSignatureBasic,
                            FunctionSignatureTuning) const;

  Serializer& serialize(Serializer&) const;
};

} // namespace moya

#endif // MOYA_TALYN_STATS_H
