#include "Call.h"
#include "JITContext.h"
#include "Summary.h"
#include "common/DataLayout.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/Config/llvm-config.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/DataLayout.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>

using namespace llvm;
using namespace moya;

class DynamicConstantPointersPass : public FunctionPass {
private:
  std::string function;
  Vector<Call::Arg> args;
  const Summary* summary;
  const JITContext* conf;
  ConstantGenerator* cgen;
  moya::DataLayout layout;

private:
  bool replaceArg(Function&, unsigned);
  bool propagate(Value*, Summary::Address, Value*, const void*);
  int getArgumentNumber(CallInst*, Value*);

public:
  static char ID;

public:
  DynamicConstantPointersPass();

  void setFunction(const std::string&);
  void setArgs(const Vector<Call::Arg>&);
  void setSummary(const Summary*);
  void setJITContext(const JITContext*);

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnFunction(Function&);
};

DynamicConstantPointersPass::DynamicConstantPointersPass()
    : FunctionPass(ID), function(""), summary(nullptr), conf(nullptr),
      cgen(nullptr) {
  ;
}

StringRef DynamicConstantPointersPass::getPassName() const {
  return "Moya Dynamic Constant Pointers";
}

void DynamicConstantPointersPass::getAnalysisUsage(AnalysisUsage& AU) const {
  ;
}

void DynamicConstantPointersPass::setFunction(const std::string& f) {
  this->function = f;
}

void DynamicConstantPointersPass::setArgs(const Vector<Call::Arg>& args) {
  this->args = args;
}

void DynamicConstantPointersPass::setSummary(const Summary* summary) {
  this->summary = summary;
}

void DynamicConstantPointersPass::setJITContext(const JITContext* conf) {
  this->conf = conf;
}

int DynamicConstantPointersPass::getArgumentNumber(CallInst* call,
                                                   Value* prev) {
  for(unsigned argno = 0; argno < call->getNumArgOperands(); argno++)
    if(call->getArgOperand(argno) == prev)
      return argno;
  return -1;
}

bool DynamicConstantPointersPass::propagate(Value* inst,
                                            Summary::Address addr,
                                            Value* prev,
                                            const void* ptr) {
  bool changed = false;

  if(not summary->contains(addr))
    return false;

  if(auto* load = dyn_cast<LoadInst>(inst)) {
    Type* ptrTy = load->getPointerOperand()->getType();
    Type* elemTy = dyn_cast<PointerType>(ptrTy)->getElementType();
    if(ptr and elemTy->isPointerTy()) {
      const void* nextPtr = *reinterpret_cast<const void* const*>(ptr);
      const Summary::Targets& targets = summary->getTargets(addr);
      if(targets.size() == 1) {
        Summary::Address nextAddr = targets.at(0);
        unsigned i = 0;
        for(Use& u : load->uses()) {
          changed |= propagate(u.getUser(), nextAddr, inst, nextPtr);
          // This is here because replaceAllUsesWith will
          // invalidate the iterator here
          if(i++ > load->getNumUses())
            break;
        }
      } else {
        moya_error("Unsupported: multiple targets");
      }
    }
  } else if(auto* gep = dyn_cast<GetElementPtrInst>(inst)) {
    if(ptr and gep->hasAllConstantIndices()) {
      int64_t offset = layout.getOffsetInGEP(gep);
      const void* nextPtr = &reinterpret_cast<const byte*>(ptr)[offset];
      Summary::Address next
          = summary->propagate(gep, gep->getPointerOperandType(), 1, {}, addr);
      unsigned i = 0;
      for(Use& u : gep->uses()) {
        changed |= propagate(u.getUser(), next, inst, nextPtr);
        if(i++ > gep->getNumUses())
          break;
      }
    } else {
      // FIXME?: This doesn't feel right. I should be checking if it is safe
      // to stick the pointer value in here
      if(prev and ptr) {
        Type* type = gep->getPointerOperandType();
        Constant* c = cgen->getConstant(type, ptr);
        prev->replaceAllUsesWith(c);
        changed = true;
      }
    }
  } else if(auto* cst = dyn_cast<CastInst>(inst)) {
    unsigned i = 0;
    for(Use& u : cst->uses()) {
      changed |= propagate(u.getUser(), addr, inst, ptr);
      if(i++ > cst->getNumUses())
        break;
    }
  } else if(auto* phi = dyn_cast<PHINode>(inst)) {
    if(LLVMUtils::arePHINodeIncomingValuesEqual(phi)) {
      unsigned i = 0;
      for(Use& u : phi->uses()) {
        // FIXME?: Is this correct?
        changed |= propagate(u.getUser(), addr, inst, ptr);
        if(i++ > phi->getNumUses())
          break;
      }
    }
  } else if(auto* call = dyn_cast<CallInst>(inst)) {
    if(prev) {
      int argno = getArgumentNumber(call, prev);
      if(argno != -1) {
        Type* type = call->getArgOperand(argno)->getType();
        Constant* c = cgen->getConstant(type, ptr);
        call->setArgOperand(argno, c);
        changed = true;
      }
    }
  }

  return changed;
}

bool DynamicConstantPointersPass::replaceArg(Function& f, unsigned argno) {
  bool changed = false;
  Argument* arg = LLVMUtils::getArgument(f, argno);

  if((not moya::Metadata::hasIgnore(arg)) and arg->getType()->isPointerTy()) {
    const auto* ptr = reinterpret_cast<const void*>(args.at(argno));
    Summary::Address addr = summary->lookup(argno);
    for(Use& u : arg->uses())
      changed |= propagate(u.getUser(), addr, nullptr, ptr);
  }
  return changed;
}

bool DynamicConstantPointersPass::runOnFunction(Function& f) {
  if(f.getName().str() != function)
    return false;
  moya_message(getPassName());

  bool changed = false;
  layout.reset(*LLVMUtils::getModule(f));
  cgen = conf->getConstantGenerator(moya::Metadata::getJITID(f));

  for(unsigned i = 0; i < args.size(); i++)
    changed |= replaceArg(f, i);
  for(GlobalVariable& g : f.getParent()->globals()) {
    if(not g.isConstant()) {
      const void* ptr = conf->getGlobalPtr(g.getName());
      Summary::Address addr = summary->lookup(g.getName());
      for(Use& u : g.uses())
        if(auto* inst = dyn_cast<Instruction>(u.getUser()))
          if(LLVMUtils::getFunctionName(inst) == function)
            changed |= propagate(inst, addr, nullptr, ptr);
    }
  }
  return changed;
}

char DynamicConstantPointersPass::ID = 0;
static RegisterPass<DynamicConstantPointersPass>
    X("moya-jit-pointers",
      "Hard-codes pointers to constant memory (should ONLY be used at runtime)",
      false,
      false);

Pass* createDynamicConstantPointersPass(const std::string& f,
                                        const Vector<Call::Arg>& args,
                                        const Summary* summary,
                                        const JITContext* conf) {
  auto* pass = new DynamicConstantPointersPass();
  pass->setFunction(f);
  pass->setArgs(args);
  pass->setSummary(summary);
  pass->setJITContext(conf);
  return pass;
}
