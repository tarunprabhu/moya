#include "Call.h"
#include "common/Config.h"

#include <cstring>

namespace moya {

Call::Call(JITID id, unsigned numArgs) : key(id), args(numArgs) {
  ;
}

void Call::reset() {
  signature.reset();
}

const JITID& Call::getKey() const {
  return key;
}

const Signature& Call::getSignature() const {
  return signature;
}

const Vector<CallArg>& Call::getArgs() const {
  return args;
}

template <typename T>
void Call::registerArg(unsigned i, ArgType type, const T arg, ArgDeref deref) {
  args.at(i).reset(arg, type, deref);
  if(deref != ArgDeref::Ignore)
    signature.addComponent(arg);
}

template void Call::registerArg(unsigned, ArgType, const bool, ArgDeref);
template void Call::registerArg(unsigned, ArgType, const int8_t, ArgDeref);
template void Call::registerArg(unsigned, ArgType, const int16_t, ArgDeref);
template void Call::registerArg(unsigned, ArgType, const int32_t, ArgDeref);
template void Call::registerArg(unsigned, ArgType, const int64_t, ArgDeref);
template void Call::registerArg(unsigned, ArgType, const float, ArgDeref);
template void Call::registerArg(unsigned, ArgType, const double, ArgDeref);
template void Call::registerArg(unsigned, ArgType, const long double, ArgDeref);

template void Call::registerArg(unsigned, ArgType, const void*, ArgDeref);

template <typename T>
void Call::registerArg(unsigned i,
                       ArgType type,
                       const void* ptr,
                       const T sig,
                       ArgDeref deref) {
  args.at(i).reset(ptr, type, deref);
  if(deref != ArgDeref::Ignore)
    signature.addComponent(sig);
}

template void
Call::registerArg(unsigned, ArgType, const void*, const bool, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const void*, const int8_t, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const void*, const int16_t, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const void*, const int32_t, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const void*, const int64_t, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const void*, const float, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const void*, const double, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const void*, const long double, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const void*, const void*, ArgDeref);

template <typename T,
          typename std::enable_if_t<std::is_arithmetic<T>::value, int>>
void Call::registerArg(unsigned i,
                       ArgType type,
                       const T* ptr,
                       int32_t num,
                       ArgDeref deref) {
  args.at(i).reset(reinterpret_cast<const void*>(ptr), type, deref, num);
  if(deref != ArgDeref::Ignore)
    signature.addComponent(ptr, num);
}

template void
Call::registerArg<bool>(unsigned, ArgType, const bool*, int32_t, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const int8_t*, int32_t, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const int16_t*, int32_t, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const int32_t*, int32_t, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const int64_t*, int32_t, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const float*, int32_t, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const double*, int32_t, ArgDeref);
template void
Call::registerArg(unsigned, ArgType, const long double*, int32_t, ArgDeref);

} // namespace moya
