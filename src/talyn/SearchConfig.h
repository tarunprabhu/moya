#ifndef MOYA_TALYN_SEARCH_CONFIG_H
#define MOYA_TALYN_SEARCH_CONFIG_H

class SearchConfig {
private:
  // The minimum number of times a function should have been called before we
  // try to explore again
  unsigned thresholdNumCalls;

public:
  SearchConfig(unsigned);

  unsigned getThresholdNumCalls() const;
};

#endif
