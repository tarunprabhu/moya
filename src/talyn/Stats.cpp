#include "Stats.h"
#include "common/Config.h"
#include "common/Diagnostics.h"

#include <sstream>
#include <sys/types.h>
#include <unistd.h>

#ifdef ENABLED_PAPI
#include <papi.h>
#endif // ENABLED_PAPI

namespace moya {

Stats::Stats()
    : initialized(false), region(Config::getInvalidRegionID()), papiEventSet(0),
      memMax(0), memHeap(0) {
  ;
}

void Stats::initialize(const JITContext& jitContext) {
  if(not jitContext.isInitialized())
    moya_error(
        "Cannot initialize statistics with uninitialized JITContext object");

  initialized = true;

#ifdef ENABLED_PAPI
  papiEventSet = PAPI_NULL;
  PAPI_library_init(PAPI_VER_CURRENT);

  if(PAPI_is_initialized()) {
    // We prioritize the events that we want to record because there might
    // not be enough hardware counters available. This array is ordered from
    // most important to least important event
    Vector<int> presets = {PAPI_TOT_INS,
                           PAPI_LD_INS,
                           PAPI_BR_INS,
                           PAPI_L1_TCM,
                           PAPI_L2_TCM,
                           PAPI_L3_TCM,
                           PAPI_L1_ICM,
                           PAPI_L2_ICM,
                           PAPI_TOT_CYC,
                           PAPI_STL_ICY};

    unsigned numCounters = PAPI_num_counters();
    unsigned numEvents = std::min(numCounters, (unsigned)presets.size());

    PAPI_create_eventset(&papiEventSet);
    for(int event : presets) {
      if(papiEvents.size() < numEvents) {
        PAPI_event_info_t info;
        PAPI_get_event_info(event, &info);

        std::string derived = info.derived;
        if(derived == "NOT_DERIVED") {
          PAPI_add_event(papiEventSet, event);
          papiEvents.push_back(info.long_descr);
          snapshot.push_back(0);
          counters.push_back(0);
        }
      }
    }
  }
#endif // ENABLED_PAPI

  for(const Region& region : jitContext.getRegions())
    regionStats.emplace(region.getID(),
                        RegionStats(region, jitContext, papiEvents));
}

void Stats::startCounters() {
  if(not initialized)
    moya_error("Starting counters before initializing object");

#ifdef ENABLED_PAPI
  if(papiEvents.size()) {
    PAPI_start(papiEventSet);
    PAPI_read(papiEventSet, snapshot.data());
    for(unsigned i = 0; i < papiEvents.size(); i++)
      counters[i] -= snapshot[i];
  }
#endif // ENABLED_PAPI
}

void Stats::stopCounters() {
  if(not initialized)
    moya_error("Stopping counters on uninitialized object");

#ifdef ENABLED_PAPI
  if(papiEvents.size()) {
    PAPI_stop(papiEventSet, snapshot.data());
    for(unsigned i = 0; i < papiEvents.size(); i++)
      counters[i] += snapshot[i];
  }
#endif // ENABLED_PAPI
}

// All transitions after the start of the program are essentially entering a
// region. The first region that we enter is the "<null>" or global region -
// essentially any code that is not within a JIT region explicitly marked by
// the user. When exiting from a JIT region, we return to the "<null>" region.
void Stats::enterRegion(RegionID regionID) {
  RegionID prev = region;
  regionStats.at(prev).stopTotal();
  RegionID next = regionID;

  region = regionID;

  regionStats.at(next).startTotal();
}

// This is called at application start. This is effectively entering the
// "<null>"
// or global region
void Stats::beginProgram() {
  regionStats.at(Config::getInvalidRegionID()).startTotal();
}

// The only distinction is that
void Stats::endProgram() {
  regionStats.at(Config::getInvalidRegionID()).stopTotal();
#ifdef ENABLED_PAPI
  PAPI_dmem_info_t memory;
  PAPI_get_dmem_info(&memory);
  memMax = memory.high_water_mark;
  memHeap = memory.heap;
#endif // ENABLED_PAPI
}

void Stats::addVariant(JITID key,
                       FunctionSignatureBasic sig,
                       FunctionSignatureTuning variant,
                       const std::string& variantStr) {
  regionStats.at(region).addVariant(key, sig, variant, variantStr);
}

void Stats::startTotal() {
  total.start();
}

void Stats::stopTotal() {
  total.stop();
}

void Stats::startInitJITContext() {
  initJITContext.start();
}

void Stats::stopInitJITContext() {
  initJITContext.stop();
}

void Stats::startInitTalyn() {
  initTalyn.start();
}

void Stats::stopInitTalyn() {
  initTalyn.stop();
}

void Stats::startInitStats() {
  initStats.start();
}

void Stats::stopInitStats() {
  initStats.stop();
}

void Stats::startCall(JITID key) {
  regionStats.at(region).startCall(key);
}

void Stats::stopCall(JITID key) {
  regionStats.at(region).stopCall(key);
}

void Stats::startCompile(JITID key,
                         FunctionSignatureBasic sig) {
  readPAPICounters();
  regionStats.at(region).startCompile(key, sig, snapshot.data());
}

void Stats::stopCompile(JITID key,
                        FunctionSignatureBasic sig) {
  readPAPICounters();
  regionStats.at(region).stopCompile(key, sig, snapshot.data());
}

void Stats::startLoad(JITID key,
                      FunctionSignatureBasic sig,
                      FunctionSignatureTuning variant) {
  regionStats.at(region).startLoad(key, sig, variant);
}

void Stats::stopLoad(JITID key,
                     FunctionSignatureBasic sig,
                     FunctionSignatureTuning variant) {
  regionStats.at(region).stopLoad(key, sig, variant);
}

void Stats::startOptimize(JITID key,
                          FunctionSignatureBasic sig,
                          FunctionSignatureTuning variant) {
  regionStats.at(region).startOptimize(key, sig, variant);
}

void Stats::stopOptimize(JITID key,
                         FunctionSignatureBasic sig,
                         FunctionSignatureTuning variant) {
  regionStats.at(region).stopOptimize(key, sig, variant);
}

void Stats::startCodegen(JITID key,
                         FunctionSignatureBasic sig,
                         FunctionSignatureTuning variant) {
  readPAPICounters();
  regionStats.at(region).startCodegen(key, sig, variant);
}

void Stats::stopCodegen(JITID key,
                        FunctionSignatureBasic sig,
                        FunctionSignatureTuning variant) {
  readPAPICounters();
  regionStats.at(region).stopCodegen(key, sig, variant);
}

void Stats::startExecute(JITID key,
                         FunctionSignatureBasic sig,
                         FunctionSignatureTuning version) {
  readPAPICounters();
  regionStats.at(region).startExecute(key, sig, version, snapshot.data());
}

void Stats::stopExecute(JITID key,
                        FunctionSignatureBasic sig,
                        FunctionSignatureTuning version) {
  readPAPICounters();
  regionStats.at(region).stopExecute(key, sig, version, snapshot.data());
}

void Stats::readPAPICounters() {
#ifdef ENABLED_PAPI
  PAPI_read(papiEventSet, snapshot.data());
#endif // ENABLED_PAPI
}

unsigned Stats::getNumRegions() const {
  return regionStats.size();
}

FunctionSignatureTuning
Stats::getBestVariant(RegionID region,
                      JITID key,
                      FunctionSignatureBasic sig) const {
  return regionStats.at(region).getBestVariant(key, sig);
}

unsigned long Stats::getNumCalls(RegionID region,
                                 JITID key,
                                 FunctionSignatureBasic sig,
                                 FunctionSignatureTuning version) const {
  return regionStats.at(region).getNumCalls(key, sig, version);
}

Serializer& Stats::serialize(Serializer& s) const {
  s.mapStart();

  // Initialization time
  s.mapStart("Initializer");
  s.add("JITContext", initJITContext.nsecs());
  s.add("Talyn", initTalyn.nsecs());
  s.add("Stats", initStats.nsecs());
  s.mapEnd();

  // Overall statistics
  s.mapStart("Execute");
  s.add("Total", total.nsecs());
  for(unsigned i = 0; i < papiEvents.size(); i++)
    s.add(papiEvents.at(i), counters.at(i));

  // Overall memory usage
  s.mapStart("Memory");
  s.add("Max", memMax);
  s.add("Heap", memHeap);
  s.mapEnd();

  s.mapEnd();

  // Statistics for each region include region "0" which is effectively
  // outside any region
  s.mapStart("Regions");
  for(const auto& it : regionStats) {
    const RegionStats& region = it.second;
    if(region.isExecuted())
      s.add(region.getRegion().getName(), region);
  }
  s.mapEnd();

  s.mapEnd();

  return s;
}

} // namespace moya
