#ifndef MOYA_TALYN_SEARCH_STRATEGY_H
#define MOYA_TALYN_SEARCH_STRATEGY_H

#include "SearchSpace.h"
#include "common/Vector.h"

namespace moya {

// Base class for the various search strategies that we can use to explore
// the tuning space
class SearchStrategy {
private:
  // Cached value of the enumeration of the state because we likely will
  // use it often.
  // IMPORTANT: This *MUST* be set whenever we move to a new state, in other
  // words, whenever next() is called
  SearchSpace::Enumeration curr;
  std::string currStr;

protected:
  SearchSpace& space;

  // The current point in the search space. Where we go from here will be
  // determined by each specific search strategy.
  // The i'th element of the vector corresponds to the i'th command line
  // argument that will get passed to polly. The value at the i'th element is
  // the value that that option will take
  moya::Vector<SearchSpace::Val> vals;

protected:
  void setCurr(SearchSpace::Enumeration);
  SearchStrategy(SearchSpace&);

public:
  virtual ~SearchStrategy() = default;

  SearchSpace::Enumeration getEnumeration() const;
  const std::string& getEnumerationStr() const;
  Vector<char*>& getPollyArgs();

  // This doesn't have to be virtual because we will have started if the
  // current enumeration of the state is valid
  bool isStarted() const;

  // We might stop searching the state space if we are convinced that we can't
  // do better anyway, but only the search strategy would know that
  virtual bool isFinished() const = 0;

  // Initializes the tuning state. Returns false if the state could not be
  // initialized. We don't always have to start at the same state in every
  // strategy
  virtual bool start() = 0;

  // Gets the next state to search. Updates the tuning state each time this
  // function is called. Returns false if there are no more states available
  virtual bool next() = 0;
};

} // namespace moya

#endif // MOYA_TALYN_SEARCH_STRATEGY_H
