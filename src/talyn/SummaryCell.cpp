#include "SummaryCell.h"
#include "Payload.h"

using namespace llvm;

namespace moya {

SummaryCell::SummaryCell() : SummaryCellBase() {
  ;
}

SummaryCell& SummaryCell::unpickle(Payload& p) {
  addr = p.unpickle<Address>();
  status = p.unpickle<Status>();
  size_t numTargets = p.unpickle<size_t>();
  for(unsigned i = 0; i < numTargets; i++) {
    Address target = p.unpickle<Address>();
    targets.push_back(target);
  }

  return *this;
}

} // namespace moya
