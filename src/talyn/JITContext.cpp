#include "JITContext.h"
#include "Payload.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/FileSystemUtils.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/PathUtils.h"
#include "common/Stringify.h"

#include <dlfcn.h>

using namespace llvm;

namespace moya {

JITContext::JITContext()
    : ptrConstGenHost(nullptr), ptrConstGenCuda(nullptr), exe("a.out"),
      initialized(false) {
  regions.emplace_back();
}

const std::string& JITContext::getExe() const {
  return exe;
}

bool JITContext::isInitialized() const {
  return initialized;
}

typedef void* getter_fn(void);
template <typename T>
static void* lookupSymbol(T& t, void* handle) {
  std::string name = t.getName().str();
  if(void* ptr = dlsym(handle, name.c_str()))
    return ptr;

  if(Metadata::hasSourceName(t))
    if(void* ptr = dlsym(handle, Metadata::getSourceName(t).c_str()))
      return ptr;

  if(Metadata::hasGetter(t))
    if(void* pptr = dlsym(handle, Metadata::getGetter(t).c_str()))
      // This is a global variable whose contents are the actual pointer
      return *reinterpret_cast<void**>(pptr);

  return nullptr;
}

bool JITContext::initialize() {
  // We only need to know that this function has been entered
  initialized = true;
  bool moyaDisabled = Config::getEnvDisabled();
  std::string self = FileSystemUtils::readlink("/proc/self/exe");

  exe = PathUtils::basename(self);

  void* handle = dlopen(nullptr, RTLD_NOW);
  if(not handle)
    moya_error("Could not dlopen() executable");

  // Even if Moya is disabled, we need to read the payload because we need
  // to initialize the regions and function ids
  Payload payload(self);
  if(not payload.parse()) {
    if(not moyaDisabled)
      moya_warning("Could not read JIT payload. Disabling JIT'ing");
    moyaDisabled = true;
  }

  // Initialize summaries and alias analysis results
  for(SummaryBase* base : payload.getSummaries()) {
    Summary& summary = *static_cast<Summary*>(base);
    Region region = summary.getRegion();
    JITID key = summary.getFunction();
    stores[region.getID()][key].reset(&summary);
    regions.push_back(region);

    // We need the regions even if Moya is disabled, but we don't
    // need this
    if(not moyaDisabled)
      aaResults[region.getID()][key].reset(new AAResult(summary));
  }

  // Initialize symbols and function IDs
  for(llvm::Module* module : payload.getModules()) {
    const std::string& hash = payload.getHash(module);
    GlobalVariable* gFunc = LLVMUtils::getPayloadFunctionGlobal(*module);
    GlobalVariable* gRegion = LLVMUtils::getPayloadRegionGlobal(*module);

    auto* f = dyn_cast<Function>(gFunc->getInitializer()->getOperand(0));
    auto region = LLVMUtils::getInitializerAs<RegionID>(gRegion);
    JITID key = Metadata::getJITID(f);
    std::string userName = f->getName();
    if(Metadata::hasQualifiedName(f))
      userName = Metadata::getQualifiedName(f);
    else if(Metadata::hasSourceName(f))
      userName = Metadata::getSourceName(f);
    funcs.emplace(key,
                  FuncMeta(f->getName().str(),
                           userName,
                           f->getFunctionType()->getNumParams(),
                           Metadata::getSourceLang(f),
                           Metadata::getTuningLevel(f),
                           hash));

    // If the function ids are initialized, we don't need the rest if Moya is
    // disabled
    if(moyaDisabled)
      continue;

    // We just need some module that will last the lifetime of the program
    // to initialize the constant generators. So use the first one we find
    if(not ptrConstGenHost)
      ptrConstGenHost.reset(new ConstantGeneratorHost(*module));
#ifdef ENABLED_CUDA
    if(not ptrConstGenCuda)
      if(isCuda(Metadata::getSourceLang(f)))
        ptrConstGenCuda.reset(new ConstantGeneratorCuda(*module));
#endif // ENABLED_CUDA

    // Set the constant generator for each function that will be JIT'ed
    if(Metadata::hasCudaKernelDevice(f))
      cgens[key] = ptrConstGenCuda.get();
    else
      cgens[key] = ptrConstGenHost.get();

    // Now find the addresses of all the global variables and functions that
    // we might need
    for(GlobalVariable& g : module->globals())
      if(void* ptr = lookupSymbol(g, handle))
        gPtrs[g.getName().str()] = ptr;

    for(Function& f : module->functions())
      if(not f.size())
        if(void* ptr = lookupSymbol(f, handle))
          fPtrs[f.getName().str()] = ptr;

    // Do any clean up that we need to do on the module
    // We don't need the payload globals any longer
    gRegion->eraseFromParent();
    gFunc->eraseFromParent();

    modules[region][key].reset(module);
    stores[region][key]->setLayout(*module);
  }

  return not moyaDisabled;
}

Module& JITContext::getModule(RegionID region, JITID key) const {
  return *modules.at(region).at(key).get();
}

ConstantGenerator& JITContext::getConstantGenerator(JITID key) const {
  return *cgens.at(key);
}

const std::string& JITContext::getName(JITID key) const {
  return funcs.at(key).llvmName;
}

  const std::string& JITContext::getUserName(JITID key) const {
  return funcs.at(key).userName;
}

unsigned JITContext::getNumArgs(JITID key) const {
  return funcs.at(key).numParams;
}

unsigned JITContext::getTuningLevel(JITID key) const {
  return funcs.at(key).tuningLevel;
}

SourceLang JITContext::getLang(JITID key) const {
  return funcs.at(key).lang;
}

const std::string& JITContext::getHash(JITID key) const {
  return funcs.at(key).hash;
}

AAResult& JITContext::getAAResult(RegionID id, JITID key) const {
  return *aaResults.at(id).at(key).get();
}

const Summary& JITContext::getStore(RegionID id, JITID key) const {
  return *stores.at(id).at(key).get();
}

bool JITContext::hasFunctionPtr(const std::string& name) const {
  return fPtrs.contains(name);
}

bool JITContext::hasGlobalPtr(const std::string& name) const {
  return gPtrs.contains(name);
}

const void* JITContext::getFunctionPtr(const std::string& name) const {
  return fPtrs.at(name);
}

const void* JITContext::getGlobalPtr(const std::string& name) const {
  return gPtrs.at(name);
}

JITContext::jitid_const_range JITContext::getJITIDs() const {
  return funcs.keys();
}

const Vector<Region>& JITContext::getRegions() const {
  return regions;
}

} // namespace moya
