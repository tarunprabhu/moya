#ifndef MOYA_TALYN_PASSES_H
#define MOYA_TALYN_PASSES_H

#include "Call.h"
#include "common/APITypes.h"
#include "common/Vector.h"

#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>

namespace moya {

class JITContext;
class Summary;

} // namespace moya

llvm::Pass* createPrepareJITPass(JITID);

llvm::Pass* createPropagateAnalysisTypesPass(JITID);

llvm::Pass*
createDynamicConstantPropagationPass(JITID,
                                     const moya::Vector<moya::CallArg>&,
                                     RegionID,
                                     const moya::JITContext&);

// llvm::Pass* createDynamicLoopReductionPass(JITID,
//                                            const
//                                            moya::Vector<moya::Call::Arg>&,
//                                            RegionID,
//                                            const moya::JITContext&);

// llvm::Pass*
// createDynamicConstantPointersPass(JITID,
//                                   const moya::Vector<moya::Call::Arg>&,
//                                   RegionID,
//                                   const moya::JITContext&);

llvm::Pass* createMarkInvariantRangesPass(JITID,
                                          const moya::Vector<moya::CallArg>&,
                                          const moya::Summary&);

llvm::Pass* createPollyASTPrinterPass(llvm::raw_ostream&);

llvm::Pass* createIROutputPass(const std::string&);

#endif // MOYA_TALYN_PASSES_H
