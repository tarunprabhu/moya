#ifndef MOYA_TALYN_C_H
#define MOYA_TALYN_C_H

#include "TalynBase.h"

namespace moya {

class TalynC : public TalynBase {
protected:
  template <typename T>
  void registerArgImpl(JITID, unsigned, ArgType, const T*, ArgDeref);

public:
  TalynC(const JITContext&,
         MachineCodeCacheManager&,
         RuntimeCacheManager&,
         Stats&,
         SearchSpace&);
  virtual ~TalynC() = default;

  virtual void
  registerArg(JITID, unsigned, ArgType, const bool*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const int8_t*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const int16_t*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const int32_t*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const int64_t*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const float*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const double*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const long double*, ArgDeref) override;
  virtual void
  registerArg(JITID, unsigned, ArgType, const void**, ArgDeref) override;
};

} // namespace moya

#endif // MOYA_TALYN_C_H
