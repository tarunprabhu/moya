#include "Call.h"
#include "JITContext.h"
#include "Summary.h"
#include "common/Config.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/IR/DataLayout.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class MarkInvariantRangesPass : public FunctionPass {
public:
  static char ID;

private:
  struct Invariant {
    moya::Vector<Value*> trace;
    uint64_t size;
  };

private:
  std::string function;
  const Summary* store;
  moya::Vector<Call::Arg> args;
  moya::Map<Summary::Address, Invariant> invariants;

private:
  bool replaceArg(Function&, unsigned argno);
  bool propagate(Function&,
                 Value*,
                 Summary::Address addr,
                 const void* ptr,
                 moya::Vector<Value*>,
                 const Summary&);

public:
  MarkInvariantRangesPass();

  void setFunction(const std::string&);
  void setStore(const Summary* store);
  void setArgs(const moya::Vector<Call::Arg>& args);

  virtual void getAnalysisUsage(AnalysisUsage& AU) const;
  virtual bool runOnFunction(Function& f);
};

// IMPORTANT: UnifyFunctionExitNodes *MUST* be called before we run this pass
static Instruction* getExitInst(Function& f) {
  for(auto i = inst_begin(f); i != inst_end(f); i++)
    if(isa<ReturnInst>(&*i))
      return &*i;
  return nullptr;
}

MarkInvariantRangesPass::MarkInvariantRangesPass() : FunctionPass(ID) {
  ;
}

void MarkInvariantRangesPass::getAnalysisUsage(AnalysisUsage& AU) const {
  ;
}

void MarkInvariantRangesPass::setFunction(const std::string& f) {
  this->function = f;
}

void MarkInvariantRangesPass::setStore(const Summary* store) {
  this->store = store;
}

void MarkInvariantRangesPass::setArgs(const moya::Vector<Call::Arg>& args) {
  this->args = args;
}

// I shouldn't be adding the invariant additions here as well, but I think
// it will be tricky to do it in a separate pass once the instructions have
// been combined unless I change the typeinfo objects

bool MarkInvariantRangesPass::propagate(Function& f,
                                        Value* inst,
                                        Summary::Address addr,
                                        const void* ptr,
                                        moya::Vector<Value*> trace,
                                        const Summary& store) {
  bool changed = false;

  // Module &module = *f.getParent();
  // const DataLayout &dl = *module.getDataLayout();
  // trace.push_back(inst);
  // if(LoadInst *load = dyn_cast<LoadInst>(inst)) {
  //   Type *ptrTy = load->getPointerOperand()->getType();
  //   Type *elemTy = dyn_cast<PointerType>(ptrTy)->getElementType();
  //   if(store.getStatus(addr).isConstant()) {
  //     if(ptr and elemTy->isPointerTy()) {
  //       TypeInfo *t = cast<PointerTI>(ti)->inner();
  //       if ((not t->status().isVariable()) and t->parent()) {
  //         bool makeInvariant = false;
  //         if (t->parent()->isStructTI()) {
  //           // TODO: Add support for partially constant structs
  //           if (t->parent()->status().isConstant())
  //             makeInvariant = true;
  //         } else if (t->parent()->isDescriptorTI()) {
  //           makeInvariant = true;
  //         } else if (t->parent()->isArrayTI()) {
  //           makeInvariant = true;
  //         }
  //         if (makeInvariant) {
  //           Type *type = t->parent()->getLLVMType(module);
  //           moya::Vector<Value *> tr(trace);
  //           tr.pop_back();
  //           if (isa<CastInst>(tr.back()))
  //             tr.pop_back();
  //           invariants.insert(std::std::make_pair(
  //               t->parent(), Invariant{tr, DL.getTypeStoreSize(type)}));
  //         }

  //         // TODO: Right now, we ignore the final load which pulls up the
  //         // innermost buffer. If this is a primitive type, we will also know
  //         // whether or not it is constant. We should add support to mark
  //         that
  //         // range as constant as well. I'm not sure whether or not that will
  //         // be beneficial, but it will certainly be more complete.
  //       }
  //       void *nextPtr = *((void **)ptr);
  //       TypeInfo *nextTi = ti->processInst(load, module);
  //       unsigned i = 0;
  //       for (auto u = load->use_begin(); u != load->use_end(); u++) {
  //         changed |= propagate(f, u->getUser(), nextTi, nextPtr, trace);
  //         // This is here because RAUW will invalidate the iterator
  //         if (i++ > load->getNumUses())
  //           break;
  //       }
  //     }
  //   }
  // } else if (GetElementPtrInst *gep = dyn_cast<GetElementPtrInst>(inst)) {
  //   if (not ti->status().isVariable()) {
  //     APInt coff(DL.getPointerSize() * 8, 0, false);
  //     if (ptr) {
  //       if (gep->accumulateConstantOffset(DL, coff)) {
  //         uint64_t offset = coff.getLimitedValue();
  //         void *nextPtr = (void *)((uint8_t *)ptr + offset);
  //         TypeInfo *nextTi = nullptr;
  //         // TypeInfo *nextTi = ti->processGEP(gep, module);
  //         unsigned i = 0;
  //         for (auto u = gep->use_begin(); u != gep->use_end(); u++) {
  //           changed |= propagate(f, u->getUser(), nextTi, nextPtr, trace);
  //           if (i++ > gep->getNumUses())
  //             break;
  //         }
  //       }
  //     }
  //   }
  // } else if (CastInst *cst = dyn_cast<CastInst>(inst)) {
  //   unsigned i = 0;
  //   TypeInfo *nextTi = do_cast(ti, cst, module);
  //   for (auto u = cst->use_begin(); u != cst->use_end(); u++) {
  //     changed |= propagate(f, u->getUser(), nextTi, ptr, trace);
  //     if (i++ > cst->getNumUses())
  //       break;
  //   }
  // } else if (PHINode *phi = dyn_cast<PHINode>(inst)) {
  //   if (arePHINodeIncomingValuesEqual(phi)) {
  //     unsigned i = 0;
  //     for (auto u = phi->use_begin(); u != phi->use_end(); u++) {
  //       // FIXME? : Is this correct?
  //       changed |= propagate(f, u->getUser(), ti, ptr, trace);
  //       if (i++ > phi->getNumUses())
  //         break;
  //     }
  //   }
  // }
  // // I probably don't need to do propagate into the call arguments because
  // // the subsequent passes will take care of it for me, but I should check
  // // NOTE: The comment above might only apply for Fortran because everything
  // // gets passed by pointer in Fortran
  return changed;
}

bool MarkInvariantRangesPass::replaceArg(Function& f, unsigned argno) {
  Argument* arg = getArgument(f, argno);
  const auto* ptr = reinterpret_cast<const void*>(args.at(argno));
  Summary::Address addr = store->lookup(moya::Metadata::getJITID(f), argno);
  bool changed = false;
  for(Use& u : arg->uses())
    changed
        |= propagate(f, u.getUser(), addr, ptr, moya::Vector<Value*>(), *store);
  return changed;
}

bool MarkInvariantRangesPass::runOnFunction(Function& f) {
  moya_error("MarkInvariantRanges not implemented");

  bool changed = false;
  // for (unsigned i = 0; i < args.size(); i++)
  //   changed |= replaceArg(f, i);

  // if(invariants.size()) {
  //   Module &module = *(f.getParent());
  //   LLVMContext &context = f.getContext();
  //   IRBuilder<> builder(context);

  //   Type *i8ptr = Type::getInt8PtrTy(context);
  //   Type *i64 = Type::getInt64Ty(context);
  //   Type *startRet = StructType::get(context)->getPointerTo();
  //   Type *endRet = Type::getVoidTy(context);

  //   moya::Vector<Type *> startTys = {i64, i8ptr };
  //   FunctionType *startFType = FunctionType::get(startRet, startTys, false);
  //   std::string startName = "llvm.invariant.start";
  //   Function *invStart =
  //     cast<Function>(module.getOrInsertFunction(startName, startFType));

  //   moya::Vector<Type *> endTys = {startRet, i64, i8ptr };
  //   FunctionType *endFType = FunctionType::get(endRet, endTys, false);
  //   std::string endName = "llvm.invariant.end";
  //   Function *invEnd =
  //     cast<Function>(module.getOrInsertFunction(endName, endFType));

  //   BasicBlock *bbStart =
  //       BasicBlock::Create(context, "inv_start", &f, &f.front());
  //   Instruction *ret = getExitInst(f);

  //   for(auto &it : invariants) {
  //     moya::Vector<Value*> trace = it.second.trace;
  //     unsigned size = it.second.size;

  //     // Make sure everything is an instruction so we can clone it
  //     bool okToMark = trace.size();
  //     if(GetElementPtrInst *gep = dyn_cast<GetElementPtrInst>(trace.back()))
  //       if(gep->getNumIndices() > 2)
  //         okToMark = false;
  //     for(auto &v : trace)
  //       if(not isa<Instruction>(v))
  //         okToMark = false;
  //     if(not okToMark)
  //       continue;

  //     builder.SetInsertPoint(bbStart);
  //     Instruction *prev = nullptr;
  //     for(auto &v : trace) {
  //       Instruction *i = cast<Instruction>(v)->clone();
  //       if(prev)
  //         i->setOperand(0, prev);
  //       prev = builder.Insert(i);

  //     }
  //     Instruction *addr = prev;

  //     Value *cst = builder.CreatePointerCast(addr, i8ptr, "");
  //     moya::Vector<Value *> startArgs = {ConstantInt::get(i64, size), cst};
  //     Value *inv = builder.CreateCall(invStart, startArgs, "");

  //     builder.SetInsertPoint(ret);
  //     moya::Vector<Value *> endArgs = {inv, ConstantInt::get(i64, size),
  //     cst}; builder.CreateCall(invEnd, endArgs, ""); changed |= true;
  //   }
  //   builder.SetInsertPoint(bbStart);
  //   builder.CreateBr(bbStart->getNextNode());
  // }

  return changed;
}

char MarkInvariantRangesPass::ID = 0;

static RegisterPass<MarkInvariantRangesPass>
    X("moya-invariant-ranges",
      "Marks memory regions as invariant",
      true,
      false);

Pass* createMarkInvariantRangesPass(const std::string& f,
                                    const moya::Vector<Call::Arg>& args,
                                    const Summary* store) {
  auto* pass = new MarkInvariantRangesPass();
  pass->setFunction(f);
  pass->setArgs(args);
  pass->setStore(store);
  return pass;
}
