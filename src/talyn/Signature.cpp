#include "Signature.h"
#include "common/Config.h"
#include "common/Diagnostics.h"

#include <llvm/Support/raw_ostream.h>

namespace moya {

// This is the function used to compute the argument hash. It's basically the
// 64-bit std::string hashcode function in Java
//
// uint64_t hash(integer n) {
//   uint64_t h = 0;
//   for(i = 0; i < sizeof(n); i++, n >>= 8)
//     h = h*31 + (arg & 0xff);
//   return h
// }

template <typename T>
void computeHash(T arg, FunctionSignatureBasic& signature) {
  for(unsigned i = 0; i < sizeof(T); i++, arg >>= std::min(sizeof(T), 8UL))
    signature = signature * 257 + (arg & 0xff);
}

template <typename T>
T deref(const T* ptr) {
  if(ptr)
    return *ptr;
  return 0;
}

Signature::Signature() {
  reset();
}

void Signature::reset() {
  signature = 1125899906842597L;
}

FunctionSignatureBasic Signature::get() const {
  return signature;
}

template <>
void Signature::addComponent(float arg) {
  const float f = arg;
  const int32_t* i32 = reinterpret_cast<const int32_t*>(&f);
  computeHash(*i32, signature);
}

template <>
void Signature::addComponent(double arg) {
  const double g = arg;
  const int64_t* i64 = reinterpret_cast<const int64_t*>(&g);
  computeHash(*i64, signature);
}

template <>
void Signature::addComponent(long double arg) {
  const long double g = arg;
  const int64_t* i64 = reinterpret_cast<const int64_t*>(&g);
  computeHash(i64[0], signature);
  computeHash(i64[1], signature);
}

template <typename T,
          typename std::enable_if_t<std::is_arithmetic<T>::value, int>>
void Signature::addComponent(T arg) {
  computeHash(arg, signature);
}

template void Signature::addComponent(bool);
template void Signature::addComponent(int8_t);
template void Signature::addComponent(int16_t);
template void Signature::addComponent(int32_t);
template void Signature::addComponent(int64_t);

template <typename T,
          typename std::enable_if_t<std::is_arithmetic<T>::value, int>>
void Signature::addComponent(const T* ptr, int32_t num) {
  for(int32_t i = 0; i < num; i++)
    addComponent(ptr[i]);
}

template void Signature::addComponent(const bool*, int32_t);
template void Signature::addComponent(const int8_t*, int32_t);
template void Signature::addComponent(const int16_t*, int32_t);
template void Signature::addComponent(const int32_t*, int32_t);
template void Signature::addComponent(const int64_t*, int32_t);
template void Signature::addComponent(const float*, int32_t);
template void Signature::addComponent(const double*, int32_t);
template void Signature::addComponent(const long double*, int32_t);

void Signature::addComponent(const void* ptr) {
  addComponent(reinterpret_cast<int64_t>(ptr));
}

} // namespace moya
