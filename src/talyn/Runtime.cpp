#include "Runtime.h"
#include "TalynC.h"
#include "TalynCXX.h"
#include "TalynF77.h"
#include "TalynFort.h"
#include "common/Config.h"
#include "common/FileSystemUtils.h"
#include "common/PathUtils.h"
#include "common/Stringify.h"

#ifdef ENABLED_CUDA
#include "TalynCuda.h"
#endif // ENABLED_PAPI

#ifdef ENABLED_PAPI
#include <papi.h>
#endif // ENABLED_PAPI

#ifdef ENABLED_MPI
#include <mpi.h>
#endif // ENABLED_MPI

#include <ctime>
#include <fstream>
#include <locale>
#include <sched.h>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

namespace moya {

Runtime::Runtime()
    : searchSpace(system), moyaDisabled(false), setupSaveDir(false),
      setupCacheDir(false), saveStats(Config::isEnvSaveStats()),
      usePolly(Config::isEnvUsePolly()),
      currentRegion(Config::getInvalidRegionID()) {
  // Sanity checks
#ifndef ENABLED_POLLY
  if(usePolly) {
    moya_error("Moya not built with support for Polly");
  }
#endif // ENABLED_POLLY
}

void Runtime::initialize() {
  stats.startTotal();
  initJITContext();
  initStats();
  initTalyn();
  stats.startCounters();
  stats.beginProgram();
}

void Runtime::finalize() {
  stats.endProgram();
  stats.stopCounters();
  stats.stopTotal();
  if(rtCacheMgr.isEnabled() and saveStats) {
    std::stringstream ss;
    ss << "stats.";
    if(moyaDisabled)
      ss << "off";
    else if(usePolly)
      ss << "polly";
    else
      ss << "moya";
    rtCacheMgr.writeStats(stats, ss.str());
  }
}

const JITContext& Runtime::getJITContext() const {
  return jitContext;
}

const Stats& Runtime::getStats() const {
  return stats;
}

int Runtime::getRank() const {
  int rank = 0;
#ifdef ENABLED_MPI
  {
    int initialized = 0;
    MPI_Initialized(&initialized);
    if(initialized) {
      // Everyone gets rank 0's pid
      // MPI_Bcast(&time, 64, MPI_CHAR, 0, MPI_COMM_WORLD);
      MPI_Comm_rank(MPI_COMM_WORLD, &rank);

      // Put a barrier here to ensure that the save directory is set up
      // before the other ranks move ahead, else they might not find the
      // directory if they try to save something
      //
      MPI_Barrier(MPI_COMM_WORLD);
    }
  }
#endif // ENABLED_MPI

  return rank;
}

// The setupDirs() methods are not called from the constructor
// because MPI_Init may not have been  called when the constructor is executed
// and we need MPI to be correctly initialized
//

void Runtime::setupCacheDirs() {
  if(mcCacheMgr.isEnabled() and (not moyaDisabled)) {
    int rank = getRank();
    std::string label = jitContext.getExe();
    std::string base = PathUtils::join(mcCacheMgr.getDirectory(), label);
    std::string dir = PathUtils::join(base, moya::str(rank));

    if(rank == 0)
      FileSystemUtils::mkdir(base);
    FileSystemUtils::mkdir(dir);

    mcCacheMgr.setSubdir(PathUtils::join(label, moya::str(rank)));
    for(const Region& region : jitContext.getRegions()) {
      if(region.getID() != Config::getInvalidRegionID()) {
        std::string regionDir = PathUtils::join(dir, moya::str(region.getID()));
        FileSystemUtils::mkdir(regionDir);
        for(JITID key : jitContext.getJITIDs()) {
          std::string hash = jitContext.getHash(key);
          FileSystemUtils::mkdir(PathUtils::join(regionDir, moya::str(key)));
          FileSystemUtils::mkdir(
              PathUtils::join(regionDir, moya::str(key), hash));
        }
      }
    }
    setupCacheDir = true;
  }
}

void Runtime::setupSaveDirs() {
  if(rtCacheMgr.isEnabled()) {
    int rank = getRank();
    std::string label = jitContext.getExe();
    std::string base = PathUtils::join(rtCacheMgr.getDirectory(), label);
    std::string dir = PathUtils::join(base, moya::str(rank));

    if(rank == 0)
      FileSystemUtils::mkdir(base);
    FileSystemUtils::mkdir(dir);

    rtCacheMgr.setSubdir(PathUtils::join(label, moya::str(rank)));
    for(const Region& region : jitContext.getRegions()) {
      if(region.getID() != Config::getInvalidRegionID()) {
        std::string regionDir = PathUtils::join(dir, moya::str(region.getID()));
        FileSystemUtils::mkdir(regionDir);
        FileSystemUtils::mkdir(PathUtils::join(regionDir, "specialized"));
        FileSystemUtils::mkdir(PathUtils::join(regionDir, "optimized"));
        FileSystemUtils::mkdir(PathUtils::join(regionDir, "reports"));
        FileSystemUtils::mkdir(PathUtils::join(regionDir, "asm"));
      }
    }
    setupSaveDir = true;
  }
}

void Runtime::initJITContext() {
  stats.startInitJITContext();

  moyaDisabled = not jitContext.initialize();

  stats.stopInitJITContext();
}

void Runtime::initStats() {
  stats.startInitStats();

  stats.initialize(jitContext);

  stats.stopInitStats();
}

void Runtime::initTalyn() {
  stats.startInitTalyn();

  talyn[SourceLang::C].reset(
      new TalynC(jitContext, mcCacheMgr, rtCacheMgr, stats, searchSpace));
  talyn[SourceLang::CXX].reset(
      new TalynCXX(jitContext, mcCacheMgr, rtCacheMgr, stats, searchSpace));
  talyn[SourceLang::Fort].reset(
      new TalynFort(jitContext, mcCacheMgr, rtCacheMgr, stats, searchSpace));
  talyn[SourceLang::F77].reset(
      new TalynF77(jitContext, mcCacheMgr, rtCacheMgr, stats, searchSpace));
#ifdef ENABLED_CUDA
  talyn[SourceLang::Cuda].reset(
      new TalynCuda(jitContext, cacheMgr, stats, searchSpace));
#endif

  stats.stopInitTalyn();
}

void Runtime::clearTalyn() {
  for(auto& it : talyn)
    it.second.reset(nullptr);
}

bool Runtime::isInRegion() {
  if(moyaDisabled)
    return false;

  return not Config::isInvalidRegionID(currentRegion);
}

void Runtime::enterRegion(RegionID regionID) {
  if(not moyaDisabled)
    for(auto& it : talyn)
      it.second->enterRegion(regionID);

  // Setup the save directories when a JIT region is entered for the first
  // time. It's rather lousy doing it this way, but it has to be done after
  // MPI is setup
  if(rtCacheMgr.isEnabled() and (not setupSaveDir))
    setupSaveDirs();

  if(mcCacheMgr.isEnabled() and (not setupCacheDir))
    setupCacheDirs();

  // It is safe to set the current region here because if Moya is disabled,
  // isInRegion() will always return false and will not check the current
  // region
  currentRegion = regionID;
  stats.enterRegion(currentRegion);
}

void Runtime::exitRegion(RegionID regionID) {
  if(not moyaDisabled)
    for(auto& it : talyn)
      it.second->exitRegion(regionID);

  // We can just reset the region because nested regions are not supported
  currentRegion = Config::getInvalidRegionID();
  stats.enterRegion(currentRegion);
}

void* Runtime::compile(JITID key) {
  return talyn.at(jitContext.getLang(key))->compile(key);
}

void Runtime::beginCall(JITID key) {
  talyn.at(jitContext.getLang(key))->beginCall(key);
}

template <typename T>
void Runtime::registerArg(JITID key,
                          unsigned i,
                          ArgType type,
                          const T arg,
                          ArgDeref deref) {
  talyn.at(jitContext.getLang(key))->registerArg(key, i, type, arg, deref);
}

template <typename T>
void Runtime::registerArg(JITID key,
                          unsigned i,
                          ArgType type,
                          const T* ptr,
                          int32_t num,
                          ArgDeref deref) {
  talyn.at(jitContext.getLang(key))->registerArg(key, i, type, ptr, num, deref);
}

template void Runtime::registerArg(JITID, unsigned, ArgType, bool, ArgDeref);
template void Runtime::registerArg(JITID, unsigned, ArgType, int8_t, ArgDeref);
template void Runtime::registerArg(JITID, unsigned, ArgType, int16_t, ArgDeref);
template void Runtime::registerArg(JITID, unsigned, ArgType, int32_t, ArgDeref);
template void Runtime::registerArg(JITID, unsigned, ArgType, int64_t, ArgDeref);
template void Runtime::registerArg(JITID, unsigned, ArgType, float, ArgDeref);
template void Runtime::registerArg(JITID, unsigned, ArgType, double, ArgDeref);
template void
Runtime::registerArg(JITID, unsigned, ArgType, long double, ArgDeref);
template void
Runtime::registerArg(JITID, unsigned, ArgType, const void*, ArgDeref);
template void
Runtime::registerArg(JITID, unsigned, ArgType, const bool*, ArgDeref);
template void
Runtime::registerArg(JITID, unsigned, ArgType, const int8_t*, ArgDeref);
template void
Runtime::registerArg(JITID, unsigned, ArgType, const int16_t*, ArgDeref);
template void
Runtime::registerArg(JITID, unsigned, ArgType, const int32_t*, ArgDeref);
template void
Runtime::registerArg(JITID, unsigned, ArgType, const int64_t*, ArgDeref);
template void
Runtime::registerArg(JITID, unsigned, ArgType, const float*, ArgDeref);
template void
Runtime::registerArg(JITID, unsigned, ArgType, const double*, ArgDeref);
template void
Runtime::registerArg(JITID, unsigned, ArgType, const long double*, ArgDeref);
template void
Runtime::registerArg(JITID, unsigned, ArgType, const void**, ArgDeref);
template void
Runtime::registerArg(JITID, unsigned, ArgType, const bool*, int32_t, ArgDeref);
template void Runtime::registerArg(JITID,
                                   unsigned,
                                   ArgType,
                                   const int8_t*,
                                   int32_t,
                                   ArgDeref);
template void Runtime::registerArg(JITID,
                                   unsigned,
                                   ArgType,
                                   const int16_t*,
                                   int32_t,
                                   ArgDeref);
template void Runtime::registerArg(JITID,
                                   unsigned,
                                   ArgType,
                                   const int32_t*,
                                   int32_t,
                                   ArgDeref);
template void Runtime::registerArg(JITID,
                                   unsigned,
                                   ArgType,
                                   const int64_t*,
                                   int32_t,
                                   ArgDeref);
template void
Runtime::registerArg(JITID, unsigned, ArgType, const float*, int32_t, ArgDeref);
template void Runtime::registerArg(JITID,
                                   unsigned,
                                   ArgType,
                                   const double*,
                                   int32_t,
                                   ArgDeref);
template void Runtime::registerArg(JITID,
                                   unsigned,
                                   ArgType,
                                   const long double*,
                                   int32_t,
                                   ArgDeref);

#ifdef ENABLED_CUDA
void Runtime::executeCuda(JITID key,
                          FunctionSignatureBasic sig,
                          unsigned grid_x,
                          unsigned grid_y,
                          unsigned grid_z,
                          unsigned block_x,
                          unsigned block_y,
                          unsigned block_z,
                          unsigned sharedMemBytes,
                          CUstream hstream,
                          void** kernelParams) {
  FunctionSignatureTuning version = Config::getUntunedFunctionSignatureTuning();
  if(not isCompiled(key, sig)) {
    Function& f
        = *jitContext.getModule(key).getFunction(jitContext.getName(key));
    unsigned nargs = f.getFunctionType()->getNumParams();
    compile(key, sig, (const byte**)kernelParams, nargs);
  }

  statsStartCall(key);
  CUfunction cudaFunction = static_cast<CUfunction>(getCompiled(key, sig));
  statsStopCall(key);

  TalynCuda* talynCuda = static_cast<TalynCuda*>(talyn.at(SourceLang::Cuda));
  statsStartExecute(key, sig, version);
  talynCuda->executeCuda(cudaFunction,
                         grid_x,
                         grid_y,
                         grid_z,
                         block_x,
                         block_y,
                         block_z,
                         sharedMemBytes,
                         hstream,
                         kernelParams);
  statsStopExecute(key, sig, version);
}
#endif // ENABLED_CUDA

void Runtime::statsStartCall(JITID key) {
  stats.startCall(key);
}

void Runtime::statsStopCall(JITID key) {
  stats.stopCall(key);
}

void Runtime::statsStartExecute(JITID key,
                                FunctionSignatureBasic sig,
                                FunctionSignatureTuning version) {
  stats.startExecute(key, sig, version);
}

void Runtime::statsStopExecute(JITID key,
                               FunctionSignatureBasic sig,
                               FunctionSignatureTuning version) {
  stats.stopExecute(key, sig, version);
}

} // namespace moya
