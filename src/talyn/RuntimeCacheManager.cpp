#include "RuntimeCacheManager.h"
#include "Stats.h"
#include "common/Config.h"
#include "common/PassManager.h"
#include "common/PathUtils.h"
#include "common/Serializer.h"

#include <llvm/Support/FileSystem.h>
#include <llvm/Support/raw_ostream.h>

#include <sstream>

namespace moya {

RuntimeCacheManager::RuntimeCacheManager()
    : CacheManagerBase(Config::getEnvSaveDir()) {
  ;
}

void RuntimeCacheManager::setSubdir(const std::string& subdir) {
  setCacheDir(PathUtils::join(getDirectory(), subdir));
}

void RuntimeCacheManager::writeStats(const Stats& stats,
                                     const std::string& label) {
  Serializer serializer;
  serializer.initialize(getFilename(label, "json"));

  stats.serialize(serializer);
  serializer.finalize();
}

void RuntimeCacheManager::writeAssembler(llvm::Module& module,
                                         const std::string& label) {
  std::error_code ec;
  std::string filename = getFilename(label, "s");
  llvm::raw_fd_ostream fs(filename, ec, llvm::sys::fs::OpenFlags::F_None);
  if(ec)
    moya_error("Error opening output assembler file: " << filename);

  PassManager writer(module);
  writer.addEmitAssemblerFilePass(fs);
  writer.run(module);
}

void RuntimeCacheManager::writeModule(llvm::Module& module,
                                      const std::string& label) {
  std::error_code ec;
  std::string filename = getFilename(label, "ll");
  llvm::raw_fd_ostream fs(filename, ec, llvm::sys::fs::OpenFlags::F_None);
  if(ec)
    moya_error("Error opening output LLVM IR file: " << filename);

  fs << module;

  fs.close();
}

template <typename T,
          std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
static void writeSerializedArg(const CallArg& arg, Serializer& s) {
  switch(arg.getDeref()) {
  case ArgDeref::None:
    s.add("arg", arg.getArgAs<T>());
    break;
  case ArgDeref::Many:
    s.arrStart("sig");
    for(unsigned i = 0; i < arg.getCount(); i++)
      s.serialize(arg.getArgAs<const T*>()[i]);
    s.arrEnd("sig");
    break;
  default:
    break;
  }
}

template <typename T,
          std::enable_if_t<std::is_same<T, const void*>::value, int> = 0>
static void writeSerializedArg(const CallArg& arg, Serializer& s) {
  s.add("arg", arg.getArgAs<T>());
}

template <typename T,
          std::enable_if_t<std::is_pointer<T>::value
                               && !std::is_same<T, const void*>::value,
                           int> = 0>
static void writeSerializedArg(const CallArg& arg, Serializer& s) {
  s.add("arg", arg.getArgAs<T>());
  switch(arg.getDeref()) {
  case ArgDeref::One:
    s.add("sig", *arg.getArgAs<T>());
    break;
  default:
    break;
  }
}

void RuntimeCacheManager::writeSpecializedArgs(
    const moya::Vector<CallArg>& args,
    const std::string& label) {
  std::error_code ec;
  std::string filename = getFilename(label, "args");

  Serializer s;
  s.initialize(filename);
  s.mapStart();
  for(unsigned i = 0; i < args.size(); i++) {
    const CallArg& arg = args.at(i);
    s.mapStart(moya::str(i));
    if(arg.getDeref() == ArgDeref::Ignore) {
      s.add("ignore", true);
    } else {
      switch(arg.getType()) {
      case ArgType::Bool:
        s.add("type", "bool");
        writeSerializedArg<bool>(arg, s);
        break;
      case ArgType::Int8:
        s.add("type", "i8");
        writeSerializedArg<int8_t>(arg, s);
        break;
      case ArgType::Int16:
        s.add("type", "i16");
        writeSerializedArg<int16_t>(arg, s);
        break;
      case ArgType::Int32:
        s.add("type", "i32");
        writeSerializedArg<int32_t>(arg, s);
        break;
      case ArgType::Int64:
        s.add("type", "i64");
        writeSerializedArg<int64_t>(arg, s);
        break;
      case ArgType::Float:
        s.add("type", "float");
        writeSerializedArg<float>(arg, s);
        break;
      case ArgType::Double:
        s.add("type", "double");
        writeSerializedArg<double>(arg, s);
        break;
      case ArgType::LongDouble:
        s.add("type", "long double");
        s.add("arg", "<<not supported>>");
        // writeSerializedArg<long double>(arg, s);
        break;
      case ArgType::Ptr:
        s.add("type", "void*");
        writeSerializedArg<const void*>(arg, s);
        break;
      case ArgType::BoolPtr:
        s.add("type", "bool*");
        writeSerializedArg<const bool*>(arg, s);
        break;
      case ArgType::Int8Ptr:
        s.add("type", "i8*");
        writeSerializedArg<const int8_t*>(arg, s);
        break;
      case ArgType::Int16Ptr:
        s.add("type", "i16*");
        writeSerializedArg<const int16_t*>(arg, s);
        break;
      case ArgType::Int32Ptr:
        s.add("type", "i32*");
        writeSerializedArg<const int32_t*>(arg, s);
        break;
      case ArgType::Int64Ptr:
        s.add("type", "i64*");
        writeSerializedArg<const int64_t*>(arg, s);
        break;
      case ArgType::FloatPtr:
        s.add("type", "float*");
        writeSerializedArg<const float*>(arg, s);
        break;
      case ArgType::DoublePtr:
        s.add("type", "double*");
        writeSerializedArg<const double*>(arg, s);
        break;
      case ArgType::LongDoublePtr:
        s.add("type", "long double*");
        s.add("arg", "<<not supported>>");
        // writeSerializedArg<long double*>(arg, s);
        break;
      case ArgType::PtrPtr:
        s.add("type", "void**");
        writeSerializedArg<const void**>(arg, s);
        break;
      default:
        break;
      }
    }
    s.mapEnd(moya::str(i));
  }
  s.finalize();
}

void RuntimeCacheManager::writeOptReport(const std::string& report,
                                         const std::string& label) {
  std::error_code ec;
  std::string filename = getFilename(label, "txt");
  llvm::raw_fd_ostream fs(filename, ec, llvm::sys::fs::OpenFlags::F_None);
  if(ec)
    moya_error("Error opening output optimization report file: " << filename);
  fs << report << "\n";

  fs.close();
}

} // namespace moya
