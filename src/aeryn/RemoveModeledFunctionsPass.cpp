#include "Passes.h"
#include "common/Metadata.h"
#include "common/Vector.h"

#include <llvm/Pass.h>

class RemoveModeledPass : public llvm::ModulePass {
public:
  RemoveModeledPass();

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);

public:
  static char ID;
};

RemoveModeledPass::RemoveModeledPass() : llvm::ModulePass(ID) {
  // llvm::initializeRemoveModeledPassPass(llvm::PassRegistry::getPassRegistry());
}

llvm::StringRef RemoveModeledPass::getPassName() const {
  return "Moya Remove Modeled Functions";
}

void RemoveModeledPass::getAnalysisUsage(llvm::AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool RemoveModeledPass::runOnModule(llvm::Module& module) {
  bool changed = false;

  // This pass was originally designed to work around PlasCom2 which has
  // classes that inherit from std::*stream. These use virtual inheritance
  // which results in the inclusion of vtable functions in the code that
  // are not analyzable resulting in Moya's analysis failing, often
  // spectacularly. The workaround was to introduce a pragma that would
  // specify that a function in the source was to be modeled instead of
  // being analyzed. By deleting those functions here, hopefully the vtables
  // would also be removed in a subsequent DCE pass.
  //
  // FIXME: Could we also remove functions with Metadata::NoAnalyze here?
  // The effect this would have would probably be to break JIT'ing of C++
  // functions that use STL containers because most of STL has been marked
  // as NoAnalyze.
  //
  moya::Vector<llvm::Function*> remove;
  for(llvm::Function& f : module.functions())
    if(f.size() and moya::Metadata::hasForceModel(f))
      remove.push_back(&f);

  for(llvm::Function* f : remove)
    f->deleteBody();

  changed |= remove.size();

  return changed;
}

char RemoveModeledPass::ID = 0;

using namespace llvm;

static const char* name = "moya-remove-modeled";
static const char* descr = "Removes functions that are to be modeled";
static const bool cfg = true;
static const bool analysis = false;

// INITIALIZE_PASS(RemoveModeledPass, name, descr, cfg, analysis)

static RegisterPass<RemoveModeledPass> X(name, descr, cfg, analysis);

Pass* createRemoveModeledPass() {
  return new RemoveModeledPass();
}
