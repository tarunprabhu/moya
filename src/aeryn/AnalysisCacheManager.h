#ifndef MOYA_AERYN_ANALYSIS_CACHE_MANAGER_H
#define MOYA_AERYN_ANALYSIS_CACHE_MANAGER_H

#include "common/APITypes.h"
#include "common/CacheManagerBase.h"
#include "common/Dotifier.h"
#include "common/Serializer.h"

#include <string>

namespace moya {

class Environment;
class Store;
class Summary;

class AnalysisCacheManager : public CacheManagerBase {
private:
  Serializer serializer;
  Dotifier dotifier;

private:
  Serializer& getSerializer(const std::string&, const std::string& = "json");
  Dotifier& getDotifier(const std::string&, const std::string& = "dot");

public:
  AnalysisCacheManager(const std::string&,
                       const std::string&,
                       const std::string&,
                       const std::string&);
  virtual ~AnalysisCacheManager();

  Serializer& getCompleteSerializer();
  Serializer& getSourceSerializer();
  Serializer& getEnvironmentSerializer();
  Serializer& getStoreSerializer();
  Serializer& getTypesSerializer();
  Serializer& getClassHeirarchiesSerializer();
  Serializer& getCallGraphSerializer();
  Dotifier& getCallGraphDotifier(RegionID);
  Dotifier& getClosureDotifier(RegionID);
  Dotifier& getClassHeirarchiesDotifier();
  Serializer& getSummarySerializer(const Summary&);
  Serializer& getStatsSerializer(const std::string&);

  void writeStore(const Store&);
  void writeEnvironment(const Environment&);
  void writeSummary(const Summary&);
};

} // namespace moya

#endif // MOYA_AERYN_ANALYSIS_CACHE_MANAGER_H
