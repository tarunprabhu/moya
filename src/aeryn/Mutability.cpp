#include "Mutability.h"
#include "AbstractUtils.h"
#include "AnalysisCacheManager.h"
#include "common/AerynConfig.h"
#include "common/CallGraph.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/Hexify.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/PathUtils.h"
#include "common/StoreCell.h"
#include "common/StringUtils.h"
#include "common/Vector.h"

#include <llvm/IR/DebugInfo.h>
#include <llvm/IR/DebugInfoMetadata.h>
#include <llvm/IR/GlobalVariable.h>
#include <llvm/IR/InlineAsm.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Intrinsics.h>
#include <llvm/IR/TypeFinder.h>
#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;

namespace moya {

void Mutability::printInstructionOperands(const Instruction* inst,
                                          const Environment& env,
                                          const Store& store,
                                          const Function* f,
                                          const Module& module) const {
  errs() << *inst << "\n";
  if(not moya::Metadata::hasIgnore(inst)) {
    if(const auto* call = dyn_cast<CallInst>(inst)) {
      printCallees(call, call->getCalledValue(), env, store, f, module);
      for(unsigned i = 0; i < call->getNumArgOperands(); i++)
        errs() << "    " << i << ": "
               << moya::str(env.lookup(call->getArgOperand(i))) << "\n";
    } else if(const auto* call = dyn_cast<InvokeInst>(inst)) {
      printCallees(call, call->getCalledValue(), env, store, f, module);
      for(unsigned i = 0; i < call->getNumArgOperands(); i++)
        errs() << "    " << i << ": "
               << moya::str(env.lookup(call->getArgOperand(i))) << "\n";
    } else {
      for(unsigned i = 0; i < inst->getNumOperands(); i++)
        errs() << "    " << i << ": "
               << moya::str(env.lookup(inst->getOperand(i))) << "\n";
    }
  }
}

void Mutability::printInstructionResult(const Instruction* inst,
                                        const Environment& env,
                                        const Store& store,
                                        const Function* f,
                                        const Module& module) const {
  if(not inst->getType()->isVoidTy() and not moya::Metadata::hasIgnore(inst)) {
    errs() << "    result: " << moya::str(env.lookup(inst)) << "\n";
    if(moya::Metadata::hasAnalysisType(inst))
      errs() << "    analysis: " << *moya::Metadata::getAnalysisType(inst)
             << "\n";
  }
}

void Mutability::printCallees(const Instruction* call,
                              const Value* called,
                              const Environment& env,
                              const Store& store,
                              const Function* callee,
                              const Module& module) const {
  errs() << "    callees: [";
  for(const Function* f : getCallees(call, env.lookup(called), store))
    if(moya::Metadata::hasModel(f))
      errs() << "*" << moya::Metadata::getModel(f) << " ";
    else
      errs() << getDiagnostic(f) << " ";
  errs() << "]\n";
}

void Mutability::printFunction(unsigned now,
                               const Function* f,
                               const Environment& env,
                               const Store& store) const {
  const std::string& fname = f->getName().str();
  if(now == moya::Config::DebugFnBefore) {
    errs() << "Begin: " << fname << " [ " << *f->getFunctionType() << "]\n";
    for(const Argument& arg : f->args())
      errs() << "    " << arg.getArgNo() << ": " << env.lookup(&arg).str()
             << "\n"
             << "      " << *LLVMUtils::getAnalysisType(arg) << "\n";
  } else if(now == moya::Config::DebugFnAfter) {
    errs() << "End: " << fname << "\n";
  }
}

void Mutability::maybePrintCall(const Instruction* inst,
                                const Set<const Function*>& callees,
                                const Environment& env,
                                const Store& store,
                                const Function* caller,
                                const Module& module) const {
  for(const Function* f : callees) {
    if(debugFnsImmediate.contains(f->getName())) {
      errs() << "Callsite: " << *inst << "\n";
      errs() << "Caller: " << caller->getName() << "\n";
      errs() << "Calling: " << f->getName() << "\n";
      if(const auto* call = dyn_cast<CallInst>(inst))
        for(const Use& op : call->arg_operands())
          errs() << moya::str(env.lookup(op.get())) << "\n";
      else if(const auto* call = dyn_cast<InvokeInst>(inst))
        for(const Use& op : call->arg_operands())
          errs() << moya::str(env.lookup(op.get())) << "\n";
    }
  }
}

void Mutability::maybePrintStore(unsigned now,
                                 const Function* f,
                                 const Environment& env,
                                 const Store& store) const {
  if(f and debugFnsImmediate.contains(f->getName().str())
     and (debugStore & now))
    errs() << env.str() << "\n" << store.str() << "\n";
}

void Mutability::maybePrintInstructionOperands(const Instruction* inst,
                                               const Environment& env,
                                               const Store& store,
                                               const Function* f,
                                               const Module& module) const {
  if(f and debugFnsImmediate.contains(f->getName().str()))
    printInstructionOperands(inst, env, store, f, module);
}

void Mutability::maybePrintInstructionResult(const Instruction* inst,
                                             const Environment& env,
                                             const Store& store,
                                             const Function* f,
                                             const Module& module) const {
  if(f and debugFnsImmediate.contains(f->getName().str()))
    printInstructionResult(inst, env, store, f, module);
}

void Mutability::maybePrintFunction(unsigned now,
                                    const Function* f,
                                    const Environment& env,
                                    const Store& store) const {
  if(f and debugFnsImmediate.contains(f->getName().str()))
    printFunction(now, f, env, store);
}

Vector<const Function*>
Mutability::processLevel(const Vector<const Function*>& fns,
                         const HashSet<const Function*>& seen) {
  HashSet<const Function*> uniq;
  Vector<const Function*> callees;
  for(const Function* f : fns)
    for(auto i = inst_begin(f); i != inst_end(f); i++)
      if(const Function* callee = LLVMUtils::getCalledFunction(&*i, false))
        if(callee->size() and not seen.contains(callee)
           and not uniq.contains(callee)) {
          callees.push_back(callee);
          uniq.insert(callee);
        }
  return callees;
}

Vector<const Function*>
Mutability::generateProcessingOrder(const Function* root) {
  Map<unsigned, Vector<const Function*>> depths;
  HashSet<const Function*> seen;
  Vector<const Function*> order;
  unsigned level = 0;
  const Module& module = *LLVMUtils::getModule(root);

  depths[level] = Vector<const Function*>();
  for(const Function& f : module.functions())
    if(moya::Metadata::hasGlobalConstructor(f)
       or moya::Metadata::hasGlobalDestructor(f))
      depths.at(level).push_back(&f);

  depths.at(level).push_back(root);
  for(level += 1; depths.at(level - 1).size(); level++) {
    for(const Function* f : depths.at(level - 1))
      seen.insert(f);
    depths[level] = Vector<const Function*>();
    for(const Function* f : processLevel(depths.at(level - 1), seen))
      depths.at(level).push_back(f);
  }

  // Any function that is not in the build order was probably called indirectly
  // (passed as an argument to another function and called as a value) so it
  // should be added as the lowest level.
  for(const Function& f : module.functions())
    if(f.size() and not seen.contains(&f))
      depths[level].push_back(&f);

  for(auto& it : depths)
    for(const Function* f : it.second)
      if(not(moya::Metadata::hasNoAnalyze(f) or moya::Metadata::hasModel(f)))
        order.push_back(f);
  return order;
}

void Mutability::initializeZero(PointerType* pty,
                                const Store::Address* base,
                                Store& store) {
  store.write(base, store.getNullptr(), pty);
}

void Mutability::initializeZero(ArrayType* aty,
                                const Store::Address* base,
                                Store& store) {
  Type* elem = LLVMUtils::getFlattenedElementType(aty);
  uint64_t nelems = store.getArrayLength(base, elem, 1);
  uint64_t size = store.getTypeAllocSize(elem);
  for(uint64_t i = 0; i < nelems; i++)
    initializeZero(elem, store.make(base, i * size), store);
}

void Mutability::initializeZero(StructType* sty,
                                const Store::Address* base,
                                Store& store) {
  const moya::StructLayout& sl = store.getStructLayout(sty);
  for(unsigned i = 0; i < sty->getNumElements(); i++)
    initializeZero(sty->getElementType(i),
                   store.make(base, sl.getElementOffset(i)),
                   store);
}

void Mutability::initializeZero(Type* type,
                                const Store::Address* base,
                                Store& store) {
  if(auto* ity = dyn_cast<IntegerType>(type))
    store.write(base, store.make(ConstantInt::get(ity, 0)), type);
  else if(type->isFloatTy() or type->isDoubleTy() or type->isX86_FP80Ty())
    store.write(base, store.make(ConstantFP::get(type, 0.0)), type);
  else if(auto* pty = dyn_cast<PointerType>(type))
    initializeZero(pty, base, store);
  else if(auto* aty = dyn_cast<ArrayType>(type))
    initializeZero(aty, base, store);
  else if(auto* sty = dyn_cast<StructType>(type))
    initializeZero(sty, base, store);
  else
    moya_error("Unsupported type to zero-initialize: " << *type << " at "
                                                       << base->str());
}

Set<const Function*> Mutability::getCallees(const Instruction* call,
                                            const Contents& called,
                                            Type* calledType,
                                            Environment& env,
                                            Store& store,
                                            const Function* caller,
                                            AnalysisStatus& changed) const {
  Set<const Function*> ret;

  // In indirect calls, this might be a pointer to a value
  Type* fty = calledType;
  if(auto* pty = dyn_cast<PointerType>(fty))
    fty = pty->getElementType();

  for(const auto* code : store.readAs<ContentCode>(called, fty, call, changed))
    ret.insert(code->getFunction());

  return ret;
}

Set<const Function*> Mutability::getCallees(const Instruction* call,
                                            const Contents& called,
                                            const Store& store) const {
  Set<const Function*> ret;

  for(const ContentAddress* addr : called.getAs<ContentAddress>(call))
    for(const ContentCode* code : store.read(addr).getAs<ContentCode>(call))
      ret.insert(code->getFunction());

  return ret;
}

AnalysisStatus Mutability::processCall(const Instruction* call,
                                       const Value* called,
                                       const Vector<const Value*>& args,
                                       Environment& env,
                                       Store& store,
                                       const Function* caller,
                                       const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Vector<Contents> cargs;
  Vector<Type*> argTys;
  for(const Value* arg : args) {
    cargs.push_back(env.lookup(arg));
    argTys.push_back(LLVMUtils::getAnalysisType(arg));
  }

  Set<const Function*> callees = getCallees(call,
                                            env.lookup(called),
                                            LLVMUtils::getAnalysisType(called),
                                            env,
                                            store,
                                            caller,
                                            changed);
  maybePrintCall(call, callees, env, store, caller, module);

  changed |= AbstractUtils::callFunction(
      call, callees, cargs, argTys, models, env, store, caller);

  return changed;
}

AnalysisStatus Mutability::processInlineAsm(const Instruction* call,
                                            const InlineAsm* asmCode,
                                            const Vector<const Value*>& args,
                                            Environment& env,
                                            Store& store,
                                            const Function* caller,
                                            const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // FIXME: This may be unsafe. This assumes that any ASM that modifies memory
  // only modifies memory accessible in a "sane" way from any pointers passed
  // to the asm call. "Sane" here means that the pointer must either:
  //
  // - point to an explicitly allocated object (statically or dynamically)
  //   and does not access any memory outside of the object.
  //
  //         OR
  //
  // - points to a field of an object and does not access any memory outside
  //   of that field
  //
  if(moya::Metadata::hasAsmClobbersMemory(call))
    for(const Value* arg : args)
      if(auto* pty = dyn_cast<PointerType>(arg->getType()))
        for(const auto* addr : env.lookup(arg).getAs<Address>(call))
          store.write(addr,
                      store.make(pty->getElementType()),
                      pty->getElementType(),
                      call,
                      changed);

  Type* ret = call->getType();
  Vector<Type*> types;
  if(auto* sty = dyn_cast<StructType>(ret))
    for(Type* type : sty->elements())
      types.push_back(type);
  else if(LLVMUtils::isScalarTy(ret) or ret->isPointerTy())
    types.push_back(ret);
  else if(ret->isVoidTy())
    ;
  else
    moya_error("Unsupported return type from asm: " << *ret << " at "
                                                    << getDiagnostic(call));

  Vector<Contents> contents;
  if(types.size()) {
    size_t inpIdx = -1;
    size_t typIdx = types.size() - 1;
    InlineAsm::ConstraintInfoVector constraints = asmCode->ParseConstraints();
    for(size_t i = constraints.size(); i > 0; i--) {
      const InlineAsm::ConstraintInfo& constraint = constraints.at(i - 1);
      if(constraint.Type == InlineAsm::ConstraintPrefix::isOutput) {
        if(constraint.MatchingInput != -1) {
          contents.push_back(
              env.lookup(args.at(constraint.MatchingInput - inpIdx)));
        } else {
          contents.push_back(store.make(types.at(typIdx)));
        }
        typIdx--;
      } else if(constraint.Type == InlineAsm::ConstraintPrefix::isInput) {
        inpIdx = i - 1;
      }
    }
  }

  if(contents.size() == 1) {
    env.push(contents.at(0));
  } else if(contents.size() != 0) {
    contents.reverse();
    env.push(store.make(contents));
  }

  return changed;
}

// DataLayout::getIndexedOperand requires that the base type be a pointer
// type. Because the range of each compile-time unknown index is different,
// we have to process them one at a time.
AnalysisStatus Mutability::processIndices(const GetElementPtrInst* gep,
                                          unsigned op,
                                          Type* type,
                                          Vector<const Value*> indices,
                                          const Store::Address* base,
                                          int64_t offset,
                                          Environment& env,
                                          Store& store,
                                          const Function* f,
                                          const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  Type* nextTy = nullptr;
  Type* ety = type;
  if(auto* aty = dyn_cast<ArrayType>(type))
    ety = LLVMUtils::getFlattenedElementType(aty);

  int64_t begin = 0;
  int64_t end = store.getArrayLength(store.make(base, offset), ety, 1);
  if(auto* sty = dyn_cast<StructType>(type))
    end = sty->getNumElements();

  // The very first index of the GEP instruction determines which element of
  // the "array" that the pointer points to is being referenced. Once we are
  // "inside" the structure being processed by the GEP, we don't have to worry
  // about the inner types because no pointers are dereferenced
  if(op == 1) {
    if(auto* pty = dyn_cast<PointerType>(type))
      end = store.getArrayLength(base, pty->getElementType(), end);
    else if(auto* aty = dyn_cast<ArrayType>(type))
      end = store.getArrayLength(
          base, LLVMUtils::getFlattenedElementType(aty), end);
  }

  // We need to process indices depending on the type of the object which
  // we are currently "in". The LLVM type itself is not the best to use because
  // it could have been cast to something else. For instance, a struct type
  // could be cast to void* and then the field of the struct accessed using
  // the absolute offset of the field from the start of the struct rather than
  // using the index of the field.
  //
  // But we can't be exactly sure of what type we are "in" because there
  // are multiple types that could overlap at any given address. For instance,
  // if the first element of a struct is an array, then there are at least
  // two types at an offset of 0 - the array and the element of the array.
  //
  const Value* idx = gep->getOperand(op);
  if(auto* cint = dyn_cast<ConstantInt>(idx)) {
    int64_t val = (int64_t)cint->getLimitedValue();
    if(auto* pty = dyn_cast<PointerType>(type)) {
      if(val >= begin and val < end) {
        begin = val;
        end = val + 1;
      } else if(val >= end) {
        // If we are looking beyond the end of what we should be looking at,
        // it is probably because we are dealing with a truncated array.
        // On the other hand, it is isn't a truncated array, this is almost
        // certainly a bug in the code. In either case, we should be touching
        // all the elements of the array. This has already been computed
        // so we don't need to adjust begin and end.
      } else {
        moya_error("Negative index?: " << val << " at " << getDiagnostic(gep));
      }
      nextTy = pty->getElementType();
    } else if(auto* aty = dyn_cast<ArrayType>(type)) {
      if(val >= begin and val < end) {
        begin = val;
        end = val + 1;
      } else if(val >= end) {
        if(not(LLVMUtils::isScalarArrayTy(aty)
               or LLVMUtils::isStructTy(aty->getElementType()))) {
          // If we are looking beyond the end of what we should be looking at,
          // it is probably because we are dealing with a truncated array.
          // On the other hand, it is isn't a truncated array, this is almost
          // certainly a bug in the code. In either case, we should be touching
          // all the elements of the array. This has already been computed
          // so we don't need to adjust begin and end.
          moya_error("Index beyond end of array\n"
                     << "   idx: " << val << "\n"
                     << "  type: " << *aty << "\n"
                     << "  inst: " << getDiagnostic(gep));
        }
      } else {
        moya_error("Negative index?: " << *gep);
      }
      nextTy = aty->getElementType();
    } else if(auto* sty = dyn_cast<StructType>(type)) {
      if(val >= begin and val < end) {
        begin = val;
        end = val + 1;
      } else {
        moya_error("Invalid index for struct\n"
                   << "   idx: " << val << "\n"
                   << "  type: " << *sty << "\n"
                   << "  inst: " << getDiagnostic(gep));
      }
      nextTy = sty->getElementType(val);
    } else {
      moya_error("Unexpected type in GEP\n"
                 << "  type: " << *type << "\n"
                 << "  inst: " << getDiagnostic(gep));
    }
  } else {
    if(auto* pty = dyn_cast<PointerType>(type)) {
      begin = 0;
      end = 1;
      nextTy = pty->getElementType();
    } else if(auto* aty = dyn_cast<ArrayType>(type)) {
      // If this is not a truncated array, we need to iterate over all the
      // elements of the array
      nextTy = aty->getElementType();
    } else if(StructType* sty = dyn_cast<StructType>(type)) {
      if(op == gep->getNumIndices()) {
        moya_warning("Expected constant operand for struct field\n"
                     << "    op: " << op << "\n"
                     << "  inst: " << getDiagnostic(gep));
        // This can happen when a struct is converted into an array of bytes
        // (or some other scalar) and then GEP'ed. I am not sure how safe it is
        // to allow this, but for the moment, it will stay.
        // If the cause of reaching this situation is not a "safe" cast,
        // then the analysis will eventually derail
        for(uint64_t offset : LLVMUtils::getFlattenedOffsets(sty, module))
          changed |= env.add(gep, store.make(base, offset));
        return changed;
      } else {
        moya_error("Expected constant operand for struct field\n"
                   << "    op: " << op << "\n"
                   << "  inst: " << getDiagnostic(gep));
      }
    } else {
      moya_error("Unexpected type in GEP\n"
                 << "  type: " << *type << "\n"
                 << "  inst: " << getDiagnostic(gep));
    }
  }

  indices.push_back(nullptr);
  Type* idxTy = LLVMUtils::getAnalysisType(idx);
  // The indices will correspond to the actual type that is being passed in,
  // not the analysis type
  Type* elemTy
      = dyn_cast<PointerType>(gep->getPointerOperandType())->getElementType();
  if(op == gep->getNumIndices()) {
    for(int64_t i = begin; i < end; i++) {
      indices[indices.size() - 1] = ConstantInt::getSigned(idxTy, i);
      int64_t offset = store.getOffsetInType(elemTy, indices);
      changed |= env.add(gep, store.make(base, offset));
    }
  } else {
    for(int64_t i = begin; i < end; i++) {
      indices[indices.size() - 1] = ConstantInt::getSigned(idxTy, i);
      int64_t offset = store.getOffsetInType(elemTy, indices);
      changed |= processIndices(
          gep, op + 1, nextTy, indices, base, offset, env, store, f, module);
    }
  }
  return changed;
}

AnalysisStatus Mutability::processIndices(const InsertValueInst* insval,
                                          const Address* base,
                                          Type* type,
                                          const ArrayRef<unsigned>& indices,
                                          unsigned idx,
                                          Environment& env,
                                          Store& store,
                                          const Function* f,
                                          const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  unsigned i = indices[idx];
  unsigned offset = 0;
  Type* nextType = nullptr;
  if(auto* aty = dyn_cast<ArrayType>(type)) {
    moya_error("Unimplemented: Array types in insert value\n"
               << "  inst: " << *insval);
  } else if(auto* sty = dyn_cast<StructType>(type)) {
    const moya::StructLayout& sl = store.getStructLayout(sty);
    if(i > sty->getNumElements())
      moya_error("Unexpected index in struct type:\n"
                 << "  inst: " << getDiagnostic(insval) << "\n"
                 << "   sty: " << *sty << "\n"
                 << "   idx: " << i);
    offset = sl.getElementOffset(i);
    nextType = sty->getElementType(i);
  } else {
    moya_error("Unexpected type in insert value:\n"
               << "  inst: " << getDiagnostic(insval) << "\n"
               << "  type: " << *type);
  }

  if(idx == indices.size() - 1) {
    store.write(store.make(base, offset),
                env.lookup(insval->getInsertedValueOperand()),
                nextType);
  } else {
    changed |= processIndices(insval,
                              store.make(base, offset),
                              nextType,
                              indices,
                              idx + 1,
                              env,
                              store,
                              f,
                              module);
  }

  return changed;
}

AnalysisStatus Mutability::processIndices(const ExtractValueInst* ext,
                                          const ContentComposite* c,
                                          const ArrayRef<unsigned>& indices,
                                          unsigned idx,
                                          Environment& env,
                                          Store& store,
                                          const Function* f,
                                          const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  unsigned i = indices[idx];
  if(idx == indices.size() - 1)
    changed |= env.add(ext, c->at(i));
  else
    for(const Content* content : c->at(i))
      changed |= processIndices(ext,
                                dyn_cast<ContentComposite>(content),
                                indices,
                                idx + 1,
                                env,
                                store,
                                f,
                                module);

  return changed;
}

AnalysisStatus Mutability::process(const AllocaInst* alloca,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(alloca)) {
    Type* allocated = LLVMUtils::getAllocatedType(alloca);

    if(auto* cint = dyn_cast<ConstantInt>(alloca->getArraySize())) {
      if(cint->getLimitedValue() > 1)
        allocated = ArrayType::get(allocated, cint->getLimitedValue());
    } else {
      allocated = ArrayType::get(allocated, AerynConfig::getArrayLength());
    }

    CellFlags flags(CellFlags::Stack, CellFlags::Initialized);
    const Address* addr = store.allocate(allocated, flags, true, alloca, f);
    changed |= store.deallocate(allocated, addr, nullptr, f);
    changed |= env.addAlloca(alloca, addr);
  }

  return changed;
}

AnalysisStatus Mutability::process(const BinaryOperator* binst,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // It would be nice to be able to do some arithmetic, but there's
  // every chance that I will end up with non-termination issues
  changed |= env.add(binst, store.getRuntimeScalar());

  return changed;
}

AnalysisStatus Mutability::process(const BranchInst* br,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  // Because we aren't intepreting in a fully flow sensitive manner,
  // we ignore all branch instructions. We only care about the calls
  return AnalysisStatus::Unchanged;
}

AnalysisStatus Mutability::process(const IndirectBrInst* br,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  return AnalysisStatus::Unchanged;
}

AnalysisStatus Mutability::process(const AtomicRMWInst* rmw,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Value* ptr = rmw->getPointerOperand();
  const Value* val = rmw->getValOperand();

  changed |= process(ptr, env, store, f, module);
  changed |= process(val, env, store, f, module);
  changed |= AbstractUtils::store(rmw,
                                  env.lookup(ptr),
                                  LLVMUtils::getAnalysisType(ptr),
                                  env.lookup(val),
                                  LLVMUtils::getAnalysisType(val),
                                  env,
                                  store,
                                  f);

  return changed;
}

AnalysisStatus Mutability::process(const AtomicCmpXchgInst* xchg,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Value* ptr = xchg->getPointerOperand();
  const Value* val = xchg->getNewValOperand();

  changed |= process(ptr, env, store, f, module);
  changed |= process(val, env, store, f, module);
  changed |= AbstractUtils::store(xchg,
                                  env.lookup(ptr),
                                  LLVMUtils::getAnalysisType(ptr),
                                  env.lookup(val),
                                  LLVMUtils::getAnalysisType(val),
                                  env,
                                  store,
                                  f);

  return changed;
}

AnalysisStatus Mutability::process(const FenceInst* fence,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  return AnalysisStatus::Unchanged;
}

static bool
canCast(const BitCastInst* bitcast, const Store::Address* addr, Store& store) {
  const Module& module = *LLVMUtils::getModule(bitcast);
  LLVMContext& context = bitcast->getContext();
  Type* pi8 = Type::getInt8PtrTy(context);
  Type* pi64 = Type::getInt64PtrTy(context);
  auto* pty = LLVMUtils::getAnalysisTypeAs<PointerType>(bitcast);

  // Having an analysis type suggests that there has been some other analysis
  // pass that has already determined whether or not this cast is safe
  if(moya::Metadata::hasAnalysisType(bitcast))
    return true;

  // This probably is a cast to void* or byte*. In either cast, it should be
  // safe to do this. FIXME: Probably should add a Moya annotation to this
  // instruction
  if(pty == pi8)
    return true;

  // This is the obvious case where an object of the destination type does
  // exist at the address
  if(store.isTypeAt(addr, pty->getElementType()))
    return true;

  // If the object does not directly exist, we may still be in the case where
  // the object being cast to has a virtual base class in which case we may
  // only find the virtual header here
  if(auto* sty = dyn_cast<StructType>(pty->getElementType()))
    if(StructType* tty = store.getLargestStructAt(addr))
      if(moya::Metadata::hasVirtualBody(tty, module)
         and moya::Metadata::hasVirtualHeader(sty, module)
         and (moya::Metadata::getVirtualBody(tty, module) == sty)
         and (moya::Metadata::getVirtualHeader(sty, module) == tty))
        return true;

  // With LLVM > 3.6, pointers to pointers are often cast to
  // i64* before loading.
  if((pty == pi64) and bitcast->getOperand(0)->getType()->isPointerTy())
    return true;

  return false;
}

AnalysisStatus Mutability::process(const BitCastInst* bitcast,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(auto* pty = dyn_cast<PointerType>(bitcast->getDestTy()))
    for(const Content* c : env.lookup(bitcast->getOperand(0)))
      if(const auto* addr = dyn_cast<Address>(c))
        if(moya::Metadata::hasCastToBase(bitcast)
           or moya::Metadata::hasCastFromUnion(bitcast)
           or canCast(bitcast, addr, store))
          changed |= env.add(bitcast, c);
        else
          moya_warning("Cannot cast " << addr->str() << " at "
                                      << getDiagnostic(bitcast));
      else if(isa<ContentNullptr>(c) or isa<ContentUndef>(c))
        ;
      else if(isa<ContentRuntimePointer>(c))
        changed |= setTop(bitcast);
      else
        moya_warning("Casting something not a pointer: "
                     << *bitcast << " in " << f->getName() << ": " << c->str());
  else
    changed |= env.add(bitcast, env.lookup(bitcast->getOperand(0)));

  return changed;
}

AnalysisStatus Mutability::process(const InvokeInst* call,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Vector<const Value*> args;
  const Value* callee = call->getCalledValue();
  for(unsigned i = 0; i < call->getNumArgOperands(); i++)
    args.push_back(call->getArgOperand(i));

  changed |= processCall(call, callee, args, env, store, f, module);

  return changed;
}

AnalysisStatus Mutability::process(const CallInst* call,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Vector<const Value*> args;
  const Value* callee = call->getCalledValue();
  for(unsigned i = 0; i < call->getNumArgOperands(); i++)
    args.push_back(call->getArgOperand(i));

  if(const auto* inlineAsm = dyn_cast<InlineAsm>(callee))
    changed |= processInlineAsm(call, inlineAsm, args, env, store, f, module);
  else
    changed |= processCall(call, callee, args, env, store, f, module);

  return changed;
}

AnalysisStatus Mutability::process(const PtrToIntInst* ptrtoint,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* i64 = IntegerType::getInt64Ty(module.getContext());
  for(const Content* c : env.lookup(ptrtoint->getOperand(0))) {
    if(const auto* addr = dyn_cast<Address>(c)) {
      auto* cint = cast<ConstantInt>(ConstantInt::get(i64, addr->getRaw()));
      changed |= env.add(ptrtoint, store.make(cint));
    } else if(isa<ContentRuntimePointer>(c)) {
      changed |= env.add(ptrtoint, store.getRuntimeScalar());
    } else if(isa<ContentUndef>(c)) {
      ;
    } else {
      moya_warning("Expected address to ptrtoint: " << *ptrtoint
                                                    << "\nGot: " << c->str());
    }
  }

  return changed;
}

AnalysisStatus Mutability::process(const IntToPtrInst* inttoptr,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // The newer (> 3.6) versions of LLVM introduce inttoptr's all over the place
  // because pointers are constantly being treated as 64-bit integers.
  // Those are ok because we can usually analyze our way through them.
  // Casts from compile-time constants to pointers are much rarer and should
  // have the analysis going to TOP. We need to catch that separately becuase
  // looking for CScalar() objects won't work because if the pointer was
  // converted to int using ptrtoint, then there would be a CScalar object
  // there too.
  //
  // This could still appear in a ConstantExpr in which case we don't bother
  // with these hacks
  if(f) {
    if(const auto* cint = dyn_cast<ConstantInt>(inttoptr->getOperand(0))) {
      if(cint->isZero() or moya::Metadata::hasMemSafe(f))
        for(const Content* c : env.lookup(inttoptr->getOperand(0)))
          if(const auto* scalar = dyn_cast<ContentScalar>(c))
            changed |= env.add(inttoptr, store.make(scalar->getIntegerValue()));
          else if(isa<ContentRuntimeScalar>(c))
            changed |= env.add(inttoptr, store.getRuntimePointer());
          else
            moya_warning("Expected integer in inttoptr: "
                         << *inttoptr << "\nGot: " << c->str());
      else if((int64_t)cint->getLimitedValue() == (int64_t)-1)
        moya_warning("Ignoring: " << *inttoptr << " at " << getDiagnostic(f));
      else
        return setTop(inttoptr);
    } else {
      for(const Content* c : env.lookup(inttoptr->getOperand(0)))
        if(const auto* caddr = dyn_cast<ContentAddress>(c))
          changed |= env.add(inttoptr, caddr);
        else if(const auto* cscalar = dyn_cast<ContentScalar>(c))
          changed |= env.add(inttoptr, store.make(cscalar->getIntegerValue()));
        else if(isa<ContentRuntimeScalar>(c))
          changed |= env.add(inttoptr, store.getRuntimePointer());
        else
          return setTop(inttoptr);
    }
  }

  return changed;
}

AnalysisStatus Mutability::process(const FPExtInst* fpext,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(fpext, env.lookup(fpext->getOperand(0)));

  return changed;
}

AnalysisStatus Mutability::process(const SExtInst* sext,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(sext, env.lookup(sext->getOperand(0)));

  return changed;
}

AnalysisStatus Mutability::process(const ZExtInst* zext,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(zext, env.lookup(zext->getOperand(0)));

  return changed;
}

AnalysisStatus Mutability::process(const TruncInst* trunc,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(trunc, env.lookup(trunc->getOperand(0)));

  return changed;
}

AnalysisStatus Mutability::process(const FPTruncInst* trunc,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(trunc, env.lookup(trunc->getOperand(0)));

  return changed;
}

AnalysisStatus Mutability::process(const SIToFPInst* sitofp,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(sitofp, env.lookup(sitofp->getOperand(0)));

  return changed;
}

AnalysisStatus Mutability::process(const FPToSIInst* fptosi,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(fptosi, env.lookup(fptosi->getOperand(0)));

  return changed;
}

AnalysisStatus Mutability::process(const UIToFPInst* uitofp,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(uitofp, env.lookup(uitofp->getOperand(0)));

  return changed;
}

AnalysisStatus Mutability::process(const FPToUIInst* fptoui,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(fptoui, env.lookup(fptoui->getOperand(0)));

  return changed;
}

AnalysisStatus Mutability::process(const GetElementPtrInst* gep,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Value* ptr = gep->getPointerOperand();
  auto* ptrTy = LLVMUtils::getAnalysisTypeAs<PointerType>(ptr);
  Type* elemTy = ptrTy->getElementType();
  // The vtables are saved in the store as an array of i8*, so we need
  // to lookup that type. If this flag is set, then we are guaranteed that
  // GEPIndex will also have been set
  if(moya::Metadata::hasGEPVtableLookup(gep))
    elemTy = Type::getInt8PtrTy(gep->getContext());

  for(const auto* addr : env.lookup(ptr).getAs<Address>(gep)) {
    if(moya::Metadata::hasGEPIndex(gep)) {
      int64_t offset = moya::Metadata::getGEPIndex(gep);
      int64_t size = store.getTypeAllocSize(elemTy);
      bool ignore = false;
      if(moya::Metadata::hasGEPFieldLookup(gep)) {
        if(offset >= size) {
          moya_error("Accessing struct out of bounds:\n"
                     << "  offset: " << offset << "\n"
                     << "    size: " << size << "\n"
                     << "    elem: " << *elemTy << "\n"
                     << "    addr: " << addr->str() << "\n"
                     << "    inst: " << getDiagnostic(gep) << "\n\n"
                     << env.str() << "\n\n"
                     << store.str());
        }
      } else {
        int64_t length = store.getArrayLength(addr, elemTy, 1);
        if(offset >= length * size) {
          if(moya::Metadata::hasGEPOverflowIgnore(gep))
            ignore = true;
          else if(store.hasFlag(addr, CellFlags::LibAlloc))
            offset = 0;
          else if(store.hasFlag(addr, CellFlags::Heap))
            offset = 0;
          else
            moya_error("Accessing an array out of bounds:\n"
                       << "  offset: " << offset << "\n"
                       << "    size: " << length * size << "\n"
                       << "    elem: " << *elemTy << "\n"
                       << "    addr: " << addr->str() << "\n"
                       << "    inst: " << getDiagnostic(gep) << "\n\n"
                       << env.str() << "\n\n"
                       << store.str());
        }
      }
      if(not ignore)
        changed |= env.add(gep, store.make(addr, offset));
    } else if(gep->hasAllZeroIndices()) {
      // Apart from being a quick short-circuit (Let's be honest, it wasn't
      // really going to save us all that much), this works around a thorny
      // problem of [0 x type] allocas. For some reason, these do appear
      // in C++ code (probably the result of some template expansion insanity).
      // The canonicalized type of [0 x type] is just type, in which case there
      // is likely to be an exta offset in a GEP instruction that does anything
      // with this. These kludges seem to be accumulating ... :(
      changed |= env.add(gep, env.lookup(ptr));
    } else {
      changed
          |= processIndices(gep, 1, ptrTy, {}, addr, 0, env, store, f, module);
    }
  }

  return changed;
}

AnalysisStatus Mutability::process(const CmpInst* cmp,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(cmp))
    // We don't really use any boolean values that go in here anyway, so
    // we might as well make things simpler by just putting an unknown scalar
    // in there
    changed |= env.add(cmp, store.getRuntimeScalar());

  return changed;
}

AnalysisStatus Mutability::process(const LoadInst* load,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  const Value* ptr = load->getPointerOperand();

  changed
      |= AbstractUtils::load(load,
                             env.lookup(ptr),
                             LLVMUtils::getAnalysisType(load)->getPointerTo(),
                             env,
                             store,
                             f);

  return changed;
}

AnalysisStatus Mutability::process(const PHINode* phi,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  for(unsigned i = 0; i < phi->getNumIncomingValues(); i++) {
    Value* incoming = phi->getIncomingValue(i);
    changed |= process(incoming, env, store, f, module);
    if(auto* inst = dyn_cast<Instruction>(incoming)) {
      if(not moya::Metadata::hasLoopVariant(inst))
        changed |= env.add(phi, env.lookup(incoming));
    } else {
      changed |= env.add(phi, env.lookup(incoming));
    }
  }
  return changed;
}

AnalysisStatus Mutability::process(const SelectInst* select,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(select, env.lookup(select->getTrueValue()));
  changed |= env.add(select, env.lookup(select->getFalseValue()));

  return changed;
}

AnalysisStatus Mutability::process(const StoreInst* storeInst,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Value* val = storeInst->getValueOperand();
  const Value* ptr = storeInst->getPointerOperand();

  changed |= AbstractUtils::store(storeInst,
                                  env.lookup(ptr),
                                  LLVMUtils::getAnalysisType(ptr),
                                  env.lookup(val),
                                  LLVMUtils::getAnalysisType(val),
                                  env,
                                  store,
                                  f);

  return changed;
}

AnalysisStatus Mutability::process(const SwitchInst* swich,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  changed |= process(swich->getCondition(), env, store, f, module);
  // Because we aren't interpreting the progam in a fully flow-sensitive
  // manner, there's no need to do anything more with this.
  return changed;
}

AnalysisStatus Mutability::process(const InsertValueInst* insval,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  const Value* dst = insval->getAggregateOperand();
  ArrayRef<unsigned> indices = insval->getIndices();

  // The Content objects are immutable, so the only way to modify them is to
  // do it through the store, but we don't want to pollute the store,
  if(not env.contains(insval))
    changed |= env.addInsertValue(
        insval, store.allocate(dst->getType(), CellFlags::Dummy, true));

  const Store::Address* addr = env.lookupInsertValue(insval);
  StructType* sty = dyn_cast<StructType>(insval->getType());
  for(const Content* c : env.lookup(insval->getAggregateOperand()))
    if(const auto* ccomp = dyn_cast<ContentComposite>(c))
      store.write(addr, ccomp, sty);
    else if(isa<ContentUndef>(c))
      ;
    else
      moya_error("Expecting composite object:\n"
                 << "  inst: " << getDiagnostic(insval) << "\n"
                 << "   got: " << c->str());

  changed
      |= processIndices(insval, addr, sty, indices, 0, env, store, f, module);
  changed |= env.add(insval, store.read(addr, sty));

  return changed;
}

AnalysisStatus Mutability::process(const ExtractValueInst* extval,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  ArrayRef<unsigned> indices = extval->getIndices();

  for(const auto* ccomp :
      env.lookup(extval->getAggregateOperand()).getAs<ContentComposite>(extval))
    changed |= processIndices(extval, ccomp, indices, 0, env, store, f, module);

  return changed;
}

AnalysisStatus Mutability::process(const ReturnInst* retrn,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // The return value of malloc wrappers are a special case because there
  // should be a different return value for each callsite. That gets handled
  // elsewhere
  if(not(moya::Metadata::hasSystemMalloc(f)
         or moya::Metadata::hasUserMalloc(f))) {
    if(const Value* retval = retrn->getReturnValue()) {
      for(const Instruction* call : env.getCallSites(f))
        // A callsite may be null for the top-level functions (main, the
        // global constructors and global destructors)
        if(call)
          changed |= env.add(call, env.lookup(retval));
      env.add(retrn, env.lookup(retval));
    }
  }
  return changed;
}

AnalysisStatus Mutability::process(const ResumeInst* resume,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  return AnalysisStatus::Unchanged;
}

AnalysisStatus Mutability::process(const LandingPadInst* landing,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(landing, store.getExceptionObject());

  return changed;
}

AnalysisStatus Mutability::process(const UnreachableInst* inst,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  return AnalysisStatus::Unchanged;
}

AnalysisStatus Mutability::process(const Instruction* inst,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  maybePrintInstructionOperands(inst, env, store, f, module);

  // First process the operands, then the actual instruction
  for(const Use& op : inst->operands())
    changed |= process(op.get(), env, store, f, module);

  if(moya::Metadata::hasIgnore(inst))
    ;
  else if(const auto* alloca = dyn_cast<AllocaInst>(inst))
    changed |= process(alloca, env, store, f, module);
  else if(const auto* bitcast = dyn_cast<BitCastInst>(inst))
    changed |= process(bitcast, env, store, f, module);
  else if(const auto* inttoptr = dyn_cast<IntToPtrInst>(inst))
    changed |= process(inttoptr, env, store, f, module);
  else if(const auto* ptrtoint = dyn_cast<PtrToIntInst>(inst))
    changed |= process(ptrtoint, env, store, f, module);
  else if(const auto* fpext = dyn_cast<FPExtInst>(inst))
    changed |= process(fpext, env, store, f, module);
  else if(const auto* sext = dyn_cast<SExtInst>(inst))
    changed |= process(sext, env, store, f, module);
  else if(const auto* zext = dyn_cast<ZExtInst>(inst))
    changed |= process(zext, env, store, f, module);
  else if(const auto* trunc = dyn_cast<TruncInst>(inst))
    changed |= process(trunc, env, store, f, module);
  else if(const auto* trunc = dyn_cast<FPTruncInst>(inst))
    changed |= process(trunc, env, store, f, module);
  else if(const auto* sitofp = dyn_cast<SIToFPInst>(inst))
    changed |= process(sitofp, env, store, f, module);
  else if(const auto* fptosi = dyn_cast<FPToSIInst>(inst))
    changed |= process(fptosi, env, store, f, module);
  else if(const auto* uitofp = dyn_cast<UIToFPInst>(inst))
    changed |= process(uitofp, env, store, f, module);
  else if(const auto* fptoui = dyn_cast<FPToUIInst>(inst))
    changed |= process(fptoui, env, store, f, module);
  else if(const auto* load = dyn_cast<LoadInst>(inst))
    changed |= process(load, env, store, f, module);
  else if(const auto* gep = dyn_cast<GetElementPtrInst>(inst))
    changed |= process(gep, env, store, f, module);
  else if(const auto* phi = dyn_cast<PHINode>(inst))
    changed |= process(phi, env, store, f, module);
  else if(const auto* select = dyn_cast<SelectInst>(inst))
    changed |= process(select, env, store, f, module);
  else if(const auto* st = dyn_cast<StoreInst>(inst))
    changed |= process(st, env, store, f, module);
  else if(const auto* br = dyn_cast<BranchInst>(inst))
    changed |= process(br, env, store, f, module);
  else if(const auto* call = dyn_cast<CallInst>(inst))
    changed |= process(call, env, store, f, module);
  else if(const auto* invoke = dyn_cast<InvokeInst>(inst))
    changed |= process(invoke, env, store, f, module);
  else if(const auto* cmp = dyn_cast<CmpInst>(inst))
    changed |= process(cmp, env, store, f, module);
  else if(const auto* bo = dyn_cast<BinaryOperator>(inst))
    changed |= process(bo, env, store, f, module);
  else if(const auto* swich = dyn_cast<SwitchInst>(inst))
    changed |= process(swich, env, store, f, module);
  else if(const auto* insval = dyn_cast<InsertValueInst>(inst))
    changed |= process(insval, env, store, f, module);
  else if(const auto* extval = dyn_cast<ExtractValueInst>(inst))
    changed |= process(extval, env, store, f, module);
  else if(const auto* retrn = dyn_cast<ReturnInst>(inst))
    changed |= process(retrn, env, store, f, module);
  else if(const auto* unreachable = dyn_cast<UnreachableInst>(inst))
    changed |= process(unreachable, env, store, f, module);
  else if(const auto* resume = dyn_cast<ResumeInst>(inst))
    changed |= process(resume, env, store, f, module);
  else if(const auto* landing = dyn_cast<LandingPadInst>(inst))
    changed |= process(landing, env, store, f, module);
  else if(const auto* fence = dyn_cast<FenceInst>(inst))
    changed |= process(fence, env, store, f, module);
  else if(const auto* cmpxchg = dyn_cast<AtomicCmpXchgInst>(inst))
    changed |= process(cmpxchg, env, store, f, module);
  else if(const auto* rmw = dyn_cast<AtomicRMWInst>(inst))
    changed |= process(rmw, env, store, f, module);
  else if(const auto* br = dyn_cast<IndirectBrInst>(inst))
    changed |= process(br, env, store, f, module);
  else if(const auto* shuffle = dyn_cast<ShuffleVectorInst>(inst))
    changed |= process(shuffle, env, store, f, module);
  else if(const auto* va = dyn_cast<VAArgInst>(inst))
    changed |= process(va, env, store, f, module);
  //
  // TODO: These instructions are new in LLVM 3.7 and should be made available
  // once LLVM 3.7 is supported
  //
  // else if(CleanupPadInst *cleanup = dyn_cast<CleanupPadInst>(inst))
  //   return process(cleanup, env, store, f);
  // else if(CatchPadInst *catchpad = dyn_cast<CatchPadInst>(inst))
  //   return process(catchpad, env, store, f);
  // else if(CatchEndPadInst *catchendpad = dyn_cast<CatchEndPadInst>(inst))
  //   return process(catchendpad, env, store, f);
  // else if(CleanupEndPadInst *cleanup = dyn_cast<CleanupEndPadInst>(inst))
  //   return process(cleanup, env, store, f);
  // else if(CleanupRetInst *ret = dyn_cast<CleanupRetInst>(inst))
  //   return process(ret, env, store, f);
  // else if(TerminatePadInst *terminate = dyn_cast<TerminatePadInst>(inst))
  //   return process(terminate, env, store, f);
  else
    moya_error("Unknown instruction: " << *inst);

  maybePrintInstructionResult(inst, env, store, f, module);

  return changed;
}

AnalysisStatus Mutability::process(const ConstantInt* cint,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(cint, store.make(cint));

  return changed;
}

AnalysisStatus Mutability::process(const ConstantFP* cfp,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(cfp, store.make(cfp));

  return changed;
}

AnalysisStatus Mutability::process(const ConstantPointerNull* cnull,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(cnull, store.make(cnull));

  return changed;
}

AnalysisStatus Mutability::process(const ConstantExpr* cexpr,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not cexprInsts.contains(cexpr))
    moya_error("not found: " << *cexpr);
  const Instruction* inst = cexprInsts.at(cexpr);
  changed |= process(inst, env, store, f, module);
  changed |= env.add(cexpr, env.lookup(inst));

  return changed;
}

AnalysisStatus Mutability::process(const ConstantDataArray* cda,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(unsigned i = 0; i < cda->getNumElements(); i++)
    changed |= process(cda->getElementAsConstant(i), env, store, f, module);

  // We can't really do anything useful with arrays of primitives, so we
  // just truncate the entire thing
  changed |= env.add(cda, store.make({{store.getRuntimeScalar()}}));

  return changed;
}

AnalysisStatus Mutability::process(const ConstantArray* carray,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // We cannot just use the make method to create Composite content object
  // because the array could contain constexprs which have to be processed
  ArrayType* aty = LLVMUtils::getFlattened(carray->getType());
  Type* ety = aty->getElementType();
  uint64_t nelems = AerynConfig::getArrayLength(aty);
  if(LLVMUtils::isScalarTy(ety)) {
    changed |= env.add(carray, store.make({{store.getRuntimeScalar()}}));
  } else {
    Vector<Contents> elems;
    for(unsigned i = 0; i < nelems; i++) {
      const Value* element = carray->getOperand(i);
      changed |= process(element, env, store, f, module);
      elems.push_back(env.lookup(element));
    }
    changed |= env.add(carray, store.make(elems));
  }

  return changed;
}

AnalysisStatus Mutability::process(const ConstantStruct* cstruct,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // We cannot just use the make method to create Composite content object
  // because the array could contain constexprs which have to be processed
  Vector<Contents> elems;
  for(unsigned i = 0; i < cstruct->getType()->getNumElements(); i++) {
    const Value* op = cstruct->getOperand(i);
    changed |= process(op, env, store, f, module);
    elems.push_back(env.lookup(op));
  }
  changed |= env.add(cstruct, store.make(elems));

  return changed;
}

AnalysisStatus Mutability::process(const ConstantAggregateZero* czero,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // This is not a "real" constant in the sense that it can't be read
  // It is only ever used to initialize structs and arrays so it is handled
  // correctly in the initialization code

  return changed;
}

AnalysisStatus Mutability::process(const UndefValue* undef,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(undef, store.make(undef));

  return changed;
}

AnalysisStatus Mutability::process(const GlobalAlias* g,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= env.add(g, env.lookup(g->getAliasee()));

  return changed;
}

AnalysisStatus Mutability::process(const InlineAsm* asmcode,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  return changed;
}

AnalysisStatus Mutability::process(const Value* v,
                                   Environment& env,
                                   Store& store,
                                   const Function* f,
                                   const Module& module) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  // Function arguments will have been added in process(Function*) but they
  // will not have been marked as read there
  if(isa<Instruction>(v)) {
    ;
  } else if(const auto* undef = dyn_cast<UndefValue>(v)) {
    changed |= process(undef, env, store, f, module);
  } else if(const auto* czero = dyn_cast<ConstantAggregateZero>(v)) {
    changed |= process(czero, env, store, f, module);
  } else if(const auto* cptr = dyn_cast<ConstantPointerNull>(v)) {
    changed |= process(cptr, env, store, f, module);
  } else if(const auto* cexpr = dyn_cast<ConstantExpr>(v)) {
    changed |= process(cexpr, env, store, f, module);
  } else if(const auto* cint = dyn_cast<ConstantInt>(v)) {
    changed |= process(cint, env, store, f, module);
  } else if(const auto* cfp = dyn_cast<ConstantFP>(v)) {
    changed |= process(cfp, env, store, f, module);
  } else if(const auto* cstruct = dyn_cast<ConstantStruct>(v)) {
    changed |= process(cstruct, env, store, f, module);
  } else if(const auto* cvec = dyn_cast<ConstantVector>(v)) {
    changed |= process(cvec, env, store, f, module);
  } else if(const auto* cda = dyn_cast<ConstantDataArray>(v)) {
    changed |= process(cda, env, store, f, module);
  } else if(const auto* carray = dyn_cast<ConstantArray>(v)) {
    changed |= process(carray, env, store, f, module);
  } else if(const auto* alias = dyn_cast<GlobalAlias>(v)) {
    changed |= process(alias, env, store, f, module);
  } else if(const auto* inlineAsm = dyn_cast<InlineAsm>(v)) {
    changed |= process(inlineAsm, env, store, f, module);
  } else if(isa<Argument>(v) or isa<GlobalVariable>(v) or isa<Function>(v)) {
    // Don't need to do anything with these because they will have been
    // either preprocessed before the analysis began or by other instructions
  } else if(isa<MetadataAsValue>(v)) {
    // Don't need to do anything with fields used in debug instrinsics
  } else if(isa<BasicBlock>(v)) {
    // Obviously don't do anything with basic blocks
  } else {
    moya_error("Unknown value: " << *v);
  }
  return changed;
}

AnalysisStatus
Mutability::process(const Function* f, Environment& env, Store& store) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // See comments in getEnvDebugStore() for details about the first parameter
  // to this function
  moya_message("Mutability::process: " << f->getName());
  maybePrintStore(moya::Config::DebugFnBefore, f, env, store);
  maybePrintFunction(moya::Config::DebugFnBefore, f, env, store);

  // Don't bother analyzing this function unless it is actually called
  // somewhere. This will be an additional optimization since the dead-code
  // cannot be cleanly eliminated in C++
  if(env.hasCallSites(f)) {
    // The functions which should not be analyzed will already have been
    // filtered out when generating the build order
    for(auto i = inst_begin(f); i != inst_end(f); i++) {
      AnalysisStatus tmp
          = process(&*i, env, store, f, *LLVMUtils::getModule(f));
      // Short-circuit if we hit top
      if(moya::isTop(tmp)) {
        if(not moya::Metadata::hasMemSafe(f))
          return setTop(&*i);
        else
          tmp = AnalysisStatus::Unchanged;
      }
      changed |= tmp;
    }
  }

  maybePrintStore(moya::Config::DebugFnAfter, f, env, store);
  maybePrintFunction(moya::Config::DebugFnAfter, f, env, store);

  return changed;
}

void Mutability::collectConstantExprs(Value* v, Set<ConstantExpr*>& cexprs) {
  if(auto* cexpr = dyn_cast<ConstantExpr>(v)) {
    if(cexprs.insert(cexpr))
      for(unsigned i = 0; i < cexpr->getNumOperands(); i++)
        collectConstantExprs(cexpr->getOperand(i), cexprs);
  } else if(auto* caggr = dyn_cast<ConstantAggregate>(v)) {
    for(unsigned i = 0; i < caggr->getNumOperands(); i++)
      collectConstantExprs(caggr->getOperand(i), cexprs);
  } else if(auto* g = dyn_cast<GlobalVariable>(v)) {
    if(g->hasInitializer())
      collectConstantExprs(g->getInitializer(), cexprs);
  }
}

void Mutability::initialize() {
  moya_message("Mutability::initialize");

  // Collect all the ConstantExpr's in the IR. These may be in the initializers
  // of global variables or in instruction operands
  Set<ConstantExpr*> cexprs;

  for(StructType* sty : module.getIdentifiedStructTypes())
    if(moya::Metadata::hasSourceLocation(sty, module))
      env.addFile(moya::Metadata::getSourceLocation(sty, module).getFile());

  moya_message("Mutability::initialize regions");
  for(const moya::Region& region : moya::Metadata::getRegions(module))
    env.addFile(region.getLocation().getFile());

  moya_message("Mutability::initialize globals");
  for(GlobalVariable& g : module.globals()) {
    if(g.hasInitializer())
      collectConstantExprs(g.getInitializer(), cexprs);
    if(moya::Metadata::hasSourceLocation(g))
      env.addFile(moya::Metadata::getSourceLocation(g).getFile());
  }

  moya_message("Mutability::initialize functions");
  for(Function& f : module.functions()) {
    if(f.size()) {
      if(moya::Metadata::hasSourceLang(f)) {
        for(auto i = inst_begin(f); i != inst_end(f); i++)
          for(Value* op : (&*i)->operands())
            collectConstantExprs(op, cexprs);

        // Add the file containing the function
        if(moya::Metadata::hasSourceLocation(f))
          env.addFile(moya::Metadata::getSourceLocation(f).getFile());
        else if(llvm::DISubprogram* di = f.getSubprogram())
          env.addFile(di->getFilename());
      }
    }
  }

  for(Function& f : module.functions())
    if(moya::Metadata::hasGlobalConstructor(f)
       or moya::Metadata::hasGlobalDestructor(f) or moya::Metadata::hasMain(f))
      env.addCallsite(&f, nullptr);

  for(ConstantExpr* cexpr : cexprs) {
    Instruction* inst = cexpr->getAsInstruction();
    insts.push_back(inst);
    cexprInsts[cexpr] = inst;
    instCExprs[inst] = cexpr;
  }
}

bool Mutability::runOnModule() {
  moya_message("Moya Mutability Info");

  initialize();

  // If the module has a main function, then this module corresponds to an
  // executable. In that case, it has a single entry point which is main().
  // If not, then we are creating a shared library in which case each externally
  // visible routine could be an entry point
  if(not LLVMUtils::getMainFunction(module)) {
    moya_error("Unimplemented: Aeryn for libraries");
    return false;
  }

  // Setup functions and their private stacks
  moya_message("Mutability::setup functions");
  for(const Function& f : module.functions()) {
    if(not moya::Metadata::hasLLVMDebug(f)) {
      const Store::Address* faddr = store.allocate(
          f.getFunctionType(),
          CellFlags().set(CellFlags::Code).set(CellFlags::Implicit),
          false);
      for(const Argument& arg : f.args()) {
        Type* argTy = LLVMUtils::getAnalysisType(arg);

        // Function arguments should only be primitive types or 0-length arrays
        // The debug functions might have metadata arguments
        if(not(LLVMUtils::isScalarTy(argTy) or argTy->isPointerTy()
               or LLVMUtils::is0ArrayTy(argTy)))
          moya_error("Unsupported function argument: " << *argTy);

        env.addArg(
            &arg,
            store.allocate(
                argTy,
                CellFlags().set(CellFlags::Argument).set(CellFlags::Implicit),
                false));

        // The fake arg is used to lookup the status but the status is usually
        // not set correctly because it is never actually read (it is fake after
        // all). So we add a reader so that the Talyn optimizer actually bothers
        // to look at the argument
        store.addReader(env.lookupArg(&arg), &f);
      }
      env.addFunction(&f, faddr);
      store.write(faddr, store.make(&f), f.getFunctionType());
    }
  }

  // Setup global variables
  moya_message("Mutability::setup globals");
  for(const GlobalVariable& g : module.globals()) {
    if(not moya::Metadata::hasNoAnalyze(g)) {
      CellFlags flags(CellFlags::Global);
      flags.set(CellFlags::Implicit);
      if(g.isConstant())
        flags.set(CellFlags::Constant);
      bool truncate = true;
      if(moya::Metadata::hasCXXVtable(g) or moya::Metadata::hasCXXVTT(g)) {
        flags.set(CellFlags::Vtable);
        truncate = false;
      }

      auto* gelem
          = LLVMUtils::getAnalysisTypeAs<PointerType>(g)->getElementType();
      if(truncate and gelem->isPointerTy())
        truncate = false;

      // Since these are created at load-time, we treat the allocation and
      // deallocation as having taken place outside of any function
      const Store::Address* addr = store.allocate(gelem, flags, truncate);
      store.deallocate(gelem, addr, nullptr, nullptr);
      env.addGlobal(&g, addr);
    }
  }

  for(GlobalVariable& g : module.globals()) {
    if(moya::Metadata::hasNoAnalyze(g))
      continue;

    auto* gelem
        = LLVMUtils::getAnalysisTypeAs<PointerType>(g)->getElementType();
    const Store::Address* addr = env.lookupGlobal(&g);

    // If this is an externally defined global, it is likely provided by
    // a dynamically linked library. We can't really do much about complex
    // objects or pointers, but we can initialize scalar globals
    if(not g.hasInitializer()) {
      if(g.hasExternalLinkage() and g.hasDefaultVisibility())
        if(LLVMUtils::isScalarTy(gelem))
          store.write(addr, store.getRuntimeScalar(), gelem);
      continue;
    }

    Constant* init = g.getInitializer();
    process(init, env, store, nullptr, module);
    if(isa<ConstantInt>(init) or isa<ConstantFP>(init)
       or isa<ConstantPointerNull>(init)) {
      for(const Content* c : env.lookup(init))
        store.write(addr, c, init->getType());
    } else if(isa<ConstantAggregateZero>(init)) {
      initializeZero(gelem, addr, store);
    } else if(auto* carray = dyn_cast<ConstantArray>(init)) {
      Type* ety = LLVMUtils::getFlattenedElementType(carray->getType());
      uint64_t step = store.getTypeAllocSize(ety);
      for(const Content* c : env.lookup(init)) {
        if(const auto* ccomp = dyn_cast<ContentComposite>(c))
          for(uint64_t i = 0; i < ccomp->size(); i++)
            store.write(store.make(addr, i * step), ccomp->at(i), ety);
        else
          moya_error("Expected composite content for: " << *carray << "\n"
                                                        << "Got " << c->str());
      }
    } else if(auto* cda = dyn_cast<ConstantDataArray>(init)) {
      Type* ety = cda->getType()->getElementType();
      uint64_t step = store.getTypeAllocSize(ety);
      for(const Content* c : env.lookup(init))
        if(const auto* ccomp = dyn_cast<ContentComposite>(c))
          for(unsigned i = 0; i < ccomp->size(); i++)
            store.write(store.make(addr, i * step), ccomp->at(i), ety);
        else
          moya_error("Expected composite content for: " << *cda << "\n"
                                                        << "Got " << c->str());
    } else if(auto* cstruct = dyn_cast<ConstantStruct>(init)) {
      StructType* sty = cstruct->getType();
      const moya::StructLayout& sl = store.getStructLayout(sty);
      for(const Content* c : env.lookup(init))
        if(const auto* ccomp = dyn_cast<ContentComposite>(c))
          for(unsigned i = 0; i < sty->getNumElements(); i++)
            store.write(store.make(addr, sl.getElementOffset(i)),
                        ccomp->at(i),
                        sty->getElementType(i));
        else
          moya_error("Expected composite content for: " << *cstruct << "\n"
                                                        << "Got " << c->str());
    } else if(auto* cexpr = dyn_cast<ConstantExpr>(init)) {
      store.write(addr, env.lookup(cexpr), cexpr->getType());
    } else if(isa<UndefValue>(init)) {
      // Can't really initialize anything with an undef value
    } else {
      moya_error("Unsupported global initializer\n"
                 << "global: " << g << "\n"
                 << "  init: " << *init);
    }
  }

  moya_message("Mutability::setup main");
  if(const Function* main = LLVMUtils::getMainFunction(module)) {
    if(const Argument* argc = LLVMUtils::getArgument(main, 0)) {
      env.add(argc, store.getRuntimeScalar());
      store.write(
          env.lookupArg(argc), store.getRuntimeScalar(), argc->getType());
    }

    if(const Argument* argv = LLVMUtils::getArgument(main, 1)) {
      AnalysisStatus changed = AnalysisStatus::Unchanged;
      LLVMContext& context = module.getContext();
      Type* i8 = Type::getInt8Ty(context);
      Type* pi8 = Type::getInt8PtrTy(context);

      // Allocate arrays representing command line arguments
      CellFlags flags(CellFlags::Implicit);
      const Store::Address* str = store.allocate(i8, flags, false);
      const Store::Address* argv0 = store.allocate(pi8, flags, false);
      store.write(str, store.getRuntimeScalar(), i8, nullptr, changed);
      store.write(argv0, str, pi8, nullptr, changed);

      env.add(argv, argv0);
      store.write(env.lookupArg(argv), argv0, pi8);
    }
  }

  moya_message("Mutability::iterate");
  const Vector<const Function*> order
      = generateProcessingOrder(LLVMUtils::getMainFunction(module));
  for(iteration = 1; iteration <= AerynConfig::getMaxIterations();
      iteration++) {
    AnalysisStatus changed = AnalysisStatus::Unchanged;
    for(const Function* f : order) {
      changed |= process(f, env, store);
      // Short-circuit if top
      if(moya::isTop(changed)) {
        this->top = true;
        break;
      }
    }

    moya_message("Done iteration: " << iteration);
    if(not changed)
      break;
    if(moya::isTop(changed)) {
      outs() << "\n\n!!! TOP !!!\n\n";
      break;
    }
    moya_error_if(iteration == AerynConfig::getMaxIterations(),
                  "Max iterations without convergence");
  }

  moya_message("Done!");

  for(const Function& f : module.functions()) {
    const std::string& fname = f.getName().str();
    if(f.size() and debugFns.contains(fname)) {
      printFunction(moya::Config::DebugFnBefore, &f, env, store);
      for(auto i = inst_begin(f); i != inst_end(f); i++) {
        printInstructionOperands(&*i, env, store, &f, module);
        printInstructionResult(&*i, env, store, &f, module);
      }
      printFunction(moya::Config::DebugFnAfter, &f, env, store);
    }
  }

  // FIXME: Will this cause problems when the environment is serialized?
  // We need to explicitly delete the instructions generated from
  // ConstantExprs otherwise we get broken module errors
  for(Instruction* inst : insts)
    inst->deleteValue();

  return false;
}

Mutability::Mutability(Module& module, const ClassHeirarchies& heirarchies)
    : module(module), env(module, heirarchies), store(module, env),
      models(module), top(false), debugFns(moya::Config::getEnvDebugFns()),
      debugFnsImmediate(moya::Config::getEnvDebugFnsImmediate()) {
  debugStore = moya::Config::getEnvDebugStore();
}

Mutability::~Mutability() {
  ;
}

const Environment& Mutability::getEnvironment() const {
  return env;
}

const Store& Mutability::getStore() const {
  return store;
}

bool Mutability::isTop() const {
  return top;
}

unsigned Mutability::getIterations() const {
  return iteration;
}

} // namespace moya
