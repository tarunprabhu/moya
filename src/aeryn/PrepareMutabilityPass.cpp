#include "Passes.h"
#include "common/AerynConfig.h"
#include "common/DemanglerCXX.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/StringUtils.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Instructions.h>
#include <llvm/Pass.h>

using namespace llvm;

// This pass runs adds some last-minute instrument before running the
// mutability analysis. Some of this is a repeat of some of the instrumentation
// passes that are run on modules coming out of the compiler but considerably
// simpler because most of it will have been done already.
class PrepareMutabilityPass : public ModulePass {
public:
  static char ID;

protected:
  template <typename T>
  bool instrument(T*);
  bool isLLVMDebugFunction(const Function&);

public:
  PrepareMutabilityPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

PrepareMutabilityPass::PrepareMutabilityPass() : ModulePass(ID) {
  // initializePrepareMutabilityPassPass(*PassRegistry::getPassRegistry());
}

StringRef PrepareMutabilityPass::getPassName() const {
  return "Prepare Mutability Info";
}

void PrepareMutabilityPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool PrepareMutabilityPass::isLLVMDebugFunction(const Function& f) {
  return moya::StringUtils::startswith(f.getName(), "llvm.dbg");
}

template <typename T>
bool PrepareMutabilityPass::instrument(T* call) {
  bool changed = false;

  if(Function* f = moya::LLVMUtils::getCalledFunction(call))
    if(moya::Metadata::hasLLVMDebug(f))
      changed |= moya::Metadata::setIgnore(call);

  return changed;
}

bool PrepareMutabilityPass::runOnModule(Module& module) {
  bool changed = false;

  // Instrument functions
  moya::DemanglerCXX demangler;
  for(Function& f : module.functions()) {
    if(not f.size()) {
      if(isLLVMDebugFunction(f)) {
        changed |= moya::Metadata::setLLVMDebug(f);
        changed |= moya::Metadata::setNoAnalyze(f);
      } else if(not moya::Metadata::hasModel(f)) {
        if(moya::LLVMUtils::isPureFunction(f))
          changed |= moya::Metadata::setModel(
              f, moya::AerynConfig::getPureFunctionModel());
        else
          // It's alright to use the demangler here because the C++ demangler
          // will subsume other demanglers. Any functions that are demangled
          // here will have been missed by the frontend processors. That is
          // likely a result of the compiler generating code usually the
          // result of template expansions which happen only in C++. The
          // functions from other languages which for some reason don't have
          // models will not be demangled, exactly the behavior we want
          changed |= moya::Metadata::setModel(
              f, demangler.demangle(f, moya::Demangler::Action::Model));
      }
    }
  }

  // Instrument instructions
  for(Function& f : module.functions())
    for(auto i = inst_begin(f); i != inst_end(f); i++)
      if(auto* call = dyn_cast<CallInst>(&*i))
        changed |= instrument(call);
      else if(auto* invoke = dyn_cast<InvokeInst>(&*i))
        changed |= instrument(invoke);

  return changed;
}

char PrepareMutabilityPass::ID = 0;

static const char* name = "moya-prepare-mutability";
static const char* descr
    = "Adds instrumentation in just before running the mutability analysis";
static const bool cfg = true;
static const bool analysis = false;

// INITIALIZE_PASS(PrepareMutabilityPass, name, descr, cfg, analysis)

static RegisterPass<PrepareMutabilityPass> X(name, descr, cfg, analysis);

Pass* createPrepareMutabilityPass() {
  return new PrepareMutabilityPass();
}
