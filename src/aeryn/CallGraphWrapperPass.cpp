#include "CallGraphWrapperPass.h"
#include "AnalysisCacheManager.h"
#include "CacheManagerWrapperPass.h"
#include "Mutability.h"
#include "MutabilityWrapperPass.h"
#include "Passes.h"
#include "common/CallGraph.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/Environment.h"
#include "common/Metadata.h"
#include "common/Store.h"

using namespace llvm;
using namespace moya;

CallGraphWrapperPass::CallGraphWrapperPass() : ModulePass(ID) {
  // initializeCallGraphWrapperPassPass(*PassRegistry::getPassRegistry());
}

StringRef CallGraphWrapperPass::getPassName() const {
  return "Moya Callgraph Wrapper";
}

void CallGraphWrapperPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesAll();
  AU.addRequired<MutabilityWrapperPass>();
  AU.addRequired<CacheManagerWrapperPass>();
}

bool CallGraphWrapperPass::hasCallGraph(RegionID region) const {
  return cgs.contains(region);
}

const moya::CallGraph&
CallGraphWrapperPass::getCallGraph(RegionID region) const {
  return *cgs.at(region).get();
}

bool CallGraphWrapperPass::runOnModule(Module& module) {
  moya_message(getPassName());

  AnalysisCacheManager& cacheMgr
      = getAnalysis<CacheManagerWrapperPass>().getCacheManager();
  const Mutability& mutability
      = getAnalysis<MutabilityWrapperPass>().getMutability();

  const Environment& env = mutability.getEnvironment();
  const Store& store = mutability.getStore();

  for(const Region& region : moya::Metadata::getRegions(module)) {
    CallGraph* cg = new CallGraph(region, module);

    // We can't always construct a callgraph for a region because we might
    // have a region marked in a function which never gets used
    if(cg->canConstruct()) {
      if(not mutability.isTop())
        cg->construct(env, store);

      if(cacheMgr.isEnabled()) {
        cg->dot("callgraph", cacheMgr.getCallGraphDotifier(region.getID()));
        cg->dot("closure", cacheMgr.getClosureDotifier(region.getID()));
      }
    }
    cgs[region.getID()].reset(cg);
  }

  CallGraph* cg = new CallGraph(module);
  if(not mutability.isTop())
    cg->construct(env, store);
  if(cacheMgr.isEnabled())
    cg->serialize(cacheMgr.getCallGraphSerializer());
  cgs[Config::getInvalidRegionID()].reset(cg);

  return false;
}

char CallGraphWrapperPass::ID = 0;

static const char* name = "moya-callgraph";
static const char* descr
    = "Generates the callgraph augmented with the mutability analysis";
static const bool cfg = true;
static const bool analysis = true;

// INITIALIZE_PASS_BEGIN(CallGraphWrapperPass, name, descr, cfg, analysis)
// INITIALIZE_PASS_DEPENDENCY(CacheManagerWrapperPass)
// INITIALIZE_PASS_DEPENDENCY(MutabilityWrapperPass)
// INITIALIZE_PASS_END(CallGraphWrapperPass, name, descr, cfg, analysis)

static RegisterPass<CallGraphWrapperPass> X(name, descr, cfg, analysis);

Pass* createCallGraphWrapperPass() {
  return new CallGraphWrapperPass();
}
