#include "DemanglerStdCpp.h"
#include "common/StringUtils.h"

#include <cctype>

using namespace llvm;

static std::string cleanup(const std::string& s) {
  // Because of duplicate functions that are sometimes generated in the bitcode,
  // the name cannot be demangled. To fix the problem, we strip the numbers
  // that LLVM adds to the end of the name which
  std::stringstream ss;
  unsigned end = s.size() - 1;
  for(end = s.size() - 1; isdigit(s[end]); end--)
    ;
  for(unsigned i = 0; i <= end; i++)
    ss << s[i];
  return ss.str();
}

// We have to parse the args carefully because when the argument is of function
// pointer type, then it can have commas inside it. We distinguish function
// pointer types from other types by the fact that they have a parenthesis in
// then which other types should not have
static moya::Vector<std::string> parseArgs(const std::string& s) {
  int depth = 0;
  moya::Vector<std::string> ret;
  std::stringstream ss;
  for(char c : s) {
    if(c == '(') {
      depth += 1;
      ss << c;
    } else if(c == ')') {
      depth -= 1;
      ss << c;
    } else if(c == ',') {
      if(depth == 0) {
        ret.push_back(moya::StringUtils::strip(ss.str()));
        ss.str("");
      } else {
        ss << c;
      }
    } else {
      ss << c;
    }
  }
  ret.push_back(moya::StringUtils::strip(ss.str()));
  return ret;
}

static unsigned long isMatch(const moya::Vector<StringRef>& s1,
                             const moya::Vector<std::string>& s2) {
  int wildcards = 0;
  if(s1.size() != s2.size())
    return std::numeric_limits<unsigned long>::max();
  for(unsigned i = 0; i < s1.size(); i++)
    if(s1.at(i) == "*")
      wildcards += 1;
    else if(s1.at(i) == s2.at(i))
      ;
    else
      return std::numeric_limits<unsigned long>::max();
  return wildcards;
}

DemanglerStdCpp::DemanglerStdCpp() {
  ;
}

DemanglerStdCpp::DemanglerStdCpp(DemanglerStdCpp::Specs wcs) : wcs(wcs) {
  ;
}

void DemanglerStdCpp::addSpecs(const DemanglerStdCpp::Specs& specs) {
  wcs.insert(specs.begin(), specs.end());
}

moya::Vector<StringRef>
DemanglerStdCpp::getMatchingSpec(const std::string& f,
                                 const moya::Vector<std::string>& args) const {
  // Match the function with the least number of wildcards
  moya::Vector<StringRef> ret;
  auto ranges = wcs.equal_range(f);
  unsigned long wildcards = args.size();
  for(auto it = ranges.first; it != ranges.second; it++) {
    unsigned long m = isMatch(it->second, args);
    if(m != std::numeric_limits<unsigned long>::max() and m <= wildcards) {
      wildcards = m;
      ret = it->second;
    }
  }
  return ret;
}

std::string DemanglerStdCpp::getModelName(std::string name) const {
  std::stringstream ss;
  std::string model = DemanglerCXX::getModelName(cleanup(name));
  size_t pos = model.find("(");
  // This will include the first left paren
  std::string f = model.substr(0, pos);
  ss << f << "(";
  if(wcs.contains(StringRef(f))) {
    // This should get rid of the last right paren
    std::string t = model.substr(pos + 1, model.size() - f.size() - 2);
    moya::Vector<std::string> args = ::parseArgs(moya::StringUtils::strip(t));
    moya::Vector<StringRef> spec = getMatchingSpec(f, args);
    if(spec.size()) {
      ss << spec.at(0).str();
      for(unsigned i = 1; i < spec.size(); i++)
        ss << ", " << spec.at(i).str();
      ss << ")";
      return ss.str();
    }
  }
  return model;
}
