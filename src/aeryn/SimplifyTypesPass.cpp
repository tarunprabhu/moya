#include "Passes.h"
#include "common/Config.h"
#include "common/HashMap.h"
#include "common/LLVMUtils.h"
#include "common/Vector.h"

#include <llvm/IR/DebugInfo.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/TypeFinder.h>
#include <llvm/IR/Verifier.h>
#include <llvm/Pass.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/Utils/Cloning.h>

using namespace llvm;

// This pass replaces the hundreds of array descriptor types that DragonEgg
// generates with a single descriptor type.
class SimplifyTypesPass : public ModulePass {
public:
  static char ID;

private:
  moya::HashMap<Type*, StructType*> replace;
  moya::Vector<StructType*> descs;

private:
  Type* genReplType(Type*, Type*);
  Type* replaceType(Type*);
  Type* getReplaceType(Type*);
  bool shouldReplace(StructType*);
  bool mutate(Value*);

public:
  SimplifyTypesPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

static std::string getMoyaDescriptorNamePrefix() {
  return "struct.moya.descriptor.";
}

static bool isMoyaDescriptor(Type* type) {
  if(not isDescriptorType(type, true))
    return false;
  StructType* stype = dyn_cast<StructType>(type);
  return stype->getName().find(getMoyaDescriptorNamePrefix()) == 0;
}

static std::string getMoyaDescriptorName(unsigned rank) {
  std::stringstream ss;
  ss << getMoyaDescriptorNamePrefix() << rank;
  return ss.str();
}

SimplifyTypesPass::SimplifyTypesPass()
    : ModulePass(ID), descs(moya::Config::getMaxDescriptorDimensions() + 1) {
  ;
}

StringRef SimplifyTypesPass::getPassName() const {
  return "Moya Simplify Types";
}

void SimplifyTypesPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool SimplifyTypesPass::mutate(Value* v) {
  Type* repl = getReplaceType(v->getType());
  if(repl) {
    if(isa<Function>(v))
      llvm::errs() << v->getName() << ": " << *repl << " => " << *v->getType()
                   << "\n";
    v->mutateType(repl);
  }
  return repl;
}

Type* SimplifyTypesPass::getReplaceType(Type* ty) {
  Type* repl = replaceType(ty);
  if(repl != ty)
    return repl;
  return nullptr;
}

Type* SimplifyTypesPass::genReplType(Type* ty, Type* replTy) {
  if(ArrayType* arrTy = dyn_cast<ArrayType>(ty))
    return ArrayType::get(genReplType(arrTy->getElementType(), replTy),
                          arrTy->getNumElements());
  else if(PointerType* ptrTy = dyn_cast<PointerType>(ty))
    return genReplType(ptrTy->getElementType(), replTy)->getPointerTo();
  return replTy;
}

bool SimplifyTypesPass::shouldReplace(StructType* sty) {
  if(not sty->hasName())
    return true;
  if(isDescriptorType(sty, true) and not isMoyaDescriptor(sty))
    return true;
  for(unsigned i = 0; i < sty->getNumElements(); i++)
    if(replaceType(sty->getElementType(i)) != sty->getElementType(i))
      return true;
  return false;
}

Type* SimplifyTypesPass::replaceType(Type* ty) {
  // This finds the innermost type in a potentially pointer/array type
  // If the innermost type is a struct, and it is in the replace list,
  // then we simple replace it. If it isn't, getArrayDescriptorDims checks
  // if it is an array descriptor and returns the appropriate number of
  // dimensions if it is (0 otherwise). If neither is true, then we just
  // return the old type
  if(Type* baseTy = getInnermost(ty)) {
    auto it = replace.find(baseTy);
    if(isDescriptorType(baseTy, true)) {
      if(not isMoyaDescriptor(baseTy))
        return genReplType(ty, descs[getDescriptorRank(baseTy, true)]);
    } else if(it != replace.end()) {
      return genReplType(ty, it->second);
    } else if(FunctionType* fty = dyn_cast<FunctionType>(baseTy)) {
      Type* newretty = replaceType(fty->getReturnType());
      moya::Vector<Type*> tys;
      for(unsigned j = 0; j < fty->getNumParams(); j++)
        tys.push_back(replaceType(fty->getParamType(j)));
      return genReplType(ty, FunctionType::get(newretty, tys, fty->isVarArg()));
    }
  }
  return ty;
}

bool SimplifyTypesPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;
  LLVMContext& context = module.getContext();
  Type* i64 = Type::getInt64Ty(context);

  moya::Set<StructType*> structTypes;
  TypeFinder finder;
  finder.run(module, false);
  for(TypeFinder::iterator it = finder.begin(); it != finder.end(); it++)
    structTypes.insert(*it);

  moya::Vector<Type*> sdimstys = {i64, i64, i64};
  StructType* sdims = module.getTypeByName("struct.descriptor_dimension");

  if(sdims) {
    for(unsigned i = 0; i <= moya::Config::getMaxDescriptorDimensions(); i++) {
      std::string descr = getMoyaDescriptorName(i);
      moya::Vector<Type*> stys = {Type::getInt8PtrTy(context), i64, i64};
      if(i > 0)
        stys.push_back(ArrayType::get(sdims, i));
      descs[i] = StructType::create(context, stys, descr);
    }
  }

  unsigned long oldsize;
  do {
    oldsize = replace.size();
    for(StructType* sty : structTypes) {
      if(shouldReplace(sty) and not contains(replace, cast<Type>(sty))) {
        std::string sname = "";
        if(sty->hasName()) {
          sname = sty->getName();
          sty->setName(sname + ".old");
        } else {
          sname = getAnonymousStructName(sty);
        }

        llvm::errs() << sname << "\n";
        replace[sty] = StructType::create(context, sname);
      }
    }
  } while(oldsize != replace.size());

  for(auto it : replace) {
    StructType* sty = cast<StructType>(it.first);
    moya::Vector<Type*> stys;
    for(Type* ty : sty->elements())
      stys.push_back(replaceType(ty));
    replace[sty]->setBody(stys);
  }

  for(GlobalVariable& g : module.globals()) {
    changed |= mutate(&g);
    if(g.hasInitializer()) {
      Constant* init = g.getInitializer();
      if(ConstantExpr* cexpr = dyn_cast<ConstantExpr>(init))
        for(unsigned i = 0; i < cexpr->getNumOperands(); i++)
          changed |= mutate(cexpr->getOperand(i));
      else
        changed |= mutate(init);
    }
  }

  for(GlobalAlias& g : module.aliases()) {
    changed |= mutate(&g);
  }

  for(Function& f : module.functions()) {
    if(f.isIntrinsic())
      continue;
    changed |= mutate(&f);
    for(Argument& a : f.args())
      changed |= mutate(&a);
    for(inst_iterator i = inst_begin(f); i != inst_end(f); i++) {
      for(unsigned j = 0; j < i->getNumOperands(); j++) {
        if(ConstantExpr* cexpr = dyn_cast<ConstantExpr>(i->getOperand(j))) {
          for(unsigned k = 0; k < cexpr->getNumOperands(); k++)
            changed |= mutate(cexpr->getOperand(k));
          changed |= mutate(cexpr);
        } else if(Constant* c = dyn_cast<Constant>(i->getOperand(j))) {
          changed |= mutate(c);
        }
      }

#if LLVM_VERSION_ABOVE(3, 9)
      if(GetElementPtrInst* gep = dyn_cast<GetElementPtrInst>(&*i)) {
        if(Type* repl = getReplaceType(gep->getSourceElementType()))
          gep->setSourceElementType(repl);
      } else if(AllocaInst* alloca = dyn_cast<AllocaInst>(&*i)) {
        if(Type* repl = getReplaceType(alloca->getAllocatedType()))
          alloca->setAllocatedType(repl);
      }
      if(CallInst* call = dyn_cast<CallInst>(&*i)) {
        if(Function* f = call->getCalledFunction(f))
          if(Type* repl = getReplaceType(f->getFunctionType()))
            call->mutateFunctionType(repl);
      }
#endif
      changed |= mutate(&*i);
    }
  }

  return changed;
}

char SimplifyTypesPass::ID = 0;
static RegisterPass<SimplifyTypesPass> X(
    "moya-simplify-types",
    "Fixes the types to remove the huge number of different array descriptors",
    true,
    false);

Pass* createSimplifyTypesPass() {
  return new SimplifyTypesPass();
}
