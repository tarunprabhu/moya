#include "Mutability.h"
#include "MutabilityWrapperPass.h"
#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/Environment.h"
#include "common/LLVMUtils.h"
#include "common/Map.h"
#include "common/Metadata.h"
#include "common/Set.h"

#include <llvm/IR/Attributes.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class AnnotateNoAliasPass : public ModulePass {
private:
  void collect(Value*, const moya::Environment&, moya::Contents&);
  moya::Contents collect(Argument&, const moya::Environment&);
  bool runOnFunction(Function&, const moya::Environment&);

public:
  AnnotateNoAliasPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);

public:
  static char ID;
};

AnnotateNoAliasPass::AnnotateNoAliasPass() : ModulePass(ID) {
  // initializeAnnotateNoAliasPassPass(*PassRegistry::getPassRegistry());
}

StringRef AnnotateNoAliasPass::getPassName() const {
  return "Moya Annotate NoAlias";
}

void AnnotateNoAliasPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
  AU.addRequired<MutabilityWrapperPass>();
}

void AnnotateNoAliasPass::collect(Value* v,
                                  const moya::Environment& env,
                                  moya::Contents& contents) {
  if(auto* load = dyn_cast<LoadInst>(v)) {
    contents.insert(env.lookup(load->getPointerOperand()));
    if(load->getType()->isPointerTy()) {
      contents.insert(env.lookup(load));
      for(Use& u : load->uses())
        collect(u.getUser(), env, contents);
    }
  } else if(auto* gep = dyn_cast<GetElementPtrInst>(v)) {
    contents.insert(env.lookup(gep->getPointerOperand()));
    contents.insert(env.lookup(gep));
    for(Use& u : gep->uses())
      collect(u.getUser(), env, contents);
  } else if(auto* store = dyn_cast<StoreInst>(v)) {
    contents.insert(env.lookup(store->getPointerOperand()));
  } else if(auto* cst = dyn_cast<CastInst>(v)) {
    if(cst->getOperand(0)->getType()->isPointerTy())
      contents.insert(env.lookup(cst->getOperand(0)));
    if(cst->getDestTy()->isPointerTy()) {
      contents.insert(env.lookup(cst));
      for(Use& u : cst->uses())
        collect(u.getUser(), env, contents);
    }
  }
}

// Transitively walk over all the uses of the function argument and collect
// all the pointers that are seen.
moya::Contents AnnotateNoAliasPass::collect(Argument& arg,
                                            const moya::Environment& env) {
  moya::Contents contents;
  for(Use& u : arg.uses())
    collect(u.getUser(), env, contents);
  return contents;
}

bool AnnotateNoAliasPass::runOnFunction(Function& f,
                                        const moya::Environment& env) {
  bool changed = false;

  moya::Vector<moya::Contents> accessed(moya::LLVMUtils::getNumParams(f));
  moya::Vector<unsigned> noaliases(moya::LLVMUtils::getNumParams(f));
  for(Argument& arg : f.args()) {
    if(arg.getType()->isPointerTy()) {
      if(not arg.hasNoAliasAttr())
        accessed[arg.getArgNo()] = collect(arg, env);
      else if(arg.hasByValAttr())
        noaliases[arg.getArgNo()] = false;
      else
        noaliases[arg.getArgNo()] = true;
    } else {
      noaliases[arg.getArgNo()] = false;
    }
  }

  for(unsigned i = 0; i < accessed.size(); i++)
    if(accessed.at(i).size())
      for(unsigned j = i + 1; j < accessed.size(); j++)
        noaliases[i] &= accessed.at(i).disjoint(accessed.at(j));

  for(Argument& arg : f.args())
    if(not arg.hasNoAliasAttr() and noaliases.at(arg.getArgNo())) {
      f.addAttribute(arg.getArgNo() + 1, Attribute::AttrKind::NoAlias);
      changed |= true;
    }

  return changed;
}

bool AnnotateNoAliasPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  const moya::Mutability& mutability
      = getAnalysis<MutabilityWrapperPass>().getMutability();
  const moya::Environment& env = mutability.getEnvironment();

  for(Function& f : module.functions())
    if(moya::Metadata::hasSpecialize(f))
      changed |= runOnFunction(f, env);

  return changed;
}

char AnnotateNoAliasPass::ID = 0;

static const char* name = "moya-annotate-noalias";
static const char* descr = "Adds a noalias argument to attributes";
static const bool cfg = true;
static const bool analysis = false;

// INITIALIZE_PASS_BEGIN(AnnotateNoAliasPass, name, descr, cfg, analysis)
// INITIALIZE_PASS_DEPENDENCY(MutabilityWrapperPass)
// INITIALIZE_PASS_END(AnnotateNoAliasPass, name, descr, cfg, analysis)

static RegisterPass<AnnotateNoAliasPass> X(name, descr, cfg, analysis);

Pass* createAnnotateNoAliasPass() {
  return new AnnotateNoAliasPass();
}
