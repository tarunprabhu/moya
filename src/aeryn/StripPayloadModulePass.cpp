#include "Passes.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/Set.h"
#include "common/Vector.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class StripPayloadModulePass : public ModulePass {
public:
  static char ID;

protected:
  // These fields will be cleared and reset before each new function's
  // bitcode is generated
  moya::Set<GlobalAlias*> keepAliases;
  moya::Set<Function*> keepFunctions;
  moya::Set<GlobalVariable*> keepGlobals;

  moya::Vector<Function*> removedFns;

protected:
  void collectDependencies(Function*);
  void collectDependencies(Function*, Constant*);
  void maybeAddDependency(Function*, Constant*);
  void addFunctionToKeep(Function&);
  void addGlobalToKeep(GlobalVariable&);
  bool stripAliases(Module&);
  bool stripGlobals(Module&);
  bool stripFunctions(Module&);
  bool stripMoyaMetadata(Module&);
  bool stripModule(Function&);

public:
  StripPayloadModulePass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

StripPayloadModulePass::StripPayloadModulePass() : ModulePass(ID) {
  // initializeStripPayloadModulePassPass(*PassRegistry::getPassRegistry());
}

StringRef StripPayloadModulePass::getPassName() const {
  return "Moya Strip Payload Module";
}

void StripPayloadModulePass::getAnalysisUsage(AnalysisUsage&) const {
  ;
}

bool StripPayloadModulePass::stripGlobals(Module& module) {
  moya::Vector<GlobalVariable*> globals;
  for(GlobalVariable& g : module.globals())
    if(not keepGlobals.contains(&g))
      globals.push_back(&g);

  for(GlobalVariable* g : globals) {
    g->setInitializer(nullptr);
    g->setComdat(nullptr);
    g->removeFromParent();
  }

  return globals.size();
}

bool StripPayloadModulePass::stripAliases(Module& module) {
  moya::Vector<GlobalAlias*> aliases;
  for(GlobalAlias& a : module.aliases())
    if(not keepAliases.contains(&a))
      aliases.push_back(&a);

  for(GlobalAlias* a : aliases)
    a->removeFromParent();

  return aliases.size();
}

bool StripPayloadModulePass::stripFunctions(Module& module) {
  moya::Vector<Function*> functions;
  for(Function& f : module.functions()) {
    // We get rid of the comdat because we currently don't do inlining
    // and we don't expect there to be conflicts
    f.setComdat(nullptr);
    if(not keepFunctions.contains(&f))
      functions.push_back(&f);
  }

  for(Function* f : functions)
    f->deleteBody();

  for(Function* f : functions)
    if(f->getNumUses() == 0)
      f->removeFromParent();

  return functions.size();
}

bool StripPayloadModulePass::stripMoyaMetadata(Module& module) {
  LLVMContext& context = module.getContext();

  SmallVector<StringRef, 16> attrNames;
  context.getMDKindNames(attrNames);

  SmallVector<unsigned, 16> attrIds;
  moya::Set<std::string> optAttrs
      = moya::Metadata::getOptimizationAttributeNames();
  for(StringRef attr : attrNames)
    if(moya::Metadata::isMoyaAttributeName(attr)
       and not optAttrs.contains(attr.str()))
      attrIds.push_back(context.getMDKindID(attr));

  for(Function& f : module.functions())
    for(inst_iterator i = inst_begin(f); i != inst_end(f); i++)
      for(unsigned id : attrIds)
        i->setMetadata(id, nullptr);

  for(const auto& fn :
      {std::bind<NamedMDNode*(const Module&)>(moya::Metadata::getTypesNode,
                                              std::placeholders::_1),
       std::bind<NamedMDNode*(const Module&)>(moya::Metadata::getRegionsNode,
                                              std::placeholders::_1)})
    if(NamedMDNode* nmd = fn(module))
      module.eraseNamedMetadata(nmd);

  if(NamedMDNode* nmd = moya::Metadata::getFunctionsNode(module)) {
    moya::Vector<MDNode*> nodes;
    for(MDNode* md : nmd->operands())
      if(auto* cmd = dyn_cast<ConstantAsMetadata>(md->getOperand(0)))
        if(auto* f = dyn_cast<Function>(cmd->getValue()))
          if(f->size() and f->getParent())
            nodes.push_back(md);

    nmd->eraseFromParent();
    NamedMDNode* newNode = moya::Metadata::getOrInsertFunctionsNode(module);
    for(MDNode* node : nodes)
      newNode->addOperand(node);
  }

  if(NamedMDNode* nmd = moya::Metadata::getGlobalsNode(module)) {
    moya::Vector<MDNode*> nodes;
    for(MDNode* md : nmd->operands())
      if(auto* cmd = dyn_cast<ConstantAsMetadata>(md->getOperand(0)))
        if(auto* g = dyn_cast<GlobalVariable>(cmd->getValue()))
          if(g->getParent())
            nodes.push_back(md);
    nmd->eraseFromParent();
    NamedMDNode* newNode = moya::Metadata::getOrInsertGlobalsNode(module);
    for(MDNode* node : nodes)
      newNode->addOperand(node);
  }

  if(NamedMDNode* nmd = moya::Metadata::getBackupFunctionAttributesNode(module))
    nmd->eraseFromParent();

  return true;
}

void StripPayloadModulePass::maybeAddDependency(Function* root, Constant* c) {
  if(not c)
    return;
  if(Function* f = dyn_cast<Function>(c)) {
    if(f != root)
      addFunctionToKeep(*f);
  } else if(GlobalVariable* g = dyn_cast<GlobalVariable>(c)) {
    addGlobalToKeep(*g);
  } else {
    collectDependencies(root, c);
  }
}

void StripPayloadModulePass::collectDependencies(Function* root, Constant* c) {
  if(c
     and (isa<ConstantStruct>(c) or isa<ConstantArray>(c)
          or isa<ConstantExpr>(c)))
    for(Use& op : c->operands())
      maybeAddDependency(root, dyn_cast<Constant>(op.get()));
}

void StripPayloadModulePass::collectDependencies(Function* f) {
  for(inst_iterator i = inst_begin(f); i != inst_end(f); i++)
    for(Use& op : i->operands())
      maybeAddDependency(f, dyn_cast<Constant>(op.get()));
}

void StripPayloadModulePass::addFunctionToKeep(Function& f) {
  keepFunctions.insert(&f);
  if(Function* personality = moya::LLVMUtils::getPersonalityFn(f))
    keepFunctions.insert(personality);
}

void StripPayloadModulePass::addGlobalToKeep(GlobalVariable& g) {
  keepGlobals.insert(&g);
}

bool StripPayloadModulePass::stripModule(Function& root) {
  bool changed = false;
  Module& module = *root.getParent();

  // Keep the STL functions. If we delete them, we may not be able to find
  // them at runtime
  for(Function& f : module.functions())
    if(f.size() and moya::Metadata::hasSTL(f))
      addFunctionToKeep(f);

  // First replace all uses of global aliases so we can safely delete them
  for(GlobalAlias& a : module.aliases()) {
    a.replaceAllUsesWith(a.getAliasee());
    changed |= true;
  }

  collectDependencies(&root);
  // Drop the bodies of the dependencies here because the strip functions
  // will not do it. We only need the declaration for the bitcode to be valid
  for(Function* f : keepFunctions)
    f->deleteBody();

  addFunctionToKeep(root);
  if(auto* g
     = module.getNamedGlobal(moya::Config::getPayloadRegionGlobalName()))
    addGlobalToKeep(*g);
  if(auto g
     = module.getNamedGlobal(moya::Config::getPayloadFunctionGlobalName()))
    addGlobalToKeep(*g);

  changed |= stripAliases(module);
  changed |= stripFunctions(module);
  changed |= stripGlobals(module);
  changed |= stripMoyaMetadata(module);

  return changed;
}

bool StripPayloadModulePass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  GlobalVariable* gFunc
      = module.getNamedGlobal(moya::Config::getPayloadFunctionGlobalName());
  auto* root = dyn_cast<Function>(gFunc->getInitializer()->getOperand(0));

  keepAliases.clear();
  keepFunctions.clear();
  keepGlobals.clear();
  removedFns.clear();

  changed |= stripModule(*root);

  return changed;
}

char StripPayloadModulePass::ID = 0;

static const char* name = "moya-strip-payload-module";
static const char* descr = "Strips all functions except the one for which the "
                           "module is being created";
static const bool cfg = false;
static const bool analysis = false;

// INITIALIZE_PASS(StripPayloadModulePass, name, descr, cfg, analysis)

static RegisterPass<StripPayloadModulePass> X(name, descr, cfg, analysis);

Pass* createStripPayloadModulePass() {
  return new StripPayloadModulePass();
}
