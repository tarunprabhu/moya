#include "AnalysisCacheManager.h"
#include "CacheManagerWrapperPass.h"
#include "CallGraphWrapperPass.h"
#include "Passes.h"
#include "Payload.h"
#include "Summary.h"
#include "SummaryWrapperPass.h"
#include "common/CallGraph.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/Hexify.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/PathUtils.h"
#include "common/Vector.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Transforms/Utils/Cloning.h>

#include <fstream>
#include <iostream>

using namespace llvm;

static cl::opt<std::string>
    PayloadDir("moya-payload-directory",
               cl::desc("The directory to which to write payload files"),
               cl::init("."));

class GeneratePayloadComponentsPass : public ModulePass {
protected:
  std::string payloadDir;

public:
  void annotate(LoadInst*, const moya::Summary&);
  void annotate(StoreInst*, const moya::Summary&);
  void annotate(AllocaInst*, const moya::Summary&);

  std::string getPickledFilename(const std::string&,
                                 const std::string&,
                                 const std::string&);

public:
  GeneratePayloadComponentsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);

public:
  static char ID;
};

GeneratePayloadComponentsPass::GeneratePayloadComponentsPass()
    : ModulePass(ID), payloadDir(PayloadDir) {
  // initializeGeneratePayloadComponentsPassPass(*PassRegistry::getPassRegistry());
}

StringRef GeneratePayloadComponentsPass::getPassName() const {
  return "Moya Payload Component";
}

void GeneratePayloadComponentsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesAll();
  AU.addRequired<SummaryWrapperPass>();
  AU.addRequired<CallGraphWrapperPass>();
  AU.addRequired<CacheManagerWrapperPass>();
}

std::string
GeneratePayloadComponentsPass::getPickledFilename(const std::string& prefix,
                                                  const std::string& label,
                                                  const std::string& ext) {
  std::stringstream file;

  file << prefix << "_moya_" << label << "." << ext;
  return moya::PathUtils::construct(payloadDir, file.str());
}

void GeneratePayloadComponentsPass::annotate(LoadInst* load,
                                             const moya::Summary& summary) {
  MoyaID id = moya::Metadata::getID(load);
  const moya::Summary::Addresses& addrs = summary.getAddresses(id);

  moya::Status status = moya::Status::getUnused();
  for(moya::Summary::Address addr : addrs)
    status.update(summary.getStatus(addr));

  if(not status.isVariable())
    moya::LLVMUtils::setMetadata(load, "invariant.load");
  moya::Metadata::setSummaryAddress(load, addrs);
}

void GeneratePayloadComponentsPass::annotate(AllocaInst* alloca,
                                             const moya::Summary& summary) {
  MoyaID id = moya::Metadata::getID(alloca);
  const moya::Summary::Addresses& addrs = summary.getAddresses(id);
  if(addrs.size() > 1)
    moya_error("Expect only single address for alloca");

  moya::Metadata::setSummaryAddress(alloca, addrs);
}

bool GeneratePayloadComponentsPass::runOnModule(Module& module) {
  moya_message(getPassName());

  moya::Vector<Function*> funcs;
  for(Function& f : module.functions())
    if(f.size() and moya::Metadata::hasSpecialize(f))
      funcs.push_back(&f);

  // Create a bitcode file for each function to be JIT'ed - one for each
  // region. The module will be tagged with a global variable indicating
  // which region it is intended for and which function the module is for.
  // The module will be stripped in a later pass
  //
  for(const moya::Region& region : moya::Metadata::getRegions(module)) {
    moya_message("Payload component for region: " << moya::hex(region.getID()));
    RegionID regionID = region.getID();

    // The region may have been marked, but if the code in which the
    // region was marked isn't ever called, the callgraph will not have been
    // constructed
    const moya::CallGraph& cg
        = getAnalysis<CallGraphWrapperPass>().getCallGraph(regionID);
    if(not cg.isConstructed())
      continue;

    moya::AnalysisCacheManager& cacheMgr
        = getAnalysis<CacheManagerWrapperPass>().getCacheManager();
    const std::string& prefix = cacheMgr.getPrefix();

    for(Function* f : funcs) {
      JITID jitid = moya::Metadata::getJITID(f);
      moya_message("Payload component for function: "
                   << f->getName() << " [" << moya::Metadata::getID(f) << " => "
                   << jitid << "]");

      const moya::Summary& summary
          = getAnalysis<SummaryWrapperPass>().getSummary(regionID, jitid);

      // If the function is not called within this region, the summary will not
      // be valid
      if(not summary.isSummarized())
        continue;

      // The module is cloned for a particular function in a particular
      // region. This function is called the root function. The module will be
      // cleaned up in a separate pass
      std::unique_ptr<Module> cloned(CloneModule(module).release());
      Module& clone = *cloned.get();
      Function* root = clone.getFunction(f->getName());

      LLVMContext& context = clone.getContext();
      Type* pi8 = Type::getInt8PtrTy(context);

      // Add global variables to the module which will be used to identify
      // the root function and the region for which this module is created
      GlobalVariable* gRegion = moya::LLVMUtils::getPayloadRegionGlobal(clone);
      gRegion->setConstant(true);
      gRegion->setLinkage(GlobalValue::LinkageTypes::ExternalLinkage);
      gRegion->setInitializer(
          moya::LLVMUtils::getRegionIDConstant(context, regionID));

      GlobalVariable* gFunc = moya::LLVMUtils::getPayloadFunctionGlobal(clone);
      gFunc->setConstant(true);
      gFunc->setLinkage(GlobalValue::LinkageTypes::ExternalLinkage);
      gFunc->setInitializer(ConstantExpr::getPointerCast(root, pi8));

      // Add annotations to the root function
      for(auto i = inst_begin(root); i != inst_end(root); i++) {
        if(auto* load = dyn_cast<LoadInst>(&*i))
          annotate(load, summary);
        else if(auto* alloca = dyn_cast<AllocaInst>(&*i))
          annotate(alloca, summary);
      }

      // Write the summary to disk
      std::string pklBase
          = moya::Config::getPickledSummaryFilename(regionID, jitid);
      std::string pkl = getPickledFilename(prefix, pklBase, "pkl");
      moya::Payload(pkl).pickle(summary);

      // Write the bitcode file to disk. A future pass will clean it up
      std::string bcBase
          = moya::Config::getPickledModuleFilename(regionID, *root);
      std::string bc = getPickledFilename(prefix, bcBase, "bc");
      moya::Payload(bc).pickle(clone);
    }
  }

  return false;
}

char GeneratePayloadComponentsPass::ID = 0;

static const char* name = "moya-generate-payload-components";
static const char* descr
    = "Generates all the components that will go into the payload";
static const bool cfg = true;
static const bool analysis = true;

// INITIALIZE_PASS_BEGIN(GeneratePayloadComponentsPass, name, descr, cfg,
// analysis) INITIALIZE_PASS_DEPENDENCY(SummaryWrapperPass)
// INITIALIZE_PASS_DEPENDENCY(CallGraphWrapperPass)
// INITIALIZE_PASS_DEPENDENCY(CacheManagerWrapperPass)
// INITIALIZE_PASS_END(GeneratePayloadComponentsPass, name, descr, cfg,
// analysis)

static RegisterPass<GeneratePayloadComponentsPass>
    X(name, descr, cfg, analysis);

Pass* createGeneratePayloadComponentsPass() {
  return new GeneratePayloadComponentsPass();
}
