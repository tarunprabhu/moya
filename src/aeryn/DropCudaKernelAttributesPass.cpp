#include "common/ContainerUtils.h"
#include "common/Metadata.h"
#include "common/StringUtils.h"

#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>

using namespace llvm;

// Cuda kernels are executed on the GPU and are identified with the __global__
// tag. When these are compiled, a host and device version of the function
// are created. The host version only sets up the arguments and launches the
// kernel on the device.
// For Aeryn's analysis to work, we actually need the device code because
// that is where the work is done and we need to know what happens to the
// parameters passed to the kernels. To suppress warnings since we are force
// importing code form a module for a different target, we need to get
// rid of the target-specific attributes form the kernels
class DropCudaKernelAttributesPass : public ModulePass {
public:
  static char ID;

public:
  DropCudaKernelAttributesPass() : ModulePass(ID) {
    ;
  }

  virtual void getAnalysisUsage(AnalysisUsage& AU) const {
    AU.setPreservesCFG();
  }

  virtual bool runOnModule(Module& module) {
    LLVMContext& context = module.getContext();
    for(Function& f : module.functions()) {
      if(moya::Metadata::hasCudaKernelDevice(f)) {
        AttrBuilder builder(f.getAttributes(), AttributeSet::FunctionIndex);
        builder.removeAttribute("target-features");
        builder.removeAttribute("target-cpu");
        f.setAttributes(
            AttributeSet::get(context, AttributeSet::FunctionIndex, builder));
      }
    }

    return true;
  }
};

char DropCudaKernelAttributesPass::ID = 0;

static RegisterPass<DropCudaKernelAttributesPass>
    X("moya-drop-cuda-kernel-attributes",
      "Drop the host code corresponding to the Cuda kernels",
      true,
      false);

Pass* createDropCudaKernelAttributesPass() {
  return new DropCudaKernelAttributesPass();
}
