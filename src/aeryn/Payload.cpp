#include "Payload.h"
#include "common/Config.h"
#include "common/Diagnostics.h"

#include <llvm/Bitcode/BitcodeWriter.h>
#include <llvm/Support/raw_ostream.h>

using namespace llvm;

namespace moya {

Payload::Payload(const std::string& file,
                 const Vector<std::string>& bcs,
                 const Vector<std::string>& pkls)
    : PayloadBase(file, std::ios::app), bcs(bcs), pkls(pkls) {
  ;
}

void Payload::pickle(const Summary& summary) {
  summary.pickle(*this);
}

void Payload::pickle(const Module& module) {
  std::string s;
  raw_string_ostream ss(s);

  WriteBitcodeToFile(module, ss);
  write(ss.str(), false);
}

bool Payload::create() {
  // Offset of header from start of executable. This will be written at the
  // end of the payload
  size_t offset = fout().tellp();

  // Header
  write<uint64_t>(Config::getPayloadMagicNumber());

  // Summaries
  write<size_t>(pkls.size());
  for(const std::string& pkl : pkls) {
    std::ifstream file(pkl, std::ios::in | std::ios::binary);
    fout() << file.rdbuf();
    file.close();
  }

  // Modules
  write<size_t>(bcs.size());
  for(const std::string& bc : bcs) {
    std::ifstream file(bc, std::ios::in | std::ios::binary | std::ios::ate);

    size_t size = file.tellg();
    file.seekg(0, std::ios::beg);

    write<size_t>(size);
    fout() << file.rdbuf();
    file.close();
  }

  // Offset of header from start of executable
  write<size_t>(offset);

  // Footer
  write<uint64_t>(Config::getPayloadMagicNumber());

  return true;
}

} // namespace moya
