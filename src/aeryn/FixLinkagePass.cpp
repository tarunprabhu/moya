#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>

#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

using namespace llvm;

// When the bitcode is linked by clang, all the functions' linkages are
// changed to internal. When generating object code out of this, everything
// gets dead-code eliminated because the main() function is never called.
// This finds the main function and changes the linkage to external
// so the dead-code eliminator doesn't get too trigger-happy
//
// UPDATE: This does the same for all other functions and global variables
// for the same reason - otherwise, they don't get found by Talyn
//
// **********************
//

class FixLinkagePass : public ModulePass {
public:
  static char ID;

public:
  FixLinkagePass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

FixLinkagePass::FixLinkagePass() : ModulePass(ID) {
  ;
}

StringRef FixLinkagePass::getPassName() const {
  return "Moya Fix Linkage";
}

void FixLinkagePass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool FixLinkagePass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  for(Function& f : module.functions()) {
    if(moya::Metadata::hasLinkage(f)) {
      f.setLinkage(GlobalValue::LinkageTypes::ExternalLinkage);
      changed |= true;
    } else {
      f.setLinkage(GlobalValue::LinkageTypes::ExternalLinkage);
    }
  }

  for(GlobalVariable& g : module.globals()) {
    if(moya::Metadata::hasLinkage(g)) {
      g.setLinkage(moya::Metadata::getLinkage(g));
      changed |= true;
    }
  }

  return changed;
}

char FixLinkagePass::ID = 0;

static RegisterPass<FixLinkagePass>
    X("moya-fix-linkage",
      "Sets the main function to have external linkage",
      true,
      false);

Pass* createFixLinkagePass() {
  return new FixLinkagePass();
}
