#ifndef MOYA_AERYN_MUTABILITY_H
#define MOYA_AERYN_MUTABILITY_H

#include "common/HashMap.h"
#include "common/Map.h"
#include "common/Set.h"
#include "common/Vector.h"
#include "models/Models.h"

#include <llvm/IR/Constants.h>
#include <llvm/IR/Module.h>

namespace moya {

class Environment;
class Store;
class StoreCell;

class Mutability {
private:
  llvm::Module& module;

  // We can get away with using a single environment for the entire thing
  // because
  // - The environment is indexed with the LLVM Value which is known to be
  //   unique, so we needen't worry about allocated variables leaking into
  //   callees
  // - The analysis is flow-insensitive and context insensitive,
  //   so we fold all calls anyway
  Environment env;

  // Regardless of how many environments are used, only a single store should
  // be used
  Store store;

  Models models;

  // The instructions for the ConstantExprs
  Vector<llvm::Instruction*> insts;

  // These are instructions that are generated from constexprs and need to
  // be deleted explicitly
  Map<const llvm::ConstantExpr*, const llvm::Instruction*> cexprInsts;

  // The reverse lookup to see if an instruction was originally from a
  // ConstantExpr or not
  Map<const llvm::Instruction*, const llvm::ConstantExpr*> instCExprs;

  // This is set to true if the analysis "fails"
  // One way of thinking about top is that the analysis cannot be sure of
  // anything in the program. Here are some examples when this occurs
  //
  // Example 1:
  //
  //   void(*fn)() = (void(*)()) dlsym(s)
  //   fn();
  //
  // In this case, we have no idea what function was requested from dlsym,
  // so we have no way of knowing what it does with the objects in the program
  // so we to mark everything as being mutable
  //
  // Example 2:
  //
  //   void *ptr = ...
  //   unsigned long iptr = (unsigned long)ptr;
  //   ptr += n;
  //   *ptr = 0;
  //
  // Here, we have added an unknown value to a pointer, so now we have no idea
  // where we are pointing. Since we don't know what object is being modified,
  // we assume that everything is modified
  bool top = false;

  // These are several parameters that are set depending on the environment
  // variables which are useful when debugging
  // This is a set of functions for which every instruction is printed to
  // stderr. This is useful for debugging. The set is initialized using
  // environment variables (see the constructor for the class)
  Set<std::string> debugFns;
  Set<std::string> debugFnsImmediate;
  unsigned debugStore;

  // This is the iteration counter
  unsigned iteration;

private:
  Set<const llvm::Function*> getCallees(const llvm::Instruction*,
                                        const Contents&,
                                        llvm::Type*,
                                        Environment&,
                                        Store&,
                                        const llvm::Function*,
                                        AnalysisStatus&) const;
  Set<const llvm::Function*>
  getCallees(const llvm::Instruction*, const Contents&, const Store&) const;

  Vector<const llvm::Function*>
  processLevel(const Vector<const llvm::Function*>&,
               const HashSet<const llvm::Function*>&);
  Vector<const llvm::Function*> generateProcessingOrder(const llvm::Function*);

  void collectConstantExprs(llvm::Value*, Set<llvm::ConstantExpr*>&);

  void initialize();

  std::string get_err_string(const llvm::Function*);

  void initializeZero(llvm::Type*, const Store::Address*, Store&);
  void initializeZero(llvm::PointerType*, const Store::Address*, Store&);
  void initializeZero(llvm::ArrayType*, const Store::Address*, Store&);
  void initializeZero(llvm::StructType*, const Store::Address*, Store&);

  AnalysisStatus processCall(const llvm::Instruction*,
                             const llvm::Value*,
                             const Vector<const llvm::Value*>&,
                             Environment&,
                             Store&,
                             const llvm::Function*,
                             const llvm::Module&);

  AnalysisStatus processInlineAsm(const llvm::Instruction*,
                                  const llvm::InlineAsm*,
                                  const Vector<const llvm::Value*>&,
                                  Environment&,
                                  Store&,
                                  const llvm::Function*,
                                  const llvm::Module&);

  // This function is only used for GEP instructions because we it always
  // computes offsets off a pointer, so actual store addresses are always
  // computed
  AnalysisStatus processIndices(const llvm::GetElementPtrInst*,
                                unsigned,
                                llvm::Type*,
                                Vector<const llvm::Value*>,
                                const Store::Address*,
                                int64_t,
                                Environment&,
                                Store&,
                                const llvm::Function*,
                                const llvm::Module&);

  // This function is used in InsertValue and ExtractValue instructions because
  // in that case, there's an actual object on which the offsets are computed
  // and not an address
  AnalysisStatus processIndices(const llvm::InsertValueInst*,
                                const Store::Address*,
                                llvm::Type*,
                                const llvm::ArrayRef<unsigned>&,
                                unsigned,
                                Environment&,
                                Store&,
                                const llvm::Function*,
                                const llvm::Module&);
  AnalysisStatus processIndices(const llvm::ExtractValueInst*,
                                const ContentComposite*,
                                const llvm::ArrayRef<unsigned>&,
                                unsigned,
                                Environment&,
                                Store&,
                                const llvm::Function*,
                                const llvm::Module&);

  // The process instructions return true only if one of the typeinfo
  // objects being processed were changed or if something new was added
  // to the environment
  // The environment and store are passed explicitly because although the
  // analysis is currentlt flow insensitive, it might at some point be rewritten
  // to be flow-sensitive, in which case we will at least need multiple
  // environments
  AnalysisStatus process(const llvm::AllocaInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::BinaryOperator*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::BranchInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::IndirectBrInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::CallInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::InvokeInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::BitCastInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::PtrToIntInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::IntToPtrInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::ZExtInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::SExtInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::FPExtInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::TruncInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::FPTruncInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::SIToFPInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::FPToSIInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::UIToFPInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::FPToUIInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::CmpInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::GetElementPtrInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::LoadInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::PHINode*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::SelectInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::StoreInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::SwitchInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::InsertValueInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::ExtractValueInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::ReturnInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::UnreachableInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::LandingPadInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::ResumeInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::FenceInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::AtomicRMWInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::AtomicCmpXchgInst*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::Instruction*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);

  // Constants
  AnalysisStatus process(const llvm::UndefValue*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::ConstantInt*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::ConstantFP*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::ConstantPointerNull*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::ConstantDataArray*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::ConstantArray*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::ConstantStruct*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::ConstantAggregateZero*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);
  AnalysisStatus process(const llvm::ConstantExpr*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);

  // We should go to top when we encounter this
  AnalysisStatus process(const llvm::InlineAsm*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);

  AnalysisStatus process(const llvm::GlobalAlias*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);

  AnalysisStatus process(const llvm::Value*,
                         Environment&,
                         Store&,
                         const llvm::Function*,
                         const llvm::Module&);

  AnalysisStatus process(const llvm::Function*, Environment&, Store&);

  // Debug functions controlled by environment variables
  void maybePrintInstructionOperands(const llvm::Instruction*,
                                     const Environment&,
                                     const Store&,
                                     const llvm::Function*,
                                     const llvm::Module&) const;
  void maybePrintInstructionResult(const llvm::Instruction*,
                                   const Environment&,
                                   const Store&,
                                   const llvm::Function*,
                                   const llvm::Module&) const;
  void maybePrintStore(unsigned,
                       const llvm::Function*,
                       const Environment&,
                       const Store&) const;
  void maybePrintCall(const llvm::Instruction*,
                      const Set<const llvm::Function*>&,
                      const Environment&,
                      const Store&,
                      const llvm::Function*,
                      const llvm::Module&) const;
  void maybePrintFunction(unsigned,
                          const llvm::Function*,
                          const Environment&,
                          const Store&) const;

  void printCallees(const llvm::Instruction*,
                    const llvm::Value*,
                    const Environment&,
                    const Store&,
                    const llvm::Function*,
                    const llvm::Module&) const;
  void printInstructionOperands(const llvm::Instruction*,
                                const Environment&,
                                const Store&,
                                const llvm::Function*,
                                const llvm::Module&) const;
  void printInstructionResult(const llvm::Instruction*,
                              const Environment&,
                              const Store&,
                              const llvm::Function*,
                              const llvm::Module&) const;
  void printFunction(unsigned,
                     const llvm::Function*,
                     const Environment&,
                     const Store&) const;

public:
  Mutability(llvm::Module&, const ClassHeirarchies&);
  virtual ~Mutability();

  const Environment& getEnvironment() const;
  const Store& getStore() const;

  bool isTop() const;
  unsigned getIterations() const;

  bool runOnModule();
};

} // namespace moya

#endif // MOYA_AERYN_MUTABILITY_H
