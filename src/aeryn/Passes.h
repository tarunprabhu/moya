#ifndef MOYA_AERYN_PASSES_H
#define MOYA_AERYN_PASSES_H

#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Pass.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

#include <string>

llvm::Pass* createSummaryWrapperPass();
llvm::Pass* createCallGraphWrapperPass();
llvm::Pass* createMutabilityWrapperPass();
llvm::Pass* createCacheManagerWrapperPass();
llvm::Pass* createClassHeirarchiesWrapperPass();

llvm::Pass* createAnnotateNoAliasPass();
llvm::Pass* createAnnotateMallocWrappersPass();
llvm::Pass* createAnnotateNewWrappersPass();
llvm::Pass* createAnnotateLoopVariantPointersPass();
llvm::Pass* createAnnotateMallocsPass();
llvm::Pass* createAnnotateAllocatesPass();
llvm::Pass* createAnnotateSystemMallocsPass();
llvm::Pass* createCanonicalizeMetadataPass();
llvm::Pass* createNameStructsPass();
llvm::Pass* createSimplifyCastsPass();
llvm::Pass* createEliminateRedirectPass();
llvm::Pass* createFixCastsPass();
llvm::Pass* createFixDeadBranchesPass();
llvm::Pass* createFixLinkagePass();
llvm::Pass* createStripCastsPass();
llvm::Pass* createStripPayloadModulePass();
llvm::Pass* createGenerateIDsPass();
llvm::Pass* createGeneratePayloadComponentsPass();
llvm::Pass* createIROutputPass(const std::string&);
llvm::Pass* createGlobalAliasEliminationPass();
llvm::Pass* createIdentifySummarizableFunctionsPass();
llvm::Pass* createSetModelNamesPass();
llvm::Pass* createSanityCheckPass();

llvm::Pass* createDropCudaKernelAttributesPass();
llvm::Pass* createDropCudaKernelLaunchersPass();

namespace llvm {
void initializeAnnotateMallocWrappersPassPass(PassRegistry&);
void initializeAnnotateNewWrappersPassPass(PassRegistry&);
void initializeAnnotateNoAliasPassPass(PassRegistry&);
void initializeCanonicalizeMetadataPassPass(PassRegistry&);
void initializeEliminateRedirectPassPass(PassRegistry&);
void initializeFixLinkagePassPass(PassRegistry&);
void initializeFixDeadBranchesPassPass(PassRegistry&);
void initializeGenerateIDsPassPass(PassRegistry&);
void initializeGeneratePayloadComponentsPassPass(PassRegistry&);
void initializeGlobalAliasEliminationPassPass(PassRegistry&);
void initializeMutabilityWrapperPassPass(PassRegistry&);
void initializeCacheManagerWrapperPassPass(PassRegistry&);
void initializeCallGraphWrapperPassPass(PassRegistry&);
void initializeClassHeirarchiesWrapperPassPass(PassRegistry&);
void initializeSummaryWrapperPassPass(PassRegistry&);
void initializePrepareMutabilityPassPass(PassRegistry&);
void initializeRestoreFunctionAttributesPassPass(PassRegistry&);
void initializeSanityCheckPassPass(PassRegistry&);
void initializeSetModelNamesPassPass(PassRegistry&);
void initializeStripPayloadModulePassPass(PassRegistry&);
void initializeStripCastsPassPass(PassRegistry&);
} // namespace llvm

#endif // MOYA_AERYN_PASSES_H
