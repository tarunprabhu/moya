#include "Passes.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/Metadata.h"
#include "common/Set.h"

#include <llvm/IR/InstIterator.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Metadata.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class GenerateIDsPass : public ModulePass {
public:
  static char ID;

public:
  GenerateIDsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

GenerateIDsPass::GenerateIDsPass() : ModulePass(ID) {
  // initializeGenerateIDsPassPass(*PassRegistry::getPassRegistry());
}

StringRef GenerateIDsPass::getPassName() const {
  return "Moya Generate IDs";
}

void GenerateIDsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool GenerateIDsPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;
  MoyaID id = moya::Config::getFirstValidMoyaID(IDKind::Function);

  // Functions with definitions
  for(Function& f : module.functions())
    if(f.size() and not moya::Metadata::hasNoAnalyze(f))
      changed |= moya::Metadata::setID(f, id = moya::Config::getNextMoyaID(id));

  // Arguments of functions to be JIT'ed
  for(Function& f : module.functions())
    if(f.size() and moya::Metadata::hasSpecialize(f))
      for(Argument& arg : f.args())
        changed
            |= moya::Metadata::setID(arg, id = moya::Config::getNextMoyaID(id));

  // Instructions in functions to be JIT'ed
  for(Function& f : module.functions())
    if(f.size() and moya::Metadata::hasSpecialize(f))
      for(auto i = inst_begin(f); i != inst_end(f); i++)
        changed
            |= moya::Metadata::setID(&*i, id = moya::Config::getNextMoyaID(id));

  // Global non-constant and accessible variables
  for(GlobalVariable& g : module.globals())
    if(g.hasName() and not g.isConstant()
       and not moya::Metadata::hasNoAnalyze(g))
      changed |= moya::Metadata::setID(g, id = moya::Config::getNextMoyaID(id));

  return changed;
}

char GenerateIDsPass::ID = 0;

static const char* name = "moya-ids";
static const char* descr
    = "Generates ids for functions, arguments, instructions and globals";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(GenerateIDsPass, name, descr, cfg, analysis)

static RegisterPass<GenerateIDsPass> X(name, descr, cfg, analysis);

Pass* createGenerateIDsPass() {
  return new GenerateIDsPass();
}
