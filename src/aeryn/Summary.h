#ifndef MOYA_AERYN_SUMMARY_H
#define MOYA_AERYN_SUMMARY_H

#include "SummaryCell.h"
#include "common/APITypes.h"
#include "common/Serializer.h"
#include "common/Set.h"
#include "common/SourceLang.h"
#include "common/Status.h"
#include "common/Store.h"
#include "common/SummaryBase.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>

namespace moya {

// This intentionally inherits from Summary. We need an intermediate class
// that has some of the functionality of the store, but doesn't need all of
// the features that a store provides but is not quite as stripped down as a
// summary. This is also cleaner because it keeps the summary clean by moving
// all of the analysis-time code here.

class CallGraph;
class Environment;
class Payload;

class Summary : public SummaryBase {
public:
  using Address = SummaryBase::Address;

protected:
  llvm::Function& f;

  const CallGraph& cg;
  const Environment& env;
  const Store& store;

  std::string fname;

  // This will be set to the first available address in the store when we
  // begin summarizing. The idea is that the addresses of global variables,
  // function arguments and allocated memory will be the same as the store.
  // But all the folded arrays will be freshly allocated so as not to collide
  // with the already allocated addresses.
  Address nextAvailableAddress;

  // These are here so we don't have to pass the module around everywhere
  moya::Set<llvm::StructType*> recursive;
  moya::Set<llvm::Type*> polymorphic;

  moya::Set<std::pair<Address, const Store::Address*>> merged;

protected:
  void initialize(llvm::Module&, const Store&);

  bool isConstInClosureOfFunction(const StoreCell&,
                                  const llvm::Function*,
                                  const CallGraph&) const;
  bool isConstInProperClosureOfFunction(const StoreCell&,
                                        const llvm::Function*,
                                        const CallGraph&) const;
  bool isRHSConstInClosureOfRegion(const StoreCell&, const CallGraph&) const;

  Status computeStatus(const StoreCell&, const CallGraph&) const;

  Status computeGlobalStatus(llvm::Type*, Address);
  Status computeGlobalStatus(llvm::GlobalVariable&, llvm::Type*, Address);
  Status
  computeGlobalStatus(llvm::GlobalVariable&, llvm::PointerType*, Address);
  Status computeGlobalStatus(llvm::GlobalVariable&, llvm::ArrayType*, Address);
  Status computeGlobalStatus(llvm::GlobalVariable&, llvm::StructType*, Address);
  Status computeGlobalStatus(llvm::GlobalVariable&, Address);

  Status computeArgumentStatus(llvm::Argument&, Address);
  template <SourceLang l>
  Status computeArgumentStatus(llvm::Argument&, Address);

  void propagate(llvm::LoadInst*, Address);
  void propagate(llvm::GetElementPtrInst*, Address);
  void propagate(llvm::AllocaInst*, Address);
  void propagate(llvm::PHINode*, Address);
  void propagate(llvm::Value*, Address);

  SummaryCell& makeCell(const StoreCell&);
  SummaryCell& getCell(Address);
  const SummaryCell& getCell(Address) const;
  bool hasCell(Address) const;

  bool hasPolymorphicType(llvm::Type*, moya::Set<llvm::Type*>&);
  bool hasPolymorphicType(llvm::Type*);

  void setStatus(Address, const Status&);
  void updateStatus(Address, const Status&);

  void summarize();

public:
  Summary(llvm::Function&,
          const moya::Region&,
          bool,
          const CallGraph&,
          const Environment&,
          const Store&);
  virtual ~Summary() = default;

  RegionID getRegionID() const;
  MoyaID getAnalysisID() const;
  const std::string& getFunctionName() const;

  std::string str(bool = false, int = 0) const;
  void serialize(Serializer&) const;
  void pickle(Payload&) const;
};

} // namespace moya

#endif // MOYA_AERYN_SUMMARY_H
