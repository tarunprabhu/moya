#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/Vector.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instructions.h>

using namespace llvm;

// This is needed to remove the block containing the redirect function
// Even though the function call is removed, the arguments to the function
// are marked as read which results in a unused fields being marked as read.
// While this is safe, it would be nice to not have that be the case
//
class FixDeadBranchesPass : public ModulePass {
protected:
  bool runOnFunction(Function&);

public:
  FixDeadBranchesPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);

public:
  static char ID;
};

static bool areOperandsEqual(Instruction* inst) {
  if(inst->getNumOperands() == 0)
    return true;
  for(unsigned i = 1; i < inst->getNumOperands(); i++)
    if(inst->getOperand(i) != inst->getOperand(0))
      return false;
  return true;
}

FixDeadBranchesPass::FixDeadBranchesPass() : ModulePass(ID) {
  // initializeFixDeadBranchesPassPass(*PassRegistry::getPassRegistry());
}

StringRef FixDeadBranchesPass::getPassName() const {
  return "Moya Fix Dead Branches";
}

void FixDeadBranchesPass::getAnalysisUsage(AnalysisUsage& AU) const {
  ;
}

bool FixDeadBranchesPass::runOnFunction(Function& f) {
  bool changed = false;

  BasicBlock* block = nullptr;
  for(inst_iterator i = inst_begin(f); i != inst_end(f); i++)
    if(BranchInst* br = dyn_cast<BranchInst>(&*i))
      if(br->isConditional())
        if(CmpInst* cmp = dyn_cast<CmpInst>(br->getCondition()))
          // If the operands are identical, then one of the successors is dead
          if(areOperandsEqual(cmp) and br->getNumSuccessors() == 2)
            // This gets the branch which is not taken
            block = br->getSuccessor(cmp->isTrueWhenEqual() ? 1 : 0);

  // From the block, delete all but the terminator
  if(block) {
    moya::Vector<Instruction*> todel;
    for(auto i = block->begin(); i != block->end(); i++)
      if(not(*i).isTerminator() and (*i).use_empty())
        todel.push_back(&*i);
    changed |= todel.size();
    for(Instruction* i : todel)
      i->eraseFromParent();
    if(todel.size() == 0)
      return changed;
  }

  return changed;
}

bool FixDeadBranchesPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  for(Function& f : module.functions())
    if(f.size())
      changed |= runOnFunction(f);

  return changed;
}

char FixDeadBranchesPass::ID = 0;

static const char* name = "moya-fix-dead-branches";
static const char* descr
    = "Removes all the instructions from dead basic blocks";
static const bool cfg = false;
static const bool analysis = false;

INITIALIZE_PASS(FixDeadBranchesPass, name, descr, cfg, analysis)

static RegisterPass<FixDeadBranchesPass> X(name, descr, cfg, analysis);

Pass* createFixDeadBranchesPass() {
  return new FixDeadBranchesPass();
}
