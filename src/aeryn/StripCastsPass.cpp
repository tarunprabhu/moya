#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/Vector.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instructions.h>
#include <llvm/Pass.h>

using namespace llvm;

// Gets rid of redundant casts, i.e. casts where the source and destination
// type are the same. This happens a lot in DragonEgg
//
class StripCastsPass : public ModulePass {
protected:
  bool runOnFunction(Function&);

public:
  StripCastsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);

public:
  static char ID;
};

StripCastsPass::StripCastsPass() : ModulePass(ID) {
  // initializeStripCastsPassPass(*PassRegistry::getPassRegistry());
}

StringRef StripCastsPass::getPassName() const {
  return "Moya Strip Casts";
}

void StripCastsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool StripCastsPass::runOnFunction(Function& f) {
  bool changed = false;
  moya::Vector<Instruction*> todelete;

  for(auto i = inst_begin(f); i != inst_end(f); i++) {
    if(CastInst* cst = dyn_cast<CastInst>(&*i)) {
      if(cst->getSrcTy() == cst->getDestTy()) {
        cst->replaceAllUsesWith(cst->getOperand(0));
        todelete.push_back(cst);
        changed = true;
      }
    }
  }
  for(auto inst : todelete)
    inst->eraseFromParent();

  return changed;
}

bool StripCastsPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  for(Function& f : module.functions())
    if(f.size())
      changed |= runOnFunction(f);

  return changed;
}

char StripCastsPass::ID = 0;

static const char* name = "moya-strip-casts";
static const char* descr = "Removes unnecessary bitcasts";
static const bool cfg = true;
static const bool analysis = false;

// INITIALIZE_PASS(StripCastsPass, name, descr, cfg, analysis)

static RegisterPass<StripCastsPass> X(name, descr, cfg, analysis);

Pass* createStripCastsPass() {
  return new StripCastsPass();
}
