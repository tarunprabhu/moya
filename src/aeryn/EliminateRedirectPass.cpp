#include "Passes.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instructions.h>
#include <llvm/Pass.h>

using namespace llvm;

static CallInst* getCheckInstruction(Function& f) {
  // There will be a single call to moya_is_in_region()
  for(inst_iterator i = inst_begin(f); i != inst_end(f); i++)
    if(auto* call = dyn_cast<CallInst>(&*i))
      if(Function* callee = moya::LLVMUtils::getCalledFunction(call))
        if(callee->getName() == moya::Config::getIsInRegionFunctionName())
          return call;
  return nullptr;
}

// static moya::Vector<Instruction*> getStatsRecordInstructions(Function& f) {
//   moya::Vector<Instruction*> calls;
//   for(inst_iterator i = inst_begin(f); i != inst_end(f); i++)
//     if(auto* call = dyn_cast<CallInst>(&*i))
//       if(Function* callee = moya::LLVMUtils::getCalledFunction(call))
//         if(callee->getName() ==
//         moya::Config::getStatsStartExecuteFunctionName()
//            or callee->getName()
//                   == moya::Config::getStatsStopExecuteFunctionName())
//           calls.push_back(call);
//   return calls;
// }

// Removes the "within JIT region" check from the function. This is intended
// to be run on code that will be JITted. Also removes the timiing
// instrumentation that Moya might have inserted
class EliminateRedirectPass : public ModulePass {
protected:
  bool runOnFunction(Function&);

public:
  EliminateRedirectPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);

public:
  static char ID;
};

EliminateRedirectPass::EliminateRedirectPass() : ModulePass(ID) {
  // initializeEliminateRedirectPassPass(*PassRegistry::getPassRegistry());
}

StringRef EliminateRedirectPass::getPassName() const {
  return "Moya Eliminate Redirect";
}

void EliminateRedirectPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool EliminateRedirectPass::runOnFunction(Function& f) {
  bool changed = false;

  if(moya::Metadata::hasSpecialize(f)) {
    // The -jump-threading and -adce passes will get rid of the code. This just
    // disables the check
    if(Instruction* check = getCheckInstruction(f)) {
      // The first use of this could be a trunc instruction.
      LLVMContext& context = f.getContext();
      Constant* zero = ConstantInt::get(check->getType(), 0);
      Constant* fls = ConstantInt::getFalse(Type::getInt1Ty(context));
      if(auto* trunc = dyn_cast<TruncInst>(check->use_begin()->getUser()))
        trunc->replaceAllUsesWith(fls);
      check->replaceAllUsesWith(zero);
      check->eraseFromParent();
      changed |= true;
    }

    // for(Instruction* call : getStatsRecordInstructions(f)) {
    //   call->eraseFromParent();
    //   changed |= true;
    // }
  }

  return changed;
}

bool EliminateRedirectPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  for(Function& f : module.functions())
    if(moya::Metadata::hasSpecialize(f))
      changed |= runOnFunction(f);

  return changed;
}

char EliminateRedirectPass::ID = 0;

static const char* name = "moya-eliminate-redirect";
static const char* descr = "Removes the check to redirect if in a JIT region";
static const bool cfg = false;
static const bool analysis = false;

// INITIALIZE_PASS(EliminateRedirectPass, name, descr, cfg, analysis)

static RegisterPass<EliminateRedirectPass> X(name, descr, cfg, analysis);

Pass* createEliminateRedirectPass() {
  return new EliminateRedirectPass();
}
