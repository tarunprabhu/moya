#ifndef MOYA_STDCPP_DEMANGLER_H
#define MOYA_STDCPP_DEMANGLER_H

#include "common/DemanglerCXX.h"
#include "common/Map.h"
#include "common/MultiMap.h"
#include "common/Vector.h"

#include <llvm/ADT/StringRef.h>

class DemanglerStdCpp : public DemanglerCXX {
public:
  using Specs = moya::MultiMap<llvm::StringRef, moya::Vector<llvm::StringRef>>;

protected:
  Specs wcs;

protected:
  moya::Vector<llvm::StringRef>
  getMatchingSpec(const std::string&, const moya::Vector<std::string>&) const;

public:
  DemanglerStdCpp();
  DemanglerStdCpp(Specs wcs);
  virtual ~DemanglerStdCpp() = default;

  void addSpecs(const Specs&);
  virtual std::string getModelName(std::string name) const;
};

#endif
