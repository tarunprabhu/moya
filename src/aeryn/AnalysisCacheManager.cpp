#include "AnalysisCacheManager.h"
#include "Summary.h"
#include "common/Config.h"
#include "common/Dotifier.h"
#include "common/Environment.h"
#include "common/Region.h"
#include "common/Serializer.h"
#include "common/Store.h"

#include <llvm/Support/FileSystem.h>
#include <llvm/Support/raw_ostream.h>

#include <sstream>

namespace moya {

AnalysisCacheManager::AnalysisCacheManager(const std::string& prefix,
                                           const std::string& sig,
                                           const std::string& time,
                                           const std::string& path)
    : CacheManagerBase(Config::getEnvSaveDir(), prefix, sig, time, path) {
  ;
}

AnalysisCacheManager::~AnalysisCacheManager() {
  if(serializer.isInitialized())
    serializer.finalize();
  if(dotifier.isInitialized())
    dotifier.finalize();
}

Serializer& AnalysisCacheManager::getSerializer(const std::string& base,
                                                const std::string& ext) {
  if(serializer.isInitialized())
    serializer.finalize();
  serializer.initialize(getFilename(base, ext));

  return serializer;
}

Dotifier& AnalysisCacheManager::getDotifier(const std::string& base,
                                            const std::string& ext) {
  if(dotifier.isInitialized())
    dotifier.finalize();
  dotifier.initialize(getFilename(base, ext));

  return dotifier;
}

Serializer& AnalysisCacheManager::getCompleteSerializer() {
  return getSerializer("analysis", "moya");
}

Serializer& AnalysisCacheManager::getSourceSerializer() {
  return getSerializer("files");
}

Serializer& AnalysisCacheManager::getEnvironmentSerializer() {
  return getSerializer("env");
}

Serializer& AnalysisCacheManager::getStoreSerializer() {
  return getSerializer("store");
}

Serializer& AnalysisCacheManager::getTypesSerializer() {
  return getSerializer("types");
}

Serializer& AnalysisCacheManager::getClassHeirarchiesSerializer() {
  return getSerializer("heirarchies");
}

Dotifier& AnalysisCacheManager::getClassHeirarchiesDotifier() {
  return getDotifier("heirarchies");
}

Dotifier& AnalysisCacheManager::getCallGraphDotifier(RegionID region) {
  std::stringstream ss;

  ss << "callgraph_" << region;

  return getDotifier(ss.str());
}

Dotifier& AnalysisCacheManager::getClosureDotifier(RegionID region) {
  std::stringstream ss;

  ss << "closure_" << region;

  return getDotifier(ss.str());
}

Serializer& AnalysisCacheManager::getCallGraphSerializer() {
  return getSerializer("callgraph");
}

Serializer& AnalysisCacheManager::getSummarySerializer(const Summary& summary) {
  std::stringstream ss;

  ss << "summary_" << summary.getFunctionName() << "_" << summary.getRegionID()
     << "_" << summary.getAnalysisID();

  return getSerializer(ss.str());
}

void AnalysisCacheManager::writeStore(const Store& store) {
  std::error_code ec;
  std::string filename = getFilename("store", "txt");
  llvm::raw_fd_ostream fs(filename, ec, llvm::sys::fs::OpenFlags::F_None);

  fs << store.str();

  fs.close();
}

void AnalysisCacheManager::writeEnvironment(const Environment& env) {
  std::error_code ec;
  std::string filename = getFilename("env", "txt");
  llvm::raw_fd_ostream fs(filename, ec, llvm::sys::fs::OpenFlags::F_None);

  fs << env.str();

  fs.close();
}

void AnalysisCacheManager::writeSummary(const Summary& summary) {
  std::stringstream ss;
  ss << "summary_" << summary.getFunctionName() << "_" << summary.getRegionID()
     << "_" << summary.getAnalysisID();

  std::error_code ec;
  std::string filename = getFilename(ss.str(), "txt");
  llvm::raw_fd_ostream fs(filename, ec, llvm::sys::fs::OpenFlags::F_None);

  fs << summary.str();

  fs.close();
}

} // namespace moya
