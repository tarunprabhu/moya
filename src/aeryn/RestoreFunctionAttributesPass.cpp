#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/Location.h"
#include "common/Map.h"
#include "common/Metadata.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>

using namespace llvm;

#define GET_0(BASE)                                               \
  {                                                               \
    moya::Metadata::get##BASE##FnAttrName(),                      \
        std::bind<bool(Function*)>(moya::Metadata::set##BASE, _1) \
  }

#define GET_1(BASE, TYPE)                              \
  {                                                    \
    moya::Metadata::get##BASE##FnAttrName(),           \
        std::bind<bool(Function*, const TYPE&, bool)>( \
            moya::Metadata::set##BASE, _1, _2, _3)     \
  }

#define GET_1P(BASE, TYPE)                         \
  {                                                \
    moya::Metadata::get##BASE##FnAttrName(),       \
        std::bind<bool(Function*, TYPE, bool)>(    \
            moya::Metadata::set##BASE, _1, _2, _3) \
  }

template <typename T>
static T get(const Metadata*);

template <>
Function* get(const Metadata* md) {
  if(auto* cm = dyn_cast_or_null<ConstantAsMetadata>(md))
    return dyn_cast_or_null<Function>(cm->getValue());
  return nullptr;
}

template <>
StructType* get(const Metadata* md) {
  if(auto* cm = dyn_cast_or_null<ConstantAsMetadata>(md))
    if(Constant* c = cm->getValue())
      return dyn_cast<StructType>(c->getType());
  return nullptr;
}

template <>
Type* get(const Metadata* md) {
  if(auto* cm = dyn_cast_or_null<ConstantAsMetadata>(md))
    if(Constant* c = cm->getValue())
      return dyn_cast<Type>(c->getType());
  return nullptr;
}

template <>
std::string get(const Metadata* md) {
  return cast<MDString>(md)->getString();
}

template <>
moya::Location get(const Metadata* md) {
  const MDNode* mdNode = cast<MDNode>(md);
  std::string file = get<std::string>(mdNode->getOperand(0));
  unsigned line = get<unsigned>(mdNode->getOperand(1));
  unsigned col = get<unsigned>(mdNode->getOperand(2));

  return moya::Location(file, line, col);
}

template <typename T>
static T get(const Metadata* md) {
  return static_cast<T>(
      cast<ConstantInt>(cast<ConstantAsMetadata>(md)->getValue())
          ->getLimitedValue());
}

// Attributes of declared functions are lost when the IR is written to file
// and reloaded. We have saved a copy of the attributes in named metadata node
// so we can restore them
//
class RestoreFunctionAttributesPass : public ModulePass {
protected:
  template <typename T>
  using RefFuncMap
      = moya::Map<std::string, std::function<bool(Function*, const T&, bool)>>;

  template <typename T>
  using PtrFuncMap
      = moya::Map<std::string, std::function<bool(Function*, T, bool)>>;

protected:
  moya::Map<std::string, std::function<bool(Function*)>> funcs0;
  std::tuple<PtrFuncMap<Type*>,
             PtrFuncMap<StructType*>,
             RefFuncMap<std::string>,
             RefFuncMap<unsigned long>,
             RefFuncMap<moya::STDKind>,
             RefFuncMap<SourceLang>,
             RefFuncMap<moya::Location>>
      funcs1;

protected:
  bool has0(const std::string&);
  bool set0(const std::string&, Function*);

  template <typename T,
            typename std::enable_if_t<std::is_pointer<T>::value, int> = 0>
  bool has1(const std::string& name) {
    return get<PtrFuncMap<T>>(funcs1).contains(name);
  }

  template <typename T,
            typename std::enable_if_t<!std::is_pointer<T>::value, int> = 0>
  bool has1(const std::string& name) {
    return get<RefFuncMap<T>>(funcs1).contains(name);
  }

  template <typename T,
            typename std::enable_if_t<std::is_pointer<T>::value, int> = 0>
  bool set1(const std::string& name, Function* f, const Metadata* md) {
    return get<PtrFuncMap<T>>(funcs1).at(name)(f, get<T>(md), false);
  }

  template <typename T,
            typename std::enable_if_t<!std::is_pointer<T>::value, int> = 0>
  bool set1(const std::string& name, Function* f, const Metadata* md) {
    return get<RefFuncMap<T>>(funcs1).at(name)(f, get<T>(md), false);
  }

public:
  RestoreFunctionAttributesPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);

public:
  static char ID;
};

RestoreFunctionAttributesPass::RestoreFunctionAttributesPass()
    : ModulePass(ID) {
  // initializeRestoreFunctionAttributesPassPass(*PassRegistry::getPassRegistry());

  using std::placeholders::_1;
  using std::placeholders::_2;
  using std::placeholders::_3;

  funcs0 = {
      GET_0(Pure),
      GET_0(NoAnalyze),
      GET_0(LLVM),
      GET_0(LLVMDebug),
      GET_0(SystemMalloc),
      GET_0(SystemFree),
      GET_0(SystemMemcpy),
      GET_0(SystemMemset),
      GET_0(Flang),
      GET_0(FlangMemcpy),
      GET_0(FlangMemset),
      GET_0(FlangAllocator),
      GET_0(FlangDeallocator),
      GET_0(FlangMalloc),
      GET_0(SystemMalloc),
      GET_0(SystemNew),
      GET_0(Std),
      GET_0(StdImpl),
      GET_0(STL),
      GET_0(Mangled),
      GET_0(Constructor),
      GET_0(Destructor),
      GET_0(Virtual),
  };

  funcs1 = std::make_tuple<PtrFuncMap<Type*>,
                           PtrFuncMap<StructType*>,
                           RefFuncMap<std::string>,
                           RefFuncMap<unsigned long>,
                           RefFuncMap<moya::STDKind>,
                           RefFuncMap<SourceLang>,
                           RefFuncMap<moya::Location>>(
      {GET_1P(STLValueType, Type*)},
      {GET_1P(STLContainerType, StructType*),
       GET_1P(STLIteratorType, StructType*),
       GET_1P(Class, StructType*)},
      {GET_1(Model, std::string),
       GET_1(SourceName, std::string),
       GET_1(QualifiedName, std::string),
       GET_1(FullName, std::string)},
      {GET_1(STLIteratorOffset, unsigned long),
       GET_1(STLPointerOffset, unsigned long),
       GET_1(STLValueOffset, unsigned long)},
      {GET_1(STLKind, moya::STDKind)},
      {GET_1(SourceLang, SourceLang)},
      {GET_1(SourceLocation, moya::Location)});
}

StringRef RestoreFunctionAttributesPass::getPassName() const {
  return "Moya Restore Function Attributes";
}

void RestoreFunctionAttributesPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool RestoreFunctionAttributesPass::has0(const std::string& name) {
  return funcs0.contains(name);
}

bool RestoreFunctionAttributesPass::set0(const std::string& name, Function* f) {
  return funcs0.at(name)(f);
}

bool RestoreFunctionAttributesPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  if(const NamedMDNode* nmd
     = moya::Metadata::getBackupFunctionAttributesNode(module)) {
    for(const MDNode* op : nmd->operands()) {
      const Metadata* mdFunc = op->getOperand(0).get();
      const MDNode* mdAttrs = cast<MDNode>(op->getOperand(1));
      if(auto* f = get<Function*>(mdFunc)) {
        for(const Metadata* md : mdAttrs->operands()) {
          const Metadata* key = cast<MDNode>(md)->getOperand(0);
          const Metadata* val = cast<MDNode>(md)->getOperand(1);
          auto name = get<std::string>(key);
          if(has0(name))
            changed |= set0(name, f);
          else if(has1<Type*>(name))
            changed |= set1<Type*>(name, f, val);
          else if(has1<StructType*>(name))
            changed |= set1<StructType*>(name, f, val);
          else if(has1<std::string>(name))
            changed |= set1<std::string>(name, f, val);
          else if(has1<unsigned long>(name))
            changed |= set1<unsigned long>(name, f, val);
          else if(has1<moya::STDKind>(name))
            changed |= set1<moya::STDKind>(name, f, val);
          else if(has1<SourceLang>(name))
            changed |= set1<SourceLang>(name, f, val);
          else if(has1<moya::Location>(name))
            changed |= set1<moya::Location>(name, f, val);
          else
            moya_error("Unsupported function attribute to restore: " << name);
        }
      }
    }
  }

  return changed;
}

char RestoreFunctionAttributesPass::ID = 0;

static const char* name = "moya-restore-function-attrs";
static const char* descr
    = "Restore the Moya attributes of functions without defintions";
static const bool cfg = true;
static const bool analysis = false;

// INITIALIZE_PASS(RestoreFunctionAttributesPass, name, descr, cfg, analysis)

static RegisterPass<RestoreFunctionAttributesPass>
    X(name, descr, cfg, analysis);

void registerRestoreFunctionAttributesPass(const PassManagerBuilder&,
                                           legacy::PassManagerBase& pm) {
  pm.add(new RestoreFunctionAttributesPass());
}

Pass* createRestoreFunctionAttributesPass() {
  return new RestoreFunctionAttributesPass();
}
