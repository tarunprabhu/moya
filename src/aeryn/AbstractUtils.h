#ifndef MOYA_AERYN_ABSTRACT_UTILS_H
#define MOYA_AERYN_ABSTRACT_UTILS_H

#include "common/AnalysisStatus.h"
#include "common/Content.h"
#include "common/Store.h"
#include "common/Vector.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/Instructions.h>

namespace moya {

class Environment;
class Models;

namespace AbstractUtils {

// These routines actually do the "allocation"
Contents allocate(const llvm::Instruction*,
                  Store&,
                  const llvm::Function*,
                  const CellFlags& = CellFlags());
const Address* allocate(llvm::Type*,
                        Store&,
                        const llvm::Instruction*,
                        const llvm::Function*,
                        const CellFlags& = CellFlags());

// These are utilities and models that might be common across libraries
AnalysisStatus malloc(const llvm::Instruction*,
                      Environment&,
                      Store&,
                      const llvm::Function*,
                      const CellFlags& = CellFlags());

AnalysisStatus
neew(const llvm::Instruction*, Environment&, Store&, const llvm::Function*);

AnalysisStatus
calloc(const llvm::Instruction*, Environment&, Store&, const llvm::Function*);

AnalysisStatus realloc(const llvm::Instruction*,
                       const Addresses&,
                       Environment&,
                       Store&,
                       const llvm::Function*);

AnalysisStatus free(const llvm::Instruction*,
                    const Contents&,
                    llvm::Type*,
                    Environment&,
                    Store&,
                    const llvm::Function*);

// This is visible because it is used to simulate
// a call-by-value when functions with pointer arguments have a byval
// attribute
AnalysisStatus memcpyImpl(const Address*,
                          const Address*,
                          long,
                          Store&,
                          const llvm::Instruction* = nullptr);

AnalysisStatus memcpy(const llvm::Instruction*,
                      const Vector<Contents>&,
                      Environment&,
                      Store&,
                      const llvm::Function*);

AnalysisStatus memset(const llvm::Instruction*,
                      const Vector<Contents>&,
                      Environment&,
                      Store&,
                      const llvm::Function*);

// The load and store and other processing is sometimes used by the models
// as well, particularly the atomic loads and stores
AnalysisStatus store(const llvm::Instruction*,
                     const Contents&,
                     llvm::Type*,
                     const Contents&,
                     llvm::Type*,
                     Environment&,
                     Store&,
                     const llvm::Function*);
AnalysisStatus load(const llvm::Instruction*,
                    const Contents&,
                    llvm::Type*,
                    Environment&,
                    Store&,
                    const llvm::Function*);

// Models which accept function pointers also are callsites because in most
// cases the function pointer being passed is actually derefenced and the
// function called
AnalysisStatus callFunction(const llvm::Instruction*,
                            const Set<const llvm::Function*>&,
                            const Vector<Contents>&,
                            const Vector<llvm::Type*>&,
                            const Models&,
                            Environment&,
                            Store&,
                            const llvm::Function*);

AnalysisStatus callModel(const llvm::Instruction*,
                         const llvm::Function*,
                         const Vector<Contents>&,
                         const Vector<llvm::Type*>&,
                         const Models&,
                         Environment&,
                         Store&,
                         const llvm::Function*);
} // namespace AbstractUtils

} // namespace moya

#endif // MOYA_AERYN_ABSTRACT_UTILS_H
