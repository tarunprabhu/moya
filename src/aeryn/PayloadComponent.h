#ifndef MOYA_AERYN_PAYLOAD_COMPONENT_H
#define MOYA_AERYN_PAYLOAD_COMPONENT_H

#include "common/PayloadBase.h"

class PayloadComponent : public PayloadBase {
public:
  PayloadComponent(const std::string&);
};

#endif // MOYA_AERYN_PAYLOAD_COMPONENT_H
