#ifndef MOYA_CACHE_MANAGER_WRAPPER_PASS_H
#define MOYA_CACHE_MANAGER_WRAPPER_PASS_H

#include "AnalysisCacheManager.h"
#include <llvm/Pass.h>

#include <memory>

class CacheManagerWrapperPass : public llvm::ModulePass {
public:
  static char ID;

private:
  std::unique_ptr<moya::AnalysisCacheManager> manager;

public:
  CacheManagerWrapperPass();

  moya::AnalysisCacheManager& getCacheManager();

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);
};

#endif // MOYA_CACHE_MANAGER_WRAPPER_PASS_H
