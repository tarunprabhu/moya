#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;

class NameStructsPass : public ModulePass {
public:
  static char ID;

public:
  NameStructsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

NameStructsPass::NameStructsPass() : ModulePass(ID) {
  ;
}

StringRef NameStructsPass::getPassName() const {
  return "Moya Name Structs";
}

void NameStructsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool NameStructsPass::runOnModule(Module& module) {
  bool changed = false;

  TypeFinder finder;
  finder.run(module, false);
  for(TypeFinder::iterator it = finder.begin(); it != finder.end(); it++) {
    StructType* sty = *it;
    if(not sty->hasName() and not isComplexTy(sty)) {
      // The only struct that we don't want to name is the literal struct
      // that represents complex numbers.
      std::stringstream ss;
      ss << ".moya.name." << reinterpret_cast<uintptr_t>(sty);
      sty->setName(ss.str());
      changed |= true;
    }
  }

  return changed;
}

char NameStructsPass::ID = 0;

static RegisterPass<NameStructsPass>
    X("moya-name-structs", "Names all anonymous structs", true, false);

Pass* createNameStructsPass() {
  return new NameStructsPass();
}
