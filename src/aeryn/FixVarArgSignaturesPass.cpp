#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Map.h"
#include "common/Set.h"
#include "common/Vector.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>

using namespace llvm;

class FixVarArgSignaturesPass : public ModulePass {
public:
  static char ID;

protected:
  FunctionType* inferFunctionType(Function&);
  bool normalizeUses(Function&, FunctionType*);

  bool replaceCasts(const moya::Vector<CastInst*>&, Function*);
  bool replaceCalls(const moya::Vector<CallInst*>&, Function*);

public:
  FixVarArgSignaturesPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

FixVarArgSignaturesPass::FixVarArgSignaturesPass() : ModulePass(ID) {
  ;
}

StringRef FixVarArgSignaturesPass::getPassName() const {
  return "Moya Fix Vararg Function Signatures";
}

void FixVarArgSignaturesPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

FunctionType* FixVarArgSignaturesPass::inferFunctionType(Function& f) {
  moya::Set<FunctionType*> ftys;
  for(Use& u : f.uses()) {
    if(auto* cst = dyn_cast<CastInst>(u.getUser())) {
      if(auto* pty = dyn_cast<PointerType>(cst->getDestTy()))
        if(auto* fty = dyn_cast<FunctionType>(pty->getElementType()))
          ftys.insert(fty);
    } else if(auto* call = dyn_cast<CallInst>(u.getUser())) {
      // The only way a vararg function with 0 arguments gets called directly
      // is if the function does not take any arguments anyway.
      ftys.insert(FunctionType::get(call->getType(), ArrayRef<Type*>(), false));
    } else {
      llvm::errs() << f.getName() << " => " << *(u.getUser()) << "\n";
    }
  }

  if(ftys.size() == 1)
    return *ftys.begin();
  llvm::errs() << "Could not infer functionType: " << f.getName() << "\n";
  for(FunctionType* fty : ftys)
    llvm::errs() << "  => " << *fty << "\n";
  return nullptr;
}

bool FixVarArgSignaturesPass::replaceCasts(const moya::Vector<CastInst*>& casts,
                                           Function* fn) {
  for(CastInst* cst : casts) {
    cst->replaceAllUsesWith(fn);
    cst->eraseFromParent();
  }

  return true;
}

bool FixVarArgSignaturesPass::replaceCalls(const moya::Vector<CallInst*>& calls,
                                           Function* fn) {
  for(CallInst* call : calls) {
    moya::Vector<Value*> args;
    for(Value* arg : call->arg_operands())
      args.push_back(arg);
    CallInst* newCall = CallInst::Create(fn, makeArrayRef(args), "", call);
    call->replaceAllUsesWith(newCall);
    call->eraseFromParent();
  }

  return true;
}

bool FixVarArgSignaturesPass::normalizeUses(Function& f, FunctionType* ftype) {
  Module& module = *f.getParent();

  moya::Vector<CastInst*> casts;
  moya::Vector<CallInst*> calls;
  for(Use& u : f.uses())
    if(CastInst* cst = dyn_cast<CastInst>(u.getUser()))
      casts.push_back(cst);
    else if(CallInst* call = dyn_cast<CallInst>(u.getUser()))
      calls.push_back(call);

  StringRef name = f.getName();
  f.setName(name + ".old");

  Function* newfn = cast<Function>(module.getOrInsertFunction(name, ftype));
  newfn->copyAttributesFrom(&f);

  replaceCasts(casts, newfn);
  replaceCalls(calls, newfn);

  f.eraseFromParent();

  return true;
}

bool FixVarArgSignaturesPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  // Find vararg functions where none of the arguments are fixed.
  // These are most likely going to be functions coming from some Fortran
  // library. By looking only for those vararg functions which do not have any
  // fixed arguments, we exclude vararg functions like printf and scanf which
  // is what we want to do
  moya::Map<Function*, FunctionType*> normalize;
  for(Function& f : module.functions())
    if(not f.size() and f.isVarArg() and getNumParams(f) == 0)
      if(FunctionType* ftype = inferFunctionType(f))
        normalize[&f] = ftype;

  for(auto& it : normalize)
    changed |= normalizeUses(*it.first, it.second);

  return changed;
}

char FixVarArgSignaturesPass::ID = 0;

static RegisterPass<FixVarArgSignaturesPass> X("moya-fix-vararg-signatures",
                                               "Finds vararg functions and "
                                               "tries to determine a signature "
                                               "for them so the analysis can "
                                               "do something useful with the.",
                                               true,
                                               false);

ModulePass* createFixVarArgSignaturesPass() {
  return new FixVarArgSignaturesPass();
}
