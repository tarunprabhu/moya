#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Map.h"
#include "common/Metadata.h"
#include "common/Set.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

#include <functional>

using namespace llvm;
using namespace std::placeholders;

// Removes all metadata nodes which have been invalidated or have redundant
// operands.
// The struct nodes may have redundant operands because the same struct could
// have been processed in several different modules which are linked. During
// the linking, the operands are duplicated.
// Nodes could be invalidate by dead code elimination passes which remove
// the global that the node refers to.
//
class CanonicalizeMetadataPass : public ModulePass {
public:
  static char ID;

public:
  CanonicalizeMetadataPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

CanonicalizeMetadataPass::CanonicalizeMetadataPass() : ModulePass(ID) {
  // initializeCanonicalizeMetadataPassPass(*PassRegistry::getPassRegistry());
}

StringRef CanonicalizeMetadataPass::getPassName() const {
  return "Moya Canonicalize Metadata";
}

void CanonicalizeMetadataPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesAll();
}

static llvm::Metadata* stripCastFromMetadata(llvm::Metadata* md,
                                             Module& module) {
  if(ConstantAsMetadata* cm = dyn_cast_or_null<ConstantAsMetadata>(md)) {
    if(ConstantExpr* cexpr = dyn_cast<ConstantExpr>(cm->getValue())) {
      Type* type = cast<PointerType>(cexpr->getType())->getElementType();
      return ConstantAsMetadata::get(UndefValue::get(type));
    }
  }
  return md;
}

static NamedMDNode*
replace(Module& module,
        const moya::Set<MDNode*>& ops,
        const std::function<NamedMDNode*(Module&)>& getOrInsert) {
  module.eraseNamedMetadata(getOrInsert(module));
  NamedMDNode* nmd = getOrInsert(module);
  for(MDNode* md : ops)
    nmd->addOperand(md);
  return nmd;
}

static bool
removeDuplicates(Module& module,
                 const std::function<NamedMDNode*(Module&)>& getOrInsert) {
  NamedMDNode* old = getOrInsert(module);

  moya::Set<MDNode*> ops;
  for(MDNode* md : old->operands())
    if(not ops.contains(md))
      ops.insert(md);

  if(ops.size() != old->getNumOperands()) {
    replace(module, ops, getOrInsert);
    return true;
  }

  return false;
}

// Some of the MDNodes have null instead of a function/global/type
// This probably happens because the value was removed from the module for
// whatever reason
template <typename T>
static bool
removeInvalid(Module& module,
              const std::function<NamedMDNode*(Module&)>& getOrInsert) {
  NamedMDNode* old = getOrInsert(module);

  moya::Set<MDNode*> ops;
  for(MDNode* node : old->operands())
    if(Metadata* md = node->getOperand(0).get())
      if(moya::Metadata::getAs<T*>(md))
        ops.insert(node);

  if(ops.size() != old->getNumOperands()) {
    replace(module, ops, getOrInsert);
    return true;
  }

  return false;
}

static bool removeDistinctMDNodes(Module& module) {
  bool changed = false;

  LLVMContext& context = module.getContext();
  for(Function& f : module.functions())
    for(auto i = inst_begin(f); i != inst_end(f); i++) {
      SmallVector<std::pair<unsigned, MDNode*>, 16> mds;
      i->getAllMetadataOtherThanDebugLoc(mds);
      for(auto& p : mds) {
        MDNode* md = p.second;
        if(md->isDistinct()) {
          moya::Vector<Metadata*> ops(md->op_begin(), md->op_end());
          i->setMetadata(
              p.first,
              MDNode::getIfExists(context, moya::LLVMUtils::makeArrayRef(ops)));
          changed |= true;
        }

        moya::Vector<llvm::Metadata*> ops;
        for(const MDOperand& op : md->operands())
          ops.push_back(stripCastFromMetadata(op.get(), module));
        i->setMetadata(
            p.first, MDNode::get(context, moya::LLVMUtils::makeArrayRef(ops)));
      }
    }

  return changed;
}

template <typename T>
static bool
mergeAttributes(Module& module,
                const std::function<NamedMDNode*(Module&)>& getOrInsert) {
  moya::Set<T*> replace;
  moya::Map<T*, moya::Set<Metadata*>> ops;
  LLVMContext& context = module.getContext();

  // The attributes are always the last element in each MDNode
  NamedMDNode* nmd = getOrInsert(module);
  for(MDNode* md : nmd->operands()) {
    if(auto* t = moya::Metadata::getAs<T*>(md->getOperand(0))) {
      MDNode* attrs = cast<MDNode>(md->getOperand(md->getNumOperands() - 1));
      if(not ops.emplace(t, moya::Set<Metadata*>()))
        replace.insert(t);
      for(const MDOperand& attr : attrs->operands())
        ops.at(t).insert(attr);
    }
  }

  for(MDNode* md : nmd->operands()) {
    if(auto* t = moya::Metadata::getAs<T*>(md->getOperand(0))) {
      if(replace.contains(t)) {
        moya::Vector<Metadata*> args(ops.at(t).begin(), ops.at(t).end());
        md->replaceOperandWith(
            md->getNumOperands() - 1,
            MDNode::get(context, moya::LLVMUtils::makeArrayRef(args)));
      }
    }
  }

  return replace.size();
}

static bool canonicalizeTypesNode(Module& module) {
  bool changed = false;

  auto getOrInsert = std::bind<NamedMDNode*(Module&)>(
      moya::Metadata::getOrInsertTypesNode, _1);

  changed |= removeDuplicates(module, getOrInsert);
  changed |= removeInvalid<StructType>(module, getOrInsert);
  changed |= mergeAttributes<StructType>(module, getOrInsert);

  return changed;
}

static bool canonicalizeGlobalsNode(Module& module) {
  bool changed = false;

  auto getOrInsert = std::bind<NamedMDNode*(Module&)>(
      moya::Metadata::getOrInsertGlobalsNode, _1);

  changed |= removeDuplicates(module, getOrInsert);
  changed |= removeInvalid<GlobalVariable>(module, getOrInsert);
  changed |= mergeAttributes<GlobalVariable>(module, getOrInsert);

  return changed;
}

static bool canonicalizeFunctionsNode(Module& module) {
  bool changed = false;

  auto getOrInsert = std::bind<NamedMDNode*(Module&)>(
      moya::Metadata::getOrInsertFunctionsNode, _1);

  changed |= removeDuplicates(module, getOrInsert);
  changed |= removeInvalid<Function>(module, getOrInsert);
  changed |= mergeAttributes<Function>(module, getOrInsert);

  return changed;
}

static bool canonicalizeBackupAttributesNode(Module& module) {
  bool changed = false;

  auto getOrInsert = std::bind<NamedMDNode*(Module&)>(
      moya::Metadata::getOrInsertBackupFunctionAttributesNode, _1);

  changed |= removeDuplicates(module, getOrInsert);
  changed |= removeInvalid<Function>(module, getOrInsert);

  return changed;
}

static bool canonicalizeRegionsNode(Module& module) {
  bool changed = false;

  auto getOrInsert = std::bind<NamedMDNode*(Module&)>(
      moya::Metadata::getOrInsertRegionsNode, _1);

  changed |= removeDuplicates(module, getOrInsert);

  return changed;
}

bool CanonicalizeMetadataPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  changed |= removeDistinctMDNodes(module);
  changed |= canonicalizeTypesNode(module);
  changed |= canonicalizeFunctionsNode(module);
  changed |= canonicalizeGlobalsNode(module);
  changed |= canonicalizeBackupAttributesNode(module);
  changed |= canonicalizeRegionsNode(module);

  return changed;
}

char CanonicalizeMetadataPass::ID = 0;

static const char* name = "moya-canonicalize-metadata";
static const char* descr = "Removes any invalid metadata";
static const bool cfg = true;
static const bool analysis = true;

// INITIALIZE_PASS(CanonicalizeMetadataPass, name, descr, cfg, analysis)

static RegisterPass<CanonicalizeMetadataPass> X(name, descr, cfg, analysis);

Pass* createCanonicalizeMetadataPass() {
  return new CanonicalizeMetadataPass();
}
