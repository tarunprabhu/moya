#ifndef MOYA_AERYN_SUMMARY_WRAPPER_PASS_H
#define MOYA_AERYN_SUMMARY_WRAPPER_PASS_H

#include "common/APITypes.h"
#include "common/Map.h"

#include <llvm/Pass.h>

namespace moya {

class CallGraph;
class Environment;
class Region;
class Store;
class Summary;

} // namespace moya

class SummaryWrapperPass : public llvm::ModulePass {
protected:
  moya::Map<RegionID, moya::Map<JITID, std::unique_ptr<moya::Summary>>>
      summaries;

protected:
  moya::Summary& addSummary(llvm::Function&,
                            const moya::Region&,
                            bool,
                            const moya::CallGraph&,
                            const moya::Environment&,
                            const moya::Store&);

public:
  SummaryWrapperPass();

  const moya::Summary& getSummary(RegionID, JITID) const;

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);

public:
  static char ID;
};

#endif // MOYA_AERYN_SUMMARY_WRAPPER_PASS_H
