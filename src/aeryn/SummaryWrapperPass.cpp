#include "SummaryWrapperPass.h"
#include "AnalysisCacheManager.h"
#include "CacheManagerWrapperPass.h"
#include "CallGraphWrapperPass.h"
#include "Mutability.h"
#include "MutabilityWrapperPass.h"
#include "Passes.h"
#include "Summary.h"
#include "common/Diagnostics.h"
#include "common/Environment.h"
#include "common/Metadata.h"
#include "common/Store.h"

using namespace llvm;

SummaryWrapperPass::SummaryWrapperPass() : ModulePass(ID) {
  // initializeSummaryWrapperPassPass(*PassRegistry::getPassRegistry());
}

StringRef SummaryWrapperPass::getPassName() const {
  return "Moya Summary Wrapper";
}

void SummaryWrapperPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
  AU.addRequired<MutabilityWrapperPass>();
  AU.addRequired<CallGraphWrapperPass>();
  AU.addRequired<CacheManagerWrapperPass>();
}

const moya::Summary& SummaryWrapperPass::getSummary(RegionID region,
                                                    JITID id) const {
  return *summaries.at(region).at(id).get();
}

moya::Summary& SummaryWrapperPass::addSummary(Function& f,
                                              const moya::Region& region,
                                              bool top,
                                              const moya::CallGraph& cg,
                                              const moya::Environment& env,
                                              const moya::Store& store) {
  RegionID regionID = region.getID();
  JITID jitID = moya::Metadata::getJITID(f);
  moya::Summary* summary = new moya::Summary(f, region, top, cg, env, store);

  summaries[regionID][jitID].reset(summary);

  return *summary;
}

bool SummaryWrapperPass::runOnModule(Module& module) {
  moya_message(getPassName());

  const moya::Mutability& mutability
      = getAnalysis<MutabilityWrapperPass>().getMutability();
  moya::AnalysisCacheManager& cacheMgr
      = getAnalysis<CacheManagerWrapperPass>().getCacheManager();

  bool top = mutability.isTop();
  const moya::Environment& env = mutability.getEnvironment();
  const moya::Store& store = mutability.getStore();

  for(const moya::Region& region : moya::Metadata::getRegions(module)) {
    const moya::CallGraph& cg
        = getAnalysis<CallGraphWrapperPass>().getCallGraph(region.getID());

    for(Function& f : module.functions()) {
      if(moya::Metadata::hasSpecialize(f)) {
        moya_message("Summarizing function: " << f.getName());

        moya::Summary& summary = addSummary(f, region, top, cg, env, store);
        if(cacheMgr.isEnabled()) {
          summary.serialize(cacheMgr.getSummarySerializer(summary));
          cacheMgr.writeSummary(summary);
        }
      }
    }
  }

  return false;
}

char SummaryWrapperPass::ID = 0;

static const char* name = "moya-summarize";
static const char* descr = "Computes a summary for the specified region";
static const bool cfg = true;
static const bool analysis = true;

// INITIALIZE_PASS_BEGIN(SummaryWrapperPass, name, descr, cfg, analysis)
// INITIALIZE_PASS_DEPENDENCY(MutabilityWrapperPass)
// INITIALIZE_PASS_DEPENDENCY(CacheManagerWrapperPass)
// INITIALIZE_PASS_DEPENDENCY(CallGraphWrapperPass)
// INITIALIZE_PASS_END(SummaryWrapperPass, name, descr, cfg, analysis)

static RegisterPass<SummaryWrapperPass> X(name, descr, cfg, analysis);

Pass* createSummaryWrapperPass() {
  return new SummaryWrapperPass();
}
