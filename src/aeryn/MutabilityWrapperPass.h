#ifndef MOYA_AERYN_MUTABILITY_WRAPPER_PASS_H
#define MOYA_AERYN_MUTABILITY_WRAPPER_PASS_H

#include <llvm/Pass.h>

namespace moya {

class Mutability;

} // namespace moya

class MutabilityWrapperPass : public llvm::ModulePass {
public:
  static char ID;

protected:
  std::unique_ptr<moya::Mutability> mutability;

public:
  MutabilityWrapperPass();

  const moya::Mutability& getMutability() const;

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);
};

#endif // MOYA_AERYN_MUTABILITY_WRAPPER_PASS_H
