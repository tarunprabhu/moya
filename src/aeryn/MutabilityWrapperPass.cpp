#include "MutabilityWrapperPass.h"
#include "CacheManagerWrapperPass.h"
#include "ClassHeirarchiesWrapperPass.h"
#include "Mutability.h"
#include "Passes.h"
#include "common/CallGraph.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/Encoder.h"
#include "common/JSONUtils.h"
#include "common/Metadata.h"
#include "common/Region.h"
#include "common/Serializer.h"
#include "common/Stringify.h"
#include "common/Vector.h"

using namespace llvm;

MutabilityWrapperPass::MutabilityWrapperPass()
    : ModulePass(ID), mutability(nullptr) {
  // initializeMutabilityWrapperPassPass(*PassRegistry::getPassRegistry());
}

StringRef MutabilityWrapperPass::getPassName() const {
  return "Moya Mutability Wrapper";
}

void MutabilityWrapperPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesAll();
  AU.addRequired<CacheManagerWrapperPass>();
  AU.addRequired<ClassHeirarchiesWrapperPass>();
}

const moya::Mutability& MutabilityWrapperPass::getMutability() const {
  return *mutability.get();
}

bool MutabilityWrapperPass::runOnModule(Module& module) {
  moya_message(getPassName());

  moya::AnalysisCacheManager& cache
      = getAnalysis<CacheManagerWrapperPass>().getCacheManager();
  const moya::ClassHeirarchies& heirarchies
      = getAnalysis<ClassHeirarchiesWrapperPass>().getHeirarchies();

  mutability.reset(new moya::Mutability(module, heirarchies));
  mutability->runOnModule();

  const moya::Environment& env = mutability->getEnvironment();
  const moya::Store& store = mutability->getStore();

  // The cache manager will check whether or not the user actually wants
  // to write things out to cache.
  if(cache.isEnabled() and not mutability->isTop()) {
    // cacheMgr.writeStore(store);
    // cacheMgr.writeEnvironment(env);

    // heirarchies.serialize(cacheMgr.getClassHeirarchiesSerializer());
    // heirarchies.dotify(cacheMgr.getClassHeirarchiesDotifier());

    moya::Serializer& s = cache.getCompleteSerializer();
    s.mapStart();

    s.add("name", cache.getPrefix());
    s.add("analyzed", cache.getPath());
    s.add("sha512", cache.getSignature());
    s.add("created", cache.getTime());
    s.add("compression", "GZ");
    s.serialize(store);

    moya::Vector<moya::Region> regions = moya::Metadata::getRegions(module);
    moya::Vector<RegionID> ids = {moya::Config::getInvalidRegionID()};
    for(const moya::Region& region : moya::Metadata::getRegions(module))
      ids.push_back(region.getID());

    s.mapStart("regions");
    for(const moya::Region& region : regions) {
      s.mapStart(moya::str(region.getID()));
      s.add("_class", "Region");
      s.add("name", region.getName());
      // s.add("location", region.getLocation());
      // s.add("begin", region.getBeginLoc());
      // s.add("end", region.getEndLoc());

      moya::CallGraph cg(region, module);
      if(cg.canConstruct())
        s.add("callgraph", cg.construct(env, store));
      s.mapEnd(moya::str(region.getID()));
    }
    s.mapEnd("regions");

    s.add("callgraph", moya::CallGraph(module).construct(env, store));

    s.mapEnd(); // all
  }

  return false;
}

char MutabilityWrapperPass::ID = 0;

static const char* name = "moya-mutability-analysis";
static const char* descr = "Moya's main mutability analysis";
static const bool cfg = true;
static const bool analysis = true;

// INITIALIZE_PASS_BEGIN(MutabilityWrapperPass, name, descr, cfg, analysis)
// INITIALIZE_PASS_DEPENDENCY(CacheManagerWrapperPass)
// INITIALIZE_PASS_DEPENDENCY(ClassHeirarchiesWrapperPass)
// INITIALIZE_PASS_END(MutabilityWrapperPass, name, descr, cfg, analysis)

static RegisterPass<MutabilityWrapperPass> X(name, descr, cfg, analysis);

Pass* createMutabilityWrapperPass() {
  return new MutabilityWrapperPass();
}
