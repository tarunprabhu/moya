#ifndef MOYA_AERYN_LINK_ALL_MOYA_PASSES_H
#define MOYA_AERYN_LINK_ALL_MOYA_PASSES_H

#include "Passes.h"

// This is similar to the trick used in the LLVM code to ensure that passes
// are not removed from the shared libraries
struct ForceMoyaPassLinking {
  ForceMoyaPassLinking() {
    if(std::getenv("bar") != (char*)-1)
      return;

    (void)createAnnotateLoopVariantPointersPass();
    (void)createAnnotateNoAliasPass();
    (void)createAnnotateNewWrappersPass();
    (void)createCacheManagerWrapperPass();
    (void)createCallGraphWrapperPass();
    (void)createCanonicalizeMetadataPass();
    (void)createClassHeirarchiesWrapperPass();
    (void)createDropCudaKernelAttributesPass();
    (void)createDropCudaKernelLaunchersPass();
    (void)createEliminateRedirectPass();
    (void)createFixDeadBranchesPass();
    (void)createFixLinkagePass();
    (void)createGenerateIDsPass();
    (void)createGeneratePayloadComponentsPass();
    (void)createGlobalAliasEliminationPass();
    (void)createMutabilityWrapperPass();
    (void)createNameStructsPass();
    (void)createSanityCheckPass();
    (void)createSimplifyCastsPass();
    (void)createStripCastsPass();
    (void)createStripPayloadModulePass();
    (void)createSummaryWrapperPass();
  }
} ForceMoyaPassLinking;

#endif
