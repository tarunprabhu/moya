#include "common/ContainerUtils.h"
#include "common/Metadata.h"
#include "common/StringUtils.h"

#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/Utils/Cloning.h>

using namespace llvm;

// Cuda kernels are executed on the GPU and are identified with the __global__
// tag. When these are compiled, a host and device version of the function
// are created. The host version only sets up the arguments and launches the
// kernel on the device.
// For Aeryn's analysis to work, we actually need the device code because
// that is where the work is done and we need to know what happens to the
// parameters passed to the kernels. Since both the host and the device code
// have the same name, we need to drop the bodies of the host function so
// we can link in the device code when doing the analysis
// This function drops the bodies of the host functions whose names are
// passed in as a semicolon separated std::string on the command line
class DropCudaKernelLaunchersPass : public ModulePass {
public:
  static char ID;

public:
  DropCudaKernelLaunchersPass() : ModulePass(ID) {
    ;
  }

  virtual void getAnalysisUsage(AnalysisUsage& AU) const {
    AU.setPreservesCFG();
  }

  virtual bool runOnModule(Module& module) {
    for(Function& f : module.functions())
      if(moya::Metadata::hasCudaKernelHost(f))
        f.deleteBody();

    return true;
  }
};

char DropCudaKernelLaunchersPass::ID = 0;

static RegisterPass<DropCudaKernelLaunchersPass>
    X("moya-drop-cuda-kernel-launchers",
      "Drop the host code corresponding to the Cuda kernels",
      true,
      false);

Pass* createDropCudaKernelLaunchersPass() {
  return new DropCudaKernelLaunchersPass();
}
