#ifndef MOYA_AERYN_CLASS_HEIRARCHIES_WRAPPER_PASS_H
#define MOYA_AERYN_CLASS_HEIRARCHIES_WRAPPER_PASS_H

#include "common/ClassHeirarchies.h"

#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

class ClassHeirarchiesWrapperPass : public llvm::ModulePass {
public:
  static char ID;

protected:
  moya::ClassHeirarchies heirarchies;

public:
  ClassHeirarchiesWrapperPass();

  const moya::ClassHeirarchies& getHeirarchies() const;

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);
};

#endif // MOYA_AERYN_CLASS_HEIRARCHIES_WRAPPER_PASS_H
