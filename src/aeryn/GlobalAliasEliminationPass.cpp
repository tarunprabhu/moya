#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/Vector.h"

#include <llvm/IR/Module.h>

using namespace llvm;

// This is a very simple pass that eliminates global aliases from the module
// The global aliases are simply alternate names for functions but they cause
// enough problems that they really need to go
//
class GlobalAliasEliminationPass : public ModulePass {
public:
  GlobalAliasEliminationPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage& AU) const;
  virtual bool runOnModule(Module&);

public:
  static char ID;
};

GlobalAliasEliminationPass::GlobalAliasEliminationPass() : ModulePass(ID) {
  // initializeGlobalAliasEliminationPassPass(*PassRegistry::getPassRegistry());
}

StringRef GlobalAliasEliminationPass::getPassName() const {
  return "Moya Eliminate Global Alias";
}

void GlobalAliasEliminationPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool GlobalAliasEliminationPass::runOnModule(Module& module) {
  moya_message(getPassName());

  moya::Vector<GlobalAlias*> gas;
  for(GlobalAlias& g : module.aliases())
    gas.push_back(&g);

  for(GlobalAlias* g : gas) {
    g->replaceAllUsesWith(g->getAliasee());
    if(g->use_empty())
      g->eraseFromParent();
  }

  for(GlobalAlias& g : module.aliases())
    moya_warning("Could not erase alias: " << g);

  return gas.size();
}

char GlobalAliasEliminationPass::ID = 0;

static const char* name = "moya-eliminate-global-alias";
static const char* descr = "Removes global aliases";
static const bool cfg = false;
static const bool analysis = false;

// INITIALIZE_PASS(GlobalAliasEliminationPass, name, descr, cfg, analysis)

static RegisterPass<GlobalAliasEliminationPass> X(name, descr, cfg, analysis);

Pass* createGlobalAliasEliminationPass() {
  return new GlobalAliasEliminationPass();
}
