#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>

using namespace llvm;

class IdentifySummarizableFunctionsPass : public ModulePass {
public:
  static char ID;

public:
  IdentifySummarizableFunctionsPass() : ModulePass(ID) {
    ;
  }

  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

static bool isSelfContained(Type* ty) {
  if(StructType* sty = dyn_cast<StructType>(ty)) {
    // This is a weak test for whether or not the struct is a class, but for
    // the moment, it should do
    if(sty->hasName() and sty->getName().find("class") == 0)
      return false;
    for(Type* ety : sty->elements())
      if(ety->isPointerTy())
        return false;
      else if(StructType* tty = dyn_cast<StructType>(ety))
        if(not isSelfContained(tty))
          return false;
    return true;
  } else if(PointerType* pty = dyn_cast<PointerType>(ty)) {
    Type* ety = pty->getElementType();
    if(StructType* sty = dyn_cast<StructType>(ety)) {
      if(not isSelfContained(sty))
        return false;
    } else if(isa<PointerType>(ety) or isa<FunctionType>(ety)) {
      return false;
    } else if(ArrayType* aty = dyn_cast<ArrayType>(ety)) {
      if(not isSelfContained(aty->getElementType()))
        return false;
    }
  } else if(ArrayType* aty = dyn_cast<ArrayType>(ty)) {
    if(not isSelfContained(aty->getElementType()))
      return false;
  } else if(isa<FunctionType>(ty)) {
    return false;
  }
  return true;
}

static bool areArgumentsSelfContained(Function& f) {
  for(Argument& arg : f.args())
    if(not isSelfContained(arg.getType()))
      return false;
  return true;
}

static bool accessesNonConstantGlobals(Function& f) {
  for(auto i = inst_begin(f); i != inst_end(f); i++)
    for(Use& op : i->operands())
      if(GlobalVariable* g = dyn_cast<GlobalVariable>(op.get()))
        if(not g->isConstant())
          return true;
  return false;
}

void IdentifySummarizableFunctionsPass::getAnalysisUsage(
    AnalysisUsage& AU) const {
  ;
}

bool IdentifySummarizableFunctionsPass::runOnModule(Module& module) {
  bool changed = false;

  // Summarizable functions are those which satisfy ALL of the following
  // conditions:
  //
  // - Any arguments passed by pointer must be single-level pointers only
  //   i.e. type* is allowed, but type** is not
  // - All structs passed by value or by pointer to the function must be
  //   "self-contained" structs. A struct is said to be self-contained if all
  //   of the fields of the struct are either primitive types or
  //   self-contained structs.
  // - None of the arguments to the function may be function pointers
  // - None of the arguments to the function may be classes
  // - If the function returns a pointer, it must be an allocator function or
  //   the pointer being returned must be one of the arguments
  // - The function does not access any non-constant global variables.
  //   TODO: This condition can be relaxed. We only need to ensure that global
  //   variables are not modified by the function, but that we can hold off
  //   on that until later.

  for(Function& f : module.functions()) {
    if(areArgumentsSelfContained(f) and not isa<PointerType>(f.getReturnType())
       and not accessesNonConstantGlobals(f)) {
      errs() << "Summarizable: " << f.getName() << "\n";
    }
  }

  return changed;
}

char IdentifySummarizableFunctionsPass::ID = 0;
static RegisterPass<IdentifySummarizableFunctionsPass>
    X("moya-identify-summarizable-functions",
      "Identifies functions whose behavior can be summarized",
      true,
      true);

Pass* createIdentifySummarizableFunctionsPass() {
  return new IdentifySummarizableFunctionsPass();
}
