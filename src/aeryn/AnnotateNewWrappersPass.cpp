#include "Passes.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Map.h"
#include "common/Metadata.h"
#include "common/Set.h"
#include "common/Trace.h"
#include "common/Vector.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class AnnotateNewWrappersPass : public ModulePass {
private:
  bool hasNew(Value*);
  bool
  areAllReturnedValuesNewed(const moya::Vector<std::unique_ptr<moya::Trace>>&);
  bool areAllReturnedValuesNewed(const moya::Vector<Value*>&&);
  moya::Vector<Value*> getReturnedValues(Function&);

public:
  AnnotateNewWrappersPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);

public:
  static char ID;
};

AnnotateNewWrappersPass::AnnotateNewWrappersPass() : ModulePass(ID) {
  // initializeAnnotateNewWrappersPassPass(*PassRegistry::getPassRegistry());
}

StringRef AnnotateNewWrappersPass::getPassName() const {
  return "Moya Annotate new() Wrappers";
}

void AnnotateNewWrappersPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool AnnotateNewWrappersPass::hasNew(Value* v) {
  if(auto* call = dyn_cast<CallInst>(v)) {
    if(Function* f = moya::LLVMUtils::getCalledFunction(call))
      if(moya::Metadata::hasSystemNew(f) or moya::Metadata::hasUserNew(f))
        return true;
  } else if(auto* invoke = dyn_cast<InvokeInst>(v)) {
    if(Function* f = moya::LLVMUtils::getCalledFunction(invoke))
      if(moya::Metadata::hasSystemNew(f) or moya::Metadata::hasUserNew(f))
        return true;
  } else if(auto* f = dyn_cast<Function>(v)) {
    if(moya::Metadata::hasSystemNew(f) or moya::Metadata::hasUserNew(f))
      return true;
  }
  return false;
}

moya::Vector<Value*> AnnotateNewWrappersPass::getReturnedValues(Function& f) {
  moya::Vector<Value*> ret;
  for(BasicBlock& bb : f.getBasicBlockList())
    if(auto* r = dyn_cast<ReturnInst>(&bb.back()))
      ret.push_back(r);
  return ret;
}

bool AnnotateNewWrappersPass::areAllReturnedValuesNewed(
    const moya::Vector<Value*>&& fronts) {
  if(fronts.size() == 1) {
    if(hasNew(fronts[0]))
      return true;
  } else {
    for(Value* front : fronts)
      if(not(hasNew(front) or isa<ConstantPointerNull>(front)))
        return false;
    return true;
  }
  return false;
}

bool AnnotateNewWrappersPass::areAllReturnedValuesNewed(
    const moya::Vector<std::unique_ptr<moya::Trace>>& traces) {
  bool allNewed = true;

  // If all the returned values originate from a new,
  // then it is likely a new wrapper. If the return type is not an i8*,
  // then it is a generator for objects of a specific type.
  for(const std::unique_ptr<moya::Trace>& trace : traces)
    if(areAllReturnedValuesNewed(trace->fronts()))
      allNewed &= true;

  return allNewed;
}

// This pass only identifies functions which wrap around new and new[]
// We do this to reduce the number of aliases if a generator function is called
// from different callsites
bool AnnotateNewWrappersPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  moya::Map<Function*, moya::Vector<std::unique_ptr<moya::Trace>>> returns;
  for(Function& f : module.functions())
    if(moya::Metadata::hasSourceLang(f)
       and isCXX(moya::Metadata::getSourceLang(f))
       and (not moya::Metadata::hasNoAnalyze(f)))
      if(f.getReturnType()->isPointerTy())
        for(Value* ret : getReturnedValues(f))
          returns[&f].emplace_back(new moya::Trace(ret));

  // If the wrapper is deep, we will uncover more newed functions if we
  // keep iterating
  bool converged = false;
  while(not converged) {
    bool wrapper = false;

    for(auto& it : returns)
      if(Function* f = it.first)
        if(areAllReturnedValuesNewed(it.second))
          if(not moya::Metadata::hasUserNew(f))
            wrapper |= moya::Metadata::setUserNew(f);

    changed |= wrapper;
    converged = not wrapper;
  }

  // We just mark all the values as being returned values and leave it to
  // later passes to use this information as they like
  for(auto& it : returns)
    if(moya::Metadata::hasUserNew(it.first))
      for(std::unique_ptr<moya::Trace>& trace : it.second)
        for(Value* front : trace->fronts())
          if(auto* inst = dyn_cast<Instruction>(front))
            changed |= moya::Metadata::setReturn(inst, nullptr);

  return changed;
}

char AnnotateNewWrappersPass::ID = 0;

static const char* name = "moya-annotate-new-wrappers";
static const char* descr = "Identifies new-like functions";
static const bool cfg = true;
static const bool analysis = true;

// INITIALIZE_PASS(AnnotateNewWrappersPass, name, descr, cfg, analysis)

static RegisterPass<AnnotateNewWrappersPass> X(name, descr, cfg, analysis);

Pass* createAnnotateNewWrappersPass() {
  return new AnnotateNewWrappersPass();
}
