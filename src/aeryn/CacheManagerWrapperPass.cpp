#include "CacheManagerWrapperPass.h"
#include "Passes.h"
#include "common/Diagnostics.h"

using namespace llvm;

static cl::opt<std::string> analyzedPrefix("moya-analyzed-prefix",
                                           cl::desc("Prefix for cached files"),
                                           cl::value_desc("exe"),
                                           cl::init("a.out"));

static cl::opt<std::string> analyzedSig("moya-analyzed-sig",
                                        cl::desc("Signature for this analysis"),
                                        cl::value_desc("sig"),
                                        cl::init("0"));

static cl::opt<std::string>
    analyzedTime("moya-analyzed-time",
                 cl::desc("Modification time for this analysis"),
                 cl::value_desc("time"),
                 cl::init("00:00:00--0000:00:00"));

static cl::opt<std::string>
    analyzedPath("moya-analyzed-path",
                 cl::desc("Path to the analyzed executable/library"),
                 cl::value_desc("path"),
                 cl::init(""));

CacheManagerWrapperPass::CacheManagerWrapperPass()
    : ModulePass(ID), manager(nullptr) {
  // initializeCacheManagerWrapperPassPass(*PassRegistry::getPassRegistry());
}

StringRef CacheManagerWrapperPass::getPassName() const {
  return "Moya Cache Manager Wrapper";
}

void CacheManagerWrapperPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesAll();
}

moya::AnalysisCacheManager& CacheManagerWrapperPass::getCacheManager() {
  return *manager.get();
}

bool CacheManagerWrapperPass::runOnModule(Module&) {
  moya_message(getPassName());

  manager.reset(new moya::AnalysisCacheManager(
      analyzedPrefix, analyzedSig, analyzedTime, analyzedPath));

  return false;
}

char CacheManagerWrapperPass::ID = 0;

static const char* name = "moya-cache-manager";
static const char* descr
    = "Sets up an object to manage the cache. The cache will contain the "
      "output of the analysis and may also contain outputs of previous "
      "invocations of the analysis which may be used";
static const bool cfg = true;
static const bool analysis = true;

// INITIALIZE_PASS(CacheManagerWrapperPass, name, descr, cfg, analysis)

static RegisterPass<CacheManagerWrapperPass> X(name, descr, cfg, analysis);

Pass* createCacheManagerWrapperPass() {
  return new CacheManagerWrapperPass();
}
