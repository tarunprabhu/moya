#ifndef MOYA_AERYN_SUMMARY_CELL_H
#define MOYA_AERYN_SUMMARY_CELL_H

#include "common/Set.h"
#include "common/SummaryCellBase.h"

#include <llvm/IR/Type.h>

namespace moya {

class Payload;
class Serializer;
class StoreCell;

class SummaryCell : public SummaryCellBase {
public:
  using Address = SummaryCellBase::Address;
  using Funcs = moya::Set<MoyaID>;

protected:
  llvm::Type* type;

  Funcs readers;
  Funcs writers;
  Funcs allocators;
  Funcs deallocators;
  Contents contents;

public:
  SummaryCell(Address, llvm::Type*, Status = Status::getUnused());
  virtual ~SummaryCell() = default;

  void addTarget(Address);
  void merge(const StoreCell&);

  llvm::Type* getType() const;
  Funcs getReaders() const;
  Funcs getWriters() const;
  Funcs getAllocators() const;
  Funcs getDeallocators() const;

  void updateStatus(const Status&);
  void setStatus(const Status&);
  void setStatusConstant();
  void setStatusDerefInKey();
  void setStatusUsedInKey();

  std::string str(unsigned = 0) const;
  void serialize(Serializer&) const;
  void pickle(Payload&) const;
};

} // namespace moya

#endif // MOYA_AERYN_SUMMARY_CELL_H
