#include "common/AerynConfig.h"
#include "common/DemanglerCXX.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Map.h"
#include "common/Metadata.h"
#include "common/StringUtils.h"

#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class SetModelNamesPass : public ModulePass {
public:
  static char ID;

public:
  SetModelNamesPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

SetModelNamesPass::SetModelNamesPass() : ModulePass(ID) {
  ;
}

StringRef SetModelNamesPass::getPassName() const {
  return "Moya Set Model Names";
}

void SetModelNamesPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool SetModelNamesPass::runOnModule(Module& module) {
  moya_message(getPassName());
  bool changed = false;

  // DemanglerCXX demangler;
  // for(Function& f : module.functions()) {
  //   if(not f.size() and not moya::Metadata::hasModel(f)) {
  //     if(isPureFunction(f))
  //       changed
  //           |= moya::Metadata::setModel(f,
  //           AerynConfig::getPureFunctionModel());
  //     else
  //       changed
  //           |= moya::Metadata::setModel(f,
  //           demangler.getModelName(f.getName()));
  //   }
  // }

  // FIXME: This is a temporary hack. Since I am hacking everything by
  // rewriting functions in C, to deal with arrays, there is a small template
  // library that wraps around Flang's arrays. But the array lookup operators
  // are functions and it ends up causing spurious aliases in the analysis.
  // Inlining them doesn't seem to work correctly, so we'll just cheat and
  // model our way out of it
  moya_warning("\n"
               " ********************************************************\n"
               " *                                                      *\n"
               " *            FL2C and Overkit hacks in use             *\n"
               " *                                                      *\n"
               " ********************************************************\n");
  for(Function& f : module.functions()) {
    if(moya::Metadata::hasQualifiedName(f)
       and moya::StringUtils::startswith(moya::Metadata::getQualifiedName(f),
                                         "fl2c::")) {
      // changed |= moya::Metadata::setModel(f, funcs.at(f.getName().str()));
      changed |= moya::Metadata::setNoAnalyze(f);
      f.removeFnAttr(Attribute::AttrKind::NoInline);
      f.removeFnAttr(Attribute::AttrKind::OptimizeNone);
      f.addFnAttr(Attribute::AttrKind::AlwaysInline);
    } else if(moya::Metadata::hasQualifiedName(f)
              and (moya::StringUtils::startswith(
                                                 moya::Metadata::getQualifiedName(f), "ovk::"))) {
      changed |= moya::Metadata::setModel(
          f, moya::AerynConfig::getPureFunctionModel());
    } else if(f.getName().startswith("ovk")) {
      changed |= moya::Metadata::setModel(
          f, moya::AerynConfig::getPureFunctionModel());
    } else if(f.getName().startswith("cblas_d")) {
      changed |= moya::Metadata::setModel(f, moya::AerynConfig::getPureFunctionModel());
    }
  }

  return changed;
}

char SetModelNamesPass::ID = 0;

static RegisterPass<SetModelNamesPass>
    X("moya-set-model-names",
      "Sets the model name for all library functions",
      true,
      true);

Pass* createSetModelNamesPass() {
  return new SetModelNamesPass();
}
