#include "AbstractUtils.h"
#include "common/AerynConfig.h"
#include "common/Diagnostics.h"
#include "common/Environment.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/Store.h"
#include "common/StoreCell.h"
#include "models/Models.h"

using namespace llvm;

namespace moya {

namespace AbstractUtils {

const Address* allocate(Type* type,
                        Store& store,
                        const Instruction* call,
                        const Function* caller,
                        const CellFlags& flg) {
  CellFlags flags(flg);
  flags.set(CellFlags::Heap);
  return store.allocate(type, flags, true, call, caller);
}

Contents allocate(const Instruction* call,
                  Store& store,
                  const Function* caller,
                  const CellFlags& flags) {
  Contents addrs;

  if(Type* allocatedPtr = Metadata::getAnalysisType(call)) {
    Type* allocated = dyn_cast<PointerType>(allocatedPtr)->getElementType();
    addrs.insert(allocate(allocated, store, call, caller, flags));
  }

  return addrs;
}

AnalysisStatus malloc(const Instruction* call,
                      Environment& env,
                      Store& store,
                      const Function* caller,
                      const CellFlags& flags) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  // If malloc is called by a function which is a malloc wrapper, then we
  // create a different object at each callsite where the wrapper is called
  // Those objects should then be used whereever the result of malloc is
  // used in this function.
  // There may be more than one malloc inside a function which acts as a
  // malloc. In that case, we should only treat the malloc whose return value
  // is returned by the function as a special case. The remaining mallocs in
  // the function should be treated normally
  if((Metadata::hasSystemMalloc(caller) or Metadata::hasUserMalloc(caller))
     and Metadata::hasReturn(call)) {
    for(const Instruction* callsite : env.getCallSites(caller)) {
      if(not env.contains(callsite))
        changed |= env.add(callsite, allocate(callsite, store, caller, flags));
      changed |= env.add(call, env.lookup(callsite));
    }
  } else {
    if(not env.contains(call))
      changed |= env.add(call, allocate(call, store, caller, flags));
  }
  env.push(env.lookup(call));
  return changed;
}

AnalysisStatus neew(const Instruction* call,
                    Environment& env,
                    Store& store,
                    const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  // If new is called by a function which is a new wrapper, then we
  // create a different object at each callsite where the wrapper is called
  if(env.contains(call))
    env.push(env.lookup(call));
  // There should be an analysis type for the call, but in case there isn't,
  // use the return type of the instruction because new, unlike malloc, is
  // typed so this should work
  else if(Metadata::hasAnalysisType(call))
    env.push(allocate(call, store, caller, CellFlags::Initialized));
  else
    env.push(allocate(dyn_cast<PointerType>(call->getType())->getElementType(),
                      store,
                      call,
                      caller,
                      CellFlags::Initialized));
  return changed;
}

AnalysisStatus calloc(const Instruction* call,
                      Environment& env,
                      Store& store,
                      const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // Even if we are allocating an array of pointers, we don't know the
  // size of the array being allocated, so we can't track it anyway, so
  // we just treat it as a malloc.
  changed |= malloc(call, env, store, caller, CellFlags::Initialized);

  // The difference between malloc and calloc is that while malloc returns
  // uninitialized data, calloc sets all bits to zero. We don't explicitly
  // write any zeros though because that might screw up any pointers that might
  // be allocated, so we just mark it as having been written.
  for(const auto* addr : env.top().getAs<Address>(call))
    store.write(addr, Contents(), nullptr, call, changed);

  return changed;
}

AnalysisStatus realloc(const Instruction* call,
                       const Addresses& ptrs,
                       Environment& env,
                       Store& store,
                       const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const Address* addr : ptrs)
    store.addAllocator(addr, call);

  env.push(ptrs);

  return changed;
}

AnalysisStatus free(const Instruction* call,
                    const Contents& vals,
                    Type* ty,
                    Environment& env,
                    Store& store,
                    const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const auto* addr : vals.getAs<Address>(call))
    if(Metadata::hasCallFree(call))
      store.deallocate(Metadata::getCallFree(call), addr, call, caller);
    else if(store.hasAllocated(addr))
      store.deallocate(store.getAllocated(addr), addr, call, caller);
    else
      moya_error("Trying to free pointer without allocated type @ "
                 << addr->str() << ": " << getDiagnostic(call));

  return changed;
}

// Actually do the memcpy
AnalysisStatus memcpyImpl(const Address* dstBase,
                          const Address* srcBase,
                          long bytes,
                          Store& store,
                          const Instruction* inst) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  for(long copied = 0; copied < bytes; copied += 1) {
    // In the case of structs, a cell will only exist at element
    // boundaries. These will not be visible in a memcpy because we
    // don't know the types being copied. So we will only copy those
    // cells which exist
    if(store.contains(srcBase, copied) and store.contains(dstBase, copied)) {
      const Address* srcAddr = store.make(srcBase, copied);
      const Address* dstAddr = store.make(dstBase, copied);
      store.write(dstAddr,
                  store.read(srcAddr, nullptr, inst, changed),
                  nullptr,
                  inst,
                  changed);
    }
  }
  return changed;
}

AnalysisStatus memcpy(const Instruction* inst,
                      const Vector<Contents>& args,
                      Environment& env,
                      Store& store,
                      const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const auto* dst : args.at(0).getAs<Address>(inst)) {
    for(const auto* src : args.at(1).getAs<Address>(inst)) {
      for(const Content* n : args.at(2)) {
        Type* type = nullptr;
        if(Metadata::hasCallMemcpy(inst)) {
          type = Metadata::getCallMemcpy(inst);
        } else {
          Type* dstType
              = LLVMUtils::getAnalysisType(LLVMUtils::getArgOperand(inst, 0));
          Type* srcType
              = LLVMUtils::getAnalysisType(LLVMUtils::getArgOperand(inst, 1));
          if(dstType != srcType)
            moya_error("Non-equivalent types: " << *dstType << " AND "
                                                << *srcType << " at "
                                                << getDiagnostic(inst));
          else
            type = dstType;
        }
        unsigned long size = store.getTypeAllocSize(type);
        unsigned long dstN = store.getArrayLength(dst, type, 1);
        unsigned long srcN = store.getArrayLength(src, type, 1);
        long bytes = std::min(dstN * size, srcN * size);
        if(const auto* scalar = dyn_cast<ContentScalar>(n)) {
          changed |= memcpyImpl(dst,
                                src,
                                std::min(scalar->getIntegerValue(), bytes),
                                store,
                                inst);
        } else if(isa<ContentRuntimeScalar>(n)) {
          changed |= memcpyImpl(dst, src, bytes, store, inst);
        } else if(isa<ContentUndef>(n)) {
          ;
        } else {
          moya_error("Invalid argument type for n in memcpy: "
                     << *inst << ". Got " << n->str());
        }
      }
    }
  }

  return changed;
}

static AnalysisStatus do_set(const Address* dst,
                             const Contents& vals,
                             long m,
                             Store& store,
                             const Instruction* inst) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  for(long copied = 0; copied < m;) {
    if(store.contains(dst, copied)) {
      const Address* addr = store.make(dst, copied);
      Type* type = store.getCell(addr).getType();
      // memset is typically used to null pointer values and zero out buffers.
      // We can't really do much with the actual value being written, but if
      // there are type mismatches, we get unnecessary warnings. To avoid this
      // as much as possible, we check the value being set and try to do
      // something "sensible"
      const Content* val = nullptr;
      for(const Content* c : vals)
        if(const auto* scalar = dyn_cast<ContentScalar>(c))
          if(not scalar->getValue()->isZeroValue())
            val = store.getRuntimeScalar();
      if(auto* pty = dyn_cast<PointerType>(type)) {
        if(not val)
          val = store.make(ConstantPointerNull::get(pty));
      } else if(LLVMUtils::isScalarTy(type)) {
        if(not val)
          val = store.getRuntimeScalar();
      } else {
        moya_error("memset on supported type: " << *type);
      }
      store.write(addr, val, nullptr, inst, changed);
      copied += store.getCell(addr).getSize();
    } else {
      copied += 1;
    }
  }
  return changed;
}

AnalysisStatus memset(const Instruction* call,
                      const Vector<Contents>& vals,
                      Environment& env,
                      Store& store,
                      const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const auto* base : vals.at(0).getAs<Address>(call)) {
    for(const Content* n : vals.at(2)) {
      if(const auto* scalar = dyn_cast<ContentScalar>(n)) {
        changed
            |= do_set(base, vals.at(1), scalar->getIntegerValue(), store, call);
      } else if(isa<ContentRuntimeScalar>(n)) {
        Type* type = Metadata::getCallMemset(call);
        unsigned long elems = store.getArrayLength(base, type, 0);
        unsigned long size = store.getTypeAllocSize(type);
        changed |= do_set(base, vals.at(1), elems * size, store, call);
      }
    }
  }

  return changed;
}

AnalysisStatus store(const Instruction* call,
                     const Contents& ptrs,
                     Type* ptrTy,
                     const Contents& vals,
                     Type* valTy,
                     Environment& env,
                     Store& store,
                     const Function* f) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const auto* addr : ptrs.getAs<Address>(call))
    store.write(addr, vals, valTy, call, changed);

  return changed;
}

AnalysisStatus load(const Instruction* load,
                    const Contents& ptrs,
                    Type* ptrTy,
                    Environment& env,
                    Store& store,
                    const Function* f) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  Type* ety = dyn_cast<PointerType>(ptrTy)->getElementType();
  Contents read;
  for(const auto* addr : ptrs.getAs<Address>(load))
    // Because of unions, we could have situations where we write different
    // types to the same memory location. But when we read from that location,
    // we should only read the "right" type
    if(LLVMUtils::isScalarTy(ety))
      for(const Content* r : store.read(addr, ety, load, changed))
        read.insert(r);
    else if(auto* pty = dyn_cast<PointerType>(ety))
      for(const Content* r : store.read(addr, ety, load, changed))
        if(isa<ContentAddress>(r) or isa<ContentRuntimePointer>(r))
          read.insert(r);
        else if(isa<ContentNullptr>(r) or isa<ContentUndef>(r))
          ;
        else
          moya_error("Expected address at " << addr->str() << " at "
                                            << getDiagnostic(load) << ". Got "
                                            << r->str());
    else if(isa<ArrayType>(ety))
      moya_error("Cannot read array from store: " << getDiagnostic(load));
    else if(auto* sty = dyn_cast<StructType>(ety))
      read.insert(store.read(addr, ety, load, changed));
    else
      moya_error("Unsupported type to read: " << getDiagnostic(load));

  changed |= env.add(load, read);

  return changed;
}

AnalysisStatus callFunction(const Instruction* call,
                            const Set<const Function*>& callees,
                            const Vector<Contents>& args,
                            const Vector<Type*>& argTys,
                            const Models& models,
                            Environment& env,
                            Store& store,
                            const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const Function* f : callees) {
    // Setup the args. We pretend that each function has its own private
    // stack. This way, we can actually implement pass by value semantics
    // without introducing spurious aliases
    for(const Argument& arg : f->args()) {
      Type* argTy = LLVMUtils::getAnalysisType(arg);
      unsigned argNo = arg.getArgNo();
      const Address* argAddr = env.lookupArg(&arg);
      // C allows a function to be called with fewer arguments than the number
      // that are declared.
      if(argNo >= args.size())
        continue;

      // If a function is called through a vtable lookup, we check the
      // first "this" argument and make sure that it is of the right type.
      // This is to handle the common case where a derived class method is
      // called using a base class pointer. Because we are doing a
      // flow-insensitive analysis, we could have the base class pointer
      // potentially point to many different derived class objects and we
      // have to ensure that the method is called with the right class type
      // TODO: It might help to do a type-check for all arguments and all
      // languages instead of just C++ and the first argument of class members.
      // FIXME: Find a more fool-proof way of determining whether this is a
      // class member function.
      if(argNo == 0 and moya::Metadata::hasVirtual(f)) {
        Type* classTy = cast<PointerType>(argTy)->getElementType();

        for(const Content* c : args.at(0)) {
          if(const auto* addr = dyn_cast<Address>(c)) {
            if(store.isTypeAt(addr, classTy))
              store.write(argAddr, c, argTy, call, changed);
          } else if(isa<ContentRuntimePointer>(c)) {
            store.write(
                argAddr, c, LLVMUtils::getAnalysisType(arg), call, changed);
          } else if(isa<ContentNullptr>(c) or isa<ContentUndef>(c)) {
            ;
          } else {
            moya_warning(
                "Expected address as first argument to C++ class method "
                << f->getName() << " in " << *call << " from "
                << caller->getName() << ". Got " << c->str());
          }
        }
      } else {
        store.write(argAddr, args.at(argNo), argTy, call, changed);
      }
      // If a model is present for the function, then we don't add a mapping
      // to the model's argument. For functions for which we can compute models,
      // this improves the precision of the analysis by introducing some
      // semblance of callsite-sensitivity. Adding a mapping potentially
      // introduces aliases between objects at different callsites and we avoid
      // it as much as possible
      if(f->size() and not moya::Metadata::hasModel(f))
        changed
            |= env.add(&arg, store.read(env.lookupArg(&arg), arg.getType()));
    }

    // It doesn't matter if this is a function with a model or not. We add the
    // callsite to the environment, but it might not necessarily be used
    env.addCallsite(f, call);

    // If we have a model, then just process the model here. This improves the
    // precision of the analysis
    if(not f->size() or moya::Metadata::hasModel(f))
      changed |= callModel(call, f, args, argTys, models, env, store, caller);

    if(moya::Metadata::hasUserMalloc(f)) {
      changed |= malloc(call, env, store, caller);
      env.pop();
    }
  }

  return changed;
}

AnalysisStatus callModel(const Instruction* call,
                         const Function* callee,
                         const Vector<Contents>& args,
                         const Vector<Type*>& argTys,
                         const Models& models,
                         Environment& env,
                         Store& store,
                         const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(models.contains(callee)) {
    Model* model = models.get(callee);
    changed |= model->process(call, callee, args, argTys, env, store, caller);
    if(not callee->getReturnType()->isVoidTy()) {
      if(env.isEmpty())
        moya_error("Empty return stack: " << getDiagnostic(call));
      env.add(call, env.pop());
    }
  } else if(Metadata::hasSTL(callee) or Metadata::hasStd(callee)) {
    moya_error("No model found for STD function\n"
               << "   func: " << callee->getName() << "\n"
               << "  model: " << moya::Metadata::getModel(callee) << "\n"
               << "   site: " << getDiagnostic(call));
    changed |= setTop(call);
  } else if(not moya::Metadata::hasNoAnalyze(callee)) {
    moya_warning("No model found for function\n"
                 << "   func: " << callee->getName() << "\n"
                 << "  model: " << Metadata::getModel(callee) << "\n"
                 << "   site: " << getDiagnostic(call));
    changed |= setTop(call);
  }

  return changed;
}

} // namespace AbstractUtils

} // namespace moya
