#include "common/Diagnostics.h"
#include "common/Map.h"

#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instructions.h>
#include <llvm/Pass.h>

using namespace llvm;

// Simplifies several common casting patterns seen in the DragonEgg code.
// These code simplification passes present cleaner code to Aeryn so there's
// less scope for confusion and the analysis there becomes simpler. Some of the
// patterns that are replaced are given below.
//
// ----------------------------------------------------
// bitcast A => [0 x type]
// bitcast [0 x type] => B
//
// becomes
//
// bitcast A => B
// ----------------------------------------------------
// bitcast (*)(...) => (*)(A, B, ...)
// where A is bitcast((*)(C, D, ...) to (*)(...))
//
// becomes
//
// bitcast (*)(C, D, ...) => (*)(A, B, ...)
// ----------------------------------------------------
// bitcast A => A
//
// is eliminated
// ----------------------------------------------------
class SimplifyCastsPass : public FunctionPass {
public:
  static char ID;

public:
  SimplifyCastsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnFunction(Function&);
};

// bitcast A to [0 x type]
static bool isCastToZeroElementArray(CastInst* cst) {
  if(ArrayType* aty = dyn_cast<ArrayType>(cst->getDestTy()))
    return (aty->getNumElements() == 0);
  return false;
}

// bitcast A to [0 x type]*
static bool isCastToZeroElementArrayPointer(CastInst* cst) {
  if(PointerType* pty = dyn_cast<PointerType>(cst->getDestTy()))
    if(ArrayType* aty = dyn_cast<ArrayType>(pty->getElementType()))
      return (aty->getNumElements() == 0);
  return false;
}

// bitcast (bitcast (*)(C, D, ...) => (*)(...)) => (*)(A, B, ...)
static bool isFunctionPointerCast(CastInst* cst) {
  if(PointerType* p = dyn_cast<PointerType>(cst->getDestTy())) {
    if(p->getElementType()->isFunctionTy()) {
      if(ConstantExpr* c = dyn_cast<ConstantExpr>(cst->getOperand(0))) {
        bool retval = false;
        Instruction* inst = c->getAsInstruction();
        if(isa<CastInst>(inst))
          retval = true;
        inst->deleteValue();
        return retval;
      }
    }
  }
  return false;
}

SimplifyCastsPass::SimplifyCastsPass() : FunctionPass(ID) {
  ;
}

StringRef SimplifyCastsPass::getPassName() const {
  return "Moya Simplify Casts";
}

void SimplifyCastsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool SimplifyCastsPass::runOnFunction(Function& f) {
  moya_message(getPassName());

  bool changed = false;
  moya::Map<Value*, Value*> repl;
  for(inst_iterator i = inst_begin(f); i != inst_end(f); i++) {
    if(CastInst* cst = dyn_cast<CastInst>(&*i)) {
      if(isCastToZeroElementArray(cst)
         or isCastToZeroElementArrayPointer(cst)) {
        for(auto u = cst->use_begin(); u != cst->use_end(); u++)
          if(CastInst* v = dyn_cast<CastInst>(u->getUser()))
            repl[v] = CastInst::CreateBitOrPointerCast(
                cst->getOperand(0), v->getDestTy(), "", v);
      } else if(isFunctionPointerCast(cst)) {
        Value* op = cst->getOperand(0);
        Instruction* inst = dyn_cast<ConstantExpr>(op)->getAsInstruction();
        CastInst* c = dyn_cast<CastInst>(inst);
        repl[cst] = CastInst::CreateBitOrPointerCast(
            c->getOperand(0), cst->getDestTy(), "", cst);
        inst->deleteValue();
      } else if(cst->getOperand(0)->getType() == cst->getDestTy()) {
        repl[cst] = cst->getOperand(0);
      }
    }
  }

  changed |= repl.size();
  for(auto it : repl)
    it.first->replaceAllUsesWith(it.second);

  return changed;
}

char SimplifyCastsPass::ID = 0;

static RegisterPass<SimplifyCastsPass> X("moya-simplify-casts",
                                         "Simplifies the bit cast instructions",
                                         true,
                                         false);

Pass* createSimplifyCastsPass() {
  return new SimplifyCastsPass();
}
