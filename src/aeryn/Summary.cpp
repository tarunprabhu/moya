#include "Summary.h"
#include "Payload.h"
#include "common/ArgumentUtils.h"
#include "common/CallGraph.h"
#include "common/Diagnostics.h"
#include "common/Environment.h"
#include "common/Formatting.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/Store.h"
#include "common/StoreCell.h"

using llvm::dyn_cast;
using llvm::isa;

namespace moya {

Summary::Summary(llvm::Function& f,
                 const Region& region,
                 bool top,
                 const CallGraph& cg,
                 const Environment& env,
                 const Store& store)
    : SummaryBase(Metadata::getJITID(f), region, top), f(f), cg(cg), env(env),
      store(store), fname(Metadata::getSourceName(f)),
      nextAvailableAddress(SummaryBase::null) {
  layout.reset(*LLVMUtils::getModule(f));
  nextAvailableAddress = SummaryBase::null + layout.getPointerSize();
  summarize();
}

RegionID Summary::getRegionID() const {
  return region.getID();
}

MoyaID Summary::getAnalysisID() const {
  return Metadata::getID(f);
}

const std::string& Summary::getFunctionName() const {
  return fname;
}

void Summary::setStatus(Address addr, const Status& status) {
  getCell(addr).setStatus(status);
}

void Summary::updateStatus(Address addr, const Status& status) {
  getCell(addr).updateStatus(status);
}

SummaryCell& Summary::makeCell(const StoreCell& cell) {
  Address addr = cell.getAddress();
  llvm::Type* type = cell.getType();
  SummaryCell& newCell = *new SummaryCell(addr, type);

  if(isa<llvm::PointerType>(type))
    for(const Content* c : cell.read())
      if(const auto* caddr = dyn_cast<ContentAddress>(c))
        newCell.addTarget(caddr->getRaw());
  cells[addr].reset(&newCell);

  return newCell;
}

SummaryCell& Summary::getCell(Address addr) {
  return *static_cast<SummaryCell*>(cells.at(addr).get());
}

const SummaryCell& Summary::getCell(Address addr) const {
  return *static_cast<SummaryCell*>(cells.at(addr).get());
}

bool Summary::hasCell(Address addr) const {
  return cells.contains(addr);
}

bool Summary::hasPolymorphicType(llvm::Type* ty, Set<llvm::Type*>& seen) {
  if(seen.contains(ty))
    return false;

  if(polymorphic.contains(ty))
    return true;

  seen.insert(ty);
  if(LLVMUtils::isScalarTy(ty)) {
    return false;
  } else if(auto* pty = dyn_cast<llvm::PointerType>(ty)) {
    return hasPolymorphicType(pty->getElementType(), seen);
  } else if(auto* aty = dyn_cast<llvm::ArrayType>(ty)) {
    return hasPolymorphicType(aty->getElementType(), seen);
  } else if(auto* sty = dyn_cast<llvm::StructType>(ty)) {
    for(llvm::Type* ety : sty->elements())
      if(hasPolymorphicType(ety, seen))
        return true;
    return false;
  } else if(auto* fty = dyn_cast<llvm::FunctionType>(ty)) {
    for(llvm::Type* ety : fty->params())
      if(hasPolymorphicType(ety, seen))
        return true;
  }

  return false;
}

bool Summary::hasPolymorphicType(llvm::Type* ty) {
  Set<llvm::Type*> seen;
  return hasPolymorphicType(ty, seen);
}

// bool Summary::isConstInClosureOfFunction(const StoreCell& cell,
//                                          const llvm::Function* f,
//                                          const CallGraph& cg) const {
//   for(const auto* w : cell.getWriters<llvm::Function>())
//     if(cg.isInClosureOf(w, f))
//       return false;
//   return true;
// }

// bool Summary::isConstInProperClosureOfFunction(const StoreCell& cell,
//                                                const llvm::Function* f,
//                                                const CallGraph& cg) const {
//   for(const auto* w : cell.getWriters<llvm::Function>())
//     if(cg.isInProperClosureOf(w, f))
//       return false;
//   return true;
// }

// bool Summary::isRHSConstInClosureOfRegion(const StoreCell& cell,
//                                           const CallGraph& cg) const {
//   for(const auto* w : cell.getWriters<llvm::Function>())
//     if(cg.isInClosureOfRoot(w))
//       if(not cell.hasConstantContents())
//         return false;
//   return true;
// }

// At this point, the statuses are set independent of regions. So if an
// object has been marked as Variable, it reflects that it has been written
// to somewhere in the program. We now need to make the status region-aware.
// If an object is marked variable outside a JIT region, but is not
// modified within a region, then update the status to reflect this
Status Summary::computeStatus(const StoreCell& cell,
                              const CallGraph& cg) const {
  Status status = Status::getUnused();
  for(const auto* f : cell.getReaders<llvm::Instruction>())
    if(cg.isInClosureOfRoot(f))
      status.setConstant();

  for(const auto* f : cell.getWriters<llvm::Instruction>())
    if(cg.isInClosureOfRoot(f))
      status.setVariable();

  for(const auto* f : cell.getAllocators<llvm::Instruction>())
    if(cg.isInClosureOfRoot(f))
      status.setVariable();

  for(const auto* f : cell.getDeallocators<llvm::Instruction>())
    if(cg.isInClosureOfRoot(f))
      status.setVariable();

  return status;
}

template <>
Status Summary::computeArgumentStatus<SourceLang::Cuda>(llvm::Argument& arg,
                                                        Address addr) {
  moya_error("Unimplemented: Argument status for Cuda");
}

template <>
Status Summary::computeArgumentStatus<SourceLang::Fort>(llvm::Argument& arg,
                                                        Address addr) {
  Status status = Status::getConstant();
  // In Fortran, a primitive type is passed by pointer
  // type (unless a "value" keyword was used in the original source).
  // We want to use this the primitve as the key
  //
  if(auto* pty = dyn_cast<llvm::PointerType>(LLVMUtils::getAnalysisType(arg))) {
    if(LLVMUtils::isScalarTy(pty->getElementType()))
      return status.setDerefInKey();

    // Two-level pointers get passed when the argument has a Fortran
    // pointer attribute
    else if(isa<llvm::PointerType>(pty->getElementType()))
      return status.setDerefInKey();
  }
  return status;
}

template <>
Status Summary::computeArgumentStatus<SourceLang::CXX>(llvm::Argument& arg,
                                                       Address addr) {
  Status status = Status::getConstant();

  // Dereference parameters passed by reference. These are the only ones
  // that will be used when JIT'ing because using pointers in C/C++ is much
  // too problematic
  if(arg.getDereferenceableBytes()) {
    llvm::Type* ety = LLVMUtils::getAnalysisTypeAs<llvm::PointerType>(arg)
                          ->getElementType();

    // If a scalar is passed by reference, use the value of the scalar in
    // the key
    if(LLVMUtils::isScalarTy(ety))
      return status.setDerefInKey();

    // If a two-level pointer is passed, we should use the pointer in the key
    // becuase the "outer" pointer is actually a reference
    else if(ety->isPointerTy())
      return status.setDerefInKey();

    // FIXME: Structs passed by reference should be used in the key
  } else if(auto* pty
            = dyn_cast<llvm::PointerType>(LLVMUtils::getAnalysisType(arg))) {
    if(isa<llvm::ArrayType>(pty->getElementType())
       or Metadata::hasFixedArray(arg) or Metadata::hasDynArray(arg)) {
      status.setDerefInKey();
    } else {
      // Anything else should have been caught much earlier and should have
      // had the ignore flag set and control should never get here
      moya_error("Unexpected argument type when computing status\n"
                 << "   arg: " << arg.getArgNo() << "\n"
                 << "  type: " << *arg.getType() << "\n"
                 << "  func: " << arg.getParent()->getName());
    }
  }

  return status;
}

template <>
Status Summary::computeArgumentStatus<SourceLang::C>(llvm::Argument& arg,
                                                     Address addr) {
  Status status = Status::getConstant();

  if(auto* pty = dyn_cast<llvm::PointerType>(LLVMUtils::getAnalysisType(arg))) {
    if(isa<llvm::ArrayType>(pty->getElementType())
       or Metadata::hasFixedArray(arg) or Metadata::hasDynArray(arg)) {
      // If an argument is passed by pointer, the only case where it is safe to
      // use it is if it is a pointer to a fixed-length array
      status.setDerefInKey();
    } else {
      // Anything else should have been caught much earlier and should have
      // had the ignore flag set and control should never get here
      moya_error("Unexpected argument type when computing status\n"
                 << "   arg: " << arg.getArgNo() << "\n"
                 << "  type: " << *arg.getType() << "\n"
                 << "  func: " << arg.getParent()->getName());
    }
  }

  return status;
}

// We treat function arguments slightly differently. Because we do runtime
// specialization and can generate a new function for each
//
// TODO: As an optimization, we can probably do the same thing with pointers
// to struct types i.e. if any of the fields of the structs being pointed to
// is not modified by any of the function's callees in the callgraph, then
// we can fold that constant in.
Status Summary::computeArgumentStatus(llvm::Argument& arg, Address addr) {
  llvm::Function* f = arg.getParent();
  if(Metadata::hasForce(arg) or (not Metadata::hasIgnore(arg))) {
    if(isFortran(Metadata::getSourceLang(f)))
      return computeArgumentStatus<SourceLang::Fort>(arg, addr);
    else if(isCXX(Metadata::getSourceLang(f)))
      return computeArgumentStatus<SourceLang::CXX>(arg, addr);
    else if(isC(Metadata::getSourceLang(f)))
      return computeArgumentStatus<SourceLang::C>(arg, addr);
    else if(isCuda(Metadata::getSourceLang(f)))
      if(Metadata::hasCudaKernelDevice(f))
        return computeArgumentStatus<SourceLang::Cuda>(arg, addr);
      else
        return computeArgumentStatus<SourceLang::CXX>(arg, addr);
    else
      moya_error("Unknown function source language to be JIT'ed");
  }

  return Status::getUnused();
}

Status Summary::computeGlobalStatus(llvm::GlobalVariable& g,
                                    llvm::Type* type,
                                    Address addr) {
  if(LLVMUtils::isScalarTy(type))
    return computeGlobalStatus(type, addr);
  else if(auto* pty = dyn_cast<llvm::PointerType>(type))
    return computeGlobalStatus(g, pty, addr);
  else if(auto* aty = dyn_cast<llvm::ArrayType>(type))
    return computeGlobalStatus(g, aty, addr);
  else if(auto* sty = dyn_cast<llvm::StructType>(type))
    return computeGlobalStatus(g, sty, addr);

  moya_error("Cannot compute status of unsupported type in global:\n"
             << "  global: " << g.getName() << "\n"
             << "    type: " << *type << "\n"
             << "    addr: " << addr);

  // Just to silence warnings
  return Status::getVariable();
}

Status Summary::computeGlobalStatus(llvm::Type* type, Address addr) {
  if(hasCell(addr))
    return getCell(addr).getStatus();
  return Status::getUnused();
}

Status Summary::computeGlobalStatus(llvm::GlobalVariable& g,
                                    llvm::PointerType* pty,
                                    Address addr) {
  // If a pointer appears in a global variable, we can treat it as just
  // another scalar. The point of computing the status of global variables is
  // to be able to call llvm::GlobalVariable::setConstant() at JIT-time.
  // In that case, the raw pointer will get folded into the initializer of the
  // global which is also fine because the pointer will remain valid for as
  // long as the global is used anyway
  //
  if(hasCell(addr))
    return getCell(addr).getStatus();
  return Status::getUnused();
}

Status Summary::computeGlobalStatus(llvm::GlobalVariable& g,
                                    llvm::ArrayType* aty,
                                    Address addr) {
  return computeGlobalStatus(g, LLVMUtils::getFlattenedElementType(aty), addr);
}

Status Summary::computeGlobalStatus(llvm::GlobalVariable& g,
                                    llvm::StructType* sty,
                                    Address base) {
  Status status = Status::getUnused();
  const StructLayout& sl = layout.getStructLayout(sty);
  for(unsigned i = 0; i < sty->getNumElements(); i++)
    status.update(computeGlobalStatus(
        g, sty->getElementType(i), base + sl.getElementOffset(i)));

  return status;
}

// To get the status of a global variable, we need to fold the status of
// all the fields of the variable (if it is a struct) or all elements (if it
// is an array). The status of the individual cells is not enough to tell
// whether or not the variable is constant. We don't chase pointers because
// the const-ness of the global variable is not transitive. We can't really
// deal with pointers anyway. The point of computing the status is if the
// global is a constant
Status Summary::computeGlobalStatus(llvm::GlobalVariable& g, Address addr) {
  std::string gname = g.getName().str();
  llvm::Type* type
      = LLVMUtils::getAnalysisTypeAs<llvm::PointerType>(g)->getElementType();

  // If the global variable has no initializer, is externally visible and
  // has no allocator, then it was likely defined somewhere that we cannot
  // see. In that case, it is safest to mark it as variable so it doesn't
  // get accidentally folded. This is mainly here so we can deal with
  // stdout and stderr correctly.
  if(g.hasExternalLinkage() and not g.hasInitializer())
    return Status::getVariable();
  return computeGlobalStatus(g, type, addr);
}

void Summary::initialize(llvm::Module& module, const Store& store) {
  nextAvailableAddress = Summary::null + layout.getPointerSize();

  for(llvm::StructType* sty : module.getIdentifiedStructTypes())
    if(Metadata::hasRecursive(sty, module))
      recursive.insert(sty);
    else if(Metadata::hasPolymorphic(sty, module))
      polymorphic.insert(sty);
}

void Summary::propagate(llvm::LoadInst* load, Address addr) {
  MoyaID id = Metadata::getID(load);
  if(not addresses[id].contains(addr)) {
    addresses[id].push_back(addr);

    if(isa<llvm::PointerType>(load->getType()))
      for(Address next : getTargets(addr))
        for(llvm::Use& u : load->uses())
          propagate(u.getUser(), next);
  }
}

void Summary::propagate(llvm::AllocaInst* alloca, Address addr) {
  MoyaID id = Metadata::getID(alloca);
  if(not addresses[id].contains(addr)) {
    addresses[id].push_back(addr);

    for(llvm::Use& u : alloca->uses())
      propagate(u.getUser(), addr);
  }
}

void Summary::propagate(llvm::GetElementPtrInst* gep, Address addr) {
  MoyaID id = Metadata::getID(gep);
  if(not addresses[id].contains(addr)) {
    addresses[id].push_back(addr);

    Address next = addr;
    if(Metadata::hasGEPIndex(gep))
      next = addr + Metadata::getGEPIndex(gep);
    else
      next = SummaryBase::propagate(
          gep, gep->getPointerOperandType(), 1, {}, addr);
    for(llvm::Use& u : gep->uses())
      propagate(u.getUser(), next);
  }
}

void Summary::propagate(llvm::PHINode* phi, Address addr) {
  MoyaID id = Metadata::getID(phi);
  if(not addresses[id].contains(addr)) {
    addresses[id].push_back(addr);

    for(llvm::Use& u : phi->uses())
      propagate(u.getUser(), addr);
  }
}

void Summary::propagate(llvm::Value* v, Address addr) {
  if(auto* load = dyn_cast<llvm::LoadInst>(v))
    propagate(load, addr);
  else if(auto* gep = dyn_cast<llvm::GetElementPtrInst>(v))
    propagate(gep, addr);
  else if(auto* alloca = dyn_cast<llvm::AllocaInst>(v))
    propagate(alloca, addr);
  else if(auto* cst = dyn_cast<llvm::CastInst>(v))
    for(llvm::Use& u : cst->uses())
      propagate(u.getUser(), addr);
  else if(auto* phi = dyn_cast<llvm::PHINode>(v))
    propagate(phi, addr);
  else if(not isa<llvm::Instruction>(v))
    moya_error("Expected use to be instruction: " << *v);
}

void Summary::summarize() {
  // If the analysis hit top, then we don't bother doing anything
  if(isTop())
    return;

  // If the callgraph was not constructed, it is because the function was
  // marked as to be specialized, but no JIT region was defined
  if(not cg.isConstructed())
    return;

  // If the function is not ever called in the region, don't bother about it
  if(not cg.isInClosureOfRoot(&f))
    return;

  initialize(*f.getParent(), store);

  // Copy all the cells that are used by this function. We don't inline
  // during the JIT, so this is safe to do.
  for(Store::RawAddress addr : store.getCellAddresses()) {
    const StoreCell& cell = store.getCell(addr);
    if(cell.isUsedByFunction(Metadata::getID(f))) {
      SummaryCell& newCell = makeCell(cell);

      // FIXME: We can probably compute a more accurate status by examining
      // the closure of the callgraph
      newCell.setStatus(computeStatus(cell, cg));
    }
  }

  // Propagate addresses of everything that is reachable in the function
  // to the load instructions. Things that are reachable would be global
  // variables, function arguments and stack allocated variables

  // Trying to figure out whether or not a global variable is "used" by
  // looking at all the fields (and other things accessible through the
  // global) is likely impossible because void* can mess things up. You don't
  // even need to have void* in the code because, for instance, Fortran arrays
  // could possibly be represented using a void* which could make things
  // quite unpleasant. Instead, we have copied all the cells that are used
  // by this function. So eveything that might be needed is available. We
  // only need to then save those globals whose pointers are "directly" used.
  // Even if the global variable is used a different way (for instance
  // through a pointer saved somewhere else or by its having been passed as
  // an argument to the function), the cell containing both the pointer and
  // all of the contents will still be present.
  //
  for(llvm::GlobalVariable& g : LLVMUtils::getModule(f)->globals()) {
    if(not g.isConstant() and not Metadata::hasNoAnalyze(g)
       and LLVMUtils::hasUsesInFunction(g, f)) {
      llvm::Type* ty = LLVMUtils::getAnalysisTypeAs<llvm::PointerType>(g)
                           ->getElementType();
      if(hasPolymorphicType(ty))
        moya_error("Unsupported polymorphic global:\n"
                   << "  global: " << g.getName() << "\n"
                   << "    type: " << *ty);

      Address addr = env.lookupGlobal(g)->getRaw();
      for(llvm::Use& u : g.uses())
        if(LLVMUtils::getFunction(u.getUser()) == &f)
          propagate(u.getUser(), addr);
      globals[g.getName()] = std::make_tuple(env.lookupGlobal(g)->getRaw(),
                                             computeGlobalStatus(g, addr));
    }
  }

  // The function argument status just computes additional flags that may
  // need to be added to the argument to determine how to use it when
  // computing the key to the function depending on the language and/or other
  // user options
  //
  for(llvm::Argument& arg : f.args()) {
    // I am not sure if the function argument stack slots will already have
    // been marked as used, so we add them in case
    Address addr = env.lookupArg(&arg)->getRaw();

    if(not cells.contains(addr))
      makeCell(store.getCell(addr));

    llvm::Type* argTy = LLVMUtils::getAnalysisType(arg);

    if(hasPolymorphicType(argTy))
      moya_error("Unsupported polymorphic argument:\n"
                 << "  func: " << f.getName() << "\n"
                 << "   arg: " << arg.getArgNo() << "\n"
                 << "  type: " << *argTy);

    Status status = computeArgumentStatus(arg, addr);
    if(auto* pty = dyn_cast<llvm::PointerType>(argTy)) {
      for(Address target : getTargets(addr)) {
        // If the argument is a pointer to a struct, the target will be to the
        // start of the first element of the struct. But that element may or
        // may not be used. Since we don't try to compute a status for the
        // struct as a whole, it'll be propagated.
        // In all other cases, if the argument is not used in the function
        // there won't be a cell
        if(isa<llvm::StructType>(pty->getElementType()) or hasCell(target)) {
          if(status.isDerefInKey())
            getCell(target).setStatusUsedInKey();
          for(llvm::Use& u : arg.uses())
            propagate(u.getUser(), target);
        }
      }
    }

    setStatus(addr, status);
    args.push_back(std::make_tuple(addr, status));
  }

  for(llvm::inst_iterator i = inst_begin(f); i != inst_end(f); i++) {
    // Ideally, there should not be any allocas in a JIT'ed function because
    // they interfere with the optimizations and prevent constants from being
    // uncovered. The main reason to propagate() allocas is so that any loads
    // where the pointer is obtained from loading from an alloca get the
    // correct Target associated with them.
    //
    if(auto* alloca = dyn_cast<llvm::AllocaInst>(&*i)) {
      llvm::Type* ty = store.getAllocated(env.lookupAlloca(alloca));
      if(hasPolymorphicType(ty))
        moya_error("Unsupported polymorphic alloca:\n"
                   << "  func: " << f.getName() << "\n"
                   << "  inst: " << *alloca << "\n"
                   << "  type: " << *ty);

      propagate(alloca, env.lookupAlloca(alloca)->getRaw());
    }
    // There are these occasional weird cases where there is a load from an
    // undef. It is almost certainly a potential bug in the code, but
    // it nevertheless does happen. If that is not propagated here, it causes
    // problems later on when there is no entry for the load in the summary
    else if(auto* load = dyn_cast<llvm::LoadInst>(&*i)) {
      if(isa<llvm::Constant>(load->getPointerOperand())) {
        MoyaID id = Metadata::getID(load);
        if(not addresses.contains(id))
          addresses[id] = Vector<Address>();
      }
    }
  }

  // MoyaID fid = Metadata::getID(f);
  // for(const Store::RawAddress addr : store.getCellAddresses()) {
  //   const StoreCell& cell = store.getCell(addr);
  //   Status status = computeStatus(cell, cg);

  //   if(status.isUsed()) {
  //     // // If the cell is not the first element of a static array, then
  //     its
  //     // // accessors and contents will have been folded into the first
  //     element
  //     // of
  //     // // the array anyway. Also, the runtime optimizer will only look at
  //     the
  //     // // first element of the array, so there's no need to actually
  //     create
  //     // the
  //     // // cell which will not be used
  //     // if(not(cell.isStatic() and cell.isArray() and cell.isElement())) {
  //     //   cells[addr].reset(new SummaryCell(addr, cell.getType()));

  //     SummaryCell& summary = *new SummaryCell(addr, cell.getType());
  //     cells[addr].reset(&summary);
  //     // If the cell is part of a "fake" stack through which function
  //     // arguments are passed, then it must be constant because even if an
  //     // argument is passed by pointer/reference, the actual pointer itself
  //     // is passed by value
  //     if(cell.getFlags().hasArgument())
  //       summary.setStatusConstant();

  //     // If the cell is a constant in the function and all its callees
  //     // FIXME: Is this correct??? Even if the cell is constant in the
  //     callees
  //     // of the function, it may stil be modified by a function "higher" up
  //     // in the callgraph of the region. If we reuse the JIT'ed code, we
  //     may
  //     // end up reusing the stale value
  //     else if(isConstInClosureOfFunction(cell, fid, cg))
  //       summary.setStatusConstant();

  //     // If the cell is written to within the region and only constants are
  //     // written to it, we can usually mark it as constant by assuming that
  //     // the program is correct and whatever is written will be read AFTER
  //     it
  //     // has been written. The only exception to this is when the write
  //     takes
  //     // place in a function g() which is called by the function f()
  //     // currently being JIT'ted. In that case, we cannot treat constant
  //     RHS's
  //     // as constant because g() has not been called at JIT time for f().
  //     // The example below illustrates this:
  //     //
  //     // int global_array[10];
  //     //
  //     // #pragma moya specialize
  //     // void f(int i) {
  //     //   g(i);
  //     //   int t = global_array[i];
  //     // }
  //     //
  //     // void g(int i) {
  //     //   global_array[i] = 33;
  //     // }
  //     //
  //     // int main(int argc, char *argv[]) {
  //     // #pragma moya jit
  //     //   {
  //     //     f(atoi(argv[1])
  //     //   }
  //     // }
  //     //
  //     // When JIT'ting f(), we cannot replace the value of
  //     // global_array[i] because it is only set AFTER g() is called. To
  //     deal
  //     // with cases like this, global_array[] should be marked as variable
  //     //
  //     // More generally, a cell should be marked as constant for a function
  //     f
  //     // iff it is never modified in the closure of the function
  //     // else if(not isConstInClosureOfFunction(cell, fid, regionID, cg)
  //     //         and isRHSConstInClosureOfRegion(cell, regionID, cg))
  //     //   statuses.insert(make_pair(fid, Status::getConstant()));
  //     else
  //       summary.setStatus(status);

  //     Type* type = cell.getType();
  //     Contents contents = cell.read();
  //     Store::RawAddress target = Store::null;
  //     if(cell.getType()->isPointerTy()) {
  //       if(contents.size()) {
  //         const Content* c = *contents.begin();
  //         if(const Store::Address* t = dyn_cast<Store::Address>(c))
  //           target = t->getRaw();
  //         else if(isa<ContentNullptr>(c))
  //           target = Store::null;
  //         else if(isa<ContentRuntimePointer>(c))
  //           target = ~Store::null;
  //         else
  //           moya_warning("Target of pointer should be address. Got "
  //                        << c->str());
  //       }
  //     }
  //     summary.addTarget(target);
  //   }
  // }

  // computeArgumentStatus(cg, store, module);
  // computeGlobalStatus(env, store, module);

  summarized = true;
}

std::string Summary::str(bool all, int depth) const {
  std::string buf;
  llvm::raw_string_ostream ss(buf);
  std::string tab1 = space(indent1(depth));
  std::string tab2 = space(indent2(depth));
  std::string tab3 = space(indent3(depth));

  ss << space(depth) << "summary: \n";
  ss << tab1 << "region: " << region.getID() << "\n";
  ss << tab1 << "location: " << region.getLocation().str() << "\n";
  ss << tab1 << "function: " << Metadata::getFullName(f) << "\n";
  ss << tab1 << "jit: " << Metadata::getJITID(f) << "\n";
  ss << tab1 << "id: " << Metadata::getID(f) << "\n";
  ss << tab1 << "summarized: " << summarized << "\n";
  ss << "\n";

  ss << tab1 << "args [" << args.size() << "]:\n";
  for(const auto& t : args)
    ss << tab2 << std::get<Address>(t) << "\n";
  ss << tab1 << "\n";

  ss << tab1 << "globals [" << globals.size() << "]:\n";
  for(auto& it : globals) {
    std::string gname = it.first;
    auto addr = std::get<Summary::Address>(it.second);
    auto status = std::get<Status>(it.second);
    if(status.isUsed()) {
      ss << tab2 << gname << ":\n";
      ss << tab3 << "addr: " << addr << "\n";
      ss << tab3 << "status: " << status.str() << "\n";
      ss << tab2 << "\n";
    }
  }

  Vector<Summary::Address> addrs;
  for(auto& it : cells)
    addrs.push_back(it.first);
  llvm::sort(addrs.begin(), addrs.end());

  ss << tab1 << "cells [" << addrs.size() << "]:\n";
  for(Summary::Address addr : addrs)
    if(all or cells.at(addr)->getStatus().isUsed())
      ss << getCell(addr).str(indent2(depth)) << "\n";

  ss << tab1 << "\n";

  return ss.str();
}

void Summary::serialize(Serializer& s) const {
  // Sort addresses before writing out cells
  Vector<Summary::Address> addrs;
  for(auto& it : cells)
    addrs.push_back(it.first);
  llvm::sort(addrs.begin(), addrs.end());

  s.mapStart();

  s.add("region", region);
  s.add("function", Metadata::getFullName(f));
  s.add("jit", Metadata::getJITID(f));
  s.add("id", Metadata::getID(f));
  s.add("top", top);
  s.add("summarized", summarized);

  // Arguments
  s.arrStart("args");
  for(const auto& t : args)
    s.serialize(std::get<Address>(t));
  s.arrEnd();

  // Globals
  s.mapStart("globals");
  for(auto& it : globals) {
    const std::string& gname = it.first;
    const Summary::Address& addr = std::get<Address>(it.second);
    const Status& status = std::get<Status>(it.second);

    s.mapStart(gname);
    s.add("addr", addr);
    s.add("status", status.str());
    s.mapEnd();
  }
  s.mapEnd();

  // Cells
  s.mapStart("cells");
  for(Summary::Address addr : addrs)
    if(getCell(addr).getStatus().isUsed())
      s.add(moya::str(addr), getCell(addr));
  s.mapEnd();

  s.mapEnd();
}

void Summary::pickle(Payload& p) const {
  // Pickle header
  p.pickle(getMagicNumber());
  p.pickle(region);
  p.pickle(function);
  p.pickle(top);
  p.pickle(summarized);

  if(isSummarized()) {
    // Function arguments
    p.pickle(args.size());
    for(const auto& t : args) {
      Address addr = std::get<Address>(t);
      Status status = std::get<Status>(t);

      p.pickle(addr);
      p.pickle(status);
    }

    // Globals
    p.pickle(globals.size());
    for(const auto& it : globals) {
      const std::string& gname = it.first;
      Address addr = std::get<Address>(it.second);
      Status status = std::get<Status>(it.second);

      p.pickle(gname);
      p.pickle(addr);
      p.pickle(status);
    }

    // Cells
    p.pickle(cells.size());
    for(const auto& it : cells)
      getCell(it.first).pickle(p);
  }
}

} // namespace moya

// Status Summary::fold(Address addr, PointerType* pty) const {
//   Status status = Status::getUnused();
//   for(Address target : getTargets(addr))
//     status.update(fold(target, pty->getElementType()));
//   return status;
// }

// Status Summary::fold(Address addr, ArrayType* aty) const {
//   Status status = Status::getUnused();
//   uint64_t size = layout.getTypeAllocSize(aty->getElementType());
//   for(unsigned i = 0; i < aty->getNumElements(); i++)
//     status.update(fold(addr + i * size, aty->getElementType()));
//   return status;
// }

// Status Summary::fold(Address addr, StructType* sty) const {
//   Status status = Status::getUnused();
//   const StructLayout& sl = layout.getStructLayout(sty);
//   for(unsigned i = 0; i < sty->getNumElements(); i++) {
//     uint64_t offset = sl.getElementOffset(i);
//     status.update(fold(addr + offset, sty->getElementType(i)));
//   }
//   return status;
// }

// Status Summary::fold(Address addr, Type* type) const {
//   if(LLVMUtils::isScalarTy(type))
//     return cells.at(addr)->getStatus();
//   else if(auto* pty = dyn_cast<PointerType>(type))
//     return fold(addr, pty);
//   else if(auto* aty = dyn_cast<ArrayType>(type))
//     return fold(addr, aty);
//   else if(auto* sty = dyn_cast<StructType>(type))
//     return fold(addr, sty);
//   else
//     moya_error("Cannot fold type: " << *type);
//   return Status::getUnused();
// }

// void Summary::merge(Address dst,
//                     const Store::Address* src,
//                     const Store& store) {
//   getCell(dst).merge(store.getCell(src));
// }

// void Summary::merge(PointerType* pty,
//                     Address dst,
//                     const Store::Address* src,
//                     const Store& store,
//                     Set<const Store::Address*>& seen) {
//   if(merged.contains(std::make_pair(dst, src)))
//     return;

//   getCell(dst).merge(store.getCell(src));

//   Type* ety = pty->getElementType();
//   if(polymorphic.contains(ety)) {
//     moya_error("Unsupported: polymorphic merge");
//   } else {
//     for(const Address& target : getTargets(dst))
//       for(const Content* c : store.read(src))
//         if(const auto* addr = dyn_cast<ContentAddress>(c))
//           merge(ety, target, addr, store, seen);
//   }
//   merged.insert(std::make_pair(dst, src));
// }

// void Summary::merge(ArrayType* aty,
//                     Address dst,
//                     const Store::Address* src,
//                     const Store& store,
//                     Set<const Store::Address*>& seen) {
//   Type* ety = LLVMUtils::getFlattenedElementType(aty);
//   uint64_t size = layout.getTypeAllocSize(ety);
//   uint64_t nelems = store.getArrayLength(src, ety, 1);
//   for(uint64_t i = 0; i < nelems; i++)
//     merge(ety, dst, store.make(src, i * size), store, seen);
// }

// void Summary::merge(StructType* sty,
//                     Address dst,
//                     const Store::Address* src,
//                     const Store& store,
//                     Set<const Store::Address*>& seen) {
//   const StructLayout& sl = layout.getStructLayout(sty);
//   for(unsigned i = 0; i < sty->getNumElements(); i++) {
//     uint64_t offset = sl.getElementOffset(i);
//     merge(sty->getElementType(i),
//           dst + offset,
//           store.make(src, offset),
//           store,
//           seen);
//   }
// }

// void Summary::merge(Type* ty,
//                     Address dst,
//                     const Store::Address* src,
//                     const Store& store,
//                     Set<const Store::Address*>& seen) {
//   if(seen.contains(src))
//     return;

//   seen.insert(src);
//   if(LLVMUtils::isScalarTy(ty) or ty->isFunctionTy())
//     merge(dst, src, store);
//   else if(auto* pty = dyn_cast<PointerType>(ty))
//     merge(pty, dst, src, store, seen);
//   else if(auto* aty = dyn_cast<ArrayType>(ty))
//     merge(aty, dst, src, store, seen);
//   else if(auto* sty = dyn_cast<StructType>(ty))
//     merge(sty, dst, src, store, seen);
//   else
//     moya_error("Unknown type to merge: " << *ty);
// }

// Summary::Address Summary::align(Type* ty) {
//   Store::RawAddress addr = nextAvailableAddress;
//   unsigned alignment = layout.getTypeAlignment(ty);
//   if(addr % alignment)
//     addr = (addr / alignment + 1) * alignment;
//   nextAvailableAddress = addr;

//   return nextAvailableAddress;
// }

// Summary::Address Summary::reserve(Type* ty) {
//   Address base = align(ty);
//   nextAvailableAddress += layout.getTypeAllocSize(ty);
//   return base;
// }

// Summary::Address Summary::createCell(Type* ty, Address base) {
//   cells[base].reset(new SummaryCell(base, ty));
//   return base;
// }

// Summary::Address Summary::createCell(Type* ty) {
//   Address base = reserve(ty);
//   createCell(ty, base);
//   return base;
// }

// void Summary::populate(PointerType* pty,
//                        Address base,
//                        Map<StructType*, Address>& allocated) {
//   Type* ety = pty->getElementType();
//   if(polymorphic.contains(ety)) {
//     moya_warning("UNIMPLEMENTED: Populate polymorphic pointers: " << *ety);
//   } else {
//     createCell(pty, base);
//     Address pointee = allocate(ety, allocated);
//     getCell(base).addTarget(pointee);
//   }
// }

// void Summary::populate(ArrayType* aty,
//                        Address base,
//                        Map<StructType*, Address>& allocated) {
//   populate(LLVMUtils::getFlattenedElementType(aty), base, allocated);
// }

// void Summary::populate(StructType* sty,
//                        Address base,
//                        Map<StructType*, Address>& allocated) {
//   if(sty->isOpaque()) {
//     createCell(sty, base);
//   } else {
//     const StructLayout& sl = layout.getStructLayout(sty);
//     for(unsigned i = 0; i < sty->getNumElements(); i++)
//       populate(
//           sty->getElementType(i), base + sl.getElementOffset(i),
//           allocated);
//   }
// }

// void Summary::populate(Type* ty,
//                        Address base,
//                        Map<StructType*, Address>& allocated) {
//   if(LLVMUtils::isScalarTy(ty) or ty->isFunctionTy())
//     createCell(ty, base);
//   else if(auto* pty = dyn_cast<PointerType>(ty))
//     populate(pty, base, allocated);
//   else if(auto* aty = dyn_cast<ArrayType>(ty))
//     populate(aty, base, allocated);
//   else if(auto* sty = dyn_cast<StructType>(ty))
//     populate(sty, base, allocated);
//   else
//     moya_error("Unsupported type to populate: " << *ty);
// }

// Summary::Address Summary::allocate(PointerType* pty,
//                                    Map<StructType*, Address>& allocated) {
//   Address base = Summary::null;

//   Type* ety = pty->getElementType();
//   if(polymorphic.contains(ety)) {

//     moya_error("UNIMPLEMENTED: Polymorphic pointers: ");
//   } else {
//     base = createCell(pty);
//     Address pointee = allocate(ety, allocated);
//     getCell(base).addTarget(pointee);
//   }

//   return base;
// }

// Summary::Address Summary::allocate(ArrayType* aty,
//                                    Map<StructType*, Address>& allocated) {
//   // "Reserve" space for the entire array because we still want everything
//   to
//   // line up correctly, but we will only actually allocate a single cell.
//   // This also ensures that if the array element is a pointer, we can
//   // allocate space for the pointee correctly
//   ArrayType* flattened = LLVMUtils::getFlattened(aty);
//   Address base = reserve(flattened);
//   populate(flattened->getElementType(), base, allocated);

//   return base;
// }

// Summary::Address Summary::allocate(StructType* sty,
//                                    Map<StructType*, Address>& allocated) {
//   // If the struct is opaque, we just create a single cell for it
//   if(sty->isOpaque())
//     return createCell(sty);

//   if(recursive.contains(sty) and allocated.contains(sty))
//     return allocated.at(sty);

//   // Reserve space for the struct before populating the elements. We do it
//   // this way because we might have pointers in the struct and we want to
//   // be able to allocate space for the pointees correctly
//   Address base = reserve(sty);

//   // We only create a single instance of recursive structs because in
//   general,
//   // we can't be sure of not falling into an infinite loop with mutually
//   // recursive structs.
//   // FIXME: If we keep the store around during the allocation, we can
//   overcome
//   // this limitation.
//   if(recursive.contains(sty))
//     allocated[sty] = base;
//   const StructLayout& sl = layout.getStructLayout(sty);
//   for(unsigned i = 0; i < sty->getNumElements(); i++)
//     populate(sty->getElementType(i), base + sl.getElementOffset(i),
//     allocated);
//   return base;
// }

// Summary::Address Summary::allocate(Type* ty,
//                                    Map<StructType*, Address>& allocated) {
//   if(LLVMUtils::isScalarTy(ty) or ty->isFunctionTy())
//     return createCell(ty);
//   else if(auto* pty = dyn_cast<PointerType>(ty))
//     return allocate(pty, allocated);
//   else if(auto* aty = dyn_cast<ArrayType>(ty))
//     return allocate(aty, allocated);
//   else if(auto* sty = dyn_cast<StructType>(ty))
//     return allocate(sty, allocated);
//   else
//     moya_error("Unsupported type to collect: " << *ty);
//   return Summary::null;
// }

// Summary::Address Summary::allocate(Type* ty) {
//   Map<StructType*, Address> allocated;
//   return allocate(ty, allocated);
// }

// bool Summary::isUsed(PointerType* pty,
//                      const Store::Address* addr,
//                      const Store& store,
//                      Set<const Store::Address*>& seen) {
//   for(const Content* c : store.read(addr)) {
//     if(const auto* next = dyn_cast<ContentAddress>(c)) {
//       if(isUsed(pty->getElementType(), next, store, seen))
//         return true;
//     } else if(isa<ContentRuntimePointer>(c)) {
//       moya_error("Cannot compute whether the pointer is used with an
//       unknown "
//                  "target address at "
//                  << addr->getRaw());
//     }
//   }
//   return false;
// }

// bool Summary::isUsed(ArrayType* aty,
//                      const Store::Address* addr,
//                      const Store& store,
//                      Set<const Store::Address*>& seen) {
//   Type* ety = LLVMUtils::getFlattenedElementType(aty);
//   uint64_t size = layout.getTypeAllocSize(ety);
//   uint64_t nelems = store.getArrayLength(addr, ety, 1);

//   for(uint64_t i = 0; i < nelems; i++)
//     if(isUsed(ety, store.make(addr, i * size), store, seen))
//       return true;
//   return false;
// }

// bool Summary::isUsed(StructType* sty,
//                      const Store::Address* addr,
//                      const Store& store,
//                      Set<const Store::Address*>& seen) {
//   const StructLayout& sl = store.getStructLayout(sty);
//   for(unsigned i = 0; i < sty->getNumElements(); i++) {
//     uint64_t offset = sl.getElementOffset(i);
//     if(isUsed(sty->getElementType(i), store.make(addr, offset), store,
//     seen))
//       return true;
//   }
//   return false;
// }

// bool Summary::isUsed(Type* ty,
//                      const Store::Address* addr,
//                      const Store& store,
//                      Set<const Store::Address*>& seen) {
//   if(seen.contains(addr))
//     return true;

//   seen.insert(addr);
//   if(LLVMUtils::isScalarTy(ty) or ty->isFunctionTy())
//     return store.getCell(addr).isUsedByFunction(id);
//   else if(auto* pty = dyn_cast<PointerType>(ty))
//     return isUsed(pty, addr, store, seen);
//   else if(auto* aty = dyn_cast<ArrayType>(ty))
//     return isUsed(aty, addr, store, seen);
//   else if(auto* sty = dyn_cast<StructType>(ty))
//     return isUsed(sty, addr, store, seen);
//   else
//     moya_error("Unsupported type for used: " << *ty);
//   return false;
// }

// bool Summary::isUsed(GlobalVariable& g,
//                      const Store::Address* addr,
//                      const Store& store) {
//   auto* ty =
//   LLVMUtils::getAnalysisTypeAs<PointerType>(g)->getElementType(); Set<const
//   Store::Address*> seen; return isUsed(ty, addr, store, seen);
// }
