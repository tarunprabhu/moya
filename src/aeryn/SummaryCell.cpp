#include "SummaryCell.h"
#include "Payload.h"
#include "common/Formatting.h"
#include "common/Metadata.h"
#include "common/Serializer.h"
#include "common/Store.h"
#include "common/StoreCell.h"
#include "common/Stringify.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;

namespace moya {

SummaryCell::SummaryCell(Address addr, Type* type, Status status)
    : SummaryCellBase(), type(type) {
  this->addr = addr;
  this->status = status;
}

Type* SummaryCell::getType() const {
  return type;
}

SummaryCell::Funcs SummaryCell::getReaders() const {
  return readers;
}

SummaryCell::Funcs SummaryCell::getWriters() const {
  return writers;
}

SummaryCell::Funcs SummaryCell::getAllocators() const {
  return allocators;
}

SummaryCell::Funcs SummaryCell::getDeallocators() const {
  return deallocators;
}

void SummaryCell::merge(const StoreCell& cell) {
  for(const Function* f : cell.getReaders<Function>())
    readers.insert(moya::Metadata::getID(f));

  for(const Function* f : cell.getWriters<Function>())
    writers.insert(moya::Metadata::getID(f));

  for(const Function* f : cell.getAllocators<Function>())
    allocators.insert(moya::Metadata::getID(f));

  for(const Function* f : cell.getDeallocators<Function>())
    deallocators.insert(moya::Metadata::getID(f));

  for(const Content* c : cell.read())
    contents.insert(c);
}

void SummaryCell::addTarget(Address addr) {
  if(not targets.contains(addr))
    targets.push_back(addr);
}

void SummaryCell::setStatusConstant() {
  status.setConstant(true);
}

void SummaryCell::setStatusDerefInKey() {
  status.setDerefInKey();
}

void SummaryCell::setStatusUsedInKey() {
  status.setUsedInKey();
}

void SummaryCell::setStatus(const Status& newStatus) {
  status = newStatus;
}

void SummaryCell::updateStatus(const Status& newStatus) {
  status.update(newStatus);
}

std::string SummaryCell::str(unsigned depth) const {
  std::string buf;
  llvm::raw_string_ostream ss(buf);
  std::string tab1 = moya::space(moya::indent1(depth));

  ss << moya::space(depth) << addr << ":\n";
  ss << tab1 << "type: " << *type << "\n";
  ss << tab1 << "status: " << status.str() << "\n";
  if(targets.size())
    ss << tab1 << "targets: " << moya::str(targets) << "\n";

  return ss.str();
}

void SummaryCell::serialize(Serializer& s) const {
  s.mapStart();
  s.add("addr", addr);
  s.add("status", status.str());
  s.add("type", moya::str(type));
  if(targets.size())
    s.add("targets", targets);
  s.mapEnd();
}

void SummaryCell::pickle(Payload& p) const {
  p.pickle(addr);
  p.pickle(status);
  p.pickle(targets.size());
  for(Address target : targets)
    p.pickle(target);
}

} // namespace moya
