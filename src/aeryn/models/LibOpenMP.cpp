#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Vector.h"

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>

using namespace llvm;
using namespace moya;

static void mkReadOnly(Models& ms, const std::string& name) {
  ms.add(new ModelReadOnly(name));
  ms.add(new ModelReadOnly(name + "_"));
}

static void mkReadWrite(Models& ms,
                        const std::string& name,
                        const Vector<Model::ArgSpec>& isConst) {
  ms.add(new ModelReadWrite(name, isConst));
  ms.add(new ModelReadWrite(name + "_", isConst));
}

void Models::initLibOpenMP(const Module& module) {
  Models& ms = *this;

  mkReadOnly(ms, "omp_set_num_threads");
  mkReadOnly(ms, "omp_get_num_threads");
  mkReadOnly(ms, "omp_get_max_threads");
  mkReadOnly(ms, "omp_get_thread_num");
  mkReadOnly(ms, "omp_get_num_procs");

  mkReadOnly(ms, "omp_in_parallel");

  mkReadOnly(ms, "omp_set_dynamic");
  mkReadOnly(ms, "omp_set_dynamic_4");
  mkReadOnly(ms, "omp_set_dynamic_8");
  mkReadOnly(ms, "omp_get_dynamic");

  mkReadOnly(ms, "omp_set_nested");
  mkReadOnly(ms, "omp_get_nested");

  mkReadOnly(ms, "omp_get_wtime");
  mkReadOnly(ms, "omp_get_wtick");

  mkReadOnly(ms, "omp_get_thread_limit");
  mkReadOnly(ms, "omp_set_max_active_levels");
  mkReadOnly(ms, "omp_get_max_active_levels");
  mkReadOnly(ms, "omp_get_level");
  mkReadOnly(ms, "omp_get_ancestor_thread_num");
  mkReadOnly(ms, "omp_get_team_size");
  mkReadOnly(ms, "omp_get_active_level");

  mkReadOnly(ms, "omp_in_final");

  mkReadOnly(ms, "omp_get_cancellation");

  mkReadOnly(ms, "omp_set_default_device");
  mkReadOnly(ms, "omp_get_default_device");
  mkReadOnly(ms, "omp_get_num_devices");
  mkReadOnly(ms, "omp_get_num_teams");
  mkReadOnly(ms, "omp_get_team_num");

  mkReadOnly(ms, "omp_is_initial_device");

  mkReadWrite(ms, "omp_init_lock", {Model::ArgSpec::ReadWrite});
  mkReadWrite(ms, "omp_destroy_lock", {Model::ArgSpec::ReadWrite});
  mkReadWrite(ms, "omp_set_lock", {Model::ArgSpec::ReadWrite});
  mkReadWrite(ms, "omp_unset_lock", {Model::ArgSpec::ReadWrite});
  mkReadWrite(ms, "omp_test_lock", {Model::ArgSpec::ReadWrite});

  mkReadWrite(ms, "omp_init_nest_lock", {Model::ArgSpec::ReadWrite});
  mkReadWrite(ms, "omp_destroy_nest_lock", {Model::ArgSpec::ReadWrite});
  mkReadWrite(ms, "omp_set_nest_lock", {Model::ArgSpec::ReadWrite});
  mkReadWrite(ms, "omp_unset_nest_lock", {Model::ArgSpec::ReadWrite});
  mkReadWrite(ms, "omp_test_nest_lock", {Model::ArgSpec::ReadWrite});

  mkReadWrite(ms,
              "omp_set_schedule",
              {Model::ArgSpec::ReadOnly, Model::ArgSpec::ReadOnly});
  mkReadWrite(ms,
              "omp_get_schedule",
              {Model::ArgSpec::ReadWrite, Model::ArgSpec::ReadWrite});

  mkReadOnly(ms, "omp_get_proc_bind");
}
