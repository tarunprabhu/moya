#include "ModelSTLList.h"
#include "Models.h"
#include "common/LangConfig.h"

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_list);
}

static std::string mk(const std::string& name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "std::operator" << op << "(" << getContainerName() << ", "
       << getContainerName() << ")";
    ms.add(new ModelSTLList(ss.str(), ModelSTLList::Compare, ms));
  }
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLList(name, fn, ms));
}

void Models::initSTLList(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms, mk("list()"), ModelSTLList::ConstructorEmpty);
  mkModel(ms, mk("list(allocator)"), ModelSTLList::ConstructorEmpty);
  mkModel(ms, mk("list(unsigned long)"), ModelSTLList::ConstructorEmpty);
  mkModel(
      ms, mk("list(unsigned long, allocator)"), ModelSTLList::ConstructorEmpty);
  mkModel(ms,
          mk("list(unsigned long, *, allocator)"),
          ModelSTLList::ConstructorElement);
  mkModel(ms,
          mk("list(iterator, iterator, allocator)"),
          ModelSTLList::ConstructorRange);
  mkModel(ms, mk("list(std::list)"), ModelSTLList::ConstructorCopy);
  mkModel(ms,
          mk("list(std::initializer_list, allocator)"),
          ModelSTLList::ConstructorInitList);

  // Destructor
  mkModel(ms, mk("~list()"), ModelSTLList::Destructor);

  // operator =
  mkModel(ms, mk("operator=(std::list)"), ModelSTLList::OperatorContainer);
  mkModel(ms,
          mk("operator=(std::initializer_list)"),
          ModelSTLList::OperatorInitList);

  // allocator
  mkModel(ms, mk("get_allocator()"), ModelSTLList::GetAllocator);

  // assign
  mkModel(ms, mk("assign(unsigned long, *)"), ModelSTLList::AssignMany);
  mkModel(ms, mk("assign(iterator, iterator)"), ModelSTLList::AssignRange);
  mkModel(
      ms, mk("assign(std::initializer_list)"), ModelSTLList::AssignInitList);

  // Element access
  mkModel(ms, mk("front()"), ModelSTLList::AccessElement);
  mkModel(ms, mk("back()"), ModelSTLList::AccessElement);

  // s
  mkModel(ms, mk("begin()"), ModelSTLList::Begin);
  mkModel(ms, mk("cbegin()"), ModelSTLList::Begin);
  mkModel(ms, mk("end()"), ModelSTLList::End);
  mkModel(ms, mk("cend()"), ModelSTLList::End);
  mkModel(ms, mk("rbegin()"), ModelSTLList::Begin);
  mkModel(ms, mk("crbegin()"), ModelSTLList::Begin);
  mkModel(ms, mk("rend()"), ModelSTLList::End);
  mkModel(ms, mk("crend()"), ModelSTLList::End);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLList::Size);
  mkModel(ms, mk("size()"), ModelSTLList::Size);
  mkModel(ms, mk("max_size()"), ModelSTLList::Size);

  // Modifiers
  mkModel(ms, mk("clear()"), ModelSTLList::Clear);

  mkModel(ms, mk("emplace(iterator, ...)"), ModelSTLList::EmplaceElementAt);
  mkModel(ms, mk("emplace_front(...)"), ModelSTLList::EmplaceElement);
  mkModel(ms, mk("emplace_back(...)"), ModelSTLList::EmplaceElement);

  mkModel(ms, mk("insert(iterator, *)"), ModelSTLList::InsertElement);
  mkModel(ms,
          mk("insert(iterator, iterator, iterator)"),
          ModelSTLList::InsertRange);
  mkModel(
      ms, mk("insert(iterator, unsigned long, *)"), ModelSTLList::InsertMany);
  mkModel(ms,
          mk("insert(iterator, std::initializer_list)"),
          ModelSTLList::InsertInitList);

  mkModel(ms, mk("erase(iterator)"), ModelSTLList::EraseElementAt);
  mkModel(ms, mk("erase(iterator, iterator)"), ModelSTLList::EraseRange);

  mkModel(ms, mk("push_front(*)"), ModelSTLList::AddElement);
  mkModel(ms, mk("push_back(*)"), ModelSTLList::AddElement);
  mkModel(ms, mk("pop_front()"), ModelSTLList::RemoveElement);
  mkModel(ms, mk("pop_back()"), ModelSTLList::RemoveElement);

  // The resize function will change the pointers and might also affect the
  // data, so it behaves somewhat like clear
  mkModel(ms, mk("resize(unsigned long)"), ModelSTLList::Clear);
  // This overload of resize also assigns to the modified list, so it
  // behaves somewhat like assign
  mkModel(ms, mk("resize(unsigned long, *)"), ModelSTLList::AssignMany);

  mkModel(ms, mk("swap(std::list)"), ModelSTLList::Swap);

  // Operations
  mkModel(ms, mk("merge(std::list)"), ModelSTLList::Merge);
  mkModel(ms, mk("merge(std::list, *)"), ModelSTLList::Merge);

  mkModel(ms, mk("splice(iterator, std::list)"), ModelSTLList::Splice);
  mkModel(
      ms, mk("splice(iterator, std::list, iterator)"), ModelSTLList::Splice);
  mkModel(ms,
          mk("splice(iterator, std::list, iterator, iterator)"),
          ModelSTLList::Splice);

  // The clear method marks both the container and the data buffer as being
  // read which is what we need
  mkModel(ms, mk("reverse()"), ModelSTLList::Clear);
  mkModel(ms, mk("unique()"), ModelSTLList::Clear);
  mkModel(ms, mk("sort()"), ModelSTLList::Clear);
  mkModel(ms, mk("remove(*)"), ModelSTLList::Clear);
  // mkModel(ms, mk("unique(*)"), ModelSTLList::Clear);
  // mkModel(ms, mk("remove_if(*)"), ModelSTLList::Clear);
  // mkModel(ms, mk("sort(*)"), ModelSTLList::Clear);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);
}
