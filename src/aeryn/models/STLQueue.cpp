#include "ModelSTLQueue.h"
#include "Models.h"
#include "common/LangConfig.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_queue);
}

static std::string mk(const std::string& name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "std::operator" << op << "(" << getContainerName() << ", "
       << getContainerName() << ")";
    ms.add(new ModelSTLQueue(ss.str(), ModelSTLQueue::Compare, ms));
  }
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLQueue(name, fn, ms));
}

void Models::initSTLQueue(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms, mk("queue()"), ModelSTLQueue::ConstructorEmpty);
  mkModel(ms, mk("queue(*)"), ModelSTLQueue::ConstructorCopy);
  mkModel(ms, mk("queue(std::queue)"), ModelSTLQueue::ConstructorCopy);

  // operator =
  mkModel(ms, mk("operator=(std::queue)"), ModelSTLQueue::OperatorContainer);

  // Destructors
  mkModel(ms, mk("~queue()"), ModelSTLQueue::Destructor);

  // Element access
  mkModel(ms, mk("front()"), ModelSTLQueue::AccessElement);
  mkModel(ms, mk("back()"), ModelSTLQueue::AccessElement);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLQueue::Size);
  mkModel(ms, mk("size()"), ModelSTLQueue::Size);

  // Modifiers
  mkModel(ms, mk("emplace(...)"), ModelSTLQueue::EmplaceElement);
  mkModel(ms, mk("push(*)"), ModelSTLQueue::AddElement);
  mkModel(ms, mk("pop()"), ModelSTLQueue::RemoveElement);
  mkModel(ms, mk("swap(std::queue)"), ModelSTLQueue::Swap);

  // Compare container operators
  initContainerCompareModels(ms);
}
