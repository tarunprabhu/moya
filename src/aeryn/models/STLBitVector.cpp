#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "ModelSTLBitVector.h"
#include "Models.h"
#include "common/LangConfig.h"

#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_bitvector);
}

static std::string mk(const std::string& name) {
  std::stringstream ss;
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(const std::string& op : LangConfigCXX::getComparisonOperators()) {
    std::stringstream ss;
    std::string cont = getContainerName();
    ss << "std::operator" << op << "(" << cont << ", " << cont << ")";
    ms.add(new ModelSTLBitVector(ss.str(), ModelSTLBitVector::Compare, ms));
  }
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLBitVector::FuncID fn) {
  ms.add(new ModelSTLBitVector(name, fn, ms));
}

void Models::initSTLBitVector(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms, mk("vector()"), ModelSTLBitVector::ConstructorEmpty);
  mkModel(ms, mk("vector(allocator)"), ModelSTLBitVector::ConstructorEmpty);
  mkModel(ms,
          mk("vector(unsigned long, allocator)"),
          ModelSTLBitVector::ConstructorEmpty);
  mkModel(ms,
          mk("vector(unsigned long, bool, allocator)"),
          ModelSTLBitVector::ConstructorElement);
  mkModel(ms,
          mk("vector(iterator, iterator, allocator)"),
          ModelSTLBitVector::ConstructorRange);
  mkModel(
      ms, mk("vector(std::vector<bool>)"), ModelSTLBitVector::ConstructorCopy);
  mkModel(ms,
          mk("vector(std::initializer_list, allocator)"),
          ModelSTLBitVector::ConstructorInitList);

  // Destructor
  mkModel(ms, mk("~vector()"), ModelSTLBitVector::Destructor);

  // operator =
  mkModel(ms,
          mk("operator=(std::vector<bool>)"),
          ModelSTLBitVector::OperatorContainer);
  mkModel(ms,
          mk("operator=(std::initializer_list)"),
          ModelSTLBitVector::OperatorInitList);

  // allocator
  mkModel(ms, mk("get_allocator()"), ModelSTLBitVector::GetAllocator);

  // assign
  mkModel(ms, mk("assign(unsigned long, bool)"), ModelSTLBitVector::AssignMany);
  mkModel(ms, mk("assign(int, bool)"), ModelSTLBitVector::AssignMany);
  mkModel(ms, mk("assign(iterator, iterator)"), ModelSTLBitVector::AssignRange);
  mkModel(ms,
          mk("assign(std::initializer_list)"),
          ModelSTLBitVector::AssignInitList);

  // Element access
  mkModel(ms, mk("at(unsigned long)"), ModelSTLBitVector::AccessElement);
  mkModel(
      ms, mk("operator[](unsigned long)"), ModelSTLBitVector::AccessElement);
  mkModel(ms, mk("front()"), ModelSTLBitVector::AccessElement);
  mkModel(ms, mk("back()"), ModelSTLBitVector::AccessElement);
  mkModel(ms, mk("data()"), ModelSTLBitVector::Begin);

  // Iterators
  mkModel(ms, mk("begin()"), ModelSTLBitVector::Begin);
  mkModel(ms, mk("cbegin()"), ModelSTLBitVector::Begin);
  mkModel(ms, mk("end()"), ModelSTLBitVector::End);
  mkModel(ms, mk("cend()"), ModelSTLBitVector::End);
  mkModel(ms, mk("rbegin()"), ModelSTLBitVector::Begin);
  mkModel(ms, mk("crbegin()"), ModelSTLBitVector::Begin);
  mkModel(ms, mk("rend()"), ModelSTLBitVector::End);
  mkModel(ms, mk("crend()"), ModelSTLBitVector::End);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLBitVector::Size);
  mkModel(ms, mk("size()"), ModelSTLBitVector::Size);
  mkModel(ms, mk("max_size()"), ModelSTLBitVector::Size);
  mkModel(ms, mk("capacity()"), ModelSTLBitVector::Size);
  // Both reserve and shrink_to_fit behave like clear in the sense that the
  // underlying data is touched in some way. So we can get away with using the
  // same model for all three methods
  mkModel(ms, mk("reserve(unsigned long)"), ModelSTLBitVector::Resize);
  mkModel(ms, mk("shrink_to_fit()"), ModelSTLBitVector::Resize);

  // Modifiers
  mkModel(ms, mk("clear()"), ModelSTLBitVector::Clear);

  mkModel(ms, mk("insert(iterator, bool)"), ModelSTLBitVector::InsertElement);
  mkModel(ms,
          mk("insert(iterator, unsigned long, bool)"),
          ModelSTLBitVector::InsertMany);
  mkModel(ms,
          mk("insert(iterator, iterator, iterator)"),
          ModelSTLBitVector::InsertRange);
  mkModel(ms,
          mk("insert(iterator, std::initializer_list)"),
          ModelSTLBitVector::InsertInitList);

  mkModel(ms, mk("erase(iterator)"), ModelSTLBitVector::EraseElementAt);
  mkModel(ms, mk("erase(iterator, iterator)"), ModelSTLBitVector::EraseRange);

  mkModel(ms, mk("push_back(bool)"), ModelSTLBitVector::AddElement);
  mkModel(ms, mk("pop_back()"), ModelSTLBitVector::RemoveElement);

  // The resize function will change the pointers and might also affect the
  // data, so it behaves somewhat like clear
  mkModel(ms, mk("resize(unsigned long)"), ModelSTLBitVector::Resize);
  // This overload of resize also assigns to the modified vector, so it
  // behaves somewhat like assign
  mkModel(ms, mk("resize(unsigned long, bool)"), ModelSTLBitVector::Resize);

  mkModel(ms, mk("flip()"), ModelSTLBitVector::Flip);

  mkModel(ms, mk("swap(std::vector<bool>)"), ModelSTLBitVector::Swap);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);

  // The bit-reference is publicly visible
  ms.add(new ModelReadWrite("std::_Bit_reference::operator=(bool)",
                            {0, 1},
                            Model::ReturnSpec::Arg0,
                            true));
  ms.add(new ModelReadWrite("std::_Bit_reference::flip()", {0}, true));
  ms.add(new ModelReadOnly("std::_Bit_reference::operator bool()"));
}
