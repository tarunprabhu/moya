#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;
using namespace moya;

static inline std::string mk(const std::string& base, const std::string& name) {
  std::stringstream ss;

  ss << "std::" << base << "::" << name;

  return ss.str();
}

static inline std::string mkiss(const std::string& name) {
  return mk("basic_istringstream", name);
}

static inline std::string mkoss(const std::string& name) {
  return mk("basic_ostringstream", name);
}

static inline std::string mkss(const std::string& name) {
  return mk("basic_stringstream", name);
}

void Models::initStdStringStream(const Module& module) {
  // std::istringstream models
  add(new ModelReadWrite(mkiss("basic_istringstream()"), {0}, true));
  add(new ModelReadWrite(
      mkiss("basic_istringstream(std::_Ios_Openmode)"), {0, 1}, true));
  add(new ModelReadWrite(
      mkiss("basic_istringstream(std::basic_string, std::_Ios_Openmode)"),
      {0, 1, 1},
      true));
  add(new ModelReadWrite(mkiss("~basic_istringstream()"), {0, 1}, true));
  add(new ModelReadWrite(
      mkiss("swap(std::basic_istringstream)"), {0, 0}, true));
  add(new ModelReadWrite(mkiss("str()"), {0}, true));
  add(new ModelReadWrite(mkiss("str(std::basic_string)"), {0, 1}, true));

  // ostringstream models
  add(new ModelReadWrite(mkoss("basic_ostringstream()"), {0}, true));
  add(new ModelReadWrite(
      mkoss("basic_ostringstream(std::_Ios_Openmode)"), {0, 1}, true));
  add(new ModelReadWrite(
      mkoss("basic_ostringstream(std::basic_string, std::_Ios_Openmode)"),
      {0, 1, 1},
      true));
  add(new ModelReadWrite(mkoss("~basic_ostringstream()"), {0, 1}, true));
  add(new ModelReadWrite(
      mkoss("swap(std::basic_ostringstream)"), {0, 0}, true));
  add(new ModelReadWrite(mkoss("str()"), {0}, true));
  add(new ModelReadWrite(mkoss("str(std::basic_string)"), {0, 1}, true));

  // std::stringstream models
  add(new ModelReadWrite(mkss("basic_stringstream()"), {0}, true));
  add(new ModelReadWrite(
      mkss("basic_stringstream(std::_Ios_Openmode)"), {0, 1}, true));
  add(new ModelReadWrite(
      mkss("basic_stringstream(std::basic_string, std::_Ios_Openmode)"),
      {0, 1, 1},
      true));
  add(new ModelReadWrite(mkss("~basic_stringstream()"), {0, 1}, true));
  add(new ModelReadWrite(
      mkss("swap(std::basic_std::stringstream)"), {0, 0}, true));
  add(new ModelReadWrite(mkss("str()"), {0}, true));
  add(new ModelReadWrite(mkss("str(std::basic_string)"), {0, 1}, true));
}
