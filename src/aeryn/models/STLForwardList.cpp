#include "ModelSTLForwardList.h"
#include "Models.h"
#include "common/LangConfig.h"

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_forward_list);
}

static std::string mk(const std::string& name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "std::operator" << op << "(" << getContainerName() << ", "
       << getContainerName() << ")";
    ms.add(new ModelSTLForwardList(ss.str(), ModelSTLForwardList::Compare, ms));
  }
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLForwardList(name, fn, ms));
}

void Models::initSTLForwardList(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms, mk("forward_list()"), ModelSTLForwardList::ConstructorEmpty);
  mkModel(
      ms, mk("forward_list(allocator)"), ModelSTLForwardList::ConstructorEmpty);
  mkModel(ms,
          mk("forward_list(unsigned long, allocator)"),
          ModelSTLForwardList::ConstructorEmpty);
  mkModel(ms,
          mk("forward_list(unsigned long, *, allocator)"),
          ModelSTLForwardList::ConstructorElement);
  mkModel(ms,
          mk("forward_list(iterator, iterator, allocator)"),
          ModelSTLForwardList::ConstructorRange);
  mkModel(ms,
          mk("forward_list(std::forward_list)"),
          ModelSTLForwardList::ConstructorCopy);
  mkModel(ms,
          mk("forward_list(std::initializer_list, allocator)"),
          ModelSTLForwardList::ConstructorInitList);

  // Destructor
  mkModel(ms, mk("~forward_list()"), ModelSTLForwardList::Destructor);

  // operator =
  mkModel(ms,
          mk("operator=(std::forward_list)"),
          ModelSTLForwardList::OperatorContainer);
  mkModel(ms,
          mk("operator=(std::initializer_list)"),
          ModelSTLForwardList::OperatorInitList);

  // allocator
  mkModel(ms, mk("get_allocator()"), ModelSTLForwardList::GetAllocator);

  // assign
  mkModel(ms, mk("assign(unsigned long, *)"), ModelSTLForwardList::AssignMany);
  mkModel(
      ms, mk("assign(iterator, iterator)"), ModelSTLForwardList::AssignRange);
  mkModel(ms,
          mk("assign(std::initializer_list)"),
          ModelSTLForwardList::AssignInitList);

  // Element access
  mkModel(ms, mk("front()"), ModelSTLForwardList::AccessElement);

  // s
  mkModel(ms, mk("before_begin()"), ModelSTLForwardList::Begin);
  mkModel(ms, mk("cbefore_begin()"), ModelSTLForwardList::Begin);
  mkModel(ms, mk("begin()"), ModelSTLForwardList::Begin);
  mkModel(ms, mk("cbegin()"), ModelSTLForwardList::Begin);
  mkModel(ms, mk("end()"), ModelSTLForwardList::End);
  mkModel(ms, mk("cend()"), ModelSTLForwardList::End);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLForwardList::Size);
  mkModel(ms, mk("max_size()"), ModelSTLForwardList::Size);

  // Modifiers
  mkModel(ms, mk("clear()"), ModelSTLForwardList::Clear);

  mkModel(ms, mk("emplace_front(...)"), ModelSTLForwardList::EmplaceElement);
  mkModel(ms,
          mk("emplace_after(iterator, ...)"),
          ModelSTLForwardList::EmplaceElementAt);

  mkModel(ms,
          mk("insert_after(iterator, iterator, iterator)"),
          ModelSTLForwardList::InsertRange);
  mkModel(
      ms, mk("insert_after(iterator, *)"), ModelSTLForwardList::InsertElement);
  mkModel(ms,
          mk("insert_after(iterator, unsigned long, *)"),
          ModelSTLForwardList::InsertMany);
  mkModel(ms,
          mk("insert_after(iterator, std::initializer_list)"),
          ModelSTLForwardList::InsertInitList);

  mkModel(ms, mk("erase_after(iterator)"), ModelSTLForwardList::EraseElementAt);
  mkModel(ms,
          mk("erase_after(iterator, iterator)"),
          ModelSTLForwardList::EraseRange);

  mkModel(ms, mk("push_front(*)"), ModelSTLForwardList::AddElement);
  mkModel(ms, mk("pop_front()"), ModelSTLForwardList::RemoveElement);

  // The resize function will change the pointers and might also affect the
  // data, so it behaves somewhat like clear
  mkModel(ms, mk("resize(unsigned long)"), ModelSTLForwardList::Clear);
  // This overload of resize also assigns to the modified forward_list, so it
  // behaves somewhat like assign
  mkModel(ms, mk("resize(unsigned long, *)"), ModelSTLForwardList::AssignMany);

  mkModel(ms, mk("swap(std::forward_list)"), ModelSTLForwardList::Swap);

  // Operations
  mkModel(ms, mk("merge(std::forward_list)"), ModelSTLForwardList::Merge);
  mkModel(ms, mk("merge(std::forward_list, *)"), ModelSTLForwardList::Merge);

  mkModel(ms,
          mk("splice_after(iterator, std::forward_list)"),
          ModelSTLForwardList::Splice);
  mkModel(ms,
          mk("splice_after(iterator, std::forward_list, iterator)"),
          ModelSTLForwardList::Splice);
  mkModel(ms,
          mk("splice_after(iterator, std::forward_list, iterator, iterator)"),
          ModelSTLForwardList::Splice);

  // The clear method marks both the container and the data buffer as being
  // read which is what we need
  mkModel(ms, mk("remove(*)"), ModelSTLForwardList::Clear);
  mkModel(ms, mk("reverse()"), ModelSTLForwardList::Clear);
  mkModel(ms, mk("unique()"), ModelSTLForwardList::Clear);
  mkModel(ms, mk("sort()"), ModelSTLForwardList::Clear);
  // // FIXME: We should fix these so that the arguments get correctly passed to
  // // the comparator function
  // mkModel(ms, mk("unique(*)"), ModelSTLForwardList::Clear);
  // mkModel(ms, mk("sort(*)"), ModelSTLForwardList::Clear);
  // mkModel(ms, mk("remove_if(*)"), ModelSTLForwardList::Clear);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);
}
