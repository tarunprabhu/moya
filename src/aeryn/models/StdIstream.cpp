#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;

namespace moya {

static inline std::string mk(const std::string& base, const std::string& name) {
  std::stringstream ss;

  ss << "std::" << base << "::" << name;

  return ss.str();
}

static inline std::string mkbis(const std::string& name) {
  return mk("basic_istream", name);
}

static inline std::string mkifs(const std::string& name) {
  return mk("basic_ifstream", name);
}

static AnalysisStatus modelOperator(const ModelCustom& model,
                                    const Instruction* call,
                                    const Function* f,
                                    const Vector<Contents>& args,
                                    Environment& env,
                                    Store& store,
                                    const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(unsigned i = 1; i < args.size(); i++) {
    Type* argTy = LLVMUtils::getArgumentType(f, i);
    changed |= model.markRead(args.at(i), argTy, env, store, call);
    if(not LLVMUtils::isPtrToFunctionTy(argTy))
      changed |= model.markWrite(args.at(i), argTy, env, store, call);
  }

  // This is a virtually inherited class of some form. The last element of
  // the class will be an instance of std::basic_ios. We need to iterate over
  // all the elements in the object until we get to that instance and mark
  // everything as being modified along the way. This is necessarily
  // conservative but there is no reasonable way to handle it otherwise.
  //
  // Only one virtual base class is allowed, std::basic_ios. Whatever the
  // argument to this function, it is guaranteed to be a descendant of this
  // class
  const Module& module = *LLVMUtils::getModule(call);
  auto* sty = dyn_cast<StructType>(
      LLVMUtils::getInnermost(LLVMUtils::getArgumentType(f, 0)));
  StructType* vbase = Metadata::getVirtualBases(sty, module).at(0);

  for(const auto* addr : args.at(0).getAs<Address>(call)) {
    unsigned long offset = 0;
    const Address* next = addr;
    do {
      Type* pty = store.getTypesAt(next).at(0)->getPointerTo();
      changed |= model.markWrite(next, pty, env, store, call);
      for(offset++; not store.contains(addr, offset); offset++)
        ;
      next = store.make(addr, offset);
    } while(not(store.isStructAt(next, vbase)));
    changed |= model.markWrite(next, vbase->getPointerTo(), env, store, call);
  }

  env.push(args.at(0));

  return changed;
}

static AnalysisStatus model_rdbuf(const ModelCustom& model,
                                  const Instruction* call,
                                  const Function* f,
                                  const Vector<Contents>& args,
                                  Environment& env,
                                  Store& store,
                                  const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(auto* sty = dyn_cast<StructType>(
         LLVMUtils::getArgumentTypeAs<PointerType>(f, 0)->getElementType())) {
    const StructLayout& sl = store.getStructLayout(sty);
    for(unsigned i = 0; i < sty->getNumElements(); i++) {
      Type* ty = sty->getElementType(i);
      if(ty->getPointerTo() == f->getReturnType()) {
        uint64_t offset = sl.getElementOffset(i);
        Contents ret;
        for(const Address* addr : args.at(0).getAs<Address>(call))
          ret.insert(store.make(addr, offset));
        changed |= env.push(ret);
        return changed;
      }
    }
  }

  changed |= env.push(store.getRuntimePointer());

  return changed;
}

void Models::initStdIstream(const Module& module) {
  // basic_istream models
  add(new ModelCustom(
      "std::operator>>(std::basic_istream, *)", modelOperator, true, false));
  add(new ModelReadWrite(
      mkbis("basic_istream(std::basic_streambuf*)"), {0, 1}, true));
  add(new ModelReadWrite(mkbis("~basic_istream()"), {0, 1}, true));
  add(new ModelCustom(mkbis("operator>>(*)"), modelOperator, true, false));
  add(new ModelReadWrite(
      mkbis("get(char*)"), {0, 0}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(mkbis("get(char*, unsigned long)"),
                         {0, 0, 1},
                         Model::ReturnSpec::Arg0,
                         true));
  add(new ModelReadWrite(mkbis("get(char*, unsigned long, char)"),
                         {0, 0, 1, 1},
                         Model::ReturnSpec::Arg0,
                         true));
  add(new ModelReadWrite(mkbis("get(std::basic_streambuf)"),
                         {0, 0},
                         Model::ReturnSpec::Arg0,
                         true));
  add(new ModelReadWrite(mkbis("get(std::basic_streambuf, char)"),
                         {0, 0, 1},
                         Model::ReturnSpec::Arg0,
                         true));
  add(new ModelReadOnly(mkbis("peek()")));
  add(new ModelReadWrite(mkbis("unget()"), {0}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(
      mkbis("putback()"), {0, 1}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(
      mkbis("getline(char*, long)"), {0, 0, 1}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(mkbis("getline(char*, long, char)"),
                         {0, 0, 1, 1},
                         Model::ReturnSpec::Arg0,
                         true));
  add(new ModelReadWrite(
      mkbis("read(char*, long)"), {0, 0, 1}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(mkbis("readsome(char*, long)"), {0, 0, 1, true}));
  add(new ModelReadOnly(mkbis("gcount()")));
  add(new ModelReadOnly(mkbis("tellg()")));
  add(new ModelReadWrite(
      mkbis("seekg(std::fpos)"), {0, 1}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(
      mkbis("seekg(unsigned long)"), {0, 1}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(mkbis("seekg(unsigned long, int)"),
                         {0, 1},
                         Model::ReturnSpec::Arg0,
                         true));

  add(new ModelReadWrite(mkbis("sync()"), {0}, true));

  // ifstream models
  add(new ModelReadWrite(mkifs("basic_ifstream()"), {0}, true));
  add(new ModelReadWrite(
      mkifs("basic_ifstream(char*, std::_Ios_Openmode)"), {0, 1, 1}, true));
  add(new ModelReadWrite(
      mkifs("basic_ifstream(std::basic_string, std::_Ios_Openmode"),
      {0, 1, 1},
      true));
  add(new ModelReadWrite(
      mkifs("basic_ifstream(std::basic_ifstream"), {0, 1}, true));
  add(new ModelReadWrite(mkifs("~basic_ifstream()"), {0, 1}, true));
  add(new ModelReadWrite(mkifs("swap(std::basic_ifstream"), {0, 0}, true));
  add(new ModelReadOnly(mkifs("is_open()")));
  add(new ModelReadWrite(
      mkifs("open(char*, std::_Ios_Openmode)"), {0, 1, 1}, true));
  add(new ModelReadWrite(
      mkifs("open(std::basic_string, std::_Ios_Openmode)"), {0, 1, 1}, true));
  add(new ModelReadWrite(mkifs("close()"), {0, 1}, true));
  add(new ModelCustom(mkifs("rdbuf()"), model_rdbuf));

  add(new ModelReadWrite(
      "std::flush(std::basic_ostream)", {0}, Model::ReturnSpec::Arg0, true));
}

} // namespace moya
