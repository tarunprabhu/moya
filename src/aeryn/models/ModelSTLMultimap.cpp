#include "ModelSTLMultimap.h"

using namespace llvm;

namespace moya {

ModelSTLMultimap::ModelSTLMultimap(const std::string& fname,
                                   FuncID fn,
                                   const Models& ms)
    : ModelSTLMap(fname, STDKind::STD_STL_multimap, fn, ms) {
  ;
}

} // namespace moya
