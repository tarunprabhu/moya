#include "ModelReadOnly.h"
#include "ModelCustom.h"
#include "ModelSTLVector.h"
#include "Models.h"
#include "common/LangConfig.h"
#include "common/FunctionUtils.h"

#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_vector);
}

static std::string mk(const std::string& name) {
  std::stringstream ss;
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(const std::string& op : LangConfigCXX::getComparisonOperators()) {
    std::stringstream ss;
    std::string cont = getContainerName();
    ss << "std::operator" << op << "(" << cont << ", " << cont << ")";
    ms.add(new ModelSTLVector(ss.str(), ModelSTLVector::Compare, ms));
  }
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLVector::FuncID fn) {
  ms.add(new ModelSTLVector(name, fn, ms));
}

/* FIXME: The init_list models should be moved */
static AnalysisStatus model_init_list_begin(const ModelCustom& model,
                                            const Instruction* call,
                                            const Function* f,
                                            const Vector<Contents>& args,
                                            Environment& env,
                                            Store& store,
                                            const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  auto* sty = dyn_cast<StructType>(
      dyn_cast<PointerType>(moya::LLVMUtils::getArgument(f, 0)->getType())
          ->getElementType());
  auto* pty = sty->getElementType(0);
  const Addresses& conts = args.at(0).getAs<Address>(call);

  env.push(store.readAs<Address>(conts, pty, call, changed));

  return changed;
}

static AnalysisStatus model_init_list_end(const ModelCustom& model,
                                          const Instruction* call,
                                          const Function* f,
                                          const Vector<Contents>& args,
                                          Environment& env,
                                          Store& store,
                                          const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  env.push(store.getNullptr());

  return changed;
}

void Models::initSTLVector(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms, mk("vector()"), ModelSTLVector::ConstructorEmpty);
  mkModel(ms, mk("vector(allocator)"), ModelSTLVector::ConstructorEmpty);
  mkModel(ms,
          mk("vector(unsigned long, allocator)"),
          ModelSTLVector::ConstructorEmpty);
  mkModel(ms,
          mk("vector(unsigned long, *, allocator)"),
          ModelSTLVector::ConstructorElement);
  mkModel(ms, mk("vector(T, T, allocator)"), ModelSTLVector::ConstructorRange);
  mkModel(ms, mk("vector(std::vector)"), ModelSTLVector::ConstructorCopy);
  mkModel(ms,
          mk("vector(std::initializer_list, allocator)"),
          ModelSTLVector::ConstructorInitList);

  // Destructor
  mkModel(ms, mk("~vector()"), ModelSTLVector::Destructor);

  // operator =
  mkModel(ms, mk("operator=(std::vector)"), ModelSTLVector::OperatorContainer);
  mkModel(ms,
          mk("operator=(std::initializer_list)"),
          ModelSTLVector::OperatorInitList);

  // allocator
  mkModel(ms, mk("get_allocator()"), ModelSTLVector::GetAllocator);

  // assign
  mkModel(ms, mk("assign(unsigned long, *)"), ModelSTLVector::AssignMany);
  mkModel(ms, mk("assign(int, *)"), ModelSTLVector::AssignMany);
  mkModel(ms, mk("assign(T, T)"), ModelSTLVector::AssignRange);
  mkModel(
      ms, mk("assign(std::initializer_list)"), ModelSTLVector::AssignInitList);

  // Element access
  mkModel(ms, mk("at(unsigned long)"), ModelSTLVector::AccessElement);
  mkModel(ms, mk("operator[](unsigned long)"), ModelSTLVector::AccessElement);
  mkModel(ms, mk("front()"), ModelSTLVector::AccessElement);
  mkModel(ms, mk("back()"), ModelSTLVector::AccessElement);
  mkModel(ms, mk("data()"), ModelSTLVector::Begin);

  // Iterators
  mkModel(ms, mk("begin()"), ModelSTLVector::Begin);
  mkModel(ms, mk("cbegin()"), ModelSTLVector::Begin);
  mkModel(ms, mk("end()"), ModelSTLVector::End);
  mkModel(ms, mk("cend()"), ModelSTLVector::End);
  mkModel(ms, mk("rbegin()"), ModelSTLVector::Begin);
  mkModel(ms, mk("crbegin()"), ModelSTLVector::Begin);
  mkModel(ms, mk("rend()"), ModelSTLVector::End);
  mkModel(ms, mk("crend()"), ModelSTLVector::End);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLVector::Size);
  mkModel(ms, mk("size()"), ModelSTLVector::Size);
  mkModel(ms, mk("max_size()"), ModelSTLVector::Size);
  mkModel(ms, mk("capacity()"), ModelSTLVector::Size);
  // Both reserve and shrink_to_fit behave like clear in the sense that the
  // underlying data is touched in some way. So we can get away with using the
  // same model for all three methods
  mkModel(ms, mk("reserve(unsigned long)"), ModelSTLVector::Resize);
  mkModel(ms, mk("shrink_to_fit()"), ModelSTLVector::Resize);

  // Modifiers
  mkModel(ms, mk("clear()"), ModelSTLVector::Clear);

  mkModel(ms, mk("emplace_back(...)"), ModelSTLVector::EmplaceElement);
  mkModel(ms, mk("emplace(iterator, ...)"), ModelSTLVector::EmplaceElementAt);

  mkModel(ms, mk("insert(iterator, *)"), ModelSTLVector::InsertElement);
  mkModel(
      ms, mk("insert(iterator, unsigned long, *)"), ModelSTLVector::InsertMany);
  mkModel(ms, mk("insert(iterator, T, T)"), ModelSTLVector::InsertRange);
  mkModel(ms,
          mk("insert(iterator, std::initializer_list)"),
          ModelSTLVector::InsertInitList);

  mkModel(ms, mk("erase(iterator)"), ModelSTLVector::EraseElementAt);
  mkModel(ms, mk("erase(iterator, iterator)"), ModelSTLVector::EraseRange);

  mkModel(ms, mk("push_back(*)"), ModelSTLVector::AddElement);
  mkModel(ms, mk("pop_back()"), ModelSTLVector::RemoveElement);

  // The resize function will change the pointers and might also affect the
  // data, so it behaves somewhat like clear
  mkModel(ms, mk("resize(unsigned long)"), ModelSTLVector::Resize);
  // This overload of resize also assigns to the modified vector, so it
  // behaves somewhat like assign
  mkModel(ms, mk("resize(unsigned long, *)"), ModelSTLVector::AssignMany);

  mkModel(ms, mk("swap(std::vector)"), ModelSTLVector::Swap);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);

  /* FIXME: GET RID OF THESE */
  mkModel(ms,
          mk("vector(int, int, allocator)"),
          ModelSTLVector::ConstructorElement);

  /* FIXME: This should be moved to its own file */
  ms.add(
      new ModelCustom("std::initializer_list::begin()", model_init_list_begin));
  ms.add(new ModelCustom("std::initializer_list::end()", model_init_list_end));
}
