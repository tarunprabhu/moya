#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;
using namespace moya;

static inline std::string mk(const std::string& base, const std::string& name) {
  std::stringstream ss;

  ss << "std::" << base << "::" << name;

  return ss.str();
}

static inline std::string mkfs(const std::string& name) {
  return mk("fstream", name);
}

void Models::initStdFstream(const Module& module) {
  add(new ModelReadWrite(mkfs("basic_fstream()"), {0}, true));
  add(new ModelReadWrite(
      mkfs("basic_fstream(char*, std::_Ios_Openmode)"), {0, 1, 1}, true));
  add(new ModelReadWrite(
      mkfs("basic_fstream(std::basic_string, std::_Ios_Openmode"),
      {0, 1, 1},
      true));
  add(new ModelReadWrite(
      mkfs("basic_fstream(std::basic_fstream"), {0, 1}, true));
  add(new ModelReadWrite(mkfs("~basic_fstream()"), {0, 1}, true));
  add(new ModelReadWrite(mkfs("swap(std::basic_fstream"), {0, 0}, true));
  add(new ModelReadOnly(mkfs("is_open()")));
  add(new ModelReadWrite(
      mkfs("open(char*, std::_Ios_Openmode)"), {0, 1, 1}, true));
  add(new ModelReadWrite(
      mkfs("open(std::basic_string, std::_Ios_Openmode)"), {0, 1, 1}, true));
  add(new ModelReadWrite(mkfs("close()"), {0, 1}, true));
}
