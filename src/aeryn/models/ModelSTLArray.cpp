#include "ModelSTLArray.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

using namespace llvm;

namespace moya {

ModelSTLArray::ModelSTLArray(const std::string& fname,
                             STDKind cont,
                             STDKind iter,
                             STDKind citer,
                             FuncID fn,
                             const Models& ms)
    : ModelSTLContainer(fname, cont, iter, citer, fn, ms) {
  switch(fn) {
  case ModelSTLArray::Begin:
    processFn = static_cast<ExecuteFn>(&ModelSTLArray::modelBegin);
    break;
  case ModelSTLArray::End:
    processFn = static_cast<ExecuteFn>(&ModelSTLArray::modelEnd);
    break;
  default:
    // Everything else will have been initialized in the ModelSTLContainer()
    // constructor
    break;
  }
}

ModelSTLArray::ModelSTLArray(const std::string& fname,
                             FuncID fn,
                             const Models& ms)
    : ModelSTLArray(fname,
                    STDKind::STD_STL_array,
                    STDKind::STD_None,
                    STDKind::STD_None,
                    fn,
                    ms) {
  ;
}

Addresses ModelSTLArray::getBuffer(const Addresses& conts,
                                   const Instruction* call,
                                   const Function* f,
                                   Store& store,
                                   AnalysisStatus& changed) const {
  return conts;
}

Addresses ModelSTLArray::getNode(const Addresses& conts,
                                 const Instruction* call,
                                 const Function* f,
                                 Store& store,
                                 AnalysisStatus& changed) const {
  return conts;
}

AnalysisStatus ModelSTLArray::modelDestructor(const Instruction* call,
                                              const Function* f,
                                              const Vector<Contents>& args,
                                              Environment& env,
                                              Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // Nothing happens here because the array element destructors will have
  // been called already and there's nothing else that needs to be touched

  return changed;
}

AnalysisStatus ModelSTLArray::modelBegin(const Instruction* call,
                                         const Function* f,
                                         const Vector<Contents>& args,
                                         Environment& env,
                                         Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* bufTy = Metadata::getSTLValueType(f);
  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents ret = getBeginIterator(conts, call, f, store, changed);

  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, ret, bufTy, call, changed);
  else
    env.push(ret);

  return changed;
}

AnalysisStatus ModelSTLArray::modelEnd(const Instruction* call,
                                       const Function* f,
                                       const Vector<Contents>& args,
                                       Environment& env,
                                       Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* bufTy = Metadata::getSTLValueType(f);
  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents ret = getEndIterator(conts, call, f, store, changed);

  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, ret, bufTy, call, changed);
  else
    env.push(ret);

  return changed;
}

} // namespace moya
