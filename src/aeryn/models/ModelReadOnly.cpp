#include "ModelReadOnly.h"
#include "aeryn/AbstractUtils.h"
#include "common/LLVMUtils.h"

using namespace llvm;

namespace moya {

ModelReadOnly::ModelReadOnly(const std::string& fname, Model::ReturnSpec ret)
    : Model(fname, Model::Kind::ReadOnlyKind, ret) {
  ;
}

AnalysisStatus ModelReadOnly::process(const Instruction* inst,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      const Vector<Type*>& argTys,
                                      Environment& env,
                                      Store& store,
                                      const Function* caller) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(f->hasStructRetAttr())
    changed |= markWrite(args.at(0), argTys.at(0), env, store, inst);
  for(unsigned i = f->hasStructRetAttr(); i < args.size(); i++)
    changed |= markRead(args.at(i), argTys.at(i), env, store, inst);

  if(not f->getReturnType()->isVoidTy()) {
    Contents retvals;
    switch(ret) {
    case Model::ReturnSpec::Nullptr:
      retvals.insert(store.getNullptr());
      break;
    case Model::ReturnSpec::AllocateAndInitialize:
      if(not env.contains(inst)) {
        auto* objTy
            = LLVMUtils::getAnalysisTypeAs<PointerType>(inst)->getElementType();
        const Store::Address* allocated
            = AbstractUtils::allocate(objTy, store, inst, caller);
        changed |= env.add(inst, allocated);
      }
      retvals.insert(env.lookup(inst));
      break;
    case Model::ReturnSpec::Allocate:
      if(not env.contains(inst)) {
        auto* objTy
            = LLVMUtils::getAnalysisTypeAs<PointerType>(inst)->getElementType();
        const Store::Address* allocated
            = AbstractUtils::allocate(objTy, store, inst, caller);
        changed |= env.add(inst, allocated);
      }
      retvals.insert(env.lookup(inst));
      break;
    case Model::ReturnSpec::Default:
      retvals.insert(store.getRuntimeValue(f->getReturnType()));
      break;
    case Model::ReturnSpec::ArgsAll:
      for(unsigned i = 0; i < args.size(); i++)
        retvals.insert(args.at(i));
      break;
    default:
      retvals.insert(args.at(static_cast<int>(ret)));
      break;
    }
    env.push(retvals);
  }

  return changed;
}

bool ModelReadOnly::classof(const Model* model) {
  return model->getKind() == Model::Kind::ReadOnlyKind;
}

} // namespace moya
