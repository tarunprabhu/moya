#include "ModelSTLList.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

using namespace llvm;

namespace moya {

// This is the list layout in memory:
//
// list = { base*, base*, i64 }
//
// base* points to a node which contains the data
//
// node = { base*, base*, T }
//
// When a list is created, a single node is allocated into which all the
// data is stored. The pointer in the node is self-referential.
//

ModelSTLList::ModelSTLList(const std::string& fname,
                           STDKind cont,
                           STDKind iter,
                           STDKind citer,
                           FuncID fn,
                           const Models& ms)
    : ModelSTLContainer(fname, cont, iter, citer, fn, ms) {
  switch(fn) {
  case Begin:
    processFn = static_cast<ExecuteFn>(&ModelSTLList::modelBegin);
    break;
  case End:
    processFn = static_cast<ExecuteFn>(&ModelSTLList::modelEnd);
    break;
  case Merge:
    processFn = static_cast<ExecuteFn>(&ModelSTLList::modelMerge);
    break;
  case Splice:
    processFn = static_cast<ExecuteFn>(&ModelSTLList::modelSplice);
    break;
  default:
    // Everything else will have been initialized in the ModelSTLContainer()
    // constructor
    break;
  }
}

ModelSTLList::ModelSTLList(const std::string& fname,
                           FuncID fn,
                           const Models& ms)
    : ModelSTLList(fname,
                   STDKind::STD_STL_list,
                   STDKind::STD_STL_iterator_list,
                   STDKind::STD_STL_const_iterator_list,
                   fn,
                   ms) {
  ;
}

Contents ModelSTLList::getBeginIterator(const Addresses& conts,
                                        const Instruction* call,
                                        const Function* f,
                                        Store& store,
                                        AnalysisStatus& changed) const {
  Contents ret;
  for(const Address* node : getNode(conts, call, f, store, changed))
    ret.insert(node);
  return ret;
}

Addresses ModelSTLList::getIteratorNode(const Addresses& iters,
                                        const Instruction* call,
                                        const Function* f,
                                        Store& store,
                                        AnalysisStatus& changed) const {
  Type* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();

  return store.readAs<Address>(iters, pBaseTy, call, changed);
}

Addresses ModelSTLList::getIteratorDeref(const Addresses& iters,
                                         const Instruction* call,
                                         const Function* f,
                                         Store& store,
                                         AnalysisStatus& changed) const {
  Addresses ret;
  PointerType* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();
  unsigned long offset = Metadata::getSTLValueOffset(f);

  for(const auto* ptr : store.readAs<Address>(iters, pBaseTy, call, changed))
    ret.insert(store.make(ptr, offset));

  return ret;
}

Addresses ModelSTLList::getIteratorNew(const Addresses& iters,
                                       const Instruction* call,
                                       const Function* f,
                                       Store& store,
                                       AnalysisStatus& changed) const {
  PointerType* pBaseTy = Metadata::getSTLIteratorType(f)->getPointerTo();

  return store.readAs<Address>(iters, pBaseTy, call, changed);
}

AnalysisStatus ModelSTLList::updateContainerPointers(const Address* base,
                                                     const Addresses& vals,
                                                     const Instruction* call,
                                                     const Function* f,
                                                     Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* pBaseTy = Metadata::getSTLValueType(f)->getPointerTo();
  unsigned vtadj = getContainerVtableOffset(base, call, f, store);
  for(unsigned offset : {0, 8})
    for(const Content* c : vals)
      store.write(store.make(base, vtadj + offset), c, pBaseTy, call, changed);

  return changed;
}

AnalysisStatus ModelSTLList::updateNodePointers(const Address* base,
                                                const Addresses& vals,
                                                const Instruction* call,
                                                const Function* f,
                                                Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* pBaseTy = Metadata::getSTLValueType(f)->getPointerTo();
  unsigned vtadj = getContainerVtableOffset(base, call, f, store);
  for(unsigned offset : {0, 8})
    for(const Content* c : vals)
      store.write(store.make(base, vtadj + offset), c, pBaseTy, call, changed);

  return changed;
}

AnalysisStatus ModelSTLList::modelBegin(const Instruction* call,
                                        const Function* f,
                                        const Vector<Contents>& args,
                                        Environment& env,
                                        Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  PointerType* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents ret = getBeginIterator(conts, call, f, store, changed);

  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, ret, pBaseTy, call, changed);
  else
    env.push(ret);

  return changed;
}

AnalysisStatus ModelSTLList::modelEnd(const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  PointerType* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents ret = getEndIterator(conts, call, f, store, changed);

  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, ret, pBaseTy, call, changed);
  else
    env.push(ret);

  return changed;
}

AnalysisStatus ModelSTLList::modelMerge(const Instruction* call,
                                        const Function* f,
                                        const Vector<Contents>& args,
                                        Environment& env,
                                        Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts1 = args.at(0).getAs<Address>(call);
  const Addresses& conts2 = args.at(1).getAs<Address>(call);
  Addresses bufs2 = getBuffer(conts2, call, f, store, changed);

  changed |= writeToContainer(conts1, bufs2, 0, call, f, store);
  changed |= markContainerWrite(conts1, call, f, store);
  changed |= markContainerWrite(conts2, call, f, store);
  changed |= markBufferWrite(conts1, call, f, store);
  changed |= markBufferWrite(conts2, call, f, store);

  return changed;
}

AnalysisStatus ModelSTLList::modelSplice(const Instruction* call,
                                         const Function* f,
                                         const Vector<Contents>& args,
                                         Environment& env,
                                         Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts1 = args.at(0).getAs<Address>(call);
  const Addresses& conts2 = args.at(2).getAs<Address>(call);
  Addresses bufs2 = getBuffer(conts2, call, f, store, changed);

  changed |= writeToContainer(conts1, bufs2, 0, call, f, store);
  changed |= markContainerWrite(conts1, call, f, store);
  changed |= markContainerWrite(conts2, call, f, store);
  changed |= markBufferWrite(conts1, call, f, store);
  changed |= markBufferWrite(conts2, call, f, store);

  return changed;
}

} // namespace moya
