#include "ModelCustom.h"
#include "ModelNull.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/Vector.h"

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>

#include <sstream>

using namespace llvm;
using namespace moya;

static AnalysisStatus modelMemcpy(const ModelCustom& model,
                                  const Instruction* call,
                                  const Function* f,
                                  const Vector<Contents>& args,
                                  Environment& env,
                                  Store& store,
                                  const Function* caller) {
  return AbstractUtils::memcpy(call, args, env, store, caller);
}

static AnalysisStatus modelMemset(const ModelCustom& model,
                                  const Instruction* call,
                                  const Function* f,
                                  const Vector<Contents>& args,
                                  Environment& env,
                                  Store& store,
                                  const Function* caller) {
  return AbstractUtils::memset(call, args, env, store, caller);
}

static AnalysisStatus modelLLVMTrampolineInit(const ModelCustom& model,
                                              const Instruction* call,
                                              const Function* f,
                                              const Vector<Contents>& args,
                                              Environment& env,
                                              Store& store,
                                              const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const auto* addr : args.at(0).getAs<Address>(call)) {
    for(StructType* sty : store.getStructsAt(addr)) {
      const moya::StructLayout& sl = store.getStructLayout(sty);
      const Address* dst = store.make(addr, sl.getElementOffset(1));
      store.write(dst,
                  args.at(1),
                  Type::getInt8Ty(caller->getContext()),
                  call,
                  changed);
    }
  }

  return changed;
}

static AnalysisStatus modelLLVMTrampolineAdjust(const ModelCustom& model,
                                                const Instruction* call,
                                                const Function* f,
                                                const Vector<Contents>& args,
                                                Environment& env,
                                                Store& store,
                                                const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Contents ret;
  for(const auto* addr : args.at(0).getAs<Address>(call)) {
    for(StructType* sty : store.getStructsAt(addr)) {
      const moya::StructLayout& sl = store.getStructLayout(sty);
      const Address* dst = store.make(addr, sl.getElementOffset(1));
      ret.insert(store.read(dst, nullptr, call, changed));
    }
  }

  env.push(ret);

  return changed;
}

static AnalysisStatus modelStackSave(const ModelCustom& model,
                                     const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store,
                                     const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(call)) {
    Type* i8 = Type::getInt8Ty(call->getContext());
    const Address* addr = AbstractUtils::allocate(i8, store, call, caller);
    store.write(addr, store.getRuntimeScalar(), i8, call, changed);
    changed |= env.add(call, addr);
  }

  env.push(env.lookup(call));

  return changed;
}

void Models::initLibLLVM(const Module& module) {
  // These functions are for the compiler to take advantage of and don't
  // really do anything for the user code
  add(new ModelNull("llvm.lifetime.end"));
  add(new ModelNull("llvm.lifetime.start"));

  // The dbg functions do take arguments, but I don't care about them,
  // so I'm leaving it empty. If the type verifier complains at some point,
  // I'll fix it
  add(new ModelNull("llvm.dbg.declare"));
  add(new ModelNull("llvm.dbg.value"));

  add(new ModelNull("llvm.expect"));

  // These are actual intrinsics that affect user code
  add(new ModelCustom("llvm.memcpy", modelMemcpy));
  add(new ModelCustom("llvm.memset", modelMemset));
  add(new ModelCustom("llvm.memmove", modelMemcpy));

  // Stack intrinsics
  add(new ModelCustom("llvm.stacksave", modelStackSave));
  add(new ModelReadWrite("llvm.stackrestore", {0}));

  // Prefetch intrinsics
  add(new ModelReadOnly("llvm.prefetch"));

  // Trap intrinsics
  add(new ModelNull("llvm.trap"));
  add(new ModelNull("llvm.debugtrap"));

  add(new ModelReadWrite("llvm.va_start", {0}));
  add(new ModelReadWrite("llvm.va_end", {0}));
  add(new ModelReadWrite("llvm.va_copy", {0, 1}));

  add(new ModelReadOnly("llvm.eh.typeid.for"));
  add(new ModelReadOnly("llvm.eh.begincatch"));
  add(new ModelReadOnly("llvm.eh.endcatch"));
  add(new ModelReadOnly("llvm.eh.exceptionpointer"));
  add(new ModelReadWrite("llvm.eh.sjlj.setjmp", {0}));
  add(new ModelReadOnly("llvm.eh.sjlj.longjmp"));
  add(new ModelReadOnly("llvm.eh.sjlj.lsda"));
  add(new ModelReadOnly("llvm.eh.sjlj.callsite"));

  add(new ModelCustom("llvm.init.trampoline", modelLLVMTrampolineInit));
  add(new ModelCustom("llvm.adjust.trampoline", modelLLVMTrampolineAdjust));
}
