#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "ModelSTLVector.h"
#include "Models.h"
#include "common/LangConfig.h"

#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;
using namespace moya;

static std::string getIteratorName() {
  return STDConfig::getSTDName(STDKind::STD_STL_iterator_normal);
}

static std::string mkIter(const std::string& name) {
  std::stringstream ss;
  ss << getIteratorName() << "::" << name;
  return ss.str();
}

static std::string mkCompare(const std::string& op) {
  std::stringstream ss;
  std::string iter = getIteratorName();
  ss << iter << "::operator" << op << "(" << iter << ")";
  return ss.str();
}

static std::string mkStdCompare(const std::string& op) {
  std::stringstream ss;
  std::string iter = getIteratorName();
  ss << "__gnu_cxx::operator" << op << "(" << iter << ", " << iter << ")";
  return ss.str();
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLVector::FuncID fn) {
  ms.add(new ModelSTLVector(name, fn, ms));
}

void Models::initSTLVectorIterator(const Module& module) {
  Models& ms = *this;

  mkModel(ms,
          mkIter("__normal_iterator()"),
          ModelSTLVector::IteratorConstructorEmpty);
  mkModel(ms,
          mkIter("__normal_iterator(iterator)"),
          ModelSTLVector::IteratorConstructorCopy);

  // Add the iterator comparison operator overload models
  for(const std::string& op : LangConfigCXX::getComparisonOperators()) {
    mkModel(ms, mkCompare(op), ModelSTLVector::IteratorCompare);
    mkModel(ms, mkStdCompare(op), ModelSTLVector::IteratorCompare);
  }

  // Operations on iterators
  mkModel(ms, mkIter("operator*()"), ModelSTLVector::IteratorDeref);
  mkModel(ms, mkIter("operator->()"), ModelSTLVector::IteratorDeref);
  mkModel(ms, mkIter("operator[](long)"), ModelSTLVector::IteratorDeref);
  mkModel(ms, mkIter("operator++()"), ModelSTLVector::IteratorSelf);
  mkModel(ms, mkIter("operator++(int)"), ModelSTLVector::IteratorDeref);
  mkModel(ms, mkIter("operator--()"), ModelSTLVector::IteratorSelf);
  mkModel(ms, mkIter("operator--(int)"), ModelSTLVector::IteratorDeref);
  mkModel(ms, mkIter("operator+=(long)"), ModelSTLVector::IteratorSelf);
  mkModel(ms, mkIter("operator+(long)"), ModelSTLVector::IteratorDeref);
  mkModel(ms, mkIter("operator-=(long)"), ModelSTLVector::IteratorSelf);
  mkModel(ms, mkIter("operator-(long)"), ModelSTLVector::IteratorDeref);
  mkModel(ms, mkIter("base()"), ModelSTLVector::IteratorBase);
  add(new ModelReadOnly("__gnu_cxx::operator+(iterator, iterator)"));
  add(new ModelReadOnly("__gnu_cxx::operator-(iterator, iterator)"));
}
