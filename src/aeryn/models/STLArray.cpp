#include "ModelSTLArray.h"
#include "Models.h"
#include "common/LangConfig.h"

using namespace llvm;
using namespace moya;

// The vectors for this version of Clang/libstdc++ is at it's heart a
// struct whose elements are:
//
// struct array = { [n x T] }
//
// Here, n is the length of the array. Elements of the array can be changed
// but the length of the array is fixed
//
// All iterators will return a pointer to the array.
//

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_array);
}

static std::string mk(const std::string& name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "std::operator" << op << "(" << getContainerName() << ", "
       << getContainerName() << ")";
    ms.add(new ModelSTLArray(ss.str(), ModelSTLContainer::Compare, ms));
  }
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLArray(name, fn, ms));
}

void Models::initSTLArray(const Module& module) {
  Models& ms = *this;

  // Destructor
  mkModel(ms, mk("~array()"), ModelSTLArray::Destructor);

  // Element access
  mkModel(ms, mk("at(unsigned long)"), ModelSTLArray::AccessElement);
  mkModel(ms, mk("operator[](unsigned long)"), ModelSTLArray::AccessElement);
  mkModel(ms, mk("front()"), ModelSTLArray::AccessElement);
  mkModel(ms, mk("back()"), ModelSTLArray::AccessElement);
  mkModel(ms, mk("data()"), ModelSTLArray::Begin);

  // Iterators
  mkModel(ms, mk("begin()"), ModelSTLArray::Begin);
  mkModel(ms, mk("cbegin()"), ModelSTLArray::Begin);
  mkModel(ms, mk("end()"), ModelSTLArray::End);
  mkModel(ms, mk("cend()"), ModelSTLArray::End);
  mkModel(ms, mk("rbegin()"), ModelSTLArray::Begin);
  mkModel(ms, mk("crbegin()"), ModelSTLArray::Begin);
  mkModel(ms, mk("rend()"), ModelSTLArray::End);
  mkModel(ms, mk("crend()"), ModelSTLArray::End);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLArray::Size);
  mkModel(ms, mk("size()"), ModelSTLArray::Size);
  mkModel(ms, mk("max_size()"), ModelSTLArray::Size);

  // Modifiers
  mkModel(ms, mk("fill(*)"), ModelSTLArray::AddElement);
  mkModel(ms, mk("swap(std::array)"), ModelSTLArray::Swap);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);
}
