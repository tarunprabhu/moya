#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

#include <sstream>

using namespace llvm;
using namespace moya;

std::string mk(const std::string& module, const std::string& fn) {
  std::stringstream ss;
  ss << "__cantera_" << module << "_MOD_" << fn;
  return ss.str();
}

void Models::initLibCantera(const Module& module) {
  // Funcs
  add(new ModelReadWrite(mk("funcs", "ctfunc_importphase"),
                         {0, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mk("funcs", "ctfunc_phase_report"), {0, 0, 1, 1}));
  add(new ModelReadWrite(mk("funcs", "ctfunc_getcanteraerror"), {0, 1}));
  add(new ModelReadWrite(mk("funcs", "ctfunc_addcanteradirectory"), {0, 1, 1}));

  // Kinetics
  add(new ModelReadWrite(mk("kinetics", "newkinetics"), {1, 0, 1, 1, 1, 1}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_kineticstype"), {0}));
  add(new ModelReadOnly(mk("kinetics", "ctkin_kineticsstart")));
  add(new ModelReadWrite(mk("kinetics", "ctkin_kineticsspeciesindex"),
                         {0, 1, 1, 1, 1}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_ntotalspecies"), {0}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_nreactions"), {0}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_nphases"), {0}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_phaseindex"), {0, 1, 1}));
  add(new ModelReadOnly(mk("kinetics", "ctkin_reactantstoichcoeff")));
  add(new ModelReadOnly(mk("kinetics", "ctkin_productstoichcoeff")));
  add(new ModelReadOnly(mk("kinetics", "ctkin_reactiontype")));
  add(new ModelReadWrite(mk("kinetics", "ctkin_getfwdratesofprogress"),
                         {0, 0}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_getrevratesofprogress"),
                         {0, 0}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_isreversible"), {0, 1}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_getnetratesofprogress"),
                         {0, 0}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_getcreationrates"), {0, 0}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_getdestructionrates"), {0, 0}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_getnetproductionrates"),
                         {0, 0}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_multiplier"), {0, 1}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_getequilibriumconstants"),
                         {0, 0}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_getreactionstring"),
                         {0, 1, 0, 1}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_setmultiplier"), {0, 1, 0}));
  add(new ModelReadWrite(mk("kinetics", "ctkin_advancecoverages"), {0, 1}));

  // XML
  add(new ModelReadWrite(mk("xml", "new_xml_node"), {0, 1, 1, 1, 1, 1}));
  add(new ModelReadOnly(mk("xml", "ctxml_clear")));
  add(new ModelReadWrite(mk("xml", "ctxml_getattrib"), {0, 1, 0, 1, 1}));
  add(new ModelReadWrite(mk("xml", "ctxml_addattrib"), {0, 1, 1, 1, 1}));
  add(new ModelReadWrite(mk("xml", "ctxml_addcomment"), {0, 1, 1}));
  add(new ModelReadWrite(mk("xml", "ctxml_gettag"), {0, 0, 1}));
  add(new ModelReadWrite(mk("xml", "ctxml_getvalue"), {0, 0, 1}));
  add(new ModelReadWrite(mk("xml", "ctxml_child"), {0, 1, 1, 1, 1, 1, 1, 1}));
  add(new ModelReadOnly(mk("xml", "ctxml_nchildren")));
  add(new ModelReadWrite(mk("xml", "ctxml_addchild"), {0, 1, 1, 1, 1}));
  add(new ModelReadWrite(mk("xml", "ctxml_write"), {0, 1, 1}));

  // Thermo
  add(new ModelReadWrite(mk("thermo", "newthermophase"), {0, 0, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_getname"), {0, 0, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_nelements"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_nspecies"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_temperature"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_settemperature"), {0, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_density"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_setdensity"), {0, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_molardensity"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_meanmolecularweight"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_elementindex"), {0, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_speciesindex"), {0, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_getmolefractions"), {0, 0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_molefraction"), {0, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_getmassfractions"), {0, 0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_massfraction"), {0, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_setmolefractions"), {0, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_setmolefractionsbyname"),
                         {0, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_setmassfractions"), {0, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_setmassfractionsbyname"),
                         {0, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_getatomicweights"), {0, 0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_getmolecularweights"), {0, 0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_getspeciesname"),
                         {0, 1, 0, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_getelementname"),
                         {0, 1, 0, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_natoms"), {0, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_eostype"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_enthalpy_mole"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_intenergy_mole"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_entropy_mole"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_gibbs_mole"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_cp_mole"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_cv_mole"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_pressure"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_enthalpy_mass"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_intenergy_mass"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_entropy_mass"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_gibbs_mass"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_cp_mass"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_cv_mass"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_chempotentials"), {0, 0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_setpressure"), {0, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_setstate_tpx"), {0, 1, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctstring_setstate_tpx"),
                         {0, 1, 1, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_setstate_try"), {0, 1, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctstring_setstate_try"),
                         {0, 1, 1, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_setstate_hp"), {0, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_setstate_uv"), {0, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_setstate_sv"), {0, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_setstate_sp"), {0, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_equilibrate"), {0, 1, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_refpressure"), {0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_mintemp"), {0, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_maxtemp"), {0, 1}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_getenthalpies_rt"), {0, 0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_getentropies_r"), {0, 0}));
  add(new ModelReadWrite(mk("thermo", "ctthermo_getcp_r"), {0, 0, 0}));

  // Transport
  add(new ModelReadWrite(mk("transport", "ctrans_viscosity"), {0}));
  add(new ModelReadWrite(mk("transport", "ctrans_electricalconductivity"),
                         {0}));
  add(new ModelReadWrite(mk("transport", "ctrans_thermalconductivity"), {0}));
  add(new ModelReadWrite(mk("transport", "ctrans_getthermaldiffcoeffs"),
                         {0, 0}));
  add(new ModelReadWrite(mk("transport", "ctrans_getmixdiffcoeffs"), {0, 0}));
  add(new ModelReadWrite(mk("transport", "ctrans_getmixdiffcoeffsmass"),
                         {0, 0}));
  add(new ModelReadWrite(mk("transport", "ctrans_getmixdiffcoeffsmole"),
                         {0, 0}));
  add(new ModelReadWrite(mk("transport", "ctrans_getbindiffcoeffs"),
                         {0, 1, 0}));
  add(new ModelReadWrite(mk("transport", "ctrans_getmultidiffcoeffs"),
                         {0, 1, 0}));
  add(new ModelReadWrite(mk("transport", "ctrans_setparameters"),
                         {0, 1, 1, 1}));
}
