#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>

#include "common/Vector.h"

using namespace llvm;
using namespace moya;

// This contains models for libm functions. Most of the libm functions have
// different versions for different data types.
//
// The default for the floating point function is to take a double and return
// a double. A suffix of 'f' indicates that the input and output are floating
// points and suffix of 'l' indicates that the function uses long doubles.
// On x86-64, these are 80-bit floating point numbers.
//
// Some, but not all the functions have complex variants which have a 'c'
// prefix. The complex numbers themselves can be represented with float, double
// or long double types. The default is double. The 'f' and 'l' suffixes
// can also be applied to these functions
//
// For integer functions, the default argument type is int (32-bit). Prefixes
// of 'l' and 'll' are used to indicate long and long long variants. On x86-64,
// these are both represented as i64.

static inline void create(Models& ms, std::string fname, bool cxx) {
  ms.add(new ModelReadOnly(fname));

  // The double underscore versions are here because GCC sometimes replaces
  // these functions with its own builtins
  ms.add(new ModelReadOnly("__" + fname));

  if(cxx)
    ms.add(new ModelReadOnly("std::" + fname));
}

// Construct all the floating point variants of functions which take 1 or more
// arguments of a particular type and return an argument of the same type
static void createf(Models& ms, std::string fn, bool complex, bool cxx) {
  create(ms, fn, cxx);
  create(ms, fn + "f", cxx);
  create(ms, fn + "l", cxx);
  if(complex) {
    create(ms, "c" + fn, false);
    create(ms, "c" + fn + "f", false);
    create(ms, "c" + fn + "l", false);
  }
}

// Similar to the one above except for integers
static void createi(Models& ms, std::string fn, bool cxx) {
  create(ms, fn, cxx);
  create(ms, "l" + fn, cxx);
  create(ms, "ll" + fn, cxx);
}

// For functions only intended for complex numbers
static void createc(Models& ms, std::string fn) {
  create(ms, fn, false);
  create(ms, fn + "f", false);
  create(ms, fn + "l", false);
}

void Models::initLibM(const Module& module) {
  Models& ms = *this;

  createf(ms, "sin", true, true);
  createf(ms, "cos", true, true);
  createf(ms, "tan", true, true);

  add(new ModelReadWrite("sincos", {1, 0, 0}));
  add(new ModelReadWrite("sincosf", {1, 0, 0}));
  add(new ModelReadWrite("sincosl", {1, 0, 0}));

  createf(ms, "asin", true, true);
  createf(ms, "acos", true, true);
  createf(ms, "atan", true, true);
  createf(ms, "atan2", true, true);

  createf(ms, "sinh", true, true);
  createf(ms, "cosh", true, true);
  createf(ms, "tanh", true, true);

  createf(ms, "asinh", true, true);
  createf(ms, "acosh", true, true);
  createf(ms, "atanh", true, true);

  createf(ms, "exp", true, true);
  createf(ms, "exp2", false, true);
  createf(ms, "exp10", false, false);
  createf(ms, "expm1", false, true);

  createf(ms, "pow", true, true);
  createf(ms, "pow10", false, true);

  createf(ms, "log", true, true);
  createf(ms, "log2", false, true);
  createf(ms, "log10", true, true);
  createf(ms, "log1p", false, true);
  createf(ms, "logb", false, true);
  createf(ms, "ilogb", false, true);

  createf(ms, "cbrt", false, true);

  createf(ms, "hypot", false, true);

  createf(ms, "sqrt", true, true);

  createf(ms, "ceil", false, true);
  createf(ms, "floor", false, true);
  createf(ms, "trunc", false, true);

  createf(ms, "rint", false, true);
  createf(ms, "nearbyint", false, true);
  createf(ms, "lrint", false, true);
  createf(ms, "llrint", false, true);

  createf(ms, "round", false, true);
  createf(ms, "lround", false, true);
  createf(ms, "llround", false, true);

  createf(ms, "fmod", false, true);
  createf(ms, "drem", false, false);
  createf(ms, "remainder", false, false);
  add(new ModelReadWrite("remquo", {1, 1, 0}));
  add(new ModelReadWrite("remquof", {1, 1, 0}));
  add(new ModelReadWrite("remquol", {1, 1, 0}));

  createf(ms, "fabs", false, true);
  createi(ms, "abs", true);
  add(new ModelReadOnly("cabs"));
  add(new ModelReadOnly("cabsf"));
  add(new ModelReadOnly("cabsl"));

  createf(ms, "copysign", false, true);
  createf(ms, "nextafter", false, true);
  createf(ms, "nexttoward", false, true);

  add(new ModelReadWrite("modf", {1, 0}));
  add(new ModelReadWrite("std::modf", {1, 0}));
  add(new ModelReadWrite("modff", {1, 0}));
  add(new ModelReadWrite("modfl", {1, 0}));

  add(new ModelReadOnly("nan"));
  add(new ModelReadOnly("std::nan"));
  add(new ModelReadOnly("nanf"));
  add(new ModelReadOnly("std::nanf"));
  add(new ModelReadOnly("nanl"));
  add(new ModelReadOnly("std::nanl"));

  add(new ModelReadOnly("fpclassify"));
  createf(ms, "isinf", false, true);
  createf(ms, "isnan", false, true);
  createf(ms, "isfinite", false, true);

  createf(ms, "fmin", false, false);
  createf(ms, "fmax", false, false);
  createf(ms, "fdim", false, false);
  createf(ms, "fma", false, true);

  add(new ModelReadWrite("frexp", {1, 0}));
  add(new ModelReadWrite("std::frexp", {1, 0}));
  add(new ModelReadWrite("frexpf", {1, 0}));
  add(new ModelReadWrite("frexpl", {1, 0}));

  add(new ModelReadOnly("ldexp"));
  add(new ModelReadOnly("ldexpf"));
  add(new ModelReadOnly("ldexpl"));

  createf(ms, "scalb", false, true);
  createf(ms, "scalbln", false, true);
  createf(ms, "scalbn", false, true);

  createf(ms, "significand", false, false);

  createf(ms, "erf", false, true);
  createf(ms, "erfc", false, true);
  createf(ms, "lgamma", false, true);
  add(new ModelReadWrite("lgamma_r", {1, 0}));
  add(new ModelReadWrite("lgammaf_r", {1, 0}));
  add(new ModelReadWrite("lgammal_r", {1, 0}));
  createf(ms, "gamma", false, false);
  createf(ms, "tgamma", false, true);

  createf(ms, "j0", false, false);
  createf(ms, "j1", false, false);
  add(new ModelReadOnly("jn"));
  add(new ModelReadOnly("jnf"));
  add(new ModelReadOnly("jnl"));

  createf(ms, "y1", false, false);
  add(new ModelReadOnly("yn"));
  add(new ModelReadOnly("ynf"));
  add(new ModelReadOnly("ynl"));

  createc(ms, "creal");
  createc(ms, "cimag");
  createc(ms, "conj");
  createc(ms, "carg");
  createc(ms, "cproj");

  add(new ModelReadOnly("feclearexcept"));
  add(new ModelReadOnly("fetestexcept"));
  add(new ModelReadOnly("feraiseexcept"));
  add(new ModelReadWrite("fegetexceptflag", {0, 1}));
  add(new ModelReadOnly("fesetexceptflag"));
  add(new ModelReadOnly("fesetround"));
  add(new ModelReadOnly("fegetround"));
  add(new ModelReadWrite("fegetenv", {0}));
  add(new ModelReadOnly("fesetenv"));
  add(new ModelReadWrite("feholdexcept", {0}));
  add(new ModelReadOnly("feupdateenv"));

  add(new ModelReadOnly("std::fpclassify"));
  add(new ModelReadOnly("std::isnormal"));
  add(new ModelReadOnly("std::signbit"));
  add(new ModelReadOnly("std::isgreater"));
  add(new ModelReadOnly("std::isgreaterequal"));
  add(new ModelReadOnly("std::isless"));
  add(new ModelReadOnly("std::islessequal"));
  add(new ModelReadOnly("std::islessgreater"));
  add(new ModelReadOnly("std::isunordered"));
}
