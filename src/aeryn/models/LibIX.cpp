#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>

#include "aeryn/AbstractUtils.h"
#include "common/LLVMUtils.h"
#include "common/StringUtils.h"
#include "common/Vector.h"

using llvm::dyn_cast;

using namespace moya;

// This is just a workaround to get PlasCom2 to work.
// objects in IX inherit from std::istream which causes all sorts of problems
// because a whole lot of vtable madness ends up getting imported into the
// PlasCom2 IR. This is impossible to statically analyze. So to try and get
// around it, create models for LibIX

static const Address* allocate(llvm::Type*,
                               const llvm::Instruction*,
                               Environment&,
                               Store&,
                               const llvm::Function*);

static const Address* allocate_string(llvm::StructType* sty,
                                      const llvm::Instruction* call,
                                      Environment& env,
                                      Store& store,
                                      const llvm::Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  const llvm::Module& module = *LLVMUtils::getModule(call);
  llvm::LLVMContext& context = module.getContext();
  llvm::Type* i8 = llvm::Type::getInt8Ty(context);
  llvm::Type* i64 = llvm::Type::getInt64Ty(context);
  const Content* rtScalar = store.getRuntimeScalar();

  const Address* buf = allocate(i8, call, env, store, caller);

  // moya.std.string = type { i8*, i64, i64, [8 x i8] }
  const Address* str
      = AbstractUtils::allocate(sty, store, call, caller, CellFlags());

  store.write(str, buf, i8->getPointerTo(), call, changed);
  store.write(store.make(str, 8), rtScalar, i64, call, changed);
  store.write(store.make(str, 16), rtScalar, i64, call, changed);

  return str;
}

static const Address* allocate_scalar_struct(llvm::StructType* sty,
                                             const llvm::Instruction* call,
                                             Environment& env,
                                             Store& store,
                                             const llvm::Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  const llvm::Module& module = *LLVMUtils::getModule(caller);
  const llvm::DataLayout& dl = module.getDataLayout();
  const llvm::StructLayout& sl = *dl.getStructLayout(sty);
  const Content* rtScalar = store.getRuntimeScalar();

  const Address* buf
      = AbstractUtils::allocate(sty, store, call, caller, CellFlags());
  for(unsigned i = 0; i < sty->getNumElements(); i++) {
    uint64_t offset = sl.getElementOffset(i);
    llvm::Type* field = sty->getElementType(i);
    store.write(store.make(buf, offset), rtScalar, field, call, changed);
  }

  return buf;
}

static llvm::StructType* getSTDStringType(llvm::StructType* sty,
                                          const llvm::Module& module) {
  llvm::StructType* stringTy = moya::LLVMUtils::getMoyaStdStringType(module);
  for(llvm::StructType* s : moya::LLVMUtils::getEquivalent(sty, module))
    if(s == stringTy)
      return stringTy;
  return nullptr;
}

static const Address* allocate(llvm::Type* type,
                               const llvm::Instruction* call,
                               Environment& env,
                               Store& store,
                               const llvm::Function* caller) {
  const llvm::Module& module = *LLVMUtils::getModule(caller);

  if(LLVMUtils::isScalarTy(type)) {
    const Address* buf
        = AbstractUtils::allocate(type, store, call, caller, CellFlags());
    store.write(buf, store.getRuntimeScalar(), type);
    return buf;
  } else if(auto* pty = dyn_cast<llvm::PointerType>(type)) {
    moya_error("Unsupported type to allocate in generic constructor:\n"
               << "  type: " << *pty);
  } else if(auto* aty = dyn_cast<llvm::ArrayType>(type)) {
    moya_error("Unsupported type to allocate in generic constructor:\n"
               << "  type: " << *aty);
  } else if(auto* sty = dyn_cast<llvm::StructType>(type)) {
    if(llvm::StructType* strTy = getSTDStringType(sty, module))
      return allocate_string(strTy, call, env, store, caller);
    else if(LLVMUtils::isScalarStructTy(sty))
      return allocate_scalar_struct(sty, call, env, store, caller);
    else
      moya_error("Unsupported type to allocate in generic constructor:\n"
                 << "  type: " << *sty);
  } else {
    moya_error("Unsupported type to allocate in generic constructor:\n"
               << "  type: " << *type);
  }

  return nullptr;
}

// static AnalysisStatus model_getenv(const ModelCustom& model,
//                                    const llvm::Instruction* call,
//                                    const llvm::Function* f,
//                                    const Vector<Contents>& args,
//                                    Environment& env,
//                                    Store& store,
//                                    const llvm::Function* caller) {
//   AnalysisStatus changed = AnalysisStatus::Unchanged;
//   const llvm::Module& module = *moya::LLVMUtils::getModule(caller);

//   if(not env.contains(call)) {
//     // This returns a string that will have been created when the object
//     // is constructed. But we can't do much with the contents of a string
//     // anyway, so we just return some string
//     llvm::StructType* strTy = moya::LLVMUtils::getMoyaStdStringType(module);
//     const Address* str = allocate_string(strTy, call, env, store, caller);

//     changed |= AnalysisStatus::Changed;

//     env.push(str);
//   } else {
//     env.push(env.lookup(call));
//   }

//   return changed;
// }

static AnalysisStatus write(const Addresses& addrs,
                            uint64_t offset,
                            const Content* c,
                            llvm::Type* type,
                            const llvm::Instruction* call,
                            Store& store) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const Address* addr : addrs)
    store.write(store.make(addr, offset), c, type, call, changed);

  return changed;
}

static AnalysisStatus init_object(const Addresses& addrs,
                                  uint64_t offset,
                                  llvm::Type* type,
                                  const llvm::Instruction* call,
                                  Environment& env,
                                  Store& store,
                                  const llvm::Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= write(addrs, offset, store.getRuntimeScalar(), type, call, store);

  return changed;
}

static AnalysisStatus init_object(const Addresses& addrs,
                                  uint64_t offset,
                                  llvm::PointerType* pty,
                                  const llvm::Instruction* call,
                                  Environment& env,
                                  Store& store,
                                  const llvm::Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // If this is a pointer to an opaque struct, there's not much we can
  // do with it anyway, so just set it to NULL
  if(auto* sty = dyn_cast<llvm::StructType>(pty->getElementType())) {
    if(sty->isOpaque())
      changed |= write(addrs, offset, store.getNullptr(), pty, call, store);
  } else {
    moya_error("Unsupported type in generic construct\n"
               << "  type: " << *pty << "\n"
               << "   off: " << offset << "\n"
               << getDiagnostic(call));
  }

  return changed;
}

static AnalysisStatus init_object(const Addresses& addrs,
                                  uint64_t offset,
                                  llvm::ArrayType* aty,
                                  const llvm::Instruction* call,
                                  Environment& env,
                                  Store& store,
                                  const llvm::Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  llvm::Type* ety = LLVMUtils::getFlattenedElementType(aty);
  if(not LLVMUtils::isScalarTy(ety))
    moya_error("Unsupported non-scalar array type in generic construct:\n"
               << "  type: " << *aty << "\n"
               << "   off: " << offset << "\n"
               << getDiagnostic(call));
  changed |= write(addrs, offset, store.getRuntimeScalar(), ety, call, store);

  return changed;
}

static AnalysisStatus init_object(const Addresses& addrs,
                                  uint64_t offset,
                                  llvm::StructType* sty,
                                  const llvm::Instruction* call,
                                  Environment& env,
                                  Store& store,
                                  const llvm::Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const llvm::Module& module = *LLVMUtils::getModule(caller);
  llvm::LLVMContext& context = module.getContext();
  llvm::Type* i8 = llvm::Type::getInt8Ty(context);
  llvm::Type* i64 = llvm::Type::getInt64Ty(context);
  llvm::Type* pi8 = llvm::Type::getInt8PtrTy(context);
  const Content* rtScalar = store.getRuntimeScalar();

  if(getSTDStringType(sty, module)) {
    // moya.std.string = type {i8*, i64, i64, [8 x i8]}
    const Address* buf = allocate(i8, call, env, store, caller);
    for(const Address* addr : addrs) {
      store.write(store.make(addr, offset + 0), buf, pi8, call, changed);
      store.write(store.make(addr, offset + 8), rtScalar, i64, call, changed);
      store.write(store.make(addr, offset + 16), rtScalar, i64, call, changed);
    }
  } else if(sty->hasName()
            and StringUtils::startswith(sty->getName(), "class.std::vector")) {
    // FIXME: THIS MUST GO AWAY FAST!
    //
    // Because this is not compiled with Moya, we won't have Moya's
    // vector types that can be used to determine what the value type
    // is etc. So we have to do it the old-fashoined way
    auto* pValTy
        = dyn_cast<llvm::PointerType>(LLVMUtils::getFlattenedTypes(sty).at(0));
    llvm::Type* valTy = pValTy->getElementType();
    const Address* buf = allocate(valTy, call, env, store, caller);
    for(const Address* addr : addrs) {
      // moya.std.vector = type { T*, T*, T* }
      store.write(store.make(addr, offset + 0), buf, pValTy);
      store.write(store.make(addr, offset + 8), buf, pValTy);
      store.write(store.make(addr, offset + 16), buf, pValTy);
    }
  } else {
    moya_error("Unsupported struct type in generic construct:\n"
               << "  type: " << *sty << "\n"
               << "   off: " << offset << "\n"
               << getDiagnostic(call));
  }

  return changed;
}

// In many of these cases, we are constructing an object which requires
// other nested objects to be constructed. To do this correctly would be
// more trouble than I am particularly keen on accepting. So for the moment,
// this will try to only allocate strings and vectors, but only if they are
// single-level. Eventually, this will become smarter.
//
static AnalysisStatus model_construct_object(const ModelCustom& model,
                                             const llvm::Instruction* call,
                                             const llvm::Function* f,
                                             const Vector<Contents>& args,
                                             Environment& env,
                                             Store& store,
                                             const llvm::Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(call)) {
    const Addresses& addrs = args.at(0).getAs<Address>(call);
    llvm::StructType* clss = dyn_cast<llvm::StructType>(
        LLVMUtils::getInnermost(LLVMUtils::getArgument(f, 0)));

    const llvm::Module& module = *LLVMUtils::getModule(f);
    const llvm::DataLayout dl = module.getDataLayout();
    const llvm::StructLayout& sl = *dl.getStructLayout(clss);

    for(unsigned i = 0; i < clss->getNumElements(); i++) {
      uint64_t offset = sl.getElementOffset(i);
      llvm::Type* field = clss->getElementType(i);

      if(LLVMUtils::isScalarTy(field))
        changed |= init_object(addrs, offset, field, call, env, store, caller);
      else if(auto* pty = dyn_cast<llvm::PointerType>(field))
        changed |= init_object(addrs, offset, pty, call, env, store, caller);
      else if(auto* aty = dyn_cast<llvm::ArrayType>(field))
        changed |= init_object(addrs, offset, aty, call, env, store, caller);
      else if(auto* sty = dyn_cast<llvm::StructType>(field))
        changed |= init_object(addrs, offset, sty, call, env, store, caller);
      else
        moya_error("Unexpected type in generic construct:\n"
                   << "  type: " << *field << "\n"
                   << "   idx: " << i << "\n"
                   << "   off: " << offset << "\n"
                   << "  clss: " << *clss);
    }
    env.add(call);

    changed |= AnalysisStatus::Changed;
  }

  return changed;
}

void Models::initLibIX(const llvm::Module& module) {
  Models& ms = *this;

  add(new ModelReadWrite(
      "ix::sys::operator<<(std::ostream&, ix::sys::platform_info&)",
      {Model::ArgSpec::ReadWrite, Model::ArgSpec::ReadOnly},
      true));
  add(new ModelCustom("ix::sys::PlatformInfo(std::basic_string&)",
                      model_construct_object));
  add(new ModelReadWrite("ix::sys::SystemInfo()",
                         {Model::ArgSpec::AllocateString}));
  add(new ModelReadWrite("ix::sys::LogTime()",
                         {Model::ArgSpec::AllocateString}));
  // add(new ModelCustom("ix::sys::TokenizePath()", model_tokenize_path)
  add(new ModelReadWrite(
      "ix::sys::TempFileName(std::basic_string&, std::basic_string&)",
      {Model::ArgSpec::AllocateString, Model::ArgSpec::ReadOnly}));
  add(new ModelReadOnly("ix::sys::OpenTemp()"));
  add(new ModelReadWrite(
      "ix::sys::Hostname(long)",
      {Model::ArgSpec::AllocateString, Model::ArgSpec::ReadOnly}));
  add(new ModelReadWrite(
      "ix::sys::StripDirs(std::basic_string&)",
      {Model::ArgSpec::AllocateString, Model::ArgSpec::ReadOnly}));
  add(new ModelReadWrite("ix::sys::CWD()", {Model::ArgSpec::AllocateString}));
  add(new ModelReadOnly(
      "ix::sys::SymLink(std::basic_string&, std::basic_string&)"));
  add(new ModelReadOnly(
      "ix::sys::SafeRemove(std::basic_string&, std::basic_string&)"));
  add(new ModelReadOnly("ix::sys::ChDir(std::basic_string&)"));
  add(new ModelReadOnly("ix::sys::FILEEXISTS(std::basic_string&)"));
  add(new ModelReadOnly("ix::sys::ISDIR(std::basic_string&)"));
  add(new ModelReadOnly("ix::sys::ISLINK(std::basic_string&)"));
  add(new ModelReadOnly("ix::sys::CreateDirectory(std::basic_string&)"));
  add(new ModelReadWrite(
      "ix::sys::ResolveLink(std::basic_string&)",
      {Model::ArgSpec::AllocateString, Model::ArgSpec::ReadOnly}));
  add(new ModelReadOnly("ix::sys::Remove(std::basic_string&)"));
  add(new ModelReadOnly(
      "ix::sys::Rename(std::basic_string&, std::basic_string&)"));

  // FIXME: This is not correct but I don't care right now
  add(new ModelReadOnly("ix::sys::Environment::Environment()"));
}
