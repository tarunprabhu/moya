#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"

using namespace llvm;
using namespace moya;

void Models::initLibCharm(const Module& module) {
  add(new ModelReadOnly("CmiPrintf"));
  add(new ModelReadOnly("CmiAbort"));
  add(new ModelReadOnly("CmiError"));
}
