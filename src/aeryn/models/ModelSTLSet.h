#ifndef MOYA_MODELS_MODEL_STL_SET_H
#define MOYA_MODELS_MODEL_STL_SET_H

#include "ModelSTLTree.h"

namespace moya {

class ModelSTLSet : public ModelSTLTree {
protected:
  ModelSTLSet(const std::string&, STDKind, FuncID, const Models&);

public:
  ModelSTLSet(const std::string&, FuncID, const Models&);

  virtual ~ModelSTLSet() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_SET_H
