#ifndef MOYA_MODELS_MODEL_STL_LIST_H
#define MOYA_MODELS_MODEL_STL_LIST_H

#include "ModelSTLContainer.h"

namespace moya {

class ModelSTLList : public ModelSTLContainer {
protected:
  virtual AnalysisStatus updateContainerPointers(const Address*,
                                                 const Addresses&,
                                                 const llvm::Instruction*,
                                                 const llvm::Function*,
                                                 Store&) const;

  virtual AnalysisStatus updateNodePointers(const Address*,
                                            const Addresses&,
                                            const llvm::Instruction*,
                                            const llvm::Function*,
                                            Store&) const;

  virtual Contents getBeginIterator(const Addresses&,
                                    const llvm::Instruction*,
                                    const llvm::Function*,
                                    Store&,
                                    AnalysisStatus&) const;
  virtual IteratorFn getIteratorNode;
  virtual IteratorFn getIteratorDeref;
  virtual IteratorFn getIteratorNew;

  virtual ModelFn modelBegin;
  virtual ModelFn modelEnd;
  virtual ModelFn modelMerge;
  virtual ModelFn modelSplice;

protected:
  ModelSTLList(const std::string&,
               STDKind,
               STDKind,
               STDKind,
               FuncID,
               const Models&);

public:
  ModelSTLList(const std::string&, FuncID, const Models&);
  virtual ~ModelSTLList() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_LIST_H
