#ifndef MOYA_MODELS_MODEL_STL_UNORDERED_SET_H
#define MOYA_MODELS_MODEL_STL_UNORDERED_SET_H

#include "ModelSTLHashtable.h"

namespace moya {

class ModelSTLUnorderedSet : public ModelSTLHashtable {
protected:
  ModelSTLUnorderedSet(const std::string&, STDKind, FuncID, const Models&);

public:
  ModelSTLUnorderedSet(const std::string&, FuncID, const Models&);

  virtual ~ModelSTLUnorderedSet() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_UNORDERED_SET_H
