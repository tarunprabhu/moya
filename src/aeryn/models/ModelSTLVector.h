#ifndef MOYA_MODELS_MODEL_STL_VECTOR_H
#define MOYA_MODELS_MODEL_STL_VECTOR_H

#include "ModelSTLContainer.h"

namespace moya {

class ModelSTLVector : public ModelSTLContainer {
protected:
  virtual AnalysisStatus updateContainerPointers(const Address*,
                                                 const Addresses&,
                                                 const llvm::Instruction*,
                                                 const llvm::Function*,
                                                 Store&) const;

  virtual IteratorFn getIteratorNode;
  virtual IteratorFn getIteratorDeref;

  virtual ModelFn modelInsertRange;
  virtual ModelFn modelBegin;
  virtual ModelFn modelEnd;
  virtual ModelFn modelIteratorBase;

protected:
  ModelSTLVector(const std::string&,
                 STDKind,
                 STDKind,
                 STDKind,
                 FuncID,
                 const Models&);

public:
  ModelSTLVector(const std::string&, FuncID, const Models&);

  virtual ~ModelSTLVector() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_VECTOR_H
