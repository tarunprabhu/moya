#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

using namespace llvm;
using namespace moya;

static AnalysisStatus modelParallel(const ModelCustom& model,
                                    const Instruction* call,
                                    const Function* f,
                                    const Vector<Contents>& args,
                                    Environment& env,
                                    Store& store,
                                    const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(call)) {
    for(const auto* code :
        store.readAs<ContentCode>(args.at(0), nullptr, call, changed))
      if(const Function* g = code->getFunction())
        store.write(env.lookupArg(LLVMUtils::getArgument(g, 0)),
                    args.at(1),
                    nullptr,
                    call,
                    changed);
    env.add(call);
  }

  return changed;
}

void Models::initLibGOMP(const Module& module) {
  add(new ModelCustom("GOMP_parallel", modelParallel));

  add(new ModelReadOnly("GOMP_critical_start"));
  add(new ModelReadOnly("GOMP_critical_end"));
  add(new ModelReadOnly("GOMP_atomic_start"));
  add(new ModelReadOnly("GOMP_atomic_end"));
  add(new ModelReadOnly("GOMP_barrier"));
  add(new ModelReadOnly("GOMP_ordered_start"));
  add(new ModelReadOnly("GOMP_ordered_end"));
}
