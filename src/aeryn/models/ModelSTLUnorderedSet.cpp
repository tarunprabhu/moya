#include "ModelSTLUnorderedSet.h"

using namespace llvm;

namespace moya {

ModelSTLUnorderedSet::ModelSTLUnorderedSet(const std::string& fname,
                                           STDKind cont,
                                           FuncID fn,
                                           const Models& ms)
    : ModelSTLHashtable(fname, cont, fn, ms) {
  ;
}

ModelSTLUnorderedSet::ModelSTLUnorderedSet(const std::string& fname,
                                           FuncID fn,
                                           const Models& ms)
    : ModelSTLUnorderedSet(fname, STDKind::STD_STL_unordered_set, fn, ms) {
  ;
}

} // namespace moya
