#ifndef MOYA_MODELS_STL_PAIR_H
#define MOYA_MODELS_STL_PAIR_H

#include "ModelSTLContainer.h"

namespace moya {

class ModelSTLPair : public ModelSTLContainer {
protected:
  virtual Addresses getBuffer(const Addresses&,
                              const llvm::Instruction*,
                              const llvm::Function*,
                              Store&,
                              AnalysisStatus&) const;
  virtual Addresses getNode(const Addresses&,
                            const llvm::Instruction*,
                            const llvm::Function*,
                            Store&,
                            AnalysisStatus&) const;

  virtual AnalysisStatus writeToContainer(const Addresses&,
                                          const Contents&,
                                          const llvm::Instruction*,
                                          const llvm::Function*,
                                          Store&) const;

  virtual ModelFn modelConstructorEmpty;
  virtual ModelFn modelConstructorElement;
  virtual ModelFn modelConstructorCopy;
  virtual ModelFn modelDestructor;
  virtual ModelFn modelOperatorContainer;
  virtual ModelFn modelSwap;

protected:
  ModelSTLPair(const std::string&,
               STDKind,
               STDKind,
               STDKind,
               FuncID,
               const Models&);

public:
  ModelSTLPair(const std::string&, FuncID, const Models&);
  virtual ~ModelSTLPair() = default;
};

} // namespace moya

#endif // MOYA_MODELS_STL_PAIR_H
