#include "ModelSTLUnorderedMultimap.h"

using namespace llvm;

namespace moya {

ModelSTLUnorderedMultimap::ModelSTLUnorderedMultimap(const std::string& fname,
                                                     FuncID fn,
                                                     const Models& ms)
    : ModelSTLUnorderedMap(fname, STDKind::STD_STL_unordered_multimap, fn, ms) {
  ;
}

} // namespace moya
