#ifndef MOYA_MODELS_MODEL_MPI_READ_WRITE_H
#define MOYA_MODELS_MODEL_MPI_READ_WRITE_H

#include "ModelReadWrite.h"

namespace moya {

// MPI functions take "special" parameters - for instance MPI_STATUS_IGNORE
// which is a constant and is often implemented as an integer which gets
// passed to functions which expect a pointer. We need to special case for
// situations where these parameters may be passed and not try to recursively
// mark the arguments in those cases
class ModelMPIReadWrite : public ModelReadWrite {
public:
  ModelMPIReadWrite(const std::string&,
                    const std::initializer_list<int>&,
                    Model::ReturnSpec = Model::ReturnSpec::Default);

  virtual ~ModelMPIReadWrite() = default;

  virtual AnalysisStatus process(const llvm::Instruction*,
                                 const llvm::Function*,
                                 const Vector<Contents>&,
                                 const Vector<llvm::Type*>&,
                                 Environment&,
                                 Store&,
                                 const llvm::Function*) const;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_MPI_READ_WRITE_H
