#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/CellFlags.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;
using namespace moya;

static AnalysisStatus model_malloc(const ModelCustom& model,
                                   const Instruction* call,
                                   const Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= AbstractUtils::malloc(call, env, store, caller);

  return changed;
}

static AnalysisStatus model_calloc(const ModelCustom& model,
                                   const Instruction* call,
                                   const Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= AbstractUtils::calloc(call, env, store, caller);

  return changed;
}

static AnalysisStatus model_realloc(const ModelCustom& model,
                                    const Instruction* call,
                                    const Function* f,
                                    const Vector<Contents>& args,
                                    Environment& env,
                                    Store& store,
                                    const Function* caller) {
  // For the moment, we assume that nothing is reallocated by the function
  // This seems alright because the current Moya analyses will gain nothing by
  // assuming that realloc does reallocate the buffer.

  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // For some reason, there is this bizarre thing that realloc is defined
  // with its arguments "backwards" i.e. (i8*, i64) vs. (i64, i8*)
  // Since we only want to associate an allocator with the pointer, we look
  // for the right arguments the hard way.
  Addresses ptrs;
  for(const Content* c : args.at(0))
    if(const auto* addr = dyn_cast<Address>(c))
      ptrs.insert(addr);
    else if(const auto* scalar = dyn_cast<ContentScalar>(c))
      if(scalar->getIntegerValue() != Store::null)
        moya_error("Need to fix realloc");
  // ptrs.insert(store.make(scalar->getIntegerValue()));

  changed |= AbstractUtils::realloc(call, ptrs, env, store, caller);

  return changed;
}

static AnalysisStatus model_aligned_alloc(const ModelCustom& model,
                                          const Instruction* call,
                                          const Function* f,
                                          const Vector<Contents>& args,
                                          Environment& env,
                                          Store& store,
                                          const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= AbstractUtils::malloc(call, env, store, caller);

  return changed;
}

static AnalysisStatus model_memalign(const ModelCustom& model,
                                     const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store,
                                     const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= AbstractUtils::malloc(call, env, store, caller);

  return changed;
}

static AnalysisStatus model_posix_memalign(const ModelCustom& model,
                                           const Instruction* call,
                                           const Function* f,
                                           const Vector<Contents>& args,
                                           Environment& env,
                                           Store& store,
                                           const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(call)) {
    Type* bufTy = moya::Metadata::getCallAllocated(call);
    for(const auto* addr : args.at(0).getAs<Address>(call)) {
      const Address* buf = AbstractUtils::allocate(bufTy, store, call, caller);
      store.write(addr, buf, bufTy->getPointerTo(), call, changed);
    }
  }

  env.push(store.getRuntimeScalar());

  return changed;
}

static AnalysisStatus model_free(const ModelCustom& model,
                                 const Instruction* call,
                                 const Function* f,
                                 const Vector<Contents>& args,
                                 Environment& env,
                                 Store& store,
                                 const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= AbstractUtils::free(
      call, args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, caller);

  return changed;
}

static AnalysisStatus model___ret_allocated_str(const ModelCustom& model,
                                                const Instruction* call,
                                                const Function* f,
                                                const Vector<Contents>& args,
                                                Environment& env,
                                                Store& store,
                                                const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(call)) {
    Type* i8 = Type::getInt8Ty(f->getContext());
    const Address* addr = AbstractUtils::allocate(
        i8, store, call, caller, CellFlags::Initialized);
    changed |= env.add(call, addr);
  }
  env.push(env.lookup(call));

  return changed;
}

static AnalysisStatus model_asprintf(const ModelCustom& model,
                                     const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store,
                                     const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(call)) {
    Type* i8 = Type::getInt8Ty(f->getContext());
    Type* pi8 = i8->getPointerTo();

    const Address* allocated = AbstractUtils::allocate(
        i8, store, call, caller, CellFlags::Initialized);
    for(const auto* ptr : args.at(0).getAs<Address>(call))
      store.write(ptr, allocated, pi8, call, changed);
  }
  env.push(store.getRuntimeScalar());

  return changed;
}

static AnalysisStatus model_strdup(const ModelCustom& model,
                                   const Instruction* call,
                                   const Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);

  if(env.contains(call)) {
    env.push(env.lookup(call));
  } else {
    Type* i8 = Type::getInt8Ty(call->getContext());
    const Address* allocated = store.allocate(i8, CellFlags::LibAlloc, false);
    store.write(allocated, store.getRuntimeScalar(), i8, call, changed);
    env.push(allocated);
  }

  return changed;
}

static AnalysisStatus model_fopen(const ModelCustom& model,
                                  const Instruction* call,
                                  const Function* f,
                                  const Vector<Contents>& args,
                                  Environment& env,
                                  Store& store,
                                  const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* filetype = dyn_cast<PointerType>(f->getReturnType())->getElementType();
  if(env.contains(call))
    env.push(env.lookup(call));
  else
    env.push(store.allocate(filetype, CellFlags::LibAlloc, true));
  return changed;
}

static AnalysisStatus model_sscanf(const ModelCustom& model,
                                   const Instruction* call,
                                   const Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils ::getArgumentType(f, 0), env, store, call);
  changed |= model.markRead(
      args.at(1), LLVMUtils::getArgumentType(f, 1), env, store, call);
  for(unsigned i = 2; i < args.size(); i++)
    changed |= model.markWrite(
        args.at(i), call->getOperand(i)->getType(), env, store, call);
  env.push(store.getRuntimeScalar());
  return changed;
}

static AnalysisStatus model_scanf(const ModelCustom& model,
                                  const Instruction* call,
                                  const Function* f,
                                  const Vector<Contents>& args,
                                  Environment& env,
                                  Store& store,
                                  const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  for(unsigned i = 1; i < args.size(); i++)
    changed |= model.markWrite(
        args.at(i), call->getOperand(i)->getType(), env, store, call);
  env.push(store.getRuntimeScalar());

  return changed;
}

static AnalysisStatus model_fscanf(const ModelCustom& model,
                                   const Instruction* call,
                                   const Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markWrite(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= model.markRead(
      args.at(1), LLVMUtils::getArgumentType(f, 1), env, store, call);
  for(unsigned i = 2; i < args.size(); i++)
    changed |= model.markWrite(
        args.at(i), call->getOperand(i)->getType(), env, store, call);
  env.push(store.getRuntimeScalar());

  return changed;
}

static AnalysisStatus model_asctime(const ModelCustom& model,
                                    const Instruction* call,
                                    const Function* f,
                                    const Vector<Contents>& args,
                                    Environment& env,
                                    Store& store,
                                    const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);

  if(env.contains(call))
    env.push(env.lookup(call));
  else
    env.push(store.allocate(f->getReturnType(), CellFlags::LibAlloc, true));

  return changed;
}

static AnalysisStatus model_asctime_r(const ModelCustom& model,
                                      const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store,
                                      const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= model.markWrite(
      args.at(1), LLVMUtils::getArgumentType(f, 1), env, store, call);
  env.push(args.at(1));

  return changed;
}

static AnalysisStatus model_ctime(const ModelCustom& model,
                                  const Instruction* call,
                                  const Function* f,
                                  const Vector<Contents>& args,
                                  Environment& env,
                                  Store& store,
                                  const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);

  if(env.contains(call))
    env.push(env.lookup(call));
  else
    env.push(store.allocate(f->getReturnType(), CellFlags::LibAlloc, true));

  return changed;
}

static AnalysisStatus model_ctime_r(const ModelCustom& model,
                                    const Instruction* call,
                                    const Function* f,
                                    const Vector<Contents>& args,
                                    Environment& env,
                                    Store& store,
                                    const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= model.markRead(
      args.at(1), LLVMUtils::getArgumentType(f, 1), env, store, call);
  env.push(args.at(1));

  return changed;
}

static AnalysisStatus model_gmtime(const ModelCustom& model,
                                   const Instruction* call,
                                   const Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);

  if(env.contains(call))
    env.push(env.lookup(call));
  else
    env.push(store.allocate(
        dyn_cast<PointerType>(f->getReturnType())->getElementType(),
        CellFlags::LibAlloc,
        true));

  return changed;
}

static AnalysisStatus model_gmtime_r(const ModelCustom& model,
                                     const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store,
                                     const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= model.markWrite(
      args.at(1), LLVMUtils::getArgumentType(f, 1), env, store, call);

  env.push(args.at(1));

  return changed;
}

static AnalysisStatus model_localtime(const ModelCustom& model,
                                      const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store,
                                      const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);

  if(env.contains(call))
    env.push(env.lookup(call));
  else
    env.push(store.allocate(
        dyn_cast<PointerType>(f->getReturnType())->getElementType(),
        CellFlags::LibAlloc,
        true));

  return changed;
}

static AnalysisStatus model_localtime_r(const ModelCustom& model,
                                        const Instruction* call,
                                        const Function* f,
                                        const Vector<Contents>& args,
                                        Environment& env,
                                        Store& store,
                                        const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= model.markWrite(
      args.at(1), LLVMUtils::getArgumentType(f, 1), env, store, call);

  env.push(args.at(1));

  return changed;
}

static AnalysisStatus model___errno_location(const ModelCustom& model,
                                             const Instruction* call,
                                             const Function* f,
                                             const Vector<Contents>& args,
                                             Environment& env,
                                             Store& store,
                                             const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(env.contains(call))
    env.push(env.lookup(call));
  else {
    env.push(store.allocate(f->getReturnType(), CellFlags::LibAlloc, true));
    changed = AnalysisStatus::Changed;
  }

  return changed;
}

static AnalysisStatus model___ctype_b_loc(const ModelCustom& model,
                                          const Instruction* call,
                                          const Function* f,
                                          const Vector<Contents>& args,
                                          Environment& env,
                                          Store& store,
                                          const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // The documentation says that it returns a pointer into an array of
  // characters. It is used for ctype functions which don't really matter to
  // out analysis anyway, so we'll just push a null pointer
  env.push(store.getNullptr());

  return changed;
}

static AnalysisStatus model_initstate(const ModelCustom& model,
                                      const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store,
                                      const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= model.markWrite(
      args.at(1), LLVMUtils::getArgumentType(f, 1), env, store, call);
  changed |= model.markRead(
      args.at(2), LLVMUtils::getArgumentType(f, 2), env, store, call);

  env.push(args.at(1));

  return changed;
}

static AnalysisStatus model_setstate(const ModelCustom& model,
                                     const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store,
                                     const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markWrite(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);

  env.push(args.at(0));

  return changed;
}

static AnalysisStatus model_gets(const ModelCustom& model,
                                 const Instruction* call,
                                 const Function* f,
                                 const Vector<Contents>& args,
                                 Environment& env,
                                 Store& store,
                                 const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markWrite(
      args.at(0), call->getOperand(0)->getType(), env, store, call);

  env.push(args.at(0));

  return changed;
}

static AnalysisStatus model_getline(const ModelCustom& model,
                                    const Instruction*call,
                                    const Function* f,
                                    const Vector<Contents>& args,
                                    Environment& env,
                                    Store& store,
                                    const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markWrite(
      args.at(1), call->getOperand(1)->getType(), env, store, call);

  if(env.contains(call)) {
    env.push(env.lookup(call));
  } else {
    Type* i8 = Type::getInt8Ty(f->getContext());
    const Store::Address* buf = store.allocate(i8, CellFlags::LibAlloc, true);
    store.write(buf, store.getRuntimeScalar(), i8, call, changed);
    for(const auto* addr : args.at(0).getAs<Address>(call))
      store.write(addr, buf, i8->getPointerTo(), call, changed);
  }
  env.push(store.getRuntimeScalar());

  return changed;
}

static AnalysisStatus model_memset(const ModelCustom& model,
                                   const Instruction* call,
                                   const Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= AbstractUtils::memset(call, args, env, store, caller);
  env.push(args.at(0));

  return changed;
}

static AnalysisStatus model_memcpy(const ModelCustom& model,
                                   const Instruction* call,
                                   const Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const Function* caller) {
  return AbstractUtils::memcpy(call, args, env, store, caller);
}

static AnalysisStatus model_getenv(const ModelCustom& model,
                                   const Instruction* call,
                                   const Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const Function* caller) {
  return model___ret_allocated_str(model, call, f, args, env, store, caller);
}

static AnalysisStatus model_strerror(const ModelCustom& model,
                                     const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store,
                                     const Function* caller) {
  return model___ret_allocated_str(model, call, f, args, env, store, caller);
}

static AnalysisStatus model_strtol(const ModelCustom& model,
                                   const Instruction* call,
                                   const Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const auto* addr : args.at(1).getAs<Address>(call))
    store.write(
        addr, args.at(0), LLVMUtils::getArgumentType(f, 0), call, changed);

  env.push(store.getRuntimeScalar());

  return changed;
}

static AnalysisStatus model_qsort(const ModelCustom& model,
                                  const Instruction* call,
                                  const Function* f,
                                  const Vector<Contents>& args,
                                  Environment& env,
                                  Store& store,
                                  const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markWrite(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= model.markRead(
      args.at(1), LLVMUtils::getArgumentType(f, 1), env, store, call);
  changed |= model.markRead(
      args.at(2), LLVMUtils::getArgumentType(f, 2), env, store, call);

  for(const Content* content : args.at(3)) {
    if(const auto* addr = dyn_cast<Address>(content)) {
      for(const Content* c :
          store.read(addr, LLVMUtils::getArgumentType(f, 3), call, changed)) {
        if(const auto* code = dyn_cast<ContentCode>(c)) {
          const Function* sortfn = code->getFunction();
          changed |= env.add(LLVMUtils::getArgument(sortfn, 0), args.at(0));
          changed |= env.add(LLVMUtils::getArgument(sortfn, 1), args.at(0));
        } else {
          moya_error("Sort function should be a function: " << *call);
        }
      }
    } else {
      moya_error("Not passing address to function pointer arg in sort");
    }
  }
  return changed;
}

static AnalysisStatus model_getaddrinfo(const ModelCustom& model,
                                        const Instruction* call,
                                        const Function* f,
                                        const Vector<Contents>& args,
                                        Environment& env,
                                        Store& store,
                                        const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(call)) {
    Type* ty = dyn_cast<PointerType>(LLVMUtils::getArgumentType(f, 2))
                   ->getElementType();
    Type* pty = ty->getPointerTo();

    const Address* allocated = AbstractUtils::allocate(
        ty, store, call, caller, CellFlags::Initialized);
    for(const Address* addr : args.at(3).getAs<Address>(call))
      store.write(addr, allocated, pty, call, changed);
  }

  env.push(store.getRuntimeScalar());

  return changed;
}

void Models::initLibC(const Module& module) {
  // File IO
  add(new ModelReadWrite("fread", {0, 1, 1, 0}, true));
  add(new ModelReadWrite("fwrite", {1, 1, 1, 0}, true));
  add(new ModelReadWrite("freopen", {1, 1, 0}, Model::ReturnSpec::Arg2));
  add(new ModelCustom("fopen", model_fopen));
  add(new ModelCustom("fopen64", model_fopen));
  add(new ModelReadWrite("fclose", {0}, true));
  add(new ModelReadOnly("ferror"));
  add(new ModelReadWrite("fgetc", {0}, true));
  add(new ModelReadWrite("fputc", {1, 0}, true));
  add(new ModelReadWrite("fgets", {0, 1, 0}, true));
  add(new ModelReadWrite("fputs", {1, 0}, true));
  add(new ModelCustom("fscanf", model_fscanf, true));
  add(new ModelReadWrite("fprintf", {0, 1}, true));
  add(new ModelReadWrite("vfprintf", {0, 1, 0}, true));
  add(new ModelCustom("__isoc99_fscanf", model_fscanf, true));
  add(new ModelReadWrite("__isoc99_fprintf", {0, 1}, true));
  add(new ModelCustom("\1__isoc99_fscanf", model_fscanf, true));
  add(new ModelReadWrite("\1__isoc99_fprintf", {0, 1}, true));
  add(new ModelReadWrite("fflush", {0}, true));
  add(new ModelReadWrite("feof", {0}, true));
  add(new ModelReadOnly("ftell"));
  add(new ModelReadWrite("fseek", {0, 1, 1}, true));
  add(new ModelReadWrite("fgetpos", {0, 0}, true));
  add(new ModelReadWrite("fsetpos", {0, 1}, true));
  add(new ModelReadWrite("rewind", {0}, true));
  add(new ModelCustom("fdopen", model_fopen, true));
  add(new ModelReadOnly("fileno"));
  add(new ModelReadOnly("fileno_unlocked"));
  add(new ModelReadOnly("open64"));
  add(new ModelReadOnly("creat64"));
  add(new ModelReadWrite("fgetpos64", {0, 0}, true));
  add(new ModelReadWrite("fsetpos64", {0, 1}, true));
  add(new ModelReadOnly("lseek"));
  add(new ModelReadOnly("lseek64"));
  add(new ModelReadWrite("setbuf", {0, 0}, true));
  add(new ModelReadWrite("setbuffer", {0, 0, 1}, true));
  add(new ModelReadWrite("setlinebuf", {0}, true));
  add(new ModelReadWrite("setvbuf", {0, 0, 1, 1}, true));
  add(new ModelCustom("getline", model_getline));

  // Directories
  add(new ModelReadOnly("opendir", Model::ReturnSpec::AllocateAndInitialize));
  add(new ModelReadOnly("fopendir", Model::ReturnSpec::AllocateAndInitialize));
  add(new ModelReadOnly("dirfd"));
  add(new ModelReadWrite("closedir", {0}));
  add(new ModelReadWrite("readdir", {0}, Model::ReturnSpec::Allocate));
  add(new ModelReadWrite("readdir_r", {0, 1, 0}, Model::ReturnSpec::Allocate));
  add(new ModelReadWrite("readdir64", {0}, Model::ReturnSpec::Allocate));
  add(new ModelReadWrite(
      "readdir64_r", {0, 1, 0}, Model::ReturnSpec::Allocate));
  add(new ModelCustom("tmpfile", model_fopen));
  add(new ModelCustom("tmpfile64", model_fopen));
  add(new ModelReadOnly("tmpnam", Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("tmpnam_r", Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("tempnam"));
  add(new ModelReadWrite("mktemp", {0}));
  add(new ModelReadWrite("mkstemp", {0}));
  add(new ModelReadWrite("mkdtemp", {0}));

  // Memory management
  add(new ModelCustom("malloc", model_malloc));
  add(new ModelCustom("realloc", model_realloc));
  add(new ModelCustom("calloc", model_calloc));
  add(new ModelCustom("free", model_free));
  add(new ModelCustom("memalign", model_memalign));
  add(new ModelCustom("aligned_alloc", model_aligned_alloc));
  add(new ModelCustom("posix_memalign", model_posix_memalign));

  // IO routines
  add(new ModelReadOnly("printf"));
  add(new ModelReadOnly("vprintf"));
  add(new ModelCustom("scanf", model_scanf));
  add(new ModelCustom("gets", model_gets));
  add(new ModelReadWrite("puts", {0}, true));
  add(new ModelReadWrite("putchar", {0}, true));
  add(new ModelReadWrite("putc", {0}, true));
  add(new ModelReadWrite("getchar", {0}, true));
  add(new ModelReadWrite("getc", {0}, true));
  add(new ModelReadWrite("getw", {0}, true));
  add(new ModelReadWrite("ungetc", {1, 0}, true));
  add(new ModelReadWrite("ungetwc", {1, 0}, true));

  // Process routines
  add(new ModelReadOnly("abort"));
  add(new ModelReadOnly("exit"));
  add(new ModelReadOnly("perror"));
  add(new ModelCustom("strerror", model_strerror));
  add(new ModelReadWrite("strerror_r", {1, 0, 1}, Model::ReturnSpec::Arg1));
  add(new ModelReadOnly("error"));
  add(new ModelReadOnly("error_at_line"));
  add(new ModelReadOnly("warn"));
  add(new ModelReadOnly("vwarn"));
  add(new ModelReadOnly("warnx"));
  add(new ModelReadOnly("vwarnx"));
  add(new ModelReadOnly("err"));
  add(new ModelReadOnly("verr"));
  add(new ModelReadOnly("errx"));
  add(new ModelReadOnly("verrx"));

  // String routines
  add(new ModelCustom("asprintf", model_asprintf));
  add(new ModelCustom("vasprintf", model_asprintf));
  add(new ModelReadWrite("vsprintf", {0, 1, 0}));
  add(new ModelReadWrite("vsnprintf", {0, 1, 1, 0}));
  add(new ModelReadWrite("sprintf", {0, 1}));
  add(new ModelReadWrite("snprintf", {0, 1, 1}));
  add(new ModelReadWrite("__sprintf_chk", {0, 1, 1}));
  add(new ModelCustom("sscanf", model_sscanf));
  add(new ModelCustom("__isoc99_sscanf", model_sscanf));
  add(new ModelReadWrite("stpcpy", {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite("stpncpy", {0, 1, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite("strcat", {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite("strncat", {0, 1, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite("strlcat", {0, 1, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite("strcpy", {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite("strncpy", {0, 1, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite("strset", {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite("strnset", {0, 1, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("strstr", Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("strtok", Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("strcmp"));
  add(new ModelReadOnly("strncmp"));
  add(new ModelReadOnly("stricmp"));
  add(new ModelReadOnly("strcasecmp"));
  add(new ModelReadOnly("strncasecmp"));
  add(new ModelReadOnly("strverscmp"));
  add(new ModelReadOnly("strlen"));
  add(new ModelReadOnly("strcoll"));
  add(new ModelReadOnly("strchr", Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("strrchr", Model::ReturnSpec::Arg0));
  add(new ModelCustom("strdup", model_strdup));
  add(new ModelReadOnly("strspn"));
  add(new ModelReadWrite("strlwr", {0}));
  add(new ModelReadWrite("strupr", {0}));
  add(new ModelReadWrite("strxfrm", {0, 1}));

  // Character type functions
  add(new ModelReadOnly("isalnum"));
  add(new ModelReadOnly("isalpha"));
  add(new ModelReadOnly("isascii"));
  add(new ModelReadOnly("isatty"));
  add(new ModelReadOnly("isblank"));
  add(new ModelReadOnly("iscntrl"));
  add(new ModelReadOnly("isdigit"));
  add(new ModelReadOnly("isgraph"));
  add(new ModelReadOnly("islower"));
  add(new ModelReadOnly("isprint"));
  add(new ModelReadOnly("ispunct"));
  add(new ModelReadOnly("isspace"));
  add(new ModelReadOnly("isupper"));
  add(new ModelReadOnly("isxdigit"));

  // Memory buffer routines
  add(new ModelReadOnly("memcmp"));
  add(new ModelCustom("memcpy", model_memcpy));
  add(new ModelCustom("mempcpy", model_memcpy));
  add(new ModelCustom("memccpy", model_memcpy));
  add(new ModelCustom("memset", model_memset));
  add(new ModelCustom("memmove", model_memcpy));

  // Time functions
  add(new ModelCustom("asctime", model_asctime));
  add(new ModelCustom("asctime_r", model_asctime_r));
  add(new ModelCustom("ctime", model_ctime));
  add(new ModelCustom("ctime_r", model_ctime_r));
  add(new ModelCustom("gmtime", model_gmtime));
  add(new ModelCustom("gmtime_r", model_gmtime_r));
  add(new ModelCustom("localtime", model_localtime));
  add(new ModelCustom("localtime_r", model_localtime_r));
  add(new ModelReadWrite("mktime", {0}));
  add(new ModelReadWrite("strftime", {0, 1, 1, 1}));
  add(new ModelReadOnly("difftime"));
  add(new ModelCustom("__errno_location", model___errno_location));
  add(new ModelCustom("__ctype_b_loc", model___ctype_b_loc));

  // Random numbers
  add(new ModelReadOnly("rand"));
  add(new ModelReadOnly("srand"));
  add(new ModelReadOnly("rand_r"));
  add(new ModelReadOnly("random"));
  add(new ModelReadOnly("srandom"));
  add(new ModelCustom("initstate", model_initstate));
  add(new ModelCustom("setstate", model_setstate));

  // getopt
  add(new ModelReadOnly("getopt"));
  add(new ModelReadWrite("getopt_long", {1, 1, 1, 1, 0}));
  add(new ModelReadWrite("getopt_long_only", {1, 1, 1, 1, 0}));

  // Environment access
  add(new ModelCustom("getenv", model_getenv));
  add(new ModelReadOnly("secure_getenv"));
  add(new ModelReadWrite("putenv", {0}));
  add(new ModelReadOnly("setenv"));
  add(new ModelReadOnly("unsetenv"));
  add(new ModelReadOnly("clearenv"));

  // Pipes and FIFO's
  add(new ModelCustom("popen", model_fopen, true));
  add(new ModelReadWrite("pclose", {0}, true));

  // assert
  add(new ModelReadOnly("assert"));
  add(new ModelReadOnly("__assert_fail"));
  add(new ModelReadOnly("__assert_fail_base"));

  // number parsing routines
  add(new ModelReadOnly("atoi"));
  add(new ModelReadOnly("atol"));
  add(new ModelReadOnly("atoll"));
  add(new ModelCustom("strtol", model_strtol));
  add(new ModelCustom("strtoul", model_strtol));
  add(new ModelCustom("strtoll", model_strtol));
  add(new ModelCustom("strtoull", model_strtol));
  add(new ModelReadOnly("atof"));
  add(new ModelReadWrite("strtof", {1, 0}));
  add(new ModelReadWrite("strtod", {1, 0}));
  add(new ModelReadWrite("strtold", {1, 0}));

  // sort functions
  add(new ModelCustom("qsort", model_qsort));

  // time functions
  add(new ModelReadOnly("clock"));

  // case conversion functions
  add(new ModelReadOnly("toupper"));
  add(new ModelReadOnly("tolower"));
  add(new ModelReadOnly("toascii"));
  add(new ModelReadOnly("_toupper"));
  add(new ModelReadOnly("_tolower"));

  // Obsolete rand48 functions
  add(new ModelReadOnly("drand48"));
  add(new ModelReadOnly("erand48"));
  add(new ModelReadOnly("lrand48"));
  add(new ModelReadOnly("nrand48"));
  add(new ModelReadOnly("mrand48"));
  add(new ModelReadOnly("jrand48"));
  add(new ModelReadOnly("srand48"));
  add(new ModelReadWrite("seed48", {0}, Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("lcong48"));

  // Others
  add(new ModelReadWrite("klogctl", {1, 0, 1}));
  add(new ModelReadOnly("unlink"));
  add(new ModelReadOnly("atexit"));

  // Network
  add(new ModelCustom("getaddrinfo", model_getaddrinfo));
  add(new ModelReadWrite("freeaddrinfo", {-1}));

  // Process
  add(new ModelReadOnly("system"));
  add(new ModelReadOnly("execv"));
  add(new ModelReadOnly("execl"));
  add(new ModelReadOnly("execve"));
  add(new ModelReadOnly("execle"));
  add(new ModelReadOnly("execvp"));
  add(new ModelReadOnly("execlp"));
  add(new ModelReadOnly("fork"));
  add(new ModelReadOnly("vfork"));

  // Filesystem routines
  add(new ModelReadOnly("remove"));
}
