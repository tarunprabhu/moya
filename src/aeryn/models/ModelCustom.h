#ifndef MOYA_MODELS_MODEL_CUSTOM_H
#define MOYA_MODELS_MODEL_CUSTOM_H

#include "Model.h"

#include <functional>

namespace moya {

class ModelCustom : public Model {
public:
  using ProcessFn = std::function<AnalysisStatus(const ModelCustom&,
                                                 const llvm::Instruction*,
                                                 const llvm::Function*,
                                                 const Vector<Contents>&,
                                                 Environment&,
                                                 Store&,
                                                 const llvm::Function*)>;

protected:
  bool markArgsRead;
  const ProcessFn processFn;

public:
  ModelCustom(const std::string&, const ProcessFn&, bool = false, bool = true);

  virtual ~ModelCustom() = default;

  const ProcessFn& getProcessFn() const;

  virtual AnalysisStatus process(const llvm::Instruction*,
                                 const llvm::Function*,
                                 const Vector<Contents>&,
                                 const Vector<llvm::Type*>&,
                                 Environment&,
                                 Store&,
                                 const llvm::Function*) const;

public:
  static bool classof(const Model*);
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_CUSTOM_H
