#ifndef MOYA_MODELS_MODEL_STL_UNORDERED_MULTISET_H
#define MOYA_MODELS_MODEL_STL_UNORDERED_MULTISET_H

#include "ModelSTLUnorderedSet.h"

namespace moya {

class ModelSTLUnorderedMultiset : public ModelSTLUnorderedSet {
public:
  ModelSTLUnorderedMultiset(const std::string&, FuncID, const Models&);

  virtual ~ModelSTLUnorderedMultiset() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_UNORDERED_MULTISET_H
