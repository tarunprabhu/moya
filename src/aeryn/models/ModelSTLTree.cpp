#include "ModelSTLTree.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

using namespace llvm;

namespace moya {

// This is the layout for the red-black tree used in libstdc++ to represent
// STL sets, maps, multisets and multimaps
//
// tree = { i8, base, i64 }
//
// base = { i32, base*, base*, base* }
//
// All the base* pointers point to a node which contains the data stored in
// the container
//
// node = { i32, base*, base*, base*, T }
//
// In the case of maps and multimaps, T is actually a pair <K, V>.
// All the base* pointers are accessed and modified in tandem - when one is
// read, all are read, when one is modified, all are modified.
//

ModelSTLTree::ModelSTLTree(const std::string& fname,
                           STDKind cont,
                           FuncID fn,
                           const Models& ms)
    : ModelSTLContainer(fname,
                        cont,
                        STDKind::STD_STL_iterator_tree,
                        STDKind::STD_STL_const_iterator_tree,
                        fn,
                        ms) {
  switch(fn) {
  case Begin:
    processFn = static_cast<ExecuteFn>(&ModelSTLTree::modelBegin);
    break;
  case End:
    processFn = static_cast<ExecuteFn>(&ModelSTLTree::modelEnd);
    break;
  case InsertElement:
    processFn = static_cast<ExecuteFn>(&ModelSTLTree::modelInsertElement);
    break;
  case InsertElementAt:
    processFn = static_cast<ExecuteFn>(&ModelSTLTree::modelInsertElementAt);
    break;
  case InsertRange:
    processFn = static_cast<ExecuteFn>(&ModelSTLTree::modelInsertRange);
    break;
  case InsertInitList:
    processFn = static_cast<ExecuteFn>(&ModelSTLTree::modelInsertInitList);
    break;
  case Find:
    processFn = static_cast<ExecuteFn>(&ModelSTLTree::modelFind);
    break;
  case FindRange:
    processFn = static_cast<ExecuteFn>(&ModelSTLTree::modelFindRange);
    break;
  default:
    // Everything else will have been initialized in the ModelSTLContainer()
    // constructor
    break;
  }
}

Contents ModelSTLTree::getBeginIterator(const Addresses& conts,
                                        const Instruction* call,
                                        const Function* f,
                                        Store& store,
                                        AnalysisStatus& changed) const {
  Contents ret;
  for(const Address* node : getNode(conts, call, f, store, changed))
    ret.insert(node);
  return ret;
}

Addresses ModelSTLTree::getIteratorNode(const Addresses& iters,
                                        const Instruction* call,
                                        const Function* f,
                                        Store& store,
                                        AnalysisStatus& changed) const {
  PointerType* pBaseTy = Metadata::getSTLIteratorType(f)->getPointerTo();

  return store.readAs<Address>(iters, pBaseTy, call, changed);
}

Addresses ModelSTLTree::getIteratorDeref(const Addresses& iters,
                                         const Instruction* call,
                                         const Function* f,
                                         Store& store,
                                         AnalysisStatus& changed) const {
  Addresses ret;
  PointerType* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();
  unsigned long offset = Metadata::getSTLValueOffset(f);

  for(const auto* ptr : store.readAs<Address>(iters, pBaseTy, call, changed))
    ret.insert(store.make(ptr, offset));
  return ret;
}

Addresses ModelSTLTree::getIteratorNew(const Addresses& iters,
                                       const Instruction* call,
                                       const Function* f,
                                       Store& store,
                                       AnalysisStatus& changed) const {
  PointerType* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();

  return store.readAs<Address>(iters, pBaseTy, call, changed);
}

AnalysisStatus ModelSTLTree::updateContainerPointers(const Address* base,
                                                     const Addresses& vals,
                                                     const Instruction* call,
                                                     const Function* f,
                                                     Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();
  int64_t vtadj = getContainerVtableOffset(base, call, f, store);
  for(unsigned offset : {16, 24, 32})
    for(const Content* c : vals)
      store.write(store.make(base, vtadj + offset), c, pBaseTy, call, changed);

  return changed;
}

AnalysisStatus ModelSTLTree::updateNodePointers(const Address* base,
                                                const Addresses& vals,
                                                const Instruction* call,
                                                const Function* f,
                                                Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();
  for(unsigned offset : {8, 16, 24})
    for(const Content* c : vals)
      store.write(store.make(base, offset), c, pBaseTy, call, changed);

  return changed;
}

AnalysisStatus ModelSTLTree::modelBegin(const Instruction* call,
                                        const Function* f,
                                        const Vector<Contents>& args,
                                        Environment& env,
                                        Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  PointerType* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents ret = getBeginIterator(conts, call, f, store, changed);

  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, ret, pBaseTy, call, changed);
  else
    env.push(ret);

  return changed;
}

AnalysisStatus ModelSTLTree::modelEnd(const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  PointerType* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents ret = getEndIterator(conts, call, f, store, changed);

  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, ret, pBaseTy, call, changed);
  else
    env.push(ret);

  return changed;
}

AnalysisStatus ModelSTLTree::modelInsertElement(const Instruction* call,
                                                const Function* f,
                                                const Vector<Contents>& args,
                                                Environment& env,
                                                Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  Contents iters = getBeginIterator(conts, call, f, store, changed);
  Contents flags(store.getRuntimeScalar());

  changed |= writeToContainer(conts, args, 1, call, f, store);

  env.push(store.make({iters, flags}));

  return changed;
}

AnalysisStatus ModelSTLTree::modelInsertElementAt(const Instruction* call,
                                                  const Function* f,
                                                  const Vector<Contents>& args,
                                                  Environment& env,
                                                  Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  Contents iters = getBeginIterator(conts, call, f, store, changed);
  Contents flags(store.getRuntimeScalar());

  changed |= writeToContainer(conts, args, 2, call, f, store);

  env.push(store.make({iters, flags}));

  return changed;
}

AnalysisStatus ModelSTLTree::modelInsertRange(const Instruction* call,
                                              const Function* f,
                                              const Vector<Contents>& args,
                                              Environment& env,
                                              Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  unsigned long offset = Metadata::getSTLValueOffset(f);
  const Addresses& conts = args.at(0).getAs<Address>(call);
  const Addresses& addrs1 = args.at(1).getAs<Address>(call);
  const Addresses& addrs2 = args.at(2).getAs<Address>(call);

  changed |= markContainerWrite(conts, call, f, store);
  changed |= writeToContainer(conts, addrs1, offset, call, f, store);
  changed |= writeToContainer(conts, addrs2, offset, call, f, store);

  return changed;
}

AnalysisStatus ModelSTLTree::modelInsertInitList(const Instruction* call,
                                                 const Function* f,
                                                 const Vector<Contents>& args,
                                                 Environment& env,
                                                 Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  const Addresses& addrs = args.at(1).getAs<Address>(call);

  changed |= writeToContainer(conts, addrs, 0, call, f, store);

  return changed;
}

AnalysisStatus ModelSTLTree::modelFind(const Instruction* call,
                                       const Function* f,
                                       const Vector<Contents>& args,
                                       Environment& env,
                                       Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  Contents begs = getBeginIterator(conts, call, f, store, changed);

  env.push(begs);

  return changed;
}

AnalysisStatus ModelSTLTree::modelFindRange(const Instruction* call,
                                            const Function* f,
                                            const Vector<Contents>& args,
                                            Environment& env,
                                            Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  Contents begs = getBeginIterator(conts, call, f, store, changed);
  Contents ends = getEndIterator(conts, call, f, store, changed);

  env.push(store.make({begs, ends}));

  return changed;
}

} // namespace moya
