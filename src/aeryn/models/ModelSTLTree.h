#ifndef MOYA_MODELS_MODEL_STL_TREE_H
#define MOYA_MODELS_MODEL_STL_TREE_H

#include "ModelSTLContainer.h"

namespace moya {

class ModelSTLTree : public ModelSTLContainer {
protected:
  virtual AnalysisStatus updateContainerPointers(const Address*,
                                                 const Addresses&,
                                                 const llvm::Instruction*,
                                                 const llvm::Function*,
                                                 Store&) const;

  virtual AnalysisStatus updateNodePointers(const Address*,
                                            const Addresses&,
                                            const llvm::Instruction*,
                                            const llvm::Function*,
                                            Store&) const;

  virtual Contents getBeginIterator(const Addresses&,
                                    const llvm::Instruction*,
                                    const llvm::Function*,
                                    Store&,
                                    AnalysisStatus&) const;
  virtual IteratorFn getIteratorNode;
  virtual IteratorFn getIteratorDeref;
  virtual IteratorFn getIteratorNew;

  virtual ModelFn modelBegin;
  virtual ModelFn modelEnd;
  virtual ModelFn modelInsertElement;
  virtual ModelFn modelInsertElementAt;
  virtual ModelFn modelInsertInitList;
  virtual ModelFn modelInsertRange;
  virtual ModelFn modelFind;
  virtual ModelFn modelFindRange;

public:
  ModelSTLTree(const std::string&, STDKind, FuncID, const Models&);

  virtual ~ModelSTLTree() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_TREE_H
