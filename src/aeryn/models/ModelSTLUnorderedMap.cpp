#include "ModelSTLUnorderedMap.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

using namespace llvm;

namespace moya {

ModelSTLUnorderedMap::ModelSTLUnorderedMap(const std::string& fname,
                                           STDKind kind,
                                           FuncID fn,
                                           const Models& ms)
    : ModelSTLHashtable(fname, kind, fn, ms) {
  switch(fn) {
  case AccessElement:
    processFn
        = static_cast<ExecuteFn>(&ModelSTLUnorderedMap::modelAccessElement);
    break;
  case EmplaceMapped:
    processFn
        = static_cast<ExecuteFn>(&ModelSTLUnorderedMap::modelEmplaceMapped);
    break;
  default:
    // Everything else will have been initialized in the ModelSTLContainer()
    // constructor
    break;
  }
}

ModelSTLUnorderedMap::ModelSTLUnorderedMap(const std::string& fname,
                                           FuncID fn,
                                           const Models& ms)
    : ModelSTLUnorderedMap(fname, STDKind::STD_STL_unordered_map, fn, ms) {
  ;
}

AnalysisStatus
ModelSTLUnorderedMap::modelAccessElement(const Instruction* call,
                                         const Function* f,
                                         const Vector<Contents>& args,
                                         Environment& env,
                                         Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Contents ret;
  unsigned long offsetVal = Metadata::getSTLMappedOffset(f);
  const Addresses& conts = args.at(0).getAs<Address>(call);
  const Contents& keys = args.at(1);

  changed |= writeToContainer(conts, keys, call, f, store);
  for(const Address* node : getNode(conts, call, f, store, changed))
    ret.insert(store.make(node, offsetVal));

  env.push(ret);

  return changed;
}

AnalysisStatus
ModelSTLUnorderedMap::modelEmplaceMapped(const Instruction* call,
                                         const Function* f,
                                         const Vector<Contents>& args,
                                         Environment& env,
                                         Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Contents ret;
  Type* keyTy = Metadata::getSTLKeyType(f);
  uint64_t offsetKey = Metadata::getSTLKeyOffset(f);
  uint64_t offsetVal = Metadata::getSTLMappedOffset(f);
  const Addresses& conts = args.at(0).getAs<Address>(call);
  const Addresses& keys = args.at(1).getAs<Address>(call);

  // Write the keys to the map
  for(const Address* node : getNode(conts, call, f, store, changed)) {
    for(const Address* key : keys)
      store.copy(store.make(node, offsetKey), key, keyTy, call, changed);
    ret.insert(store.make(node, offsetVal));
  }
  // for(const Address* buf : getBuffer(conts, call, f, store, changed))
  //   if(store.empty(buf, mappedTy))
  // moya_warning(
  //     "Need to implement adding a default element: ModelSTLUnorderedMap");
  changed |= markContainerWrite(conts, call, f, store);
  changed |= markContainerRead(conts, call, f, store);

  env.push(ret);

  return changed;
}

} // namespace moya
