#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"

using namespace llvm;
using namespace moya;

static void mkReadOnly(Models& ms, const std::string& model) {
  ms.add(new ModelReadOnly(model + "_"));
  ms.add(new ModelReadOnly("LAPACK_" + model));
}

static void mkReadWrite(Models& ms,
                        const std::string& model,
                        const std::initializer_list<int>& isConst) {
  ms.add(new ModelReadWrite(model + "_", isConst));
  ms.add(new ModelReadWrite("LAPACK_" + model, isConst));
}

void Models::initLibLapack(const Module& module) {
  Models& ms = *this;

  add(new ModelReadOnly("Csys2blacs_handle"));
  add(new ModelReadWrite("Cblacs_gridinit", {1, 0, 1, 1}));
  add(new ModelReadWrite("Cblacs_gridinfo", {1, 0, 0, 0, 0}));
  add(new ModelReadOnly("Cblacs_gridexit"));

  mkReadWrite(ms, "dcopy", {1, 1, 1, 0, 1});
  mkReadWrite(ms, "dswap", {1, 0, 1, 0, 1});
  mkReadWrite(ms, "dgemm", {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1});
  mkReadWrite(ms, "dger", {1, 1, 1, 1, 1, 1, 1, 0, 1});
  mkReadWrite(ms, "daxpy", {1, 1, 1, 1, 0, 1});
  mkReadOnly(ms, "numroc");
  mkReadWrite(ms, "descinit", {0, 1, 1, 1, 1, 1, 1, 1, 1, 0});
  mkReadWrite(ms, "pdposv", {1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0});
  mkReadWrite(ms, "dgemv", {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1});
  mkReadOnly(ms, "idamax");
  mkReadWrite(ms, "dscal", {1, 1, 0, 1});

  /* Everything below here is auto-generated */
  mkReadOnly(ms, "disnan");
  mkReadOnly(ms, "dlaisnan");
  mkReadWrite(ms, "dpotf2", {1, 1, 0, 1, 0, 1});
  mkReadWrite(ms, "dpotrf", {1, 1, 0, 1, 0, 1});
  mkReadOnly(ms, "ieeeck");
  mkReadOnly(ms, "ilaenv");
  mkReadOnly(ms, "iparmq");
  mkReadOnly(ms, "lsame");
  mkReadWrite(ms, "dlacpy", {1, 1, 1, 1, 1, 0, 1, 1});
  mkReadWrite(ms, "dlae2", {1, 1, 1, 0, 0});
  mkReadWrite(ms, "dlaed0", {1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0});
  mkReadWrite(ms, "dlaed1", {1, 0, 0, 1, 0, 1, 1, 0, 0, 0});
  mkReadWrite(
      ms, "dlaed2", {0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0});
  mkReadWrite(ms, "dlaed3", {1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0});
  mkReadWrite(ms, "dlaed4", {1, 1, 1, 1, 0, 1, 0, 0});
  mkReadWrite(ms, "dlaed5", {1, 1, 1, 0, 1, 0});
  mkReadWrite(ms, "dlaed6", {1, 1, 1, 1, 1, 1, 0, 0});
  mkReadWrite(ms, "dlaed7", {1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1,
                             1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0});
  mkReadWrite(ms, "dlaed8", {1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1,
                             0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0});
  mkReadWrite(ms, "dlaed9", {1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0});
  mkReadWrite(ms, "dlaeda", {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0});
  mkReadWrite(ms, "dlaev2", {1, 1, 1, 0, 0, 0, 0});
  mkReadOnly(ms, "dlamch");
  mkReadWrite(ms, "dlamrg", {1, 1, 1, 1, 1, 0});
  mkReadWrite(ms, "dlansp", {1, 1, 1, 1, 0, 1, 1});
  mkReadOnly(ms, "dlanst");
  mkReadOnly(ms, "dlapy2");
  mkReadWrite(ms, "dlarf", {1, 1, 1, 1, 1, 1, 0, 1, 0, 1});
  mkReadWrite(ms, "dlarfg", {1, 0, 0, 1, 0});
  mkReadWrite(ms, "dlartg", {1, 1, 0, 0, 0});
  mkReadWrite(ms, "dlascl", {1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1});
  mkReadWrite(ms, "dlaset", {1, 1, 1, 1, 1, 0, 1, 1});
  mkReadWrite(ms, "dlasr", {1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1});
  mkReadWrite(ms, "dlasrt", {1, 1, 0, 0, 1});
  mkReadWrite(ms, "dlassq", {1, 1, 1, 0, 0});
  mkReadWrite(ms, "dopmtr", {1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1});
  mkReadWrite(ms, "dspevd", {1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1});
  mkReadWrite(ms, "dsptrd", {1, 1, 0, 0, 0, 0, 0, 1});
  mkReadWrite(ms, "dstedc", {1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1});
  mkReadWrite(ms, "dsteqr", {1, 1, 0, 0, 0, 1, 0, 0, 1});
  mkReadWrite(ms, "dsterf", {1, 0, 0, 0});
  mkReadOnly(ms, "iladlc");
  mkReadOnly(ms, "iladlr");
  mkReadWrite(ms, "dtrti2", {1, 1, 1, 0, 1, 0, 1, 1});
  mkReadWrite(ms, "dtrtri", {1, 1, 1, 0, 1, 0, 1, 1});
  mkReadWrite(ms, "dgetri", {1, 0, 1, 1, 0, 1, 0});
  mkReadOnly(ms, "xerbla");
  mkReadWrite(ms, "dgetf2", {1, 1, 0, 1, 0, 0});
  mkReadWrite(ms, "dgetrf", {1, 1, 0, 1, 0, 0});
  mkReadWrite(ms, "dlaswp", {1, 0, 1, 1, 1, 1, 1});
}
