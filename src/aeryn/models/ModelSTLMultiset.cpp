#include "ModelSTLMultiset.h"

using namespace llvm;

namespace moya {

ModelSTLMultiset::ModelSTLMultiset(const std::string& fname,
                                   FuncID fn,
                                   const Models& ms)
    : ModelSTLSet(fname, STDKind::STD_STL_multiset, fn, ms) {
  ;
}

} // namespace moya
