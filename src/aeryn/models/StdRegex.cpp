#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;
using namespace moya;

static inline std::string mk(const std::string& base, const std::string& name) {
  std::stringstream ss;

  ss << "std::" << base << "::" << name;

  return ss.str();
}

static inline std::string mkre(const std::string& name) {
  return mk("basic_regex", name);
}

void Models::initStdRegex(const Module& module) {
  add(new ModelReadWrite(mkre("basic_regex()"), {0}));
  add(new ModelReadWrite(mkre("basic_regex(char*, unsigned long, int)"),
                         {0, 1, 1, 1}));
  add(new ModelReadWrite(mkre("basic_regex(char*, char*, int)"), {0, 1, 1}));
  add(new ModelReadWrite(mkre("~basic_regex()"), {0}));
  add(new ModelReadWrite(
      mkre("operator=(basic_regex&)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      mkre("operator=(char*)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      mkre("assign(std::basic_string&)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      mkre("assign(std::string&)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      mkre("assign(char*, int)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(mkre("assign(char*, unsigned long, int)"),
                         {0, 1, 1, 1},
                         Model::ReturnSpec::Arg0));
  add(new ModelReadOnly(mkre("mark_count()")));
  add(new ModelReadOnly(mkre("flags()")));
  add(new ModelReadOnly(mkre("getloc()")));
  add(new ModelReadWrite(
      mkre("imbue(std::locale_type&)"), {0, 1}, Model::ReturnSpec::Arg1));
  add(new ModelReadWrite(mkre("swap(std::basic_regex&)"), {0, 0}));
  add(new ModelReadOnly(mkre("length()")));
  add(new ModelReadOnly(mkre("str()")));
  add(new ModelReadOnly(mkre("string_type()")));
  add(new ModelReadOnly(mkre("compare(std::basic_regex::sub_match&)")));
  add(new ModelReadOnly(mkre("compare(std::basic_string&)")));
  add(new ModelReadOnly(mkre("compare(std::string&)")));
  add(new ModelReadWrite(mkre("match_results(match_results&)"), {0, 1}));
  add(new ModelReadWrite(mkre("~match_results()"), {0}));
  add(new ModelReadWrite(
      mkre("operator==(match_results&)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadOnly(mkre("get_allocator()")));
  add(new ModelReadOnly(mkre("ready()")));
  add(new ModelReadOnly(mkre("empty()")));
  add(new ModelReadOnly(mkre("size()")));
  add(new ModelReadOnly(mkre("max_size()")));
  add(new ModelReadOnly(mkre("length(unsigned long)")));
  add(new ModelReadOnly(mkre("str(unsigned long)")));
  add(new ModelReadOnly(mkre("operator[](unsigned long)"),
                        Model::ReturnSpec::Arg0));
  add(new ModelReadOnly(mkre("prefix()")));
  add(new ModelReadOnly(mkre("suffix()")));
}
