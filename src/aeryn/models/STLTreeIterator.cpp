#include "ModelReadWrite.h"
#include "ModelSTLTree.h"
#include "Models.h"
#include "common/LangConfig.h"

#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;
using namespace moya;

static std::string getIteratorName() {
  return STDConfig::getSTDName(STDKind::STD_STL_iterator_tree);
}

static std::string getConstIteratorName() {
  return STDConfig::getSTDName(STDKind::STD_STL_const_iterator_tree);
}

static std::string mkIter(const std::string& name, bool cnst) {
  std::string s;
  raw_string_ostream ss(s);
  std::string iterator = cnst ? getConstIteratorName() : getIteratorName();

  ss << iterator << "::" << name;

  return ss.str();
}

static std::string mkCompare(const std::string& op, bool cnst) {
  std::string s;
  raw_string_ostream ss(s);
  std::string iterator = cnst ? getConstIteratorName() : getIteratorName();

  ss << iterator << "::operator" << op << "(iterator)";

  return ss.str();
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLTree(name, STDKind::STD_STL_iterator_tree, fn, ms));
}

void Models::initSTLTreeIterator(const Module& module) {
  Models& ms = *this;

  add(new ModelReadWrite(mkIter("_Rb_tree_iterator()", false), {0}, true));
  add(new ModelReadWrite(
      mkIter("_Rb_tree_iterator(iterator)", false), {0, 1}, true));
  add(new ModelReadWrite(mkIter("_Rb_tree_const_iterator()", true), {0}, true));
  add(new ModelReadWrite(
      mkIter("_Rb_tree_const_iterator(iterator)", true), {0, 1}, true));

  // Add the iterator comparison operator overload models
  for(const std::string& op : {"==", "!="})
    for(bool cnst : {false, true})
      mkModel(ms, mkCompare(op, cnst), ModelSTLTree::IteratorCompare);

  for(bool cnst : {false, true}) {
    mkModel(ms, mkIter("operator*()", cnst), ModelSTLTree::IteratorDeref);
    mkModel(ms, mkIter("operator->()", cnst), ModelSTLTree::IteratorDeref);
    mkModel(ms, mkIter("operator++()", cnst), ModelSTLTree::IteratorSelf);
    mkModel(ms, mkIter("operator++(int)", cnst), ModelSTLTree::IteratorNew);
    mkModel(ms, mkIter("operator--()", cnst), ModelSTLTree::IteratorSelf);
    mkModel(ms, mkIter("operator--(int)", cnst), ModelSTLTree::IteratorNew);
  }
}
