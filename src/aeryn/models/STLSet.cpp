#include "ModelSTLSet.h"
#include "Models.h"
#include "common/LangConfig.h"

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_set);
}

static std::string mk(const std::string& name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "std::operator" << op << "(" << getContainerName() << ", "
       << getContainerName() << ")";
    ms.add(new ModelSTLSet(ss.str(), ModelSTLSet::Compare, ms));
  }
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLSet(name, fn, ms));
}

void Models::initSTLSet(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms, mk("set()"), ModelSTLSet::ConstructorEmpty);
  mkModel(ms, mk("set(std::less, allocator)"), ModelSTLSet::ConstructorEmpty);
  mkModel(ms, mk("set(T, T)"), ModelSTLSet::ConstructorRange);
  mkModel(
      ms, mk("set(T, T, std::less, allocator)"), ModelSTLSet::ConstructorRange);
  mkModel(ms, mk("set(std::set)"), ModelSTLSet::ConstructorCopy);
  mkModel(ms,
          mk("set(std::initializer_list, std::less, allocator)"),
          ModelSTLSet::ConstructorInitList);

  // Destructor
  mkModel(ms, mk("~set()"), ModelSTLSet::Destructor);

  // operator =
  mkModel(ms, mk("operator=(std::set)"), ModelSTLSet::OperatorContainer);
  mkModel(ms,
          mk("operator=(std::initializer_list)"),
          ModelSTLSet::OperatorInitList);

  // allocator
  mkModel(ms, mk("get_allocator()"), ModelSTLSet::GetAllocator);

  // Iterators
  mkModel(ms, mk("begin()"), ModelSTLSet::Begin);
  mkModel(ms, mk("cbegin()"), ModelSTLSet::Begin);
  mkModel(ms, mk("end()"), ModelSTLSet::End);
  mkModel(ms, mk("cend()"), ModelSTLSet::End);
  mkModel(ms, mk("rbegin()"), ModelSTLSet::Begin);
  mkModel(ms, mk("crbegin()"), ModelSTLSet::Begin);
  mkModel(ms, mk("rend()"), ModelSTLSet::End);
  mkModel(ms, mk("crend()"), ModelSTLSet::End);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLSet::Size);
  mkModel(ms, mk("size()"), ModelSTLSet::Size);
  mkModel(ms, mk("max_size()"), ModelSTLSet::Size);

  // Modifiers
  mkModel(ms, mk("clear()"), ModelSTLSet::Clear);

  mkModel(ms, mk("emplace(...)"), ModelSTLSet::EmplaceElement);
  mkModel(ms, mk("emplace_hint(iterator, ...)"), ModelSTLSet::EmplaceElementAt);

  mkModel(ms, mk("insert(*)"), ModelSTLSet::InsertElement);
  mkModel(ms, mk("insert(T)"), ModelSTLSet::InsertElement);
  mkModel(ms, mk("insert(iterator, *)"), ModelSTLSet::InsertElementAt);
  mkModel(ms, mk("insert(T, T)"), ModelSTLSet::InsertRange);
  mkModel(ms, mk("insert(std::initializer_list)"), ModelSTLSet::InsertInitList);

  mkModel(ms, mk("erase(*)"), ModelSTLSet::EraseElement);
  mkModel(ms, mk("erase(iterator)"), ModelSTLSet::EraseElementAt);
  mkModel(ms, mk("erase(iterator, iterator)"), ModelSTLSet::EraseRange);

  mkModel(ms, mk("swap(std::set)"), ModelSTLSet::Swap);

  // Lookup
  mkModel(ms, mk("count(*)"), ModelSTLSet::Size);
  mkModel(ms, mk("find(*)"), ModelSTLSet::Find);
  mkModel(ms, mk("equal_range(*)"), ModelSTLSet::FindRange);
  mkModel(ms, mk("lower_bound(*)"), ModelSTLSet::FindRange);
  mkModel(ms, mk("upper_bound(*)"), ModelSTLSet::FindRange);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);
}
