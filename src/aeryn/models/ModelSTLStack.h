#ifndef MOYA_MODELS_MODEL_STL_STACK_H
#define MOYA_MODELS_MODEL_STL_STACK_H

#include "ModelSTLDeque.h"

namespace moya {

class ModelSTLStack : public ModelSTLDeque {
public:
  ModelSTLStack(const std::string&, FuncID, const Models&);

  virtual ~ModelSTLStack() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_STACK_H
