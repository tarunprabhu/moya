#include "ModelSTLForwardList.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

using namespace llvm;

namespace moya {

// This is the forward list layout in memory:
//
// forward_list = { base* }
//
// base* points to a node which contains the data
//
// node = { base*, T }
//
// When a forward_list is created, a single node is allocated into which all the
// data is stored. The pointer in the node is self-referential.
//

ModelSTLForwardList::ModelSTLForwardList(const std::string& fname,
                                         STDKind cont,
                                         STDKind iter,
                                         STDKind citer,
                                         FuncID fn,
                                         const Models& ms)
    : ModelSTLContainer(fname, cont, iter, citer, fn, ms) {
  switch(fn) {
  case Merge:
    processFn = static_cast<ExecuteFn>(&ModelSTLForwardList::modelMerge);
    break;
  case Splice:
    processFn = static_cast<ExecuteFn>(&ModelSTLForwardList::modelSplice);
    break;
  default:
    // Everything else will have been initialized in the ModelSTLContainer()
    // constructor
    break;
  }
}

ModelSTLForwardList::ModelSTLForwardList(const std::string& fname,
                                         FuncID fn,
                                         const Models& ms)
    : ModelSTLForwardList(fname,
                          STDKind::STD_STL_forward_list,
                          STDKind::STD_STL_iterator_forward_list,
                          STDKind::STD_STL_const_iterator_forward_list,
                          fn,
                          ms) {
  ;
}

Contents ModelSTLForwardList::getBeginIterator(const Addresses& conts,
                                               const Instruction* call,
                                               const Function* f,
                                               Store& store,
                                               AnalysisStatus& changed) const {
  Contents ret;
  for(const Address* node : getNode(conts, call, f, store, changed))
    ret.insert(node);
  return ret;
}

Addresses ModelSTLForwardList::getIteratorNode(const Addresses& iters,
                                               const Instruction* call,
                                               const Function* f,
                                               Store& store,
                                               AnalysisStatus& changed) const {
  PointerType* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();

  return store.readAs<Address>(iters, pBaseTy, call, changed);
}

Addresses ModelSTLForwardList::getIteratorDeref(const Addresses& iters,
                                                const Instruction* call,
                                                const Function* f,
                                                Store& store,
                                                AnalysisStatus& changed) const {
  Addresses ret;
  PointerType* pty = Metadata::getSTLBaseType(f)->getPointerTo();
  unsigned long offset = Metadata::getSTLValueOffset(f);

  for(const auto* a : store.readAs<Address>(iters, pty, call, changed))
    ret.insert(store.make(a, offset));
  return ret;
}

Addresses ModelSTLForwardList::getIteratorNew(const Addresses& iters,
                                              const Instruction* call,
                                              const Function* f,
                                              Store& store,
                                              AnalysisStatus& changed) const {
  Addresses ret;
  Type* pty = Metadata::getSTLBaseType(f)->getPointerTo();
  unsigned long offset = Metadata::getSTLValueOffset(f);

  for(const auto* a : store.readAs<Address>(iters, pty, call, changed))
    ret.insert(store.make(a, offset));
  return ret;
}

AnalysisStatus
ModelSTLForwardList::updateContainerPointers(const Address* base,
                                             const Addresses& vals,
                                             const Instruction* call,
                                             const Function* f,
                                             Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  unsigned vtadj = getContainerVtableOffset(base, call, f, store);
  Type* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();
  for(const Content* c : vals)
    store.write(store.make(base, vtadj), c, pBaseTy, call, changed);

  return changed;
}

AnalysisStatus ModelSTLForwardList::updateNodePointers(const Address* base,
                                                       const Addresses& vals,
                                                       const Instruction* call,
                                                       const Function* f,
                                                       Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();
  for(const Content* c : vals)
    store.write(base, c, pBaseTy, call, changed);

  return changed;
}

AnalysisStatus ModelSTLForwardList::modelMerge(const Instruction* call,
                                               const Function* f,
                                               const Vector<Contents>& args,
                                               Environment& env,
                                               Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts1 = args.at(0).getAs<Address>(call);
  const Addresses& conts2 = args.at(1).getAs<Address>(call);
  Addresses bufs2 = getBuffer(conts2, call, f, store, changed);

  changed |= writeToContainer(conts1, bufs2, 0, call, f, store);
  changed |= markContainerWrite(conts1, call, f, store);
  changed |= markContainerWrite(conts2, call, f, store);
  changed |= markBufferWrite(conts1, call, f, store);
  changed |= markBufferWrite(conts2, call, f, store);

  return changed;
}

AnalysisStatus ModelSTLForwardList::modelSplice(const Instruction* call,
                                                const Function* f,
                                                const Vector<Contents>& args,
                                                Environment& env,
                                                Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts1 = args.at(0).getAs<Address>(call);
  const Addresses& conts2 = args.at(2).getAs<Address>(call);
  Addresses bufs2 = getBuffer(conts2, call, f, store, changed);

  changed |= writeToContainer(conts1, bufs2, 0, call, f, store);
  changed |= markContainerWrite(conts1, call, f, store);
  changed |= markContainerWrite(conts2, call, f, store);
  changed |= markBufferWrite(conts1, call, f, store);
  changed |= markBufferWrite(conts2, call, f, store);

  return changed;
}

} // namespace moya
