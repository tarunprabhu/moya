#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

using namespace llvm;
using namespace moya;

static AnalysisStatus modelOpen(const ModelCustom& model,
                                const Instruction* call,
                                const Function* f,
                                const Vector<Contents>& args,
                                Environment& env,
                                Store& store,
                                const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(call)) {
    Type* allocTy = dyn_cast<PointerType>(f->getReturnType())->getElementType();
    const Address* addr = AbstractUtils::allocate(allocTy, store, call, caller);
    store.write(addr, Contents(), allocTy, call, changed);
    changed |= env.add(call, addr);
  }
  env.push(env.lookup(call));

  return changed;
}

static AnalysisStatus modelError(const ModelCustom& model,
                                 const Instruction* call,
                                 const Function* f,
                                 const Vector<Contents>& args,
                                 Environment& env,
                                 Store& store,
                                 const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(call)) {
    Type* i8 = Type::getInt8Ty(f->getContext());
    const Address* addr = AbstractUtils::allocate(i8, store, call, caller);
    store.write(addr, store.getRuntimeScalar(), i8, call, changed);
    changed |= env.add(call, addr);
  }
  env.push(env.lookup(call));

  return changed;
}

void Models::initLibZlib(const Module& module) {
  add(new ModelCustom("gzopen", modelOpen));
  add(new ModelCustom("gzopen64", modelOpen));
  add(new ModelCustom("gzdopen", modelOpen));
  add(new ModelReadWrite("gzbuffer", {0, 1}, true));
  add(new ModelReadWrite("gzsetparams", {0, 1, 1}, true));
  add(new ModelReadWrite("gzread", {0, 0, 1}, true));
  add(new ModelReadWrite("gzwrite", {0, 1, 1}, true));
  add(new ModelReadWrite("gzprintf", {0, 1}, true));
  add(new ModelReadWrite("gzputs", {0, 1}, true));
  add(new ModelReadWrite("gzgets", {0, 0, 1}, true));
  add(new ModelReadWrite("gzungetc", {1, 0}, true));
  add(new ModelReadWrite("gzflush", {0, 1}, true));
  add(new ModelReadWrite("gzseek", {0, 1, 1}, true));
  add(new ModelReadWrite("gzrewind", {0}, true));
  add(new ModelReadOnly("gztell"));
  add(new ModelReadOnly("gzoffset"));
  add(new ModelReadOnly("gzeof"));
  add(new ModelReadOnly("gzdirect"));
  add(new ModelReadWrite("gzclose", {0}, true));
  add(new ModelReadWrite("gzclose_r", {0}, true));
  add(new ModelReadWrite("gzclose_w", {0}, true));
  add(new ModelCustom("gzerror", modelError));
  add(new ModelReadWrite("gzclearerr", {0}, true));

  add(new ModelReadWrite("adler32", {1, 0, 1}));
  add(new ModelReadOnly("adler32_combine"));
  add(new ModelReadWrite("crc32", {1, 0, 1}));

  add(new ModelReadWrite("deflateInit", {0, 1}, true));
  add(new ModelReadWrite("deflate", {0, 1}, true));
  add(new ModelReadWrite("deflateEnd", {0}, true));
  add(new ModelReadWrite("inflateInit", {0}, true));
  add(new ModelReadWrite("inflate", {0, 1}, true));
  add(new ModelReadWrite("inflateEnd", {0}, true));
  add(new ModelReadWrite("deflateInit2", {0, 1, 1, 1, 1, 1}, true));
  add(new ModelReadWrite("deflateInit2_", {0, 1, 1, 1, 1, 1, 1, 1}, true));
  add(new ModelReadWrite("deflateSetDictionary", {0, 1, 1}, true));
  add(new ModelReadWrite("deflateReset", {0}, true));
  add(new ModelReadWrite("deflateParams", {0, 1, 1}, true));
  add(new ModelReadWrite("deflateTune", {0, 1, 1, 1, 1}, true));
  add(new ModelReadWrite("deflateBound", {0, 1}, true));
  add(new ModelReadWrite("deflatePending", {0, 0, 0}, true));
  add(new ModelReadWrite("deflatePrime", {0, 1, 1}, true));
  add(new ModelReadWrite("deflateSetHeader", {0, 0}, true));
  add(new ModelReadWrite("inflateInit2", {0, 1}, true));
  add(new ModelReadWrite("inflateInit2_", {0, 1, 1, 1}, true));
  add(new ModelReadWrite("inflateSetDictionary", {0, 1, 1}, true));
  add(new ModelReadWrite("inflateGetDictionary", {0, 0, 0}, true));
  add(new ModelReadWrite("inflateSync", {0}, true));
  add(new ModelReadWrite("inflateReset", {0}, true));
  add(new ModelReadWrite("inflateReset2", {0, 1}, true));
  add(new ModelReadWrite("inflatePrime", {0, 1, 1}, true));
  add(new ModelReadWrite("inflateMark", {0}, true));
  add(new ModelReadWrite("inflateGetHeader", {0, 0}, true));
  add(new ModelReadWrite("inflateBackInit", {0, 1, 0}, true));
  add(new ModelReadWrite("inflateBack", {0, 1, 1}, true));
  add(new ModelReadWrite("inflateBackEnd", {0}, true));
  add(new ModelReadOnly("zlibCompileFlags"));
  add(new ModelReadWrite("compress", {0, 0, 1, 1}));
  add(new ModelReadWrite("compress2", {0, 0, 1, 1, 1}));
  add(new ModelReadOnly("compressBound"));
  add(new ModelReadWrite("uncompress", {0, 0, 1, 1}));
}
