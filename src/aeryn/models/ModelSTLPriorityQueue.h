#ifndef MOYA_MODELS_MODEL_STL_PRIORITY_QUEUE_H
#define MOYA_MODELS_MODEL_STL_PRIORITY_QUEUE_H

#include "ModelSTLVector.h"

namespace moya {

// Currently, std::priority_queue is implemented using a vector under the hood
class ModelSTLPriorityQueue : public ModelSTLVector {
public:
  ModelSTLPriorityQueue(const std::string&, FuncID, const Models&);
  virtual ~ModelSTLPriorityQueue() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_PRIORITY_QUEUE_H
