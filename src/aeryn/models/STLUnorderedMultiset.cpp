#include "ModelSTLUnorderedMultiset.h"
#include "Models.h"
#include "common/LangConfig.h"

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_unordered_multiset);
}

static std::string mk(std::string name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "std::operator" << op << "(" << getContainerName() << ", "
       << getContainerName() << ")";
    ms.add(new ModelSTLUnorderedMultiset(
        ss.str(), ModelSTLUnorderedMultiset::Compare, ms));
  }
}

static void
mkModel(Models& ms, std::string name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLUnorderedMultiset(name, fn, ms));
}

void Models::initSTLUnorderedMultiset(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms,
          mk("unordered_multiset()"),
          ModelSTLUnorderedMultiset::ConstructorEmpty);
  mkModel(ms,
          mk("unordered_multiset(unsigned long, std::hash, std::equal_to, "
             "allocator)"),
          ModelSTLUnorderedMultiset::ConstructorEmpty);
  mkModel(ms,
          mk("unordered_multiset(iterator, iterator, unsigned long, std::hash, "
             "std::equal_to, allocator)"),
          ModelSTLUnorderedMultiset::ConstructorRange);
  mkModel(ms,
          mk("unordered_set(iterator, iterator, std::hash, "
             "std::equal_to, allocator)"),
          ModelSTLUnorderedMultiset::ConstructorRange);
  mkModel(ms,
          mk("unordered_multiset(std::unordered_multiset)"),
          ModelSTLUnorderedMultiset::ConstructorCopy);
  mkModel(ms,
          mk("unordered_multiset(std::initializer_list, unsigned long, "
             "std::hash, std::equal_to, allocator)"),
          ModelSTLUnorderedMultiset::ConstructorInitList);

  // Destructor
  mkModel(
      ms, mk("~unordered_multiset()"), ModelSTLUnorderedMultiset::Destructor);

  // operator =
  mkModel(ms,
          mk("operator=(std::unordered_multiset)"),
          ModelSTLUnorderedMultiset::OperatorContainer);
  mkModel(ms,
          mk("operator=(std::initializer_list)"),
          ModelSTLUnorderedMultiset::OperatorInitList);

  // allocator
  mkModel(ms, mk("get_allocator()"), ModelSTLUnorderedMultiset::GetAllocator);

  // Iterators
  mkModel(ms, mk("begin()"), ModelSTLUnorderedMultiset::Begin);
  mkModel(ms, mk("cbegin()"), ModelSTLUnorderedMultiset::Begin);
  mkModel(ms, mk("end()"), ModelSTLUnorderedMultiset::End);
  mkModel(ms, mk("cend()"), ModelSTLUnorderedMultiset::End);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLUnorderedMultiset::Size);
  mkModel(ms, mk("size()"), ModelSTLUnorderedMultiset::Size);
  mkModel(ms, mk("max_size()"), ModelSTLUnorderedMultiset::Size);

  // Modifiers
  mkModel(ms, mk("clear()"), ModelSTLUnorderedMultiset::Clear);

  mkModel(ms, mk("emplace(...)"), ModelSTLUnorderedMultiset::EmplaceElement);
  mkModel(ms,
          mk("emplace_hint(iterator, ...)"),
          ModelSTLUnorderedMultiset::EmplaceElementAt);

  mkModel(ms, mk("insert(*)"), ModelSTLUnorderedMultiset::InsertElement);
  mkModel(ms,
          mk("insert(iterator, *)"),
          ModelSTLUnorderedMultiset::InsertElementAt);
  mkModel(ms,
          mk("insert(iterator, iterator)"),
          ModelSTLUnorderedMultiset::InsertRange);
  mkModel(ms,
          mk("insert(std::initializer_list)"),
          ModelSTLUnorderedMultiset::InsertInitList);

  mkModel(ms, mk("erase(*)"), ModelSTLUnorderedMultiset::EraseElement);
  mkModel(ms, mk("erase(iterator)"), ModelSTLUnorderedMultiset::EraseElementAt);
  mkModel(ms,
          mk("erase(iterator, iterator)"),
          ModelSTLUnorderedMultiset::EraseRange);

  mkModel(
      ms, mk("swap(std::unordered_multiset)"), ModelSTLUnorderedMultiset::Swap);

  // Lookup
  mkModel(ms, mk("count(*)"), ModelSTLUnorderedMultiset::Size);
  mkModel(ms, mk("find(*)"), ModelSTLUnorderedMultiset::Find);
  mkModel(ms, mk("equal_range(*)"), ModelSTLUnorderedMultiset::FindRange);
  mkModel(ms, mk("lower_bound(*)"), ModelSTLUnorderedMultiset::FindRange);
  mkModel(ms, mk("upper_bound(*)"), ModelSTLUnorderedMultiset::FindRange);

  // Buckets
  mkModel(
      ms, mk("begin(unsigned long)"), ModelSTLUnorderedMultiset::BucketBegin);
  mkModel(
      ms, mk("cbegin(unsigned long)"), ModelSTLUnorderedMultiset::BucketBegin);
  mkModel(ms, mk("end(unsigned long)"), ModelSTLUnorderedMultiset::BucketEnd);
  mkModel(ms, mk("cend(unsigned long)"), ModelSTLUnorderedMultiset::BucketEnd);
  mkModel(ms, mk("bucket_count()"), ModelSTLUnorderedMultiset::Buckets);
  mkModel(ms, mk("max_bucket_count()"), ModelSTLUnorderedMultiset::Buckets);
  mkModel(
      ms, mk("bucket_size(unsigned long)"), ModelSTLUnorderedMultiset::Buckets);
  mkModel(ms, mk("bucket(*)"), ModelSTLUnorderedMultiset::Buckets);

  // Hash policy
  mkModel(ms, mk("load_factor()"), ModelSTLUnorderedMultiset::GetLoadFactor);
  mkModel(
      ms, mk("max_load_factor()"), ModelSTLUnorderedMultiset::GetLoadFactor);
  mkModel(ms,
          mk("max_load_factor(float)"),
          ModelSTLUnorderedMultiset::SetLoadFactor);
  mkModel(ms, mk("rehash(unsigned long)"), ModelSTLUnorderedMultiset::Resize);
  mkModel(ms, mk("reserve(unsigned long)"), ModelSTLUnorderedMultiset::Resize);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);
}
