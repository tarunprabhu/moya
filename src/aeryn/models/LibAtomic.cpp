#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/LLVMUtils.h"

using namespace llvm;
using namespace moya;

static std::string mk(std::string base, int suffix) {
  std::stringstream ss;
  ss << base << "_" << suffix;
  return ss.str();
}

static AnalysisStatus modelLoad(const ModelCustom& model,
                                const Instruction* load,
                                const Function* f,
                                const Vector<Contents>& args,
                                Environment& env,
                                Store& store,
                                const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(1), LLVMUtils::getArgumentType(f, 1), env, store, load);
  changed |= AbstractUtils::load(
      load, args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, caller);
  env.push(env.lookup(load));

  return changed;
}

void Models::initLibAtomic(const Module& module) {
  for(int i : {1, 2, 4, 8}) {
    add(new ModelCustom(mk("__atomic_load", i), modelLoad));
    add(new ModelReadOnly(mk("__atomic_fetch_add", i)));
  }
}
