#include "ModelCustom.h"
#include "Models.h"

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>

#include "aeryn/AbstractUtils.h"
#include "common/LLVMUtils.h"
#include "common/StringUtils.h"
#include "common/Vector.h"

using llvm::dyn_cast;

using namespace moya;

static AnalysisStatus model_lookup(const ModelCustom& model,
                                   const llvm::Instruction* call,
                                   const llvm::Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const llvm::Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(auto* pty = LLVMUtils::getAnalysisTypeAs<llvm::PointerType>(
         LLVMUtils::getArgument(f, 0)))
    if(auto* sty = dyn_cast<llvm::StructType>(pty->getElementType()))
      for(const Store::Address* addr : args.at(0).getAs<Address>(call))
        env.push(store.read(addr,
                            sty->getElementType(0),
                            call,
                            changed));

  return changed;
}

void Models::initLibFL2C(const llvm::Module& module) {
  add(new ModelCustom("fl2c::1", model_lookup));
  add(new ModelCustom("fl2c::2", model_lookup));
  add(new ModelCustom("fl2c::3", model_lookup));
  add(new ModelCustom("fl2c::4", model_lookup));
  add(new ModelCustom("fl2c::5", model_lookup));
  add(new ModelCustom("fl2c::6", model_lookup));
  add(new ModelCustom("fl2c::7", model_lookup));
}
