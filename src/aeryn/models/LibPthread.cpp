#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

using namespace llvm;
using namespace moya;

static AnalysisStatus modelCreate(const ModelCustom& model,
                                  const Instruction* call,
                                  const Function* f,
                                  const Vector<Contents>& args,
                                  Environment& env,
                                  Store& store,
                                  const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markWrite(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= model.markWrite(
      args.at(1), LLVMUtils::getArgumentType(f, 1), env, store, call);
  changed |= model.markRead(
      args.at(2), LLVMUtils::getArgumentType(f, 2), env, store, call);
  changed |= model.markRead(
      args.at(3), LLVMUtils::getArgumentType(f, 3), env, store, call);

  for(const auto* code :
      store.readAs<ContentCode>(args.at(2), nullptr, call, changed))
    changed
        |= env.add(LLVMUtils::getArgument(code->getFunction(), 0), args.at(3));

  env.push(store.getRuntimeScalar());

  return changed;
}

static AnalysisStatus modelOnce(const ModelCustom& model,
                                const Instruction* call,
                                const Function* f,
                                const Vector<Contents>& args,
                                Environment& env,
                                Store& store,
                                const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= model.markRead(
      args.at(1), LLVMUtils::getArgumentType(f, 1), env, store, call);

  return changed;
}

static AnalysisStatus modelKeyCreate(const ModelCustom& model,
                                     const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store,
                                     const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("UNIMPLEMENTED: Model for pthread_key_create");

  return changed;
}

static AnalysisStatus modelCleanupPush(const ModelCustom& model,
                                       const Instruction* call,
                                       const Function* f,
                                       const Vector<Contents>& args,
                                       Environment& env,
                                       Store& store,
                                       const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;
  Type* arg0Ty = LLVMUtils::getArgumentType(f, 0);
  Type* arg1Ty = LLVMUtils::getArgumentType(f, 1);

  changed |= model.markRead(args.at(0), arg0Ty, env, store, call);
  changed |= model.markRead(args.at(1), arg1Ty, env, store, call);

  for(const auto* code :
      store.readAs<ContentCode>(args.at(0), arg0Ty, call, changed))
    changed
        |= env.add(LLVMUtils::getArgument(code->getFunction(), 0), args.at(1));

  return changed;
}

void Models::initLibPThread(const Module& module) {
  add(new ModelCustom("pthread_create", modelCreate));
  add(new ModelReadOnly("pthread_equal"));
  add(new ModelReadOnly("pthread_exit"));
  add(new ModelReadWrite("pthread_join", {1, 0}));
  add(new ModelReadWrite("pthread_join_np", {1, 0}));
  add(new ModelReadOnly("pthread_self"));
  add(new ModelReadWrite("pthread_mutex_init", {0, 1}));
  add(new ModelReadWrite("pthread_mutex_destroy", {0}));
  add(new ModelReadWrite("pthread_mutex_lock", {0}, true));
  add(new ModelReadWrite("pthread_mutex_trylock", {0}, true));
  add(new ModelReadWrite("pthread_mutex_unlock", {0}, true));
  add(new ModelReadWrite("pthread_mutex_getprioceiling", {1, 0}));
  add(new ModelReadWrite("pthread_mutex_setprioceiling", {0, 1, 0}));
  add(new ModelReadWrite("pthread_cond_init", {0, 1}));
  add(new ModelReadWrite("pthread_cond_destroy", {0}));
  add(new ModelReadWrite("pthread_cond_wait", {0, 0}));
  add(new ModelReadWrite("pthread_cond_timedwait", {0, 0, 1}));
  add(new ModelReadWrite("pthread_cond_signal", {0}));
  add(new ModelReadWrite("pthread_cond_broadcast", {0}));
  add(new ModelReadWrite("pthread_condattr_init", {0}));
  add(new ModelReadWrite("pthread_condattr_destroy", {0}));
  add(new ModelReadWrite("pthread_condattr_getpshared", {1, 0}));
  add(new ModelReadWrite("pthread_condattr_setpshared", {0, 1}));
  add(new ModelCustom("pthread_once", modelOnce));
  add(new ModelCustom("pthread_key_create", modelKeyCreate));
  add(new ModelReadWrite("pthread_key_delete", {0}));
  add(new ModelReadWrite("pthread_setspecific", {0, 1}));
  add(new ModelReadOnly("pthread_getspecific"));
  add(new ModelCustom("pthread_cleanup_push", modelCleanupPush));
  add(new ModelReadOnly("pthread_cleanup_pop"));
  add(new ModelReadWrite("pthread_attr_init", {0}));
  add(new ModelReadWrite("pthread_attr_destroy", {0}));
  add(new ModelReadWrite("pthread_attr_getstacksize", {1, 0}));
  add(new ModelReadWrite("pthread_attr_setstacksize", {0, 1}));
  add(new ModelReadWrite("pthread_attr_getdetachstate", {1, 0}));
  add(new ModelReadWrite("pthread_attr_setdetachstate", {0, 1}));
  add(new ModelReadWrite("flockfile", {0}));
  add(new ModelReadWrite("ftrylockfile", {0}));
  add(new ModelReadWrite("getc_unlocked", {0}));
  add(new ModelReadOnly("getchar_unlocked"));
  add(new ModelReadWrite("putc_unlocked", {1, 0}));
  add(new ModelReadOnly("putchar_unlocked"));
  add(new ModelReadWrite("pthread_attr_getinheritsched", {1, 0}));
  add(new ModelReadWrite("pthread_attr_setinheritsched", {0, 1}));
  add(new ModelReadWrite("pthread_attr_getschedparam", {1, 0}));
  add(new ModelReadWrite("pthread_attr_setschedparam", {0, 1}));
  add(new ModelReadOnly("pthread_clear_exit"));
  add(new ModelReadOnly("pthread_delay_np"));
  add(new ModelReadOnly("pthread_detach"));
  add(new ModelReadWrite("pthread_extendedjoin_np", {1, 0, 1}));
  add(new ModelReadOnly("pthread_getconcurrency"));
  add(new ModelReadOnly("pthread_setconcurrency"));
  add(new ModelReadWrite("pthread_getpthreadoption_np", {0}));
  add(new ModelReadWrite("pthread_setpthreadoption_np", {1}));
  add(new ModelReadOnly("pthread_getthreadid_np"));
  add(new ModelReadWrite("pthread_getunique_np", {1, 0}));
  add(new ModelReadOnly("pthread_is_multithreaded_np"));
  add(new ModelReadOnly("pthread_cancel"));
  add(new ModelReadWrite("pthread_cleanup_peek_np", {0}));
  add(new ModelReadWrite("pthread_getcancelstate_np", {0}));
  add(new ModelReadWrite("pthread_setcancelstate_np", {1, 0}));
  add(new ModelReadWrite("pthread_setcanceltype", {1, 0}));
  add(new ModelReadOnly("pthread_testcancel"));
  add(new ModelReadWrite("pthread_test_exit_np", {0}));
  add(new ModelReadWrite("pthread_mutexattr_destroy", {0}));
  add(new ModelReadWrite("pthread_mutexattr_getkind_np", {1, 0}));
  add(new ModelReadWrite("pthread_mutexattr_setkind_np", {0, 1}));
  add(new ModelReadWrite("pthread_mutexattr_getname_np", {1, 0}));
  add(new ModelReadWrite("pthread_mutexattr_setname_np", {0, 1}));
  add(new ModelReadWrite("pthread_mutexattr_getpshared", {1, 0}));
  add(new ModelReadWrite("pthread_mutexattr_setpshared", {0, 1}));
  add(new ModelReadWrite("pthread_mutexattr_gettype", {1, 0}));
  add(new ModelReadWrite("pthread_mutexattr_settype", {0, 1}));
  add(new ModelReadWrite("pthread_mutexattr_init", {0}));
  add(new ModelReadOnly("pthread_set_mutexattr_default_np"));
  add(new ModelReadOnly("pthread_lock_global_np"));
  add(new ModelReadOnly("pthread_unlock_global_np"));
  add(new ModelReadWrite("pthread_get_expiration_np", {1, 0}));
  add(new ModelReadWrite("pthread_rwlockattr_init", {0}));
  add(new ModelReadWrite("pthread_rwlockattr_destroy", {0}));
  add(new ModelReadWrite("pthread_rwlockattr_getpshared", {1, 0}));
  add(new ModelReadWrite("pthread_rwlockattr_setpshared", {0, 1}));
  add(new ModelReadWrite("rwlock_init", {0}));
  add(new ModelReadWrite("rwlock_destroy", {0}));
  add(new ModelReadWrite("rwlock_rdlock", {0}));
  add(new ModelReadWrite("rwlock_wrlock", {0}));
  add(new ModelReadWrite("rwlock_unlock", {0}));
  add(new ModelReadWrite("rwlock_tryrdlock", {0}));
  add(new ModelReadWrite("rwlock_trywrlock", {0}));
  add(new ModelReadWrite("rwlock_timedrdlock_np", {0}));
  add(new ModelReadWrite("rwlock_timedwrlock_np", {0}));
  add(new ModelReadOnly("pthread_kill"));
  add(new ModelReadWrite("pthread_sigmask", {1, 0, 0}));
  add(new ModelReadWrite("pthread_signal_to_cancel_np", {0, 0}));
  add(new ModelReadOnly("pthread_atfork"));
  add(new ModelReadWrite("pthread_atfork_np", {0, 1, 1, 1}));
  add(new ModelReadWrite("pthread_attr_getschedpolicy", {1, 0}));
  add(new ModelReadWrite("pthread_attr_setschedpolicy", {0, 1}));
  add(new ModelReadWrite("pthread_attr_getscope", {1, 0}));
  add(new ModelReadWrite("pthread_attr_setscope", {0, 1}));
  add(new ModelReadWrite("pthread_attr_getstackaddr", {1, 0}));
  add(new ModelReadWrite("pthread_attr_setstackaddr", {0, 1}));
}
