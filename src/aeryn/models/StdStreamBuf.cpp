#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;
using namespace moya;

static inline std::string mk(const std::string& base, const std::string& name) {
  std::stringstream ss;

  ss << "std::" << base << "::" << name;

  return ss.str();
}

static inline std::string mksb(const std::string& name) {
  return mk("basic_streambuf", name);
}

static inline std::string mkisbit(const std::string& name) {
  return mk("istreambuf_iterator", name);
}

static inline std::string mkosbit(const std::string& name) {
  return mk("ostreambuf_iterator", name);
}

static AnalysisStatus modelAllocate(const ModelCustom& model,
                                    const Instruction* call,
                                    const Function* f,
                                    const Vector<Contents>& args,
                                    Environment& env,
                                    Store& store,
                                    const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* i8 = Type::getInt8Ty(call->getContext());
  if(not env.contains(call)) {
    for(const auto* addr : args.at(0).getAs<Address>(call)) {
      auto* ty = dyn_cast<PointerType>(LLVMUtils::getArgumentType(f, 0))
                     ->getElementType();
      StructType* sty = dyn_cast<StructType>(ty);
      const moya::StructLayout& sl = store.getStructLayout(sty);
      for(unsigned i = 1; i < 7; i++) {
        const Address* allocated
            = AbstractUtils::allocate(i8, store, call, caller);
        store.write(allocated, store.getRuntimeScalar(), i8, call, changed);
        store.write(store.make(addr, sl.getElementOffset(i)),
                    allocated,
                    i8->getPointerTo(),
                    call,
                    changed);
      }
    }
    changed |= env.add(call);
  }

  return changed;
}

static AnalysisStatus modelGet(const ModelCustom& model,
                               const Instruction* call,
                               const Function* f,
                               const Vector<Contents>& args,
                               Environment& env,
                               Store& store,
                               const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Contents ret;
  for(const auto* addr : args.at(0).getAs<Address>(call)) {
    Type* ty = dyn_cast<PointerType>(LLVMUtils::getArgumentType(f, 0))
                   ->getElementType();
    StructType* sty = dyn_cast<StructType>(ty);
    const moya::StructLayout& sl = store.getStructLayout(sty);
    ret.insert(store.read(store.make(addr, sl.getElementOffset(1)),
                          sty->getElementType(1),
                          call,
                          changed));
  }

  env.push(ret);

  return changed;
}

void Models::initStdStreambuf(const Module& module) {
  add(new ModelCustom(mksb("basic_streambuf()"), modelAllocate));
  add(new ModelReadWrite(
      mksb("basic_streambuf(basic_streambuf)"), {0, 1}, true));
  add(new ModelReadWrite(mksb("~basic_streambuf()"), {0, 1}, true));
  add(new ModelReadWrite(
      mksb("pubimbue()"), {0, 1}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(
      mksb("imbue()"), {0, 1}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(mksb("getloc()"), {0, 0}, true));
  add(new ModelReadWrite(mksb("pubsetbuf(char*, std::streamsize)"),
                         {0, 0, 1},
                         Model::ReturnSpec::Arg0,
                         true));
  add(new ModelReadWrite(mksb("setbuf(char*, std::streamsize)"),
                         {0, 0, 1},
                         Model::ReturnSpec::Arg0,
                         true));
  add(new ModelReadWrite(mksb("pubsetbuf(char*, long)"),
                         {0, 0, 1},
                         Model::ReturnSpec::Arg0,
                         true));
  add(new ModelReadWrite(
      mksb("setbuf(char*, long)"), {0, 0, 1}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(
      mksb("pubseekoff(unsigned long, ios_base::seekdir, std::_Ios_Openmode)"),
      {0, 1, 1, 1},
      Model::ReturnSpec::Arg0,
      true));
  add(new ModelReadWrite(
      mksb("seekoff(unsigned long, ios_base::seekdir, std::_Ios_Openmode)"),
      {0, 1, 1, 1},
      Model::ReturnSpec::Arg0,
      true));
  add(new ModelReadWrite(mksb("pubseekpos(unsigned long, std::_Ios_Openmode)"),
                         {0, 1, 1},
                         Model::ReturnSpec::Arg0,
                         true));
  add(new ModelReadWrite(mksb("seekpos(unsigned long, std::_Ios_Openmode)"),
                         {0, 1, 1},
                         Model::ReturnSpec::Arg0,
                         true));
  add(new ModelReadWrite(mksb("pubsync()"), {0}, true));
  add(new ModelReadWrite(mksb("sync()"), {0}, true));
  add(new ModelReadWrite(mksb("in_avail()"), {0}, true));
  add(new ModelReadWrite(mksb("snextc()"), {0}, true));
  add(new ModelReadWrite(mksb("sbumpc()"), {0}, true));
  add(new ModelReadWrite(mksb("sgetc()"), {0}, true));
  add(new ModelReadWrite(
      mksb("sgetn(char*, std::streamsize)"), {0, 0, 1}, true));
  add(new ModelReadWrite(
      mksb("xsgetn(char*, std::streamsize)"), {0, 0, 1}, true));
  add(new ModelReadWrite(mksb("sputc(char)"), {0, 1}, true));
  add(new ModelReadWrite(
      mksb("sputn(char*, std::streamsize)"), {0, 1, 1}, true));
  add(new ModelReadWrite(
      mksb("xsputn(char*, std::streamsize)"), {0, 1, 1}, true));
  add(new ModelReadWrite(mksb("sputbackc(char)"), {0, 1}, true));
  add(new ModelReadWrite(mksb("sungetc()"), {0}, true));
  add(new ModelReadWrite(
      mksb("operator=(std::basic_streambuf)"), {0, 1}, true));
  add(new ModelReadWrite(mksb("swap(std::basic_streambuf)"), {0, 0}, true));
  add(new ModelReadWrite(mksb("showmanyc()"), {0}, true));
  add(new ModelReadWrite(mksb("underflow()"), {0}, true));
  add(new ModelReadWrite(mksb("uflow()"), {0}, true));
  add(new ModelCustom(mksb("eback()"), modelGet));
  add(new ModelCustom(mksb("gptr()"), modelGet));
  add(new ModelCustom(mksb("egptr()"), modelGet));
  add(new ModelReadWrite(mksb("gbump(int)"), {0, 1}, true));
  add(new ModelReadWrite(
      mksb("setg(char*, char*, char*)"), {0, 1, 1, 1}, true));
  add(new ModelReadWrite(mksb("overflow(int)"), {0, 1}, true));
  add(new ModelReadOnly(mksb("pbase()")));
  add(new ModelReadOnly(mksb("pptr()")));
  add(new ModelReadOnly(mksb("epptr()")));
  add(new ModelReadWrite(mksb("pbump(int)"), {0, 1}, true));
  add(new ModelReadWrite(mksb("setg(char*, char*)"), {0, 1, 1}, true));
  add(new ModelReadWrite(mksb("pbackfail(int)"), {0, 1}, true));

  add(new ModelReadWrite(mkisbit("istreambuf_iterator()"), {0}));
  add(new ModelReadWrite(mkisbit("istreambuf_iterator(std::basic_istream)"),
                         {0, 1}));

  add(new ModelReadWrite(mkosbit("ostreambuf_iterator(std::basic_ostream)"),
                         {0, 1}));
}
