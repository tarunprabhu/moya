#include "ModelSTLStack.h"
#include "Models.h"
#include "common/LangConfig.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_stack);
}

static std::string mk(const std::string& name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "std::operator" << op << "(" << getContainerName() << ", "
       << getContainerName() << ")";
    ms.add(new ModelSTLStack(ss.str(), ModelSTLStack::Compare, ms));
  }
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLStack(name, fn, ms));
}

void Models::initSTLStack(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms, mk("stack()"), ModelSTLStack::ConstructorEmpty);
  mkModel(ms, mk("stack(*)"), ModelSTLStack::ConstructorCopy);
  mkModel(ms, mk("stack(std::stack)"), ModelSTLStack::ConstructorCopy);

  // operator =
  mkModel(ms, mk("operator=(std::stack)"), ModelSTLStack::OperatorContainer);

  // Element access
  mkModel(ms, mk("top()"), ModelSTLStack::AccessElement);

  // Destructors
  mkModel(ms, mk("~stack()"), ModelSTLStack::Destructor);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLStack::Size);
  mkModel(ms, mk("size()"), ModelSTLStack::Size);

  // Modifiers
  mkModel(ms, mk("emplace(...)"), ModelSTLStack::EmplaceElement);
  mkModel(ms, mk("push(*)"), ModelSTLStack::AddElement);
  mkModel(ms, mk("pop()"), ModelSTLStack::RemoveElement);
  mkModel(ms, mk("swap(std::stack)"), ModelSTLStack::Swap);

  // Compare container operators
  initContainerCompareModels(ms);
}
