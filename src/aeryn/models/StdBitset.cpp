#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"

#include <sstream>

using namespace llvm;
using namespace moya;

static inline std::string mk(const std::string& name) {
  std::stringstream ss;

  ss << "std::bitset::" << name;

  return ss.str();
}

void Models::initStdBitset(const Module& ms) {
  add(new ModelReadWrite(mk("bitset()"), {0}));
  add(new ModelReadWrite(mk("bitset(unsigned long)"), {0, 1}));
  add(new ModelReadWrite(mk("bitset(unsigned long long)"), {0, 1}));
  add(new ModelReadWrite(mk("bitset(std::basic_string, unsigned long, "
                            "unsigned long, unsigned long)"),
                         {0, 1}));
  add(new ModelReadWrite(mk("bitset(std::basic_string, unsigned long, "
                            "unsigned long, unsigned long, char, char)"),
                         {0, 1}));
  add(new ModelReadWrite(
      mk("bitset(char*, unsigned long, unsigned long, char, char)"), {0, 1}));
  add(new ModelReadOnly(mk("operator==(std::bitset)")));
  add(new ModelReadOnly(mk("operator!=(std::bitset)")));
  add(new ModelReadOnly(mk("operator[](unsigned long)")));
  add(new ModelReadOnly(mk("test(unsigned long)")));
  add(new ModelReadOnly(mk("all()")));
  add(new ModelReadOnly(mk("any()")));
  add(new ModelReadOnly(mk("none()")));
  add(new ModelReadOnly(mk("count()")));
  add(new ModelReadOnly(mk("size()")));
  add(new ModelReadWrite(
      mk("operator&=(std::bitset)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      mk("operator|=(std::bitset)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      mk("operator^=(std::bitset)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(mk("operator~()"), {0, 1}));
  add(new ModelReadWrite(mk("operator<<(unsigned long)"), {0, 1}));
  add(new ModelReadWrite(
      mk("operator<<=(unsigned long)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(mk("operator>>(unsigned long)"), {0, 1}));
  add(new ModelReadWrite(
      mk("operator>>=(unsigned long)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(mk("set()"), {0}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      mk("set(unsigned long, bool)"), {0, 1, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(mk("reset()"), {0}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      mk("reset(unsigned long)"), {0}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(mk("flip()"), {0}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      mk("flip(unsigned long)"), {0}, Model::ReturnSpec::Arg0));
  add(new ModelReadOnly(mk("to_ulong()")));
  add(new ModelReadOnly(mk("to_ullong()")));
}
