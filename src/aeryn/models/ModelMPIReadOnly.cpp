#include "ModelMPIReadOnly.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

using namespace llvm;

namespace moya {

ModelMPIReadOnly::ModelMPIReadOnly(const std::string& fname,
                                   Model::ReturnSpec ret)
    : ModelReadOnly(fname, ret) {
  ;
}

AnalysisStatus ModelMPIReadOnly::process(const Instruction* inst,
                                         const Function* f,
                                         const Vector<Contents>& args,
                                         const Vector<Type*>& argTys,
                                         Environment& env,
                                         Store& store,
                                         const Function* caller) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(unsigned i = 0; i < args.size(); i++)
    if(auto* pty = dyn_cast<PointerType>(argTys.at(i))) {
      if(auto* sty = dyn_cast<StructType>(pty->getElementType())) {
        for(const auto* addr : args.at(i).getAs<Address>(inst))
          if(store.isTypeAt(addr, sty))
            changed |= markRead({addr}, pty, env, store, inst);
      } else {
        changed |= markRead(args.at(i), argTys.at(i), env, store, inst);
      }
    } else {
      changed |= markRead(args.at(i), argTys.at(i), env, store, inst);
    }

  if(not f->getReturnType()->isVoidTy()) {
    switch(ret) {
    case Model::ReturnSpec::Default:
      env.push(store.getRuntimeValue(f->getReturnType()));
      break;
    default:
      env.push(args.at(static_cast<int>(ret)));
      break;
    }
  }

  return changed;
}

} // namespace moya
