#include "ModelSTLMap.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

using namespace llvm;

namespace moya {

ModelSTLMap::ModelSTLMap(const std::string& fname,
                         STDKind cont,
                         FuncID fn,
                         const Models& ms)
    : ModelSTLTree(fname, cont, fn, ms) {
  switch(fn) {
  case AccessElement:
    processFn = static_cast<ExecuteFn>(&ModelSTLMap::modelAccessElement);
    break;
  case EmplaceMapped:
    processFn = static_cast<ExecuteFn>(&ModelSTLMap::modelEmplaceMapped);
    break;
  default:
    // Everything else will have been initialized in the ModelSTLContainer()
    // constructor
    break;
  }
}

ModelSTLMap::ModelSTLMap(const std::string& fname, FuncID fn, const Models& ms)
    : ModelSTLMap(fname, STDKind::STD_STL_map, fn, ms) {
  ;
}

AnalysisStatus ModelSTLMap::modelAccessElement(const Instruction* call,
                                               const Function* f,
                                               const Vector<Contents>& args,
                                               Environment& env,
                                               Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Contents ret;
  unsigned long offsetVal = Metadata::getSTLMappedOffset(f);
  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= markContainerRead(conts, call, f, store);
  for(const Address* node : getNode(conts, call, f, store, changed))
    ret.insert(store.make(node, offsetVal));

  env.push(ret);

  return changed;
}

AnalysisStatus ModelSTLMap::modelEmplaceMapped(const Instruction* call,
                                               const Function* f,
                                               const Vector<Contents>& args,
                                               Environment& env,
                                               Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Contents ret;
  Type* keyTy = Metadata::getSTLKeyType(f);
  uint64_t offsetKey = Metadata::getSTLKeyOffset(f);
  uint64_t offsetVal = Metadata::getSTLMappedOffset(f);
  const Addresses& conts = args.at(0).getAs<Address>(call);
  const Addresses& keys = args.at(1).getAs<Address>(call);

  // Write the keys to the map
  for(const Address* node : getNode(conts, call, f, store, changed)) {
    for(const Address* key : keys)
      store.copy(store.make(node, offsetKey), key, keyTy, call, changed);
    ret.insert(store.make(node, offsetVal));
  }
  // for(const Address* buf : getBuffer(conts, call, f, store, changed))
  //   if(store.empty(buf, mappedTy))
  // moya_warning("Need to implement adding a default element: ModelSTLMap");
  changed |= markContainerWrite(conts, call, f, store);
  changed |= markContainerRead(conts, call, f, store);

  env.push(ret);

  return changed;
}

} // namespace moya
