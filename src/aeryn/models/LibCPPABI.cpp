#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/LLVMUtils.h"

using namespace llvm;
using namespace moya;

static AnalysisStatus modelExcAllocate(const ModelCustom& model,
                                       const Instruction* call,
                                       const Function* f,
                                       const Vector<Contents>& args,
                                       Environment& env,
                                       Store& store,
                                       const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= AbstractUtils::malloc(call, env, store, caller);

  return changed;
}

void Models::initLibCppABI(const Module& module) {
  add(new ModelCustom("__cxa_allocate_exception", modelExcAllocate));
  add(new ModelReadWrite("__cxa_free_exception", {0}, true));

  add(new ModelCustom("__cxa_allocate_dependent_exception", modelExcAllocate));
  add(new ModelReadWrite("__cxa_free_dependent_exception", {0}));

  add(new ModelReadWrite(
      "__cxa_get_exception_ptr", {0}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      "__cxa_begin_catch", {0}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadOnly("__cxa_end_catch"));

  add(new ModelReadWrite("__cxa_throw", {0, 1, 1}, true));
  add(new ModelReadOnly("__cxa_current_exception_type"));
  add(new ModelReadOnly("__cxa_rethrow"));
  add(new ModelReadOnly("__cxa_current_primary_exception"));
  add(new ModelReadWrite("__cxa_decrement_exception_refcount", {0}));

  // FIXME: Handle get_globals
  // if(StructType *ehGlobals = module.getTypeByName("__cxa_eh_globals"))) {
  //   PointerTI *pehGlobals = tf.createPointer(ehGlobals);
  //   add(new ModelReadOnly("__cxa_get_globals", pehGlobals->clone(), {}));
  //   add(new ModelReadOnly("__cxa_get_globals_fast", pehGlobals->clone(),
  //   {}));
  // }
  add(new ModelReadWrite("__cxa_increment_exception_refcount", {0}));
  add(new ModelReadWrite("__cxa_rethrow_primary_exception", {0}));
  add(new ModelReadOnly("__cxa_uncaught_exception"));

  // FIXME: Add __gxx_personality_v0

  add(new ModelReadWrite("__cxa_guard_acquire", {0}));
  add(new ModelReadWrite("__cxa_guard_release", {0}));
  add(new ModelReadWrite("__cxa_guard_abort", {0}));

  // FIXME: Handle vector construction and destruction

  add(new ModelReadOnly("__cxa_bad_cast"));
  add(new ModelReadOnly("__cxa_bad_typeid"));
  add(new ModelReadOnly("__cxa_pure_virtual"));
  add(new ModelReadWrite("__cxa_call_unexpected", {0}));
  add(new ModelReadWrite("__cxa_demangle", {1, 0, 0, 0}));

  add(new ModelReadOnly("__cxa_atexit"));

  // FIXME: Handle __dynamic_cast
}
