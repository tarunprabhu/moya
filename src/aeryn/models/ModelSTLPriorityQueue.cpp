#include "ModelSTLPriorityQueue.h"

using namespace llvm;

namespace moya {

// The vectors for this version of Clang/libstdc++ is at it's heart a
// struct whose elements are:
//
// struct priority_queue = { T*, T*, T*, i8 }
//
// When a constructor is called, a single cell is allocated for the
// underlying type. This will be the buffer into which all the contents of the
// vector will be stored.
//
// All three pointers will point to this buffer
//
// All the pointers will move in lockstep - so when one is read, all three will
// be read and when one is modified all three will be modified.
//
// All iterators will return a pointer to the underlying buffer.
//
// It is essentially implemented with a std::vector under the hood
//

ModelSTLPriorityQueue::ModelSTLPriorityQueue(const std::string& fname,
                                             FuncID fn,
                                             const Models& ms)
    : ModelSTLVector(fname,
                     STDKind::STD_STL_priority_queue,
                     STDKind::STD_None,
                     STDKind::STD_None,
                     fn,
                     ms) {
  ;
}

} // namespace moya
