#include "ModelReadWrite.h"
#include "ModelSTLForwardList.h"
#include "Models.h"
#include "common/LangConfig.h"

#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;
using namespace moya;

static std::string getIteratorName() {
  return STDConfig::getSTDName(STDKind::STD_STL_iterator_forward_list);
}

static std::string mkIter(std::string name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getIteratorName() << "::" << name;
  return ss.str();
}

static std::string mkCompare(StringRef op) {
  std::string s;
  raw_string_ostream ss(s);
  std::string iterator = getIteratorName();
  ss << iterator << "::operator" << op << "(" << iterator << ")";
  return ss.str();
}

static std::string mkStdCompare(StringRef op) {
  std::string s;
  raw_string_ostream ss(s);
  std::string iterator = getIteratorName();
  ss << "std::operator" << op << "(" << iterator << ", " << iterator << ")";
  return ss.str();
}

static void
mkModel(Models& ms, std::string name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLForwardList(name, fn, ms));
}

void Models::initSTLForwardListIterator(const Module& module) {
  Models& ms = *this;

  add(new ModelReadWrite(mkIter("_Fwd_list_iterator(*)"), {0}, true));

  // Add the iterator comparison operator overload models
  for(StringRef op : {"==", "!="}) {
    mkModel(ms, mkCompare(op), ModelSTLForwardList::IteratorCompare);
    mkModel(ms, mkStdCompare(op), ModelSTLForwardList::IteratorCompare);
  }

  // Operations on iterators
  mkModel(ms, mkIter("operator*()"), ModelSTLForwardList::IteratorDeref);
  mkModel(ms, mkIter("operator->()"), ModelSTLForwardList::IteratorDeref);
  mkModel(ms, mkIter("operator++()"), ModelSTLForwardList::IteratorSelf);
  mkModel(ms, mkIter("operator++(int)"), ModelSTLForwardList::IteratorNew);
}
