#include "ModelSTLVector.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

using namespace llvm;

namespace moya {

// The vectors for this version of Clang/libstdc++ is at it's heart a
// struct whose elements are:
//
// struct vector = { T*, T*, T* }
//
// When a vector's constructor is called, a single cell is allocated for the
// underlying type. This will be the buffer into which all the contents of the
// vector will be stored.
//
// All three pointers will point to this buffer
//
// All the pointers move in lockstep. When one is read, all are read. When one
// is modified, all are modified.
//
// All iterators will return a pointer to the data buffer.
//
// Resize operations will have no effect other than marking all the pointers
// and the data buffer as having been modified
//

ModelSTLVector::ModelSTLVector(const std::string& fname,
                               STDKind cont,
                               STDKind iter,
                               STDKind citer,
                               FuncID fn,
                               const Models& ms)
    : ModelSTLContainer(fname, cont, iter, citer, fn, ms) {
  switch(fn) {
  case ModelSTLVector::Begin:
    processFn = static_cast<ExecuteFn>(&ModelSTLVector::modelBegin);
    break;
  case ModelSTLVector::End:
    processFn = static_cast<ExecuteFn>(&ModelSTLVector::modelEnd);
    break;
  case ModelSTLVector::IteratorBase:
    processFn = static_cast<ExecuteFn>(&ModelSTLVector::modelIteratorBase);
    break;
  case ModelSTLVector::InsertRange:
    processFn = static_cast<ExecuteFn>(&ModelSTLVector::modelInsertRange);
    break;
  default:
    // Everything else will have been initialized in the ModelSTLContainer()
    // constructor
    break;
  }
}

ModelSTLVector::ModelSTLVector(const std::string& fname,
                               FuncID fn,
                               const Models& ms)
    : ModelSTLVector(fname,
                     STDKind::STD_STL_vector,
                     STDKind::STD_STL_iterator_normal,
                     STDKind::STD_STL_iterator_normal,
                     fn,
                     ms) {
  ;
}

Addresses ModelSTLVector::getIteratorNode(const Addresses& iters,
                                          const Instruction* call,
                                          const Function* f,
                                          Store& store,
                                          AnalysisStatus& changed) const {
  PointerType* pBufTy = dyn_cast<PointerType>(Metadata::getSTLValueType(f));

  return store.readAs<Address>(iters, pBufTy, call, changed);
}

Addresses ModelSTLVector::getIteratorDeref(const Addresses& iters,
                                           const Instruction* call,
                                           const Function* f,
                                           Store& store,
                                           AnalysisStatus& changed) const {
  PointerType* pBufTy = Metadata::getSTLValueType(f)->getPointerTo();

  return store.readAs<Address>(iters, pBufTy, call, changed);
}

AnalysisStatus ModelSTLVector::updateContainerPointers(const Address* base,
                                                       const Addresses& vals,
                                                       const Instruction* call,
                                                       const Function* f,
                                                       Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* pBufTy = Metadata::getSTLValueType(f)->getPointerTo();
  int64_t vtadj = getContainerVtableOffset(base, call, f, store);
  for(unsigned offset : {0, 8, 16})
    for(const Content* c : vals)
      store.write(store.make(base, vtadj + offset), c, pBufTy, call, changed);

  return changed;
}

AnalysisStatus ModelSTLVector::modelBegin(const Instruction* call,
                                          const Function* f,
                                          const Vector<Contents>& args,
                                          Environment& env,
                                          Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents ret = getBeginIterator(conts, call, f, store, changed);
  Type* pi8 = Type::getInt8PtrTy(call->getContext());

  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, ret, pi8, call, changed);
  else
    env.push(ret);

  return changed;
}

AnalysisStatus ModelSTLVector::modelEnd(const Instruction* call,
                                        const Function* f,
                                        const Vector<Contents>& args,
                                        Environment& env,
                                        Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents ret = getEndIterator(conts, call, f, store, changed);
  Type* pi8 = Type::getInt8PtrTy(call->getContext());

  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, ret, pi8, call, changed);
  else
    env.push(ret);

  return changed;
}

AnalysisStatus ModelSTLVector::modelIteratorBase(const Instruction* call,
                                                 const Function* f,
                                                 const Vector<Contents>& args,
                                                 Environment& env,
                                                 Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& iters = args.at(0).getAs<Address>(call);

  env.push(iters);

  return changed;
}

AnalysisStatus ModelSTLVector::modelInsertRange(const Instruction* call,
                                                const Function* f,
                                                const Vector<Contents>& args,
                                                Environment& env,
                                                Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  unsigned long offset = Metadata::getSTLIteratorOffset(f);
  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  const Addresses& iters = args.at(hasStructRet + 1).getAs<Address>(call);
  const Addresses& addrs1 = args.at(hasStructRet + 2).getAs<Address>(call);
  const Addresses& addrs2 = args.at(hasStructRet + 3).getAs<Address>(call);

  changed |= markContainerWrite(conts, call, f, store);
  changed |= writeToContainer(conts, addrs1, offset, call, f, store);
  changed |= writeToContainer(conts, addrs2, offset, call, f, store);

  // Prior to C++11, this function does not return a value
  if(LLVMUtils::hasReturn(f))
    env.push(iters);

  return changed;
}

} // namespace moya
