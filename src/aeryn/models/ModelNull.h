#ifndef MOYA_MODELS_MODEL_NULL_H
#define MOYA_MODELS_MODEL_NULL_H

#include "Model.h"

namespace moya {

class ModelNull : public Model {
public:
  ModelNull(const std::string&);

  virtual ~ModelNull() = default;

  virtual AnalysisStatus process(const llvm::Instruction*,
                                 const llvm::Function*,
                                 const Vector<Contents>&,
                                 const Vector<llvm::Type*>&,
                                 Environment&,
                                 Store&,
                                 const llvm::Function*) const;

public:
  static bool classof(const Model*);
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_NULL_H
