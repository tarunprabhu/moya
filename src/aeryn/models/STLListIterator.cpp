#include "ModelReadWrite.h"
#include "ModelSTLList.h"
#include "Models.h"
#include "common/LangConfig.h"

#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;
using namespace moya;

static std::string getIteratorName() {
  return STDConfig::getSTDName(STDKind::STD_STL_iterator_list);
}

static std::string getConstIteratorName() {
  return STDConfig::getSTDName(STDKind::STD_STL_const_iterator_list);
}

static std::string mkIter(const std::string& name, bool cnst) {
  std::string buf;
  raw_string_ostream ss(buf);
  std::string iterator = cnst ? getConstIteratorName() : getIteratorName();

  ss << iterator << "::" << name;

  return ss.str();
}

static std::string mkCompare(const std::string& op, bool cnst) {
  std::string buf;
  raw_string_ostream ss(buf);
  std::string iterator = cnst ? getConstIteratorName() : getIteratorName();

  ss << iterator << "::operator" << op << "(iterator)";

  return ss.str();
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLList(name, fn, ms));
}

void Models::initSTLListIterator(const Module& module) {
  Models& ms = *this;

  add(new ModelReadWrite(mkIter("_List_iterator()", false), {0}, true));
  add(new ModelReadWrite(mkIter("_List_iterator(iterator)", false), {0}, true));
  add(new ModelReadWrite(mkIter("_List_const_iterator()", true), {0}, true));
  add(new ModelReadWrite(
      mkIter("_List_const_iterator(iterator)", true), {0}, true));

  // Add the iterator comparison operator overload models
  for(const std::string& op : {"==", "!="})
    for(bool cnst : {false, true})
      mkModel(ms, mkCompare(op, cnst), ModelSTLList::IteratorCompare);

  // Operations on iterators
  for(bool cnst : {false, true}) {
    mkModel(ms, mkIter("operator*()", cnst), ModelSTLList::IteratorDeref);
    mkModel(ms, mkIter("operator->()", cnst), ModelSTLList::IteratorDeref);
    mkModel(ms, mkIter("operator++()", cnst), ModelSTLList::IteratorSelf);
    mkModel(ms, mkIter("operator++(int)", cnst), ModelSTLList::IteratorNew);
    mkModel(ms, mkIter("operator--()", cnst), ModelSTLList::IteratorSelf);
    mkModel(ms, mkIter("operator--(int)", cnst), ModelSTLList::IteratorNew);
  }
}
