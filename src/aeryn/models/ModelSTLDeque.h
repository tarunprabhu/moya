#ifndef MOYA_MODELS_MODEL_STL_DEQUE_H
#define MOYA_MODELS_MODEL_STL_DEQUE_H

#include "ModelSTLContainer.h"

namespace moya {

class ModelSTLDeque : public ModelSTLContainer {
protected:
  virtual AnalysisStatus updateContainerPointers(const Address*,
                                                 const Addresses&,
                                                 const llvm::Instruction*,
                                                 const llvm::Function*,
                                                 Store&) const;

  virtual Contents getBeginIterator(const Addresses&,
                                    const llvm::Instruction*,
                                    const llvm::Function*,
                                    Store&,
                                    AnalysisStatus&) const;
  virtual Contents getEndIterator(const Addresses&,
                                  const llvm::Instruction*,
                                  const llvm::Function*,
                                  Store&,
                                  AnalysisStatus&) const;

  virtual IteratorFn getIteratorNode;
  virtual IteratorFn getIteratorDeref;
  virtual IteratorFn getIteratorNew;

  virtual ModelFn modelInsertRange;

protected:
  ModelSTLDeque(const std::string&,
                STDKind,
                STDKind,
                STDKind,
                FuncID,
                const Models&);

public:
  ModelSTLDeque(const std::string&, STDKind, FuncID, const Models&);
  ModelSTLDeque(const std::string&, FuncID, const Models&);

  virtual ~ModelSTLDeque() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_DEQUE_H
