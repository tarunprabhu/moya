#ifndef MOYA_MODELS_MODEL_STL_ARRAY_H
#define MOYA_MODELS_MODEL_STL_ARRAY_H

#include "ModelSTLContainer.h"

namespace moya {

class ModelSTLArray : public ModelSTLContainer {
protected:
  virtual Addresses getBuffer(const Addresses&,
                              const llvm::Instruction*,
                              const llvm::Function*,
                              Store&,
                              AnalysisStatus&) const;
  virtual Addresses getNode(const Addresses&,
                            const llvm::Instruction*,
                            const llvm::Function*,
                            Store&,
                            AnalysisStatus&) const;

  virtual ModelFn modelDestructor;
  virtual ModelFn modelBegin;
  virtual ModelFn modelEnd;

protected:
  ModelSTLArray(const std::string&,
                STDKind,
                STDKind,
                STDKind,
                FuncID,
                const Models&);

public:
  ModelSTLArray(const std::string&, FuncID, const Models&);
  virtual ~ModelSTLArray() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_ARRAY_H
