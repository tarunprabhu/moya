#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"

using namespace llvm;
using namespace moya;

void Models::initLibOpenMPI(const Module& module) {
  add(new ModelReadOnly("ompi_op_set_cxx_callback"));
}
