#ifndef MOYA_MODELS_MODEL_STL_MULTIMAP_H
#define MOYA_MODELS_MODEL_STL_MULTIMAP_H

#include "ModelSTLMap.h"

namespace moya {

class ModelSTLMultimap : public ModelSTLMap {
public:
  ModelSTLMultimap(const std::string&, FuncID, const Models&);
  virtual ~ModelSTLMultimap() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_MULTIMAP_H
