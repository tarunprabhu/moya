#include "ModelSTLSet.h"

using namespace llvm;

namespace moya {

ModelSTLSet::ModelSTLSet(const std::string& fname,
                         STDKind cont,
                         FuncID fn,
                         const Models& ms)
    : ModelSTLTree(fname, cont, fn, ms) {
  ;
}

ModelSTLSet::ModelSTLSet(const std::string& fname, FuncID fn, const Models& ms)
    : ModelSTLSet(fname, STDKind::STD_STL_set, fn, ms) {
  ;
}

} // namespace moya
