#include "ModelSTLPriorityQueue.h"
#include "Models.h"
#include "common/LangConfig.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;
using namespace moya;

static std::string mk(const std::string& name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << ModelSTLPriorityQueue::getContainerName() << "::" << name;
  return ss.str();
}

static void
mk_model(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLPriorityQueue(name, fn, ms));
}

void Models::init_stl_priority_queue(const Module& module) {
  Models& ms = *this;

  // Dummy
  mk_model(ms, mk("moya.dummy"), nullptr);

  // Constructors
  mk_model(ms, mk("priority_queue()"), ModelSTLPriorityQueue::ConstructorEmpty);
  mk_model(ms,
           mk("priority_queue(std::priority_queue)"),
           ModelSTLPriorityQueue::ConstructorCopy);

  // Destructors
  mk_model(ms, mk("~priority_queue()"), ModelSTLPriorityQueueDestructor);

  // Element access
  mk_model(ms, mk("front()"), ModelSTLPriorityQueue::AccessElement);
  mk_model(ms, mk("back()"), ModelSTLPriorityQueue::AccessElement);

  // Capacity
  mk_model(ms, mk("empty()"), ModelSTLPriorityQueue::Size);
  mk_model(ms, mk("size()"), ModelSTLPriorityQueue::Size);

  // Modifiers
  mk_model(ms, mk("push(*)"), ModelSTLPriorityQueue::AddElement);
  mk_model(ms, mk("pop()"), ModelSTLPriorityQueue::RemoveElement);
  mk_model(ms, mk("swap(std::priority_queue)"), ModelSTLPriorityQueue::Swap);
}
