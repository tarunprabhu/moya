#include "ModelNull.h"
#include "Models.h"
#include "common/Config.h"
#include "common/Vector.h"

#include <llvm/IR/Module.h>

using namespace llvm;
using namespace moya;

void Models::initLibMoya(const Module& module) {
  add(new ModelNull(Config::getEnterRegionFunctionName()), false);
  add(new ModelNull(Config::getExitRegionFunctionName()), false);
  // add(new ModelNull(Config::getStatsStartCallFunctionName()), false);
  // add(new ModelNull(Config::getStatsStopCallFunctionName()), false);
  // add(new ModelNull(Config::getStatsStartCompileFunctionName()), false);
  // add(new ModelNull(Config::getStatsStopCompileFunctionName()), false);
  add(new ModelNull(Config::getStatsStartExecuteFunctionName()), false);
  add(new ModelNull(Config::getStatsStopExecuteFunctionName()), false);
}
