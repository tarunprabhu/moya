#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;
using namespace moya;

static inline std::string mk(const std::string& base, const std::string& name) {
  std::stringstream ss;

  ss << "std::" << base << "::" << name;

  return ss.str();
}

static inline std::string mkbos(const std::string& name) {
  return mk("basic_ostream", name);
}

static inline std::string mkofs(const std::string& name) {
  return mk("basic_ofstream", name);
}

static AnalysisStatus modelOperator(const ModelCustom& model,
                                    const Instruction* call,
                                    const Function* f,
                                    const Vector<Contents>& args,
                                    Environment& env,
                                    Store& store,
                                    const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(unsigned i = 1; i < args.size(); i++)
    changed |= model.markRead(
        args.at(i), LLVMUtils::getArgumentType(f, i), env, store, call);

  // This is a virtually inherited class of some form. The last element of
  // the class will be an instance of std::basic_ios. We need to iterate over
  // all the elements in the object until we get to that instance and mark
  // everything as being modified along the way. This is necessarily
  // conservative but there is no reasonable way to handle it otherwise.
  //
  // Only one virtual base class is allowed, std::basic_ios. Whatever the
  // argument to this function, it is guaranteed to be a descendant of this
  // class
  const Module& module = *LLVMUtils::getModule(call);
  auto* sty = dyn_cast<StructType>(
      LLVMUtils::getInnermost(LLVMUtils::getArgumentType(f, 0)));
  StructType* vbase = moya::Metadata::getVirtualBases(sty, module).at(0);

  for(const auto* addr : args.at(0).getAs<Address>(call)) {
    unsigned long offset = 0;
    const auto* next = addr;
    do {
      Type* pty = store.getTypesAt(next).at(0)->getPointerTo();
      changed |= model.markWrite(next, pty, env, store, call);
      for(offset++; not store.contains(addr, offset); offset++)
        ;
      next = store.make(addr, offset);
    } while(not(store.isStructAt(next, vbase)));
    changed |= model.markWrite(next, vbase->getPointerTo(), env, store, call);
  }

  env.push(args.at(0));

  return changed;
}

void Models::initStdOstream(const Module& module) {
  // basic_ostream models
  add(new ModelCustom(
      "std::operator<<(std::basic_ostream, *)", modelOperator, true, false));
  add(new ModelReadWrite(
      mkbos("basic_ostream(std::basic_streambuf*)"), {0, 1}, true));
  add(new ModelReadWrite(mkbos("~basic_ostream()"), {0, 1}, true));
  add(new ModelCustom(mkbos("operator<<(*)"), modelOperator, true, false));
  add(new ModelReadWrite(
      mkbos("put(char)"), {0, 1}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(mkbos("write(char*, unsigned long)"),
                         {0, 1, 1},
                         Model::ReturnSpec::Arg0,
                         true));
  add(new ModelReadOnly(mkbos("tellp()")));
  add(new ModelReadWrite(
      mkbos("seekp(std::pos)"), {0, 1}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(
      mkbos("seekp(unsigned long)"), {0, 1}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(mkbos("seekp(unsigned long, int)"),
                         {0, 1, 1},
                         Model::ReturnSpec::Arg0,
                         true));
  add(new ModelReadWrite(mkbos("flush()"), {0}, Model::ReturnSpec::Arg0, true));

  // ofstream models
  add(new ModelReadWrite(mkofs("basic_ofstream()"), {0}, true));
  add(new ModelReadWrite(
      mkofs("basic_ofstream(char*, std::_Ios_Openmode)"), {0, 1, 1}, true));
  add(new ModelReadWrite(
      mkofs("basic_ofstream(std::basic_string, std::_Ios_Openmode"),
      {0, 1, 1},
      true));
  add(new ModelReadWrite(
      mkofs("basic_ofstream(std::basic_ofstream"), {0, 1}, true));
  add(new ModelReadWrite(mkofs("~basic_ofstream()"), {0, 1}, true));
  add(new ModelReadWrite(mkofs("swap(std::basic_ofstream"), {0, 0}, true));
  add(new ModelReadOnly(mkofs("is_open()")));
  add(new ModelReadWrite(
      mkofs("open(char*, std::_Ios_Openmode)"), {0, 1, 1}, true));
  add(new ModelReadWrite(
      mkofs("open(std::basic_string, std::_Ios_Openmode)"), {0, 1, 1}, true));
  add(new ModelReadWrite(mkofs("close()"), {0}, true));
  add(new ModelReadWrite(mkofs("rdbuf()"), {0}, true));
}
