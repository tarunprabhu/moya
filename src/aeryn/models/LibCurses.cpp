#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"

using namespace llvm;
using namespace moya;

static void mkNo(Models& ms, const std::string& name) {
  ms.add(new ModelReadOnly(name));
  ms.add(new ModelReadOnly("no" + name));
}

static void mkW(Models& ms,
                const std::string& name,
                const std::initializer_list<int>& args) {
  ms.add(new ModelReadWrite(name, args));
  Vector<Model::ArgSpec> wArgs = {Model::ArgSpec::ReadWrite};
  for(int s : args)
    wArgs.push_back(static_cast<Model::ArgSpec>(s));
  ms.add(new ModelReadWrite("w" + name, wArgs));
}

static void mkMVW(Models& ms,
                  const std::string& name,
                  const std::initializer_list<int>& args) {
  ms.add(new ModelReadWrite(name, args));
  Vector<Model::ArgSpec> wArgs = {Model::ArgSpec::ReadWrite};
  for(int s : args)
    wArgs.push_back(static_cast<Model::ArgSpec>(s));
  ms.add(new ModelReadWrite("mvw" + name.substr(2), wArgs));
}

void Models::initLibCurses(const Module& module) {
  Models& ms = *this;

  add(new ModelReadOnly("initscr", Model::ReturnSpec::Allocate));
  add(new ModelReadOnly("endwin"));
  add(new ModelReadOnly("isendwin"));
  add(new ModelReadOnly("newterm", Model::ReturnSpec::Allocate));
  // This should return the previous screen, but for simplicity, we just return
  // the current screen.
  add(new ModelReadOnly("set_term", Model::ReturnSpec::Arg0));
  add(new ModelReadWrite("delscreen", {-1}));
  mkNo(ms, "cbreak");
  mkNo(ms, "echo");
  mkNo(ms, "raw");
  mkNo(ms, "qiflush");
  add(new ModelReadOnly("halfdelay"));
  add(new ModelReadWrite("intrflush", {0, 1}));
  add(new ModelReadWrite("keypad", {0, 1}));
  add(new ModelReadWrite("meta", {0, 1}));
  add(new ModelReadWrite("nodelay", {0, 1}));
  add(new ModelReadOnly("timeout"));
  add(new ModelReadWrite("notimeout", {0, 1}));
  add(new ModelReadWrite("wtimeout", {0, 1}));
  add(new ModelReadOnly("typeahead"));
  mkW(ms, "erase", {});
  mkW(ms, "clear", {});
  mkW(ms, "clrtobot", {});
  mkW(ms, "clrtoeol", {});
  add(new ModelReadOnly("newwin", Model::ReturnSpec::Allocate));
  add(new ModelReadWrite("delwin", {Model::ArgSpec::Deallocate}));
  add(new ModelReadWrite("mvwin", {0, 1, 1}));
  add(new ModelReadWrite("subwin", {0, 1, 1, 1, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite("derwin", {0, 1, 1, 1, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite("mvderwin", {0, 1, 1}));
  add(new ModelReadWrite("dupwin", {0}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite("wsyncup", {0}));
  add(new ModelReadWrite("syncok", {0}));
  add(new ModelReadWrite("wcursyncup", {0}));
  add(new ModelReadWrite("wsyncdown", {0}));
  add(new ModelReadOnly("start_color"));
  add(new ModelReadOnly("init_pair"));
  add(new ModelReadOnly("init_color"));
  add(new ModelReadOnly("has_colors"));
  add(new ModelReadOnly("can_change_color"));
  add(new ModelReadWrite("color_content", {1, 0, 0, 0}));
  add(new ModelReadWrite("pair_content", {1, 0, 0}));
  mkW(ms, "printw", {});
  mkMVW(ms, "mvprintw", {0});
  add(new ModelReadWrite("vwprintw", {0, 1, 0}));
  add(new ModelReadWrite("vw_printw", {0, 1, 0}));
  mkW(ms, "scanw", {0});
  mkMVW(ms, "mvscanw", {1, 1, 0});
  add(new ModelReadWrite("vwscanw", {0, 0, 0}));
  add(new ModelReadWrite("vw_scanw", {0, 0, 0}));
  mkW(ms, "move", {1, 1});
  mkW(ms, "addch", {1});
  mkW(ms, "echochar", {1});
  mkW(ms, "addchstr", {1});
  mkW(ms, "addchnstr", {1, 1});
  mkMVW(ms, "mvaddchstr", {1, 1, 1});
  mkMVW(ms, "mvaddchnstr", {1, 1, 1, 1});
  mkW(ms, "getch", {1});
  mkMVW(ms, "mvgetch", {1, 1});
  add(new ModelReadOnly("ungetch"));
  add(new ModelReadOnly("has_key"));
  mkW(ms, "border", {1, 1, 1, 1, 1, 1, 1, 1});
  mkW(ms, "hline", {1, 1});
  mkW(ms, "vline", {1, 1});
  mkMVW(ms, "mvhline", {1, 1, 1, 1});
  mkMVW(ms, "mvvline", {1, 1, 1, 1});
  add(new ModelReadWrite("box", {0, 1, 1}));
  mkW(ms, "refresh", {});
  add(new ModelReadWrite("wnoutrefresh", {0}));
  add(new ModelReadOnly("doupdate"));
  add(new ModelReadWrite("redrawwin", {0}));
  add(new ModelReadWrite("wredrawwin", {0, 1, 1}));
  mkW(ms, "attr_get", {0, 1, 1});
  mkW(ms, "attr_set", {0, 1, 1});
  mkW(ms, "attr_off", {1, 1});
  mkW(ms, "attr_on", {1, 1});
  mkW(ms, "attron", {1});
  mkW(ms, "attroff", {1});
  mkW(ms, "attrset", {1});
  mkW(ms, "chgat", {1, 1, 1, 1});
  mkMVW(ms, "mvchgat", {1, 1, 1, 1, 1, 1});
  mkW(ms, "color_set", {1, 1});
  mkW(ms, "standend", {});
  mkW(ms, "standout", {});
}
