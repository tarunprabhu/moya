#include "ModelSTLMultimap.h"
#include "Models.h"
#include "common/LangConfig.h"

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_multimap);
}

static std::string mk(std::string name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "std::operator" << op << "(" << getContainerName() << ", "
       << getContainerName() << ")";
    ms.add(new ModelSTLMultimap(ss.str(), ModelSTLMultimap::Compare, ms));
  }
}

static void
mkModel(Models& ms, std::string name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLMultimap(name, fn, ms));
}

void Models::initSTLMultimap(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms, mk("multimap()"), ModelSTLMultimap::ConstructorEmpty);
  mkModel(ms,
          mk("multimap(std::less, allocator)"),
          ModelSTLMultimap::ConstructorEmpty);
  mkModel(ms, mk("multimap(T, T)"), ModelSTLMultimap::ConstructorRange);
  mkModel(
      ms, mk("multimap(T, T, allocator)"), ModelSTLMultimap::ConstructorRange);
  mkModel(ms,
          mk("multimap(T, T, std::less, allocator)"),
          ModelSTLMultimap::ConstructorRange);
  mkModel(ms, mk("multimap(std::multimap)"), ModelSTLMultimap::ConstructorCopy);
  mkModel(ms,
          mk("multimap(std::initializer_list, std::less, allocator)"),
          ModelSTLMultimap::ConstructorInitList);

  // Destructor
  mkModel(ms, mk("~multimap()"), ModelSTLMultimap::Destructor);

  // operator =
  mkModel(
      ms, mk("operator=(std::multimap)"), ModelSTLMultimap::OperatorContainer);
  mkModel(ms,
          mk("operator=(std::initializer_list)"),
          ModelSTLMultimap::OperatorInitList);

  // allocator
  mkModel(ms, mk("get_allocator()"), ModelSTLMultimap::GetAllocator);

  // Iterators
  mkModel(ms, mk("begin()"), ModelSTLMultimap::Begin);
  mkModel(ms, mk("cbegin()"), ModelSTLMultimap::Begin);
  mkModel(ms, mk("end()"), ModelSTLMultimap::End);
  mkModel(ms, mk("cend()"), ModelSTLMultimap::End);
  mkModel(ms, mk("rbegin()"), ModelSTLMultimap::Begin);
  mkModel(ms, mk("crbegin()"), ModelSTLMultimap::Begin);
  mkModel(ms, mk("rend()"), ModelSTLMultimap::End);
  mkModel(ms, mk("crend()"), ModelSTLMultimap::End);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLMultimap::Size);
  mkModel(ms, mk("size()"), ModelSTLMultimap::Size);
  mkModel(ms, mk("max_size()"), ModelSTLMultimap::Size);

  // Modifiers
  mkModel(ms, mk("clear()"), ModelSTLMultimap::Clear);

  mkModel(ms, mk("emplace(...)"), ModelSTLMultimap::EmplaceElement);
  mkModel(ms,
          mk("emplace_hint(iterator, ...)"),
          ModelSTLMultimap::EmplaceElementAt);

  mkModel(ms, mk("insert(std::pair)"), ModelSTLMultimap::InsertElement);
  mkModel(ms, mk("insert(T)"), ModelSTLMultimap::InsertElement);
  mkModel(ms, mk("insert(iterator, *)"), ModelSTLMultimap::InsertElementAt);
  mkModel(ms, mk("insert(T, T)"), ModelSTLMultimap::InsertRange);
  mkModel(ms,
          mk("insert(std::initializer_list)"),
          ModelSTLMultimap::InsertInitList);

  mkModel(ms, mk("erase(*)"), ModelSTLMultimap::EraseElement);
  mkModel(ms, mk("erase(iterator)"), ModelSTLMultimap::EraseElementAt);
  mkModel(ms, mk("erase(iterator, iterator)"), ModelSTLMultimap::EraseRange);

  mkModel(ms, mk("swap(std::multimap)"), ModelSTLMultimap::Swap);

  // Lookup
  mkModel(ms, mk("count(*)"), ModelSTLMultimap::Size);
  mkModel(ms, mk("find(*)"), ModelSTLMultimap::Find);
  mkModel(ms, mk("equal_range(*)"), ModelSTLMultimap::FindRange);
  mkModel(ms, mk("lower_bound(*)"), ModelSTLMultimap::FindRange);
  mkModel(ms, mk("upper_bound(*)"), ModelSTLMultimap::FindRange);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);
}
