#include "ModelNull.h"

using namespace llvm;

namespace moya {

ModelNull::ModelNull(const std::string& fname)
    : Model(fname, Model::Kind::NullKind, Model::ReturnSpec::Default) {
  ;
}

AnalysisStatus ModelNull::process(const Instruction*,
                                  const Function* f,
                                  const Vector<Contents>&,
                                  const Vector<Type*>&,
                                  Environment& env,
                                  Store&,
                                  const Function*) const {
  if(not f->getReturnType()->isVoidTy())
    env.push(Contents());
  return AnalysisStatus::Unchanged;
}

bool ModelNull::classof(const Model* model) {
  return model->getKind() == Model::Kind::NullKind;
}

} // namespace moya
