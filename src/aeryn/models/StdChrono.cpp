#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"

#include <sstream>

using namespace llvm;
using namespace moya;

static std::string mk(const std::string& f) {
  std::stringstream ss;

  ss << "std::chrono::" << f;

  return ss.str();
}

static std::string mkDur(const std::string& f) {
  std::stringstream ss;

  ss << "std::chrono::duration::" << f;

  return ss.str();
}

static std::string mkTP(const std::string& f) {
  std::stringstream ss;

  ss << "std::chrono::time_point::" << f;

  return ss.str();
}

static std::string mkSysClk(const std::string& f) {
  std::stringstream ss;

  ss << "std::chrono::system_clock::" << f;

  return ss.str();
}

static std::string mkSteadyClk(const std::string& f) {
  std::stringstream ss;

  ss << "std::chrono::steady_clock::" << f;

  return ss.str();
}

static std::string mkHRTClk(const std::string& f) {
  std::stringstream ss;

  ss << "std::chrono::high_resolution_clock::" << f;

  return ss.str();
}

void Models::initStdChrono(const Module& module) {
  // Duration
  add(new ModelReadWrite(mkDur("duration()"), {0}));
  add(new ModelReadWrite(mkDur("duration(std::chrono::duration)"), {0, 1}));
  add(new ModelReadWrite(mkDur("operator=(std::chrono::duration)"),
                         {0, 1},
                         Model::ReturnSpec::Arg0,
                         false));
  add(new ModelReadOnly(mkDur("count()")));
  add(new ModelReadOnly(mkDur("zero()")));
  add(new ModelReadOnly(mkDur("min()")));
  add(new ModelReadOnly(mkDur("max()")));
  add(new ModelReadWrite(mkDur("operator++()"), {0}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      mkDur("operator++(int)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(mkDur("operator--()"), {0}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      mkDur("operator--(int)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(mkDur("operator+=(std::chrono::duration)"),
                         {0, 1},
                         Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(mkDur("operator-=(std::chrono::duration)"),
                         {0, 1},
                         Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(mkDur("operator*=(std::chrono::duration)"),
                         {0, 1},
                         Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(mkDur("operator/=(std::chrono::duration)"),
                         {0, 1},
                         Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(mkDur("operator%=(std::chrono::duration)"),
                         {0, 1},
                         Model::ReturnSpec::Arg0));
  add(new ModelReadOnly(
      "operator==(std::chrono::duration, std::chrono::duration)"));
  add(new ModelReadOnly(
      "operator!=(std::chrono::duration, std::chrono::duration)"));
  add(new ModelReadOnly(
      "operator>(std::chrono::duration, std::chrono::duration)"));
  add(new ModelReadOnly(
      "operator>=(std::chrono::duration, std::chrono::duration)"));
  add(new ModelReadOnly(
      "operator<(std::chrono::duration, std::chrono::duration)"));
  add(new ModelReadOnly(
      "operator<=(std::chrono::duration, std::chrono::duration)"));
  add(new ModelReadOnly(mk("duration_cast(std::chrono::duration)")));
  add(new ModelReadOnly(mk("floor(std::chrono::duration)")));
  add(new ModelReadOnly(mk("ceil(std::chrono::duration)")));
  add(new ModelReadOnly(mk("round(std::chrono::duration)")));
  add(new ModelReadOnly(mk("abs(std::chrono::duration)")));

  // Time point
  add(new ModelReadWrite(mkTP("time_point()"), {0}));
  add(new ModelReadWrite(mkTP("time_point(std::chrono::duration)"), {0, 1}));
  add(new ModelReadWrite(mkTP("operator+=(std::chrono::duration)"),
                         {0, 1},
                         Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(mkTP("operator-=(std::chrono::duration)"),
                         {0, 1},
                         Model::ReturnSpec::Arg0));
  add(new ModelReadOnly(
      mkTP("operator==(std::chrono::time_point, std::chrono::time_point)")));
  add(new ModelReadOnly(
      mkTP("operator!=(std::chrono::time_point, std::chrono::time_point)")));
  add(new ModelReadOnly(
      mkTP("operator<(std::chrono::time_point, std::chrono::time_point)")));
  add(new ModelReadOnly(
      mkTP("operator<=(std::chrono::time_point, std::chrono::time_point)")));
  add(new ModelReadOnly(
      mkTP("operator>(std::chrono::time_point, std::chrono::time_point)")));
  add(new ModelReadOnly(
      mkTP("operator>=(std::chrono::time_point, std::chrono::time_point)")));
  add(new ModelReadWrite(mkTP("time_since_epoch()"), {0}));
  add(new ModelReadOnly(mkTP("time_point_cast(std::chrono::time_point)")));
  add(new ModelReadWrite(
      mkTP("floor(std::chrono::time_point)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      mkTP("ceil(std::chrono::time_point)"), {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      mkTP("round(std::chrono::time_point)"), {0, 1}, Model::ReturnSpec::Arg0));

  // System clock
  add(new ModelReadOnly(mkSysClk("now()")));
  add(new ModelReadOnly(mkSysClk("to_time_t(std::chrono::time_point)")));
  add(new ModelReadOnly(mkSysClk("from_time_t(std::time_t)")));

  // Steady clock
  add(new ModelReadOnly(mkSteadyClk("now()")));

  // High-resolution clock
  add(new ModelReadOnly(mkHRTClk("now()")));
}
