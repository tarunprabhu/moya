#ifndef MOYA_MODELS_MODEL_STL_MULTISET_H
#define MOYA_MODELS_MODEL_STL_MULTISET_H

#include "ModelSTLSet.h"

namespace moya {

class ModelSTLMultiset : public ModelSTLSet {
public:
  ModelSTLMultiset(const std::string&, FuncID, const Models&);
  virtual ~ModelSTLMultiset() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_MULTISET_H
