#ifndef MOYA_MODELS_MODEL_STL_MAP_H
#define MOYA_MODELS_MODEL_STL_MAP_H

#include "ModelSTLTree.h"

namespace moya {

class ModelSTLMap : public ModelSTLTree {
protected:
  virtual ModelFn modelAccessElement;
  virtual ModelFn modelEmplaceMapped;

protected:
  ModelSTLMap(const std::string&, STDKind, FuncID, const Models&);

public:
  ModelSTLMap(const std::string&, FuncID, const Models&);
  virtual ~ModelSTLMap() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_MAP_H
