#include "ModelSTLUnorderedMap.h"
#include "Models.h"
#include "common/LangConfig.h"

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_unordered_map);
}

static std::string mk(const std::string& name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "std::operator" << op << "(" << getContainerName() << ", "
       << getContainerName() << ")";
    ms.add(
        new ModelSTLUnorderedMap(ss.str(), ModelSTLUnorderedMap::Compare, ms));
  }
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLUnorderedMap(name, fn, ms));
}

void Models::initSTLUnorderedMap(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms, mk("unordered_map()"), ModelSTLUnorderedMap::ConstructorEmpty);
  mkModel(ms,
          mk("unordered_map(unsigned long, std::hash, std::equal_to, "
             "allocator)"),
          ModelSTLUnorderedMap::ConstructorEmpty);
  mkModel(ms,
          mk("unordered_map(T, T, unsigned long, std::hash, "
             "std::equal_to, allocator)"),
          ModelSTLUnorderedMap::ConstructorRange);
  mkModel(ms,
          mk("unordered_map(std::unordered_map)"),
          ModelSTLUnorderedMap::ConstructorCopy);
  mkModel(ms,
          mk("unordered_map(std::initializer_list, unsigned long, "
             "std::hash, std::equal_to, allocator)"),
          ModelSTLUnorderedMap::ConstructorInitList);

  // Destructor
  mkModel(ms, mk("~unordered_map()"), ModelSTLUnorderedMap::Destructor);

  // operator =
  mkModel(ms,
          mk("operator=(std::unordered_map)"),
          ModelSTLUnorderedMap::OperatorContainer);
  mkModel(ms,
          mk("operator=(std::initializer_list)"),
          ModelSTLUnorderedMap::OperatorInitList);

  // allocator
  mkModel(ms, mk("get_allocator()"), ModelSTLUnorderedMap::GetAllocator);

  // Access
  mkModel(ms, mk("at(*)"), ModelSTLUnorderedMap::AccessElement);
  mkModel(ms, mk("operator[](*)"), ModelSTLUnorderedMap::EmplaceMapped);

  // Iterators
  mkModel(ms, mk("begin()"), ModelSTLUnorderedMap::Begin);
  mkModel(ms, mk("cbegin()"), ModelSTLUnorderedMap::Begin);
  mkModel(ms, mk("end()"), ModelSTLUnorderedMap::End);
  mkModel(ms, mk("cend()"), ModelSTLUnorderedMap::End);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLUnorderedMap::Size);
  mkModel(ms, mk("size()"), ModelSTLUnorderedMap::Size);
  mkModel(ms, mk("max_size()"), ModelSTLUnorderedMap::Size);

  // Modifiers
  mkModel(ms, mk("clear()"), ModelSTLUnorderedMap::Clear);

  mkModel(ms, mk("emplace(...)"), ModelSTLUnorderedMap::EmplaceElement);
  mkModel(ms,
          mk("emplace_hint(iterator, ...)"),
          ModelSTLUnorderedMap::EmplaceElementAt);

  mkModel(ms, mk("insert(*)"), ModelSTLUnorderedMap::InsertElement);
  mkModel(ms, mk("insert(T)"), ModelSTLUnorderedMap::InsertElement);
  mkModel(ms, mk("insert(T, T)"), ModelSTLUnorderedMap::InsertRange);
  mkModel(ms, mk("insert(iterator, *)"), ModelSTLUnorderedMap::InsertElementAt);
  mkModel(ms,
          mk("insert(std::initializer_list)"),
          ModelSTLUnorderedMap::InsertInitList);

  mkModel(ms, mk("erase(*)"), ModelSTLUnorderedMap::EraseElement);
  mkModel(ms, mk("erase(iterator)"), ModelSTLUnorderedMap::EraseElementAt);
  mkModel(
      ms, mk("erase(iterator, iterator)"), ModelSTLUnorderedMap::EraseRange);

  mkModel(ms, mk("swap(std::unordered_map)"), ModelSTLUnorderedMap::Swap);

  // Lookup
  mkModel(ms, mk("count(*)"), ModelSTLUnorderedMap::Size);
  mkModel(ms, mk("find(*)"), ModelSTLUnorderedMap::Find);
  mkModel(ms, mk("equal_range(*)"), ModelSTLUnorderedMap::FindRange);
  mkModel(ms, mk("lower_bound(*)"), ModelSTLUnorderedMap::FindRange);
  mkModel(ms, mk("upper_bound(*)"), ModelSTLUnorderedMap::FindRange);

  // Buckets
  mkModel(ms, mk("begin(unsigned long)"), ModelSTLUnorderedMap::BucketBegin);
  mkModel(ms, mk("cbegin(unsigned long)"), ModelSTLUnorderedMap::BucketBegin);
  mkModel(ms, mk("end(unsigned long)"), ModelSTLUnorderedMap::BucketEnd);
  mkModel(ms, mk("cend(unsigned long)"), ModelSTLUnorderedMap::BucketEnd);
  mkModel(ms, mk("bucket_count()"), ModelSTLUnorderedMap::Buckets);
  mkModel(ms, mk("max_bucket_count()"), ModelSTLUnorderedMap::Buckets);
  mkModel(ms, mk("bucket_size(unsigned long)"), ModelSTLUnorderedMap::Buckets);
  mkModel(ms, mk("bucket(*)"), ModelSTLUnorderedMap::Buckets);

  // Hash policy
  mkModel(ms, mk("load_factor()"), ModelSTLUnorderedMap::GetLoadFactor);
  mkModel(ms, mk("max_load_factor()"), ModelSTLUnorderedMap::GetLoadFactor);
  mkModel(
      ms, mk("max_load_factor(float)"), ModelSTLUnorderedMap::SetLoadFactor);
  mkModel(ms, mk("rehash(unsigned long)"), ModelSTLUnorderedMap::Resize);
  mkModel(ms, mk("reserve(unsigned long)"), ModelSTLUnorderedMap::Resize);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);
}
