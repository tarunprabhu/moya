#include "ModelSTLHashtable.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

using namespace llvm;

namespace moya {

// This is the layout for the hashtables used in libstdc++ to represent
// STL unordered sets, maps, multisets and multimaps
//
// hashtable = { base**, base, i64, { float, i64 }, base* }
//
// base = { base* }
//
// All the base* pointers point to a node which contains the data stored in
// the container
//
// node = { base*, T }
//
// In the case of maps and multimaps, T is actually a pair <K, V>.
// All the base* pointers are accessed and modified in tandem - when one is
// read, all are read, when one is modified, all are modified. The base**
// pointers do not point to anything, but are nevertheless modified whenever
// any of the pointers are modified
//

ModelSTLHashtable::ModelSTLHashtable(const std::string& fname,
                                     STDKind cont,
                                     FuncID fn,
                                     const Models& ms)
    : ModelSTLContainer(fname,
                        cont,
                        STDKind::STD_STL_iterator_hashtable,
                        STDKind::STD_STL_const_iterator_hashtable,
                        fn,
                        ms) {
  switch(fn) {
  case InsertElement:
    processFn = static_cast<ExecuteFn>(&ModelSTLHashtable::modelInsertElement);
    break;
  case InsertElementAt:
    processFn
        = static_cast<ExecuteFn>(&ModelSTLHashtable::modelInsertElementAt);
    break;
  case InsertInitList:
    processFn = static_cast<ExecuteFn>(&ModelSTLHashtable::modelInsertInitList);
    break;
  case InsertRange:
    processFn = static_cast<ExecuteFn>(&ModelSTLHashtable::modelInsertRange);
    break;
  case Find:
    processFn = static_cast<ExecuteFn>(&ModelSTLHashtable::modelFind);
    break;
  case FindRange:
    processFn = static_cast<ExecuteFn>(&ModelSTLHashtable::modelFindRange);
    break;
  case Buckets:
    processFn = static_cast<ExecuteFn>(&ModelSTLHashtable::modelBuckets);
    break;
  case BucketBegin:
    processFn = static_cast<ExecuteFn>(&ModelSTLHashtable::modelBucketBegin);
    break;
  case BucketEnd:
    processFn = static_cast<ExecuteFn>(&ModelSTLHashtable::modelBucketEnd);
    break;
  case BucketIteratorNew:
    processFn
        = static_cast<ExecuteFn>(&ModelSTLHashtable::modelBucketIteratorNew);
    break;
  case GetLoadFactor:
    processFn = static_cast<ExecuteFn>(&ModelSTLHashtable::modelGetLoadFactor);
    break;
  case SetLoadFactor:
    processFn = static_cast<ExecuteFn>(&ModelSTLHashtable::modelSetLoadFactor);
    break;
  default:
    // Everything else will have been initialized in the ModelSTLContainer()
    // constructor
    break;
  }
}

Contents ModelSTLHashtable::getBeginIterator(const Addresses& conts,
                                             const Instruction* call,
                                             const Function* f,
                                             Store& store,
                                             AnalysisStatus& changed) const {
  Contents ret;
  for(const auto* node : getNode(conts, call, f, store, changed))
    ret.insert(node);
  return ret;
}

Addresses ModelSTLHashtable::getIteratorNode(const Addresses& iters,
                                             const Instruction* call,
                                             const Function* f,
                                             Store& store,
                                             AnalysisStatus& changed) const {
  PointerType* pNodeTy = Metadata::getSTLNodeType(f)->getPointerTo();

  return store.readAs<Address>(iters, pNodeTy, call, changed);
}

Addresses ModelSTLHashtable::getIteratorDeref(const Addresses& iters,
                                              const Instruction* call,
                                              const Function* f,
                                              Store& store,
                                              AnalysisStatus& changed) const {
  Addresses ret;
  PointerType* pNodeTy = Metadata::getSTLNodeType(f)->getPointerTo();
  unsigned long offset = Metadata::getSTLValueOffset(f);

  for(const auto* ptr : store.readAs<Address>(iters, pNodeTy, call, changed))
    ret.insert(store.make(ptr, offset));

  return ret;
}

Addresses ModelSTLHashtable::getIteratorNew(const Addresses& iters,
                                            const Instruction* call,
                                            const Function* f,
                                            Store& store,
                                            AnalysisStatus& changed) const {
  PointerType* pNodeTy = Metadata::getSTLNodeType(f)->getPointerTo();

  return store.readAs<Address>(iters, pNodeTy, call, changed);
}

AnalysisStatus ModelSTLHashtable::modelInsertRange(const Instruction* call,
                                                   const Function* f,
                                                   const Vector<Contents>& args,
                                                   Environment& env,
                                                   Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  unsigned long offset = Metadata::getSTLValueOffset(f);
  const Addresses& conts = args.at(0).getAs<Address>(call);
  const Addresses& addrs1 = args.at(1).getAs<Address>(call);
  const Addresses& addrs2 = args.at(2).getAs<Address>(call);

  changed |= markContainerWrite(conts, call, f, store);
  changed |= writeToContainer(conts, addrs1, offset, call, f, store);
  changed |= writeToContainer(conts, addrs2, offset, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLHashtable::updateContainerPointers(const Address* base,
                                           const Addresses& vals,
                                           const Instruction* call,
                                           const Function* f,
                                           Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();
  unsigned vtadj = getContainerVtableOffset(base, call, f, store);
  for(unsigned offset : {16, 40})
    for(const Content* c : vals)
      store.write(store.make(base, vtadj + offset), c, pBaseTy, call, changed);

  return changed;
}

AnalysisStatus ModelSTLHashtable::updateNodePointers(const Address* base,
                                                     const Addresses& vals,
                                                     const Instruction* call,
                                                     const Function* f,
                                                     Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();
  for(const Content* c : vals)
    store.write(base, c, pBaseTy, call, changed);

  return changed;
}

AnalysisStatus
ModelSTLHashtable::modelInsertElement(const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  Contents iters = getBeginIterator(conts, call, f, store, changed);
  Contents flags = {store.getRuntimeScalar()};

  changed |= writeToContainer(conts, args, 1, call, f, store);

  env.push(store.make({iters, flags}));

  return changed;
}

AnalysisStatus
ModelSTLHashtable::modelInsertElementAt(const Instruction* call,
                                        const Function* f,
                                        const Vector<Contents>& args,
                                        Environment& env,
                                        Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  Contents iters = getBeginIterator(conts, call, f, store, changed);
  Contents flags = {store.getRuntimeScalar()};

  changed |= writeToContainer(conts, args, 2, call, f, store);

  env.push(store.make({iters, flags}));

  return changed;
}

AnalysisStatus
ModelSTLHashtable::modelInsertInitList(const Instruction* call,
                                       const Function* f,
                                       const Vector<Contents>& args,
                                       Environment& env,
                                       Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  const Addresses& addrs = args.at(1).getAs<Address>(call);

  changed |= writeToContainer(conts, addrs, 0, call, f, store);

  return changed;
}

AnalysisStatus ModelSTLHashtable::modelFind(const Instruction* call,
                                            const Function* f,
                                            const Vector<Contents>& args,
                                            Environment& env,
                                            Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  Contents begs = getBeginIterator(conts, call, f, store, changed);

  env.push(begs);

  return changed;
}

AnalysisStatus ModelSTLHashtable::modelFindRange(const Instruction* call,
                                                 const Function* f,
                                                 const Vector<Contents>& args,
                                                 Environment& env,
                                                 Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  Contents begs = getBeginIterator(conts, call, f, store, changed);
  Contents ends = getEndIterator(conts, call, f, store, changed);

  env.push(store.make({begs, ends}));

  return changed;
}

AnalysisStatus ModelSTLHashtable::modelBucketBegin(const Instruction* call,
                                                   const Function* f,
                                                   const Vector<Contents>& args,
                                                   Environment& env,
                                                   Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("Unimplemented model: modelBucketBegin()");

  // Addresses conts = getAddresses(args.at(1), call, changed);
  // Contents its = getBeginIterator(conts, call, f, store, changed);
  // Content* ret = store.make(
  //     {iters, {store.getRuntimeScalar()}, {store.getRuntimeScalar()}});
  // StructType* iterTy = Metadata::getSTLIteratorType(f);
  // StructType* bufTy = Metadata::getSTLValueType(f);
  // Addresses srets = getAddresses(args.at(0), call, changed);
  // for(Store::Address* sret : srets)
  //   store.write(sret, ret, bufTy, call, changed);

  return changed;
}

AnalysisStatus ModelSTLHashtable::modelBucketEnd(const Instruction* call,
                                                 const Function* f,
                                                 const Vector<Contents>& args,
                                                 Environment& env,
                                                 Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("Unimplemented model: modelBucketEnd()");

  // Addresses conts = getAddresses(args.at(1), call, changed);
  // Content* ret = store.make({{store.getNullptr()},
  //                            {store.getRuntimeScalar()},
  //                            {store.getRuntimeScalar()}});
  // StructType* iterTy = getIteratorType(getArgumentType(f, 0));
  // StructType* contentTy = getContentType(iterTy);
  // Addresses srets = getAddresses(args.at(0), call, changed);
  // for(Store::Address* sret : srets)
  //   store.write(sret, ret, contentTy, call, changed);

  return changed;
}

AnalysisStatus ModelSTLHashtable::modelBuckets(const Instruction* call,
                                               const Function* f,
                                               const Vector<Contents>& args,
                                               Environment& env,
                                               Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= markContainerRead(conts, call, f, store);

  env.push(store.getRuntimeScalar());

  return changed;
}

AnalysisStatus
ModelSTLHashtable::modelGetLoadFactor(const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= markContainerRead(conts, call, f, store);

  env.push(store.getRuntimeScalar());

  return changed;
}

AnalysisStatus
ModelSTLHashtable::modelSetLoadFactor(const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= initializeNonPointers(conts, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLHashtable::modelBucketIteratorNew(const Instruction* call,
                                          const Function* f,
                                          const Vector<Contents>& args,
                                          Environment& env,
                                          Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* i64 = Type::getInt64Ty(f->getContext());
  const Content* scalar = store.getRuntimeScalar();
  PointerType* pNodeTy = Metadata::getSTLNodeType(f)->getPointerTo();
  unsigned long offset = Metadata::getSTLValueOffset(f);
  const Addresses& iters = args.at(1).getAs<Address>(call);
  const Addresses& srets = args.at(0).getAs<Address>(call);
  Contents ret = store.readAs(iters, pNodeTy, call, changed);

  for(const Address* sret : srets) {
    store.write(sret, ret, pNodeTy, call, changed);
    for(unsigned i = 0; i < 2; i++)
      store.write(store.make(sret, i * offset), scalar, i64, call, changed);
  }

  return changed;
}

} // namespace moya
