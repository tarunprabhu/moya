#ifndef MOYA_MODELS_MODEL_STD_STRING_H
#define MOYA_MODELS_MODEL_STD_STRING_H

#include "ModelSTLContainer.h"

namespace moya {

class ModelStdString : public ModelSTLContainer {
protected:
  virtual AnalysisStatus updateContainerPointers(const Address*,
                                                 const Addresses&,
                                                 const llvm::Instruction*,
                                                 const llvm::Function*,
                                                 Store&) const;

  virtual IteratorFn getIteratorNode;
  virtual IteratorFn getIteratorDeref;

  virtual ModelFn modelConstructorGeneric;
  virtual ModelFn modelAssign;
  virtual ModelFn modelErase;
  virtual ModelFn modelInsert;
  virtual ModelFn modelCompare;
  virtual ModelFn modelCstr;
  virtual ModelFn modelBegin;
  virtual ModelFn modelEnd;
  virtual ModelFn modelFind;

protected:
  ModelStdString(const std::string&,
                 STDKind,
                 STDKind,
                 STDKind,
                 FuncID,
                 const Models&);

public:
  ModelStdString(const std::string&, FuncID, const Models&);

  virtual ~ModelStdString() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STD_STRING_H
