#include "ModelCustom.h"
#include "ModelSTLPair.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_pair);
}

static std::string mk(const std::string& name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLPair(name, fn, ms));
}

static AnalysisStatus modelMakePair(const ModelCustom& model,
                                    const Instruction* call,
                                    const Function* f,
                                    const Vector<Contents>& args,
                                    Environment& env,
                                    Store& store,
                                    const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // If the function does not have an sret argument, then the pair was
  // probably compressed into an integer type
  if(f->hasStructRetAttr()) {
    const Value* sret = LLVMUtils::getArgOperand(call, 0);
    auto* pairTy = dyn_cast<StructType>(
        LLVMUtils::stripPointers1(LLVMUtils::getAnalysisType(sret)));
    const moya::StructLayout& sl = store.getStructLayout(pairTy);
    for(const auto* dstBase : args.at(0).getAs<Address>(call)) {
      for(unsigned i = 0; i < 2; i++) {
        // Structs are passed by reference, so the argument to the function will
        // be a pointer but the element of the struct will not be. This will not
        // be annotated by the frontend because we don't really need the
        // additional annotation
        Type* elemTy = pairTy->getElementType(i);
        unsigned long size = store.getTypeAllocSize(elemTy);
        unsigned long offset = sl.getElementOffset(i);
        Type* argTy
            = LLVMUtils::getAnalysisType(LLVMUtils::getArgOperand(call, i + 1));
        const Address* dst = store.make(dstBase, offset);
        for(const auto* src : args.at(i + 1).getAs<Address>(call))
          changed |= AbstractUtils::memcpyImpl(dst, src, size, store, call);
      }
    }
  } else {
    env.push(store.getRuntimeValue(f->getReturnType()));
  }

  return changed;
}

void Models::initSTLPair(const Module& module) {
  Models& ms = *this;

  mkModel(ms, mk("pair()"), ModelSTLPair::ConstructorEmpty);
  mkModel(ms, mk("pair(*, *)"), ModelSTLPair::ConstructorElement);
  mkModel(ms, mk("pair(std::pair)"), ModelSTLPair::ConstructorCopy);
  mkModel(ms, mk("~pair()"), ModelSTLPair::Destructor);
  mkModel(ms, mk("operator=(std::pair)"), ModelSTLPair::OperatorContainer);
  mkModel(ms, mk("swap(std::pair)"), ModelSTLPair::Swap);

  add(new ModelCustom("std::make_pair", modelMakePair));
}
