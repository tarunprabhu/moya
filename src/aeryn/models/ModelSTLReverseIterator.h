#ifndef MOYA_MODEL_STL_REVERSE_ITERATOR_H
#define MOYA_MODEL_STL_REVERSE_ITERATOR_H

#include "ModelSTLContainer.h"

namespace moya {

// The reverse iterator is just a container for other regular iterators
class ModelSTLReverseIterator : public ModelSTLContainer {
protected:
  virtual IteratorFn getIteratorDeref;
  virtual IteratorFn getIteratorNew;

protected:
  ModelSTLReverseIterator(const std::string&,
                          STDKind,
                          STDKind,
                          STDKind,
                          FuncID,
                          const Models&);

public:
  ModelSTLReverseIterator(const std::string&, FuncID, const Models&);
  virtual ~ModelSTLReverseIterator() = default;
};

} // namespace moya

#endif
