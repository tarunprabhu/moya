#ifndef MOYA_MODELS_MODEL_H
#define MOYA_MODELS_MODEL_H

#include "common/AnalysisStatus.h"
#include "common/Content.h"
#include "common/Environment.h"
#include "common/Store.h"
#include "common/Vector.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Type.h>
#include <llvm/Support/Casting.h>

#include <string>

namespace moya {

class Environment;
class Store;

class Model {
public:
  enum ReturnSpec {
    Nullptr = -4,
    AllocateAndInitialize = -3,
    Allocate = -2,
    Default = -1,
    Arg0 = 0,
    Arg1,
    Arg2,
    ArgsAll = 0x7fffffff,
  };

  enum ArgSpec {
    AllocateString = -3,
    AllocateArg = -2,
    Deallocate = -1,
    ReadWrite = 0,
    ReadOnly = 1,
  };

  enum Kind {
    ReadOnlyKind,
    ReadWriteKind,
    CustomKind,
    STLContainerKind,
    StdStreamKind,
    StdAlgorithmKind,
    NullKind,
  };

protected:
  const Model::Kind kind;
  const std::string fname;
  ReturnSpec ret;
  // Models will write default values to cells unless this is true in which
  // case, the cell will be marked as written, but nothing will actually
  // be written
  bool onlyMarkWrites;

protected:
  bool verify(llvm::FunctionType*, llvm::Type*, const Vector<llvm::Value*>&);

  AnalysisStatus markRead(const Address*,
                          Addresses&,
                          llvm::PointerType*,
                          Environment&,
                          Store&,
                          const llvm::Instruction*) const;
  AnalysisStatus markRead(const Address*,
                          Addresses&,
                          llvm::ArrayType*,
                          Environment&,
                          Store&,
                          const llvm::Instruction*) const;
  AnalysisStatus markRead(const Address*,
                          Addresses&,
                          llvm::StructType*,
                          Environment&,
                          Store&,
                          const llvm::Instruction*) const;

  AnalysisStatus markWrite(const Address*,
                           Addresses&,
                           llvm::PointerType*,
                           Environment&,
                           Store&,
                           const llvm::Instruction*) const;
  AnalysisStatus markWrite(const Address*,
                           Addresses&,
                           llvm::ArrayType*,
                           Environment&,
                           Store&,
                           const llvm::Instruction*) const;
  AnalysisStatus markWrite(const Address*,
                           Addresses&,
                           llvm::StructType*,
                           Environment&,
                           Store&,
                           const llvm::Instruction*) const;

  AnalysisStatus markRead(const Address*,
                          Addresses&,
                          llvm::Type*,
                          Environment&,
                          Store&,
                          const llvm::Instruction*) const;

  AnalysisStatus markWrite(const Address*,
                           Addresses&,
                           llvm::Type*,
                           Environment&,
                           Store&,
                           const llvm::Instruction*) const;

  llvm::Type* getAdjustedType(llvm::Type*,
                              const Address*,
                              Environment&,
                              Store&,
                              const llvm::Instruction*) const;

public:
  Model(const std::string&, Model::Kind, Model::ReturnSpec, bool = false);
  virtual ~Model() = default;

  const std::string& getFunctionName() const;
  Model::Kind getKind() const;
  Model::ReturnSpec getReturnSpec() const;
  bool isOnlyMarkWrites() const;

  AnalysisStatus markRead(const Contents&,
                          llvm::Type*,
                          Environment&,
                          Store&,
                          const llvm::Instruction*) const;
  AnalysisStatus markWrite(const Contents&,
                           llvm::Type*,
                           Environment&,
                           Store&,
                           const llvm::Instruction*) const;

  virtual AnalysisStatus process(const llvm::Instruction*,
                                 const llvm::Function*,
                                 const Vector<Contents>&,
                                 const Vector<llvm::Type*>&,
                                 Environment&,
                                 Store&,
                                 const llvm::Function*) const = 0;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_H
