#include "Model.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/StoreCell.h"

using namespace llvm;

namespace moya {

Model::Model(const std::string& fname,
             Model::Kind kind,
             Model::ReturnSpec ret,
             bool onlyMarkWrites)
    : kind(kind), fname(fname), ret(ret), onlyMarkWrites(onlyMarkWrites) {
  ;
}

Type* Model::getAdjustedType(Type* type,
                             const Address* base,
                             Environment& env,
                             Store& store,
                             const Instruction* inst) const {
  // Even if we pass base class pointers around, the model should
  // conservatively mark the object that is actually allocated in the store
  // because the functions may internally call virtual functions that will
  // use the fields of the derived class object
  if(store.hasAllocated(base))
    if(Type* allocated = store.getAllocated(base))
      if(env.isDerivedFrom(allocated, type))
        return allocated;

  if(StructType* largest = store.getLargestStructAt(base)) {
    // If the type being passed is an instance of a derived class, then the
    // element here will either be a base class instance or a vtable instance.
    if(env.isDerivedFrom(largest, type))
      return largest;

    if(store.hasFlag(base, CellFlags::Vtable))
      return largest;

    // If it is anything other than this, it is almost certainly just the
    // first element of a struct.
  }

  return type;
}

const std::string& Model::getFunctionName() const {
  return fname;
}

Model::Kind Model::getKind() const {
  return kind;
}

Model::ReturnSpec Model::getReturnSpec() const {
  return ret;
}

bool Model::isOnlyMarkWrites() const {
  return onlyMarkWrites;
}

bool Model::verify(FunctionType* ftype, Type* ret, const Vector<Value*>& args) {
  if(ftype->getReturnType() == ret)
    return false;
  if(ftype->isVarArg()) {
    for(unsigned i = 0; i < ftype->getNumParams(); i++) {
      if(i > args.size())
        return false;
      if(ftype->getParamType(i) != args.at(i)->getType())
        return false;
    }
  } else {
    if(ftype->getNumParams() != args.size())
      return false;
    for(unsigned i = 0; i < args.size(); i++)
      if(ftype->getParamType(i) != args.at(i)->getType())
        return false;
  }
  return true;
}

AnalysisStatus Model::markRead(const Address* base,
                               Addresses& seen,
                               PointerType* pty,
                               Environment& env,
                               Store& store,
                               const Instruction* inst) const {
  return store.look(base, pty, inst);
}

AnalysisStatus Model::markRead(const Address* base,
                               Addresses& seen,
                               ArrayType* aty,
                               Environment& env,
                               Store& store,
                               const Instruction* inst) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not store.isTypeAt(base, aty)) {
    moya_warning("Expected array type " << *aty << "\n "
                                        << store.getCell(base).str() << "\n "
                                        << getDiagnostic(inst));
    changed |= setTop(inst);
    return changed;
  }

  Type* elem = LLVMUtils::getFlattenedElementType(aty);
  uint64_t offset = store.getTypeAllocSize(elem);
  uint64_t nelems = store.getArrayLength(base, aty->getElementType(), 1);
  for(unsigned i = 0; i < nelems; i++)
    changed
        |= markRead(store.make(base, i * offset), seen, elem, env, store, inst);
  return changed;
}

AnalysisStatus Model::markRead(const Address* base,
                               Addresses& seen,
                               StructType* sty,
                               Environment& env,
                               Store& store,
                               const Instruction* inst) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // if(not store.isTypeAt(base, sty)) {
  //   moya_warning("Expected struct type not found:\n"
  //                << "  type: " << *sty << "\n"
  //                << "  addr: " << store.getCell(base).str() << "\n"
  //                << "   has: " << store.getStructsAt(base).str() << "\n"
  //                << "  diag: " << getDiagnostic(inst));
  //   changed |= setTop(inst);
  //   return changed;
  // }

  if(not sty->isOpaque()) {
    const StructLayout& sl = store.getStructLayout(sty);
    for(unsigned i = 0; i < sty->getNumElements(); i++) {
      Type* field = sty->getElementType(i);
      uint64_t offset = sl.getElementOffset(i);
      const Address* addr = store.make(base, offset);
      changed |= markRead(addr, seen, field, env, store, inst);
    }
  }

  return changed;
}

AnalysisStatus Model::markRead(const Address* base,
                               Addresses& seen,
                               Type* type,
                               Environment& env,
                               Store& store,
                               const Instruction* inst) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not seen.insert(base))
    return changed;

  if(LLVMUtils::isScalarTy(type))
    changed |= store.look(base, type, inst);
  else if(auto* pty = dyn_cast<PointerType>(type))
    changed |= markRead(base, seen, pty, env, store, inst);
  else if(auto* aty = dyn_cast<ArrayType>(type))
    changed |= markRead(base, seen, aty, env, store, inst);
  else if(auto* sty = dyn_cast<StructType>(type))
    changed |= markRead(base, seen, sty, env, store, inst);
  else if(LLVMUtils::isFunctionTy(type))
    // FIXME: Functions that are passed to be models will presumably be called.
    // Really shouldn't have a generic model like this for those functions
    ;
  else
    moya_warning("Unexpected type to read '" << *type << "' in model "
                                             << getFunctionName() << ": "
                                             << getDiagnostic(inst));

  return changed;
}

AnalysisStatus Model::markWrite(const Address* base,
                                Addresses& seen,
                                PointerType* pty,
                                Environment& env,
                                Store& store,
                                const Instruction* inst) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(onlyMarkWrites)
    changed |= store.touch(base, pty, inst);
  else
    store.write(base, store.getRuntimePointer(), nullptr, inst, changed);

  return changed;
}

AnalysisStatus Model::markWrite(const Address* base,
                                Addresses& seen,
                                ArrayType* aty,
                                Environment& env,
                                Store& store,
                                const Instruction* inst) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not store.isTypeAt(base, aty)) {
    moya_warning("Expected array type " << *aty << "\n "
                                        << store.getCell(base).str() << "\n  "
                                        << getDiagnostic(inst));
    changed |= setTop(inst);
    return changed;
  }

  Type* elem = LLVMUtils::getFlattenedElementType(aty);
  uint64_t offset = store.getTypeAllocSize(elem);
  uint64_t nelems = store.getArrayLength(base, aty->getElementType(), 1);
  for(unsigned i = 0; i < nelems; i++)
    changed |= markWrite(
        store.make(base, i * offset), seen, elem, env, store, inst);
  return changed;
}

AnalysisStatus Model::markWrite(const Address* base,
                                Addresses& seen,
                                StructType* sty,
                                Environment& env,
                                Store& store,
                                const Instruction* inst) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not store.isTypeAt(base, sty)) {
    moya_warning("Expected struct type " << *sty << "\n "
                                         << store.getCell(base).str() << "\n "
                                         << getDiagnostic(inst));
    changed |= setTop(inst);
    return changed;
  }

  if(not sty->isOpaque()) {
    const StructLayout& sl = store.getStructLayout(sty);
    for(unsigned i = 0; i < sty->getNumElements(); i++) {
      Type* field = sty->getElementType(i);
      uint64_t offset = sl.getElementOffset(i);
      const Address* addr = store.make(base, offset);
      if(not store.hasFlag(addr, CellFlags::Vtable))
        changed |= markWrite(addr, seen, field, env, store, inst);
    }
  }

  return changed;
}

AnalysisStatus Model::markWrite(const Address* base,
                                Addresses& seen,
                                Type* type,
                                Environment& env,
                                Store& store,
                                const Instruction* inst) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not seen.insert(base))
    return changed;

  if(LLVMUtils::isScalarTy(type))
    store.write(base, store.getRuntimeScalar(), type, inst, changed);
  else if(auto* pty = dyn_cast<PointerType>(type))
    changed |= markWrite(base, seen, pty, env, store, inst);
  else if(auto* aty = dyn_cast<ArrayType>(type))
    changed |= markWrite(base, seen, aty, env, store, inst);
  else if(auto* sty = dyn_cast<StructType>(type))
    changed |= markWrite(base, seen, sty, env, store, inst);
  else
    moya_warning("Unexpected type to write '" << *type << "' in model "
                                              << getFunctionName() << ": "
                                              << getDiagnostic(inst));

  return changed;
}

AnalysisStatus Model::markRead(const Contents& vals,
                               Type* ty,
                               Environment& env,
                               Store& store,
                               const Instruction* call) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Addresses seen;
  // FIXME: We don't try to verify that we are accessing the right types int the
  // store, but we really should
  // For the remaining arguments, the LLVM-IR generator will have ensured that
  // the types match correctly, so looking at the type of the argument itself
  // is as good as looking at the function's type
  // For vararg functions, we just assume that the extra arguments that we know
  // nothing about are not doing anything evil.
  if(auto* pty = dyn_cast<PointerType>(ty)) {
    for(const auto* addr : vals.getAs<Address>(call)) {
      Type* ety = pty->getElementType();
      Type* adjType = getAdjustedType(ety, addr, env, store, call);
      changed |= markRead(addr, seen, adjType, env, store, call);
    }
  }

  return changed;
}

AnalysisStatus Model::markWrite(const Contents& vals,
                                Type* ty,
                                Environment& env,
                                Store& store,
                                const Instruction* call) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Addresses seen;

  if(auto* pty = dyn_cast<PointerType>(ty)) {
    for(const auto* addr : vals.getAs<Address>(call)) {
      Type* ety = pty->getElementType();
      Type* adjType = getAdjustedType(ety, addr, env, store, call);
      changed |= markWrite(addr, seen, adjType, env, store, call);
    }
  }

  return changed;
}

} // namespace moya
