#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"

#include <sstream>

using namespace llvm;
using namespace moya;

static inline std::string mk(const std::string& base, const std::string& name) {
  std::stringstream ss;

  ss << "std::" << base << "::" << name;

  return ss.str();
}

static inline std::string mkbios(const std::string& name) {
  return mk("basic_ios", name);
}

static inline std::string mkiosb(const std::string& name) {
  return mk("ios_base", name);
}

void Models::initStdIO(const Module& module) {
  // basic_ios models
  add(new ModelReadWrite(mkbios("basic_ios()"), {0, 0}, true));
  add(new ModelReadWrite(
      mkbios("basic_ios(std::basic_streambuf)"), {0, 0}, true));
  add(new ModelReadWrite(mkbios("~basic_ios()"), {0, 1}, true));
  add(new ModelReadOnly(mkbios("good()")));
  add(new ModelReadOnly(mkbios("eof()")));
  add(new ModelReadOnly(mkbios("fail()")));
  add(new ModelReadOnly(mkbios("bad()")));
  add(new ModelReadOnly(mkbios("operator!()")));
  add(new ModelReadOnly(mkbios("operator void *()"),
                        Model::ReturnSpec::Nullptr));
  add(new ModelReadOnly(mkbios("operator bool()")));
  add(new ModelReadOnly(mkbios("rdstate()")));
  add(new ModelReadWrite(mkbios("setstate(std::_Ios_Iostate)"), {0, 1}, true));
  add(new ModelReadWrite(mkbios("clear(std::_Ios_Iostate)"), {0, 1}, true));
  add(new ModelReadWrite(mkbios("copyfmt(std::basic_ios)"),
                         {0, 1},
                         Model::ReturnSpec::Arg0,
                         true));
  add(new ModelReadWrite(mkbios("fill()"), {0, 1}, true));
  add(new ModelReadWrite(mkbios("fill(char)"), {0, 1}, true));
  add(new ModelReadWrite(mkbios("exceptions(std::_Ios_Iostate)"), {0}, true));
  add(new ModelReadWrite(
      mkbios("exceptions(std::_Ios_Iostate, int)"), {0, 1}, true));
  add(new ModelReadWrite(
      mkbios("imbue(std::locale)"), {0, 1}, Model::ReturnSpec::Arg1, true));
  add(new ModelReadOnly(mkbios("rdbuf()"), Model::ReturnSpec::Nullptr));
  add(new ModelReadWrite(mkbios("rdbuf(std::basic_streambuf*)"),
                         {0, 1},
                         Model::ReturnSpec::Arg1,
                         true));
  add(new ModelReadOnly(mkbios("tie()")));
  add(new ModelReadWrite(mkbios("tie(std::basic_ostream)"), {0, 1}, true));
  add(new ModelReadWrite(mkbios("narrow(char, char)"), {0, 1, 1}, true));
  add(new ModelReadWrite(mkbios("widen(char)"), {0, 1}, true));

  // ios_base models
  add(new ModelReadOnly(mkiosb("flags()")));
  add(new ModelReadWrite(mkiosb("flags(std::_Ios_Fmtflags)"),
                         {0, 1},
                         Model::ReturnSpec::Arg1,
                         true));
  add(new ModelReadWrite(mkiosb("setf(std::_Ios_Fmtflags)"),
                         {0, 1},
                         Model::ReturnSpec::Arg1,
                         true));
  add(new ModelReadWrite(
      mkiosb("setf(std::_Ios_Fmtflags, std::_Ios_Fmtflags)"), {0, 1, 1}, true));
  add(new ModelReadWrite(mkiosb("unsetf(std::_Ios_Fmtflags)"), {0, 1}, true));
  add(new ModelReadOnly(mkiosb("precision()")));
  add(new ModelReadWrite(mkiosb("precision(unsigned long)"), {0, 1}, true));
  add(new ModelReadOnly(mkiosb("width()")));
  add(new ModelReadWrite(mkiosb("width(unsigned long)"), {0, 1}, true));
  add(new ModelReadWrite(
      mkiosb("imbue(std::locale)"), {0, 1}, Model::ReturnSpec::Arg1, true));
  add(new ModelReadOnly(mkiosb("getloc()")));
  add(new ModelReadOnly(mkiosb("xalloc()")));
  add(new ModelReadWrite(mkiosb("iword(int)"), {0, 1}, true));
  add(new ModelReadWrite(mkiosb("pword(int)"), {0, 1}, true));
}
