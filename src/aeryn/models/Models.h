#ifndef MOYA_MODELS_MODELS_H
#define MOYA_MODELS_MODELS_H

#include "Model.h"
#include "common/Map.h"

#include <llvm/IR/Module.h>

namespace moya {

class Models {
private:
  Map<std::string, std::unique_ptr<Model>> models;
  Map<const llvm::Function*, std::string> modelNames;

private:
  void initLibM(const llvm::Module&);
  void initLibLLVM(const llvm::Module&);
  void initLibCppABI(const llvm::Module&);
  void initLibOpenMP(const llvm::Module&);
  void initLibMPI(const llvm::Module&);
  void initLibAMPI(const llvm::Module&);
  void initLibOpenMPI(const llvm::Module&);
  void initLibC(const llvm::Module&);
  void initLibMoya(const llvm::Module&);
  void initLibLOMP(const llvm::Module&);
  void initLibGOMP(const llvm::Module&);
  void initLibAtomic(const llvm::Module&);
  void initLibPThread(const llvm::Module&);
  void initLibHWLoc(const llvm::Module&);
  void initSysCalls(const llvm::Module&);
  void initLibDL(const llvm::Module&);
  void initLibZlib(const llvm::Module&);
  void initLibLapack(const llvm::Module&);
  void initLibCurses(const llvm::Module&);
  void initLibCantera(const llvm::Module&);
  void initLibCharm(const llvm::Module&);
  void initLibHDF5(const llvm::Module&);
  void initLibCuda(const llvm::Module&);
  void initLibFlang(const llvm::Module&);

  // HACK to work around the fact that libIX inherits from std::istream and
  // std::ostream which results in a whole lot of vtable madness getting
  // imported that we simply cannot deal with
  void initLibIX(const llvm::Module&);

  // HACK to avoid having to compile Fortran 77 with Flang
  void initLibAmos(const llvm::Module&);

  // HACK to work around having to rewrite functions in C and then using a
  // wrapper around the Fortran arrays
  void initLibFL2C(const llvm::Module&);

  void initSTLArray(const llvm::Module&);
  void initSTLVector(const llvm::Module&);
  void initSTLBitVector(const llvm::Module&);
  void initSTLDeque(const llvm::Module&);
  void initSTLForwardList(const llvm::Module&);
  void initSTLList(const llvm::Module&);
  void initSTLSet(const llvm::Module&);
  void initSTLMultiset(const llvm::Module&);
  void initSTLMap(const llvm::Module&);
  void initSTLMultimap(const llvm::Module&);
  void initSTLUnorderedSet(const llvm::Module&);
  void initSTLUnorderedMultiset(const llvm::Module&);
  void initSTLUnorderedMap(const llvm::Module&);
  void initSTLUnorderedMultimap(const llvm::Module&);
  void initSTLStack(const llvm::Module&);
  void initSTLQueue(const llvm::Module&);
  void initSTLPriorityQueue(const llvm::Module&);
  void initSTLPair(const llvm::Module&);

  void initSTLIterator(const llvm::Module&);
  void initSTLBitVectorIterator(const llvm::Module&);
  void initSTLVectorIterator(const llvm::Module&);
  void initSTLDequeIterator(const llvm::Module&);
  void initSTLListIterator(const llvm::Module&);
  void initSTLForwardListIterator(const llvm::Module&);
  void initSTLTreeIterator(const llvm::Module&);
  void initSTLHashtableIterator(const llvm::Module&);

  void initStdCpp(const llvm::Module&);
  void initStdBitset(const llvm::Module&);
  void initStdChrono(const llvm::Module&);
  void initStdFstream(const llvm::Module&);
  void initStdIO(const llvm::Module&);
  void initStdIstream(const llvm::Module&);
  void initStdOstream(const llvm::Module&);
  void initStdRegex(const llvm::Module&);
  void initStdStreambuf(const llvm::Module&);
  void initStdString(const llvm::Module&);
  void initStdStringStream(const llvm::Module&);

public:
  Models(const llvm::Module&);

  void add(Model*, bool = true);
  bool contains(const std::string&) const;
  bool contains(const llvm::Function*) const;
  bool contains(const llvm::Function&) const;
  Model* get(const std::string&) const;
  Model* get(const llvm::Function*) const;
  Model* get(const llvm::Function&) const;
};

} // namespace moya

#endif // MOYA_MODELS_MODELS_H
