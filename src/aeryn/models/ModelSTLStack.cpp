#include "ModelSTLStack.h"

using namespace llvm;

namespace moya {

// This is the stack layout in memory:
//
// stack = { i8*, i64*, { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* } }
//
// This is identical to a std::deque and is implemented using a std::deque
//

ModelSTLStack::ModelSTLStack(const std::string& fname,
                             FuncID fn,
                             const Models& ms)
    : ModelSTLDeque(fname,
                    STDKind::STD_STL_stack,
                    STDKind::STD_None,
                    STDKind::STD_None,
                    fn,
                    ms) {
  ;
}

} // namespace moya
