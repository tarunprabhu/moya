#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;
using namespace moya;

static inline std::string mk(const std::string& base, const std::string& name) {
  std::stringstream ss;

  ss << "std::" << base << "::" << name;

  return ss.str();
}

static inline std::string mkexc(const std::string& name) {
  return mk("exception", name);
}

static inline std::string mkrerr(const std::string& name) {
  return mk("runtime_error", name);
}

static AnalysisStatus modelNew(const ModelCustom& model,
                               const Instruction* call,
                               const Function* f,
                               const Vector<Contents>& args,
                               Environment& env,
                               Store& store,
                               const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= AbstractUtils::neew(call, env, store, caller);

  return changed;
}

static AnalysisStatus modelDelete(const ModelCustom& model,
                                  const Instruction* call,
                                  const Function* f,
                                  const Vector<Contents>& args,
                                  Environment& env,
                                  Store& store,
                                  const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= AbstractUtils::free(
      call, args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, caller);

  return changed;
}

static AnalysisStatus modelMinMax(const ModelCustom& model,
                                  const Instruction* call,
                                  const Function* f,
                                  const Vector<Contents>& args,
                                  Environment& env,
                                  Store& store,
                                  const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Vector<Contents> elems(2);
  elems[0].insert(args.at(0));
  elems[0].insert(args.at(1));
  elems[1].insert(args.at(0));
  elems[1].insert(args.at(1));

  env.push(store.make(elems));

  return changed;
}

static AnalysisStatus modelSwap(const ModelCustom& model,
                                const Instruction* call,
                                const Function* f,
                                const Vector<Contents>& args,
                                Environment& env,
                                Store& store,
                                const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* type = dyn_cast<PointerType>(LLVMUtils::getArgument(f, 0)->getType())
                   ->getElementType();
  for(const Address* left : args.at(0).getAs<Address>(call)) {
    for(const Address* right : args.at(1).getAs<Address>(call)) {
      store.copy(left, right, type, call, changed);
      store.copy(right, left, type, call, changed);
    }
  }

  return changed;
}

static AnalysisStatus modelUnimplemented(const ModelCustom& model,
                                         const Instruction* call,
                                         const Function* f,
                                         const Vector<Contents>& args,
                                         Environment& env,
                                         Store& store,
                                         const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("Unimplemented: " << model.getFunctionName() << "\n"
                               << "  arg0: " << args.at(0).str() << "\n"
                               << "  arg1: " << args.at(1).str() << "\n"
                               << "  call: " << *call);
  // Contents ret;
  // for(unsigned i = 0; i < args.size(); i++)
  //   ret.insert(args.at(i));

  // env.push(ret);

  return changed;
}

void Models::initStdCpp(const Module& module) {
  add(new ModelCustom("operator new(unsigned long)", modelNew));
  add(new ModelCustom("operator new[](unsigned long)", modelNew));
  add(new ModelCustom("operator delete(void*)", modelDelete));
  add(new ModelCustom("operator delete[](void*)", modelDelete));

  add(new ModelReadWrite("std::allocator::allocator()", {0}));
  add(new ModelReadWrite("std::allocator::~allocator()", {0}));

  add(new ModelReadWrite("std::ios_base::Init::Init()", {0}));
  add(new ModelReadWrite("std::ios_base::Init::~Init()", {0}));
  add(new ModelReadOnly("std::ios_base::precision(long)"));

  add(new ModelReadWrite("std::getline(std::basic_istream, std::basic_string)",
                         {0, 0},
                         Model::ReturnSpec::Arg0,
                         true));
  add(new ModelReadWrite(
      "std::getline(std::basic_istream, std::basic_string, char)",
      {0, 0, 1},
      Model::ReturnSpec::Arg0,
      true));

  add(new ModelReadWrite("std::fpos::fpos(int)", {0, 1}));
  add(new ModelReadWrite("std::fpos::fpos(long)", {0, 1}));

  // Exceptions
  add(new ModelReadOnly("std::terminate()"));
  add(new ModelReadOnly("std::__throw_bad_exception()"));
  add(new ModelReadOnly("std::__throw_bad_alloc()"));
  add(new ModelReadOnly("std::__throw_bad_cast()"));
  add(new ModelReadOnly("std::__throw_bad_typeid()"));
  add(new ModelReadOnly("std::__throw_logic_error(char*)"));
  add(new ModelReadOnly("std::__throw_domain_error(char*)"));
  add(new ModelReadOnly("std::__throw_invalid_argument(char*)"));
  add(new ModelReadOnly("std::__throw_length_error(char*)"));
  add(new ModelReadOnly("std::__throw_out_of_range(char*)"));
  add(new ModelReadOnly("std::__throw_runtime_error(char*)"));
  add(new ModelReadOnly("std::__throw_range_error(char*)"));
  add(new ModelReadOnly("std::__throw_overflow_error(char*)"));
  add(new ModelReadOnly("std::__throw_underflow_error(char*)"));
  add(new ModelReadOnly("std::__throw_ios_failure(char*)"));
  add(new ModelReadOnly("std::__throw_out_of_range_fmt(char*, ...)"));
  add(new ModelReadWrite(mkexc("exception()"), {0}));
  add(new ModelReadWrite(mkexc("exception(std::exception)"), {0, 1}));
  add(new ModelReadWrite(mkexc("~exception()"), {0}));
  add(new ModelReadOnly(mkexc("what()")));
  add(new ModelReadWrite(
      mkrerr("runtime_error(std::basic_string)"), {0, 1}, true));
  add(new ModelReadWrite(mkrerr("runtime_error(char*)"), {0, 1}));
  add(new ModelReadWrite(mkrerr("~runtime_error()"), {0}));
  add(new ModelReadOnly(mkrerr("what")));

  // Casting
  add(new ModelReadOnly("__dynamic_cast", Model::ReturnSpec::Arg0));

  // IO
  add(new ModelReadOnly("std::setw(int)"));

  // Iterators
  add(new ModelReadOnly("std::advance(*, *)"));
  add(new ModelReadOnly("std::distance(*, *)"));
  add(new ModelReadOnly("std::distance(iterator, iterator)"));

  // FIXME: None of these models correctly handles the case when a comparator
  // or predicate function is provided. To do this correctly, arguments
  // must be passed correctly to the provided functions.
  //
  // FIXME: These models are wrong if an execution policy is passed as
  // an argument

  add(new ModelReadWrite(
      "std::reverse", {0, 0}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(
      "std::sort", {0, 0, 1}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite("std::unique", {0, 0}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite(
      "std::remove", {0, 0, 1}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite("std::remove_if", {0, 0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(
      "std::rotate", {0, 0, 0}, Model::ReturnSpec::Arg0, true));
  add(new ModelReadWrite("std::replace", {0, 0, 1, 1, 1}, true));
  add(new ModelReadWrite("std::replace_if", {0, 0, 1, 1, 1}, true));

  add(new ModelCustom("std::fill", modelUnimplemented));
  add(new ModelCustom("std::fill_n", modelUnimplemented));
  add(new ModelCustom("std::copy", modelUnimplemented));
  add(new ModelCustom("std::copy_n", modelUnimplemented));
  add(new ModelCustom("std::copy_backward", modelUnimplemented));
  add(new ModelCustom("std::move", modelUnimplemented));
  add(new ModelCustom("std::move_backward", modelUnimplemented));
  add(new ModelCustom("std::replace_copy", modelUnimplemented));
  add(new ModelCustom("std::remove_copy", modelUnimplemented));
  add(new ModelCustom("std::reverse_copy", modelUnimplemented));
  add(new ModelCustom("std::rotate_copy", modelUnimplemented));
  add(new ModelCustom("std::swap", modelSwap));
  add(new ModelCustom("std::swap_ranges", modelUnimplemented));
  add(new ModelCustom("std::iter_swap", modelUnimplemented));

  add(new ModelReadOnly("std::find", Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("std::find_if", Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("std::find_if_not", Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("std::find_first_of", Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("std::count"));
  add(new ModelReadOnly("std::count_if"));
  add(new ModelReadOnly("std::search", Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("std::search_n", Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("std::equal"));
  add(new ModelReadOnly("std::all_of"));
  add(new ModelReadOnly("std::any_of"));
  add(new ModelReadOnly("std::none_of"));

  add(new ModelReadOnly("std::lower_bound", Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("std::upper_bound", Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("std::binary_search"));
  add(new ModelCustom("std::equal_range", modelUnimplemented));

  add(new ModelReadOnly("std::min", Model::ReturnSpec::ArgsAll));
  add(new ModelReadOnly("std::max", Model::ReturnSpec::ArgsAll));
  add(new ModelReadOnly("std::min_element", Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("std::max_element", Model::ReturnSpec::Arg0));
  add(new ModelCustom("std::minmax", modelMinMax));
  add(new ModelCustom("std::minmax_element", modelUnimplemented));

  add(new ModelReadOnly("std::operator==(iterator, iterator)"));
  add(new ModelReadOnly("std::operator!=(iterator, iterator)"));
  add(new ModelReadOnly("std::operator=="));
  add(new ModelReadOnly("std::operator!="));
  add(new ModelReadOnly("std::operator<"));
  add(new ModelReadOnly("std::operator<="));
  add(new ModelReadOnly("std::operator>"));
  add(new ModelReadOnly("std::operator>="));

  add(new ModelReadOnly("std::fpos::operator long()"));
}
