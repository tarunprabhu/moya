#ifndef MOYA_MODELS_MODEL_STL_HASHTABLE_H
#define MOYA_MODELS_MODEL_STL_HASHTABLE_H

#include "ModelSTLContainer.h"

namespace moya {

class ModelSTLHashtable : public ModelSTLContainer {
protected:
  virtual AnalysisStatus updateContainerPointers(const Address*,
                                                 const Addresses&,
                                                 const llvm::Instruction*,
                                                 const llvm::Function*,
                                                 Store&) const;

  virtual AnalysisStatus updateNodePointers(const Address*,
                                            const Addresses&,
                                            const llvm::Instruction*,
                                            const llvm::Function*,
                                            Store&) const;

  virtual Contents getBeginIterator(const Addresses&,
                                    const llvm::Instruction*,
                                    const llvm::Function*,
                                    Store&,
                                    AnalysisStatus&) const;
  virtual IteratorFn getIteratorNode;
  virtual IteratorFn getIteratorDeref;
  virtual IteratorFn getIteratorNew;

  virtual ModelFn modelInsertElement;
  virtual ModelFn modelInsertElementAt;
  virtual ModelFn modelInsertInitList;
  virtual ModelFn modelInsertRange;
  virtual ModelFn modelFind;
  virtual ModelFn modelFindRange;
  virtual ModelFn modelBucketBegin;
  virtual ModelFn modelBucketEnd;
  virtual ModelFn modelBuckets;
  virtual ModelFn modelGetLoadFactor;
  virtual ModelFn modelSetLoadFactor;
  virtual ModelFn modelBucketIteratorNew;

public:
  ModelSTLHashtable(const std::string&, STDKind, FuncID, const Models&);

  virtual ~ModelSTLHashtable() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_HASHTABLE_H
