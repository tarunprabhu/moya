#include "ModelSTLReverseIterator.h"
#include "Models.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;
using namespace moya;

static std::string mkIter(const std::string& name) {
  std::string buf;
  raw_string_ostream ss(buf);

  ss << "std::reverse_iterator"
     << "::" << name;

  return ss.str();
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLReverseIterator(name, fn, ms));
}

void Models::initSTLIterator(const Module& module) {
  Models& ms = *this;

  mkModel(ms,
          mkIter("reverse_iterator(iterator)"),
          ModelSTLReverseIterator::IteratorConstructorCopy);
  mkModel(
      ms, mkIter("~reverse_iterator()"), ModelSTLReverseIterator::Destructor);

  mkModel(ms, mkIter("operator*()"), ModelSTLReverseIterator::IteratorDeref);
  mkModel(ms, mkIter("operator->()"), ModelSTLReverseIterator::IteratorDeref);

  mkModel(ms, mkIter("operator++()"), ModelSTLReverseIterator::IteratorSelf);
  mkModel(ms, mkIter("operator++(int)"), ModelSTLReverseIterator::IteratorNew);
  mkModel(ms, mkIter("operator--()"), ModelSTLReverseIterator::IteratorSelf);
  mkModel(ms, mkIter("operator--(int)"), ModelSTLReverseIterator::IteratorNew);
}
