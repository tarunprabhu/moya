#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/LLVMUtils.h"

using namespace llvm;
using namespace moya;

static AnalysisStatus modelOpen(const ModelCustom& model,
                                const Instruction* call,
                                const Function* f,
                                const Vector<Contents>& args,
                                Environment& env,
                                Store& store,
                                const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(call)) {
    Type* i8 = Type::getInt8Ty(f->getContext());
    const Store::Address* addr
        = AbstractUtils::allocate(i8, store, call, caller);
    store.write(addr, store.getRuntimeScalar(), i8, call, changed);
    changed |= env.add(call, addr);
  }
  env.push(env.lookup(call));

  return changed;
}

static AnalysisStatus modelError(const ModelCustom& model,
                                 const Instruction* call,
                                 const Function* f,
                                 const Vector<Contents>& args,
                                 Environment& env,
                                 Store& store,
                                 const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(call)) {
    Type* i8 = Type::getInt8Ty(f->getContext());
    const Store::Address* addr
        = AbstractUtils::allocate(i8, store, call, caller);
    store.write(addr, store.getRuntimeScalar(), i8, call, changed);
    changed |= env.add(call, addr);
  }
  env.push(env.lookup(call));

  return changed;
}

void Models::initLibDL(const Module& module) {
  add(new ModelCustom("dlopen", modelOpen));
  add(new ModelReadWrite("dlclose", {0}, true));
  add(new ModelCustom("dlerror", modelError));
  add(new ModelReadOnly("dlsym"));
}
