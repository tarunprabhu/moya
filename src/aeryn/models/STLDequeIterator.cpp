#include "ModelReadWrite.h"
#include "ModelSTLDeque.h"
#include "Models.h"
#include "common/LangConfig.h"

#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;
using namespace moya;

static std::string getIteratorName() {
  return STDConfig::getSTDName(STDKind::STD_STL_iterator_deque);
}

static std::string mkIter(const std::string& name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getIteratorName() << "::" << name;
  return ss.str();
}

static std::string mkCompare(const std::string& op) {
  std::string s;
  raw_string_ostream ss(s);
  std::string iterator = getIteratorName();
  ss << iterator << "::operator" << op << "(" << iterator << ")";
  return ss.str();
}

static std::string mkStdCompare(const std::string& op) {
  std::string s;
  raw_string_ostream ss(s);
  std::string iterator = getIteratorName();
  ss << "std::operator" << op << "(" << iterator << ", " << iterator << ")";
  return ss.str();
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLDeque(name, STDKind::STD_STL_iterator_deque, fn, ms));
}

void Models::initSTLDequeIterator(const Module& module) {
  Models& ms = *this;

  mkModel(ms,
          mkIter("_Deque_iterator(iterator)"),
          ModelSTLDeque::IteratorConstructorCopy);

  // Add the iterator comparison operator overload models
  for(const std::string& op : LangConfigCXX::getComparisonOperators()) {
    mkModel(ms, mkCompare(op), ModelSTLDeque::IteratorCompare);
    mkModel(ms, mkStdCompare(op), ModelSTLDeque::IteratorCompare);
  }

  // Operations on iterators
  mkModel(ms, mkIter("operator*()"), ModelSTLDeque::IteratorDeref);
  mkModel(ms, mkIter("operator->()"), ModelSTLDeque::IteratorDeref);
  mkModel(ms, mkIter("operator[](long)"), ModelSTLDeque::IteratorDeref);
  mkModel(ms, mkIter("operator++()"), ModelSTLDeque::IteratorSelf);
  mkModel(ms, mkIter("operator++(int)"), ModelSTLDeque::IteratorNew);
  mkModel(ms, mkIter("operator--()"), ModelSTLDeque::IteratorSelf);
  mkModel(ms, mkIter("operator--(int)"), ModelSTLDeque::IteratorNew);
  mkModel(ms, mkIter("operator+=(long)"), ModelSTLDeque::IteratorSelf);
  mkModel(ms, mkIter("operator+(long)"), ModelSTLDeque::IteratorNew);
  mkModel(ms, mkIter("operator-=(long)"), ModelSTLDeque::IteratorSelf);
  mkModel(ms, mkIter("operator-(long)"), ModelSTLDeque::IteratorNew);
  mkModel(ms, mkIter("base()"), ModelSTLDeque::IteratorBase);
}
