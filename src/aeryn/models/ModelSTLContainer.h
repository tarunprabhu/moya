#ifndef MOYA_MODELS_MODEL_STL_CONTAINER_H
#define MOYA_MODELS_MODEL_STL_CONTAINER_H

#include "Model.h"
#include "common/STDConfig.h"
#include "common/Vector.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/Instruction.h>

namespace moya {

class Environment;
class Store;
class Models;

// This is a base class that contains default implementations and common
// methods for all the STL container classes
//
// Terminology:
// For list, tree and hashtable structures, all data is contained
// within a "node". The node consists of a "base" followed by data.
//
// +------+------+
// | base | data |
// +-------------+
// |    node     |
// +-------------+
//
// The container structure consists of one or more pointers to this node.
// They will be of type base*
// For every container, we allocate a single node into which all the data is
// stored. All base* pointers point to this node. base itself could be a
// struct consisting of base* pointers.
//
// For other containers such as deque's and vectors, there is no base and the
// 'node' is simply the data buffer
//
// +------+
// | data |
// +------+
// | node |
// +------+
//
// getNode() method returns a pointer to the node
// getBuffer() returns a pointer to the actual data
// Clearly, both these functions won't necessarily return the same address
//
// getNodeOffset() returns the offset of a valid base* pointer within the
// container structure
// getBufferOffset() returns the offset of the data witin the node structure
//
// getBaseType() returns base*. This may be null.
//
// In the case of iterators, the first element is always a pointer to a node
// Naturally, the node could either be a true node or a pointer to the data
// buffer directly. Dereferencing the iterator returns a "reference" to the
// contents, so it just returns a pointer to the underlying data.
// getIteratorNode() returns a pointer to the node rather than the data and is
// used only when we have to internally dereference the iterator to get at the
// values
//
class ModelSTLContainer : public Model {
public:
  using ExecuteFn
      = AnalysisStatus (ModelSTLContainer::*)(const llvm::Instruction*,
                                              const llvm::Function*,
                                              const Vector<Contents>&,
                                              Environment&,
                                              Store&) const;

  using ModelFn = AnalysisStatus(const llvm::Instruction*,
                                 const llvm::Function*,
                                 const Vector<Contents>&,
                                 Environment&,
                                 Store&) const;

  using ImplFn = AnalysisStatus(const Addresses&,
                                const llvm::Instruction*,
                                const llvm::Function*,
                                Store&) const;

  using IteratorFn = Addresses(const Addresses&,
                               const llvm::Instruction*,
                               const llvm::Function*,
                               Store&,
                               AnalysisStatus&) const;

public:
  // ID's for the various STL container functions. Not all the function ID's
  // are valid for all containers
  enum FuncID {
    ConstructorEmpty,
    ConstructorCopy,
    ConstructorElement,
    ConstructorGeneric,
    ConstructorRange,
    ConstructorInitList,

    Destructor,

    OperatorContainer,
    OperatorInitList,

    AssignGeneric,
    AssignMany,
    AssignRange,
    AssignInitList,

    GetAllocator,
    AccessElement,
    AddElement,

    InsertGeneric,
    InsertMany,
    InsertElement,
    InsertElementAt,
    InsertRange,
    InsertInitList,

    EmplaceElement,
    EmplaceElementAt,
    EmplaceMapped,

    EraseGeneric,
    EraseElement,
    EraseElementAt,
    EraseRange,

    // Remove element is used to model pop_*() functions from either the front
    // or the back of the queue. Erase is when an element or an iterator is
    // used to remove an element
    RemoveElement,

    Size,
    Resize,
    Clear,
    Count,
    Begin,
    End,

    Swap,

    Find,
    FindRange,

    Compare,

    // Only used in STL BitVector (std::vector<bool>)
    Flip,

    // Only used in lists
    Merge,
    Splice,

    // Only used in hashtables (std::unordered_*)
    Buckets,
    BucketBegin,
    BucketEnd,
    GetLoadFactor,
    SetLoadFactor,
    BucketIteratorNew,

    // Only used in std::string
    CopyToCstr,

    IteratorConstructorEmpty,
    IteratorConstructorCopy,
    IteratorCompare,
    IteratorDeref,
    IteratorSelf,
    IteratorNew,
    IteratorBase,
  };

private:
  AnalysisStatus initializeNonPointers(const Store::Address*,
                                       llvm::StructType*,
                                       const llvm::Instruction*,
                                       Store&) const;

  AnalysisStatus updateAllPointers(const Store::Address*,
                                   const Addresses&,
                                   llvm::StructType*,
                                   llvm::PointerType*,
                                   const llvm::Instruction*,
                                   Store&) const;

  Addresses getNode(const Addresses&,
                    const llvm::Instruction*,
                    const llvm::Function*,
                    bool,
                    Store&,
                    AnalysisStatus&) const;

public:
  // Gets the buffer which contains all the containees.
  // For containers with nodes, this will be the address of the VALUE_TYPE
  // within the node
  virtual Addresses getBuffer(const Addresses&,
                              const llvm::Instruction*,
                              const llvm::Function*,
                              Store&,
                              AnalysisStatus&) const;

  // Gets the node which contains the containees and other pointers
  // If the STL container does not contain nods,
  virtual Addresses getNode(const Addresses&,
                            const llvm::Instruction*,
                            const llvm::Function*,
                            Store&,
                            AnalysisStatus&) const;

  // Initializes all the non-pointer fields in the container
  virtual AnalysisStatus initializeNonPointers(const Addresses&,
                                               const llvm::Instruction*,
                                               const llvm::Function*,
                                               Store&) const;

  // Marks all the fields in the container as having been read
  virtual AnalysisStatus markContainerRead(const Addresses&,
                                           const llvm::Instruction*,
                                           const llvm::Function*,
                                           Store&) const;

  // Marks all the fields in the container as having been modified
  virtual AnalysisStatus markContainerWrite(const Addresses&,
                                            const llvm::Instruction*,
                                            const llvm::Function*,
                                            Store&) const;

  // Marks the buffer containing the data as having been read
  virtual AnalysisStatus markBufferRead(const Addresses&,
                                        const llvm::Instruction*,
                                        const llvm::Function*,
                                        Store&) const;

  // Marks the buffer containing the data as having been written
  virtual AnalysisStatus markBufferWrite(const Addresses&,
                                         const llvm::Instruction*,
                                         const llvm::Function*,
                                         Store&) const;

  // Allocates the buffer that holds all the containees
  virtual AnalysisStatus allocateNode(const Address*,
                                      const llvm::Instruction*,
                                      const llvm::Function*,
                                      Store&) const;
  virtual AnalysisStatus allocateNode(const Addresses&,
                                      const llvm::Instruction*,
                                      const llvm::Function*,
                                      Store&) const;

  // Deallocates the buffer that holds all the containees
  virtual AnalysisStatus deallocateNode(const Addresses&,
                                        const llvm::Instruction*,
                                        const llvm::Function*,
                                        Store&) const;

  // Update the pointers in the container
  virtual AnalysisStatus updateContainerPointers(const Address*,
                                                 const Addresses&,
                                                 const llvm::Instruction*,
                                                 const llvm::Function*,
                                                 Store&) const;

  // Update the pointers in the node
  virtual AnalysisStatus updateNodePointers(const Address*,
                                            const Addresses&,
                                            const llvm::Instruction*,
                                            const llvm::Function*,
                                            Store&) const;

  // Updates all the pointers to the allocated buffer/node in the container
  virtual AnalysisStatus updateAllPointers(const Addresses&,
                                           const Addresses&,
                                           const llvm::Instruction*,
                                           const llvm::Function*,
                                           Store&) const;

  // Marks the iterator as having been read
  virtual AnalysisStatus markIteratorRead(const Addresses&,
                                          const llvm::Instruction*,
                                          const llvm::Function*,
                                          Store&) const;

  // Add vals to the container.
  virtual AnalysisStatus writeToContainer(const Addresses&,
                                          const Contents&,
                                          const llvm::Instruction*,
                                          const llvm::Function*,
                                          Store&) const;

  // Copy to the container from the given addresses
  virtual AnalysisStatus writeToContainer(const Addresses&,
                                          const Addresses&,
                                          unsigned long,
                                          const llvm::Instruction*,
                                          const llvm::Function*,
                                          Store&) const;

  // Copy to the container from the given address but a specific type
  // which may not be the value type. Mostly used to deal with containers
  // containing strings because they and NULL-terminated C-style strings are
  // treated interchangeably
  virtual AnalysisStatus writeToContainer(const Addresses&,
                                          const Addresses&,
                                          unsigned long,
                                          llvm::Type*,
                                          const llvm::Instruction*,
                                          const llvm::Function*,
                                          Store&) const;

  // Add to the container arguments to the function
  virtual AnalysisStatus writeToContainer(const Addresses&,
                                          const Vector<Contents>&,
                                          unsigned,
                                          const llvm::Instruction*,
                                          const llvm::Function*,
                                          Store&) const;

  // Returns a "reference" to the contents of the container. Since C++
  // references are actually just pointers, all this is does is return pointers
  // to the data contained in a container.
  virtual Contents readFromContainer(const Addresses&,
                                     const llvm::Instruction*,
                                     const llvm::Function*,
                                     Store&,
                                     AnalysisStatus&) const;

  // Used almost exclusively to model the begin function.
  virtual Contents getBeginIterator(const Addresses&,
                                    const llvm::Instruction*,
                                    const llvm::Function*,
                                    Store&,
                                    AnalysisStatus&) const;

  // Used almost exclusively to model the end function.
  virtual Contents getEndIterator(const Addresses&,
                                  const llvm::Instruction*,
                                  const llvm::Function*,
                                  Store&,
                                  AnalysisStatus&) const;

public:
  // When an iterator is dereferenced, it returns a "reference" to the
  // underlying data. This is essentially just a pointer to the underlying
  // data, so we return addresses
  virtual IteratorFn getIteratorNode;
  virtual IteratorFn getIteratorDeref;
  virtual IteratorFn getIteratorNew;

protected:
  // These methods are the "driver" methods for the most common STL container
  // methods. They call the specific implementations of each. They are here
  // along with getters so that the same ModelCustom-like interface can be
  // used
  virtual ModelFn modelConstructorEmpty;
  virtual ModelFn modelConstructorElement;
  virtual ModelFn modelConstructorCopy;
  virtual ModelFn modelConstructorRange;
  virtual ModelFn modelConstructorInitList;
  virtual ModelFn modelDestructor;
  virtual ModelFn modelOperatorContainer;
  virtual ModelFn modelOperatorInitList;
  virtual ModelFn modelGetAllocator;
  virtual ModelFn modelAssignMany;
  virtual ModelFn modelAssignRange;
  virtual ModelFn modelAssignInitList;
  virtual ModelFn modelAccessElement;
  virtual ModelFn modelAddElement;
  virtual ModelFn modelRemoveElement;
  virtual ModelFn modelSize;
  virtual ModelFn modelBegin;
  virtual ModelFn modelEnd;
  virtual ModelFn modelEmplace;
  virtual ModelFn modelEmplaceAt;
  virtual ModelFn modelEmplaceMapped;
  virtual ModelFn modelClear;
  virtual ModelFn modelInsertElement;
  virtual ModelFn modelInsertMany;
  virtual ModelFn modelInsertRange;
  virtual ModelFn modelInsertInitList;
  virtual ModelFn modelEraseElement;
  virtual ModelFn modelEraseElementAt;
  virtual ModelFn modelEraseRange;
  virtual ModelFn modelSwap;
  virtual ModelFn modelCount;
  virtual ModelFn modelFind;
  virtual ModelFn modelResize;
  virtual ModelFn modelCompare;
  virtual ModelFn modelIteratorConstructorEmpty;
  virtual ModelFn modelIteratorConstructorCopy;
  virtual ModelFn modelIteratorCompare;
  virtual ModelFn modelIteratorDeref;
  virtual ModelFn modelIteratorSelf;
  virtual ModelFn modelIteratorNew;
  virtual ModelFn modelIteratorBase;

  unsigned getContainerVtableOffset(const Address*,
                                    const llvm::Instruction*,
                                    const llvm::Function*,
                                    const Store&) const;

protected:
  std::string container;
  std::string iterator;
  std::string citerator;
  ExecuteFn processFn;
  const Models& models;

protected:
  ModelSTLContainer(const std::string&,
                    STDKind,
                    STDKind,
                    STDKind,
                    FuncID,
                    const Models&);

public:
  virtual ~ModelSTLContainer() = default;

  const std::string& getContainer() const;
  const std::string& getIterator() const;
  const std::string& getConstIterator() const;

  virtual AnalysisStatus process(const llvm::Instruction*,
                                 const llvm::Function*,
                                 const Vector<Contents>&,
                                 const Vector<llvm::Type*>&,
                                 Environment&,
                                 Store&,
                                 const llvm::Function*) const;

public:
  static bool classof(const Model*);
};

} // namespace moya

#endif // MOYA_MODEL_MODELS_STL_CONTAINER_H
