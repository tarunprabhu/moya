#include "ModelMPIReadWrite.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

using namespace llvm;

namespace moya {

ModelMPIReadWrite::ModelMPIReadWrite(
    const std::string& fname,
    const std::initializer_list<int>& argStatus,
    Model::ReturnSpec ret)
    : ModelReadWrite(fname, argStatus, ret) {
  ;
}

AnalysisStatus ModelMPIReadWrite::process(const Instruction* inst,
                                          const Function* f,
                                          const Vector<Contents>& args,
                                          const Vector<Type*>& argTys,
                                          Environment& env,
                                          Store& store,
                                          const Function* caller) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // There might be additional arguments to the function if a std::string is
  // passed as one of the arguments. These will be the std::string lengths and
  // added to the end of the function. They will also be immutable integers
  for(unsigned i = 0; i < std::min(args.size(), argStatus.size()); i++) {
    if(auto* pty = dyn_cast<PointerType>(argTys.at(i))) {
      if(auto* sty = dyn_cast<StructType>(pty->getElementType())) {
        for(const auto* addr : args.at(i).getAs<Address>(inst))
          if(store.isTypeAt(addr, sty)) {
            changed |= markRead({addr}, sty, env, store, inst);
            if(argStatus.at(i) == Model::ArgSpec::ReadWrite)
              changed |= markWrite({addr}, sty, env, store, inst);
          }
      } else {
        changed |= markRead(args.at(i), argTys.at(i), env, store, inst);
        if(argStatus.at(i) == Model::ArgSpec::ReadWrite)
          changed |= markWrite(args.at(i), argTys.at(i), env, store, inst);
      }
    } else {
      changed |= markRead(args.at(i), argTys.at(i), env, store, inst);
      if(argStatus.at(i) == Model::ArgSpec::ReadWrite)
        changed |= markWrite(args.at(i), argTys.at(i), env, store, inst);
    }
  }

  if(not f->getReturnType()->isVoidTy()) {
    switch(ret) {
    case Model::ReturnSpec::Default:
      env.push(store.getRuntimeValue(f->getReturnType()));
      break;
    default:
      env.push(args.at(static_cast<int>(ret)));
      break;
    }
  }

  return changed;
}

} // namespace moya
