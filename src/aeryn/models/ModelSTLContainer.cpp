#include "ModelSTLContainer.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/StoreCell.h"
#include "common/StringUtils.h"

using namespace llvm;

namespace moya {

ModelSTLContainer::ModelSTLContainer(const std::string& fname,
                                     STDKind cont,
                                     STDKind iter,
                                     STDKind citer,
                                     FuncID fn,
                                     const Models& models)
    : Model(fname, Model::Kind::STLContainerKind, Model::ReturnSpec::Default),
      container(STDConfig::getSTDName(cont)),
      iterator(STDConfig::getSTDName(iter)),
      citerator(STDConfig::getSTDName(citer)), models(models) {
  switch(fn) {
  case ConstructorEmpty:
    processFn = &ModelSTLContainer::modelConstructorEmpty;
    break;
  case ConstructorCopy:
    processFn = &ModelSTLContainer::modelConstructorCopy;
    break;
  case ConstructorElement:
    processFn = &ModelSTLContainer::modelConstructorElement;
    break;
  case ConstructorRange:
    processFn = &ModelSTLContainer::modelConstructorRange;
    break;
  case ConstructorInitList:
    processFn = &ModelSTLContainer::modelConstructorInitList;
    break;
  case Destructor:
    processFn = &ModelSTLContainer::modelDestructor;
    break;
  case OperatorContainer:
    processFn = &ModelSTLContainer::modelOperatorContainer;
    break;
  case OperatorInitList:
    processFn = &ModelSTLContainer::modelOperatorInitList;
    break;
  case AssignMany:
    processFn = &ModelSTLContainer::modelAssignMany;
    break;
  case AssignRange:
    processFn = &ModelSTLContainer::modelAssignRange;
    break;
  case AssignInitList:
    processFn = &ModelSTLContainer::modelAssignInitList;
    break;
  case GetAllocator:
    processFn = &ModelSTLContainer::modelGetAllocator;
    break;
  case AccessElement:
    processFn = &ModelSTLContainer::modelAccessElement;
    break;
  case AddElement:
    processFn = &ModelSTLContainer::modelAddElement;
    break;
  case InsertMany:
    processFn = &ModelSTLContainer::modelInsertMany;
    break;
  case InsertElement:
    processFn = &ModelSTLContainer::modelInsertElement;
    break;
  case InsertRange:
    processFn = &ModelSTLContainer::modelInsertRange;
    break;
  case InsertInitList:
    processFn = &ModelSTLContainer::modelInsertInitList;
    break;
  case EmplaceElement:
    processFn = &ModelSTLContainer::modelEmplace;
    break;
  case EmplaceElementAt:
    processFn = &ModelSTLContainer::modelEmplaceAt;
    break;
  case EraseElement:
    processFn = &ModelSTLContainer::modelEraseElement;
    break;
  case EraseElementAt:
    processFn = &ModelSTLContainer::modelEraseElementAt;
    break;
  case EraseRange:
    processFn = &ModelSTLContainer::modelEraseRange;
    break;
  case RemoveElement:
    processFn = &ModelSTLContainer::modelRemoveElement;
    break;
  case Size:
    processFn = &ModelSTLContainer::modelSize;
    break;
  case Resize:
    processFn = &ModelSTLContainer::modelResize;
    break;
  case Clear:
    processFn = &ModelSTLContainer::modelClear;
    break;
  case Swap:
    processFn = &ModelSTLContainer::modelSwap;
    break;
  case Find:
    processFn = &ModelSTLContainer::modelFind;
    break;
  case Count:
    processFn = &ModelSTLContainer::modelCount;
    break;
  case Compare:
    processFn = &ModelSTLContainer::modelCompare;
    break;
  case Begin:
    processFn = &ModelSTLContainer::modelBegin;
    break;
  case End:
    processFn = &ModelSTLContainer::modelEnd;
    break;
  case IteratorConstructorEmpty:
    processFn = &ModelSTLContainer::modelIteratorConstructorEmpty;
    break;
  case IteratorConstructorCopy:
    processFn = &ModelSTLContainer::modelIteratorConstructorCopy;
    break;
  case IteratorCompare:
    processFn = &ModelSTLContainer::modelIteratorCompare;
    break;
  case IteratorDeref:
    processFn = &ModelSTLContainer::modelIteratorDeref;
    break;
  case IteratorSelf:
    processFn = &ModelSTLContainer::modelIteratorSelf;
    break;
  case IteratorNew:
    processFn = &ModelSTLContainer::modelIteratorNew;
    break;
  case IteratorBase:
    processFn = &ModelSTLContainer::modelIteratorBase;
    break;
  default:
    // Anyting not set here will be set in the constructor of the container
    // that specializes this
    break;
  }
}

const std::string& ModelSTLContainer::getContainer() const {
  return container;
}

const std::string& ModelSTLContainer::getIterator() const {
  return iterator;
}

const std::string& ModelSTLContainer::getConstIterator() const {
  return citerator;
}

bool ModelSTLContainer::classof(const Model* model) {
  return model->getKind() == Model::Kind::STLContainerKind;
}

// If the user has inherited from the container (which they really
// shouldn't be doing in the first place), there might be a vtable(s)
// which should be skipped
unsigned ModelSTLContainer::getContainerVtableOffset(const Address* cont,
                                                     const Instruction* call,
                                                     const Function* f,
                                                     const Store& store) const {
  unsigned offset = 0;

  while(store.hasFlag(store.make(cont, offset), CellFlags::Vtable))
    offset += 8;

  return offset;
}

AnalysisStatus ModelSTLContainer::process(const Instruction* call,
                                          const Function* f,
                                          const Vector<Contents>& args,
                                          const Vector<Type*>& argTys,
                                          Environment& env,
                                          Store& store,
                                          const Function* caller) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not processFn)
    moya_error("process function not set: " << f->getName() << "\n");

  for(unsigned i = 0; i < args.size(); i++)
    changed |= markRead(args.at(i), argTys.at(i), env, store, call);

  changed |= (this->*processFn)(call, f, args, env, store);

  return changed;
}

Addresses ModelSTLContainer::getNode(const Addresses& conts,
                                     const Instruction* call,
                                     const Function* f,
                                     bool allocate,
                                     Store& store,
                                     AnalysisStatus& changed) const {
  unsigned long offset = Metadata::getSTLPointerOffset(f);
  Type* nodeTy = nullptr;
  if(Metadata::hasSTLNodeType(f))
    nodeTy = Metadata::getSTLNodeType(f);
  else if(Metadata::hasSTLValueType(f))
    nodeTy = Metadata::getSTLValueType(f);
  else
    moya_error("Container must have either node type or value type: "
               << getFunctionName() << " in "
               << getDiagnostic(LLVMUtils::getFunction(call)));
  Type* pNodeTy = nodeTy->getPointerTo();

  if(allocate)
    changed |= allocateNode(conts, call, f, store);

  Addresses ret;
  for(const Address* base : conts) {
    unsigned vtadj = getContainerVtableOffset(base, call, f, store);
    ret.insert(
        store.readAs<Address>(base, vtadj + offset, pNodeTy, call, changed));
  }
  return ret;
}

Addresses ModelSTLContainer::getNode(const Addresses& conts,
                                     const Instruction* call,
                                     const Function* f,
                                     Store& store,
                                     AnalysisStatus& changed) const {
  return getNode(conts, call, f, true, store, changed);
}

Addresses ModelSTLContainer::getBuffer(const Addresses& conts,
                                       const Instruction* call,
                                       const Function* f,
                                       Store& store,
                                       AnalysisStatus& changed) const {
  Addresses ret;

  unsigned long offset = Metadata::getSTLValueOffset(f);
  for(const Address* node : getNode(conts, call, f, store, changed))
    ret.insert(store.make(node, offset));

  return ret;
}

AnalysisStatus ModelSTLContainer::updateContainerPointers(const Address*,
                                                          const Addresses&,
                                                          const Instruction*,
                                                          const Function*,
                                                          Store&) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("Must specialize updateContainerPointers() for container: "
             << getFunctionName());

  return changed;
}

AnalysisStatus ModelSTLContainer::updateNodePointers(const Address*,
                                                     const Addresses&,
                                                     const Instruction*,
                                                     const Function*,
                                                     Store&) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("Must specialize updateNodePointers() for container: "
             << getFunctionName());

  return changed;
}

AnalysisStatus ModelSTLContainer::updateAllPointers(const Addresses& conts,
                                                    const Addresses& vals,
                                                    const Instruction* call,
                                                    const Function* f,
                                                    Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const Address* cont : conts)
    changed |= updateContainerPointers(cont, vals, call, f, store);
  if(Metadata::hasSTLBaseType(f))
    for(const Address* node : getNode(conts, call, f, store, changed))
      changed |= updateNodePointers(node, vals, call, f, store);

  return changed;
}

AnalysisStatus ModelSTLContainer::initializeNonPointers(const Address* addr,
                                                        StructType* sty,
                                                        const Instruction* call,
                                                        Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const StructLayout& sl = store.getStructLayout(sty);
  for(unsigned i = 0; i < sty->getNumElements(); i++) {
    Type* field = sty->getElementType(i);
    unsigned long off = sl.getElementOffset(i);
    if(auto* sty = dyn_cast<StructType>(field))
      changed |= initializeNonPointers(store.make(addr, off), sty, call, store);
    else if(isa<ArrayType>(field))
      // The only array types in STL containers are the arrays of bytes that
      // represent the data that is actually contained
      ;
    else if(not isa<PointerType>(field))
      store.write(store.make(addr, off),
                  store.getRuntimeScalar(),
                  field,
                  call,
                  changed);
  }

  return changed;
}

AnalysisStatus ModelSTLContainer::initializeNonPointers(const Addresses& conts,
                                                        const Instruction* call,
                                                        const Function* f,
                                                        Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  StructType* contTy = Metadata::getSTLContainerType(f);
  StructType* baseTy = nullptr;
  if(Metadata::hasSTLBaseType(f))
    baseTy = Metadata::getSTLBaseType(f);
  for(const Address* cont : conts) {
    unsigned vtadj = getContainerVtableOffset(cont, call, f, store);
    const Address* addr = store.make(cont, vtadj);
    changed |= initializeNonPointers(addr, contTy, call, store);
    if(baseTy)
      for(const Address* node : getNode(conts, call, f, store, changed))
        changed |= initializeNonPointers(node, baseTy, call, store);
  }

  return changed;
}

AnalysisStatus ModelSTLContainer::markIteratorRead(const Addresses& iters,
                                                   const Instruction* call,
                                                   const Function* f,
                                                   Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  StructType* iterTy = Metadata::getSTLIteratorType(f);
  for(const Address* iter : iters)
    changed |= store.look(iter, iterTy, call);

  return changed;
}

AnalysisStatus ModelSTLContainer::markContainerRead(const Addresses& conts,
                                                    const Instruction* call,
                                                    const Function* f,
                                                    Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const Address* cont : conts) {
    unsigned vtadj = getContainerVtableOffset(cont, call, f, store);
    const Address* addr = store.make(cont, vtadj);
    changed |= store.look(addr, Metadata::getSTLContainerType(f), call);
    if(Metadata::hasSTLBaseType(f))
      for(const Address* node : getNode(conts, call, f, store, changed))
        changed |= store.look(node, Metadata::getSTLBaseType(f), call);
  }

  return changed;
}

AnalysisStatus ModelSTLContainer::markContainerWrite(const Addresses& conts,
                                                     const Instruction* call,
                                                     const Function* f,
                                                     Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const Address* cont : conts) {
    unsigned vtadj = getContainerVtableOffset(cont, call, f, store);
    const Address* addr = store.make(cont, vtadj);
    changed |= store.touch(addr, Metadata::getSTLContainerType(f), call);
    if(Metadata::hasSTLBaseType(f))
      for(const Address* node : getNode(conts, call, f, store, changed))
        changed |= store.touch(node, Metadata::getSTLBaseType(f), call);
  }

  return changed;
}

AnalysisStatus ModelSTLContainer::markBufferRead(const Addresses& conts,
                                                 const Instruction* call,
                                                 const Function* f,
                                                 Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  unsigned long offset = Metadata::getSTLValueOffset(f);
  Type* bufTy = Metadata::getSTLValueType(f);
  for(const Address* node : getNode(conts, call, f, store, changed))
    changed |= store.look(store.make(node, offset), bufTy, call);

  return changed;
}

AnalysisStatus ModelSTLContainer::markBufferWrite(const Addresses& conts,
                                                  const Instruction* call,
                                                  const Function* f,
                                                  Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  unsigned long offset = Metadata::getSTLValueOffset(f);
  Type* bufTy = Metadata::getSTLValueType(f);
  for(const Address* node : getNode(conts, call, f, store, changed))
    changed |= store.touch(store.make(node, offset), bufTy, call);

  return changed;
}

AnalysisStatus ModelSTLContainer::allocateNode(const Address* cont,
                                               const Instruction* call,
                                               const Function* f,
                                               Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  unsigned long ptrOffset = Metadata::getSTLPointerOffset(f);
  Type* nodeTy = nullptr;
  Type* baseTy = nullptr;
  if(Metadata::hasSTLBaseType(f)) {
    baseTy = Metadata::getSTLBaseType(f);
    nodeTy = Metadata::getSTLNodeType(f);
  } else {
    baseTy = nodeTy = Metadata::getSTLValueType(f);
  }
  Type* pBaseTy = baseTy->getPointerTo();
  llvm::errs() << "ptrOffset: " << Metadata::getSTLPointerOffset(f) << "\n";

  if(not store.read(store.make(cont, ptrOffset), pBaseTy, call, changed)
             .getAs<Address>(call)
             .size()) {
    const Function* caller = LLVMUtils::getFunction(call);
    const Address* node = AbstractUtils::allocate(nodeTy, store, call, caller);
    changed
        |= updateAllPointers(Addresses(cont), Addresses(node), call, f, store);
  }

  return changed;
}

AnalysisStatus ModelSTLContainer::allocateNode(const Addresses& conts,
                                               const Instruction* call,
                                               const Function* f,
                                               Store& store) const {

  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const Address* cont : conts) {
    unsigned vtadj = getContainerVtableOffset(cont, call, f, store);
    const Address* addr = store.make(cont, vtadj);
    changed |= allocateNode(addr, call, f, store);
  }

  return changed;
}

AnalysisStatus ModelSTLContainer::deallocateNode(const Addresses& conts,
                                                 const Instruction* call,
                                                 const Function* f,
                                                 Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* nodeTy = nullptr;
  if(Metadata::hasSTLNodeType(f))
    nodeTy = Metadata::getSTLNodeType(f);
  else if(Metadata::hasSTLValueType(f))
    nodeTy = Metadata::getSTLValueType(f);
  else
    moya_error("STL function must have either node or value type: "
               << getDiagnostic(call));

  const Function* caller = LLVMUtils::getFunction(call);
  for(const Address* buf : getNode(conts, call, f, false, store, changed))
    changed |= store.deallocate(nodeTy, buf, call, caller);
  changed |= markContainerWrite(conts, call, f, store);

  return changed;
}

static Contents makeContentsFromPackedArgs(Type* ty,
                                           const Vector<Contents>& args,
                                           const Instruction* call,
                                           const Function* f,
                                           Store& store,
                                           unsigned& arg,
                                           uint64_t& remaining) {
  uint64_t size = store.getTypeSize(ty);

  if(size > remaining) {
    arg++;
    remaining = store.getTypeSize(LLVMUtils::getArgumentType(f, arg));
  }
  remaining -= size;

  return {store.getRuntimeScalar()};
}

static Contents makeContentsFromPackedArgs(PointerType* pty,
                                           const Vector<Contents>& args,
                                           const Instruction* call,
                                           const Function* f,
                                           Store& store,
                                           unsigned& arg,
                                           uint64_t& remaining) {
  Contents contents;
  uint64_t size = store.getTypeSize(pty);

  if(size > remaining) {
    arg++;
    remaining = store.getTypeSize(LLVMUtils::getArgumentType(f, arg));
  }
  remaining -= size;

  for(const auto* cint : args.at(arg).getAs<ContentScalar>(call))
    contents.insert(store.make(cint->getValue()));

  return contents;
}

static Contents makeContentsFromPackedArgs(ArrayType* aty,
                                           const Vector<Contents>& args,
                                           const Instruction* call,
                                           const Function* f,
                                           Store& store,
                                           unsigned& arg,
                                           uint64_t& remaining) {
  Contents contents;

  if(LLVMUtils::isScalarTy(aty->getElementType())) {
    uint64_t size = store.getTypeSize(aty->getElementType());
    for(unsigned i = 0; i < aty->getNumElements(); i++) {
      if(size > remaining) {
        arg++;
        remaining = store.getTypeSize(LLVMUtils::getArgumentType(f, arg));
      }
      remaining -= size;
    }
    contents.insert(store.getRuntimeScalar());
  } else {
    moya_error("Unsupported type in packed arg: " << *aty << ". From "
                                                  << getDiagnostic(call));
  }

  return contents;
}

static Contents makeContentsFromPackedArgs(StructType* sty,
                                           const Vector<Contents>& args,
                                           const Instruction* call,
                                           const Function* f,
                                           Store& store,
                                           unsigned& arg,
                                           uint64_t& remaining) {
  Vector<Contents> contents;

  for(Type* field : sty->elements())
    if(LLVMUtils::isScalarTy(field))
      contents.push_back(makeContentsFromPackedArgs(
          field, args, call, f, store, arg, remaining));
    else if(auto* pty = dyn_cast<PointerType>(field))
      contents.push_back(makeContentsFromPackedArgs(
          pty, args, call, f, store, arg, remaining));
    else if(auto* tty = dyn_cast<StructType>(field))
      contents.push_back(makeContentsFromPackedArgs(
          tty, args, call, f, store, arg, remaining));
    else if(auto* aty = dyn_cast<ArrayType>(field))
      contents.push_back(makeContentsFromPackedArgs(
          aty, args, call, f, store, arg, remaining));

  return {store.make(contents)};
}

static Contents makeContentsFromPackedArgs(StructType* sty,
                                           const Vector<Contents>& args,
                                           const Instruction* call,
                                           const Function* f,
                                           unsigned arg,
                                           Store& store) {
  uint64_t remaining = store.getTypeSize(LLVMUtils::getArgumentType(f, arg));

  return {
      makeContentsFromPackedArgs(sty, args, call, f, store, arg, remaining)};
}

AnalysisStatus ModelSTLContainer::writeToContainer(const Addresses& conts,
                                                   const Vector<Contents>& args,
                                                   unsigned start,
                                                   const Instruction* call,
                                                   const Function* f,
                                                   Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* valTy = Metadata::getSTLValueType(f);
  if(LLVMUtils::getArgumentType(f, start)->isPointerTy()) {
    // Anything that is not a compile-time constant is passed by reference
    // (C++11), (which is implemented as passing by pointer), so we need to
    // dereference it. Even when pointers are passed, they are passed by
    // reference, so the types will always be correct
    const Addresses& addrs = args.at(start).getAs<Address>(call);
    changed |= writeToContainer(conts, addrs, 0, call, f, store);
  } else if(auto* sty = dyn_cast<StructType>(valTy)) {
    // The check for pointer has to go first because even if the value type
    // is a struct, it may have been passed by reference
    Contents vals
        = makeContentsFromPackedArgs(sty, args, call, f, start, store);
    changed |= writeToContainer(conts, vals, call, f, store);
  } else {
    changed |= writeToContainer(conts, args.at(start), call, f, store);
  }

  return changed;
}

AnalysisStatus ModelSTLContainer::writeToContainer(const Addresses& conts,
                                                   const Contents& vals,
                                                   const Instruction* call,
                                                   const Function* f,
                                                   Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  unsigned long offset = Metadata::getSTLValueOffset(f);
  Type* bufTy = Metadata::getSTLValueType(f);

  for(const Address* node : getNode(conts, call, f, store, changed))
    store.write(store.make(node, offset), vals, bufTy, call, changed);

  changed |= markContainerWrite(conts, call, f, store);

  return changed;
}

AnalysisStatus ModelSTLContainer::writeToContainer(const Addresses& conts,
                                                   const Addresses& addrs,
                                                   unsigned long offset,
                                                   const Instruction* call,
                                                   const Function* f,
                                                   Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* valTy = Metadata::getSTLValueType(f);

  changed |= writeToContainer(conts, addrs, offset, valTy, call, f, store);

  return changed;
}

AnalysisStatus ModelSTLContainer::writeToContainer(const Addresses& conts,
                                                   const Addresses& addrs,
                                                   unsigned long offset,
                                                   Type* valTy,
                                                   const Instruction* call,
                                                   const Function* f,
                                                   Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const Address* buf : getBuffer(conts, call, f, store, changed))
    for(const Address* addr : addrs)
      store.copy(buf, store.make(addr, offset), valTy, call, changed);

  return changed;
}

// AnalysisStatus ModelSTLContainer::emplaceToContainer(const Addresses& conts,
//                                                      const Instruction* call,
//                                                      const Function* f,
//                                                      Store& store) const {
//   AnalysisStatus changed = AnalysisStatus::Unchanged;

//   return changed;
// }

Contents ModelSTLContainer::readFromContainer(const Addresses& conts,
                                              const Instruction* call,
                                              const Function* f,
                                              Store& store,
                                              AnalysisStatus& changed) const {
  Contents ret;

  changed |= markContainerRead(conts, call, f, store);
  for(const Address* buf : getBuffer(conts, call, f, store, changed))
    ret.insert(buf);

  return ret;
}

Contents ModelSTLContainer::getBeginIterator(const Addresses& conts,
                                             const Instruction* call,
                                             const Function* f,
                                             Store& store,
                                             AnalysisStatus& changed) const {
  Contents ret;

  changed |= markContainerRead(conts, call, f, store);
  for(const Address* buf : getBuffer(conts, call, f, store, changed))
    ret.insert(buf);

  return ret;
}

Contents ModelSTLContainer::getEndIterator(const Addresses& conts,
                                           const Instruction* call,
                                           const Function* f,
                                           Store& store,
                                           AnalysisStatus& changed) const {
  Contents ret;

  changed |= markContainerRead(conts, call, f, store);
  for(const Address* buf : getBuffer(conts, call, f, store, changed))
    ret.insert(buf);

  return ret;
}

Addresses ModelSTLContainer::getIteratorNode(const Addresses& iters,
                                             const Instruction* call,
                                             const Function* f,
                                             Store& store,
                                             AnalysisStatus& changed) const {
  Addresses ret;

  moya_error("Calling pure virtual function: getIteratorNode()");

  return ret;
}

Addresses ModelSTLContainer::getIteratorDeref(const Addresses& iters,
                                              const Instruction* call,
                                              const Function* f,
                                              Store& store,
                                              AnalysisStatus& changed) const {
  Addresses ret;

  moya_error("Calling pure virtual function: getIteratorDeref(): "
             << getDiagnostic(call));

  return ret;
}

Addresses ModelSTLContainer::getIteratorNew(const Addresses& iters,
                                            const Instruction* call,
                                            const Function* f,
                                            Store& store,
                                            AnalysisStatus& changed) const {
  Addresses ret;

  moya_error("Calling pure virtual function: getIteratorNew(): "
             << getDiagnostic(call));

  return ret;
}

// Drivers

AnalysisStatus
ModelSTLContainer::modelConstructorEmpty(const Instruction* call,
                                         const Function* f,
                                         const Vector<Contents>& args,
                                         Environment& env,
                                         Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= allocateNode(conts, call, f, store);
  changed |= initializeNonPointers(conts, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelConstructorElement(const Instruction* call,
                                           const Function* f,
                                           const Vector<Contents>& args,
                                           Environment& env,
                                           Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= allocateNode(conts, call, f, store);
  changed |= initializeNonPointers(conts, call, f, store);
  changed |= writeToContainer(conts, args, 2, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelConstructorCopy(const Instruction* call,
                                        const Function* f,
                                        const Vector<Contents>& args,
                                        Environment& env,
                                        Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& lconts = args.at(0).getAs<Address>(call);
  const Addresses& rconts = args.at(1).getAs<Address>(call);
  const Addresses& rbufs = getBuffer(rconts, call, f, store, changed);

  changed |= markContainerRead(rconts, call, f, store);
  changed |= allocateNode(lconts, call, f, store);
  changed |= initializeNonPointers(lconts, call, f, store);
  changed |= writeToContainer(lconts, rbufs, 0, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelConstructorRange(const Instruction* call,
                                         const Function* f,
                                         const Vector<Contents>& args,
                                         Environment& env,
                                         Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Module& module = *LLVMUtils::getModule(f);
  LLVMContext& context = module.getContext();

  unsigned long offset = Metadata::getSTLIteratorOffset(f);
  const Addresses& conts = args.at(0).getAs<Address>(call);
  const Addresses& addrs1 = args.at(1).getAs<Address>(call);
  const Addresses& addrs2 = args.at(2).getAs<Address>(call);

  changed |= allocateNode(conts, call, f, store);
  changed |= initializeNonPointers(conts, call, f, store);

  // Handle containers of strings as a special case. We need to do this because
  // NULL-terminated C-style strings and C++ std::strings can often be treated
  // interchangeably. This means that when initializing or assigning to a
  // container from a range, the iterators could just be pointers to const char*
  // and not proper C++ strings. This can cause all sorts of cascade failures
  // later on during the analysis. So catch it here as early as possible
  //
  StructType* stringTy = LLVMUtils::getMoyaStdStringType(module);
  if(Metadata::getSTLValueType(f) == stringTy) {
    Type* pi8 = Type::getInt8Ty(context)->getPointerTo();
    changed |= writeToContainer(conts, addrs1, offset, pi8, call, f, store);
    changed |= writeToContainer(conts, addrs2, offset, pi8, call, f, store);

    // If the store actually contained a string, mark the entire thing as
    // begin read otherwise
    for(const Address* addr : addrs1)
      if(store.isTypeAt(addr, stringTy))
        changed |= store.touch(addr, stringTy, call);
    for(const Address* addr : addrs2)
      if(store.isTypeAt(addr, stringTy))
        changed |= store.touch(addr, stringTy, call);
  } else {
    changed |= writeToContainer(conts, addrs1, offset, call, f, store);
    changed |= writeToContainer(conts, addrs2, offset, call, f, store);
  }

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelConstructorInitList(const Instruction* call,
                                            const Function* f,
                                            const Vector<Contents>& args,
                                            Environment& env,
                                            Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  const Addresses& addrs = args.at(1).getAs<Address>(call);

  changed |= allocateNode(conts, call, f, store);
  changed |= initializeNonPointers(conts, call, f, store);
  changed |= writeToContainer(conts, addrs, 0, call, f, store);

  return changed;
}

AnalysisStatus ModelSTLContainer::modelDestructor(const Instruction* call,
                                                  const Function* f,
                                                  const Vector<Contents>& args,
                                                  Environment& env,
                                                  Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  changed |= deallocateNode(conts, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelOperatorContainer(const Instruction* call,
                                          const Function* f,
                                          const Vector<Contents>& args,
                                          Environment& env,
                                          Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  unsigned long offset = Metadata::getSTLValueOffset(f);
  const Addresses& lconts = args.at(0).getAs<Address>(call);
  const Addresses& rconts = args.at(1).getAs<Address>(call);
  const Addresses& rnodes = getNode(rconts, call, f, store, changed);

  changed |= writeToContainer(lconts, rnodes, offset, call, f, store);

  env.push(args.at(0));

  return changed;
}

// This has an initialization list
AnalysisStatus
ModelSTLContainer::modelOperatorInitList(const Instruction* call,
                                         const Function* f,
                                         const Vector<Contents>& args,
                                         Environment& env,
                                         Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  const Addresses& addrs = args.at(1).getAs<Address>(call);

  changed |= writeToContainer(conts, addrs, 0, call, f, store);

  env.push(args.at(0));

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelAccessElement(const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  Contents ret = readFromContainer(conts, call, f, store, changed);

  env.push(ret);

  return changed;
}

AnalysisStatus ModelSTLContainer::modelBegin(const Instruction* call,
                                             const Function* f,
                                             const Vector<Contents>& args,
                                             Environment& env,
                                             Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  StructType* iterTy = Metadata::getSTLIteratorType(f);
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents ret = getBeginIterator(conts, call, f, store, changed);

  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, ret, iterTy, call, changed);
  else
    env.push(ret);

  return changed;
}

AnalysisStatus ModelSTLContainer::modelEnd(const Instruction* call,
                                           const Function* f,
                                           const Vector<Contents>& args,
                                           Environment& env,
                                           Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  StructType* iterTy = Metadata::getSTLIteratorType(f);
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents ret = getEndIterator(conts, call, f, store, changed);

  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, ret, iterTy, call, changed);
  else
    env.push(ret);

  return changed;
}

AnalysisStatus ModelSTLContainer::modelClear(const Instruction* call,
                                             const Function* f,
                                             const Vector<Contents>& args,
                                             Environment& env,
                                             Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= markContainerWrite(conts, call, f, store);
  changed |= markBufferWrite(conts, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelEraseElement(const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  StructType* iterTy = Metadata::getSTLIteratorType(f);
  Type* elemTy = Metadata::getSTLValueType(f);
  if(Metadata::hasSTLKeyType(f))
    elemTy = Metadata::getSTLKeyType(f);
  Type* pElemTy = elemTy->getPointerTo();
  bool hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents iters = getBeginIterator(conts, call, f, store, changed);

  changed |= markRead(args.at(hasStructRet + 1), pElemTy, env, store, call);
  changed |= markContainerWrite(conts, call, f, store);
  changed |= markBufferWrite(conts, call, f, store);
  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, iters, iterTy, call, changed);
  else
    env.push(iters);

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelEraseElementAt(const Instruction* call,
                                       const Function* f,
                                       const Vector<Contents>& args,
                                       Environment& env,
                                       Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  bool hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  StructType* iterTy = Metadata::getSTLIteratorType(f);
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents iters = getBeginIterator(conts, call, f, store, changed);

  changed |= markRead(
      args.at(hasStructRet + 1), iterTy->getElementType(0), env, store, call);
  changed |= markContainerWrite(conts, call, f, store);
  changed |= markBufferWrite(conts, call, f, store);
  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, iters, iterTy, call, changed);
  else
    env.push(iters);

  return changed;
}

AnalysisStatus ModelSTLContainer::modelEraseRange(const Instruction* call,
                                                  const Function* f,
                                                  const Vector<Contents>& args,
                                                  Environment& env,
                                                  Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  StructType* iterTy = Metadata::getSTLIteratorType(f);
  bool hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents iters = getBeginIterator(conts, call, f, store, changed);

  changed |= markRead(
      args.at(hasStructRet + 1), iterTy->getElementType(0), env, store, call);
  changed |= markRead(
      args.at(hasStructRet + 2), iterTy->getElementType(0), env, store, call);
  changed |= markContainerWrite(conts, call, f, store);
  changed |= markBufferWrite(conts, call, f, store);
  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, iters, iterTy, call, changed);
  else
    env.push(iters);

  return changed;
}

AnalysisStatus ModelSTLContainer::modelEmplace(const Instruction* call,
                                               const Function* f,
                                               const Vector<Contents>& args,
                                               Environment& env,
                                               Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("UNIMPLEMENTED: EmplaceElement");

  return changed;
}

AnalysisStatus ModelSTLContainer::modelEmplaceAt(const Instruction* call,
                                                 const Function* f,
                                                 const Vector<Contents>& args,
                                                 Environment& env,
                                                 Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("UNIMPLEMENTED: EmplaceElementAt");

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelEmplaceMapped(const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("UNIMPLEMENTED: EmplaceMapped");

  return changed;
}

AnalysisStatus ModelSTLContainer::modelAddElement(const Instruction* call,
                                                  const Function* f,
                                                  const Vector<Contents>& args,
                                                  Environment& env,
                                                  Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= writeToContainer(conts, args, 1, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelRemoveElement(const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= markBufferWrite(conts, call, f, store);
  changed |= markContainerWrite(conts, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelInsertElement(const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  StructType* iterTy = Metadata::getSTLIteratorType(f);
  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  unsigned start = hasStructRet + 2;
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents iters = getBeginIterator(conts, call, f, store, changed);

  changed |= writeToContainer(conts, args, start, call, f, store);
  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, iters, iterTy, call, changed);
  else
    env.push(iters);

  return changed;
}

AnalysisStatus ModelSTLContainer::modelInsertMany(const Instruction* call,
                                                  const Function* f,
                                                  const Vector<Contents>& args,
                                                  Environment& env,
                                                  Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  unsigned start = hasStructRet + 3;
  StructType* iterTy = Metadata::getSTLIteratorType(f);
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents iters = getBeginIterator(conts, call, f, store, changed);

  changed |= writeToContainer(conts, args, start, call, f, store);
  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, iters, iterTy, call, changed);
  else
    env.push(iters);

  return changed;
}

AnalysisStatus ModelSTLContainer::modelInsertRange(const Instruction* call,
                                                   const Function* f,
                                                   const Vector<Contents>& args,
                                                   Environment& env,
                                                   Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  unsigned long offset = Metadata::getSTLIteratorOffset(f);
  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  const Addresses& addrs1 = args.at(hasStructRet + 1).getAs<Address>(call);
  const Addresses& addrs2 = args.at(hasStructRet + 2).getAs<Address>(call);

  changed |= markContainerWrite(conts, call, f, store);
  changed |= writeToContainer(conts, addrs1, offset, call, f, store);
  changed |= writeToContainer(conts, addrs2, offset, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelInsertInitList(const Instruction* call,
                                       const Function* f,
                                       const Vector<Contents>& args,
                                       Environment& env,
                                       Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  StructType* iterTy = Metadata::getSTLIteratorType(f);
  bool hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  const Addresses& addrs = args.at(hasStructRet + 2).getAs<Address>(call);
  Contents iters = getBeginIterator(conts, call, f, store, changed);

  changed |= writeToContainer(conts, addrs, call, f, store);
  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, iters, iterTy, call, changed);
  else
    env.push(iters);

  return changed;
}

AnalysisStatus ModelSTLContainer::modelAssignMany(const Instruction* call,
                                                  const Function* f,
                                                  const Vector<Contents>& args,
                                                  Environment& env,
                                                  Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= writeToContainer(conts, args, 2, call, f, store);

  return changed;
}

AnalysisStatus ModelSTLContainer::modelAssignRange(const Instruction* call,
                                                   const Function* f,
                                                   const Vector<Contents>& args,
                                                   Environment& env,
                                                   Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  unsigned long offset = Metadata::getSTLIteratorOffset(f);
  const Addresses& conts = args.at(0).getAs<Address>(call);
  const Addresses& addrs1 = args.at(1).getAs<Address>(call);
  const Addresses& addrs2 = args.at(2).getAs<Address>(call);

  changed |= allocateNode(conts, call, f, store);
  changed |= writeToContainer(conts, addrs1, offset, call, f, store);
  changed |= writeToContainer(conts, addrs2, offset, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelAssignInitList(const Instruction* call,
                                       const Function* f,
                                       const Vector<Contents>& args,
                                       Environment& env,
                                       Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  const Addresses& addrs = args.at(1).getAs<Address>(call);

  changed |= allocateNode(conts, call, f, store);
  changed |= writeToContainer(conts, addrs, 0, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelGetAllocator(const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* i8 = Type::getInt8Ty(call->getContext());

  for(const auto* sret : args.at(0).getAs<Address>(call))
    store.write(sret, store.getRuntimeScalar(), i8, call, changed);

  return changed;
}

AnalysisStatus ModelSTLContainer::modelSize(const Instruction* call,
                                            const Function* f,
                                            const Vector<Contents>& args,
                                            Environment& env,
                                            Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= markContainerRead(conts, call, f, store);

  env.push(store.getRuntimeScalar());

  return changed;
}

AnalysisStatus ModelSTLContainer::modelResize(const Instruction* call,
                                              const Function* f,
                                              const Vector<Contents>& args,
                                              Environment& env,
                                              Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* valTy = Metadata::getSTLValueType(f);
  const Addresses& conts = args.at(0).getAs<Address>(call);
  const Function* caller = LLVMUtils::getFunction(call);

  changed |= allocateNode(conts, call, f, store);
  changed |= markContainerWrite(conts, call, f, store);
  // If an initializer element is passed, it may get written to the container
  // if the size of the container is increased. If an initializer is not passed,
  // the object will be value-initialized. This will mean writing an unknown
  // scalar, a nullptr or calling the default constructor of an object
  if(args.size() >= 3) {
    changed |= writeToContainer(conts, args, 2, call, f, store);
  } else {
    if(LLVMUtils::isScalarTy(valTy)) {
      changed
          |= writeToContainer(conts, store.getRuntimeScalar(), call, f, store);
    } else if(LLVMUtils::isPointerTy(valTy)) {
      changed |= writeToContainer(conts, store.getNullptr(), call, f, store);
    } else if(auto* sty = dyn_cast<StructType>(valTy)) {
      if(Metadata::hasSTLValueConstructor(f)) {
        Set<const Function*> callee(Metadata::getSTLValueConstructor(f));
        Vector<Contents> args = {getBuffer(conts, call, f, store, changed)};
        Vector<Type*> argTys = {Metadata::getSTLValueType(f)->getPointerTo()};
        changed |= AbstractUtils::callFunction(
            call, callee, args, argTys, models, env, store, caller);
      }
    } else if(LLVMUtils::isArrayTy(valTy)) {
      moya_error("UNIMPLEMENTED: resize for array value types");
    }
  }

  return changed;
}

AnalysisStatus ModelSTLContainer::modelCount(const Instruction* call,
                                             const Function* f,
                                             const Vector<Contents>& args,
                                             Environment& env,
                                             Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= markContainerRead(conts, call, f, store);

  env.push(store.getRuntimeScalar());

  return changed;
}

AnalysisStatus ModelSTLContainer::modelFind(const Instruction* call,
                                            const Function* f,
                                            const Vector<Contents>& args,
                                            Environment& env,
                                            Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= markContainerRead(conts, call, f, store);

  env.push(getBeginIterator(conts, call, f, store, changed));

  return changed;
}

AnalysisStatus ModelSTLContainer::modelSwap(const Instruction* call,
                                            const Function* f,
                                            const Vector<Contents>& args,
                                            Environment& env,
                                            Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts1 = args.at(0).getAs<Address>(call);
  const Addresses& conts2 = args.at(1).getAs<Address>(call);
  const Addresses& bufs1 = getNode(conts1, call, f, store, changed);
  const Addresses& bufs2 = getNode(conts2, call, f, store, changed);

  changed |= markBufferWrite(conts1, call, f, store);
  changed |= markBufferWrite(conts2, call, f, store);
  changed |= updateAllPointers(conts1, bufs2, call, f, store);
  changed |= updateAllPointers(conts2, bufs1, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelIteratorDeref(const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& iters = args.at(0).getAs<Address>(call);
  Addresses ret = getIteratorDeref(iters, call, f, store, changed);

  changed |= markIteratorRead(iters, call, f, store);

  env.push(ret);

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelIteratorSelf(const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  StructType* iterTy = Metadata::getSTLIteratorType(f);

  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      for(const auto* iter : args.at(1).getAs<Address>(call))
        store.copy(sret, iter, iterTy, call, changed);
  else
    env.push(args.at(0));

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelIteratorConstructorEmpty(const Instruction* call,
                                                 const Function* f,
                                                 const Vector<Contents>& args,
                                                 Environment& env,
                                                 Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const Address* cont : args.at(0).getAs<Address>(call)) {
    unsigned vtadj = getContainerVtableOffset(cont, call, f, store);
    const Address* addr = store.make(cont, vtadj);
    changed |= store.touch(addr, Metadata::getSTLIteratorType(f), call);
  }

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelIteratorConstructorCopy(const Instruction* call,
                                                const Function* f,
                                                const Vector<Contents>& args,
                                                Environment& env,
                                                Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  StructType* iterTy = Metadata::getSTLIteratorType(f);

  for(const auto* dst : args.at(0).getAs<Address>(call))
    for(const auto* src : args.at(1).getAs<Address>(call))
      store.copy(dst, src, iterTy, call, changed);

  return changed;
}

AnalysisStatus ModelSTLContainer::modelIteratorNew(const Instruction* call,
                                                   const Function* f,
                                                   const Vector<Contents>& args,
                                                   Environment& env,
                                                   Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  StructType* iterTy = Metadata::getSTLIteratorType(f);
  const Addresses& iters = args.at(hasStructRet).getAs<Address>(call);
  Addresses ret = getIteratorNew(iters, call, f, store, changed);

  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      for(const Address* iter : ret)
        store.copy(sret, iter, iterTy, call, changed);
  else
    env.push(ret);

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelIteratorBase(const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("Calling pure virtual implementation of modelIteratorBase for "
             << container);

  return changed;
}

AnalysisStatus ModelSTLContainer::modelCompare(const Instruction* call,
                                               const Function* f,
                                               const Vector<Contents>& args,
                                               Environment& env,
                                               Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts1 = args.at(0).getAs<Address>(call);
  const Addresses& conts2 = args.at(1).getAs<Address>(call);

  changed |= markContainerRead(conts1, call, f, store);
  changed |= markContainerRead(conts2, call, f, store);
  changed |= markBufferRead(conts1, call, f, store);
  changed |= markBufferRead(conts2, call, f, store);

  env.push(store.getRuntimeScalar());

  return changed;
}

AnalysisStatus
ModelSTLContainer::modelIteratorCompare(const Instruction* call,
                                        const Function* f,
                                        const Vector<Contents>& args,
                                        Environment& env,
                                        Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= markIteratorRead(args.at(0).getAs<Address>(call), call, f, store);
  changed |= markIteratorRead(args.at(1).getAs<Address>(call), call, f, store);

  env.push(store.getRuntimeScalar());

  return changed;
}

} // namespace moya
