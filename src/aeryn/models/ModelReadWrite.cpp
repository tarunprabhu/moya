#include "ModelReadWrite.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

using namespace llvm;

namespace moya {

ModelReadWrite::ModelReadWrite(const std::string& fname,
                               const std::initializer_list<int>& statuses,
                               Model::ReturnSpec ret,
                               bool onlyMarkWrites,
                               bool varargConst)
    : Model(fname, Model::Kind::ReadWriteKind, ret, onlyMarkWrites),
      varargConst(varargConst) {
  for(int s : statuses)
    argStatus.push_back(static_cast<Model::ArgSpec>(s));
}

ModelReadWrite::ModelReadWrite(const std::string& fname,
                               const std::initializer_list<int>& statuses,
                               bool onlyMarkWrites)
    : ModelReadWrite(fname, statuses, ReturnSpec::Default, onlyMarkWrites) {
  ;
}

ModelReadWrite::ModelReadWrite(const std::string& fname,
                               const Vector<Model::ArgSpec>& argStatus,
                               Model::ReturnSpec ret,
                               bool onlyMarkWrites,
                               bool varArgConst)
    : Model(fname, Model::Kind::ReadWriteKind, ret, onlyMarkWrites),
      argStatus(argStatus), varargConst(varArgConst) {
  ;
}

ModelReadWrite::ModelReadWrite(const std::string& fname,
                               const Vector<Model::ArgSpec>& argStatus,
                               bool onlyMarkWrites)
    : ModelReadWrite(fname, argStatus, ReturnSpec::Default, onlyMarkWrites) {
  ;
}

bool ModelReadWrite::isVarArgConst() const {
  return varargConst;
}

unsigned long ModelReadWrite::getNumArgs() const {
  return argStatus.size();
}

Model::ArgSpec ModelReadWrite::getArg(unsigned i) const {
  return argStatus.at(i);
}

AnalysisStatus ModelReadWrite::process(const Instruction* inst,
                                       const Function* f,
                                       const Vector<Contents>& args,
                                       const Vector<llvm::Type*>& argTys,
                                       Environment& env,
                                       Store& store,
                                       const Function* caller) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(f->hasStructRetAttr())
    changed |= markWrite(args.at(0), argTys.at(0), env, store, inst);

  unsigned statusIdx = 0;
  for(unsigned argIdx = f->hasStructRetAttr(); argIdx < args.size(); argIdx++) {
    const Contents& contents = args.at(argIdx);
    Type* ty = argTys.at(argIdx);

    changed |= markRead(contents, ty, env, store, inst);

    if(statusIdx < argStatus.size()) {
      Model::ArgSpec status = argStatus.at(statusIdx);
      if(status == Model::ArgSpec::ReadWrite) {
        changed |= markWrite(contents, ty, env, store, inst);
      } else if(status == Model::ArgSpec::AllocateArg) {
        Type* allocate = dyn_cast<PointerType>(ty)->getElementType();
        if(not env.contains(inst))
          for(const auto* addr : contents.getAs<Address>(inst))
            store.write(addr,
                        AbstractUtils::allocate(allocate, store, inst, caller),
                        allocate->getPointerTo(),
                        inst,
                        changed);
      } else if(status == Model::ArgSpec::Deallocate) {
        Type* deallocate = dyn_cast<PointerType>(ty)->getElementType();
        for(const Address* addr : contents.getAs<Address>(inst))
          changed |= store.deallocate(deallocate, addr, inst, caller);
      } else if(status == Model::ArgSpec::AllocateString) {
        if(not env.contains(inst)) {
          Type* i8 = Type::getInt8Ty(f->getContext());
          const Content* buf = AbstractUtils::allocate(i8, store, inst, caller);
          for(const Address* addr : contents.getAs<Address>(inst))
            store.write(addr, buf, i8, inst, changed);
        }
      }
    } else {
      if(not varargConst)
        changed |= markWrite(contents, ty, env, store, inst);
    }
    statusIdx++;
  }

  if(not f->getReturnType()->isVoidTy()) {
    Contents retval;
    switch(ret) {
    case Model::ReturnSpec::Nullptr:
      retval.insert(store.getNullptr());
      break;
    case Model::ReturnSpec::AllocateAndInitialize:
      if(not env.contains(inst)) {
        auto* objTy
            = LLVMUtils::getAnalysisTypeAs<PointerType>(inst)->getElementType();
        changed |= env.add(inst,
                           AbstractUtils::allocate(objTy, store, inst, caller));
      }
      retval.insert(env.lookup(inst));
      break;
    case Model::ReturnSpec::Allocate:
      if(not env.contains(inst)) {
        auto* objTy
            = LLVMUtils::getAnalysisTypeAs<PointerType>(inst)->getElementType();
        changed |= env.add(inst,
                           AbstractUtils::allocate(objTy, store, inst, caller));
      }
      retval.insert(env.lookup(inst));
      break;
    case Model::ReturnSpec::Default:
      retval.insert(store.getRuntimeValue(f->getReturnType()));
      break;
    case Model::ReturnSpec::ArgsAll:
      for(unsigned i = 0; i < args.size(); i++)
        retval.insert(args.at(i));
      break;
    default:
      retval.insert(args.at(static_cast<int>(ret)));
      break;
    }
    env.push(retval);
  } else {
    // If at least one of the arguments allocated something, add this to
    // the environment so it doesn't keep allocating something on subsequent
    // visits
    if(not env.contains(inst)) {
      for(ArgSpec argSpec : argStatus) {
        if(argSpec == ArgSpec::AllocateArg
           or argSpec == ArgSpec::AllocateString) {
          changed |= env.add(inst);
          break;
        }
      }
    }
  }

  return changed;
}

bool ModelReadWrite::classof(const Model* model) {
  return model->getKind() == Model::Kind::ReadWriteKind;
}

} // namespace moya
