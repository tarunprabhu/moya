#include "ModelStdString.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

using namespace llvm;

namespace moya {

// This is an 'imprecise' model of the strings by design. For the overloads
// that are defined here, we deliberately do not bother actually trying to
// correctly copy the contents of one std::string to another and just stick to
// using RuntimeScalars everywhere. This is because the actual value being
// copied around isn't really relevant for the analysis and is unlikely to ever
// be. We just want to make sure that the struct is correctly marked when we
// are using it.

// This is how a std::string is represented in libstdc++
//
// struct std::string = { i8*, i64, i64, [8 x i8] }
//
// When a string's constructor is called, a single cell is allocated for the
// contents. This will be the buffer into which all the characters will be
// stored.
//
// All iterators will return a pointer to the data buffer.
//
// Resize operations will have no effect other than marking the pointer
// and the buffer as having been modified
//

ModelStdString::ModelStdString(const std::string& fname,
                               STDKind cont,
                               STDKind iter,
                               STDKind citer,
                               FuncID fn,
                               const Models& ms)
    : ModelSTLContainer(fname, cont, iter, citer, fn, ms) {
  switch(fn) {
  case ModelStdString::Begin:
    processFn = static_cast<ExecuteFn>(&ModelStdString::modelBegin);
    break;
  case ModelStdString::End:
    processFn = static_cast<ExecuteFn>(&ModelStdString::modelEnd);
    break;
  case ModelStdString::ConstructorGeneric:
    processFn
        = static_cast<ExecuteFn>(&ModelStdString::modelConstructorGeneric);
    break;
  case ModelStdString::AssignGeneric:
    processFn = static_cast<ExecuteFn>(&ModelStdString::modelAssign);
    break;
  case ModelStdString::Compare:
    processFn = static_cast<ExecuteFn>(&ModelStdString::modelCompare);
    break;
  case ModelStdString::EraseGeneric:
    processFn = static_cast<ExecuteFn>(&ModelStdString::modelErase);
    break;
  case ModelStdString::InsertGeneric:
    processFn = static_cast<ExecuteFn>(&ModelStdString::modelInsert);
    break;
  case ModelStdString::CopyToCstr:
    processFn = static_cast<ExecuteFn>(&ModelStdString::modelCstr);
    break;
  case ModelStdString::Find:
    processFn = static_cast<ExecuteFn>(&ModelStdString::modelFind);
    break;
  default:
    // Everything else will have been initialized in the ModelSTLContainer()
    // constructor
    break;
  }
}

ModelStdString::ModelStdString(const std::string& fname,
                               FuncID fn,
                               const Models& ms)
    : ModelStdString(fname,
                     STDKind::STD_String,
                     STDKind::STD_STL_iterator_normal,
                     STDKind::STD_STL_iterator_normal,
                     fn,
                     ms) {
  ;
}

Addresses ModelStdString::getIteratorNode(const Addresses& iters,
                                          const Instruction* call,
                                          const Function* f,
                                          Store& store,
                                          AnalysisStatus& changed) const {
  PointerType* pBufTy = Type::getInt8PtrTy(call->getContext());

  return store.readAs<Address>(iters, pBufTy, call, changed);
}

Addresses ModelStdString::getIteratorDeref(const Addresses& iters,
                                           const Instruction* call,
                                           const Function* f,
                                           Store& store,
                                           AnalysisStatus& changed) const {
  PointerType* pBufTy = Type::getInt8PtrTy(call->getContext());

  return store.readAs<Address>(iters, pBufTy, call, changed);
}

AnalysisStatus ModelStdString::updateContainerPointers(const Address* base,
                                                       const Addresses& vals,
                                                       const Instruction* call,
                                                       const Function* f,
                                                       Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  unsigned vtadj = getContainerVtableOffset(base, call, f, store);
  Type* pi8 = Type::getInt8PtrTy(call->getContext());
  for(const Content* c : vals)
    store.write(store.make(base, vtadj), c, pi8, call, changed);

  return changed;
}

AnalysisStatus
ModelStdString::modelConstructorGeneric(const Instruction* call,
                                        const Function* f,
                                        const Vector<Contents>& args,
                                        Environment& env,
                                        Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  if(not env.contains(call)) {
    changed |= allocateNode(conts, call, f, store);
    env.add(call);
  }
  changed |= initializeNonPointers(conts, call, f, store);
  changed |= writeToContainer(conts, store.getRuntimeScalar(), call, f, store);

  return changed;
}

AnalysisStatus ModelStdString::modelAssign(const Instruction* call,
                                           const Function* f,
                                           const Vector<Contents>& args,
                                           Environment& env,
                                           Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= markContainerWrite(conts, call, f, store);
  changed |= writeToContainer(conts, store.getRuntimeScalar(), call, f, store);

  env.push(conts);

  return changed;
}

AnalysisStatus ModelStdString::modelErase(const Instruction* call,
                                          const Function* f,
                                          const Vector<Contents>& args,
                                          Environment& env,
                                          Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= markContainerWrite(conts, call, f, store);

  env.push(conts);

  return changed;
}

AnalysisStatus ModelStdString::modelInsert(const Instruction* call,
                                           const Function* f,
                                           const Vector<Contents>& args,
                                           Environment& env,
                                           Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= markContainerWrite(conts, call, f, store);
  changed |= writeToContainer(conts, store.getRuntimeScalar(), call, f, store);

  env.push(args.at(1));

  return changed;
}

AnalysisStatus ModelStdString::modelCompare(const Instruction* call,
                                            const Function* f,
                                            const Vector<Contents>& args,
                                            Environment& env,
                                            Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // The compare functions may compare std::string with cstrings. So we just
  // mark everything as read and return a scalar
  env.push(store.getRuntimeScalar());

  return changed;
}

AnalysisStatus ModelStdString::modelCstr(const Instruction* call,
                                         const Function* f,
                                         const Vector<Contents>& args,
                                         Environment& env,
                                         Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);
  Type* i8 = Type::getInt8Ty(call->getContext());

  changed |= markContainerRead(conts, call, f, store);
  for(const auto* addr : args.at(1).getAs<Address>(call))
    store.write(addr, store.getRuntimeScalar(), i8, call, changed);

  env.push(store.getRuntimeScalar());

  return changed;
}

AnalysisStatus ModelStdString::modelBegin(const Instruction* call,
                                          const Function* f,
                                          const Vector<Contents>& args,
                                          Environment& env,
                                          Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents ret = getBeginIterator(conts, call, f, store, changed);
  Type* pi8 = Type::getInt8PtrTy(call->getContext());

  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, ret, pi8, call, changed);
  else
    env.push(ret);

  return changed;
}

AnalysisStatus ModelStdString::modelEnd(const Instruction* call,
                                        const Function* f,
                                        const Vector<Contents>& args,
                                        Environment& env,
                                        Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  Contents ret = getEndIterator(conts, call, f, store, changed);
  Type* pi8 = Type::getInt8PtrTy(call->getContext());

  if(hasStructRet)
    for(const auto* sret : args.at(0).getAs<Address>(call))
      store.write(sret, ret, pi8, call, changed);
  else
    env.push(ret);

  return changed;
}

AnalysisStatus ModelStdString::modelFind(const Instruction* call,
                                         const Function* f,
                                         const Vector<Contents>& args,
                                         Environment& env,
                                         Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= markContainerRead(conts, call, f, store);

  env.push(store.getRuntimeScalar());

  return changed;
}

} // namespace moya
