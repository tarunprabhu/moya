#include "ModelSTLUnorderedMultiset.h"

using namespace llvm;

namespace moya {

ModelSTLUnorderedMultiset::ModelSTLUnorderedMultiset(const std::string& fname,
                                                     FuncID fn,
                                                     const Models& ms)
    : ModelSTLUnorderedSet(fname, STDKind::STD_STL_unordered_multiset, fn, ms) {
  ;
}

} // namespace moya
