#include "ModelReadWrite.h"
#include "ModelSTLHashtable.h"
#include "Models.h"
#include "common/Diagnostics.h"
#include "common/LangConfig.h"

#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;
using namespace moya;

static std::string getIteratorName() {
  return STDConfig::getSTDName(STDKind::STD_STL_iterator_hashtable);
}

static std::string getConstIteratorName() {
  return STDConfig::getSTDName(STDKind::STD_STL_const_iterator_hashtable);
}

static std::string getBucketIteratorName() {
  return STDConfig::getSTDName(STDKind::STD_STL_iterator_bucket);
}

static std::string mkIter(const std::string& name, unsigned kind) {
  std::string s;
  raw_string_ostream ss(s);
  std::string iterator;
  switch(kind) {
  case 0:
    iterator = getIteratorName();
    break;
  case 1:
    iterator = getConstIteratorName();
    break;
  case 2:
    iterator = getBucketIteratorName();
    break;
  default:
    moya_error("Unknown kind: " << kind);
    break;
  }
  ss << iterator << "::" << name;
  return ss.str();
}

static std::string mkCompare(const std::string& op, unsigned kind) {
  std::string s;
  raw_string_ostream ss(s);
  std::string iterator;
  switch(kind) {
  case 0:
    iterator = getIteratorName();
    break;
  case 1:
    iterator = getConstIteratorName();
    break;
  case 2:
    iterator = getBucketIteratorName();
    break;
  default:
    moya_error("Unknown kind: " << kind);
    break;
  }
  ss << iterator << "::operator" << op << "(" << iterator << ")";
  return ss.str();
}

static std::string mkStdCompare(const std::string& op, unsigned kind) {
  std::string s;
  raw_string_ostream ss(s);
  std::string iterator;
  switch(kind) {
  case 0:
    iterator = getIteratorName();
    break;
  case 1:
    iterator = getConstIteratorName();
    break;
  case 2:
    iterator = getBucketIteratorName();
    break;
  default:
    moya_error("Unknown kind: " << kind);
    break;
  }
  ss << "std::operator" << op << "(" << iterator << ", " << iterator << ")";
  return ss.str();
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(
      new ModelSTLHashtable(name, STDKind::STD_STL_iterator_hashtable, fn, ms));
}

void Models::initSTLHashtableIterator(const Module& module) {
  Models& ms = *this;

  // Adding iterator comparison operators
  for(const std::string& op : {"==", "!="}) {
    for(unsigned kind : {0, 1, 2}) {
      mkModel(ms, mkCompare(op, kind), ModelSTLHashtable::IteratorCompare);
      mkModel(ms, mkStdCompare(op, kind), ModelSTLHashtable::IteratorCompare);
    }
  }

  for(unsigned k : {0, 1, 2}) {
    mkModel(ms, mkIter("operator*()", k), ModelSTLHashtable::IteratorDeref);
    mkModel(ms, mkIter("operator->()", k), ModelSTLHashtable::IteratorDeref);
    mkModel(ms, mkIter("operator++()", k), ModelSTLHashtable::IteratorSelf);
  }
  mkModel(ms, mkIter("operator++(int)", 0), ModelSTLHashtable::IteratorNew);
  mkModel(ms, mkIter("operator++(int)", 1), ModelSTLHashtable::IteratorNew);
  // mkModel(ms, mkIter("operator++(int)", 2),
  //          ModelSTLHashtable::BucketIteratorNew);
}
