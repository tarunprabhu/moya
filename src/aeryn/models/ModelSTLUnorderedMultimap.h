#ifndef MOYA_MODELS_MODEL_STL_UNORDERED_MULTIMAP_H
#define MOYA_MODELS_MODEL_STL_UNORDERED_MULTIMAP_H

#include "ModelSTLUnorderedMap.h"

namespace moya {

class ModelSTLUnorderedMultimap : public ModelSTLUnorderedMap {
public:
  ModelSTLUnorderedMultimap(const std::string&, FuncID, const Models&);

  virtual ~ModelSTLUnorderedMultimap() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_UNORDERED_MULTIMAP_H
