#ifndef MOYA_MODELS_MODEL_READ_WRITE_H
#define MOYA_MODELS_MODEL_READ_WRITE_H

#include "Model.h"

namespace moya {

class ModelReadWrite : public Model {
protected:
  Vector<Model::ArgSpec> argStatus;
  bool varargConst;

public:
  ModelReadWrite(const std::string&,
                 const std::initializer_list<int>&,
                 Model::ReturnSpec = Model::ReturnSpec::Default,
                 bool = false,
                 bool = true);
  ModelReadWrite(const std::string&, const std::initializer_list<int>&, bool);

  ModelReadWrite(const std::string&,
                 const Vector<Model::ArgSpec>&,
                 Model::ReturnSpec = Model::ReturnSpec::Default,
                 bool = false,
                 bool = true);
  ModelReadWrite(const std::string&, const Vector<Model::ArgSpec>&, bool);

  virtual ~ModelReadWrite() = default;

  bool isVarArgConst() const;
  unsigned long getNumArgs() const;
  Model::ArgSpec getArg(unsigned) const;

  virtual AnalysisStatus process(const llvm::Instruction*,
                                 const llvm::Function*,
                                 const Vector<Contents>&,
                                 const Vector<llvm::Type*>&,
                                 Environment&,
                                 Store&,
                                 const llvm::Function*) const;

public:
  static bool classof(const Model*);
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_READ_WRITE_H
