#ifndef MOYA_MODELS_MODEL_STL_QUEUE_H
#define MOYA_MODELS_MODEL_STL_QUEUE_H

#include "ModelSTLDeque.h"

namespace moya {

class ModelSTLQueue : public ModelSTLDeque {
public:
  ModelSTLQueue(const std::string&, FuncID, const Models&);

  virtual ~ModelSTLQueue() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_QUEUE_H
