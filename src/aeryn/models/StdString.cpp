#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "ModelStdString.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/LLVMUtils.h"
#include "common/LangConfig.h"
#include "common/StringUtils.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;

namespace moya {

static std::string mkOpSS(const std::string& op) {
  std::stringstream ss;

  ss << "std::operator" << op << "(std::basic_string, std::basic_string)";

  return ss.str();
}

static std::string mkOpSC(const std::string& op) {
  std::stringstream ss;

  ss << "std::operator" << op << "(std::basic_string, char*)";

  return ss.str();
}

static void
mk(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  std::stringstream ss;
  ss << "std::basic_string::" << name;
  ms.add(new ModelStdString(ss.str(), fn, ms));
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    ms.add(new ModelStdString(mkOpSS(op), ModelStdString::Compare, ms));
    ms.add(new ModelStdString(mkOpSC(op), ModelStdString::Compare, ms));
  }
}

static void initFindModels(Models& ms) {
  for(std::string find : {"find",
                          "rfind",
                          "find_first_of",
                          "find_first_not_of",
                          "find_last_of",
                          "find_last_not_of"}) {
    mk(ms, find + "(std::basic_string, unsigned long)", ModelStdString::Find);
    mk(ms,
       find + "(char*, unsigned long, unsigned long)",
       ModelStdString::Find);
    mk(ms, find + "(char*, unsigned long)", ModelStdString::Find);
    mk(ms, find + "(char, unsigned long)", ModelStdString::Find);
  }
}

static AnalysisStatus model_to_string(const ModelCustom& model,
                                      const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store,
                                      const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(call)) {
    llvm::Type* i8 = llvm::Type::getInt8Ty(f->getContext());
    const llvm::Function* caller = LLVMUtils::getFunction(call);
    for(const Address* addr : args.at(0).getAs<Address>(call)) {
      const Address* buf
          = AbstractUtils::allocate(i8, store, call, caller, CellFlags());
      store.write(buf, store.getRuntimeScalar(), i8, call, changed);
      store.write(addr, buf, i8->getPointerTo(), call, changed);
    }
    changed |= env.add(call);
  }

  return changed;
}

void Models::initStdString(const Module& module) {
  Models& ms = *this;

  mk(ms, "basic_string()", ModelStdString::ConstructorGeneric);
  mk(ms, "basic_string(allocator)", ModelStdString::ConstructorGeneric);
  mk(ms,
     "basic_string(unsigned long, allocator)",
     ModelStdString::ConstructorGeneric);
  mk(ms,
     "basic_string(unsigned long, char, allocator)",
     ModelStdString::ConstructorGeneric);
  mk(ms,
     "basic_string(std::basic_string, unsigned long, unsigned long)",
     ModelStdString::ConstructorGeneric);
  mk(ms,
     "basic_string(char*, unsigned long, allocator)",
     ModelStdString::ConstructorGeneric);
  mk(ms, "basic_string(char*, allocator)", ModelStdString::ConstructorGeneric);
  mk(ms, "basic_string(std::basic_string)", ModelStdString::ConstructorGeneric);
  mk(ms,
     "basic_string(std::initializer_list, allocator)",
     ModelStdString::ConstructorGeneric);
  mk(ms,
     "basic_string(char*, char*, allocator)",
     ModelStdString::ConstructorGeneric);
  mk(ms, "basic_string(T, T, allocator)", ModelStdString::ConstructorGeneric);

  // Destructor
  mk(ms, "~basic_string()", ModelStdString::Destructor);

  // operator=
  mk(ms, "operator=(std::basic_string)", ModelStdString::AssignGeneric);
  mk(ms, "operator=(char*)", ModelStdString::AssignGeneric);
  mk(ms, "operator=(char)", ModelStdString::AssignGeneric);
  mk(ms, "operator=(std::initializer_list)", ModelStdString::AssignGeneric);

  // assign
  mk(ms, "assign(T, T)", ModelStdString::AssignGeneric);
  mk(ms, "assign(unsigned long, char)", ModelStdString::AssignGeneric);
  mk(ms, "assign(std::basic_string)", ModelStdString::AssignGeneric);
  mk(ms,
     "assign(std::basic_string, unsigned long, unsigned long)",
     ModelStdString::AssignGeneric);
  mk(ms, "assign(char*)", ModelStdString::AssignGeneric);
  mk(ms, "assign(char*, unsigned long)", ModelStdString::AssignGeneric);
  mk(ms, "assign(char, unsigned long)", ModelStdString::AssignGeneric);
  mk(ms, "assign(std::initializer_list)", ModelStdString::AssignGeneric);

  // allocator
  mk(ms, "get_allocator()", ModelStdString::GetAllocator);

  // Element access
  mk(ms, "at(unsigned long)", ModelStdString::AccessElement);
  mk(ms, "operator[](unsigned long)", ModelStdString::AccessElement);
  mk(ms, "front()", ModelStdString::AccessElement);
  mk(ms, "back()", ModelStdString::AccessElement);
  mk(ms, "c_str()", ModelStdString::AccessElement);
  mk(ms, "data()", ModelStdString::Begin);

  // Iterators
  mk(ms, "begin()", ModelStdString::Begin);
  mk(ms, "cbegin()", ModelStdString::Begin);
  mk(ms, "end()", ModelStdString::End);
  mk(ms, "cend()", ModelStdString::End);
  mk(ms, "rbegin()", ModelStdString::Begin);
  mk(ms, "crbegin()", ModelStdString::Begin);
  mk(ms, "rend()", ModelStdString::End);
  mk(ms, "crend()", ModelStdString::End);

  // Capacity
  mk(ms, "empty()", ModelStdString::Size);
  mk(ms, "size()", ModelStdString::Size);
  mk(ms, "length()", ModelStdString::Size);
  mk(ms, "max_size()", ModelStdString::Size);
  mk(ms, "capacity()", ModelStdString::Size);
  mk(ms, "reserve(unsigned long)", ModelStdString::Resize);
  mk(ms, "shrink_to_fit()", ModelStdString::Resize);

  // Modifiers
  mk(ms, "clear()", ModelStdString::Clear);

  mk(ms,
     "insert(unsigned long, unsigned long, char)",
     ModelStdString::AssignGeneric);
  mk(ms, "insert(unsigned long, char*)", ModelStdString::AssignGeneric);
  mk(ms,
     "insert(unsigned long, char*, unsigned long)",
     ModelStdString::AssignGeneric);
  mk(ms,
     "insert(unsigned long, std::basic_string)",
     ModelStdString::AssignGeneric);
  mk(ms,
     "insert(unsigned long, std::basic_string, unsigned long, unsigned long)",
     ModelStdString::AssignGeneric);
  mk(ms, "insert(iterator, char)", ModelStdString::InsertGeneric);
  mk(ms,
     "insert(iterator, unsigned long, char)",
     ModelStdString::InsertGeneric);
  mk(ms, "insert(iterator, iterator, iterator)", ModelStdString::InsertGeneric);
  mk(ms,
     "insert(iterator, std::initializer_list)",
     ModelStdString::InsertGeneric);

  mk(ms, "erase(unsigned long, unsigned long)", ModelStdString::EraseGeneric);
  mk(ms, "erase(iterator)", ModelStdString::EraseElementAt);
  mk(ms, "erase(iterator, iterator)", ModelStdString::EraseRange);

  mk(ms, "push_back(char)", ModelStdString::AddElement);
  mk(ms, "pop_back()", ModelStdString::RemoveElement);

  mk(ms, "append(unsigned long, char)", ModelStdString::AssignGeneric);
  mk(ms, "append(std::basic_string)", ModelStdString::AssignGeneric);
  mk(ms,
     "append(std::basic_string, unsigned long, unsigned long)",
     ModelStdString::AssignGeneric);
  mk(ms, "append(char*, unsigned long)", ModelStdString::AssignGeneric);
  mk(ms, "append(char*)", ModelStdString::AssignGeneric);
  mk(ms, "append(std::initializer_list)", ModelStdString::AssignInitList);

  mk(ms, "operator+=(std::basic_string)", ModelStdString::AssignGeneric);
  mk(ms, "operator+=(char*)", ModelStdString::AssignGeneric);
  mk(ms, "operator+=(char)", ModelStdString::AssignGeneric);
  mk(ms, "operator+=(std::initializer_list)", ModelStdString::AssignGeneric);

  mk(ms, "compare(std::basic_string)", ModelStdString::Compare);
  mk(ms,
     "compare(unsigned long, unsigned long, std::basic_string)",
     ModelStdString::Compare);
  mk(ms,
     "compare(unsigned long, unsigned long, std::basic_string, unsigned long)",
     ModelStdString::Compare);
  mk(ms,
     "compare(unsigned long, unsigned long, std::basic_string, unsigned long, "
     "unsigned long)",
     ModelStdString::Compare);
  mk(ms, "compare(char*)", ModelStdString::Compare);
  mk(ms,
     "compare(unsigned long, unsigned long, char*)",
     ModelStdString::Compare);
  mk(ms,
     "compare(unsigned long, unsigned long, char*, unsigned long)",
     ModelStdString::Compare);

  mk(ms,
     "replace(unsigned long, unsigned long, std::basic_string)",
     ModelStdString::AssignGeneric);
  mk(ms,
     "replace(iterator, iterator, std::basic_string)",
     ModelStdString::AssignGeneric);
  mk(ms,
     "replace(unsigned long, unsigned long, std::basic_string, unsigned long, "
     "unsigned long)",
     ModelStdString::AssignGeneric);
  mk(ms,
     "replace(unsigned long, unsigned long, char*, unsigned long)",
     ModelStdString::AssignGeneric);
  mk(ms,
     "replace(unsigned long, unsigned long, char*)",
     ModelStdString::AssignGeneric);
  mk(ms, "replace(iterator, iterator, char*)", ModelStdString::AssignGeneric);
  mk(ms,
     "replace(iterator, iterator, char*, unsigned long)",
     ModelStdString::AssignGeneric);
  mk(ms,
     "replace(iterator, iterator, iterator, iterator)",
     ModelStdString::AssignGeneric);
  mk(ms,
     "replace(unsigned long, unsigned long, unsigned long, char)",
     ModelStdString::AssignGeneric);
  mk(ms,
     "replace(iterator, iterator, unsigned long, char)",
     ModelStdString::AssignGeneric);
  mk(ms,
     "replace(iterator, iterator, std::initializer_list)",
     ModelStdString::AssignGeneric);

  mk(ms, "substr(unsigned long, unsigned long)", ModelStdString::Begin);

  mk(ms,
     "copy(char *, unsigned long, unsigned long)",
     ModelStdString::CopyToCstr);

  mk(ms, "resize(unsigned long)", ModelStdString::Resize);
  // This overload of resize also assigns to the modified string, so it
  // behaves somewhat like assign
  mk(ms, "resize(unsigned long, char)", ModelStdString::AssignGeneric);

  mk(ms, "swap(std::basic_string)", ModelStdString::Swap);

  mk(ms,
     "operator+(std::basic_string, std::basic_string)",
     ModelStdString::AssignGeneric);
  ms.add(new ModelReadWrite(
      "std::operator+(std::basic_string, std::basic_string)", {0, 1, 1}, true));
  ms.add(new ModelReadWrite(
      "std::operator+(std::basic_string, char*)", {0, 1, 1}, true));
  ms.add(new ModelReadWrite(
      "std::operator+(char*, std::basic_string)", {0, 1, 1}, true));
  ms.add(new ModelReadWrite(
      "std::operator+(char, std::basic_string)", {0, 1, 1}, true));
  ms.add(new ModelReadWrite(
      "std::operator+(std::basic_string, char)", {0, 1, 1}, true));

  initFindModels(ms);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);

  ms.add(new ModelReadOnly("std::stof(std::basic_string, unsigned long*)"));
  ms.add(new ModelReadOnly("std::stod(std::basic_string, unsigned long*)"));
  ms.add(new ModelReadOnly("std::stold(std::basic_string, unsigned long*)"));
  ms.add(
      new ModelReadOnly("std::stoi(std::basic_string, unsigned long*, int)"));
  ms.add(
      new ModelReadOnly("std::stol(std::basic_string, unsigned long*, int)"));
  ms.add(
      new ModelReadOnly("std::stoll(std::basic_string, unsigned long*, int)"));
  ms.add(
      new ModelReadOnly("std::stoul(std::basic_string, unsigned long*, int)"));
  ms.add(
      new ModelReadOnly("std::stoull(std::basic_string, unsigned long*, int)"));

  for(const char* type : {"int",
                          "long",
                          "long long",
                          "unsigned",
                          "unsigned long",
                          "unsigned long long",
                          "float",
                          "double",
                          "long double"}) {
    std::stringstream ss;
    ss << "std::to_string(" << type << ")";
    ms.add(new ModelCustom(ss.str(), model_to_string));
  }
}

} // namespace moya
