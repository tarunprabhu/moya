#include "ModelCustom.h"
#include "common/LLVMUtils.h"

using namespace llvm;

namespace moya {

ModelCustom::ModelCustom(const std::string& fname,
                         const ModelCustom::ProcessFn& fn,
                         bool onlyMarkWrites,
                         bool markArgsRead)
    : Model(fname,
            Model::Kind::CustomKind,
            Model::ReturnSpec::Default,
            onlyMarkWrites),
      markArgsRead(markArgsRead), processFn(fn) {
  ;
}

const ModelCustom::ProcessFn& ModelCustom::getProcessFn() const {
  return processFn;
}

AnalysisStatus ModelCustom::process(const Instruction* inst,
                                    const Function* f,
                                    const Vector<Contents>& args,
                                    const Vector<Type*>& argTys,
                                    Environment& env,
                                    Store& store,
                                    const Function* caller) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(markArgsRead)
    for(unsigned i = f->hasStructRetAttr(); i < args.size(); i++)
      changed |= markRead(args.at(i), argTys.at(i), env, store, inst);
  changed |= processFn(*this, inst, f, args, env, store, caller);

  return changed;
}

bool ModelCustom::classof(const Model* model) {
  return model->getKind() == Model::Kind::CustomKind;
}

} // namespace moya
