#include "ModelSTLReverseIterator.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <sstream>

using namespace llvm;

namespace moya {

ModelSTLReverseIterator::ModelSTLReverseIterator(const std::string& fname,
                                                 STDKind cont,
                                                 STDKind iter,
                                                 STDKind citer,
                                                 FuncID func,
                                                 const Models& models)
    : ModelSTLContainer(fname, cont, iter, citer, func, models) {
  ;
}

ModelSTLReverseIterator::ModelSTLReverseIterator(const std::string& fname,
                                                 FuncID func,
                                                 const Models& models)
    : ModelSTLReverseIterator(fname,
                              STDKind::STD_STL_iterator_reverse,
                              STDKind::STD_None,
                              STDKind::STD_None,
                              func,
                              models) {
  ;
}

Addresses
ModelSTLReverseIterator::getIteratorDeref(const Addresses& iters,
                                          const Instruction* call,
                                          const Function* f,
                                          Store& store,
                                          AnalysisStatus& changed) const {
  Addresses ret;
  PointerType* pBaseTy = nullptr;
  if(Metadata::hasSTLBaseType(f))
    pBaseTy = Metadata::getSTLBaseType(f)->getPointerTo();
  else
    pBaseTy = Metadata::getSTLValueType(f)->getPointerTo();
  unsigned long offset = Metadata::getSTLValueOffset(f);

  for(const auto* ptr : store.readAs<Address>(iters, pBaseTy, call, changed))
    ret.insert(store.make(ptr, offset));
  return ret;
}

Addresses
ModelSTLReverseIterator::getIteratorNew(const Addresses& iters,
                                        const Instruction* call,
                                        const Function* f,
                                        Store& store,
                                        AnalysisStatus& changed) const {
  return iters;
}

} // namespace moya
