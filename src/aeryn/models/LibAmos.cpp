#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>

#include "aeryn/AbstractUtils.h"
#include "common/LLVMUtils.h"
#include "common/StringUtils.h"
#include "common/Vector.h"

using llvm::dyn_cast;

using namespace moya;

// This is just a workaround to get PlasComCM to work.
// There is no way I am going to see if Flang can deal with Fortran 77

void Models::initLibAmos(const llvm::Module& module) {
  add(new ModelReadWrite("zbesh_", {1, 1, 1, 1, 1, 1, 0, 0, 0, 0}));
}
