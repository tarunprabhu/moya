#ifndef MOYA_MODELS_MODEL_STL_UNORDERED_MAP_H
#define MOYA_MODELS_MODEL_STL_UNORDERED_MAP_H

#include "ModelSTLHashtable.h"

namespace moya {

class ModelSTLUnorderedMap : public ModelSTLHashtable {
protected:
  virtual ModelFn modelAccessElement;
  virtual ModelFn modelEmplaceMapped;

protected:
  ModelSTLUnorderedMap(const std::string&, STDKind, FuncID, const Models&);

public:
  ModelSTLUnorderedMap(const std::string&, FuncID, const Models&);

  virtual ~ModelSTLUnorderedMap() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_UNORDERED_MAP_H
