#ifndef MOYA_MODELS_MODEL_STL_FORWARD_LIST_H
#define MOYA_MODELS_MODEL_STL_FORWARD_LIST_H

#include "ModelSTLContainer.h"

namespace moya {

class ModelSTLForwardList : public ModelSTLContainer {
protected:
  virtual AnalysisStatus updateContainerPointers(const Address*,
                                                 const Addresses&,
                                                 const llvm::Instruction*,
                                                 const llvm::Function*,
                                                 Store&) const;

  virtual AnalysisStatus updateNodePointers(const Address*,
                                            const Addresses&,
                                            const llvm::Instruction*,
                                            const llvm::Function*,
                                            Store&) const;

  virtual Contents getBeginIterator(const Addresses&,
                                    const llvm::Instruction*,
                                    const llvm::Function*,
                                    Store&,
                                    AnalysisStatus&) const;
  virtual IteratorFn getIteratorNode;
  virtual IteratorFn getIteratorDeref;
  virtual IteratorFn getIteratorNew;

  virtual ModelFn modelMerge;
  virtual ModelFn modelSplice;

protected:
  ModelSTLForwardList(const std::string&,
                      STDKind,
                      STDKind,
                      STDKind,
                      FuncID,
                      const Models&);

public:
  ModelSTLForwardList(const std::string&, FuncID, const Models&);

  virtual ~ModelSTLForwardList() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_FORWARD_LIST_H
