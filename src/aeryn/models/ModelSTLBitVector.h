#ifndef MOYA_MODELS_MODEL_STL_BIT_VECTOR_H
#define MOYA_MODELS_MODEL_STL_BIT_VECTOR_H

#include "ModelSTLVector.h"

namespace moya {

class ModelSTLBitVector : public ModelSTLVector {
protected:
  virtual AnalysisStatus updateContainerPointers(const Address*,
                                                 const Addresses&,
                                                 const llvm::Instruction*,
                                                 const llvm::Function*,
                                                 Store&) const;

  virtual Contents getBeginIterator(const Addresses&,
                                    const llvm::Instruction*,
                                    const llvm::Function*,
                                    Store&,
                                    AnalysisStatus&) const;

  virtual Contents getEndIterator(const Addresses&,
                                  const llvm::Instruction*,
                                  const llvm::Function*,
                                  Store&,
                                  AnalysisStatus&) const;

  virtual ModelFn modelAccessElement;
  virtual ModelFn modelIteratorSelf;
  virtual ModelFn modelIteratorDeref;

public:
  ModelSTLBitVector(const std::string&, FuncID, const Models&);

  virtual ~ModelSTLBitVector() = default;
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_STL_BIT_VECTOR_H
