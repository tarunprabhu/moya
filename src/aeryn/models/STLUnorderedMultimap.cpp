#include "ModelSTLUnorderedMultimap.h"
#include "Models.h"
#include "common/LangConfig.h"

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_unordered_multimap);
}

static std::string mk(std::string name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "std::operator" << op << "(" << getContainerName() << ", "
       << getContainerName() << ")";
    ms.add(new ModelSTLUnorderedMultimap(
        ss.str(), ModelSTLUnorderedMultimap::Compare, ms));
  }
}

static void
mkModel(Models& ms, std::string name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLUnorderedMultimap(name, fn, ms));
}

void Models::initSTLUnorderedMultimap(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms,
          mk("unordered_multimap()"),
          ModelSTLUnorderedMultimap::ConstructorEmpty);
  mkModel(ms,
          mk("unordered_multimap(unsigned long, std::hash, std::equal_to, "
             "allocator)"),
          ModelSTLUnorderedMultimap::ConstructorEmpty);
  mkModel(ms,
          mk("unordered_multimap(T, T, unsigned long, std::hash, "
             "std::equal_to, allocator)"),
          ModelSTLUnorderedMultimap::ConstructorRange);
  mkModel(ms,
          mk("unordered_set(T, T, std::hash, "
             "std::equal_to, allocator)"),
          ModelSTLUnorderedMultimap::ConstructorRange);
  mkModel(ms,
          mk("unordered_multimap(std::unordered_multimap)"),
          ModelSTLUnorderedMultimap::ConstructorCopy);
  mkModel(ms,
          mk("unordered_multimap(std::initializer_list, unsigned long, "
             "std::hash, std::equal_to, allocator)"),
          ModelSTLUnorderedMultimap::ConstructorInitList);

  // Destructor
  mkModel(
      ms, mk("~unordered_multimap()"), ModelSTLUnorderedMultimap::Destructor);

  // operator =
  mkModel(ms,
          mk("operator=(std::unordered_multimap)"),
          ModelSTLUnorderedMultimap::OperatorContainer);
  mkModel(ms,
          mk("operator=(std::initializer_list)"),
          ModelSTLUnorderedMultimap::OperatorInitList);

  // allocator
  mkModel(ms, mk("get_allocator()"), ModelSTLUnorderedMultimap::GetAllocator);

  // Access
  mkModel(ms, mk("at(*)"), ModelSTLUnorderedMultimap::AccessElement);
  mkModel(ms, mk("operator[](*)"), ModelSTLUnorderedMultimap::AccessElement);

  // Iterators
  mkModel(ms, mk("begin()"), ModelSTLUnorderedMultimap::Begin);
  mkModel(ms, mk("cbegin()"), ModelSTLUnorderedMultimap::Begin);
  mkModel(ms, mk("end()"), ModelSTLUnorderedMultimap::End);
  mkModel(ms, mk("cend()"), ModelSTLUnorderedMultimap::End);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLUnorderedMultimap::Size);
  mkModel(ms, mk("size()"), ModelSTLUnorderedMultimap::Size);
  mkModel(ms, mk("max_size()"), ModelSTLUnorderedMultimap::Size);

  // Modifiers
  mkModel(ms, mk("clear()"), ModelSTLUnorderedMultimap::Clear);

  mkModel(ms, mk("emplace(...)"), ModelSTLUnorderedMap::EmplaceElement);
  mkModel(ms,
          mk("emplace_hint(iterator, ...)"),
          ModelSTLUnorderedMap::EmplaceElementAt);

  mkModel(ms, mk("insert(*)"), ModelSTLUnorderedMultimap::InsertElement);
  mkModel(ms, mk("insert(T)"), ModelSTLUnorderedMultimap::InsertElement);
  mkModel(ms,
          mk("insert(iterator, *)"),
          ModelSTLUnorderedMultimap::InsertElementAt);
  mkModel(ms, mk("insert(T, T)"), ModelSTLUnorderedMultimap::InsertRange);
  mkModel(ms,
          mk("insert(std::initializer_list)"),
          ModelSTLUnorderedMultimap::InsertInitList);

  mkModel(ms, mk("erase(*)"), ModelSTLUnorderedMultimap::EraseElement);
  mkModel(ms, mk("erase(iterator)"), ModelSTLUnorderedMultimap::EraseElementAt);
  mkModel(ms,
          mk("erase(iterator, iterator)"),
          ModelSTLUnorderedMultimap::EraseRange);

  mkModel(
      ms, mk("swap(std::unordered_multimap)"), ModelSTLUnorderedMultimap::Swap);

  // Lookup
  mkModel(ms, mk("count(*)"), ModelSTLUnorderedMultimap::Size);
  mkModel(ms, mk("find(*)"), ModelSTLUnorderedMultimap::Find);
  mkModel(ms, mk("equal_range(*)"), ModelSTLUnorderedMultimap::FindRange);
  mkModel(ms, mk("lower_bound(*)"), ModelSTLUnorderedMultimap::FindRange);
  mkModel(ms, mk("upper_bound(*)"), ModelSTLUnorderedMultimap::FindRange);

  // Buckets
  mkModel(
      ms, mk("begin(unsigned long)"), ModelSTLUnorderedMultimap::BucketBegin);
  mkModel(
      ms, mk("cbegin(unsigned long)"), ModelSTLUnorderedMultimap::BucketBegin);
  mkModel(ms, mk("end(unsigned long)"), ModelSTLUnorderedMultimap::BucketEnd);
  mkModel(ms, mk("cend(unsigned long)"), ModelSTLUnorderedMultimap::BucketEnd);
  mkModel(ms, mk("bucket_count()"), ModelSTLUnorderedMultimap::Buckets);
  mkModel(ms, mk("max_bucket_count()"), ModelSTLUnorderedMultimap::Buckets);
  mkModel(
      ms, mk("bucket_size(unsigned long)"), ModelSTLUnorderedMultimap::Buckets);
  mkModel(ms, mk("bucket(*)"), ModelSTLUnorderedMultimap::Buckets);

  // Hash policy
  mkModel(ms, mk("load_factor()"), ModelSTLUnorderedMultimap::GetLoadFactor);
  mkModel(
      ms, mk("max_load_factor()"), ModelSTLUnorderedMultimap::GetLoadFactor);
  mkModel(ms,
          mk("max_load_factor(float)"),
          ModelSTLUnorderedMultimap::SetLoadFactor);
  mkModel(ms, mk("rehash(unsigned long)"), ModelSTLUnorderedMultimap::Resize);
  mkModel(ms, mk("reserve(unsigned long)"), ModelSTLUnorderedMultimap::Resize);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);
}
