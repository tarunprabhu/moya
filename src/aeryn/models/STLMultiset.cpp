#include "ModelSTLMultiset.h"
#include "Models.h"
#include "common/LangConfig.h"

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_multiset);
}

static std::string mk(std::string name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "std::operator" << op << "(" << getContainerName() << ", "
       << getContainerName() << ")";
    ms.add(new ModelSTLMultiset(ss.str(), ModelSTLMultiset::Compare, ms));
  }
}

static void
mkModel(Models& ms, std::string name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLMultiset(name, fn, ms));
}

void Models::initSTLMultiset(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms, mk("multiset()"), ModelSTLMultiset::ConstructorEmpty);
  mkModel(ms,
          mk("multiset(std::less, allocator)"),
          ModelSTLMultiset::ConstructorEmpty);
  mkModel(
      ms, mk("multiset(T, T, allocator)"), ModelSTLMultiset::ConstructorRange);
  mkModel(ms,
          mk("multiset(T, T, std::less, allocator)"),
          ModelSTLMultiset::ConstructorRange);
  mkModel(ms, mk("multiset(std::multiset)"), ModelSTLMultiset::ConstructorCopy);
  mkModel(ms,
          mk("multiset(std::initializer_list, std::less, allocator)"),
          ModelSTLMultiset::ConstructorInitList);

  // Destructor
  mkModel(ms, mk("~multiset()"), ModelSTLMultiset::Destructor);

  // operator =
  mkModel(
      ms, mk("operator=(std::multiset)"), ModelSTLMultiset::OperatorContainer);
  mkModel(ms,
          mk("operator=(std::initializer_list)"),
          ModelSTLMultiset::OperatorInitList);

  // allocator
  mkModel(ms, mk("get_allocator()"), ModelSTLMultiset::GetAllocator);

  // Iterators
  mkModel(ms, mk("begin()"), ModelSTLMultiset::Begin);
  mkModel(ms, mk("cbegin()"), ModelSTLMultiset::Begin);
  mkModel(ms, mk("end()"), ModelSTLMultiset::End);
  mkModel(ms, mk("cend()"), ModelSTLMultiset::End);
  mkModel(ms, mk("rbegin()"), ModelSTLMultiset::Begin);
  mkModel(ms, mk("crbegin()"), ModelSTLMultiset::Begin);
  mkModel(ms, mk("rend()"), ModelSTLMultiset::End);
  mkModel(ms, mk("crend()"), ModelSTLMultiset::End);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLMultiset::Size);
  mkModel(ms, mk("size()"), ModelSTLMultiset::Size);
  mkModel(ms, mk("max_size()"), ModelSTLMultiset::Size);

  // Modifiers
  mkModel(ms, mk("clear()"), ModelSTLMultiset::Clear);

  mkModel(ms, mk("emplace(...)"), ModelSTLMultiset::EmplaceElement);
  mkModel(ms,
          mk("emplace_hint(iterator, ...)"),
          ModelSTLMultiset::EmplaceElementAt);

  mkModel(ms, mk("insert(*)"), ModelSTLMultiset::InsertElement);
  mkModel(ms, mk("insert(T)"), ModelSTLMultiset::InsertElement);
  mkModel(ms, mk("insert(T, T)"), ModelSTLMultiset::InsertRange);
  mkModel(ms, mk("insert(iterator, *)"), ModelSTLMultiset::InsertElementAt);
  mkModel(
      ms, mk("insert(std::initializer_set)"), ModelSTLMultiset::InsertInitList);

  mkModel(ms, mk("erase(*)"), ModelSTLMultiset::EraseElement);
  mkModel(ms, mk("erase(iterator)"), ModelSTLMultiset::EraseElementAt);
  mkModel(ms, mk("erase(iterator, iterator)"), ModelSTLMultiset::EraseRange);

  mkModel(ms, mk("swap(std::multiset)"), ModelSTLMultiset::Swap);

  // Lookup
  mkModel(ms, mk("count(*)"), ModelSTLMultiset::Size);
  mkModel(ms, mk("find(*)"), ModelSTLMultiset::Find);
  mkModel(ms, mk("equal_range(*)"), ModelSTLMultiset::FindRange);
  mkModel(ms, mk("lower_bound(*)"), ModelSTLMultiset::FindRange);
  mkModel(ms, mk("upper_bound(*)"), ModelSTLMultiset::FindRange);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);
}
