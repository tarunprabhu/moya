#ifndef MOYA_MODELS_MODEL_READ_ONLY_H
#define MOYA_MODELS_MODEL_READ_ONLY_H

#include "Model.h"

namespace moya {

class ModelReadOnly : public Model {
private:
public:
  ModelReadOnly(const std::string&,
                Model::ReturnSpec = Model::ReturnSpec::Default);

  virtual ~ModelReadOnly() = default;

  virtual AnalysisStatus process(const llvm::Instruction*,
                                 const llvm::Function*,
                                 const Vector<Contents>&,
                                 const Vector<llvm::Type*>&,
                                 Environment&,
                                 Store&,
                                 const llvm::Function*) const;

public:
  static bool classof(const Model*);
};

} // namespace moya

#endif // MOYA_MODELS_MODEL_READ_ONLY_H
