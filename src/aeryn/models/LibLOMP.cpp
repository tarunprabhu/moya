#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/CellFlags.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

using namespace llvm;
using namespace moya;

static AnalysisStatus modelForkCall(const ModelCustom& model,
                                    const Instruction* call,
                                    const Function* f,
                                    const Vector<Contents>& args,
                                    Environment& env,
                                    Store& store,
                                    const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* i32 = Type::getInt32Ty(caller->getContext());
  if(not env.contains(call)) {
    for(const auto* code :
        store.readAs<ContentCode>(args.at(0), nullptr, call, changed)) {
      if(const Function* g = code->getFunction()) {
        for(unsigned i = 0; i < 2; i++) {
          const Argument* arg = LLVMUtils::getArgument(g, i);
          const Address* argAddr
              = store.allocate(i32, CellFlags::LibAlloc, true);
          store.write(
              env.lookupArg(arg), argAddr, arg->getType(), call, changed);
        }
        for(unsigned i = 3; i < args.size(); i++)
          store.write(env.lookupArg(LLVMUtils::getArgument(g, i - 1)),
                      args.at(i),
                      nullptr,
                      call,
                      changed);
      }
    }
    env.add(call, Contents());
  }

  return changed;
}

void Models::initLibLOMP(const Module& module) {
  add(new ModelReadOnly("__kmpc_begin"));
  add(new ModelReadOnly("__kmpc_end"));

  add(new ModelCustom("__kmpc_fork_call", modelForkCall));
  add(new ModelReadOnly("__kmpc_push_num_threads"));
  add(new ModelReadOnly("__kmpc_push_num_teams"));
  add(new ModelReadOnly("__kmpc_fork_teams"));
  add(new ModelReadOnly("__kmpc_serialized_parallel"));
  add(new ModelReadOnly("__kmpc_end_serialized_parallel"));

  add(new ModelReadOnly("__kmpc_global_thread_num"));
  add(new ModelReadOnly("__kmpc_global_num_threads"));
  add(new ModelReadOnly("__kmpc_bound_thread_num"));
  add(new ModelReadOnly("__kmpc_bound_num_threads"));
  add(new ModelReadOnly("__kmpc_in_parallel"));

  add(new ModelReadOnly("__kmpc_master"));
  add(new ModelReadOnly("__kmpc_end_master"));
  add(new ModelReadOnly("__kmpc_ordered"));
  add(new ModelReadOnly("__kmpc_end_ordered"));
  add(new ModelReadOnly("__kmpc_critical"));
  add(new ModelReadOnly("__kmpc_end_critical"));
  add(new ModelReadOnly("__kmpc_single"));
  add(new ModelReadOnly("__kmpc_end_single"));

  add(new ModelReadOnly("__kmpc_for_static_fini"));
  add(new ModelReadOnly("__kmpc_dispatch_init_4"));
  add(new ModelReadOnly("__kmpc_dispatch_init_4u"));
  add(new ModelReadOnly("__kmpc_dispatch_init_8"));
  add(new ModelReadOnly("__kmpc_dispatch_init_8u"));
  add(new ModelReadOnly("__kmpc_dist_dispatch_init_4"));
  add(new ModelReadWrite("__kmpc_dispatch_next_4", {1, 1, 0, 0, 0, 0}));
  add(new ModelReadWrite("__kmpc_dispatch_next_4u", {1, 1, 0, 0, 0, 0}));
  add(new ModelReadWrite("__kmpc_dispatch_next_8", {1, 1, 0, 0, 0, 0}));
  add(new ModelReadWrite("__kmpc_dispatch_next_8u", {1, 1, 0, 0, 0, 0}));
  add(new ModelReadOnly("__kmpc_dispatch_fini_4"));
  add(new ModelReadOnly("__kmpc_dispatch_fini_8"));
  add(new ModelReadOnly("__kmpc_dispatch_fini_4u"));
  add(new ModelReadOnly("__kmpc_dispatch_fini_8u"));
  add(new ModelReadWrite("__kmpc_for_static_init_4",
                         {1, 1, 1, 0, 0, 0, 0, 1, 1}));
  add(new ModelReadWrite("__kmpc_for_static_init_4u",
                         {1, 1, 1, 0, 0, 0, 0, 1, 1}));
  add(new ModelReadWrite("__kmpc_for_static_init_8",
                         {1, 1, 1, 0, 0, 0, 0, 1, 1}));
  add(new ModelReadWrite("__kmpc_for_static_init_8u",
                         {1, 1, 1, 0, 0, 0, 0, 1, 1}));
  add(new ModelReadWrite("__kmpc_dist_for_static_init_4",
                         {1, 1, 1, 0, 0, 0, 0, 0, 1, 1}));
  add(new ModelReadWrite("__kmpc_dist_for_static_init_4u",
                         {1, 1, 1, 0, 0, 0, 0, 0, 1, 1}));
  add(new ModelReadWrite("__kmpc_dist_for_static_init_8",
                         {1, 1, 1, 0, 0, 0, 0, 0, 1, 1}));
  add(new ModelReadWrite("__kmpc_dist_for_static_init_8u",
                         {1, 1, 1, 0, 0, 0, 0, 0, 1, 1}));
  add(new ModelReadWrite("__kmpc_team_static_init_4",
                         {1, 1, 1, 0, 0, 0, 0, 1, 1}));
  add(new ModelReadWrite("__kmpc_team_static_init_4u",
                         {1, 1, 1, 0, 0, 0, 0, 1, 1}));
  add(new ModelReadWrite("__kmpc_team_static_init_8",
                         {1, 1, 1, 0, 0, 0, 0, 1, 1}));
  add(new ModelReadWrite("__kmpc_team_static_init_8u",
                         {1, 1, 1, 0, 0, 0, 0, 1, 1}));

  add(new ModelReadOnly("__kmpc_flush"));
  add(new ModelReadOnly("__kmpc_barrier"));
  add(new ModelReadOnly("__kmpc_barrier_master"));
  add(new ModelReadOnly("__kmpc_end_barrier_master"));
  add(new ModelReadOnly("__kmpc_barrier_master_nowait"));
  add(new ModelReadWrite("__kmpc_reduce_nowait", {1, 1, 1, 1, 1, 1, 0}));
  add(new ModelReadWrite("__kmpc_end_reduce_nowait", {1, 1, 0}));
  add(new ModelReadWrite("__kmpc_reduce", {1, 1, 1, 1, 1, 1, 0}));
  add(new ModelReadWrite("__kmpc_end_reduce", {1, 1, 0}));

  add(new ModelReadOnly("__kmpc_copyprivate"));
  add(new ModelReadOnly("__kmpc_threadprivate_register"));
  add(new ModelReadOnly("__kmpc_threadprivate_cached"));
  add(new ModelReadOnly("__kmpc_threadprivate_register_vec"));

  add(new ModelReadOnly("__kmpc_omp_task_with_deps"));
  add(new ModelReadOnly("__kmpc_omp_wait_deps"));
}
