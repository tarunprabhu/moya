#include "ModelSTLMap.h"
#include "Models.h"
#include "common/LangConfig.h"

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_map);
}

static std::string mk(const std::string& name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "std::operator" << op << "(" << getContainerName() << ", "
       << getContainerName() << ")";
    ms.add(new ModelSTLMap(ss.str(), ModelSTLMap::Compare, ms));
  }
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLMap(name, fn, ms));
}

void Models::initSTLMap(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms, mk("map()"), ModelSTLMap::ConstructorEmpty);
  mkModel(ms, mk("map(std::less, allocator)"), ModelSTLMap::ConstructorEmpty);
  mkModel(ms, mk("map(T, T)"), ModelSTLMap::ConstructorRange);
  mkModel(ms, mk("map(T, T, allocator)"), ModelSTLMap::ConstructorRange);
  mkModel(
      ms, mk("map(T, T, std::less, allocator)"), ModelSTLMap::ConstructorRange);
  mkModel(ms, mk("map(std::map)"), ModelSTLMap::ConstructorCopy);
  mkModel(ms,
          mk("map(std::initializer_list, std::less, allocator)"),
          ModelSTLMap::ConstructorInitList);

  // Destructor
  mkModel(ms, mk("~map()"), ModelSTLMap::Destructor);

  // operator =
  mkModel(ms, mk("operator=(std::map)"), ModelSTLMap::OperatorContainer);
  mkModel(ms,
          mk("operator=(std::initializer_list)"),
          ModelSTLMap::OperatorInitList);

  // allocator
  mkModel(ms, mk("get_allocator()"), ModelSTLMap::GetAllocator);

  // Access
  mkModel(ms, mk("at(*)"), ModelSTLMap::AccessElement);
  mkModel(ms, mk("operator[](*)"), ModelSTLMap::EmplaceMapped);

  // Iterators
  mkModel(ms, mk("begin()"), ModelSTLMap::Begin);
  mkModel(ms, mk("cbegin()"), ModelSTLMap::Begin);
  mkModel(ms, mk("end()"), ModelSTLMap::End);
  mkModel(ms, mk("cend()"), ModelSTLMap::End);
  mkModel(ms, mk("rbegin()"), ModelSTLMap::Begin);
  mkModel(ms, mk("crbegin()"), ModelSTLMap::Begin);
  mkModel(ms, mk("rend()"), ModelSTLMap::End);
  mkModel(ms, mk("crend()"), ModelSTLMap::End);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLMap::Size);
  mkModel(ms, mk("size()"), ModelSTLMap::Size);
  mkModel(ms, mk("max_size()"), ModelSTLMap::Size);

  // Modifiers
  mkModel(ms, mk("clear()"), ModelSTLMap::Clear);

  mkModel(ms, mk("emplace(...)"), ModelSTLMap::EmplaceElement);
  mkModel(ms, mk("emplace_hint(iterator, ...)"), ModelSTLMap::EmplaceElementAt);

  mkModel(ms, mk("insert(std::pair)"), ModelSTLMap::InsertElement);
  mkModel(ms, mk("insert(T)"), ModelSTLMap::InsertElement);
  mkModel(ms, mk("insert(iterator, *)"), ModelSTLMap::InsertElementAt);
  mkModel(ms, mk("insert(T, T)"), ModelSTLMap::InsertRange);
  mkModel(ms, mk("insert(std::initializer_list)"), ModelSTLMap::InsertInitList);

  mkModel(ms, mk("erase(*)"), ModelSTLMap::EraseElement);
  mkModel(ms, mk("erase(iterator)"), ModelSTLMap::EraseElementAt);
  mkModel(ms, mk("erase(iterator, iterator)"), ModelSTLMap::EraseRange);

  mkModel(ms, mk("swap(std::map)"), ModelSTLMap::Swap);

  // Lookup
  mkModel(ms, mk("count(*)"), ModelSTLMap::Size);
  mkModel(ms, mk("find(*)"), ModelSTLMap::Find);
  mkModel(ms, mk("equal_range(*)"), ModelSTLMap::FindRange);
  mkModel(ms, mk("lower_bound(*)"), ModelSTLMap::FindRange);
  mkModel(ms, mk("upper_bound(*)"), ModelSTLMap::FindRange);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);
}
