#include "Models.h"
#include "ModelReadOnly.h"
#include "common/AerynConfig.h"
#include "common/Diagnostics.h"
#include "common/Metadata.h"
#include "common/Vector.h"

using namespace llvm;

namespace moya {

Models::Models(const Module& module) {
  initLibM(module);
  initLibLLVM(module);
  initLibCppABI(module);
  initLibOpenMP(module);
  initLibMPI(module);
  initLibOpenMPI(module);
  initLibC(module);
  initLibMoya(module);
  initLibLOMP(module);
  initLibGOMP(module);
  initLibAtomic(module);
  initLibPThread(module);
  initLibHWLoc(module);
  initSysCalls(module);
  initLibDL(module);
  initLibZlib(module);
  initLibLapack(module);
  initLibCurses(module);
  initLibCantera(module);
  initLibCharm(module);
  initLibHDF5(module);
  initLibCuda(module);
  initLibFlang(module);

  // Workarounds
  initLibIX(module);
  initLibAmos(module);
  initLibFL2C(module);

  initSTLArray(module);
  initSTLVector(module);
  initSTLBitVector(module);
  initSTLDeque(module);
  initSTLForwardList(module);
  initSTLList(module);
  initSTLSet(module);
  initSTLMultiset(module);
  initSTLMap(module);
  initSTLMultimap(module);
  initSTLUnorderedSet(module);
  initSTLUnorderedMultiset(module);
  initSTLUnorderedMap(module);
  initSTLUnorderedMultimap(module);
  initSTLStack(module);
  initSTLQueue(module);
  // init_stl_priority_queue(module);
  initSTLPair(module);

  initSTLIterator(module);
  initSTLVectorIterator(module);
  initSTLBitVectorIterator(module);
  initSTLDequeIterator(module);
  initSTLListIterator(module);
  initSTLForwardListIterator(module);
  initSTLTreeIterator(module);
  initSTLHashtableIterator(module);

  initStdCpp(module);
  initStdBitset(module);
  initStdChrono(module);
  initStdFstream(module);
  initStdIO(module);
  initStdIstream(module);
  initStdOstream(module);
  initStdStreambuf(module);
  initStdString(module);
  initStdStringStream(module);
  initStdRegex(module);

  for(const Function& f : module.functions())
    if(Metadata::hasModel(f))
      modelNames[&f] = Metadata::getModel(f);

  add(new ModelReadOnly(AerynConfig::getPureFunctionModel()));
}

void Models::add(Model* model, bool strict) {
  if(strict and models.contains(model->getFunctionName()))
    moya_error("Model " << model->getFunctionName() << " already registered");
  models[model->getFunctionName()].reset(model);
}

bool Models::contains(const std::string& fname) const {
  return models.contains(fname);
}

bool Models::contains(const Function* f) const {
  if(modelNames.contains(f))
    return models.contains(modelNames.at(f));
  return false;
}

bool Models::contains(const Function& f) const {
  return contains(&f);
}

Model* Models::get(const std::string& name) const {
  return models.at(name).get();
}

Model* Models::get(const Function* f) const {
  if(modelNames.contains(f))
    return get(modelNames.at(f));
  return nullptr;
}

Model* Models::get(const Function& f) const {
  return get(&f);
}

} // namespace moya

// The init_* functions are all defined in different files. Each file
// corresponds to the library for which the init_* method is defining models
