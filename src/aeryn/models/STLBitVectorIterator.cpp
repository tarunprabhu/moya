#include "ModelReadWrite.h"
#include "ModelSTLBitVector.h"
#include "Models.h"
#include "common/LangConfig.h"

#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;
using namespace moya;

static std::string getIteratorName(bool isConst) {
  if(isConst)
    return STDConfig::getSTDName(STDKind::STD_STL_const_iterator_bitvector);
  else
    return STDConfig::getSTDName(STDKind::STD_STL_iterator_bitvector);
}

static std::string mkIter(const std::string& name, bool isConst) {
  std::stringstream ss;
  ss << getIteratorName(isConst) << "::" << name;
  return ss.str();
}

static std::string mkCompare(const std::string& op, bool isConst) {
  std::stringstream ss;
  std::string iter = getIteratorName(isConst);
  ss << iter << "::operator" << op << "(" << iter << ")";
  return ss.str();
}

static std::string mkStdCompare(const std::string& op, bool isConst) {
  std::stringstream ss;
  std::string iter = getIteratorName(isConst);
  ss << iter << "::operator" << op << "(" << iter << ", " << iter << ")";
  return ss.str();
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLBitVector::FuncID fn) {
  ms.add(new ModelSTLBitVector(name, fn, ms));
}

void Models::initSTLBitVectorIterator(const Module& module) {
  Models& ms = *this;

  mkModel(ms,
          "std::_Bit_iterator::_Bit_iterator(iterator)",
          ModelSTLBitVector::IteratorConstructorCopy);
  mkModel(ms,
          "std::_Bit_const_iterator::_Bit_const_iterator(iterator)",
          ModelSTLBitVector::IteratorConstructorCopy);

  // Add the iterator comparison operator overload models
  for(bool isConst : {false, true}) {
    for(const std::string& op : LangConfigCXX::getComparisonOperators()) {
      mkModel(ms, mkCompare(op, isConst), ModelSTLBitVector::IteratorCompare);
      mkModel(
          ms, mkStdCompare(op, isConst), ModelSTLBitVector::IteratorCompare);
    }
  }

  // Operations on iterators
  for(bool c : {false, true}) {
    mkModel(ms, mkIter("operator*()", c), ModelSTLBitVector::IteratorDeref);
    mkModel(ms, mkIter("operator->()", c), ModelSTLBitVector::IteratorDeref);
    mkModel(
        ms, mkIter("operator[](long)", c), ModelSTLBitVector::IteratorDeref);
    mkModel(ms, mkIter("operator++()", c), ModelSTLBitVector::IteratorSelf);
    mkModel(ms, mkIter("operator++(int)", c), ModelSTLBitVector::IteratorDeref);
    mkModel(ms, mkIter("operator--()", c), ModelSTLBitVector::IteratorSelf);
    mkModel(ms, mkIter("operator--(int)", c), ModelSTLBitVector::IteratorDeref);
    mkModel(ms, mkIter("operator+=(long)", c), ModelSTLBitVector::IteratorSelf);
    mkModel(ms, mkIter("operator+(long)", c), ModelSTLBitVector::IteratorDeref);
    mkModel(ms, mkIter("operator-=(long)", c), ModelSTLBitVector::IteratorSelf);
    mkModel(ms, mkIter("operator-(long)", c), ModelSTLBitVector::IteratorDeref);
    mkModel(ms, mkIter("base()", c), ModelSTLBitVector::IteratorBase);
  }
}
