#include "ModelSTLBitVector.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

using namespace llvm;

namespace moya {

// The bitvectors for this version of Clang/libstdc++ is at it's heart a
// struct whose elements are:
//
// struct vector = { i64*, i32, i64*, i32*, i64* }
//
// When a bitvector's constructor is called, a single cell is allocated for the
// underlying type. This will be the buffer into which all the contents of the
// bitvector will be stored.
//
// All three pointers will point to this buffer
//
// All the pointers move in lockstep. When one is read, all are read. When one
// is modified, all are modified.
//
// All iterators will return a pointer to the data buffer.
//
// Resize operations will have no effect other than marking all the pointers
// and the data buffer as having been modified
//

ModelSTLBitVector::ModelSTLBitVector(const std::string& fname,
                                     FuncID fn,
                                     const Models& ms)
    : ModelSTLVector(fname,
                     STDKind::STD_STL_bitvector,
                     STDKind::STD_STL_iterator_bitvector,
                     STDKind::STD_STL_iterator_bitvector,
                     fn,
                     ms) {
  switch(fn) {
  case ModelSTLBitVector::AccessElement:
    processFn = static_cast<ExecuteFn>(&ModelSTLBitVector::modelAccessElement);
    break;
  case ModelSTLBitVector::IteratorSelf:
    processFn = static_cast<ExecuteFn>(&ModelSTLBitVector::modelIteratorSelf);
  default:
    // Everything else will have been initialized in the ModelSTLVector()
    // constructor
    break;
  }
}

Contents ModelSTLBitVector::getBeginIterator(const Addresses& conts,
                                             const Instruction* call,
                                             const Function* f,
                                             Store& store,
                                             AnalysisStatus& changed) const {
  Contents ret;
  Addresses bufs = getBuffer(conts, call, f, store, changed);

  changed |= markContainerRead(conts, call, f, store);
  for(const Store::Address* buf : bufs)
    ret.insert(store.make({{buf}, {store.getRuntimeScalar()}}));

  return ret;
}

Contents ModelSTLBitVector::getEndIterator(const Addresses& conts,
                                           const Instruction* call,
                                           const Function* f,
                                           Store& store,
                                           AnalysisStatus& changed) const {
  changed |= markContainerRead(conts, call, f, store);
  return {store.make({{store.getNullptr()}, {store.getRuntimeScalar()}})};
}

AnalysisStatus
ModelSTLBitVector::updateContainerPointers(const Address* base,
                                           const Addresses& vals,
                                           const Instruction* call,
                                           const Function* f,
                                           Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* pi64 = Type::getInt64PtrTy(call->getContext());
  unsigned vtadj = getContainerVtableOffset(base, call, f, store);
  for(unsigned offset : {0, 16, 32})
    for(const Content* c : vals)
      store.write(store.make(base, vtadj + offset), c, pi64, call, changed);

  return changed;
}

AnalysisStatus
ModelSTLBitVector::modelAccessElement(const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Contents ret;
  const Addresses& iters = args.at(0).getAs<Address>(call);

  changed |= markIteratorRead(iters, call, f, store);
  for(const Address* buf : getBuffer(iters, call, f, store, changed))
    ret.insert(store.make({{buf}, {store.getRuntimeScalar()}}));

  env.push(ret);

  return changed;
}

AnalysisStatus
ModelSTLBitVector::modelIteratorDeref(const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Contents ret;
  const Addresses& iters = args.at(0).getAs<Address>(call);

  changed |= markIteratorRead(iters, call, f, store);
  for(const Address* buf : getIteratorDeref(iters, call, f, store, changed))
    ret.insert(store.make({buf, store.getRuntimeScalar()}));

  env.push(ret);

  return changed;
}

AnalysisStatus
ModelSTLBitVector::modelIteratorSelf(const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  StructType* iterTy = Metadata::getSTLIteratorType(f);
  Contents iters = store.readAs(args.at(hasStructRet), iterTy, call, changed);

  if(hasStructRet)
    for(const Address* sret : args.at(0).getAs<Address>(call))
      store.write(sret, iters, iterTy, call, changed);
  else
    env.push(iters);

  return changed;
}

} // namespace moya
