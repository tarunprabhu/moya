#include "ModelSTLUnorderedSet.h"
#include "Models.h"
#include "common/LangConfig.h"

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_unordered_set);
}

static std::string mk(const std::string& name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "std::operator" << op << "(" << getContainerName() << ", "
       << getContainerName() << ")";
    ms.add(
        new ModelSTLUnorderedSet(ss.str(), ModelSTLUnorderedSet::Compare, ms));
  }
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLUnorderedSet(name, fn, ms));
}

void Models::initSTLUnorderedSet(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms, mk("unordered_set()"), ModelSTLUnorderedSet::ConstructorEmpty);
  mkModel(ms,
          mk("unordered_set(unsigned long, std::hash, std::equal_to, "
             "allocator)"),
          ModelSTLUnorderedSet::ConstructorEmpty);
  mkModel(ms,
          mk("unordered_set(T, T, unsigned long, std::hash, "
             "std::equal_to, allocator)"),
          ModelSTLUnorderedSet::ConstructorRange);
  mkModel(ms,
          mk("unordered_set(T, T, std::hash, std::equal_to, "
             "allocator)"),
          ModelSTLUnorderedSet::ConstructorRange);
  mkModel(ms,
          mk("unordered_set(std::unordered_set)"),
          ModelSTLUnorderedSet::ConstructorCopy);
  mkModel(ms,
          mk("unordered_set(std::initializer_list, unsigned long, "
             "std::hash, std::equal_to, allocator)"),
          ModelSTLUnorderedSet::ConstructorInitList);

  // Destructor
  mkModel(ms, mk("~unordered_set()"), ModelSTLUnorderedSet::Destructor);

  // operator =
  mkModel(ms,
          mk("operator=(std::unordered_set)"),
          ModelSTLUnorderedSet::OperatorContainer);
  mkModel(ms,
          mk("operator=(std::initializer_list)"),
          ModelSTLUnorderedSet::OperatorInitList);

  // allocator
  mkModel(ms, mk("get_allocator()"), ModelSTLUnorderedSet::GetAllocator);

  // Iterators
  mkModel(ms, mk("begin()"), ModelSTLUnorderedSet::Begin);
  mkModel(ms, mk("cbegin()"), ModelSTLUnorderedSet::Begin);
  mkModel(ms, mk("end()"), ModelSTLUnorderedSet::End);
  mkModel(ms, mk("cend()"), ModelSTLUnorderedSet::End);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLUnorderedSet::Size);
  mkModel(ms, mk("size()"), ModelSTLUnorderedSet::Size);
  mkModel(ms, mk("max_size()"), ModelSTLUnorderedSet::Size);

  // Modifiers
  mkModel(ms, mk("clear()"), ModelSTLUnorderedSet::Clear);

  mkModel(ms, mk("emplace(...)"), ModelSTLUnorderedSet::EmplaceElement);
  mkModel(ms,
          mk("emplace_hint(iterator. ...)"),
          ModelSTLUnorderedSet::EmplaceElementAt);

  mkModel(ms, mk("insert(*)"), ModelSTLUnorderedSet::InsertElement);
  mkModel(ms, mk("insert(T)"), ModelSTLUnorderedSet::InsertElement);
  mkModel(ms, mk("insert(iterator, *)"), ModelSTLUnorderedSet::InsertElementAt);
  mkModel(ms, mk("insert(T, T)"), ModelSTLUnorderedSet::InsertRange);
  mkModel(ms,
          mk("insert(std::initializer_list)"),
          ModelSTLUnorderedSet::InsertInitList);

  mkModel(ms, mk("erase(*)"), ModelSTLUnorderedSet::EraseElement);
  mkModel(ms, mk("erase(iterator)"), ModelSTLUnorderedSet::EraseElementAt);
  mkModel(
      ms, mk("erase(iterator, iterator)"), ModelSTLUnorderedSet::EraseRange);

  mkModel(ms, mk("swap(std::unordered_set)"), ModelSTLUnorderedSet::Swap);

  // Lookup
  mkModel(ms, mk("count(*)"), ModelSTLUnorderedSet::Size);
  mkModel(ms, mk("find(*)"), ModelSTLUnorderedSet::Find);
  mkModel(ms, mk("equal_range(*)"), ModelSTLUnorderedSet::FindRange);
  mkModel(ms, mk("lower_bound(*)"), ModelSTLUnorderedSet::FindRange);
  mkModel(ms, mk("upper_bound(*)"), ModelSTLUnorderedSet::FindRange);

  // Buckets
  mkModel(ms, mk("begin(unsigned long)"), ModelSTLUnorderedSet::BucketBegin);
  mkModel(ms, mk("cbegin(unsigned long)"), ModelSTLUnorderedSet::BucketBegin);
  mkModel(ms, mk("end(unsigned long)"), ModelSTLUnorderedSet::BucketEnd);
  mkModel(ms, mk("cend(unsigned long)"), ModelSTLUnorderedSet::BucketEnd);
  mkModel(ms, mk("bucket_count()"), ModelSTLUnorderedSet::Buckets);
  mkModel(ms, mk("max_bucket_count()"), ModelSTLUnorderedSet::Buckets);
  mkModel(ms, mk("bucket_size(unsigned long)"), ModelSTLUnorderedSet::Buckets);
  mkModel(ms, mk("bucket(*)"), ModelSTLUnorderedSet::Buckets);

  // Hash policy
  mkModel(ms, mk("load_factor()"), ModelSTLUnorderedSet::GetLoadFactor);
  mkModel(ms, mk("max_load_factor()"), ModelSTLUnorderedSet::GetLoadFactor);
  mkModel(
      ms, mk("max_load_factor(float)"), ModelSTLUnorderedSet::SetLoadFactor);
  mkModel(ms, mk("rehash(unsigned long)"), ModelSTLUnorderedSet::Resize);
  mkModel(ms, mk("reserve(unsigned long)"), ModelSTLUnorderedSet::Resize);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);
}
