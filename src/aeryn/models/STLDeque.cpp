#include "ModelSTLDeque.h"
#include "Models.h"
#include "common/LangConfig.h"

using namespace llvm;
using namespace moya;

static std::string getContainerName() {
  return STDConfig::getSTDName(STDKind::STD_STL_deque);
}

static std::string mk(const std::string& name) {
  std::string s;
  raw_string_ostream ss(s);
  ss << getContainerName() << "::" << name;
  return ss.str();
}

static void initContainerCompareModels(Models& ms) {
  for(StringRef op : LangConfigCXX::getComparisonOperators()) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "std::operator" << op << "(" << getContainerName() << ", "
       << getContainerName() << ")";
    ms.add(new ModelSTLDeque(ss.str(), ModelSTLDeque::Compare, ms));
  }
}

static void
mkModel(Models& ms, const std::string& name, ModelSTLContainer::FuncID fn) {
  ms.add(new ModelSTLDeque(name, fn, ms));
}

void Models::initSTLDeque(const Module& module) {
  Models& ms = *this;

  // Constructors
  mkModel(ms, mk("deque()"), ModelSTLDeque::ConstructorEmpty);
  mkModel(ms, mk("deque(allocator)"), ModelSTLDeque::ConstructorEmpty);
  mkModel(ms,
          mk("deque(unsigned long, allocator)"),
          ModelSTLDeque::ConstructorEmpty);
  mkModel(ms,
          mk("deque(unsigned long, *, allocator)"),
          ModelSTLDeque::ConstructorElement);
  mkModel(ms, mk("deque(T, T, allocator)"), ModelSTLDeque::ConstructorRange);
  mkModel(ms, mk("deque(std::deque)"), ModelSTLDeque::ConstructorCopy);
  mkModel(ms,
          mk("deque(std::initializer_list, allocator)"),
          ModelSTLDeque::ConstructorInitList);

  // Destructor
  mkModel(ms, mk("~deque()"), ModelSTLDeque::Destructor);

  // operator =
  mkModel(ms, mk("operator=(std::deque)"), ModelSTLDeque::OperatorContainer);
  mkModel(ms,
          mk("operator=(std::initializer_list)"),
          ModelSTLDeque::OperatorInitList);

  // allocator
  mkModel(ms, mk("get_allocator()"), ModelSTLDeque::GetAllocator);

  // assign
  mkModel(ms, mk("assign(unsigned long, *)"), ModelSTLDeque::AssignMany);
  mkModel(ms, mk("assign(T, T)"), ModelSTLDeque::AssignRange);
  mkModel(
      ms, mk("assign(std::initializer_list)"), ModelSTLDeque::AssignInitList);

  // Element access
  mkModel(ms, mk("at(unsigned long)"), ModelSTLDeque::AccessElement);
  mkModel(ms, mk("operator[](unsigned long)"), ModelSTLDeque::AccessElement);
  mkModel(ms, mk("front()"), ModelSTLDeque::AccessElement);
  mkModel(ms, mk("back()"), ModelSTLDeque::AccessElement);

  // Iterators
  mkModel(ms, mk("begin()"), ModelSTLDeque::Begin);
  mkModel(ms, mk("cbegin()"), ModelSTLDeque::Begin);
  mkModel(ms, mk("end()"), ModelSTLDeque::End);
  mkModel(ms, mk("cend()"), ModelSTLDeque::End);
  mkModel(ms, mk("rbegin()"), ModelSTLDeque::Begin);
  mkModel(ms, mk("crbegin()"), ModelSTLDeque::Begin);
  mkModel(ms, mk("rend()"), ModelSTLDeque::End);
  mkModel(ms, mk("crend()"), ModelSTLDeque::End);

  // Capacity
  mkModel(ms, mk("empty()"), ModelSTLDeque::Size);
  mkModel(ms, mk("size()"), ModelSTLDeque::Size);
  mkModel(ms, mk("max_size()"), ModelSTLDeque::Size);
  // Both reserve and shrink_to_fit behave like clear in the sense that the
  // underlying data is touched in some way. So we can get away with using the
  // same model for all three methods
  mkModel(ms, mk("shrink_to_fit()"), ModelSTLDeque::Resize);

  // Modifiers
  mkModel(ms, mk("clear()"), ModelSTLDeque::Clear);

  mkModel(ms, mk("emplace(iterator, ...)"), ModelSTLDeque::EmplaceElementAt);
  mkModel(ms, mk("emplace_front(...)"), ModelSTLDeque::EmplaceElement);
  mkModel(ms, mk("emplace_back(...)"), ModelSTLDeque::EmplaceElement);

  mkModel(ms, mk("insert(iterator, *)"), ModelSTLDeque::InsertElement);
  mkModel(
      ms, mk("insert(iterator, unsigned long, *)"), ModelSTLDeque::InsertMany);
  mkModel(ms, mk("insert(iterator, T, T)"), ModelSTLDeque::InsertRange);
  mkModel(ms,
          mk("insert(iterator, std::initializer_list)"),
          ModelSTLDeque::InsertInitList);

  mkModel(ms, mk("erase(iterator)"), ModelSTLDeque::EraseElementAt);
  mkModel(ms, mk("erase(iterator, iterator)"), ModelSTLDeque::EraseRange);

  mkModel(ms, mk("push_back(*)"), ModelSTLDeque::AddElement);
  mkModel(ms, mk("push_front(*)"), ModelSTLDeque::AddElement);

  mkModel(ms, mk("pop_front()"), ModelSTLDeque::RemoveElement);
  mkModel(ms, mk("pop_back()"), ModelSTLDeque::RemoveElement);

  // The resize function will change the pointers and might also affect the
  // data, so it behaves somewhat like clear
  mkModel(ms, mk("resize(unsigned long)"), ModelSTLDeque::Clear);
  // This overload of resize also assigns to the modified deque, so it
  // behaves somewhat like assign
  mkModel(ms, mk("resize(unsigned long, *)"), ModelSTLDeque::AssignMany);

  mkModel(ms, mk("swap(std::deque)"), ModelSTLDeque::Swap);

  // Add the container comparison operator overload models
  initContainerCompareModels(ms);
}
