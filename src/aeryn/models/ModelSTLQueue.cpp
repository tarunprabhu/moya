#include "ModelSTLQueue.h"

using namespace llvm;

namespace moya {

// This is the queue layout in memory:
//
// queue = { i8*, i64*, { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* } }
//
// This is identical to a std::deque and is implemented using a std::deque

ModelSTLQueue::ModelSTLQueue(const std::string& fname,
                             ModelSTLContainer::FuncID fn,
                             const Models& ms)
    : ModelSTLDeque(fname,
                    STDKind::STD_STL_queue,
                    STDKind::STD_None,
                    STDKind::STD_None,
                    fn,
                    ms) {
  ;
}

} // namespace moya
