#include "ModelSTLPair.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

using namespace llvm;

namespace moya {

ModelSTLPair::ModelSTLPair(const std::string& fname,
                           STDKind cont,
                           STDKind iter,
                           STDKind citer,
                           FuncID fn,
                           const Models& ms)
    : ModelSTLContainer(fname, cont, iter, citer, fn, ms) {
  switch(fn) {
  case ModelSTLPair::ConstructorCopy:
    processFn = static_cast<ExecuteFn>(&ModelSTLPair::modelConstructorCopy);
    break;
  case ModelSTLPair::ConstructorElement:
    processFn = static_cast<ExecuteFn>(&ModelSTLPair::modelConstructorElement);
    break;
  case ModelSTLPair::ConstructorEmpty:
    processFn = static_cast<ExecuteFn>(&ModelSTLPair::modelConstructorEmpty);
    break;
  case ModelSTLPair::Destructor:
    processFn = static_cast<ExecuteFn>(&ModelSTLPair::modelDestructor);
    break;
  case ModelSTLPair::OperatorContainer:
    processFn = static_cast<ExecuteFn>(&ModelSTLPair::modelOperatorContainer);
    break;
  case ModelSTLPair::Swap:
    processFn = static_cast<ExecuteFn>(&ModelSTLPair::modelSwap);
    break;
  default:
    // Everything else will have been initialized in the ModelSTLContainer()
    // constructor
    break;
  }
}

ModelSTLPair::ModelSTLPair(const std::string& fname,
                           FuncID fn,
                           const Models& ms)
    : ModelSTLPair(fname,
                   STDKind::STD_STL_pair,
                   STDKind::STD_None,
                   STDKind::STD_None,
                   fn,
                   ms) {
  ;
}

Addresses ModelSTLPair::getBuffer(const Addresses& conts,
                                  const Instruction* call,
                                  const Function* f,
                                  Store& store,
                                  AnalysisStatus& changed) const {
  return conts;
}

Addresses ModelSTLPair::getNode(const Addresses& conts,
                                const Instruction* call,
                                const Function* f,
                                Store& store,
                                AnalysisStatus& changed) const {
  moya_error("std::pair has no node");

  return conts;
}

AnalysisStatus ModelSTLPair::writeToContainer(const Addresses& conts,
                                              const Contents& vals,
                                              const Instruction* call,
                                              const Function* f,
                                              Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  StructType* contTy = Metadata::getSTLContainerType(f);

  for(const Store::Address* cont : conts)
    store.write(cont, vals, contTy, call, changed);

  return changed;
}

AnalysisStatus ModelSTLPair::modelConstructorEmpty(const Instruction* call,
                                                   const Function* f,
                                                   const Vector<Contents>& args,
                                                   Environment& env,
                                                   Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const Addresses& conts = args.at(0).getAs<Address>(call);

  changed |= markContainerWrite(conts, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLPair::modelConstructorElement(const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  StructType* contTy = Metadata::getSTLContainerType(f);
  Type* fstTy = contTy->getElementType(0);
  Type* sndTy = contTy->getElementType(1);
  unsigned long offsetFirst = 0;
  unsigned long offsetSecond
      = store.getStructLayout(contTy).getElementOffset(1);
  const Addresses& conts = args.at(0).getAs<Address>(call);
  const Addresses& addrs1 = args.at(1).getAs<Address>(call);
  const Addresses& addrs2 = args.at(2).getAs<Address>(call);

  if(fstTy->isStructTy()
     and (not LLVMUtils::isPointerTy(LLVMUtils::getArgumentType(f, 1))))
    moya_error("Struct types passed by value to pair is not supported. Try "
               "recompiling with C++11. Expecting "
               << *fstTy << " at " << getDiagnostic(call));

  if(sndTy->isStructTy()
     and (not LLVMUtils::isPointerTy(LLVMUtils::getArgumentType(f, 2))))
    moya_error("Struct types passed by value to pair is not supported. Try "
               "recompiling with C++11. Expecting "
               << *sndTy << " at " << getDiagnostic(call));

  for(const Address* pair : conts) {
    const Address* first = store.make(pair, offsetFirst);
    const Address* second = store.make(pair, offsetSecond);
    for(const Address* addr : addrs1)
      store.copy(first, addr, fstTy, call, changed);
    for(const Address* addr : addrs2)
      store.copy(second, addr, sndTy, call, changed);
  }

  return changed;
}

AnalysisStatus ModelSTLPair::modelConstructorCopy(const Instruction* call,
                                                  const Function* f,
                                                  const Vector<Contents>& args,
                                                  Environment& env,
                                                  Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  StructType* contTy = Metadata::getSTLContainerType(f);
  Addresses dsts = args.at(0).getAs<Address>(call);
  const Contents& vals = store.readAs(args.at(1), contTy, call, changed);

  changed |= writeToContainer(dsts, vals, call, f, store);

  return changed;
}

AnalysisStatus ModelSTLPair::modelDestructor(const Instruction* call,
                                             const Function* f,
                                             const Vector<Contents>& args,
                                             Environment& env,
                                             Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Addresses conts = args.at(0).getAs<Address>(call);
  changed |= markContainerWrite(conts, call, f, store);

  return changed;
}

AnalysisStatus
ModelSTLPair::modelOperatorContainer(const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  StructType* contTy = Metadata::getSTLContainerType(f);
  Addresses dsts = args.at(0).getAs<Address>(call);
  const Contents& vals = store.readAs(args.at(1), contTy, call, changed);

  changed |= writeToContainer(dsts, vals, call, f, store);

  env.push(args.at(0));

  return changed;
}

AnalysisStatus ModelSTLPair::modelSwap(const Instruction* call,
                                       const Function* f,
                                       const Vector<Contents>& args,
                                       Environment& env,
                                       Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  StructType* contTy = Metadata::getSTLContainerType(f);
  Addresses cont1 = args.at(0).getAs<Address>(call);
  Addresses cont2 = args.at(1).getAs<Address>(call);
  Contents vals1 = store.readAs(cont1, contTy, call, changed);
  Contents vals2 = store.readAs(cont2, contTy, call, changed);

  changed |= writeToContainer(cont1, vals2, call, f, store);
  changed |= writeToContainer(cont2, vals1, call, f, store);

  return changed;
}

} // namespace moya
