#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

using namespace llvm;
using namespace moya;

static AnalysisStatus modelTopoInit(const ModelCustom& model,
                                    const Instruction* call,
                                    const Function* f,
                                    const Vector<Contents>& args,
                                    Environment& env,
                                    Store& store,
                                    const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* type = dyn_cast<PointerType>(LLVMUtils::getArgumentType(f, 0))
                   ->getElementType();
  const Store::Address* allocated
      = AbstractUtils::allocate(type, store, call, caller);
  for(const auto* addr : args.at(0).getAs<Address>(call))
    store.write(addr, allocated, type->getPointerTo(), call, changed);

  env.push(store.getRuntimeScalar());

  return changed;
}

static AnalysisStatus modelAlloc(const ModelCustom& model,
                                 const Instruction* call,
                                 const Function* f,
                                 const Vector<Contents>& args,
                                 Environment& env,
                                 Store& store,
                                 const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= AbstractUtils::malloc(call, env, store, caller);

  return changed;
}

static AnalysisStatus modelFree(const ModelCustom& model,
                                const Instruction* call,
                                const Function* f,
                                const Vector<Contents>& args,
                                Environment& env,
                                Store& store,
                                const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= AbstractUtils::free(
      call, args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, caller);

  return changed;
}

static AnalysisStatus modelTopoDup(const ModelCustom& model,
                                   const Instruction* call,
                                   const Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const auto* addr : args.at(0).getAs<Address>(call))
    store.write(
        addr, args.at(1), LLVMUtils::getArgumentType(f, 1), call, changed);

  env.push(store.getRuntimeScalar());

  return changed;
}

static AnalysisStatus modelTopoCustomInsert(const ModelCustom& model,
                                            const Instruction* call,
                                            const Function* f,
                                            const Vector<Contents>& args,
                                            Environment& env,
                                            Store& store,
                                            const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("UNIMPLEMENTED: hwloc_custom_insert_topology");

  return changed;
}

static AnalysisStatus modelBitmapASPrintf(const ModelCustom& model,
                                          const Instruction* call,
                                          const Function* f,
                                          const Vector<Contents>& args,
                                          Environment& env,
                                          Store& store,
                                          const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(unsigned i = 1; i < args.size(); i++)
    changed |= model.markRead(
        args.at(i), LLVMUtils::getArgumentType(f, i), env, store, call);

  if(not env.contains(call)) {
    Type* i8 = Type::getInt8Ty(call->getContext());
    const Store::Address* addr
        = AbstractUtils::allocate(i8, store, call, caller);
    store.write(addr, store.getRuntimeScalar(), i8, call, changed);
  }

  env.push(store.getRuntimeScalar());
  return changed;
}

static AnalysisStatus modelBitmapAlloc(const ModelCustom& model,
                                       const Instruction* call,
                                       const Function* f,
                                       const Vector<Contents>& args,
                                       Environment& env,
                                       Store& store,
                                       const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(call)) {
    Type* type = dyn_cast<PointerType>(f->getReturnType())->getElementType();
    const Store::Address* addr
        = AbstractUtils::allocate(type, store, call, caller);
    env.push(addr);
  }

  return changed;
}

static AnalysisStatus modelBitmapFree(const ModelCustom& model,
                                      const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store,
                                      const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= AbstractUtils::free(
      call, args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, caller);

  return changed;
}

static AnalysisStatus modelBitmapCopy(const ModelCustom& model,
                                      const Instruction* call,
                                      const Function* f,
                                      const Vector<Contents>& args,
                                      Environment& env,
                                      Store& store,
                                      const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("UNIMPLEMENTED: hwloc_bitmap_copy");

  return changed;
}

static AnalysisStatus modelBitmapDup(const ModelCustom& model,
                                     const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store,
                                     const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("UNIMPLEMENTED: hwloc_bitmap_dup");

  return changed;
}

void Models::initLibHWLoc(const Module& module) {
  add(new ModelReadOnly("hwloc_get_api_version"));
  add(new ModelReadOnly("hwloc_compare_types"));
  add(new ModelReadOnly("hwloc_topology_check"));
  add(new ModelReadWrite("hwloc_topology_destroy", {0}));
  add(new ModelCustom("hwloc_topology_dup", modelTopoDup));
  add(new ModelCustom("hwloc_topology_init", modelTopoInit));
  add(new ModelReadWrite("hwloc_topology_load", {0}));
  add(new ModelReadOnly("hwloc_topology_get_flags"));
  add(new ModelReadOnly("hwloc_topology_get_support"));
  add(new ModelReadOnly("hwloc_topology_get_userdata"));
  add(new ModelReadOnly("hwloc_topology_ignore_all_keep_structure"));
  add(new ModelReadOnly("hwloc_topology_ignore_type"));
  add(new ModelReadOnly("hwloc_topology_ignore_type_keep_structure"));
  add(new ModelReadOnly("hwloc_topology_is_thissystem"));
  add(new ModelReadOnly("hwloc_topology_set_custom"));
  add(new ModelReadOnly("hwloc_topology_set_distance_matrix"));
  add(new ModelReadOnly("hwloc_topology_set_flags"));
  add(new ModelReadOnly("hwloc_topology_set_fsroot"));
  add(new ModelReadOnly("hwloc_topology_set_pid"));
  add(new ModelReadOnly("hwloc_topology_set_synthetic"));
  add(new ModelReadOnly("hwloc_topology_set_userdata"));
  add(new ModelReadOnly("hwloc_topology_set_xml"));
  add(new ModelReadOnly("hwloc_topology_set_xmlbuffer"));
  add(new ModelReadOnly("hwloc_get_depth_type"));
  add(new ModelReadOnly("hwloc_get_nbobjs_by_depth"));
  add(new ModelReadOnly("hwloc_get_nbobjs_by_type"));
  add(new ModelReadOnly("hwloc_get_next_obj_by_depth"));
  add(new ModelReadOnly("hwloc_get_next_obj_by_type"));
  add(new ModelReadOnly("hwloc_get_obj_by_depth"));
  add(new ModelReadOnly("hwloc_get_obj_by_type"));
  add(new ModelReadOnly("hwloc_get_root_obj"));
  add(new ModelReadOnly("hwloc_get_type_depth"));
  add(new ModelReadOnly("hwloc_get_type_or_above_depth"));
  add(new ModelReadOnly("hwloc_get_type_or_below_depth"));
  add(new ModelReadOnly("hwloc_topology_get_depth"));
  add(new ModelReadWrite("hwloc_obj_attr_snprintf", {0, 1, 1, 1, 1}));
  add(new ModelReadWrite("hwloc_obj_cpuset_snprintf", {0, 1, 1, 1, 1}));
  add(new ModelReadWrite("hwloc_obj_type_snprintf", {0, 1, 1, 1, 1}));
  add(new ModelReadWrite("hwloc_obj_type_sscanf", {1, 0, 0, 0, 1}));
  add(new ModelReadOnly("hwloc_obj_type_string"));
  add(new ModelReadWrite("hwloc_obj_add_info", {0, 1, 1}));
  add(new ModelReadOnly("hwloc_obj_get_info_by_name"));
  add(new ModelReadOnly("hwloc_get_cpubind"));
  add(new ModelReadOnly("hwloc_get_last_cpu_location"));
  add(new ModelReadOnly("hwloc_get_proc_cpubind"));
  add(new ModelReadOnly("hwloc_get_proc_last_cpu_location"));
  add(new ModelReadOnly("hwloc_get_thread_cpubind"));
  add(new ModelReadOnly("hwloc_set_thread_cpubind"));
  add(new ModelReadOnly("hwloc_set_cpubind"));
  add(new ModelReadOnly("hwloc_set_proc_cpubind"));
  add(new ModelCustom("hwloc_alloc", modelAlloc));
  add(new ModelCustom("hwloc_alloc_membind", modelAlloc));
  add(new ModelCustom("hwloc_alloc_membind_nodeset", modelAlloc));
  add(new ModelCustom("hwloc_alloc_membind_policy", modelAlloc));
  add(new ModelCustom("hwloc_alloc_membind_policy_nodeset", modelAlloc));
  add(new ModelCustom("hwloc_free", modelFree));
  add(new ModelReadWrite("hwloc_get_area_membind", {1, 1, 1, 0, 0, 1}));
  add(new ModelReadWrite("hwloc_get_area_membind_nodeset", {1, 1, 1, 0, 0, 1}));
  add(new ModelReadOnly("hwloc_get_area_memlocation"));
  add(new ModelReadWrite("hwloc_get_membind", {1, 0, 0, 1}));
  add(new ModelReadWrite("hwloc_get_membind_nodeset", {1, 0, 0, 1}));
  add(new ModelReadWrite("hwloc_get_proc_membind", {1, 1, 0, 0, 1}));
  add(new ModelReadWrite("hwloc_get_proc_membind_nodeset", {1, 1, 0, 0, 1}));
  add(new ModelReadOnly("hwloc_set_area_membind"));
  add(new ModelReadOnly("hwloc_set_area_membind_nodeset"));
  add(new ModelReadOnly("hwloc_set_membind"));
  add(new ModelReadOnly("hwloc_set_membind_nodeset"));
  add(new ModelReadOnly("hwloc_set_proc_membind"));
  add(new ModelReadOnly("hwloc_set_proc_membind_nodeset"));
  add(new ModelReadWrite("hwloc_topology_insert_misc_object_by_cpuset",
                         {0, 1, 1}));
  add(new ModelReadWrite("hwloc_topology_insert_misc_object_by_parent",
                         {0, 1, 1}));
  add(new ModelReadWrite("hwloc_topology_restrict", {0, 1, 1}));
  add(new ModelReadWrite("hwloc_custom_insert_group_object_by_parent",
                         {0, 0, 1}));
  add(new ModelCustom("hwloc_custom_insert_topology", modelTopoCustomInsert));
  add(new ModelReadWrite("hwloc_topology_export_synthetic", {1, 0, 1, 1}));
  add(new ModelReadOnly("hwloc_get_first_largest_obj_inside_cpuset"));
  add(new ModelReadWrite("hwloc_get_largest_objs_inside_cpuset", {1, 1, 0, 1}));
  add(new ModelReadOnly("hwloc_get_nbobjs_inside_cpuset_by_depth"));
  add(new ModelReadOnly("hwloc_get_nbobjs_inside_cpuset_by_type"));
  add(new ModelReadOnly("hwloc_get_next_obj_inside_cpuset_by_depth"));
  add(new ModelReadOnly("hwloc_get_next_obj_inside_cpuset_by_type"));
  add(new ModelReadOnly("hwloc_get_obj_inside_cpuset_by_depth"));
  add(new ModelReadOnly("hwloc_get_obj_inside_cpuset_by_type"));
  add(new ModelReadOnly("hwloc_get_child_covering_cpuset"));
  add(new ModelReadOnly("hwloc_get_next_obj_covering_cpuset_by_depth"));
  add(new ModelReadOnly("hwloc_get_next_obj_covering_cpuset_by_type"));
  add(new ModelReadOnly("hwloc_get_obj_covering_cpuset"));
  add(new ModelReadWrite("hwloc_bitmap_allbut", {0, 1}));
  add(new ModelCustom("hwloc_bitmap_alloc", modelBitmapAlloc));
  add(new ModelCustom("hwloc_bitmap_alloc_full", modelBitmapAlloc));
  add(new ModelReadWrite("hwloc_bitmap_and", {0, 1, 1}));
  add(new ModelReadWrite("hwloc_bitmap_andnot", {0, 1, 1}));
  add(new ModelCustom("hwloc_bitmap_asprintf", modelBitmapASPrintf));
  add(new ModelReadWrite("hwloc_bitmap_clr", {0, 1}));
  add(new ModelReadOnly("hwloc_bitmap_compare"));
  add(new ModelReadOnly("hwloc_bitmap_compare_first"));
  add(new ModelCustom("hwloc_bitmap_copy", modelBitmapCopy));
  add(new ModelCustom("hwloc_bitmap_dup", modelBitmapDup));
  add(new ModelReadWrite("hwloc_bitmap_fill", {0}));
  add(new ModelReadOnly("hwloc_bitmap_first"));
  add(new ModelCustom("hwloc_bitmap_free", modelBitmapFree));
  add(new ModelReadWrite("hwloc_bitmap_from_ith_ulong", {1, 0, 0}));
  add(new ModelReadWrite("hwloc_bitmap_from_ulong", {1, 0, 0}));
  add(new ModelReadOnly("hwloc_bitmap_intersects"));
  add(new ModelReadOnly("hwloc_bitmap_isequal"));
  add(new ModelReadOnly("hwloc_bitmap_isfull"));
  add(new ModelReadOnly("hwloc_bitmap_isincluded"));
  add(new ModelReadOnly("hwloc_bitmap_isset"));
  add(new ModelReadOnly("hwloc_bitmap_iszero"));
  add(new ModelReadOnly("hwloc_bitmap_last"));
  add(new ModelCustom("hwloc_bitmap_list_asprintf", modelBitmapASPrintf));
  add(new ModelReadWrite("hwloc_bitmap_list_snprintf", {1, 0, 0}));
  add(new ModelReadWrite("hwloc_bitmap_list_sscanf", {1, 0}));
  add(new ModelReadOnly("hwloc_bitmap_next"));
  add(new ModelReadWrite("hwloc_bitmap_not", {0, 1}));
  add(new ModelReadWrite("hwloc_bitmap_only", {0, 1}));
  add(new ModelReadWrite("hwloc_bitmap_or", {0, 1}));
  add(new ModelReadWrite("hwloc_bitmap_set", {0, 1}));
  add(new ModelReadWrite("hwloc_bitmap_set_ith_ulong", {0, 1, 1}));
  add(new ModelReadWrite("hwloc_bitmap_set_range", {0, 1, 1}));
  add(new ModelReadWrite("hwloc_bitmap_singlify", {0}));
  add(new ModelReadWrite("hwloc_bitmap_snprintf", {0, 1, 1}));
  add(new ModelReadWrite("hwloc_bitmap_sscanf", {0, 1}));
  add(new ModelCustom("hwloc_bitmap_taskset_asprintf", modelBitmapASPrintf));
  add(new ModelReadWrite("hwloc_bitmap_taskset_snprintf", {0, 1, 1}));
  add(new ModelReadWrite("hwloc_bitmap_taskset_sscanf", {0, 1}));
  add(new ModelReadOnly("hwloc_bitmap_to_ith_ulong"));
  add(new ModelReadOnly("hwloc_bitmap_to_ulong"));
  add(new ModelReadOnly("hwloc_bitmap_weight"));
  add(new ModelReadWrite("hwloc_bitmap_xor", {0, 1, 1}));
  add(new ModelReadWrite("hwloc_bitmap_zero", {0}));
}
