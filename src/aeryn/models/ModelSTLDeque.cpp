#include "ModelSTLDeque.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

using namespace llvm;

namespace moya {

// This is the deque layout in memory:
//
// deque = { T**, i64, { T*, T*, T*, T** }, { T*, T*, T*, T** } }
//
// When a deque is created, a single cell is allocated into which all the
// data is stored.
//
// All the T* pointers point to this cell.
// The T** pointers do not point to anything.
// All the T* and T** pointers move in lockstep - when one is modified, all are
// modified, when one is read, all are read.
//
//

ModelSTLDeque::ModelSTLDeque(const std::string& fname,
                             STDKind cont,
                             STDKind iter,
                             STDKind citer,
                             FuncID fn,
                             const Models& ms)
    : ModelSTLContainer(fname, cont, iter, citer, fn, ms) {
  switch(fn) {
  case ModelSTLDeque::InsertRange:
    processFn = static_cast<ExecuteFn>(&ModelSTLDeque::modelInsertRange);
    break;
  default:
      // Everything else will be handled in ModelSTLContainer()
      ;
  }
}

ModelSTLDeque::ModelSTLDeque(const std::string& fname,
                             STDKind cont,
                             FuncID fn,
                             const Models& ms)
    : ModelSTLDeque(fname,
                    cont,
                    STDKind::STD_STL_iterator_deque,
                    STDKind::STD_STL_iterator_deque,
                    fn,
                    ms) {
  ;
}

ModelSTLDeque::ModelSTLDeque(const std::string& fname,
                             FuncID fn,
                             const Models& ms)
    : ModelSTLDeque(fname,
                    STDKind::STD_STL_deque,
                    STDKind::STD_STL_iterator_deque,
                    STDKind::STD_STL_iterator_deque,
                    fn,
                    ms) {
  ;
}

Contents ModelSTLDeque::getBeginIterator(const Addresses& conts,
                                         const Instruction* call,
                                         const Function* f,
                                         Store& store,
                                         AnalysisStatus& changed) const {
  StructType* contTy = Metadata::getSTLContainerType(f);
  StructType* iterTy = Metadata::getSTLIteratorType(f);
  unsigned long offset = store.getStructLayout(contTy).getElementOffset(2);

  changed |= markContainerRead(conts, call, f, store);
  return store.readAs(conts, offset, iterTy, call, changed);
}

Contents ModelSTLDeque::getEndIterator(const Addresses& conts,
                                       const Instruction* call,
                                       const Function* f,
                                       Store& store,
                                       AnalysisStatus& changed) const {
  StructType* contTy = Metadata::getSTLContainerType(f);
  StructType* iterTy = Metadata::getSTLIteratorType(f);
  unsigned long offset = store.getStructLayout(contTy).getElementOffset(3);

  changed |= markContainerRead(conts, call, f, store);
  return store.readAs(conts, offset, iterTy, call, changed);
}

Addresses ModelSTLDeque::getIteratorNode(const Addresses& iters,
                                         const Instruction* call,
                                         const Function* f,
                                         Store& store,
                                         AnalysisStatus& changed) const {
  StructType* iterTy = Metadata::getSTLIteratorType(f);
  Addresses ret;

  for(const auto* ccomp :
      store.readAs<ContentComposite>(iters, iterTy, call, changed))
    ret.insert(ccomp->at(0).getAs<Address>(call));

  return ret;
}

Addresses ModelSTLDeque::getIteratorDeref(const Addresses& iters,
                                          const Instruction* call,
                                          const Function* f,
                                          Store& store,
                                          AnalysisStatus& changed) const {
  StructType* iterTy = Metadata::getSTLIteratorType(f);
  Addresses ret;

  for(const auto* ccomp :
      store.readAs<ContentComposite>(iters, iterTy, call, changed))
    ret.insert(ccomp->at(0).getAs<Address>(call));

  return ret;
}

Addresses ModelSTLDeque::getIteratorNew(const Addresses& iters,
                                        const Instruction* call,
                                        const Function* f,
                                        Store& store,
                                        AnalysisStatus& changed) const {
  return iters;
}

AnalysisStatus ModelSTLDeque::updateContainerPointers(const Address* base,
                                                      const Addresses& vals,
                                                      const Instruction* call,
                                                      const Function* f,
                                                      Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* pBufTy = Metadata::getSTLValueType(f)->getPointerTo();
  unsigned vtadj = getContainerVtableOffset(base, call, f, store);
  for(unsigned offset : {0, 8, 16})
    for(const Content* c : vals)
      store.write(store.make(base, vtadj + offset), c, pBufTy, call, changed);

  return changed;
}

AnalysisStatus ModelSTLDeque::modelInsertRange(const Instruction* call,
                                               const Function* f,
                                               const Vector<Contents>& args,
                                               Environment& env,
                                               Store& store) const {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  int hasStructRet = LLVMUtils::getArgument(f, 0)->hasStructRetAttr();
  unsigned long iterOffset = Metadata::getSTLIteratorOffset(f);
  unsigned long ptrOffset = Metadata::getSTLPointerOffset(f);
  StructType* iterTy = Metadata::getSTLIteratorType(f);
  const Addresses& conts = args.at(hasStructRet).getAs<Address>(call);
  const Addresses& addrs1 = args.at(hasStructRet + 2).getAs<Address>(call);
  const Addresses& addrs2 = args.at(hasStructRet + 3).getAs<Address>(call);

  changed |= markContainerWrite(conts, call, f, store);
  changed |= writeToContainer(conts, addrs1, iterOffset, call, f, store);
  changed |= writeToContainer(conts, addrs2, iterOffset, call, f, store);

  // Prior to C++11, this function does not return a value
  if(LLVMUtils::hasReturn(f)) {
    if(hasStructRet)
      for(const Address* sret : args.at(0).getAs<Address>(call))
        for(const Address* cont : conts)
          store.copy(sret, store.make(cont, ptrOffset), iterTy, call, changed);
    else
      env.push(args.at(hasStructRet + 1));
  }

  return changed;
}

} // namespace moya
