#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

using namespace llvm;
using namespace moya;

static AnalysisStatus modelMmap(const ModelCustom& model,
                                const Instruction* call,
                                const Function* f,
                                const Vector<Contents>& args,
                                Environment& env,
                                Store& store,
                                const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= AbstractUtils::malloc(call, env, store, caller);

  return changed;
}

static AnalysisStatus modelMunmap(const ModelCustom& model,
                                  const Instruction* call,
                                  const Function* f,
                                  const Vector<Contents>& args,
                                  Environment& env,
                                  Store& store,
                                  const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= AbstractUtils::free(
      call, args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, caller);

  return changed;
}

static AnalysisStatus modelReadV(const ModelCustom& model,
                                 const Instruction* call,
                                 const Function* f,
                                 const Vector<Contents>& args,
                                 Environment& env,
                                 Store& store,
                                 const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("Unimplemented model for readv");

  return changed;
}

static AnalysisStatus modelWriteV(const ModelCustom& model,
                                  const Instruction* call,
                                  const Function* f,
                                  const Vector<Contents>& args,
                                  Environment& env,
                                  Store& store,
                                  const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("Unimplemented model for writev");

  return changed;
}

static AnalysisStatus modelProcessVMReadV(const ModelCustom& model,
                                          const Instruction* call,
                                          const Function* f,
                                          const Vector<Contents>& args,
                                          Environment& env,
                                          Store& store,
                                          const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("Unimplemented model for process_vm_readv");

  return changed;
}

static AnalysisStatus modelProcessVMWriteV(const ModelCustom& model,
                                           const Instruction* call,
                                           const Function* f,
                                           const Vector<Contents>& args,
                                           Environment& env,
                                           Store& store,
                                           const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("Unimplemented model for process_vm_writev");

  return changed;
}

static AnalysisStatus modelSysFs(const ModelCustom& model,
                                 const Instruction* call,
                                 const Function* f,
                                 const Vector<Contents>& args,
                                 Environment& env,
                                 Store& store,
                                 const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("Unimplemented model for sysfs");

  return changed;
}

static AnalysisStatus modelClone(const ModelCustom& model,
                                 const Instruction* call,
                                 const Function* f,
                                 const Vector<Contents>& args,
                                 Environment& env,
                                 Store& store,
                                 const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  moya_error("Unimplemented model for clone");

  return changed;
}

static AnalysisStatus modelSignal(const ModelCustom& model,
                                  const Instruction* call,
                                  const Function* f,
                                  const Vector<Contents>& args,
                                  Environment& env,
                                  Store& store,
                                  const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(const Content* c : args.at(1))
    if(const auto* code = dyn_cast<ContentCode>(c)) {
      const Function* f = code->getFunction();
      const Argument* a = LLVMUtils::getArgument(f, 0);
      store.write(env.lookupArg(a), args.at(0), a->getType(), call, changed);
    }

  env.push(args.at(1));

  return changed;
}

static AnalysisStatus modelRealPath(const ModelCustom& model,
                                    const Instruction* call,
                                    const Function* f,
                                    const Vector<Contents>& args,
                                    Environment& env,
                                    Store& store,
                                    const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= model.markRead(
      args.at(0), LLVMUtils::getArgumentType(f, 0), env, store, call);
  changed |= model.markWrite(
      args.at(1), LLVMUtils::getArgumentType(f, 1), env, store, call);

  // If the user passes null, then realpath allocates memory, else it returns
  // the second parameter which contains the full path. We need to model both
  // behaviors
  Type* i8 = Type::getInt8Ty(caller->getContext());
  const Address* buffer = AbstractUtils::allocate(i8, store, call, caller);
  store.write(buffer, store.getRuntimeScalar(), i8, call, changed);

  // The same goes for return values. Both cases must be modelled
  Contents ret;
  ret.insert(args.at(1));
  ret.insert(buffer);
  env.push(ret);

  return changed;
}

void Models::initSysCalls(const Module& module) {
  add(new ModelReadWrite("accept", {1, 0, 0}));
  add(new ModelReadWrite("accept4", {1, 0, 0, 1}));
  add(new ModelReadOnly("access"));
  add(new ModelReadOnly("faccessat"));
  add(new ModelReadOnly("acct"));
  add(new ModelReadOnly("key_serial_t_add_key"));
  add(new ModelReadWrite("adjtimex", {0}));
  add(new ModelReadWrite("ntp_adjtime", {0}));
  add(new ModelReadOnly("alarm"));
  add(new ModelReadOnly("alloc_hugepages"));
  add(new ModelReadOnly("free_hugepages"));
  add(new ModelReadWrite("bind", {1, 0, 1}));
  add(new ModelReadWrite("bpf", {1, 0, 1}));
  add(new ModelReadWrite("cacheflush", {0, 1, 1}));
  add(new ModelReadWrite("capget", {0, 0}));
  add(new ModelReadWrite("capset", {0, 1}));
  add(new ModelReadOnly("chdir"));
  add(new ModelReadOnly("fchdir"));
  add(new ModelReadOnly("chmod"));
  add(new ModelReadOnly("fchmod"));
  add(new ModelReadOnly("fchmodat"));
  add(new ModelReadOnly("chown"));
  add(new ModelReadOnly("fchown"));
  add(new ModelReadOnly("lchown"));
  add(new ModelReadOnly("fchownat"));
  add(new ModelReadOnly("chroot"));
  add(new ModelReadWrite("clock_getres", {1, 0}));
  add(new ModelReadWrite("clock_gettime", {1, 0}));
  add(new ModelReadWrite("clock_settime", {1, 1}));
  add(new ModelReadWrite("clock_nanosleep", {1, 1, 1, 0}));
  add(new ModelCustom("clone", modelClone));
  add(new ModelReadOnly("close"));
  add(new ModelReadOnly("connect"));
  add(new ModelReadWrite("copy_file_range", {1, 0, 1, 0, 1, 1}));
  add(new ModelReadOnly("open"));
  add(new ModelReadOnly("creat"));
  add(new ModelReadOnly("openat"));
  add(new ModelReadOnly("dup"));
  add(new ModelReadOnly("dup2"));
  add(new ModelReadOnly("dup3"));
  add(new ModelReadOnly("epoll_create"));
  add(new ModelReadOnly("epoll_create1"));
  add(new ModelReadWrite("epoll_ctl", {1, 1, 1, 0}));
  add(new ModelReadWrite("epoll_wait", {1, 0, 1, 1}));
  add(new ModelReadWrite("epoll_pwait", {1, 0, 1, 1}));
  add(new ModelReadOnly("eventfd"));
  add(new ModelReadOnly("execveat"));
  add(new ModelReadOnly("_exit"));
  add(new ModelReadOnly("_Exit"));
  add(new ModelReadOnly("posix_fadvise"));
  add(new ModelReadOnly("fallocate"));
  add(new ModelReadOnly("fanotify_init"));
  add(new ModelReadOnly("fanotify_mark"));
  add(new ModelReadOnly("fcntl"));
  add(new ModelReadOnly("fsync"));
  add(new ModelReadOnly("fdatasync"));
  add(new ModelReadWrite("getxattr", {1, 1, 0, 1}));
  add(new ModelReadWrite("lgetxattr", {1, 1, 0, 1}));
  add(new ModelReadWrite("fgetxattr", {1, 1, 0, 1}));
  add(new ModelReadOnly("init_module"));
  add(new ModelReadOnly("finit_module"));
  add(new ModelReadWrite("listxattr", {1, 0, 1}));
  add(new ModelReadWrite("llistxattr", {1, 0, 1}));
  add(new ModelReadWrite("flistxattr", {1, 0, 1}));
  add(new ModelReadOnly("flock"));
  add(new ModelReadOnly("removexattr"));
  add(new ModelReadOnly("lremovexattr"));
  add(new ModelReadOnly("fremovexattr"));
  add(new ModelReadOnly("setxattr"));
  add(new ModelReadOnly("lsetxattr"));
  add(new ModelReadOnly("fsetxattr"));
  add(new ModelReadWrite("stat", {1, 0}));
  add(new ModelReadWrite("fstat", {1, 0}));
  add(new ModelReadWrite("lstat", {1, 0}));
  add(new ModelReadWrite("fstatat", {1, 1, 0, 1}));
  add(new ModelReadWrite("statfs", {1, 0}));
  add(new ModelReadWrite("fstatfs", {1, 0}));
  add(new ModelReadOnly("truncate"));
  add(new ModelReadOnly("ftruncate"));
  add(new ModelReadWrite("futex", {0, 1, 1, 0, 0, 1}));
  add(new ModelReadOnly("futimesat"));
  add(new ModelReadWrite("get_mempolicy", {0, 0, 1, 1, 1}));
  add(new ModelReadWrite("get_robust_list", {1, 0, 0}));
  add(new ModelReadWrite("set_robust_list", {0, 1}));
  add(new ModelReadWrite("get_thread_area", {0}));
  add(new ModelReadWrite("set_thread_area", {0}));
  add(new ModelReadWrite("getcpu", {0, 0, 0}));
  add(new ModelReadWrite("getcwd", {0, 1}, Model::ReturnSpec::Arg0));
  add(new ModelReadWrite("getwd", {0}, Model::ReturnSpec::Arg0));
  add(new ModelReadOnly("get_current_dir_name"));
  add(new ModelReadWrite("getdents", {1, 0, 1}));
  add(new ModelReadWrite("getdents64", {1, 0, 1}));
  add(new ModelReadOnly("getgid"));
  add(new ModelReadOnly("getegid"));
  add(new ModelReadOnly("getuid"));
  add(new ModelReadOnly("geteuid"));
  add(new ModelReadWrite("getgroups", {1, 0}));
  add(new ModelReadWrite("setgroups", {1, 0}));
  add(new ModelReadWrite("getitimer", {1, 0}));
  add(new ModelReadWrite("setitimer", {1, 1, 0}));
  add(new ModelReadWrite("getpeername", {1, 0, 0}));
  add(new ModelReadOnly("getpagesize"));
  add(new ModelReadOnly("getpgid"));
  add(new ModelReadOnly("setpgid"));
  add(new ModelReadOnly("getpgrp"));
  add(new ModelReadOnly("setpgrp"));
  add(new ModelReadOnly("getpid"));
  add(new ModelReadOnly("getppid"));
  add(new ModelReadOnly("getpriority"));
  add(new ModelReadOnly("setpriority"));
  add(new ModelReadWrite("getrandom", {0, 1, 1}));
  add(new ModelReadWrite("getresuid", {0, 0, 0}));
  add(new ModelReadWrite("getresgid", {0, 0, 0}));
  add(new ModelReadWrite("getrlimit", {1, 0}));
  add(new ModelReadWrite("setrlimit", {1, 1}));
  add(new ModelReadWrite("prlimit", {1, 1, 1, 0}));
  add(new ModelReadWrite("getrusage", {1, 0}));
  add(new ModelReadOnly("getsid"));
  add(new ModelReadWrite("getsockname", {1, 0, 0}));
  add(new ModelReadWrite("getsockopt", {1, 1, 1, 0, 0}));
  add(new ModelReadWrite("setsockopt", {1, 1, 1, 0, 1}));
  add(new ModelReadOnly("gettid"));
  add(new ModelReadWrite("gettimeofday", {0, 0}));
  add(new ModelReadOnly("settimeofday"));
  add(new ModelReadOnly("inotify_add_watch"));
  add(new ModelReadOnly("inotify_init"));
  add(new ModelReadOnly("inotify_init1"));
  add(new ModelReadOnly("inotify_rm_watch"));
  add(new ModelReadWrite("io_cancel", {1, 0, 0}));
  add(new ModelReadOnly("io_destroy"));
  add(new ModelReadWrite("io_getevents", {1, 1, 1, 0, 0}));
  add(new ModelReadWrite("io_setup", {1, 0}));
  add(new ModelReadWrite("io_submit", {1, 1, 0}));
  add(new ModelReadOnly("ioctl"));
  add(new ModelReadOnly("ioperm"));
  add(new ModelReadOnly("iopl"));
  add(new ModelReadOnly("ioprio_get"));
  add(new ModelReadOnly("ioprio_set"));
  add(new ModelReadWrite("ipc", {1, 1, 1, 1, 0, 1}));
  add(new ModelReadOnly("kcmp"));
  add(new ModelReadWrite("kexec_load", {1, 1, 0, 1}));
  add(new ModelReadOnly("kexec_file_load"));
  add(new ModelReadOnly("keyctl"));
  add(new ModelReadOnly("kill"));
  add(new ModelReadOnly("link"));
  add(new ModelReadOnly("linkat"));
  add(new ModelReadOnly("listen"));
  add(new ModelReadWrite("lookup_dcookie", {1, 0, 1}));
  add(new ModelReadWrite("madvise", {0, 1, 1}));
  add(new ModelReadWrite("mbind", {0, 1, 1, 1, 1, 1}));
  add(new ModelReadOnly("memfd_create"));
  add(new ModelReadOnly("migrate_pages"));
  add(new ModelReadWrite("mincore", {0, 1, 0}));
  add(new ModelReadOnly("mkdir"));
  add(new ModelReadOnly("mkdirat"));
  add(new ModelReadOnly("mknod"));
  add(new ModelReadOnly("mknodat"));
  add(new ModelReadOnly("mlock"));
  add(new ModelReadOnly("mlock2"));
  add(new ModelReadOnly("munlock"));
  add(new ModelReadOnly("mlockall"));
  add(new ModelReadOnly("munlockall"));
  add(new ModelCustom("mmap", modelMmap));
  add(new ModelCustom("munmap", modelMunmap));
  add(new ModelCustom("mmap2", modelMmap));
  add(new ModelReadWrite("modify_ldt", {1, 0, 1}));
  add(new ModelReadOnly("mount"));
  add(new ModelReadWrite("move_pages", {1, 1, 0, 1, 0, 1}));
  add(new ModelReadWrite("mprotect", {0, 1, 1}));
  add(new ModelReadWrite("mq_getsetattr", {1, 0, 0}));
  add(new ModelReadOnly("mq_notify"));
  add(new ModelReadOnly("mq_open"));
  add(new ModelReadWrite("mq_receive", {1, 0, 1, 0}));
  add(new ModelReadWrite("mq_timedreceive", {1, 0, 1, 0, 1}));
  add(new ModelReadOnly("mq_send"));
  add(new ModelReadOnly("mq_timedsend"));
  add(new ModelReadOnly("mq_unlink"));
  add(new ModelReadOnly("mremap", Model::ReturnSpec::Arg0));
  add(new ModelReadWrite("msgctl", {1, 1, 0}));
  add(new ModelReadOnly("msgget"));
  add(new ModelReadWrite("msgsnd", {1, 1, 1, 1}));
  add(new ModelReadWrite("msgrcv", {1, 0, 1, 1, 1}));
  add(new ModelReadOnly("msync"));
  add(new ModelReadWrite("name_to_handle_at", {1, 1, 0, 0, 1}));
  add(new ModelReadWrite("open_by_handle_at", {1, 0, 1}));
  add(new ModelReadWrite("nanosleep", {1, 0}));
  add(new ModelReadWrite("nfsservctl", {1, 0, 0}));
  add(new ModelReadOnly("nice"));
  add(new ModelReadOnly("pause"));
  add(new ModelReadWrite("perf_event_open", {0, 1, 1, 1, 1}));
  add(new ModelReadOnly("personality"));
  add(new ModelReadWrite("perfmonctl", {1, 1, 0, 1}));
  add(new ModelReadWrite("pipe", {0}));
  add(new ModelReadWrite("pipe2", {0, 1}));
  add(new ModelReadOnly("pivot_root"));
  add(new ModelReadWrite("poll", {0, 1, 1}));
  add(new ModelReadWrite("ppoll", {0, 1, 1, 1}));
  add(new ModelReadOnly("prctl"));
  add(new ModelReadWrite("pread", {1, 0, 1, 1}));
  add(new ModelReadOnly("pwrite"));
  add(new ModelCustom("readv", modelReadV));
  add(new ModelCustom("writev", modelWriteV));
  add(new ModelCustom("preadv", modelReadV));
  add(new ModelCustom("pwritev", modelWriteV));
  add(new ModelCustom("process_vm_readv", modelProcessVMReadV));
  add(new ModelCustom("process_vm_writev", modelProcessVMWriteV));
  add(new ModelReadWrite("select", {1, 0, 0, 0, 0}));
  add(new ModelReadWrite("pselect", {1, 0, 0, 0, 1, 1}));
  add(new ModelReadWrite("ptrace", {1, 1, 1, 0}));
  add(new ModelReadOnly("quotactl"));
  add(new ModelReadWrite("read", {1, 0, 1}));
  add(new ModelReadOnly("readahead"));
  // FIXME: There's a problem here where the syscalls conflict with a libc
  // function of the same name. This is a wider problem which we really need
  // to handle somehow
  // add(new ModelReadWrite("readdir", {1, 0, 1}));
  add(new ModelReadWrite("readlink", {1, 0, 1}));
  add(new ModelReadWrite("readlinkat", {1, 1, 0, 1}));
  add(new ModelReadOnly("reboot"));
  add(new ModelReadWrite("recv", {1, 0, 1, 1}));
  add(new ModelReadWrite("recvfrom", {1, 0, 1, 1, 0, 0}));
  add(new ModelReadWrite("recvmsg", {1, 0, 1}));
  add(new ModelReadWrite("recvmmsg", {1, 0, 1, 1, 0}));
  add(new ModelReadOnly("remap_file_pages"));
  add(new ModelReadOnly("rename"));
  add(new ModelReadOnly("renameat"));
  add(new ModelReadOnly("renameat2"));
  add(new ModelReadOnly("request_key"));
  add(new ModelReadOnly("restart_syscall"));
  add(new ModelReadOnly("rmdir"));
  add(new ModelReadWrite("sigaction", {1, 1, 0}));
  add(new ModelReadWrite("sigpending", {0}));
  add(new ModelReadWrite("sigprocmask", {1, 1, 0}));
  add(new ModelReadWrite("rt_sigqueueinfo", {1, 1, 0}));
  add(new ModelReadWrite("rt_tgsigqueueinfo", {1, 1, 1, 0}));
  add(new ModelReadOnly("sigreturn"));
  add(new ModelReadOnly("sigsuspend"));
  add(new ModelReadWrite("sigwaitinfo", {1, 0}));
  add(new ModelReadWrite("sigtimedwait", {1, 0, 1}));
  add(new ModelReadOnly("sched_get_priority_max"));
  add(new ModelReadOnly("sched_get_priority_min"));
  add(new ModelReadWrite("sched_setaffinity", {1, 1, 0}));
  add(new ModelReadWrite("sched_getaffinity", {1, 1, 0}));
  add(new ModelReadWrite("sched_setattr", {1, 0, 1}));
  add(new ModelReadWrite("sched_getattr", {1, 0, 1, 1}));
  add(new ModelReadOnly("sched_setparam"));
  add(new ModelReadWrite("sched_getparam", {1, 0}));
  add(new ModelReadOnly("sched_setscheduler"));
  add(new ModelReadOnly("sched_getscheduler"));
  add(new ModelReadWrite("sched_rr_get_interval", {1, 0}));
  add(new ModelReadOnly("sched_yield"));
  add(new ModelReadWrite("seccomp", {1, 1, 1}));
  add(new ModelReadOnly("semctl"));
  add(new ModelReadOnly("semget"));
  add(new ModelReadWrite("semop", {1, 0, 1}));
  add(new ModelReadWrite("semtimedop", {1, 0, 1, 1}));
  add(new ModelReadOnly("send"));
  add(new ModelReadOnly("sendto"));
  add(new ModelReadOnly("sendmsg"));
  add(new ModelReadWrite("sendfile", {1, 1, 0, 1}));
  add(new ModelReadWrite("sendmmsg", {1, 0, 1, 1}));
  add(new ModelReadOnly("set_mempolicy"));
  add(new ModelReadWrite("set_tid_address", {0}));
  add(new ModelReadWrite("getdomainname", {0, 1}));
  add(new ModelReadOnly("setdomainname"));
  add(new ModelReadOnly("setns"));
  add(new ModelReadOnly("setsid"));
  add(new ModelReadWrite("gethostname", {0, 1}));
  add(new ModelReadOnly("sethostname"));
  add(new ModelReadOnly("sgetmask"));
  add(new ModelReadOnly("shmat"));
  add(new ModelReadWrite("shmctl", {1, 1, 0}));
  add(new ModelReadOnly("shmdt"));
  add(new ModelReadOnly("shmget"));
  add(new ModelReadOnly("shutdown"));
  add(new ModelReadWrite("sigaltstack", {1, 0}));
  add(new ModelReadOnly("signalfd"));
  add(new ModelCustom("signal", modelSignal));
  add(new ModelCustom("sigset", modelSignal));
  add(new ModelReadOnly("sighold"));
  add(new ModelReadOnly("sigrelse"));
  add(new ModelReadOnly("sigignore"));
  add(new ModelReadOnly("socket"));
  add(new ModelReadOnly("socketcall"));
  add(new ModelReadWrite("socketpair", {1, 1, 1, 0}));
  add(new ModelReadWrite("splice", {1, 0, 1, 0, 1, 1}));
  add(new ModelReadOnly("spu_create"));
  add(new ModelReadWrite("spu_run", {1, 0, 0}));
  add(new ModelReadOnly("ssetmask"));
  add(new ModelReadOnly("stime"));
  add(new ModelReadOnly("swapon"));
  add(new ModelReadOnly("swapoff"));
  add(new ModelReadOnly("sync_file_range"));
  add(new ModelReadOnly("syncfs"));
  add(new ModelCustom("sysfs", modelSysFs));
  add(new ModelReadWrite("sysinfo", {0}));
  add(new ModelReadWrite("syslog", {1, 0, 1}));
  add(new ModelReadOnly("symlink"));
  add(new ModelReadOnly("symlinkat"));
  add(new ModelReadOnly("tee"));
  add(new ModelReadOnly("tkill"));
  add(new ModelReadOnly("tgkill"));
  add(new ModelReadWrite("time", {0}));
  add(new ModelReadWrite("timer_create", {1, 0, 0}));
  add(new ModelReadWrite("timer_delete", {1}));
  add(new ModelReadOnly("timer_getoverrun"));
  add(new ModelReadWrite("timer_settime", {1, 1, 1, 0}));
  add(new ModelReadWrite("timer_gettime", {1, 0}));
  add(new ModelReadOnly("timerfd_create"));
  add(new ModelReadWrite("timerfd_settime", {1, 1, 1, 0}));
  add(new ModelReadWrite("timerfd_gettime", {1, 0}));
  add(new ModelReadWrite("times", {0}));
  add(new ModelReadOnly("umask"));
  add(new ModelReadOnly("umount"));
  add(new ModelReadOnly("umount2"));
  add(new ModelReadWrite("uname", {0}));
  add(new ModelReadOnly("unlinkat"));
  add(new ModelReadOnly("unshare"));
  add(new ModelReadOnly("uselib"));
  add(new ModelReadWrite("ustate", {1, 0}));
  add(new ModelReadOnly("utime"));
  add(new ModelReadOnly("utimes"));
  add(new ModelReadOnly("utimensat"));
  add(new ModelReadOnly("futimens"));
  add(new ModelReadOnly("vhangup"));
  add(new ModelReadWrite("vm86old", {0}));
  add(new ModelReadWrite("vm86", {1, 0}));
  add(new ModelReadOnly("vmsplice"));
  add(new ModelReadWrite("wait3", {0, 1, 0}));
  add(new ModelReadWrite("wait4", {1, 0, 1, 0}));
  add(new ModelReadWrite("wait", {0}));
  add(new ModelReadWrite("waitpid", {1, 0, 1}));
  add(new ModelReadWrite("waitid", {1, 1, 0, 1}));
  add(new ModelReadOnly("write"));
  add(new ModelReadWrite("bzero", {0, 1}));
  add(new ModelReadOnly("usleep"));
  add(new ModelReadOnly("sleep"));
  add(new ModelReadOnly("get_nprocs"));
  add(new ModelReadOnly("get_nprocs_conf"));
  add(new ModelReadOnly("gethostbyname",
                        Model::ReturnSpec::AllocateAndInitialize));
  add(new ModelReadOnly("gethostbyaddr",
                        Model::ReturnSpec::AllocateAndInitialize));
  add(new ModelReadOnly("sethostent"));
  add(new ModelReadOnly("endhostent"));
  add(new ModelReadOnly("herror"));
  add(new ModelReadOnly("hstrerror", Model::ReturnSpec::AllocateAndInitialize));
  add(new ModelReadOnly("gethostent",
                        Model::ReturnSpec::AllocateAndInitialize));
  add(new ModelReadOnly("gethostbyname2",
                        Model::ReturnSpec::AllocateAndInitialize));
  add(new ModelCustom("realpath", modelRealPath));
  add(new ModelReadWrite("inet_pton", {1, 1, 0}, true));

  add(new ModelReadWrite("_IO_getc", {0}, true));
  add(new ModelReadWrite("_IO_putc", {0}, true));

  // These should just be one of the POSIX *stat() variants, but I think they
  // are being wrapped with a macro somewhere
  add(new ModelReadWrite("__lxstat", {1, 0}));
  add(new ModelReadWrite("__xstat", {1, 0}));
}
