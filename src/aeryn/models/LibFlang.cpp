#include "ModelCustom.h"
#include "ModelReadOnly.h"
#include "ModelReadWrite.h"
#include "Models.h"
#include "aeryn/AbstractUtils.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;
using namespace moya;

static AnalysisStatus model_alloc(unsigned allocArg,
                                  const ModelCustom& model,
                                  const Instruction* cll,
                                  const Function* f,
                                  const Vector<Contents>& args,
                                  Environment& env,
                                  Store& store,
                                  const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const auto* call = cast<CallInst>(cll);
  for(unsigned i = 0; i < allocArg; i++)
    changed
        |= model.markRead(args.at(i),
                          LLVMUtils::getAnalysisType(call->getArgOperand(i)),
                          env,
                          store,
                          call);

  for(unsigned i = allocArg + 1; i < args.size(); i++)
    changed
        |= model.markRead(args.at(i),
                          LLVMUtils::getAnalysisType(call->getArgOperand(i)),
                          env,
                          store,
                          call);

  if(not env.contains(call)) {
    Type* allocTy = moya::Metadata::getCallAllocated(call);
    Type* pAllocTy = allocTy->getPointerTo();
    const Address* allocated
        = AbstractUtils::allocate(allocTy, store, call, caller);
    for(const auto* addr : args.at(allocArg).getAs<Address>(call))
      store.write(addr, allocated, pAllocTy, call, changed);
    env.add(call);
  }

  return changed;
}

static AnalysisStatus model_auto_alloc(const ModelCustom& model,
                                       const Instruction* inst,
                                       const Function* f,
                                       const Vector<Contents>& args,
                                       Environment& env,
                                       Store& store,
                                       const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed
      |= AbstractUtils::malloc(dyn_cast<CallInst>(inst), env, store, caller);

  return changed;
}

static AnalysisStatus model_auto_dealloc(const ModelCustom& model,
                                         const Instruction* call,
                                         const Function* f,
                                         const Vector<Contents>& args,
                                         Environment& env,
                                         Store& store,
                                         const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* ty = Type::getInt8Ty(call->getContext());
  changed |= AbstractUtils::free(call, args.at(0), ty, env, store, caller);

  return changed;
}

static AnalysisStatus model_str_malloc(const ModelCustom& model,
                                       const Instruction* call,
                                       const Function* f,
                                       const Vector<Contents>& args,
                                       Environment& env,
                                       Store& store,
                                       const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  if(not env.contains(call)) {
    Type* allocTy = Type::getInt8Ty(f->getContext());
    Type* pAllocTy = allocTy->getPointerTo();
    const Address* allocated
        = AbstractUtils::allocate(allocTy, store, call, caller);
    for(const auto* addr : args.at(1).getAs<Address>(call))
      store.write(addr, allocated, pAllocTy, call, changed);
    store.write(allocated, store.getRuntimeScalar(), allocTy, call, changed);
    env.add(call, allocated);
  }

  env.push(env.lookup(call));

  return changed;
}

static AnalysisStatus model_str_free(const ModelCustom& model,
                                     const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store,
                                     const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* ty = Type::getInt8Ty(call->getContext());
  changed |= AbstractUtils::free(call, args.at(0), ty, env, store, caller);

  env.push(store.getRuntimeScalar());

  return changed;
}

static AnalysisStatus model_copy_f77_ptr(const ModelCustom& model,
                                         const Instruction* call,
                                         const Function* f,
                                         const Vector<Contents>& args,
                                         Environment& env,
                                         Store& store,
                                         const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* allocTy = LLVMUtils::getInnermost(
      LLVMUtils::getAnalysisType(LLVMUtils::getArgOperand(call, 3)));
  PointerType* pAllocTy = allocTy->getPointerTo();
  unsigned allocSize = store.getTypeAllocSize(allocTy);

  if(not env.contains(call)) {
    const Address* allocated
        = AbstractUtils::allocate(allocTy, store, call, caller);
    for(const auto* addr : args.at(3).getAs<Address>(call))
      store.write(addr, allocated, pAllocTy, call, changed);
    env.add(call);
  }

  for(const auto* dst :
      store.readAs<Address>(args.at(3), pAllocTy, call, changed))
    for(const auto* src :
        store.readAs<Address>(args.at(0), pAllocTy, call, changed))
      changed |= AbstractUtils::memcpyImpl(dst, src, allocSize, store, call);

  return changed;
}

static AnalysisStatus model_copy_f77(const ModelCustom& model,
                                     const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store,
                                     const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* allocTy = LLVMUtils::getInnermost(
      LLVMUtils::getAnalysisType(LLVMUtils::getArgOperand(call, 3)));
  PointerType* pAllocTy = allocTy->getPointerTo();
  unsigned allocSize = store.getTypeAllocSize(allocTy);

  if(not env.contains(call)) {
    const Address* allocated
        = AbstractUtils::allocate(allocTy, store, call, caller);
    for(const auto* addr : args.at(3).getAs<Address>(call))
      store.write(addr, allocated, pAllocTy, call, changed);
    env.add(call);
  }

  for(const auto* dst :
      store.readAs<Address>(args.at(3), pAllocTy, call, changed))
    for(const Address* src : args.at(0).getAs<Address>(call))
      changed |= AbstractUtils::memcpyImpl(dst, src, allocSize, store, call);

  return changed;
}

static AnalysisStatus model_copy_f90(const ModelCustom& model,
                                     const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store,
                                     const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  Type* allocTy = LLVMUtils::getInnermost(
      LLVMUtils::getAnalysisType(LLVMUtils::getArgOperand(call, 2)));
  PointerType* pAllocTy = allocTy->getPointerTo();
  unsigned allocSize = store.getTypeAllocSize(allocTy);

  if(not env.contains(call)) {
    const Address* allocated
        = AbstractUtils::allocate(allocTy, store, call, caller);
    for(const auto* addr : args.at(2).getAs<Address>(call))
      store.write(addr, allocated, pAllocTy, call, changed);
    env.add(call);
  }

  for(const auto* dst :
      store.readAs<Address>(args.at(2), pAllocTy, call, changed))
    for(const auto* src :
        store.readAs<Address>(args.at(0), pAllocTy, call, changed))
      changed |= AbstractUtils::memcpyImpl(dst, src, allocSize, store, call);

  return changed;
}

static AnalysisStatus model_memcpy(const ModelCustom& model,
                                   const Instruction* call,
                                   const Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  changed |= AbstractUtils::memcpy(call, args, env, store, caller);

  return changed;
}

static AnalysisStatus model_alloc4(const ModelCustom& model,
                                   const Instruction* call,
                                   const Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const Function* caller) {
  return model_alloc(4, model, call, f, args, env, store, caller);
}

static AnalysisStatus model_alloc5(const ModelCustom& model,
                                   const Instruction* call,
                                   const Function* f,
                                   const Vector<Contents>& args,
                                   Environment& env,
                                   Store& store,
                                   const Function* caller) {
  return model_alloc(5, model, call, f, args, env, store, caller);
}

static AnalysisStatus model_dealloc(unsigned deallocArg,
                                    const ModelCustom& model,
                                    const Instruction* call,
                                    const Function* f,
                                    const Vector<Contents>& args,
                                    Environment& env,
                                    Store& store,
                                    const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  for(unsigned i = 0; i < deallocArg; i++)
    changed |= model.markRead(
        args.at(i),
        LLVMUtils::getAnalysisType(LLVMUtils::getArgOperand(call, i)),
        env,
        store,
        call);

  for(unsigned i = deallocArg + 1; i < args.size(); i++)
    changed |= model.markRead(
        args.at(i),
        LLVMUtils::getAnalysisType(LLVMUtils::getArgOperand(call, i)),
        env,
        store,
        call);

  Type* ty = LLVMUtils::getAnalysisTypeAs<PointerType>(
                 LLVMUtils::getArgOperand(call, deallocArg))
                 ->getElementType();
  changed
      |= AbstractUtils::free(call, args.at(deallocArg), ty, env, store, caller);

  return changed;
}

static AnalysisStatus model_dealloc1(const ModelCustom& model,
                                     const Instruction* call,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store,
                                     const Function* caller) {
  return model_dealloc(1, model, call, f, args, env, store, caller);
}

// The flang descriptors are structured like this
//
// void* ptr, void* offset, [n x i32]
//
// The first pointer is the pointer to the actual array.
// I'm not acutally sure what the second offset is from
// The array of integers specifies the shape of the Fortran array
//
// When arrays descriptors are passed around, a pointer to the integer array
// is what actually gets passed. When any of the Fortran runtime array
// processing routines are called, 16 (sizeof(void*) * 2) is always
// subtracted from the address of the descriptor to get the pointer.
// Unfortunately, to model things correctly, we need to do this too because
// we might lose aliasing information otherwise
//
static Addresses get_ptr_addr_from_desc(const ModelCustom& model,
                                        const Contents& contents,
                                        const Instruction* call,
                                        Store& store,
                                        AnalysisStatus& changed) {
  Addresses ret;

  for(const auto* addr : contents.getAs<Address>(call))
    ret.insert(store.make(addr, -2 * store.getPointerSize()));

  return ret;
}

static Addresses get_offset_addr_from_desc(const ModelCustom& model,
                                           const Contents& contents,
                                           const Instruction* call,
                                           Store& store,
                                           AnalysisStatus& changed) {
  Addresses ret;

  for(const auto* addr : contents.getAs<Address>(call))
    ret.insert(store.make(addr, -1 * store.getPointerSize()));

  return ret;
}

static AnalysisStatus model_array_nullify(const ModelCustom& model,
                                          const Instruction* inst,
                                          const Function* f,
                                          const Vector<Contents>& args,
                                          Environment& env,
                                          Store& store,
                                          const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const auto* call = cast<CallInst>(inst);

  Type* pi8 = Type::getInt8PtrTy(call->getContext());
  for(const Address* addr :
      get_ptr_addr_from_desc(model, args.at(1), call, store, changed))
    store.write(addr, store.getNullptr(), pi8, inst, changed);

  return changed;
}

static AnalysisStatus model_array_assn(const ModelCustom& model,
                                       const Instruction* inst,
                                       const Function* f,
                                       const Vector<Contents>& args,
                                       Environment& env,
                                       Store& store,
                                       const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  const auto* call = cast<CallInst>(inst);
  changed |= model.markWrite(args.at(1),
                             LLVMUtils::getAnalysisType(call->getArgOperand(1)),
                             env,
                             store,
                             call);

  env.push(args.at(2));

  return changed;
}

static Type* get_available_string_type(Type* ty) {
  if(auto* aty = dyn_cast<ArrayType>(ty))
    return get_available_string_type(aty->getElementType());
  else if(auto* sty = dyn_cast<StructType>(ty))
    return get_available_string_type(sty->getElementType(0));
  return ty;
}

static AnalysisStatus model_str_copy(const ModelCustom& model,
                                     const Instruction* inst,
                                     const Function* f,
                                     const Vector<Contents>& args,
                                     Environment& env,
                                     Store& store,
                                     const Function* caller) {
  AnalysisStatus changed = AnalysisStatus::Unchanged;

  // Because of the madness of Flang's madness about converting everything into
  // a byte array, when propagating types, we can't reliably distinguish between
  // the use of a struct and the use of it's first element.
  //
  // Because we know that this function will only ever be used with strings,
  // we special case this and check if the argument is a struct and if the
  // first argument is a sequence of characters. If it is, we mark only that
  // sequence as having been read/written.
  //
  const auto* call = dyn_cast<CallInst>(inst);
  if(auto* pty
     = LLVMUtils::getAnalysisTypeAs<PointerType>(call->getArgOperand(1)))
    changed |= model.markWrite(args.at(1),
                               get_available_string_type(pty->getElementType()),
                               env,
                               store,
                               call);

  env.push(store.getRuntimeScalar());

  return changed;
}

// static AnalysisStatus model_array_nullify(const ModelCustom& model,
//                                           Instruction* inst, Function* f,
//                                           const Vector<Contents>& args,
//                                           Environment& env, Store& store,
//                                           Function* caller) {
//   AnalysisStatus changed = AnalysisStatus::Unchanged;

//   return changed;
// }

static std::string mkf90(const std::string& name,
                         const std::string& suffix = "") {
  std::stringstream ss;
  ss << "f90_" << name;
  if(suffix.length())
    ss << "_" << suffix;
  return ss.str();
}

static std::string mkio(const std::string& name,
                        const std::string& suffix = "") {
  std::stringstream ss;
  ss << "f90io_" << name;
  if(suffix.length())
    ss << "_" << suffix;
  return ss.str();
}

static std::string mkftn(const std::string& name,
                         const std::string& suffix = "") {
  std::stringstream ss;
  ss << "fort_" << name;
  if(suffix.length())
    ss << "_" << suffix;
  return ss.str();
}

static std::string mkftnio(const std::string& name,
                           const std::string& suffix = "") {
  std::stringstream ss;
  ss << "ftnio_" << name;
  if(suffix.length())
    ss << "_" << suffix;
  return ss.str();
}

static void
mk(Models& ms, const std::string& base, const std::string& suffix = "") {
  ms.add(new ModelReadOnly(mkf90(base, suffix)));
  ms.add(new ModelReadOnly(mkftn(base, suffix)));
}

static void mk(Models& ms,
               const std::string& base,
               const std::initializer_list<int>& isConst,
               const std::string& suffix = "") {
  ms.add(new ModelReadWrite(mkf90(base, suffix), isConst));
  ms.add(new ModelReadWrite(mkftn(base, suffix), isConst));
}

void Models::initLibFlang(const Module& module) {
  Models& ms = *this;

  add(new ModelReadOnly("fort_init"));

  add(new ModelCustom("fort_ptr_assn_i8", model_array_assn));

  // FIXME: There's probably more with these stupid suffixes. Will this madness
  // ever end?
  for(const std::string& suffix : {"", "i8"}) {
    add(new ModelCustom(mkf90("alloc", suffix), model_alloc4));
    add(new ModelCustom(mkf90("alloc03", suffix), model_alloc4));
    add(new ModelCustom(mkf90("alloc04", suffix), model_alloc4));
    add(new ModelCustom(mkf90("alloc04a", suffix), model_alloc4));
    add(new ModelCustom(mkf90("alloc03_chk", suffix), model_alloc4));
    add(new ModelCustom(mkf90("alloc04_chk", suffix), model_alloc4));
    add(new ModelCustom(mkf90("alloc04_chka", suffix), model_alloc4));
    add(new ModelCustom(mkf90("kalloc", suffix), model_alloc4));
    add(new ModelCustom(mkf90("calloc", suffix), model_alloc4));
    add(new ModelCustom(mkf90("calloc03", suffix), model_alloc4));
    add(new ModelCustom(mkf90("calloc04", suffix), model_alloc4));
    add(new ModelCustom(mkf90("calloc04a", suffix), model_alloc4));
    add(new ModelCustom(mkf90("kcalloc", suffix), model_alloc4));

    add(new ModelCustom(mkf90("ptr_alloc", suffix), model_alloc4));
    add(new ModelCustom(mkf90("ptr_alloc03", suffix), model_alloc4));
    add(new ModelCustom(mkf90("ptr_alloc04", suffix), model_alloc4));
    add(new ModelCustom(mkf90("ptr_alloc04a", suffix), model_alloc4));
    add(new ModelCustom(mkf90("ptr_calloc", suffix), model_alloc4));
    add(new ModelCustom(mkf90("ptr_calloc03", suffix), model_alloc4));
    add(new ModelCustom(mkf90("ptr_calloc04", suffix), model_alloc4));
    add(new ModelCustom(mkf90("ptr_calloc04a", suffix), model_alloc4));
    add(new ModelCustom(mkf90("ptr_kalloc", suffix), model_alloc4));
    add(new ModelCustom(mkf90("ptr_kalloc03", suffix), model_alloc4));
    add(new ModelCustom(mkf90("ptr_kalloc04", suffix), model_alloc4));

    add(new ModelCustom(mkf90("ptr_src_alloc03", suffix), model_alloc5));
    add(new ModelCustom(mkf90("ptr_src_alloc04", suffix), model_alloc5));
    add(new ModelCustom(mkf90("ptr_src_alloc04a", suffix), model_alloc5));
    add(new ModelCustom(mkf90("ptr_src_calloc03", suffix), model_alloc5));
    add(new ModelCustom(mkf90("ptr_src_calloc04", suffix), model_alloc5));
    add(new ModelCustom(mkf90("ptr_src_calloc04a", suffix), model_alloc5));

    add(new ModelCustom(mkf90("dealloc", suffix), model_dealloc1));
    add(new ModelCustom(mkf90("dealloc03", suffix), model_dealloc1));
    add(new ModelCustom(mkf90("dealloc03a", suffix), model_dealloc1));
    add(new ModelCustom(mkf90("dealloc_mbr", suffix), model_dealloc1));
    add(new ModelCustom(mkf90("dealloc_mbr03", suffix), model_dealloc1));
    add(new ModelCustom(mkf90("dealloc_mbr03a", suffix), model_dealloc1));

    add(new ModelReadOnly(mkf90("allocated", suffix)));

    add(new ModelReadWrite(mkf90("template", suffix), {0, 1, 1, 1}));
    add(new ModelReadWrite(mkf90("template1", suffix), {0, 1, 1, 1, 1, 1}));
    add(new ModelReadWrite(mkf90("template2", suffix),
                           {0, 1, 1, 1, 1, 1, 1, 1}));
    add(new ModelReadWrite(mkf90("template3", suffix),
                           {0, 1, 1, 1, 1, 1, 1, 1, 1, 1}));

    add(new ModelReadWrite(mkf90("set_intrin_type", suffix), {0, 1}));
    add(new ModelReadWrite(mkf90("set_type", suffix), {0, 1}));
    add(new ModelReadWrite(mkf90("test_and_set_type", suffix), {0, 1}));

    add(new ModelReadWrite(mkftn("instance", suffix), {0, 1, 1, 1, 1}));

    // We don't need to specify the status for all the arguments because those
    // that are not specified are assumed to be constant and will be marked
    // correctly
    mk(ms, "sect", {0}, suffix);
    mk(ms, "sect1", {0}, suffix);
    mk(ms, "sect1v", {0}, suffix);
    mk(ms, "sect2", {0}, suffix);
    mk(ms, "sect2v", {0}, suffix);
    mk(ms, "sect3", {0}, suffix);
    mk(ms, "sect3v", {0}, suffix);

    add(new ModelCustom(mkf90("copy_f77_arg", suffix), model_copy_f77_ptr));
    add(new ModelCustom(mkf90("copy_f77_argl", suffix), model_copy_f77_ptr));
    add(new ModelCustom(mkf90("copy_f77_argw", suffix), model_copy_f77_ptr));
    add(new ModelCustom(mkf90("copy_f77_argsl", suffix), model_copy_f77));
    add(new ModelCustom(mkf90("copy_f90_arg", suffix), model_copy_f90));
    add(new ModelCustom(mkf90("copy_f90_argl", suffix), model_copy_f90));

    add(new ModelCustom(mkf90("mcopy1", suffix), model_memcpy));
    add(new ModelCustom(mkf90("mcopy2", suffix), model_memcpy));
    add(new ModelCustom(mkf90("mcopy4", suffix), model_memcpy));
    add(new ModelCustom(mkf90("mcopy8", suffix), model_memcpy));

    add(new ModelCustom(mkf90("auto_allocv", suffix), model_auto_alloc));
    add(new ModelCustom(mkf90("auto_alloc", suffix), model_auto_alloc));
    add(new ModelCustom(mkf90("auto_alloc04", suffix), model_auto_alloc));
    add(new ModelCustom(mkf90("auto_calloc", suffix), model_auto_alloc));
    add(new ModelCustom(mkf90("auto_calloc04", suffix), model_auto_alloc));

    add(new ModelCustom(mkf90("auto_dealloc", suffix), model_auto_dealloc));

    for(const std::string& prefix : {"", "k"}) {
      for(const std::string& fn : {"maxloc", "maxlocs"}) {
        std::stringstream ss;
        ss << prefix << fn;
        mk(ms, ss.str(), {0}, suffix);
      }
    }

    // The rest of the arguments in the conformable_* functions are scalars
    // so no need to explicitly specify their status
    add(new ModelReadWrite(mkf90("conformable_nd", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_dn", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_nn", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_dd", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_d1v", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_d2v", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_d3v", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_dnv", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_1dv", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_2dv", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_3dv", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_ndv", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_11v", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_22v", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_33v", suffix), {0}));
    add(new ModelReadWrite(mkf90("conformable_nnv", suffix), {0}));

    add(new ModelReadWrite(mkf90("rseed", suffix), {0, 0, 0, 1, 1, 1}));
    add(new ModelReadWrite(mkf90("rnumd", suffix), {0, 1}));
    add(new ModelReadWrite(mkftn("rseed", suffix), {0, 0, 0, 1, 1, 1}));
    add(new ModelReadWrite(mkftn("rnumd", suffix), {0, 1}));
  }

  add(new ModelReadWrite(mkf90("mset4"), {0, 1, 1}));

  add(new ModelReadOnly(mkio("src_info03")));
  add(new ModelReadOnly(mkio("src_info03a")));
  add(new ModelReadOnly(mkio("src_xinfo03")));

  add(new ModelReadOnly(mkio("encode_fmt")));
  add(new ModelReadOnly(mkio("encode_fmta")));
  add(new ModelReadOnly(mkio("encode_fmtv")));

  add(new ModelReadWrite(mkio("open"), {1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1,
                                        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkio("opena"), {1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1,
                                         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkio("open2003"),
                         {1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1,
                          1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkio("open2003a"),
                         {1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1,
                          1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkio("open03"), {0, 1, 1, 1, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkio("open03a"), {0, 1, 1, 1, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkio("open_cvt"), {0, 1, 1}));
  add(new ModelReadWrite(mkio("open_cvta"), {0, 1, 1}));
  add(new ModelReadWrite(mkio("open_share"), {0, 1, 1}));

  add(new ModelReadWrite(mkio("close"), {1, 1, 0, 1, 1}));
  add(new ModelReadWrite(mkio("closea"), {1, 1, 0, 1, 1}));

  add(new ModelReadOnly(mkftnio("fmt_read")));
  add(new ModelReadOnly(mkftnio("fmt_reada")));
  add(new ModelReadOnly(mkftnio("fmt_read64")));
  add(new ModelReadOnly(mkftnio("fmt_write")));
  add(new ModelReadOnly(mkftnio("fmt_writea")));
  add(new ModelReadOnly(mkftnio("fmt_write64")));
  add(new ModelReadOnly(mkftnio("ldr")));
  add(new ModelReadOnly(mkftnio("ldr64")));
  add(new ModelReadOnly(mkftnio("ldw")));
  add(new ModelReadOnly(mkftnio("ldw64")));
  add(new ModelReadWrite(mkftnio("usw_read"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkftnio("usw_write"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkftnio("unf_read"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkftnio("unf_reada"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkftnio("unf_read64"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkftnio("unf_read64_aa"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkftnio("unf_write"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkftnio("unf_write64"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkftnio("unf_write64_aa"), {1, 1, 1, 0, 1}));

  add(new ModelReadWrite(mkio("fmtw_init03"), {0, 1, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkio("fmtw_init03a"), {0, 1, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkio("fmtw_init"), {1, 1, 1, 0, 1, 1, 1}));
  add(new ModelReadWrite(mkio("fmtw_inita"), {1, 1, 1, 0, 1, 1, 1}));
  add(new ModelReadWrite(mkio("fmtw_initv"), {1, 1, 1, 0, 1, 1, 1}));
  add(new ModelReadWrite(mkio("fmtw_intern_init"), {1, 1, 1, 0, 1, 1}));
  add(new ModelReadWrite(mkio("fmtw_intern_inita"), {1, 1, 1, 0, 1, 1}));
  add(new ModelReadWrite(mkio("fmtw_intern_initv"), {1, 1, 1, 0, 1, 1}));
  add(new ModelReadWrite(mkio("fmtw_intern_inite"), {1, 1, 1, 0, 1, 1}));
  add(new ModelReadWrite(mkio("fmtw_intern_initev"), {1, 1, 1, 0, 1, 1}));
  add(new ModelReadOnly(mkio("fmt_write")));
  add(new ModelReadOnly(mkio("fmt_writea")));
  add(new ModelReadOnly(mkio("fmt_write_a")));
  add(new ModelReadOnly(mkio("fmt_write_aa")));
  add(new ModelReadOnly(mkio("fmt_write64_a")));
  add(new ModelReadOnly(mkio("fmt_write64_aa")));
  add(new ModelReadOnly(mkio("fmtw_end")));
  add(new ModelReadOnly(mkio("sc_fmt_write")));
  add(new ModelReadOnly(mkio("sc_i_fmt_write")));
  add(new ModelReadOnly(mkio("sc_l_fmt_write")));
  add(new ModelReadOnly(mkio("sc_f_fmt_write")));
  add(new ModelReadOnly(mkio("sc_d_fmt_write")));
  add(new ModelReadOnly(mkio("sc_cf_fmt_write")));
  add(new ModelReadOnly(mkio("sc_cd_fmt_write")));

  add(new ModelReadWrite(mkio("fmt_read"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkio("fmt_reada"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkio("fmt_read64_a"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkio("fmtr_init2003"), {0, 1, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkio("fmtr_init2003a"), {0, 1, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkio("fmtr_init"), {1, 1, 1, 0, 1, 1, 1}));
  add(new ModelReadWrite(mkio("fmtr_inita"), {1, 1, 1, 0, 1, 1, 1}));
  add(new ModelReadWrite(mkio("fmtr_initv"), {1, 1, 1, 0, 1, 1, 1}));
  add(new ModelReadWrite(mkio("fmtr_intern_init"), {1, 1, 1, 0, 1, 1}));
  add(new ModelReadWrite(mkio("fmtr_intern_inita"), {1, 1, 1, 0, 1, 1}));
  add(new ModelReadWrite(mkio("fmtr_intern_initv"), {1, 1, 1, 0, 1, 1}));
  add(new ModelReadWrite(mkio("fmtr_intern_inite"), {1, 1, 1, 0, 1, 1}));
  add(new ModelReadWrite(mkio("fmtr_intern_initev"), {1, 1, 1, 0, 1, 1}));
  add(new ModelReadOnly(mkio("fmtr_end")));

  add(new ModelReadWrite(mkio("print_init"), {1, 1, 1, 0}));

  add(new ModelReadWrite(mkio("ldw_init"), {1, 1, 1, 0}));
  add(new ModelReadWrite(mkio("ldw_inita"), {1, 1, 1, 0}));
  add(new ModelReadWrite(mkio("ldw_init03"), {0}));
  add(new ModelReadWrite(mkio("ldw_init03a"), {0}));
  add(new ModelReadWrite(mkio("ldw_intern_init"), {1, 1, 1, 0}));
  add(new ModelReadWrite(mkio("ldw_intern_inita"), {1, 1, 1, 0}));
  add(new ModelReadWrite(mkio("ldw_intern_inite"), {1, 1, 1, 0, 1}));
  add(new ModelReadOnly(mkio("ldw")));
  add(new ModelReadOnly(mkio("ldw_a")));
  add(new ModelReadOnly(mkio("ldw64_a")));
  add(new ModelReadOnly(mkio("ldw64_aa")));
  add(new ModelReadOnly(mkio("ldw_end")));
  add(new ModelReadOnly(mkio("sc_ch_ldw")));

  add(new ModelReadWrite(mkio("ldr_init"), {1, 1, 1, 0}));
  add(new ModelReadWrite(mkio("ldr_inita"), {1, 1, 1, 0}));
  add(new ModelReadWrite(mkio("ldr_init03"), {0}));
  add(new ModelReadWrite(mkio("ldr_init03a"), {0}));
  add(new ModelReadWrite(mkio("ldr_intern_init"), {1, 1, 1, 0}));
  add(new ModelReadWrite(mkio("ldr_intern_inita"), {1, 1, 1, 0}));
  add(new ModelReadWrite(mkio("ldr_intern_inite"), {1, 1, 1, 0, 1}));
  add(new ModelReadOnly(mkio("ldr")));
  add(new ModelReadOnly(mkio("ldra")));
  add(new ModelReadOnly(mkio("ldr_a")));
  add(new ModelReadOnly(mkio("ldr64_a")));
  add(new ModelReadOnly(mkio("ldr_end")));

  add(new ModelReadWrite(mkio("unf_read"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkio("unf_reada"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkio("unf_read_a"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkio("unf_read64"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkio("unf_read64_a"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkio("unf_read64_aa"), {1, 1, 1, 0, 1}));

  add(new ModelReadOnly(mkio("unf_write")));
  add(new ModelReadOnly(mkio("unf_write64")));
  add(new ModelReadOnly(mkio("unf_writea")));
  add(new ModelReadOnly(mkio("unf_write64_a")));
  add(new ModelReadOnly(mkio("unf_write64_aa")));

  add(new ModelReadWrite(mkio("unf_init"), {1, 1, 1, 1, 0}));
  add(new ModelReadOnly(mkio("unf_end")));

  add(new ModelReadWrite(mkio("usw_init"), {1, 1, 1, 1, 0}));
  add(new ModelReadWrite(mkio("usw_read"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkio("usw_read_a"), {1, 1, 1, 0, 1}));
  add(new ModelReadWrite(mkio("usw_read64_a"), {1, 1, 1, 0, 1}));
  add(new ModelReadOnly(mkio("usw_write")));
  add(new ModelReadOnly(mkio("usw_write64_a")));
  add(new ModelReadOnly(mkio("usw_end")));

  add(new ModelReadWrite(mkio("byte_read"), {1, 1, 0, 0}));
  add(new ModelReadWrite(mkio("byte_read64"), {1, 1, 0, 0}));
  add(new ModelReadOnly(mkio("byte_write")));
  add(new ModelReadOnly(mkio("byte_write64")));

  add(new ModelReadWrite(mkf90("template1v"), {0, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkf90("template2v"), {0, 1, 1, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkf90("template3v"), {0, 1, 1, 1, 1, 1, 1, 1, 1, 1}));

  add(new ModelReadOnly(mkf90("get_object_size")));
  add(new ModelReadOnly(mkf90("kget_object_size")));

  add(new ModelReadWrite(mkftn("free"), {0}));
  add(new ModelReadWrite(
      mkftn("freen"), {0}, Model::ReturnSpec::Default, true, false));

  add(new ModelReadOnly(mkf90("loc"), Model::ReturnSpec::Arg0));
  add(new ModelReadWrite(mkf90("min"), {1, 0, 0}));
  add(new ModelReadWrite(mkf90("max"), {1, 0, 0}));
  add(new ModelReadOnly(mkf90("kichar")));
  add(new ModelReadOnly(mkf90("nlen")));
  add(new ModelReadWrite(mkf90("adjustl"), {1, 1, 0, 1}));
  add(new ModelReadWrite(mkf90("adjustr"), {1, 1, 0, 1}));
  add(new ModelReadWrite(mkf90("adjustla"), {0, 1, 1, 1}));

  for(const std::string& suffix : {"", "1", "2", "4", "8"})
    for(const std::string& fn :
        {"lb", "ub", "lba", "uba", "lbaz", "ubaz", "int", "log"}) {
      std::stringstream ss;
      ss << mkf90(fn) << suffix;
      add(new ModelReadOnly(ss.str()));
    }

  add(new ModelReadOnly(mkf90("klb")));
  add(new ModelReadOnly(mkf90("kub")));

  add(new ModelReadWrite(mkf90("kuba"), {0}));
  add(new ModelReadWrite(mkf90("klbaz"), {0}));
  add(new ModelReadWrite(mkf90("kubaz"), {0}));

  for(const std::string& fn : {"bound",
                               "ubound",
                               "lbounda",
                               "ubounda",
                               "lboundaz",
                               "uboundaz",
                               "shape"}) {
    for(const std::string& suffix : {"", "1", "2", "4", "8"}) {
      std::stringstream ss;
      ss << fn << "_i" << suffix;
      mk(ms, ss.str(), {0});
    }

    std::stringstream ss;
    ss << "k" << fn;
    mk(ms, ss.str(), {0});
  }

  add(new ModelReadWrite(mkf90("achar"), {0, 1, 1}));
  add(new ModelReadWrite(mkf90("achara"), {0, 1, 1}));
  add(new ModelReadWrite(mkf90("repeat"), {0, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkf90("repeata"), {0, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkf90("trim"), {0, 1, 1, 1}));
  add(new ModelReadWrite(mkf90("trima"), {0, 1, 1, 1}));

  for(const std::string& prefix : {"", "k"})
    for(const std::string& fn : {"iachar",
                                 "verify",
                                 "index",
                                 "indexa",
                                 "size",
                                 "present",
                                 "presentc",
                                 "present_ptr",
                                 "len",
                                 "lena",
                                 "lentrim",
                                 "lentrima"}) {
      std::stringstream ss;
      ss << prefix << mkf90(fn);
      add(new ModelReadOnly(ss.str()));
    }

  for(const std::string& suffix : {"", "i8"}) {
    for(const std::string& prefix : {"", "k"}) {
      for(const std::string& fn : {"scan", "scana"}) {
        std::stringstream ss;
        ss << prefix << fn;
        add(new ModelReadWrite(mkf90(ss.str(), suffix), {1, 0, 1, 1, 1, 1}));
      }
    }
  }

  add(new ModelReadOnly(mkf90("leadz")));
  add(new ModelReadOnly(mkf90("popcnt")));
  add(new ModelReadOnly(mkf90("poppar")));
  add(new ModelReadOnly(mkf90("jmax0")));
  add(new ModelReadOnly(mkf90("max0")));
  add(new ModelReadOnly(mkf90("kmax")));
  add(new ModelReadOnly(mkf90("min0")));
  add(new ModelReadOnly(mkf90("mod")));

  add(new ModelReadOnly(mkf90("dble")));
  add(new ModelReadOnly(mkf90("real4")));
  add(new ModelReadOnly(mkf90("real8")));

  add(new ModelReadOnly(mkf90("amax1")));
  add(new ModelReadOnly(mkf90("dmax1")));
  add(new ModelReadOnly(mkf90("amin1")));
  add(new ModelReadOnly(mkf90("dmin1")));
  add(new ModelReadOnly(mkf90("amod")));
  add(new ModelReadOnly(mkf90("dmod")));
  add(new ModelReadOnly(mkf90("modulo")));
  add(new ModelReadOnly(mkf90("i8modulo")));
  add(new ModelReadOnly(mkf90("imodulo")));
  add(new ModelReadOnly(mkf90("amodulo")));
  add(new ModelReadOnly(mkf90("dmodulo")));

  add(new ModelReadWrite(mkf90("mmul_real8"),
                         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1}));

  for(const std::string& prefix : {"", "k"})
    for(const std::string& suffix : {"", "d"})
      for(const std::string& fn : {"expon",
                                   "frac",
                                   "nearest",
                                   "ceiling",
                                   "floor",
                                   "sel_int_kind",
                                   "sel_char_kind",
                                   "sel_real_kind"}) {
        std::stringstream ss;
        ss << prefix << mkf90(fn) << suffix;
        add(new ModelReadOnly(ss.str()));
      }

  for(const std::string& suffix : {"", "d"})
    for(const std::string& fn : {"rrspacing", "scale", "setexp", "spacing"}) {
      std::stringstream ss;
      ss << mkf90(fn) << suffix;
      add(new ModelReadOnly(ss.str()));
    }

  add(new ModelReadOnly("iargc_"));
  add(new ModelReadWrite("getarg_", {1, 0, 1}));
  add(new ModelReadOnly("cmd_arg_cnt"));
  add(new ModelReadOnly("kcmd_arg_cnt"));
  add(new ModelReadWrite("get_cmd_arg_", {1, 0, 0, 0, 0, 1}));
  add(new ModelReadWrite("get_cmd_", {0, 0, 0, 0, 1}));
  add(new ModelReadWrite("get_env_var_", {1, 0, 0, 0, 0, 0, 1}));

  add(new ModelCustom(mkf90("str_malloc"), model_str_malloc));
  add(new ModelCustom(mkf90("str_malloc_klen"), model_str_malloc));
  add(new ModelCustom(mkf90("str_copy"), model_str_copy));
  add(new ModelCustom(mkf90("str_copy_klen"), model_str_copy));
  add(new ModelCustom(mkf90("str_cpy1"), model_str_copy));
  add(new ModelCustom(mkf90("str_cpy1_klen"), model_str_copy));
  add(new ModelReadOnly(mkf90("str_index")));
  add(new ModelReadOnly(mkf90("str_index_klen")));
  add(new ModelReadOnly(mkf90("strcmp")));
  add(new ModelReadOnly(mkf90("strcmp_klen")));
  add(new ModelCustom(mkf90("str_free"), model_str_free));

  add(new ModelReadWrite(mkf90("exit"), {0}));
  add(new ModelReadWrite(mkf90("stop08"), {0, 1, 1}));
  add(new ModelReadWrite(mkf90("stop08a"), {0, 1, 1}));
  add(new ModelReadOnly(mkf90("stop")));
  add(new ModelReadOnly(mkf90("pause")));

  add(new ModelReadWrite(mkio("inquire2003"),
                         {1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                          1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkio("inquire2003a"),
                         {1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                          1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}));
  add(new ModelReadWrite(mkio("inquire2"), {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}));
  add(new ModelReadWrite(mkio("inquire03"),
                         {1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
  add(new ModelReadWrite(mkio("inquire03_2"),
                         {1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}));

  add(new ModelReadWrite(mkio("rewind"), {1, 1, 0}));

  add(new ModelCustom(mkftn("nullify"), model_array_nullify));
  add(new ModelCustom(mkftn("nullify_char"), model_array_nullify));
  add(new ModelCustom(mkftn("ptr_assn"), model_array_assn));
  add(new ModelCustom(mkftn("ptr_assnx"), model_array_assn));
  add(new ModelCustom(mkftn("ptr_assn_char"), model_array_assn));
  add(new ModelCustom(mkftn("ptr_assn_charx"), model_array_assn));
  add(new ModelCustom(mkftn("ptr_assn_dchar"), model_array_assn));
  add(new ModelCustom(mkftn("ptr_assn_dcharx"), model_array_assn));
  add(new ModelCustom(mkftn("ptr_assn_assumeshp"), model_array_assn));
  add(new ModelCustom(mkftn("ptr_assn_char_assumeshp"), model_array_assn));
  add(new ModelCustom(mkftn("ptr_assn_dchar_assumeshp"), model_array_assn));
  add(new ModelCustom(mkftn("ptr_shape_assn"), model_array_assn));
  add(new ModelCustom(mkftn("ptr_shape_assnx"), model_array_assn));

  add(new ModelReadOnly(mkftn("associated")));
  add(new ModelReadOnly(mkftn("associated_t")));
  add(new ModelReadOnly(mkftn("associated_char")));
  add(new ModelReadOnly(mkftn("associated_tchar")));

  add(new ModelReadOnly(mkftn("subchk")));
  add(new ModelReadOnly(mkftn("subchk64")));
  add(new ModelReadOnly(mkftn("ptrchk")));
  add(new ModelReadOnly(mkftn("ptrcp")));

  add(new ModelReadOnly(mkf90("cmd_arg_cnt")));
  add(new ModelReadWrite(mkf90("get_cmd_arga"), {1, 0, 0, 0, 0, 1}));

  for(const std::string& suffix : {"", "i8"}) {
    for(int size : {1, 2, 4, 8}) {
      for(const std::string& type : {"int", "real", "log"}) {
        std::stringstream ss;
        ss << "matmul_" << type << size;
        ms.add(new ModelReadWrite(mkf90(ss.str(), suffix), {0, 1, 1, 0, 1, 1}));
      }
    }
  }

  for(int size : {1, 2, 4, 8}) {
    std::stringstream ss;
    ss << "mzero" << size;
    ms.add(new ModelReadWrite(mkf90(ss.str()), {0, 1}));
  }

  add(new ModelReadOnly("__get_size_of"));

  add(new ModelReadWrite("__mth_i_cdexp", {0, 1, 1}));
  add(new ModelReadWrite("__mth_i_cddiv", {0, 1, 1, 1, 1}));

  add(new ModelReadOnly("system_"));
}
