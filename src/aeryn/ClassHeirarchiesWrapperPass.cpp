#include "ClassHeirarchiesWrapperPass.h"
#include "Passes.h"
#include "common/Diagnostics.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;

ClassHeirarchiesWrapperPass::ClassHeirarchiesWrapperPass() : ModulePass(ID) {
  // initializeClassHeirarchiesWrapperPassPass(*PassRegistry::getPassRegistry());
}

StringRef ClassHeirarchiesWrapperPass::getPassName() const {
  return "Moya Class Heirarchies Wrapper";
}

void ClassHeirarchiesWrapperPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

const moya::ClassHeirarchies&
ClassHeirarchiesWrapperPass::getHeirarchies() const {
  return heirarchies;
}

bool ClassHeirarchiesWrapperPass::runOnModule(Module& module) {
  moya_message(getPassName());

  heirarchies.initialize(module);

  return false;
}

char ClassHeirarchiesWrapperPass::ID = 0;

static const char* name = "moya-class-heirarchy";
static const char* descr = "Compute the class heirarchies";
static const bool cfg = true;
static const bool analysis = true;

// INITIALIZE_PASS(ClassHeirarchiesWrapperPass, name, descr, cfg, analysis)

static RegisterPass<ClassHeirarchiesWrapperPass> X(name, descr, cfg, analysis);

Pass* createClassHeirarchiesWrapperPass() {
  return new ClassHeirarchiesWrapperPass();
}
