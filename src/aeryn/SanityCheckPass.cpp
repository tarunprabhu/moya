#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>

using namespace llvm;
using namespace moya;

class SanityCheckPass : public ModulePass {
public:
  static char ID;

public:
  SanityCheckPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

SanityCheckPass::SanityCheckPass() : ModulePass(ID) {
  ;
}

StringRef SanityCheckPass::getPassName() const {
  return "Moya Sanity Check";
}

void SanityCheckPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesAll();
}

bool SanityCheckPass::runOnModule(Module& module) {
  moya_message(getPassName());

  for(const Function& f : module.functions()) {
    for(auto i = inst_begin(f); i != inst_end(f); i++) {
      const Instruction* inst = &*i;
      Type* instTy = inst->getType();

      if(instTy->isVoidTy())
        continue;

      if(const auto* gep = dyn_cast<GetElementPtrInst>(inst)) {
        if(moya::Metadata::hasGEPIndex(gep)) {
          if((gep->getNumIndices() > 2))
            moya_error("GEP instructions with an overridden index cannot have "
                       "more than 2 operands: "
                       << getDiagnostic(gep));
          else if(not gep->hasAllConstantIndices())
            moya_error("GEP with non-constant indices cannot have an "
                       " overridden index");
        }
      }

      if(moya::Metadata::hasAnalysisType(inst)
         and not moya::Metadata::hasSane(inst))
        if(Type* analTy = moya::Metadata::getAnalysisType(inst))
          if((instTy->isPointerTy() and (not analTy->isPointerTy()))
             or (analTy->isPointerTy() and (not instTy->isPointerTy())))
            moya_error("Inconsistent analysis type: "
                       << *moya::Metadata::getAnalysisType(inst) << " at "
                       << getDiagnostic(inst));
    }
  }

  return false;
}

char SanityCheckPass::ID = 0;

static RegisterPass<SanityCheckPass>
    X("moya-sanity-check",
      "Sanity checks the code just prior to the static analysis",
      true,
      true);

Pass* createSanityCheckPass() {
  return new SanityCheckPass();
}
