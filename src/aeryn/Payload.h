#ifndef MOYA_AERYN_PAYLOAD_H
#define MOYA_AERYN_PAYLOAD_H

#include "Summary.h"
#include "common/PayloadBase.h"
#include "common/Vector.h"

#include <llvm/IR/Module.h>

namespace moya {

class Payload : public PayloadBase {
protected:
  const Vector<std::string>& bcs;
  const Vector<std::string>& pkls;

public:
  Payload(const std::string&,
          const Vector<std::string>& = Vector<std::string>(),
          const Vector<std::string>& = Vector<std::string>());

  void pickle(const Summary&);
  void pickle(const llvm::Module&);

  template <typename T>
  void pickle(const T& val) {
    PayloadBase::pickle<T>(val);
  }

  bool create();
};

} // namespace moya

#endif // MOYA_AERYN_PAYLOAD_H
