#ifndef MOYA_AERYN_CALLGRAPH_WRAPPER_PASS_H
#define MOYA_AERYN_CALLGRAPH_WRAPPER_PASS_H

#include "common/APITypes.h"
#include "common/Map.h"

#include <llvm/Pass.h>

namespace moya {

class CallGraph;

} // namespace moya

class CallGraphWrapperPass : public llvm::ModulePass {
public:
  static char ID;

protected:
  moya::Map<RegionID, std::unique_ptr<moya::CallGraph>> cgs;

public:
  CallGraphWrapperPass();

  bool hasCallGraph(RegionID) const;
  const moya::CallGraph& getCallGraph(RegionID) const;

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);
};

#endif // MOYA_AERYN_CALLGRAPH_WRAPPER_PASS_H
