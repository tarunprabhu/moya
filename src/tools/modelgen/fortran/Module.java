import java.util.ArrayList;

public class Module {
  private String name;
  private ArrayList<Function> functions;

  public Module() {
    this.name = null;
    this.functions = new ArrayList<Function>();
  }

  public Module(String name) {
    this.name = name.toLowerCase();
    this.functions = new ArrayList<Function>();
  }

  public String getName() { return this.name; }
  
  // Functions don't have to be within a module, and if that is the case,
  // we create a "fake" global module
  public boolean isGlobal() { return this.name == null; }

  public void addFunction(Function f) { this.functions.add(f); }

  public ArrayList<Function> getFunctions() { return this.functions; }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("module ");
    if(this.name != null)
      sb.append(this.name);
    sb.append("{\n");
    for(Function f : functions) {
      sb.append(f.toString());
      sb.append("\n");
    }
    sb.append("}");
    return sb.toString();
  }
}
