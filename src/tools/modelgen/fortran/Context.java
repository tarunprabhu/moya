import java.util.HashMap;

// This is sort of like the LLVMContext object in that it keeps track of all
// the various types being used in the program
public class Context {
  private Type tyVoid;

  private Type tyChar;
  private Type tyInt;
  private Type tyReal;
  private Type tyComplex;
  private Type tyLogical;

  private HashMap<String, Type> tyDerived;
  private HashMap<String, Type> tyFunction;

  private HashMap<Type, Type> arrays;
  
  public Context() {
    this.tyVoid = new Type();
    this.tyChar = new Type("character");
    this.tyInt = new Type("integer");
    this.tyReal = new Type("real");
    this.tyComplex = new Type("complex");
    this.tyLogical = new Type("logical");

    this.tyDerived = new HashMap<String, Type>();

    this.arrays = new HashMap<Type, Type>();
    this.getArrayTy(this.getIntTy());
    this.getArrayTy(this.getRealTy());
    this.getArrayTy(this.getComplexTy());
    this.getArrayTy(this.getLogicalTy());
  }

  public Type getVoidTy() {
    return this.tyVoid;
  }

  public Type getCharTy() {
    return this.tyChar;
  }
  
  public Type getIntTy() {
    return this.tyInt;
  }

  public Type getRealTy() {
    return this.tyReal;
  }

  public Type getComplexTy() {
    return this.tyComplex;
  }

  public Type getLogicalTy() {
    return this.tyLogical;
  }

  public Type getDerivedTy(String name) {
    if(!this.tyDerived.containsKey(name)) {
      Type derived = new Type(name);
      derived.setDerived();
      this.tyDerived.put(name, derived);
    }
    return this.tyDerived.get(name);
  }

  public Type getFunctionTy(String ret) {
    if(!this.tyFunction.containsKey(ret)) {
      Type function = new Type(ret);
      function.setFunction();
      this.tyFunction.put(ret, function);
    }
    return this.tyFunction.get(ret);
  }

  public Type getArrayTy(Type type) {
    if(!this.arrays.containsKey(type))
      this.arrays.put(type, new Type(type));
    return this.arrays.get(type);
  }

  public Type getIntrinsicTy(String name) {
    switch(name) {
    case "void":
      return this.getVoidTy();
    case "character":
      return this.getCharTy();
    case "integer":
      return this.getIntTy();
    case "real":
    case "double":
      return this.getRealTy();
    case "complex":
      return this.getComplexTy();
    case "logical":
      return this.getLogicalTy();
    default:
      throw new java.lang.Error(
          String.format("Invalid primitive type: %s", name));
    }
  }
}
