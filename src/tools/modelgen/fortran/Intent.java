public enum Intent {
  // The status is unknown if the argument's status is unspecified. If the
  // status is unspecified, we cannot extract a model for the subroutine
  Unknown,
  In,
  Out;
}

