import java.util.ArrayList;

public class Function {
  private String name;
  private String result;
  private ArrayList<Parameter> params;
  private Type ret;
  private Module module;
  private Location location;

  public Function(Module module) {
    this.name = null;
    this.result = this.name;
    this.params = new ArrayList<Parameter>();
    this.ret = null;
    this.module = module;
    this.location = location;
  }

  public String getName() {
    return this.name;
  }

  public String getFullName() {
    StringBuilder sb = new StringBuilder();
    if(this.module != null && !this.module.isGlobal()) {
      sb.append("__");
      sb.append(this.module.getName());
      sb.append("_MOD_");
    }
    sb.append(this.name);
    return sb.toString();
  }

  public String getResult() {
    return this.result;
  }

  public Type getReturn() {
    return this.ret;
  }
  
  public ArrayList<Parameter> getParams() {
    return this.params;
  }

  public int getNumParams() {
    return this.params.size();
  }

  public Parameter getParam(int i) {
    return this.params.get(i);
  }

  public Parameter getParam(String name) {
    for(Parameter param : this.params)
      if(param.getName().equals(name))
        return param;
    return null;
  }

  public Location getLocation() {
    return this.location;
  }
  
  public void setName(String name) {
    this.name = name.toLowerCase();
    if(this.result == null)
      this.result = this.name;
  }

  public void setResult(String result) {
    this.result = result.toLowerCase();
  }

  public void addParam(String name) {
    this.params.add(new Parameter(name));
  }

  public void setParamIntent(String name, Intent intent) {
    for(Parameter param : this.params) {
      if(param.getName() == name) {
        param.setIntent(intent);
        return;
      }
    }
  }

  public void setReturn(Type type) {
    this.ret = type;
  }

  public void setLocation(Location location) {
    this.location = location;
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    if(this.ret != null && this.ret.isVoid())
      sb.append("  subroutine ");
    else
      sb.append("  function ");
    sb.append(this.name);
    sb.append(" {\n");
    sb.append("    args {\n");
    for(Parameter param : this.params) {
      sb.append("      ");
      sb.append(param.getName());
      sb.append(": ");
      sb.append(param.getType().toString());
      sb.append("\n");
    }
    sb.append("    }\n");
    sb.append("    return {\n");
    sb.append("      ");
    if(this.ret != null)
      sb.append(this.ret.toString());
    else
      sb.append("<<< UNKNOWN >>>");
    sb.append("\n");
    sb.append("    }\n");
    sb.append("  }");
    return sb.toString();
  }
}
