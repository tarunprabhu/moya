public class Parameter {
  private String name;
  private Type type;
  private Intent intent;

  public Parameter(String name) {
    this.name = name.toLowerCase();
    this.type = null;
    this.intent = Intent.Unknown;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public void setIntent(Intent intent) {
    this.intent = intent;
  }
  
  public String getName() {
    return this.name;
  }

  public Type getType() {
    return this.type;
  }

  public Intent getIntent() {
    return this.intent;
  }

  public String toString() {
    return String.format("%s: %s => %s",
                         this.getName(), this.getType(), this.intent);
  }
}
