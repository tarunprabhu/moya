public class Location {
  String file;
  int line;
  int col;
  
  public Location(String file, int line, int col) {
    this.file = file;
    this.line = line;
    this.col = col;
  }

  public String getFile() {
    return this.file;
  }

  public int getLine() {
    return this.line;
  }

  public int getColumn() {
    return this.col;
  }

  public String toString() {
    return String.format("%s:%d:%d", this.file, this.line, this.col);
  }
}
