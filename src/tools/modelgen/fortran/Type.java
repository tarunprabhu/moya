public class Type {
  String name;
  boolean array;
  boolean derived;
  boolean function;

  // This is the constructor used for a void type
  public Type() {
    this.name = null;
    this.init();
  }

  public Type(String name) {
    this.name = name.toLowerCase();
    this.init();
  }

  public Type(Type t) {
    this.name = t.name;
    this.array = true;
    this.derived = t.derived;
    this.function = t.function;
  }
  
  private void init() {
    this.array = false;
    this.derived = false;
    this.function = false;
  }
  
  public void setDerived() {
    this.derived = true;
  }

  public void setFunction() {
    this.function = true;
  }

  public boolean isScalar() {
    return !this.isVoid()
      && !this.isFunction() && !this.isDerived() && !this.isArray();
  }

  public boolean isFunction() {
    return this.function;
  }
  
  public boolean isDerived() {
    return this.derived;
  }

  public boolean isArray() {
    return this.array;
  }
  
  public boolean isVoid() {
    return this.name == null;
  }

  public boolean isString() {
    if(!this.isVoid() && this.name.equals("character") && this.array)
      return true;
    return false;
  }
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    if(this.isVoid()) {
      sb.append("void");
    } else {
      sb.append(this.name);
      if(this.isDerived())
        sb.append("*");
      if(this.isArray())
        sb.append("[]");
      if(this.isFunction())
        sb.append("()");
    }
    return sb.toString();
  }
}
