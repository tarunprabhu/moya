public class CodeGenerator {
  private String modelReadOnly;
  private String modelReadWrite;
  private String modelAdd;

  private boolean silent;
  
  public CodeGenerator() {
    this.modelReadOnly = "ModelReadOnly";
    this.modelReadWrite = "ModelReadWrite";
    this.modelAdd = "add";

    this.silent = false;
  }

  public void setReadOnly(String name) {
    this.modelReadOnly = name;
  }

  public void setReadWrite(String name) {
    this.modelReadWrite = name;
  }

  public void setAdd(String name) {
    this.modelAdd = name;
  }

  public void setSilent(boolean silent) {
    this.silent = silent;
  }

  public boolean getSilent() {
    return this.silent;
  }
  
  // This function should be called only after the function has been
  // fully initialized and before the model is generated
  // If one of the arguments is a function, then we can't generate a model
  // because we would need to know what the function does
  public boolean canGenerateModel(Function f) {
    if(f.getReturn() == null)
      return false;
    for(Parameter param : f.getParams())
      if(param.getIntent() == Intent.Unknown || param.getType().isFunction())
        return false;
    return true;
  }

  private boolean isReadOnly(Function f) {
    if(f.getReturn() == null || f.getReturn().isDerived() ||
       f.getReturn().isArray())
      return false;
    for(Parameter arg : f.getParams())
      if(arg.getIntent() != Intent.In)
        return false;
    return true;
  }

  public String generateStatus(Parameter param) {
    if(param.getIntent() == Intent.In)
      return "1";
    return "0";
  }
  
  public String generateModel(Function f) {
    StringBuilder sb = new StringBuilder();

    if(!this.canGenerateModel(f)) {
      if(!this.silent)
        sb.append(String.format("SKIPPING: %s @ %s\n", f.getName(),
                                f.getLocation().toString()));
      return sb.toString();
    }

    sb.append(this.modelAdd);
    sb.append("(new ");
    if(this.isReadOnly(f)) {
      sb.append(this.modelReadOnly);
      sb.append("(\"");
      sb.append(f.getFullName());
      sb.append("\")");
    } else {
      sb.append(this.modelReadWrite);
      sb.append("(\"");
      sb.append(f.getFullName());
      sb.append("\", {");
      if(f.getReturn().isDerived() || f.getReturn().isArray()) {
        sb.append("0");
        if(f.getReturn().isString())
          sb.append(", 0");
        if(f.getNumParams() > 0)
          sb.append(", ");
      }

      int numStrings = 0;
      if(f.getNumParams() > 0) {
        Parameter param = f.getParam(0);
        sb.append(this.generateStatus(param));
        if(param.getType().isString())
          numStrings++;
      }
      for(int i = 1; i < f.getNumParams(); i++) {
        Parameter param = f.getParam(i);
        sb.append(", ");
        sb.append(this.generateStatus(f.getParam(i)));
        if(param.getType().isString())
          numStrings++;
      }
      for(int i = 0; i < numStrings; i++)
        sb.append(", 1");
      sb.append("})");
    }
    sb.append(");");
    return sb.toString();
  }
}
