import org.antlr.runtime.CharStream;
import org.antlr.runtime.Token;
import fortran.ofp.parser.java.FortranParserActionNull;
import fortran.ofp.parser.java.IFortranParser;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

// We use this class to try and automatically extract models for Fortran
// functions.
//
// ******************************** IMPORTANT *********************************
//
//  This class has many, many known limitations and should be used with care.
//  This is not, and is not intended to be robust. I'll document some of the
//  limitations here, but also in the code. This is a convenience, but should
//  be used with care
//
// ****************************************************************************
//
// 1. Basic operation
//
// This class looks selectively at function and subroutine parameters and uses
// the specified intents to generate a model. If any of the parameters do not
// have an intent explicitly specified, the model WILL NOT be generated.
// There is NOT attempt to do any kind of analysis to determine the correctness
// of the model or of the intents.
//
// 2. Limitations
//
// - If any of the parameters to the functions/subroutines are themselves
//   functions, we may not be able to figure it out. Overlooking this could
//   result in very unpleasant bugs
//
// - Handling functions is notoriously brittle because there seem to be any
//   number of ways of specifying the return type of the function. When we do
//   detect the return type, the model is guaranteed to be correct
//

public class MoyaFortranVisitor extends FortranParserActionNull {
  private enum Scope { Module, Function, Nested }

  private class Source {
    private String path;
    private List<String> lines;

    public Source(String path) {
      this.path = path;
      try {
        FileSystem fs = FileSystems.getDefault();
        Charset cs = Charset.forName("UTF-8");

        this.lines = Files.readAllLines(fs.getPath(path), cs);
      } catch(Exception e) {
        this.lines = null;
      }
    }

    public String getPath() {
      return this.path;
    }

    public List<String> getLines() {
      return this.lines;
    }
  }

  private ArrayList<Source> files;

  private Context context;
  
  private Module globalModule;
  private Module currModule;
  private ArrayList<Module> modules;

  private int currIndex;
  private Function currFunction;
  private Type currType;
  private String currResult;
  // We need this separately to handle the case of the return type of the
  // function appearing before the function. In that case, the array spec is
  // seen before the type itself
  private boolean currArray;
  private Intent currIntent;
  private Parameter currParam;
  private Scope currScope;

  // Code generator
  private CodeGenerator cg;

  public MoyaFortranVisitor(String[] args, IFortranParser parser,
                            String filename) {
    super(args, parser, filename);

    this.files = new ArrayList<Source>();

    this.context = new Context();
    this.globalModule = new Module();
    this.currModule = this.globalModule;
    this.modules = new ArrayList<Module>();

    this.currIndex = -1;
    this.currFunction = null;
    this.currType = null;
    this.currResult = null;
    this.currArray = false;
    this.currIntent = Intent.Unknown;
    this.currParam = null;
    this.currScope = Scope.Module;

    this.modules.add(this.globalModule);

    // Parse the code generator options and set up the code generator
    this.cg = new CodeGenerator();

    for(int i = 0; i < args.length; i += 2)
      if(args[i].equals("--quiet"))
        cg.setSilent(true);
  }

  private void enterScope() {
    if(this.currScope == Scope.Module)
      this.currScope = Scope.Function;
    else if(this.currScope == Scope.Function)
      this.currScope = Scope.Nested;
  }

  private void exitScope() {
    if(this.currScope == Scope.Nested)
      this.currScope = Scope.Function;
    else if(this.currScope == Scope.Function)
      this.currScope = Scope.Module;
  }

  private String getIdentifier(Token id) {
    return id.getText().toLowerCase();
  }

  private String getKeyword(Token attr) {
    return attr.getText().toLowerCase();
  }

  private String getCurrFilePath() {
    if(this.currIndex < this.files.size())
      return this.files.get(this.currIndex).getPath();
    return null;
  }

  private List<String> getCurrFileLines() {
    if(this.currIndex < this.files.size())
      return this.files.get(this.currIndex).getLines();
    return null;
  }

  private void maybeCreateFunction() {
    if(this.currFunction == null)
      this.currFunction = new Function(this.currModule);
  }

  public void generic_name_list_part(Token ident) {
    this.maybeCreateFunction();
    this.currFunction.addParam(ident.getText());
  }

  public void dummy_arg(Token dummy) {
    this.maybeCreateFunction();
    this.currFunction.addParam(dummy.getText());
  }

  public void function_stmt(Token label, Token keyword, Token name, Token eos,
                            boolean hasGenericNameList, boolean hasSuffix) {
    if(this.currScope == Scope.Module) {
      this.maybeCreateFunction();
      this.currFunction.setName(this.getIdentifier(name));
      String path = this.getCurrFilePath();
      int line = name.getLine();
      int col = name.getCharPositionInLine();
      this.currFunction.setLocation(new Location(path, line, col));
      this.currModule.addFunction(this.currFunction);
      if(this.currType != null)
        this.currFunction.setReturn(this.currType);
      if(this.currResult != null)
        this.currFunction.setResult(this.currResult);

      this.currArray = false;
      this.currType = null;
      this.currResult = null;
    }
    this.enterScope();
  }

  public void end_function_stmt(Token label, Token keyword1, Token keyword2,
                                Token name, Token eos) {
    if(this.currScope == Scope.Function)
      this.currFunction = null;
    this.exitScope();
  }

  public void subroutine_stmt(Token label, Token keyword, Token name, Token eos,
                              boolean hasPrefix, boolean hasDummyArgList,
                              boolean hasBindingSpec, boolean hasArgSpecifier) {
    if(this.currScope == Scope.Module) {
      this.maybeCreateFunction();
      this.currFunction.setName(name.getText());
      String path = this.getCurrFilePath();
      int line = name.getLine();
      int col = name.getCharPositionInLine();
      this.currFunction.setLocation(new Location(path, line, col));
      this.currFunction.setReturn(this.context.getVoidTy());
      this.currModule.addFunction(this.currFunction);
      this.currType = null;
    }
    this.enterScope();
  }

  public void end_subroutine_stmt(Token label, Token keyword1, Token keyword2,
                                  Token name, Token eos) {
    if(this.currScope == Scope.Function)
      this.currFunction = null;
    this.exitScope();
  }

  public void module_stmt(Token label, Token moduleKeyword, Token id,
                          Token eos) {
    this.currModule = new Module(id.getText());
    this.modules.add(this.currModule);
    this.currScope = Scope.Module;
  }

  public void end_module_stmt(Token label, Token endKeyword,
                              Token moduleKeyword, Token id, Token eos) {
    this.currModule = this.globalModule;
    this.exitScope();
  }

  // If a type declaration is seen "outside" a function scope, then it is
  //
  // - The return type of a function
  //      OR
  // - A module-level or global variable declaration
  //
  // If we see a function declaration immediately after seeing a type spec,
  // then the type is the return type of the function, else it is a global
  // variable declaration that we can ignore.
  //
  // We only need to track types in the specification part of functions
  // and the "contains" part of modules. We ignore nested functions entirely
  //
  public void intrinsic_type_spec(Token keyword1, Token keyword2, int type,
                                  boolean hasKindSelector) {
    if(this.currScope != Scope.Nested) {
      this.currType = this.context.getIntrinsicTy(this.getIdentifier(keyword1));
      if(this.currArray)
        this.currType = this.context.getArrayTy(this.currType);
      this.currIntent = Intent.Unknown;
    }
  }

  public void derived_type_spec(Token typeName, boolean hasTypeParamSpecList) {
    if(this.currScope != Scope.Nested) {
      this.currType = this.context.getDerivedTy(this.getIdentifier(typeName));
      if(this.currArray)
        this.currType = this.context.getArrayTy(this.currType);
      this.currIntent = Intent.Unknown;
    }
  }

  public void char_selector(Token tk1, Token tk2, int kindOrLen1,
                            int kindOrLen2, boolean hasAsterisk) {
    this.currArray = true;
  }

  public void array_spec(int count) {
    if(this.currType != null) {
      this.currParam.setType(this.context.getArrayTy(this.currParam.getType()));
    }
  }

  public void intent_spec(Token intentKeyword1, Token intentKeyword2,
                          int intent) {
    if(this.currType != null) {
      if(intentKeyword1.getText().toLowerCase().equals("in"))
        this.currIntent = Intent.In;
      else
        this.currIntent = Intent.Out;
    }
  }

  public void attr_spec(Token attrKeyword, int attrNum) {
    String attr = this.getKeyword(attrKeyword);
    if(attr.equals("allocatable") || attr.equals("pointer") ||
       attr.equals("dimension")) {
      if(this.currType != null)
        this.currType = this.context.getArrayTy(this.currType);
    } else if(attr.equals("external")) {
      if(this.currType != null)
        this.currType.setFunction();
    }
  }

  public void allocatable_decl(Token id, boolean hasArraySpec,
                               boolean hasCoarraySpec) {
    if(this.currScope == Scope.Function) {
      Parameter param = this.currFunction.getParam(this.getIdentifier(id));
      if(param != null)
        param.setType(this.context.getArrayTy(param.getType()));
    }
  }

  public void entity_decl(Token id, boolean hasArraySpec,
                          boolean hasCoarraySpec, boolean hasCharLength,
                          boolean hasInitialization) {
    Type type = this.currType;
    if(hasArraySpec && (this.currType != null))
      type = this.context.getArrayTy(this.currType);

    String entity = this.getIdentifier(id);
    if(entity.equals(this.currFunction.getResult())) {
      this.currFunction.setReturn(type);
    } else {
      Parameter param = this.currFunction.getParam(entity);
      if(param != null) {
        param.setType(type);
        param.setIntent(this.currIntent);
      }
      this.currParam = param;
    }

    this.currArray = false;
  }

  // There's a shitty bug in OFP because it does not return the actual
  // identifier contained within the suffix. So we have to keep the whole
  // file in memory in parallel and get it out from there. It's safe to do
  // that because by the time we get here, we are certain that there are not
  // syntax errors
  public void suffix(Token resultKeyword, boolean hasProcLangBindSpec) {
    if(this.getCurrFileLines() != null) {
      String line = this.getCurrFileLines().get(resultKeyword.getLine() - 1);
      int col = resultKeyword.getCharPositionInLine();

      // Skip over the result keyword
      col += 6;

      // Skip spaces until first parenthesis
      while(line.charAt(col) != '(')
        col++;

      int begin = col + 1;
      int end = begin + 1;
      // Find the matching closing parenthesis
      while(line.charAt(end) != ')')
        end++;

      this.currResult = line.substring(begin, end);
    }
  }

  // When we finish the specification part, we ignore all types until we
  // get back to a module-level scope because we don't need the types of
  // inner functions
  public void specification_part(int numUseStmts, int numImportStmts,
                                 int numImplStmts, int numDeclConstructs) {
    this.currType = null;
  }

  // Keep a copy of the file in memory because there are bugs in OFP
  public void start_of_file(String filename, String path) {
    this.files.add(new Source(path));
    this.currIndex++;
  }

  public void end_of_file(String filename, String path) {
    this.currIndex--;
  }

  public void cleanUp() {
    ArrayList<Function> skipped = new ArrayList<Function>();
    for(Module module : this.modules)
      for(Function f : module.getFunctions())
        if(this.cg.canGenerateModel(f))
          System.out.println(cg.generateModel(f));
        else
          skipped.add(f);

    if(!cg.getSilent()) {
      if(skipped.size() > 0) {
        System.out.println("SKIPPED");
        for(Function f : skipped)
          System.out.format("  %s: %s\n", f.getName(), f.getLocation());
      }
    }
  }
}
