#include <iostream>
#include <sstream>
#include <string>

#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/Decl.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"

#include "Config.h"
#include "Support.h"

using namespace clang;
using namespace clang::driver;
using namespace clang::tooling;
using namespace std;

static string getFunctionNameForLanguage(string fname, SourceLang lang) {
  stringstream ss;
  switch(lang) {
  case SourceLang::C:
    ss << fname;
    break;
  case SourceLang::CXX:
    ss << fname;
    break;
  case SourceLang::Fort:
  case SourceLang::F77:
    ss << StringUtils::lower(fname) << "_";
    break;
  default:
    moya_error("Unsupported language");
  }
  return ss.str();
}

static llvm::cl::OptionCategory GenerateMPISigsCategory("generate-mpi-sigs");

class MyASTVisitor : public RecursiveASTVisitor<MyASTVisitor> {
private:
  PrintingPolicy policy;
  vector<string> cdecls, fdecls;

public:
  MyASTVisitor(LangOptions& langOpts) : policy(PrintingPolicy(langOpts)) {
    ;
  }

  vector<string> get_cdecls() const {
    return cdecls;
  }
  vector<string> get_fdecls() const {
    return fdecls;
  }

  string str(const Type* type) {
    stringstream ss;
    if(const BuiltinType* builtin = dyn_cast<BuiltinType>(type)) {
      return builtin->getName(policy);
    } else if(const PointerType* pointer = dyn_cast<PointerType>(type)) {
      const QualType& pointee = pointer->getPointeeType();
      ss << str(&*pointee->getUnqualifiedDesugaredType()) << "*";
      if(pointee.isConstQualified())
        ss << " const";
    } else if(const ArrayType* array = dyn_cast<ArrayType>(type)) {
      const QualType& inner = array->getElementType();
      ss << str(&*inner->getUnqualifiedDesugaredType()) << "[] ";
      if(inner.isConstQualified())
        ss << " const";
    } else if(const FunctionProtoType* function
              = dyn_cast<FunctionProtoType>(type)) {
      ss << str(function->getReturnType()->getUnqualifiedDesugaredType());
      ss << "(*)(";
      for(const QualType& param : function->getParamTypes())
        ss << str(param->getUnqualifiedDesugaredType()) << " ";
      ss << ")";
    } else if(const RecordType* record = dyn_cast<RecordType>(type)) {
      ss << "struct "
         << cast<RecordType>(record->getUnqualifiedDesugaredType())
                ->getDecl()
                ->getName()
                .str();
    } else {
      type->dump();
      return "unknown type";
    }
    return ss.str();
  }

  string getBraceList(std::vector<bool>& consts) {
    stringstream ss;
    ss << "{";
    if(consts.size())
      ss << " " << consts[0];
    for(unsigned i = 1; i < consts.size(); i++)
      ss << ", " << consts[i];
    ss << " }";
    return ss.str();
  }

  string getFname(string s) {
    stringstream ss;
    transform(s.begin(), s.end(), s.begin(), ::tolower);
    ss << s << "_";
    return ss.str();
  }

  bool isConst(const Type* type) {
    if(isa<BuiltinType>(type)) {
      return true;
    } else if(const PointerType* pointer = dyn_cast<PointerType>(type)) {
      const QualType& pointee = pointer->getPointeeType();
      if(pointee.isConstQualified())
        return true;
    } else if(const ArrayType* array = dyn_cast<ArrayType>(type)) {
      const QualType& inner = array->getElementType();
      if(inner.isConstQualified())
        return true;
    } else if(isa<FunctionProtoType>(type)) {
      return true;
    } else if(isa<RecordType>(type)) {
      // Don't bother
    } else {
      type->dump();
      moya_error("\nunknown type");
    }
    return false;
  }

  bool VisitFunctionDecl(FunctionDecl* f) {
    // DeclarationName DeclName = f->getNameInfo().getName();
    // string FuncName = DeclName.getAsString();
    string FuncName = f->getName().str();

    // The MPI_Init function is very different between C and Fortran, so
    // we add it in manually
    if(FuncName == "MPI_Init" or FuncName == "MPI_Init_thread") {
      // MPI_Init is different in C and Fortran, so we special case it
      cout << "ms.add(new ReadWriteModel(\"MPI_Init\", "
           << "i32, {pi32, pppi8}, {0, 0}));"
           << "\n";
      cout << "ms.add(new ReadWriteModel(\"mpi_init_\", "
           << "{pi32}, {0}));"
           << "\n";

      cout << "ms.add(new ReadWriteModel(\"MPI_Init_thread\", "
           << "i32, {pi32, pppi8, i32, pi32}, {0, 0, 1, 0}));"
           << "\n";
      cout << "ms.add(new ReadWriteModel(\"mpi_init_thread_\", "
           << "{pi32, pi32, pi32}, {1, 0, 0}));"
           << "\n";

      return false;
    }

    // With the new MPI_T functions, the structs may not be i32 in Fortran
    // FIXME: Remove this limitation
    if(FuncName.find("MPI_T") == 0 or FuncName.find("PMPI_T") == 0)
      return false;

    vector<string> names;
    vector<bool> consts;
    for(ParmVarDecl* param : f->params()) {
      const Type* type
          = param->getOriginalType()->getUnqualifiedDesugaredType();
      names.push_back(param->getName().str());
      consts.push_back(isConst(type));
    }

    stringstream ss;

    // C model
    ss << "ms.add(new ReadWriteModel(\"" << FuncName << "\", "
       << getBraceList(consts) << "));";
    cout << ss.str() << "\n";

    // Fortran model
    ss.str(string());
    ss << "ms.add(new ReadWriteModel(\""
       << getFunctionNameForLanguage(FuncName, SourceLang::Fort) << "\", ";
    if(FuncName.find("MPI_Wtime") != string::npos
       or FuncName.find("MPI_Wtick") != string::npos) {
      ss << "{}, {}));";
    } else {
      vector<bool> fconsts;
      for(bool c : consts)
        fconsts.push_back(c);
      fconsts.push_back(false);
      ss << getBraceList(fconsts) << "));";
    }
    cout << ss.str() << "\n";

    return false;
  }
};

// Implementation of the ASTConsumer interface for reading an AST produced
// by the Clang parser.
class MyASTConsumer : public ASTConsumer {
private:
  MyASTVisitor Visitor;

public:
  MyASTConsumer(LangOptions& langOpts) : Visitor(langOpts) {
    ;
  }

  // Override the method that gets called for each parsed top-level
  // declaration.
  bool HandleTopLevelDecl(DeclGroupRef DR) override {
    for(DeclGroupRef::iterator b = DR.begin(), e = DR.end(); b != e; ++b)
      Visitor.TraverseDecl(*b);
    return true;
  }
};

// For each source file provided to the tool, a new FrontendAction is created.
class MyFrontendAction : public ASTFrontendAction {
private:
public:
  MyFrontendAction() {
    ;
  }

  std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance& CI,
                                                 StringRef file) override {
    return std::make_unique<MyASTConsumer>(CI.getLangOpts());
  }
};

// USAGE: generate-mpi-sigs /path/to/mpi.h --
int main(int argc, const char* argv[]) {
  CommonOptionsParser op(argc, argv, GenerateMPISigsCategory);
  ClangTool Tool(op.getCompilations(), op.getSourcePathList());

  // ClangTool::run accepts a FrontendActionFactory, which is then used to
  // create new objects implementing the FrontendAction interface. Here we use
  // the helper newFrontendActionFactory to create a default factory that will
  // return a new MyFrontendAction object every time.
  // To further customize this, we could create our own factory class.
  int retcode = Tool.run(newFrontendActionFactory<MyFrontendAction>().get());
  moya_warning("Ignoring MPI_T functions");
  return retcode;
}
