#!/usr/bin/env python3

# This script parses the text descriptions used on the online lapack
# documentation. An example input file is this:
#
# http://www.netlib.org/cgi-bin/netlibfiles.txt?format=txt&filename=/lapack/lapack_routine/dgels.f (accessed on 21-Jul-2016)
#
# The second input file is lapack-models.txt which should in the same directory
# as this script but can be explicitly supplied if necessary. It contains the
# list of generated models obtained by prior invocations of this script. Any
# new models obtained by running this script will be added to that file.
# There should be a single source containing all the
# lapack functions somewhere but I can't find it, so for now this lousy
# approach will have to do.
#
# USAGE: ./generate-lapack.py <doc-file> [<known-file>]
#

import os
import re
from collections import OrderedDict

# os.path => { string, string }
def parse_models(inp):
    models = OrderedDict({})
    re_model = re.compile('^\s*mk_r.+[(]ms,\s*[\"](.+)[\"].*$')
    with open(inp) as f:
        for l in f:
            models[re_model.match(l.strip()).group(1)] = l.strip()
    return models
            
# { string, string }, os.path => void
def update_models(models, outp):
    with open(outp, 'w') as f:
        for _, defn in models.items():
            print('  {}'.format(defn))
            print(defn, file=f)

# string, [bool], int => string
def generate_defn(model, is_const, extra_params):
    from functools import reduce
    is_const.extend([True] * extra_params)
    if reduce(lambda x, y: x and y, is_const, True):
        return 'mk_ro(ms, \"{}\");'.format(model)
    else:
        consts = ','.join([str(int(c)) for c in is_const])
        return 'mk_rw(ms, \"{}\", {{{}}});'.format(model, consts)
            
# os.path => { string, string }
def parse_spec(inp):
    models = OrderedDict({})

    re_defn = re.compile('^[*]\s+Definition:\s*$')
    re_subroutine = re.compile('^[*]\s+subroutine\s+(.+)[(](.+).*$', re.I)
    re_function = re.compile('^[*]\s+.*\s+function\s+(.+)[(](.+).*$', re.I)
    re_param = re.compile('^[*][>]\s+[\\\\]param[[](.+)[]]\s+(.+)\s*?$')
    re_type = re.compile('^[*][>]\s+(.+)\s+is\s+(.+)\s*$')

    looking_for_defn = 1
    looking_for_name = 2
    looking_for_param = 4
    looking_for_type = 8

    with open(inp) as f:
        state = looking_for_defn
        curr_param = ''
        num_params = -1
        extra_params = 0
        model = ''
        is_const = []
        for line in f:
            line = line.strip()
            if state & looking_for_defn:
                m = re_defn.match(line)
                if m is not None:
                    if model != '':
                        models[model] = generate_defn(model, is_const, extra_params)
                        curr_param = ''
                        extra_params = 0
                        model = ''
                        is_const = []
                    state = looking_for_name
            if state & looking_for_name:
                m = re_subroutine.match(line)
                if m is None:
                    m = re_function.match(line)
                if m is not None:
                    model = m.group(1).lower()
                    state = looking_for_param

            if state & looking_for_param:
                m = re_param.fullmatch(line)
                if m is not None:
                    is_const.append(m.group(1) == 'in')
                    curr_param = m.group(2)
                    state = looking_for_type

            if state & looking_for_type:
                m = re_type.fullmatch(line)
                if m is not None:
                    if m.group(1) == curr_param and \
                       m.group(2).lower().startswith('character'):
                        extra_params += 1
                    state = looking_for_param | looking_for_defn
        if model != '':
            models[model] = generate_defn(model, is_const, extra_params)
                        
    return models
            
            
# [string] => int
def main(args):
    spec_file = args[1]
    known_file = 'lapack-models.txt' if len(args) <= 2 else args[2]
    models = parse_models(known_file)
    for model, defn in parse_spec(spec_file).items():
        if model not in models:
            models[model] = defn
    update_models(models, known_file)
    return 0

if __name__ == '__main__':
    from sys import argv
    exit(main(argv))
