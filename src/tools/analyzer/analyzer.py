#!/usr/bin/env python3

from abc import ABC as AbstractBase, abstractmethod
import base64
from collections import namedtuple
from datetime import datetime
import enum
import json as JSON
import pygraphviz as pgv
import os
import re
from sys import stderr
from typing import AbstractSet, List, Mapping, Set, Union
import zlib

import gi
from gi.repository import GObject

# Required:
#
# jsonschema
# jsonpointer
# strict-rfc3339
# rfc3987
# pygraphviz

re_dst = re.compile('^\s*(%.+)\s+=.+$')


@enum.unique
class Enum(enum.Enum):
    # None => str
    def __str__(self):
        if isinstance(self.value, str):
            return self.value
        else:
            return self.name


@enum.unique
class Flag(enum.Flag):
    # None => str
    def __str__(self):
        out = []
        for flg in type(self):
            if flg.value and flg in self:
                out.append(flg.name)
        return ' | '.join(out)


# The compression used on the contents of the source code
class Compression(Enum):
    NONE = enum.auto()
    GZ = enum.auto()
    BZ2 = enum.auto()


class SourceLang(Enum):
    C = 'C'
    CPP = 'CPP'
    Fortran = 'Fortran'
    F77 = 'F77'
    LLVM = 'LLVM'


class Status(Flag):
    Unused = 0x0
    Used = 0x1
    Read = 0x2 | Used
    Written = 0x4 | Used
    Allocated = 0x8 | Used
    Freed = 0x10 | Used

    # None => bool
    def is_used(self):
        return Status.Used in self

    # None => bool
    def is_read(self):
        return Status.Read in self

    # None => bool
    def is_written(self):
        return Status.Written in self

    # None => bool
    def is_allocated(self):
        return Status.Allocated in self

    # None => bool
    def is_freed(self):
        return Status.Freed in self

    # None => str
    def __str__(self):
        return ''.join(
            [char for fn, char in zip(
                [self.is_read, self.is_written, self.is_allocated, self.is_freed],
                ['R', 'W', 'A', 'F'])
             if fn()])


class SourceFile(GObject.GObject):
    def __init__(self, analysis: 'Analysis',
                 code: str = '',
                 llvm: bool = False):
        GObject.GObject.__init__(self)
        self.analysis = analysis
        self.dir = ''
        self.base = ''
        self.path = ''
        self.code = code
        self.lines = code.split(os.linesep)
        self.lang = None if not llvm else SourceLang.LLVM
        self.llvm = llvm
        self.header = False
        self.system_header = False

    # JSON => SourceFile
    def parse(self, json):
        self.dir = json['dir']
        self.base = json['file']
        self.path = os.path.join(self.dir, self.base)
        if 'contents' in json:
            self.code = '<UNABLE TO DECOMPRESS>'
            self.code = self.analysis.decompress(json['contents'])
        else:
            self.code = '<NOT AVAILABLE>'

        _, _, ext = self.base.rpartition('.')
        if ext in ('c', 'c99'):
            self.lang = SourceLang.C
        elif ext in ('cc', 'CC', 'cpp', 'CPP', 'cxx', 'CXX', 'C', 'c++',
                     'h', 'H', 'hpp', 'hxx'):
            self.lang = SourceLang.CPP
        elif ext in ('f90', 'f95', 'f03', 'fpp', 'FPP', 'F90', 'F95', 'F03'):
            self.lang = SourceLang.Fortran
        elif ext in ('F', 'f', 'f77', 'F77', 'fort'):
            self.lang = SourceLang.F77

        if ext in ('.h', '.H', '.hxx', '.hpp'):
            self.header = True

        return self

    # SourceFile => bool
    def __lt__(self, other):
        return self.path < other.path

    # None => str
    def __str__(self):
        return self.path


class SourceLoc(GObject.GObject):
    def __init__(self, analysis: 'Analysis',
                 file: SourceFile = None,
                 line: int = 0,
                 column: int = 0):
        GObject.GObject.__init__(self)
        self.analysis = analysis
        self.file = file
        self.line = line
        self.column = column
        self.valid = bool(self.line)

    @property
    def basename(self):
        return self.file.base

    # JSON => SourceLoc
    def parse(self, json):
        self.file = self.analysis.files[json['file']]
        self.line = json['line']
        if 'column' in json:
            self.column = json['column']
        if self.line:
            self.valid = True
        return self

    # None => str
    def format(self, full=False, column=False):
        out = []
        if self.file and self.file.path:
            out.append(self.file.path if full else self.file.base)
            if self.line:
                out.append(str(self.line))
            if self.column and column:
                out.append(str(self.column))
        else:
            out.append('[LLVM]')
            out.append(str(self.line))
        return ':'.join(out)

    # SourceLoc => bool
    def __lt__(self, other):
        return (self.file < other.file) \
            or (self.line < other.line) \
            or (self.column < other.column)

    # None => str
    def __str__(self):
        if self.valid:
            if self.column:
                return '{}:{}:{}'.format(self.file.base, self.line, self.column)
            else:
                return '{}:{}'.format(self.file.base, self.line)
        return '<unknown>'


class Type(GObject.GObject):
    class Kind(Enum):
        VoidType = enum.auto()
        MetadataType = enum.auto()
        FunctionType = enum.auto()
        IntegerType = enum.auto()
        FloatType = enum.auto()
        PointerType = enum.auto()
        ArrayType = enum.auto()
        StructType = enum.auto()
        ClassType = enum.auto()
        UnionType = enum.auto()

    # int, Type.Kind, Analysis => None
    def __init__(self, id, kind, analysis):
        GObject.GObject.__init__(self)
        self.id = id
        self.kind = kind
        self.analysis = analysis
        self.size = 0
        self._llvm = ''

    # JSON => Type
    def parse(self, json):
        if 'size' in json:
            self.size = int(json['size'])
        return self

    # None => str
    def __str__(self):
        return self._llvm


class ScalarType(Type):
    # int, Analysis, Type.Kind
    def __init__(self, id, kind, analysis):
        Type.__init__(self, id, kind, analysis)

    # JSON => ScalarType
    def parse(self, json):
        if 'llvm' in json:
            self._llvm = json['llvm']

    # None => Type
    @property
    def innermost(self):
        return None

    # None => str
    @property
    def llvm(self):
        return self._llvm


class VoidType(Type):
    # int, Type.Kind, Analysis
    def __init__(self, id, kind, analysis):
        Type.__init__(self, id, kind, analysis)

    # None => Type
    @property
    def innermost(self):
        return None

    # None => Type
    @property
    def llvm(self):
        return 'void'


class MetadataType(Type):
    # int, Type.Kind, Analysis
    def __init__(self, id, kind, analysis):
        Type.__init__(self, id, kind, analysis)

    # JSON => MetadataType
    def parse(self, json):
        Type.parse(self, json)
        if 'llvm' in json:
            self._llvm = json['llvm']
        return self

    # None => Type
    @property
    def innermost(self):
        return None

    # None => Type
    @property
    def llvm(self):
        return self._llvm


class FunctionType(Type):
    # int, Type.Kind, Analysis
    def __init__(self, id, kind, analysis):
        Type.__init__(self, id, kind, analysis)

        self.ret = None
        self.params = []

    # JSON => FuncitonType
    def parse(self, json):
        Type.parse(self, json)
        self.ret = self.analysis.parse_pointer(json['return'])
        for param in json['params']:
            self.params.append(self.analysis.parse_pointer(param))

        return self

    # None => Type
    @property
    def innermost(self):
        return None

    # None => str
    @property
    def llvm(self):
        if not self._llvm:
            self._llvm = ''.join([self.ret.llvm,
                                  '(',
                                  ', '.join(p.llvm for p in self.params),
                                  ')'])
        return self._llvm


class PointerType(Type):
    # int, Type.Kind, Analysis
    def __init__(self, id, kind, analysis):
        Type.__init__(self, id, kind, analysis)

        self.element = None

    # JSON => PointerType
    def parse(self, json):
        Type.parse(self, json)
        self.element = self.analysis.parse_pointer(json['element'])

        return self

    # None => Type
    @property
    def innermost(self):
        if isinstance(self.element, ArrayType):
            return self.element.innermost
        return self.element

    # None => str
    @property
    def llvm(self):
        if not self._llvm:
            self._llvm = '{}*'.format(self.element.llvm)
        return self._llvm


class ArrayType(Type):
    # int, Type.Kind, Analysis
    def __init__(self, id, kind, analysis):
        Type.__init__(self, id, kind, analysis)

        self.element = None
        self.length = -1

    # JSON => ArrayType
    def parse(self, json):
        Type.parse(self, json)
        self.length = json['length']
        self.element = self.analysis.parse_pointer(json['element'])

        return self

    # None => Type
    @property
    def innermost(self):
        if isinstance(self.element, ArrayType):
            return self.element.innermost
        return self.element

    # None => str
    @property
    def llvm(self):
        if not self._llvm:
            self._llvm = ''.join(['[',
                                  str(self.length),
                                  ' x ',
                                  self.element.llvm,
                                  ']'])
        return self._llvm


class StructType(Type):
    Field = namedtuple('Field', ['name', 'type', 'offset'])

    # int, Type.Kind, Analysis
    def __init__(self, id, kind, analysis):
        Type.__init__(self, id, kind, analysis)

        self.source = ''
        self.kind = None
        self.loc_source = None
        self.fields = []
        self.elements = []
        self.offsets = []
        self.idx = 0
        self.artificial = False

    # JSON => StructType
    def parse(self, json):
        Type.parse(self, json)
        self._llvm = json['llvm']
        # Get rid of any known leading prefix
        for prefix in ['struct.', 'class.', 'union.']:
            if self._llvm.startswith(prefix):
                self._llvm = self._llvm[len(prefix):]
        if 'source' in json:
            self.source = json['source']
            self._llvm = self.source
        if 'location' in json:
            self.loc_source = SourceLoc(self.analysis).parse(json['location'])
        if 'artificial' in json:
            self.artificial = True
        for field in json['fields']:
            if 'name' in field:
                fld = field['name']
            else:
                fld = str(len(self.fields) + 1)
            self.fields.append(
                StructType.Field(fld,
                                 self.analysis.parse_pointer(field['type']),
                                 int(field['offset'])))

        return self

    # None => Type
    @property
    def innermost(self):
        return None

    # int => Type
    def element(self, i):
        return self.fields[i].element

    # None => str
    @property
    def llvm(self):
        return self._llvm


class Local(GObject.GObject):
    class Flags(Flag):
        Empty = 0x0
        Artificial = enum.auto()

    def __init__(self, id: int, function: 'Function', analysis: 'Analysis'):
        GObject.GObject.__init__(self)
        self.id = id
        self.function = function
        self.analysis = analysis

        self.llvm = ''
        self.source = ''
        self.artificial = ''
        self.cell = None
        self.loc_source = None
        self.loc_ir = None
        self.type = None
        self.flags = Local.Flags.Empty

    # JSON => Local
    def parse(self, json):
        if 'llvm' in json:
            self.llvm = json['llvm']
        if 'source' in json:
            self.source = json['source']
        if 'artificial' in json:
            self.artificial = json['artificial']
        self.cell = self.analysis.parse_pointer(json['address'])
        self.cell.obj = self
        self.type = self.analysis.parse_pointer(json['type'])
        if 'location' in json:
            self.loc_source = SourceLoc(self.analysis).parse(json['location'])
        else:
            self.flags |= Local.Flags.Artificial
        if 'flags' in json:
            for flag in json['flags']:
                self.flags |= Local.Flags[flag]

        return self

    # None => str
    @property
    def name(self):
        if self.source:
            return self.source
        elif self.artificial:
            return self.artificial
        elif self.llvm:
            return self.llvm
        else:
            return str(self.cell.address)


class Argument(GObject.GObject):
    class Flags(Flag):
        Empty = 0
        MoyaIgnore = enum.auto()
        Array = enum.auto()
        Artificial = enum.auto()

    # int, Function
    def __init__(self, function):
        GObject.GObject.__init__(self)
        self.function = function
        self.analysis = function.analysis

        self.llvm = ''
        self.source = ''
        self.artificial = ''
        self.loc_source = None
        self.loc_ir = self.function.loc_ir
        self.argno = -1
        self.cell = None
        self.type = None
        self.linked = None
        self.flags = Argument.Flags.Empty

    # JSON => Argument
    def parse(self, json):
        if 'llvm' in json:
            self.llvm = json['llvm']
        if 'source' in json:
            self.source = json['source']
        if 'artificial' in json:
            self.artificial = json['artificial']
        if 'location' in json:
            self.loc_source = SourceLoc(self.analysis).parse(json['location'])
        self.argno = int(json['argno'])
        self.cell = self.analysis.parse_pointer(json['address'])
        self.cell.obj = self
        self.type = self.analysis.parse_pointer(json['type'])
        for flag in json['flags']:
            self.flags |= Argument.Flags[flag]
        if 'linked' in json:
            self.linked = self.analysis.parse_pointer(json['linked'])

        return self

    @property
    def name(self) -> str:
        if Argument.Flags.Artificial in self.flags:
            return self.artificial
        elif self.source:
            return self.source
        elif self.llvm:
            return self.llvm
        else:
            return str(self.argno)

    def __str__(self) -> str:
        if self.source:
            return self.source
        elif self.llvm:
            return self.llvm
        elif self.artificial:
            return self.artificial
        else:
            return str(self.argno)


class Block(GObject.GObject):
    def __init__(self, id: int, func: 'Function', analysis: 'Analysis'):
        GObject.GObject.__init__(self)

        self.id = id
        self.analysis = analysis
        self.function = func
        self.succs = set([])
        # The instructions have to be in order
        self.instructions = []

    def parse(self, json: Mapping[str, object]) -> 'Block':
        if 'successors' in json:
            for successor in json['successors']:
                self.succs.add(self.analysis.parse_pointer(successor))

        if 'instructions' in json:
            for iptr in json['instructions']:
                inst = self.analysis.parse_pointer(iptr)
                inst.bb = self
                self.instructions.append(inst)

        return self


class Instruction(GObject.GObject):
    class Kind(Enum):
        Dummy = enum.auto()
        Load = enum.auto()
        Store = enum.auto()
        Call = enum.auto()

    def __init__(self, id: int, kind: Kind, function: 'Function', analysis: 'Analysis'):
        GObject.GObject.__init__(self)
        self.id = id
        self.kind = kind
        self.analysis = analysis
        self.loc_source = None
        self.loc_ir = None
        self.bb = None
        self.function = function

    def parse(self, json: Mapping[str, object]) -> 'Instruction':
        if 'location' in json:
            self.loc_source = SourceLoc(self.analysis).parse(json['location'])
        if 'location_ir' in json:
            self.loc_ir = SourceLoc(self.analysis,
                                    self.function.loc_ir.file,
                                    int(json['location_ir']),
                                    1)
        return self


class DummyInstruction(Instruction):
    def __init__(self, id: int, func: 'Function', analysis: 'Analysis'):
        Instruction.__init__(self, id, Instruction.Kind.Dummy, func, analysis)


class LoadInstruction(Instruction):
    def __init__(self, id: int, func: 'Function', analysis: 'Analysis'):
        Instruction.__init__(self, id, Instruction.Kind.Load, func, analysis)

        self.targets = set([])
        self.pointer = ''

    def parse(self, json: Mapping[str, object]) -> 'LoadInstruction':
        Instruction.parse(self, json)
        if 'targets' in json:
            for target in json['targets']:
                self.targets.add(self.analysis.parse_pointer(target))
        if 'pointer' in json:
            self.pointer = re_dst.match(
                self.loc_ir.file.lines[int(json['pointer'])-1]).group(1).strip()
        return self


class StoreInstruction(Instruction):
    def __init__(self, id: int, func: 'Function', analysis: 'Analysis'):
        Instruction.__init__(self, id, Instruction.Kind.Store, func, analysis)

        self.targets = set([])

    def parse(self, json: Mapping[str, object]) -> 'StoreInstruction':
        Instruction.parse(self, json)
        if 'targets' in json:
            for target in json['targets']:
                self.targets.add(self.analysis.parse_pointer(target))
        if 'pointer' in json:
            self.pointer = re_dst.match(
                self.loc_ir.file.lines[int(json['pointer'])-1]).group(1).strip()
        return self


class CallInstruction(Instruction):
    def __init__(self, id: int, func: 'Function', analysis: 'Analysis'):
        Instruction.__init__(self, id, Instruction.Kind.Call, func, analysis)

        self.functions = set([])

    def parse(self, json: Mapping[str, object]) -> 'CallInstruction':
        Instruction.parse(self, json)
        if 'functions' in json:
            for function in json['functions']:
                self.functions.add(self.analysis.parse_pointer(function))

        return self


class Function(GObject.GObject):
    class Flags(Flag):
        Empty = 0x0
        Artificial = enum.auto()
        Method = enum.auto()
        Internal = enum.auto()
        MoyaJIT = enum.auto()

    def __init__(self, id: int, analysis: 'Analysis'):
        GObject.GObject.__init__(self)
        self.id = id
        self.analysis = analysis
        self.cell = None
        self.type = None
        self.llvm = ''
        self.source = ''
        self.qualified = ''
        self.full = ''
        self.artificial = ''
        self.loc_ir = None
        self.loc_source = None
        self.loc_begin = None
        self.loc_end = None
        self.flags = Function.Flags.Empty
        self.args = []
        self.blocks = {}
        self.instructions = {}
        self.locals = {}
        self.callsites = set([])

        # If self is a method, then this is the class
        self.cls = None

        # If slef is a nested function, then this is the enclosing function
        self.enclosing = None

    def parse(self, json: Mapping[str, object]) -> 'Function':
        if 'llvm' in json:
            self.llvm = json['llvm']
        if 'source' in json:
            self.source = json['source']
        if 'qualified' in json:
            self.qualified = json['qualified']
        if 'full' in json:
            self.full = json['full']
        if 'artificial' in json:
            self.artificial = json['artificial']
        if 'address' in json:
            self.cell = self.analysis.parse_pointer(json['address'])
            self.cell.obj = self
        if 'type' in json:
            self.type = self.analysis.parse_pointer(json['type'])
        if 'location' in json:
            self.loc_source = SourceLoc(self.analysis).parse(json['location'])
        # if 'begin' in json:
        #     self.loc_begin = SourceLoc(self.analysis).parse(json['begin'])
        # if 'end' in json:
        #     self.loc_end = SourceLoc(self.analysis).parse(json['end'])
        if 'flags' in json:
            for flag in json['flags']:
                self.flags |= Function.Flags[flag]
        if 'class' in json:
            self.flags |= Function.Flags.Method
            # self.cls = self.analysis.classes[self.analysis.parse_pointer(json['class']).id]
            # self.cls.methods.append(self)
        if 'parent' in json:
            self.flags |= Function.Flags.Internal
            self.parent = self.analysis.parse_pointer(json['parent'])
        if 'ir' in json:
            src = SourceFile(self.analysis,
                             code=self.analysis.decompress(json['ir']),
                             llvm=True)
            src.base = self.llvm
            self.analysis.files[self.llvm] = src
            self.loc_ir = SourceLoc(self.analysis, src, 2, 1)

        # Needs to be done in two passes because arguments could refer to
        # one another in the case of "linked" arguments which are dummy
        # arguments inserted by the compiler
        if 'args' in json:
            for _ in json['args']:
                self.args.append(Argument(self))
            for arg, data in zip(self.args, json['args']):
                arg.parse(data)

        if 'instructions' in json:
            for key, val in json['instructions'].items():
                self.instructions[int(key)].parse(val)

        if 'blocks' in json:
            for key, val in json['blocks'].items():
                self.blocks[int(key)].parse(val)

        if 'locals' in json:
            for key, val in json['locals'].items():
                self.locals[int(key)].parse(val)

        if 'callsites' in json:
            for site in json['callsites']:
                self.callsites.add(self.analysis.parse_pointer(site))

        return self

    def get_blocks(self) -> List[Block]:
        return self.blocks.values()

    def get_instructions(self) -> List[Instruction]:
        return self.instructions.values()

    def get_locals(self) -> List[Local]:
        return self.locals.values()

    @property
    def name(self) -> str:
        if Function.Flags.Artificial in self.flags:
            return self.artificial
        elif self.source:
            return self.source
        else:
            return self.llvm

    @property
    def callers(self) -> List['Function']:
        return [inst.function for inst in self.callsites]

    def __str__(self) -> str:
        if self.qualified:
            return self.qualified
        elif self.source:
            return self.source
        elif self.llvm:
            return self.llvm
        else:
            return str(self.id)


class Global(GObject.GObject):
    class Flags(Flag):
        Empty = 0x0
        Artificial = enum.auto()

    def __init__(self, id: int, analysis: 'Analysis'):
        GObject.GObject.__init__(self)
        self.id = id
        self.analysis = analysis
        self.llvm = ''
        self.source = ''
        self.qualified = ''
        self.artificial = ''
        self.full = ''
        self.cell = None
        self.type = None
        self.size = 0
        self.loc_source = None
        self.loc_ir = None
        self.flags = Global.Flags.Empty

    def parse(self, json: Mapping[str, object]) -> 'Global':
        if 'llvm' in json:
            self.llvm = json['llvm']
        if 'source' in json:
            self.source = json['source']
        if 'qualified' in json:
            self.qualified = json['qualified']
        if 'full' in json:
            self.full = json['full']
        if 'artificial' in json:
            self.artificial = json['artificial']
        if 'address' in json:
            self.cell = self.analysis.parse_pointer(json['address'])
            self.cell.obj = self
        if 'type' in json:
            self.type = self.analysis.parse_pointer(json['type'])
        if 'size' in json:
            self.size = int(json['size'])
        if 'location' in json:
            self.loc_source = SourceLoc(self.analysis).parse(json['location'])
        if 'flags' in json:
            for flag in json['flags']:
                self.flags |= Global.Flags[flag]

        return self

    @property
    def name(self) -> str:
        if Global.Flags.Artificial in self.flags:
            return self.artificial
        elif self.source:
            return self.source
        else:
            return self.llvm

    def __str__(self) -> str:
        if self.source:
            return self.source
        elif self.llvm:
            return self.llvm
        else:
            return str(self.id)


class Class(GObject.GObject):
    def __init__(self, typ: StructType):
        GObject.GObject.__init__(self)

        self.type = typ
        self.methods = []
        self.members = []

    @property
    def location(self) -> SourceLoc:
        return self.type.location


class Region(GObject.GObject):
    def __init__(self, id: int, analysis: 'Analysis'):
        GObject.GObject.__init__(self)
        self.analysis = analysis

        self.id = id
        self.name = ''
        self.loc_source = None
        self.loc_begin = None
        self.loc_end = None
        self.cg = Callgraph(False, self.analysis)
        self.closure = Callgraph(True, self.analysis)

    def parse(self, json: Mapping[str, object]) -> 'Region':
        self.name = json['name']
        if 'location' in json:
            self.loc_source = SourceLoc(self.analysis).parse(json['location'])
        if 'begin' in json:
            self.loc_begin = SourceLoc(self.analysis).parse(json['begin'])
        if 'end' in json:
            self.loc_end = SourceLoc(self.analysis).parse(json['end'])
        self.cg.parse(json['callgraph'])
        self.closure.parse(json['callgraph'])

        return self

    # None => str
    def __str__(self) -> str:
        return '{}:{}'.format(self.id, self.name)


class Callgraph(GObject.GObject):
    def __init__(self, closure: bool, analysis: 'Analysis'):
        GObject.GObject.__init__(self)
        self.analysis = analysis
        self.closure = closure
        self.root = None

        self.callers = {}
        self.callees = {}
        self._impl = pgv.AGraph(name='moya', directed=True)

    def parse(self, json: Mapping[str, object]) -> 'Callgraph':
        self.root = self.analysis.parse_pointer(json['root'])
        self.add_node(self.root)
        for f, ptrs in json.items():
            if f.startswith('/'):
                caller = self.analysis.parse_pointer(f)
                # Sort the callees by their line number. In most code,
                # the line numbers will be a reasonable approximation
                # for the order in which the callees will get called during
                # an actual execution. Obviously this is not guaranteed and
                # in the long run, it will probably be better to have a
                # a subgraph within the callgraph which captures the
                # control-flow within a function, but as a first approximation
                # this will do
                callees = [self.analysis.parse_pointer(ptr) for ptr in ptrs]
                for callee in sorted(
                        [callee for callee in callees if callee],
                        key=lambda f: f.loc_source.line if f.loc_source else 0):
                    self.add_edge(caller, callee)

        if self.closure:
            self._create_closure()

        return self

    def add_edge(self, caller: Function, callee: Function) -> pgv.Edge:
        self._impl.add_edge(self.add_node(caller), self.add_node(callee))
        self.callers[callee].add(caller)
        self.callees[caller].add(callee)
        return self._impl.get_edge(self._impl.get_node(caller.id),
                                   self._impl.get_node(callee.id))

    def add_node(self, f: Function) -> pgv.Node:
        if not isinstance(f, Function):
            raise RuntimeError('Need a function to add a node', f)
        if f.id not in self._impl:
            self._impl.add_node(f.id, moya=f.id)
            self.callers[f] = set([])
            self.callees[f] = set([])
        return self._impl.get_node(f.id)

    def _create_closure(self):
        def collect(caller, wl=None):
            if not wl:
                wl = set([])
            for callee in self[caller]:
                if callee not in wl:
                    wl.add(callee)
                    collect(callee, wl)
            return wl

        for caller in self:
            for callee in collect(caller):
                self.add_edge(caller, callee)

    def get_subgraph(self, nodes: AbstractSet[Function]) -> pgv.AGraph:
        return self._impl.subgraph([f.id for f in nodes], name='moya')

    def __getitem__(self, function: Function) -> AbstractSet[Function]:
        if not isinstance(function, Function):
            raise TypeError('Expected function. Got', type(function))
        if function in self.callees:
            return self.callees[function]
        return set([])

    def __contains__(self, function: Function) -> bool:
        if not isinstance(function, Function):
            raise TypeError('Expected function. Got', type(function))
        return function in self.callees

    # None => {Function, {Function}}
    def __iter__(self) -> Mapping[Function, Set[Function]]:
        return iter(self.callees)

    # None => str
    def __str__(self):
        out = []
        for caller in self.callees:
            out.append(format(caller))
            for callee in self.callees[caller]:
                out.append('  {}'.format(callee))
        return '\n'.join(out)


class Cell(GObject.GObject):
    class Flags(Flag):
        Empty = 0x0
        Stack = enum.auto()
        Heap = enum.auto()
        Implicit = enum.auto()
        Code = enum.auto()
        Argument = enum.auto()
        Global = enum.auto()
        Constant = enum.auto()
        Start = enum.auto()
        Vtable = enum.auto()
        Initialized = enum.auto()
        Dummy = enum.auto()
        LibAlloc = enum.auto()

    def __init__(self, address: int, store: 'Store'):
        GObject.GObject.__init__(self)
        self.store = store
        self.analysis = store.analysis
        self.address = address
        self._readers: AbstractSet[Instruction] = set([])
        self._writers: AbstractSet[Instruction] = set([])
        self._allocators: AbstractSet[Instruction] = set([])
        self._deallocators: AbstractSet[Instruction] = set([])
        self.flags = Cell.Flags.Empty
        self.targets = set([])
        self.empty = True
        self.allocated = None
        self.structs = []

        # This could be a Function, an Argument, a Local or a Global
        # It will only be set when the cell is the start of an Argument,
        # Local or Global. There is only a single cell
        # This will be set by the objects themselves
        self.obj = None

    def parse(self, json: Mapping[str, object]) -> 'Cell':
        self.type = self.analysis.parse_pointer(json['type'])
        for category in ['readers', 'writers', 'allocators', 'deallocators']:
            if category in json:
                getattr(self, '_' + category).update(
                    set([self.analysis.parse_pointer(inst)
                         for inst in json[category]]))

        for flag in json['flags']:
            self.flags |= Cell.Flags[flag]
        if 'allocated' in json:
            self.allocated = self.analysis.parse_pointer(json['allocated'])
        if 'structs' in json:
            for s in json['structs']:
                self.structs.append(self.analysis.parse_pointer(s))

        # For now, we only care about the addresses in the contents
        if 'contents' in json:
            for content in json['contents']:
                if content['_class'] == 'Pointer':
                    if 'target' in content:
                        if isinstance(content['target'], str):
                            self.targets.add(
                                self.analysis.parse_pointer(content['target']))
                    else:
                        raise RuntimeError('!!!TOP!!!')
                else:
                    self.empty = False

        if len(self._allocators) > 1:
            raise RuntimeError('Multiple allocators for cell:', self.address)

        return self

    def _get_accessors(self,
                       accessors: AbstractSet[Instruction],
                       typ: Union[Function, Instruction, SourceLoc]) \
            -> AbstractSet[Union[Function, Instruction, SourceLoc]]:
        if typ is Function:
            return [inst.function for inst in accessors]
        elif typ is Instruction:
            return [inst for inst in accessors]
        elif typ is SourceLoc:
            return [inst.loc_source for inst in accessors if inst.loc_source]
        else:
            raise RuntimeError(
                'Type of accessor requested must be '
                'either Function or Instruction. Got {}'.format(type))

    def readers(self, typ: Union[Function, Instruction, SourceLoc]) \
            -> AbstractSet[Union[Function, Instruction, SourceLoc]]:
        return self._get_accessors(self._readers, typ)

    def writers(self, typ: Union[Function, Instruction, SourceLoc]) \
            -> AbstractSet[Union[Function, Instruction, SourceLoc]]:
        return self._get_accessors(self._writers, typ)

    def allocators(self, typ: Union[Function, Instruction, SourceLoc]) \
            -> AbstractSet[Union[Function, Instruction, SourceLoc]]:
        return self._get_accessors(self._allocators, typ)

    def deallocators(self, typ: Union[Function, Instruction, SourceLoc]) \
            -> AbstractSet[Union[Function, Instruction, SourceLoc]]:
        return self._get_accessors(self._deallocators, typ)


class Store:
    def __init__(self, analysis: 'Analysis'):
        self.analysis = analysis
        self._cells = {}

    def parse(self, json: Mapping[str, object]) -> 'Store':
        # Has to be done in two passes because the contents of a cell could
        # be a pointer to another
        for key, val in json.items():
            try:
                cell = Cell(int(key), self)
                self._cells[cell.address] = cell
            except ValueError:
                pass

        for key, val in json.items():
            try:
                self._cells[int(key)].parse(val)
            except ValueError:
                pass

        return self

    def readers(self, addr: int, typ: Union[Function, Instruction, SourceLoc]) \
            -> AbstractSet[Union[Function, Instruction, SourceLoc]]:
        return self._cells[addr].readers(typ)

    def writers(self, addr: int, typ: Union[Function, Instruction, SourceLoc]) \
            -> AbstractSet[Union[Function, Instruction, SourceLoc]]:
        return self._cells[addr].writers(typ)

    # int, type => set(type)
    def allocators(self, addr: int, typ: Union[Function, Instruction, SourceLoc]) \
            -> AbstractSet[Union[Function, Instruction, SourceLoc]]:
        return self._cells[addr].allocators(typ)

    # int, type => set(type)
    def deallocators(self, addr: int, typ: Union[Function, Instruction, SourceLoc]) \
            -> AbstractSet[Union[Function, Instruction, SourceLoc]]:
        return self._cells[addr].deallocators(typ)

    # int => Store.Flags
    def flags(self, addr):
        if addr not in self._cells:
            raise RuntimeError('Unknown cell: {}'.format(addr))
        return self._cells[addr].flags

    # None => [Cell]
    def cells(self):
        return self._cells.values()

    # int => Cell
    def __getitem__(self, addr):
        if addr in self._cells:
            return self._cells[addr]
        print('WARNING: No cell with address', addr, file=stderr)

    # int => bool
    def __contains__(self, addr):
        return addr in self._cells


class Analysis(GObject.GObject):
    initialized = GObject.Property(type=bool,
                                   default=False,
                                   nick='initialized')

    # None
    def __init__(self):
        GObject.GObject.__init__(self)

        self.clear()

    # None => None
    def clear(self):
        self.set_property('initialized', False)
        self.name = ''

        # The file containing the analysis results
        self.source = ''

        # The executable/library that was analyzed
        self.analyzed = ''
        self.sig = ''

        # The time as a struct.time object
        self.time = None

        # The compression algorithm used for the files
        self.compression = Compression.NONE

        self.files = {}
        self.types = {}
        self.functions = {}
        self.globals = {}
        self.classes = {}
        self.regions = {}

        self.cg = Callgraph(False, self)
        self.closure = Callgraph(True, self)
        self.store = Store(self)

    def decompress(self, s: str) -> str:
        if self.compression == Compression.GZ:
            return zlib.decompress(
                base64.b64decode(s)).decode('utf-8')
        elif self.compression == Compression.BZ2:
            raise RuntimeError('bzip2 compression not supported')
        elif self.compression == Compression.NONE:
            return s
        return ''

    # os.path => Analysis
    def load_from_file(self, src: str) -> 'Analysis':
        with open(src, 'r') as f:
            json = JSON.load(f)

            self.source = src
            self.name = json['name']
            self.analyzed = json['analyzed']
            self.sig = json['sha512']
            self.time = datetime.fromisoformat(json['created'])
            self.compression = Compression[json['compression']]

            # There are circular references all over the place, so we need
            # to first create all the objects and then initialize them
            # from the JSON
            type_classes = {
                Type.Kind.IntegerType: ScalarType,
                Type.Kind.FloatType: ScalarType,
                Type.Kind.VoidType: VoidType,
                Type.Kind.MetadataType: MetadataType,
                Type.Kind.PointerType: PointerType,
                Type.Kind.ArrayType: ArrayType,
                Type.Kind.FunctionType: FunctionType,
                Type.Kind.StructType: StructType,
                Type.Kind.ClassType: StructType,
                Type.Kind.UnionType: StructType}

            for key, val in json['types'].items():
                tid = int(key)
                kind = Type.Kind[val['_class']]
                self.types[tid] = type_classes[kind](tid, kind, self)
                if kind == Type.Kind.ClassType:
                    self.classes[tid] = Class(self.types[tid])

            inst_classes = {
                Instruction.Kind.Dummy: DummyInstruction,
                Instruction.Kind.Load: LoadInstruction,
                Instruction.Kind.Store: StoreInstruction,
                Instruction.Kind.Call: CallInstruction}

            for fid, function in json['functions'].items():
                fid = int(fid)
                f = Function(fid, self)
                self.functions[fid] = f
                if 'blocks' in function:
                    for bid in function['blocks']:
                        bid = int(bid)
                        f.blocks[bid] = Block(bid, f, self)
                if 'instructions' in function:
                    for iid, ival in function['instructions'].items():
                        iid = int(iid)
                        kind = Instruction.Kind[ival['_class']]
                        f.instructions[iid] = inst_classes[kind](iid, f, self)
                if 'locals' in function:
                    for aid in function['locals']:
                        aid = int(aid)
                        f.locals[aid] = Local(aid, f, self)

            for key, val in json['globals'].items():
                gid = int(key)
                self.globals[gid] = Global(gid, self)

            for key, val in json['regions'].items():
                rid = int(key)
                self.regions[rid] = Region(rid, self)

            # Second pass. Any links will be fixed up. These have to be done
            # in order
            for key, val in json['files'].items():
                self.files[key] = SourceFile(self).parse(val)

            self.store.parse(json['store'])
            self.cg.parse(json['callgraph'])
            self.closure.parse(json['callgraph'])

            for typ in self.types.values():
                typ.parse(json['types'][str(typ.id)])

            for func in self.functions.values():
                # The function could be a fake "wrapper" that was inserted
                # for a region. It will not be in the JSON
                if str(func.id) in json['functions']:
                    func.parse(json['functions'][str(func.id)])

            for gbl in self.globals.values():
                gbl.parse(json['globals'][str(gbl.id)])

            for region in self.regions.values():
                region.parse(json['regions'][str(region.id)])

            self.set_property('initialized', True)

        return self

    def parse_pointer(self, ptr: str) -> object:
        curr = self
        for i, step in enumerate(ptr.split('/')[1:]):
            if isinstance(curr, list):
                curr = curr[int(step)]
            elif isinstance(curr, dict) or isinstance(curr, object):
                try:
                    key = int(step)
                    if key not in curr:
                        print('Could not find', key, ' in', ptr, file=stderr)
                        return None
                    else:
                        curr = curr[key]
                except ValueError:
                    curr = getattr(curr, step)
            else:
                raise RuntimeError('Unexpected type: {} @ {} of {}'.format(
                    type(curr), step, ptr))
        return curr

    def get_functions(self, methods: bool = False) -> List[Function]:
        if methods:
            return list(self.functions.values())
        else:
            return [f for f in self.functions.values() if not f.cls]

    def get_function(self, id: int) -> Function:
        if not isinstance(id, int):
            raise TypeError('Expected int. Got', type(id))
        if id in self.functions:
            return self.functions[id]
        return None

    def get_arguments(self) -> List[Argument]:
        return [a for f in self.get_functions() for a in f.args]

    def get_globals(self) -> List[Global]:
        return list(self.globals.values())

    def get_global(self, id: int) -> Global:
        if not isinstance(id, int):
            raise TypeError('Expected int. Got', type(id))
        if id in self.globals:
            return self.globals[id]
        return None

    def get_structs(self) -> List[StructType]:
        return [t for t in self.types.values() if isinstance(t, StructType)]

    def get_classes(self) -> List[Class]:
        return list(self.classes.values())

    def get_types(self) -> List[Type]:
        return list(self.types.values())

    def get_regions(self) -> List[Region]:
        return list(self.regions.values())


# This is a "summary" of an object created by following pointers and the
# like from the store
class Summary(GObject.GObject):
    # Cell, Type, str, Summary, Analysis
    def __init__(self, cell, type, name, analysis):
        GObject.GObject.__init__(self)
        self.analysis = analysis
        self.cell = cell
        self.type = type
        self.name = name
        self.subs = []
        self.readers = set([])
        self.writers = set([])
        self.allocators = set([])
        self.deallocators = set([])
        self.status = {}
        self.parent = None

    # Cell => None
    def add_accessors(self, cell):
        if cell:
            self.readers.update(cell.readers(Function))
            self.writers.update(cell.writers(Function))
            self.allocators.update(cell.allocators(Function))
            self.deallocators.update(cell.deallocators(Function))

    # Object => None
    def append(self, obj):
        self.subs.append(obj)
        obj.parent = self

    # Callgraph => Status
    def compute_status(self, closure):
        def accessed(accessors):
            return bool([f for f in accessors if f in closure])

        typ = self.type

        self.status[closure] = Status.Unused
        for lst, flag in zip(
                [self.readers, self.writers, self.allocators, self.deallocators],
                [Status.Read, Status.Written, Status.Allocated, Status.Freed]):
            if accessed(lst):
                self.status[closure] |= flag

        if isinstance(typ, ScalarType) or isinstance(typ, FunctionType):
            pass
        elif isinstance(typ, PointerType):
            for sub in self.subs:
                sub.compute_status(closure)
        elif isinstance(typ, ArrayType) or isinstance(typ, StructType):
            for sub in self.subs:
                self.status[closure] |= sub.compute_status(closure)
        else:
            raise RuntimeError(
                'Unknown type to collect status: {}'.format(typ))

        return self.status[closure]

    # int, int => str
    def to_str(self, tab=0, width=0):
        typ = self.type
        indent = ' ' * (2 * tab)
        space = ' ' * len(self.source)
        addr = hex(self.addr)
        if self.addr < 0:
            addr = ' '

        nxt_width = 0
        if (isinstance(typ, ArrayType) or isinstance(typ, PointerType)) \
           and (isinstance(typ.innermost, ScalarType)
                or isinstance(typ.innermost, FunctionType)):
            nxt_width = width
            if nxt_width > 2:
                nxt_width = nxt_width - 2
        else:
            nxt_width = max([len(sub.source) for sub in self.subs], default=0)

        out = []
        name = self.source
        if width:
            name = '{{:{}}}'.format(width).format(self.source)

        line = '{}{} |{:4}| .. {}'\
            .format(indent,
                    name,
                    str(self.status),
                    self.type)
        out.append(line)
        for sub in self.subs:
            out.append(sub.to_str(tab + 1, nxt_width))

        return '\n'.join(out)

    # type(Summary), Cell, Type, str, { (int, Type): Summary } => Summary
    @classmethod
    def new(cls, cell, typ, name, objs=None):
        if objs is None:
            objs = {}
        # Cell could be none if we are creating a "fake" cell for scalar targets
        # of pointers
        if cell and (cell, typ) in objs:
            return objs[(cell, typ)]

        store = typ.analysis.store
        obj = cls(cell, typ, name, typ.analysis)
        objs[(cell, typ)] = obj
        if isinstance(typ, ScalarType) or isinstance(typ, FunctionType):
            obj.add_accessors(cell)
        elif isinstance(typ, PointerType):
            obj.add_accessors(cell)
            # Small optimization to make some things a shade easier. Merge
            # all the pointers to scalars together into a single scalar because
            # it makes things easier to visualize and it is easier than merging
            # composite (particularly polymorphic) types
            elem = typ.innermost
            merge = False
            if isinstance(elem, ScalarType) or isinstance(elem, FunctionType):
                sub = cls(None, elem, '<merged>', typ.analysis)
                for target in cell.targets:
                    sub.add_accessors(target)
                sub.compute_status(obj.analysis.closure)
                for region in obj.analysis.get_regions():
                    sub.compute_status(region.closure)
                obj.append(sub)
            else:
                for target in cell.targets:
                    obj.append(Summary.new(target, elem, '', objs))
        elif isinstance(typ, ArrayType):
            obj.append(Summary.new(cell, typ.innermost, '', objs))
        elif isinstance(typ, StructType):
            for i, (fld, fldty, offset) in enumerate(typ.fields):
                obj.append(Summary.new(
                    store[cell.address + offset], fldty, fld, objs))
        else:
            raise RuntimeError('Unknown type to summarize')

        obj.compute_status(obj.analysis.closure)
        for region in obj.analysis.get_regions():
            obj.compute_status(region.closure)

        return obj
