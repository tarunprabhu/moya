#!/usr/bin/env python3

import argparse
from base64 import standard_b64encode as b64encode
from enum import IntEnum, auto
import io
import os
import pygraphviz as pgv
import re
import sys
from typing import AbstractSet, Mapping, Set

from analyzer import (
    Analysis,
    Class, Function, Global, Argument, Local,
    Instruction, LoadInstruction, StoreInstruction, CallInstruction,
    Store, Cell,
    SourceLang, SourceLoc, SourceFile,
    Summary, Status,
    Type, ScalarType, PointerType, ArrayType, FunctionType, StructType)

import gi
gi.require_version('GdkPixbuf', '2.0')
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('GLib', '2.0')
gi.require_version('GtkSource', '4')
gi.require_version('Pango', '1.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import GLib, Gdk, Gtk, GtkSource, GObject, Pango, WebKit2
from gi.repository import GdkPixbuf

# ICONS:
#
# function.svg: https://commons.wikimedia.org/wiki/File:Psi2.svg
# global.svg: https://freesvg.org/internet-globe-icon-vector-image
# class.svg: https://commons.wikimedia.org/wiki/File:Html5_offline_storage.svg
# stack.svg: https://commons.wikimedia.org/wiki/File:Toicon-icon-afiado-stack.svg
# heap.svg: Modified version of stack
# filter.svg: https://commons.wikimedia.org/wiki/File:Filter_font_awesome.svg

# Have to define the enums for columns in a store this way because for some
# reason, the AutoNumber example in the Python documentation doesn't work as
# expected. The resulting enum cannot be directly converted to int
# and passing it to int(.) always returns 0.
ModelColsFiles = IntEnum('ModelColsFiles',
                         [ 'ID',
                           'Tooltip',
                           'SortKey',
                           'Name',
                           'Sensitive',
                           'SourceFile',
                           'Ellipsize' ],
                         start = 0)


ModelColsEntities = IntEnum('ModelColsEntities',
                            [ 'Object',
                              'Icon',
                              'Name',
                              'Style',
                              'Location'],
                            start = 0)


ModelColsRegions = IntEnum('ModelColsRegions',
                           [ 'ID',
                             'Tooltip',
                             'Name',
                             'Callgraph',
                             'Region' ],
                           start = 0)


ModelColsStore = IntEnum('ModelColsStore',
                         [ 'Cell',
                           'Icon',
                           'Address',
                           'Allocator',
                           'Start' ],
                         start=0)


ModelColsDetails = IntEnum('ModelColsDetails',
                           [ 'Object',
                             'Tooltip',
                             'SortKey',
                             'Label',
                             'Content',
                             'Cell'],
                           start=0)


ModelColsSummary = IntEnum('ModelColsSummary',
                           [ 'Summary',
                             'Type',
                             'Address',
                             'Name',
                             'IconRead',
                             'IconWritten',
                             'IconAllocated',
                             'IconFreed' ],
                           start=0)


# This will be defined when the UI is built
class StatusMsgContext(IntEnum):
    pass


# Configuration class for user options
class Config(GObject.GObject):
    mode_llvm = GObject.Property(
        type=bool,
        default=False,
        nick='llvm-mode',
        blurb='Show LLVM IR instead of source')

    show_artificial = GObject.Property(
        type=bool,
        default=False,
        nick='show-artificial',
        blurb='Show compiler-generated entities')

    code_show = GObject.Property(
        type=bool,
        default=True,
        nick='code-show',
        blurb='Show a pane containing the source code')

    code_style = GObject.Property(
        type=GtkSource.StyleScheme,
        default=GtkSource.StyleSchemeManager.get_default().get_scheme('oblivion'),
        nick='code-style',
        blurb='The code style to use')

    entities_show = GObject.Property(
        type=bool,
        default=True,
        nick='entities-show',
        blurb='Show a searchable view of all entities')

    # Must be one of ['All', 'Classes', 'Functions', 'Globals']
    # This really ought to be an enum ...
    entities_show_kind = GObject.Property(
        type=str,
        default='All',
        nick='entities-show-kind',
        blurb='The entity type to show in the entities view')

    store_show = GObject.Property(
        type=bool,
        default=True,
        nick='store-show',
        blurb='Show the store pane')

    # Must be one of ['All', 'Globals', 'Stack', 'Heap']
    # This really ought to be an enum ...
    store_show_kind = GObject.Property(
        type=str,
        default='All',
        nick='store-show-kind',
        blurb='The cell type to show in the entities view')

    store_show_only_start = GObject.Property(
        type=bool,
        default=False,
        nick='store-show-only-start',
        blurb='Only show the start cell in the store view')

    files_show = GObject.Property(
        type=bool,
        default=True,
        nick='show-files',
        blurb='Show the file browser pane')

    files_show_headers = GObject.Property(
        type=bool,
        default=True,
        nick='files-show-headers',
        blurb='Show user header files in the file browser')

    files_show_system_headers = GObject.Property(
        type=bool,
        default=True,
        nick='files-show-system-headers',
        blurb='Show system header files in the file browser')

    summary_show = GObject.Property(
        type=bool,
        default=True,
        nick='show-summary',
        blurb='Show the summary pane')

    summary_show_read = GObject.Property(
        type=bool,
        default=True,
        nick='show-summary-read',
        blurb='Show the read column in the summary view')

    summary_show_written = GObject.Property(
        type=bool,
        default=True,
        nick='show-summary-written',
        blurb='Show the written column in the summary view')

    summary_show_allocated = GObject.Property(
        type=bool,
        default=True,
        nick='show-summary-allocated',
        blurb='Show the allocated column in the summary view')

    summary_show_freed = GObject.Property(
        type=bool,
        default=True,
        nick='show-summary-freed',
        blurb='Show the freed column in the summary view')

    summary_show_unused = GObject.Property(
        type=bool,
        default=False,
        nick='show-summary-unused',
        blurb='Show unused cells in the summary view')

    def __init__(self):
        GObject.GObject.__init__(self)

    def load_from_files(self):
        raise RuntimeError('Unimplemented loading configuration from file')

    def store_to_file(self):
        raise RuntimeError('Unimplemented writing configuration to file')


# Tag for clickable elements in the LLVM IR
class LinkTag(Gtk.TextTag):
    def __init__(self, inst: Instruction, *args, **kwargs):
        Gtk.TextTag.__init__(self, *args, **kwargs)
        self.instruction = inst
        self.set_property('underline', Pango.Underline.SINGLE)


# Stack to maintain the objects that are shown in the details view.
class DetailsQueue(GObject.GObject):
    can_go_back = GObject.Property(
        type=bool,
        default=False,
        nick='can-go-back',
        blurb='True if the cursor can be moved backwards')

    can_go_forward = GObject.Property(
        type=bool,
        default=False,
        nick='can-go-forward',
        blurb='True if the cursor can be moved forwards')

    cursor = GObject.Property(
        type=int,
        default=0,
        minimum=0,
        nick='cursor',
        blurb='The current object in the queue. Any entries will be added after this position')

    empty = GObject.Property(
        type=bool,
        default=True,
        nick='empty',
        blurb='True if the queue is empty')

    def __init__(self):
        GObject.GObject.__init__(self)

        self.queue: List[GObject.GObject] = []

        self.connect('notify::cursor', self.cb_cursor_changed)

    def cb_cursor_changed(self, obj: GObject.GObject, param: GObject.ParamSpec):
        self.can_go_back = (self.cursor > 1)
        self.can_go_forward = (self.cursor < len(self.queue))

    def add(self, obj: GObject.GObject):
        if self.queue and (len(self.queue) > self.cursor):
            self.queue = self.queue[:self.cursor]
        self.queue.append(obj)
        self.cursor += 1
        self.empty = False

    def back(self):
        if self.cursor:
            self.cursor -= 1
        return self.get()

    def forward(self) -> GObject.GObject:
        if self.cursor < len(self.queue):
            self.cursor += 1
        return self.get()

    def get(self) -> GObject.GObject:
        if self.cursor:
            return self.queue[self.cursor-1]
        return None

    def clear(self):
        self.queue = []
        self.cursor = 0

    def __str__(self) -> str:
        return ' -> '.join([str(o) for o in self.queue])


# Runtime state of the application that cannot be hidden inside any of the
# widgets. Right now, this is really messy because the state is split all
# over the place. Ideally, they should all be consolidated
class State:
    def __init__(self):
        self.queue = DetailsQueue()

    def clear(self):
        self.queue.clear()

class GtkUI:
    # float, float, float, float => str
    @staticmethod
    def color_to_html(color=None):
        # float, float, float, float => str
        def impl(r, g, b, a):
            for i in (r, g, b, a):
                if not (0.0 <= i <= 1.0):
                    raise RuntimeError('Color value out of range [0,1]', i)

            return '#' + ''.join(['{:02x}'.format(int(f * 255))
                                  for f in (r, g, b, a)])

        if not color:
            return impl(0, 0, 0, 0)
        elif isinstance(color, Gdk.RGBA):
            return impl(color.red, color.green, color.blue, color.alpha)
        elif isinstance(color, tuple):
            if len(color) == 3:
                return impl(color[0], color[1], color[2], 1.0)
            elif len(color) == 4:
                return impl(color[0], color[1], color[2], color[3])
            else:
                raise ValueError('Incorrect number of elements in color tuple',
                                 len(color))
        return None

    # Gtk.Builder
    def __init__(self):
        self.config = Config()
        self.state = State()
        self.analysis = Analysis()

        # There could probably be more context ID's, but I don't think it
        # likely since there are unlikely to be several things going on at once
        self.context_ids = {}

        mgr = GtkSource.LanguageManager.get_default()
        self.langs = {
            SourceLang.C: mgr.get_language('c'),
            SourceLang.CPP: mgr.get_language('cpp'),
            SourceLang.Fortran: mgr.get_language('fortran'),
            SourceLang.F77: mgr.get_language('f77'),
            SourceLang.LLVM: mgr.get_language('llvm'),
            None: None
        }

        # Not sure why I need to register some widgets, but if an error occurs
        # about some widget not being found, it's probably because it needs to
        # be registered here
        GObject.type_register(GtkSource.View)
        GObject.type_register(WebKit2.WebView)
        GObject.type_register(WebKit2.Settings)

        self.builder = Gtk.Builder.new_from_file(
            os.path.join(
                os.path.dirname(os.path.realpath(__file__)),
                'analyzer.glade'))

        style = self['cmb_regions'].get_style_context()
        self.cg_color = GtkUI.color_to_html(
            style.get_color(Gtk.StateFlags.NORMAL))

        # Icons
        self.icon_function = self['img_function'].get_pixbuf()
        self.icon_global = self['img_global'].get_pixbuf()
        self.icon_class = self['img_class'].get_pixbuf()
        self.icon_stack = self['img_stack'].get_pixbuf()
        self.icon_heap = self['img_heap'].get_pixbuf()
        self.icon_read = 'gtk-apply'
        self.icon_written = 'gtk-edit'
        self.icon_allocated = 'gtk-new'
        self.icon_freed = 'gtk-delete'
        self.icon_source = 'gtk-file'
        self.icon_link = 'gtk-go-forward'
        self.icon_summarize = 'gtk-execute'
        self.icon_trace = 'gtk-execute'

        self.ui_setup()
        self.bind_properties()

    # Look up a UI element
    # str => GObject.GObject
    def __getitem__(self, id):
        return self.builder.get_object(id)

    # Hook up properties on the UI to the configuration file
    def bind_properties(self):
        # str, str, str | GObject.GObject, GObject.GObject, bool, bool => int
        def bind(src_prop, dst_prop, dst, src=None,
                 invert=False, bidirectional=True):
            if not src:
                src = self.config
            elif isinstance(src, str):
                src = self[src]
            if isinstance(dst, str):
                dst = self[dst]
            flags = GObject.BindingFlags.SYNC_CREATE
            if bidirectional:
                flags |= GObject.BindingFlags.BIDIRECTIONAL
            if invert:
                flags |= GObject.BindingFlags.INVERT_BOOLEAN
            return src.bind_property(src_prop, dst, dst_prop, flags)

        # Hook up properties from the configuration object
        bind('mode-llvm', 'active', 'mitm_system_llvm')
        bind('show-artificial', 'active', 'mitm_view_artificial')
        bind('files-show', 'active', 'mitm_view_files_pane')
        bind('files-show', 'sensitive', 'mitm_view_files_headers')
        bind('files-show', 'sensitive', 'mitm_view_files_syshdrs')
        bind('files-show', 'visible', 'scrw_files')
        bind('code-show', 'visible', 'scrw_code')
        bind('code-style', 'style-scheme', self['srcvw_code'].get_buffer())
        bind('entities-show', 'active', 'mitm_view_entities_pane')
        bind('entities-show', 'visible', 'scrw_entities')
        bind('store-show', 'active', 'mitm_view_store_pane')
        bind('store-show', 'visible', 'scrw_store')
        bind('files-show-headers', 'active', 'mitm_view_files_headers')
        bind('files-show-system-headers', 'active', 'mitm_view_files_syshdrs')
        bind('summary-show', 'active', 'mitm_view_summary_pane')
        bind('summary-show', 'visible', 'scrw_summary')
        bind('summary-show-read', 'active', 'mitm_view_summary_read')
        bind('summary-show-read', 'visible', 'tvcol_summary_read')
        bind('summary-show-written', 'active', 'mitm_view_summary_written')
        bind('summary-show-written', 'visible', 'tvcol_summary_written')
        bind('summary-show-allocated', 'active', 'mitm_view_summary_allocated')
        bind('summary-show-allocated', 'visible', 'tvcol_summary_allocated')
        bind('summary-show-freed', 'active', 'mitm_view_summary_freed')
        bind('summary-show-freed', 'visible', 'tvcol_summary_freed')
        bind('summary-show-unused', 'active', 'mitm_view_summary_unused')

        # Add notificiations to the configuration properties.
        # Where possible, this is preferable to setting callback functions
        # on changes to the UI
        for p in ['entities-show-kind', 'show-artificial']:
            self.config.connect(
                'notify::' + p,
                lambda *args: self['trfltr_entities'].refilter())
        for p in ['store-show-kind', 'store-show-only-start']:
            self.config.connect(
                'notify::' + p,
                lambda *args: self['trfltr_store'].refilter())
        for p in ['files-show-headers', 'files-show-system-headers']:
            self.config.connect(
                'notify::' + p,
                lambda *args: self['trfltr_files'].refilter())
        for p in ['summary-show', 'summary-show-read', 'summary-show-written',
                  'summary-show-allocated', 'summary-show-freed']:
            self.config.connect(
                'notify::' + p,
                lambda *args: self['trfltr_summary'].refilter())

        # Hookup properties from the state object
        bind('can-go-back', 'sensitive', 'tlbtn_details_back', src=self.state.queue,
             bidirectional=False)
        bind('can-go-forward', 'sensitive', 'tlbtn_details_forward', src=self.state.queue,
             bidirectional=False)

        # Hook up properties from the analysis object
        bind('initialized', 'visible', 'pnd_0',
             src=self.analysis, bidirectional=False)
        bind('initialized', 'sensitive', 'mitm_file_close',
             src=self.analysis, bidirectional=False)

        # Bind widget properties to one another
        bind('value', 'zoom-level', 'web_cg', 'adj_cg_zoom', bidirectional=False)

    # Hook up everything that you can't in Glade
    def ui_setup(self):
        self['trfltr_files'].set_visible_func(self.filter_files)
        self['trfltr_entities'].set_visible_func(self.filter_entities)
        self['trfltr_store'].set_visible_func(self.filter_store)
        self['trfltr_details'].set_visible_func(self.filter_details)
        self['trfltr_summary'].set_visible_func(self.filter_summary)

        # This is a workaround to capture events in a custom widget
        # in a Gtk.TreeViewColumn header. Solution was found here:
        # https://stackoverflow.com/a/55584947
        #
        # Have to do this here because there doesn't seem to be a way to get
        # a handle to the button in Glade to set the function there
        for widget in (self['tvcol_entities_icon'],
                       self['tvcol_files_name'],
                       self['tvcol_store_icon']):
            widget.get_button().connect(
                'realize',
                lambda btn: btn.get_event_window().set_pass_through(True))

        self['rndr_store_start'].set_alignment(0.5, 0.5)

        self['trvw_files'].set_search_entry(self['srch_files'])
        self['trvw_entities'].set_search_entry(self['srch_entities'])
        self['trvw_store'].set_search_entry(self['srch_store'])

        self['trsel_details'].set_select_function(self.select_details)
        self['trsel_summary'].set_select_function(self.select_summary)

        lst = self['lstst_entities_cmpls']
        lst.append(['Name', self['cmpl_entities_name']])
        lst.append(['Location', self['cmpl_entities_location']])
        self['cmb_entities_search'].set_active(0)

        lst = self['lstst_store_cmpls']
        lst.append(['Address', self['cmpl_store_address']])
        lst.append(['Allocator', self['cmpl_store_allocator']])
        self['cmb_store_search'].set_active(0)

        self['tvcol_summary_read'].get_button().set_tooltip_text('Read')
        self['tvcol_summary_written'].get_button().set_tooltip_text('Written')
        self['tvcol_summary_allocated'].get_button().set_tooltip_text('Allocated')
        self['tvcol_summary_freed'].get_button().set_tooltip_text('Freed')

        for i in range(1, 20):
            self['scl_cg_zoom'].add_mark(
                float(i) / 10, Gtk.PositionType.BOTTOM, None)


    # Gtk.Window, Gdk.EventConfigure
    def ui_layout(self, win, evt_config):
        #
        # +--------------------------------------------+
        # |          ||         ||           ||        |
        # | entities || details || callgraph || files  |
        # |          ||         ||           ||        |
        # +==========||======================||========|
        # |          ||                      ||        |
        # |  store   ||       summary        ||  code  |
        # |          ||                      ||        |
        # +--------------------------------------------+
        #
        # |           |        pnd_3t        |
        # |  pnd_2l  || ...... pnd_2r ...... |
        # | ........... pnd_1l ............. || pnd_1r |
        # | .................. pnd_0 ................. |

        pnd_0 = self['pnd_0']
        pnd_1l = self['pnd_1l'] # This contains pnd_2l and pnd_2r
        pnd_1r = self['pnd_1r']
        pnd_2l = self['pnd_2l']
        pnd_2r = self['pnd_2r']
        pnd_3t = self['pnd_3t'] # This contains the details and callgraphs views

        width = evt_config.width
        height = evt_config.height - self['mbar_main'].get_allocation().height

        pnd_1r.set_size_request(width * 0.3, -1)
        pnd_2l.set_size_request(width * 0.2, -1)
        pnd_2r.set_size_request(width * 0.5, -1)
        pnd_3t.set_size_request(-1, height * 2/5)
        self['scrw_entities'].set_min_content_height(height * 1/3)
        self['scrw_store'].set_min_content_height(height * 1/3)
        self['scrw_details'].set_min_content_width(width * 1/5)
        self['scrw_cg'].set_min_content_width(width * 1/5)
        self['scrw_summary'].set_min_content_height(height * 2/5)
        self['scrw_code'].set_min_content_height(height * 1/2)
        pnd_2l.set_position(height * 1/2)
        pnd_2r.set_position(height * 1/2)

        return False

    # Function that updates the UI asynchronously. There may be a lot of
    # recomputation that takes place while updating, so we disable the
    # entire UI while updating. Probably don't want to always do that
    def ui_update(self, fn, *args):
        def impl(*args):
            fn(*args)
            return False

        GLib.idle_add(impl, *args, priority=GLib.PRIORITY_DEFAULT_IDLE)

    # None => None
    def clear(self):
        self['trst_summary'].clear()
        self['trst_details'].clear()
        self['trst_entities'].clear()
        self['lstst_store'].clear()
        self['trst_files'].clear()
        self['lstst_regions'].clear()
        self['lstst_entities_search'].clear()
        self['lstst_files_search'].clear()
        self['lstst_allocators_search'].clear()
        self.reposition_panes = True
        self.state.clear()
        self.analysis.clear()

    # None => Callgraph
    def get_selected_callgraph(self):
        cmb = self['cmb_regions']
        model = cmb.get_model()
        if cmb.get_active_iter():
            return model[cmb.get_active_iter()][ModelColsRegions.Callgraph]
        return None

    # None => Region
    def get_selected_region(self):
        cmb = self['cmb_regions']
        model = cmb.get_model()
        if cmb.get_active_iter():
            return model[cmb.get_active_iter()][ModelColsRegions.Region]
        return None

    # Gtk.SortType => Gtk.SortType
    def get_opposite_sort_type(self, sort):
        if sort != Gtk.SortType.ASCENDING:
            return Gtk.SortType.ASCENDING
        return Gtk.SortType.DESCENDING

    # Cell, bool => str
    def format_address(self, cell, lead=False):
        if lead:
            return '{:>012}'.format(cell.address)
        else:
            return '{:>12} '.format(cell.address)

    def get_location(self, obj) -> SourceLoc:
        return obj.loc_ir if self.config.mode_llvm else obj.loc_source
    # Gtk.TreeModel, Gtk.TreeIter, * => bool
    def filter_entities(self, model, trit, data):
        row = model[trit]
        obj = row[ModelColsEntities.Object]
        if obj.artificial and (not self.config.show_artificial):
            return False
        if self.config.entities_show == 'Classes':
            # If we are only viewing classes, any functions (not methods)
            # should be hidden
            if isinstance(obj, Global):
                return False
            elif isinstance(obj, Function):
                if not obj.cls:
                    return False
        elif self.config.entities_show == 'Functions':
            # If we are only viewing functions, any classes that don't have
            # methods should be hidden. If the class has a function, it should
            # be visible because we might want to look at the methods
            if isinstance(obj, Global):
                return False
            elif isinstance(obj, Class):
                if not obj.methods:
                    return False
        elif self.config.entities_show == 'Globals':
            if not isinstance(obj, Global):
                return False

        srch = self['srch_entities'].get_text()
        if srch:
            cols = { 'Name': ModelColsEntities.Name,
                     'Location': ModelColsEntities.Location }
            col = cols[self['cmb_entities_search'].get_active_id()]
            if not row[col].startswith(srch):
                return False

        # # If a file has been selected in the file view, we should also
        # # filter based on that
        # if False:
        #     if self.config.fltr_entities_search == 'Location':
        #         location = row[ModelColsEntities.Location]
        #         if not location.startswith(srch):
        #             if isinstance(obj, Class):
        #                 if not [f for f in obj.methods
        #                         if f.location \
        #                         and f.location.basename.startswith(srch)]:
        #                     return False
        #             else:
        #                 return False
        return True

    # Callback function to filter the store
    # Gtk.TreeModel, Gtk.TreeIter, ? => bool
    def filter_store(self, model, trit, data):
        flags = { 'Globals': Cell.Flags.Global,
                  'Stack': Cell.Flags.Stack,
                  'Heap': Cell.Flags.Heap }

        row = model[trit]
        cell = row[ModelColsStore.Cell]
        allocator = row[ModelColsStore.Allocator]

        if self.config.store_show_only_start:
            if Cell.Flags.Start not in cell.flags:
                return False

        if self.config.store_show_kind != 'All':
            if flags[self.config.store_show_kind] not in cell.flags:
                return False

        srch = self['srch_store'].get_text()
        if srch:
            col = self['cmb_store_search'].get_active_id()
            if col == 'Address':
                if not str(cell.address).startswith(srch):
                    return False
            elif col == 'Allocator':
                if not allocator.startswith(srch):
                    return False

        return True

    # Callback function to filter the files
    # Gtk.TreeModel, Gtk.TreeIter, ? => bool
    def filter_files(self, model, trit, data):
        row = model[trit]
        name = row[ModelColsFiles.Name]
        fil = row[ModelColsFiles.SourceFile]
        srch = self['srch_files'].get_text()

        if fil:
            if (fil.header or fil.system_header) \
               and (not self.config.files_show_headers):
                return False
            elif fil.system_header \
                 and (not self.config.files_show_system_headers):
                return False

        if srch:
            if not name.startswith(srch):
                if not [c for c in row.iterchildren()
                        if c[ModelColsFiles.Name].startswith(srch)]:
                    return False
            elif not fil:
                # If this is a directory that matches, we should only show
                # it if at least one of the files matches
                if not [c for c in row.iterchildren()
                        if c[ModelColsFiles.Name].startswith(srch)]:
                    return False

        return True

    # Callback function to filter the details
    # Gtk.TreeModel, Gtk.TreeIter, ? => bool
    def filter_details(self, model, trit, data):
        obj = model[trit][ModelColsDetails.Object]
        if type(obj) in [Function, Global, Local, Argument, Class]:
            if obj.artificial and (not self.config.show_artificial):
                return False
        elif isinstance(obj, Instruction):
            if obj.function not in self.get_selected_callgraph():
                return False
        return True

    # Callback function to filter the summary
    # Gtk.TreeModel, Gtk.TreeIter, ? => bool
    def filter_summary(self, model, trit, data):
        summary = model[trit][ModelColsSummary.Summary]
        cg = self.get_selected_callgraph()
        if (not self.config.summary_show_unused) and (not summary.status[cg].is_used()):
            return False
        return True

    # Select function for the details view. Should only be allowed to
    # select types
    def select_details(self, trsel, model, path, selected):
        obj = model[model.get_iter(path)][ModelColsDetails.Object]
        if isinstance(obj, Type):
            return True
        return False

    # Select function for the details view. Should only be allowed to select
    # cells that can be traced
    def select_summary(self, trsel, model, path, selected):
        summary = model[model.get_iter(path)][ModelColsSummary.Summary]
        if summary.cell and (type(summary.type) not in [StructType, ArrayType]):
            return True
        return False

    # Refresh the code shown
    # This will get called when file or entity is selected
    # SourceLoc, int => False
    def populate_code(self, loc=None, f=None):
        # Gtk.TextIter => False
        def update_fn(it):
            srcvw.scroll_to_iter(it, 0.25, True, 0.0, 0.0)
            return False

        def collect_tags(tag):
            if isinstance(tag, LinkTag):
                tags.append(tag)

        srcvw = self.builder.get_object('srcvw_code')
        srcbuf = srcvw.get_buffer()
        # Remove any underline tags from the tagtable
        tags = []
        srcbuf.get_tag_table().foreach(collect_tags)
        for tag in tags:
            srcbuf.get_tag_table().remove(tag)

        if not loc:
            srcbuf.set_text('')
        elif loc.file:
            src = loc.file
            line = loc.line
            col = loc.column
            srcbuf.set_language(self.langs[src.lang])
            srcbuf.set_text(src.code, len(src.code))
            if col:
                it = srcbuf.get_iter_at_line_offset(line - 1, col)
            else:
                it = srcbuf.get_iter_at_line(line - 1)
            srcbuf.place_cursor(it)

            if self.config.mode_llvm and f:
                for inst in f.get_instructions():
                    if isinstance(inst, LoadInstruction) \
                       or isinstance(inst, StoreInstruction):
                        loc = inst.loc_ir
                        lno = loc.line-1
                        line = loc.file.lines[lno]
                        col = line.rfind(inst.pointer)
                        if col != -1:
                            beg = srcbuf.get_iter_at_line_offset(lno, col)
                            end = srcbuf.get_iter_at_line_offset(lno, col + len(inst.pointer))
                            tag = LinkTag(inst)
                            srcbuf.get_tag_table().add(tag)
                            srcbuf.apply_tag(tag, beg, end)

            self.ui_update(update_fn, it)

        return False

    # Refresh the known regions
    # This is only called when a new analysis file is loaded
    # None => False
    def populate_regions(self):
        store = self['lstst_regions']

        store.clear()

        store.append([
            '', 'Whole program', 'Whole program', self.analysis.closure, None])
        for region in self.analysis.get_regions():
            store.append([
                str(region.id), region.name, region.name, region.closure, region])
        self.builder.get_object('cmb_regions').set_active_id('')

        return False

    # Refresh the list of known files
    # This is only called when a new analysis file is loaded
    # None => False
    def populate_files(self):
        # Gtk.TreeIter, str, str, str, str, bool, Pango.Style, SourceFile, bool
        #   => Gtk.TreeIter
        def add_row(trit,
                    id,
                    name,
                    tooltip=None,
                    sort=None,
                    sensitive=True,
                    style=Pango.Style.NORMAL,
                    src=None,
                    ellipsize=Pango.EllipsizeMode.END):
            if not tooltip:
                tooltip = id
            if not sort:
                sort = name
            return files.append(trit,
                                [id, tooltip, sort, name, sensitive,
                                 src, ellipsize])

        # [string], {} => None
        def mk_tree(comps, tree):
            if comps:
                if comps[0] not in tree:
                    tree[comps[0]] = {}
                mk_tree(comps[1:], tree[comps[0]])

        # {}, Gtk.TreeIter, os.path => None
        def populate(curr, trit=None, path=''):
            # If the directory has only a single entry and it is not a file,
            # merge it with the current one instead of making a child
            if (len(curr) == 1) and list(curr.values())[0]:
                dir = list(curr.keys())[0]
                next_path = os.path.join(path, dir)
                if trit:
                    row = files[trit]
                    row[ModelColsFiles.Name] = dir
                    row[ModelColsFiles.Tooltip] = next_path
                    row[ModelColsFiles.Ellipsize] = Pango.EllipsizeMode.START
                populate(list(curr.values())[0], trit, next_path)
            else:
                for key, val in curr.items():
                    next_path = os.path.join(path, key)
                    # For the files, val will be an empty map
                    if val:
                        next_iter = add_row(trit,
                                            next_path,
                                            key,
                                            sensitive=False)
                        populate(val, next_iter, next_path)
                    else:
                        fil = self.analysis.files[next_path]
                        add_row(trit, next_path, key, src=fil)
                        filenames.add(fil.base)

        trvw = self['trvw_files']
        files = self['trst_files']
        cmpl = self['cmpl_files']
        search = self['lstst_files_search']
        filenames = set([])

        # Disconnect all filters
        trvw.set_model(None)
        cmpl.set_model(None)
        files.clear()
        search.clear()

        tree = {}
        for path in sorted([p for p, f in self.analysis.files.items() if f.path]):
            comps = path.split(os.sep)
            if not comps[0]:
                comps[0] = '/'
            mk_tree(comps, tree)
        populate(tree)

        for f in filenames:
            search.append([f])

        trvw.set_model(self['trsrt_files'])
        cmpl.set_model(search)

        # Update UI
        trvw.collapse_all()

        return False

    # Populate the entities. This is called only when the analysis is first
    # loaded
    def populate_entities(self):
        def add_row(obj, trit=None):
            icons = { Function: self.icon_function,
                      Global: self.icon_global,
                      Class: self.icon_class }
            names.add(obj.name)
            return entities.append(
                trit,
                [obj,
                 icons[type(obj)],
                 obj.name,
                 Pango.Style.ITALIC if obj.artificial else Pango.Style.NORMAL,
                 obj.loc_source.basename if obj.loc_source else ''])

        trvw = self['trvw_entities']
        entities = self['trst_entities']
        cmpl = self['cmpl_entities_name']
        search = self['lstst_entities_search']
        names = set([])

        # Disconnect all filters
        trvw.set_model(None)
        cmpl.set_model(None)
        entities.clear()

        for f in self.analysis.get_functions():
            add_row(f)

        for g in self.analysis.get_globals():
            add_row(g)

        for c in self.analysis.get_classes():
            add_row(c)

        for name in names:
            search.append([name])

        cmpl.set_model(search)
        trvw.set_model(self['trsrt_entities'])

        return False

    # Populate the store. This is called only when the analysis is first
    # loaded
    # None => False
    def populate_store(self):
        trvw = self['trvw_store']
        store = self['lstst_store']
        allocators = self['lstst_allocators_search']

        # Disconnect all filters and models
        trvw.set_model(None)
        store.clear()
        allocators.clear()

        allocator_names = set(['<static>'])
        allocators.append(['<static>', None])
        for cell in self.analysis.store.cells():
            if (Cell.Flags.Code in cell.flags) \
               or (Cell.Flags.Argument in cell.flags):
                continue
            icon = self.icon_heap
            if Cell.Flags.Global in cell.flags:
                icon = self.icon_global
            elif Cell.Flags.Stack in cell.flags:
                icon = self.icon_stack
            start = '*' if Cell.Flags.Start in cell.flags else ''
            allocators = list(cell.allocators(Function))
            allocator = allocators[0] if allocators else None

            store.append([cell,
                          icon,
                          self.format_address(cell),
                          allocator.name if allocator else '<static>',
                          start])
            if allocator and allocator.name not in allocator_names:
                allocator_names.add(allocator.name)
                allocators.append([allocator.name, allocator])

        trvw.set_model(self['trfltr_store'])

        return False

    # GObject.Gobject, str, str, str, str, Gtk.TreeIter
    #   => Gtk.TreeIterator
    def add_details(self,
                    obj=None, tooltip='', sort='',
                    label='', content='', cell=None,
                    action='',
                    trit=None):
        details = self['trst_details']
        return details.append(trit,
                              [obj, tooltip, sort, label, content,
                               cell, action])

    # Populate the details of an instruction
    def populate_details_instruction(self, instruction: Instruction):
        names = { LoadInstruction: 'Load',
                  StoreInstruction: 'Store',
                  CallInstruction: 'Call' }
        self.add_details(label='Kind', content=names[type(instruction)])
        self.add_details(obj=instruction.loc_ir,
                         label='Location',
                         content=instruction.loc_ir.format(),
                         action=self.icon_source)
        if instruction.targets:
            targets = list(instruction.targets)
            self.add_details(obj=targets[0],
                             label='Targets',
                             content=str(targets[0].address),
                             action=self.icon_link)
            for target in targets[1:]:
                self.add_details(obj=target,
                                 content=self.format_address(target),
                                 action=self.icon_link)
        self.add_details(obj=instruction.function,
                         label='Function',
                         content=instruction.function.name,
                         action=self.icon_link)

    # Populate the details of a function
    # Function, Gtk.TreeStore => False
    def populate_details_function(self, f):
        rndr = self['rndr_details_contents']
        rndr.set_property(
            'style',
            Pango.Style.NORMAL if not f.artificial else Pango.Style.ITALIC)

        self.add_details(label='Kind', content='Function')
        if f.source:
            self.add_details(label='Name', content=f.source)
        elif f.artificial:
            self.add_details(label='Artificial', content=f.artificial)
        if f.qualified:
            self.add_details(label='Qualified', content=f.qualified)
        if f.full:
            self.add_details(label='Full', content=f.full)
        if f.cls:
            self.add_details(obj=f.cls,
                             label='Class',
                             content=f.cls.type.name,
                             action=self.icon_link)
        if self.get_location(f):
            loc = self.get_location(f)
            self.add_details(obj=loc,
                             label='Location',
                             content=loc.format(),
                             action=self.icon_source)
        if f.args:
            argit = self.add_details(label='Arguments',
                                     obj=f.args[0],
                                     content=f.args[0].name,
                                     action=self.icon_link)
            for a in f.args[1:]:
                self.add_details(trit=argit,
                                 obj=a,
                                 content=a.name,
                                 action=self.icon_link)
        else:
            self.add_details(label='Arguments')

        if f.locals:
            allocas = list(f.get_locals())

            lit = self.add_details(label='Locals',
                                   obj=allocas[0],
                                   sort=allocas[0].name,
                                   content=allocas[0].name,
                                   action=self.icon_link)
            for alloca in allocas[1:]:
                self.add_details(trit=lit,
                                 obj=alloca,
                                 sort=alloca.name,
                                 content=alloca.name,
                                 action=self.icon_link)
        else:
            self.add_details(label='Locals')

        if f.callsites:
            sites = list(f.callsites)
            loc = self.get_location(sites[0])
            cit=self.add_details(label='Callsites',
                                 obj=loc,
                                 sort=loc.format(),
                                 content=loc.format(),
                                 action=self.icon_source)
            for inst in sites[1:]:
                loc = self.get_location(inst)
                self.add_details(trit=cit,
                                 obj=loc,
                                 sort=loc.format(),
                                 content=loc.format(),
                                 action=self.icon_source)
        else:
            self.add_details(label='Callsites')

        return False

    # Populate the details of a global
    # Global, Gtk.TreeStore => False
    def populate_details_global(self, g):
        rndr = self['rndr_details_contents']
        rndr.set_property(
            'style',
            Pango.Style.NORMAL if not g.artificial else Pango.Style.ITALIC)

        self.add_details(label='Kind', content='Global')
        if g.source:
            self.add_details(label='Name', content=g.source)
        elif g.artificial:
            self.add_details(label='Artificial', content=g.artificial)
        if g.qualified:
            self.add_details(label='Qualified', content=g.qualified)
        if g.full:
            self.add_details(label='Full', content=g.full)
        if self.get_location(g):
            loc = self.get_location(g)
            self.add_details(obj=loc,
                             sort=loc.format(),
                             label='Location',
                             content=loc.format(),
                             action=self.icon_source)
        self.add_details(label='Type',
                         content=g.type.llvm)

    # Populate the details of a class
    # Class => False
    def populate_details_class(self, cls):
        rndr = self['rndr_details_contents']
        rndr.set_property(
            'style',
            Pango.Style.NORMAL if not cls.artificial else Pango.Style.ITALIC)

        self.add_details(label='Kind', content='Class')
        if arg.source:
            self.add_details(label='Name', content=cls.source)
        elif arg.artificial:
            self.add_details(label='LLVM', content=cls.type.llvm)
        if cls.type.loc_source:
            self.add_details(obj=cls.type.loc_source,
                             label='Location',
                             content=cls.type.loc_source.basename,
                             action=self.icon_source)
        if cls.methods:
            methods = list(cls.methods)
            mit = self.add_details(label='Methods',
                                   obj=methods[0],
                                   sort=methods[0].name,
                                   content=methods[0].name,
                                   action=self.icon_link)
            for f in methods[1:]:
                self.add_details(trit=mit,
                                 obj=f,
                                 sort=f.name,
                                 content=f.name,
                                 action=self.icon_link)
        else:
            self.add_details(label='Methods')

    # Populate the details of a function argument
    # Argument => False
    def populate_details_argument(self, arg):
        rndr = self['rndr_details_contents']
        rndr.set_property(
            'style',
            Pango.Style.NORMAL if not arg.artificial else Pango.Style.ITALIC)

        self.add_details(label='Kind', content='Argument')
        self.add_details(label='#', content=str(arg.argno))
        if arg.source:
            self.add_details(label='Name', content=arg.source)
        elif arg.artificial:
            self.add_details(label='Artificial', content=arg.artificial)
        if self.get_location(arg):
            loc = self.get_location(arg)
            self.add_details(loc,
                             label='Location',
                             content=loc.format(),
                             action=self.icon_source)
        self.add_details(label='Type',
                         content=arg.type.llvm)
        self.add_details(obj=arg.function,
                         label='Function',
                         content=arg.function.name,
                         action=self.icon_link)

    # Populate the details of a local variable
    # Local => False
    def populate_details_local(self, local):
        rndr = self['rndr_details_contents']
        rndr.set_property(
            'style',
            Pango.Style.NORMAL if not local.artificial else Pango.Style.ITALIC)

        self.add_details(label='Kind', content='Local')
        if local.source:
            self.add_details(label='Name', content=local.source)
        elif local.artificial:
            self.add_details(label='Artificial', content=local.artificial)
        if self.get_location(local):
            loc = self.get_location(local)
            self.add_details(obj=loc,
                             label='Location',
                             content=loc.format(),
                             action=self.icon_source)
        self.add_details(label='Type',
                         content=local.type.llvm)
        self.add_details(obj=local.function,
                         label='Function',
                         content=local.function.name,
                         action=self.icon_link)

    # Populate the details of a store cell
    # Cell => False
    def populate_details_cell(self, cell):
        self.add_details(label='Kind', content='Cell')
        self.add_details(label='Address', content=str(cell.address))
        flags = 'Static'
        if Cell.Flags.Stack in cell.flags:
            flags = 'Stack'
        elif Cell.Flags.Heap in cell.flags:
            flags = 'Heap'
        elif Cell.Flags.Code in cell.flags:
            flags = 'Code'
        elif Cell.Flags.Argument in cell.flags:
            flags = 'Argument'
        elif Cell.Flags.Constant in cell.flags:
            flags = 'Constant'
        elif Cell.Flags.Global in cell.flags:
            flags = 'Global'
        self.add_details(label='Flags',
                         content=flags)
        self.add_details(obj=cell.type,
                         label='Type',
                         content=cell.type.llvm,
                         action=self.icon_summarize)
        if cell.allocated:
            self.add_details(obj=cell.allocated,
                             label='Allocated',
                             content=cell.allocated.llvm,
                             cell=cell,
                             action=self.icon_summarize)
        if cell.structs:
            first = True
            sit = None
            for struct in cell.structs:
                label = 'Structs' if first else None
                nxt = self.add_details(trit=sit,
                                       obj=struct,
                                       content=struct.llvm,
                                       action=self.icon_summarize)
                if first:
                    sit = nxt
                first = False

        if cell.allocators(Instruction):
            inst = list(cell.allocators(Instruction))[0]
            loc = self.get_location(inst)
            self.add_details(obj=loc,
                             label='Allocator',
                             content=inst.function.name,
                             action=self.icon_link)

        if isinstance(cell.type, PointerType):
            first = True
            tit = None
            for target in cell.targets:
                lbl = 'Targets' if first else None
                nxt = self.add_details(trit=tit,
                                       label=lbl,
                                       obj=target,
                                       sort=self.format_address(target, lead=True),
                                       content=self.format_address(target),
                                       action=self.icon_link)
                if first:
                    tit = nxt
                first = False
        else:
            self.add_details(label='Empty', content=str(cell.empty))

        for lbl, fn in zip(['Readers', 'Writers', 'Deallocators'],
                                  [cell.readers, cell.writers, cell.deallocators]):
            insts = list(fn(Instruction))
            if insts:
                first = True
                ait = None
                for inst in insts:
                    loc = self.get_location(inst)
                    if not loc:
                        print('WARNING: Could not find location for', inst.id)
                        loc = SourceLoc(self.analysis)

                    label = lbl if first else None
                    nxt = self.add_details(trit=ait,
                                           label=label,
                                           obj=loc,
                                           sort=loc.format(column=True),
                                           content=loc.format(),
                                           action=self.icon_source)
                    if first:
                        ait = nxt
                    first = False
            else:
                self.add_details(label=lbl)

    # Populate the details view when an entity or a store cell is activated
    def populate_details(self, obj: GObject.GObject, add: bool=True):
        trvw = self['trvw_details']
        details = self['trst_details']

        trvw.set_model(None)
        details.clear()
        if add:
            self.state.queue.add(obj)

        fns = { Function: self.populate_details_function,
                Global: self.populate_details_global,
                Class: self.populate_details_class,
                Argument: self.populate_details_argument,
                Local: self.populate_details_local,
                Cell: self.populate_details_cell,
                LoadInstruction: self.populate_details_instruction,
                StoreInstruction: self.populate_details_instruction,
                CallInstruction: self.populate_details_instruction}
        fns[type(obj)](obj)

        trvw.set_model(self['trfltr_details'])
        trvw.expand_all()

        return False

    # Populate the summary view when the type of an entity is selected
    # Cell, Type, str => False
    def populate_summary(self, cell, typ, name):
        def add_summary(summary, trit=None):
            addr = ''
            action = ''
            style = Pango.Style.NORMAL
            if type(summary.type) in [StructType, ArrayType]:
                style = Pango.Style.ITALIC
            if summary.cell:
                addr = self.format_address(summary.cell)
                if type(summary.type) not in [StructType, ArrayType]:
                    action = self.icon_trace
            nxt = trst.append(
                trit,
                [ summary,
                  summary.type.llvm,
                  addr,
                  summary.name,
                  self.icon_read if summary.status[cg].is_read() else '',
                  self.icon_written if summary.status[cg].is_written() else '',
                  self.icon_allocated if summary.status[cg].is_allocated() else '',
                  self.icon_freed if summary.status[cg].is_freed() else '',
                  style,
                  action])
            for sub in summary.subs:
                add_summary(sub, nxt)

        trvw = self['trvw_summary']
        trst = self['trst_summary']

        trvw.set_model(None)
        trst.clear()

        cg = self.get_selected_callgraph()
        add_summary(Summary.new(cell, typ, name))
        trvw.set_model(self['trfltr_summary'])
        trvw.expand_all()

        return False

    # Gtk.TreeViewColumn, bool => False
    def cb_tv_expand_collapse(self, tvcol, expand):
        trvw = tvcol.get_tree_view()
        if expand:
            tvcol.set_title('-')
            trvw.expand_all()
        else:
            tvcol.set_title('+')
            trvw.collapse_all()
        return False

    # * => False
    def cb_files_filter(self, *args):
        self['trfltr_files'].refilter()
        if self['srch_files'].get_text():
            self.cb_tv_expand_collapse(self['trvw_files'].get_column(0), True)

        return False

    # * => False
    def cb_files_search_start(self, *args):
        self['srch_files'].grab_focus()
        return True

    # * => False
    def cb_files_search(self, *args):
        self.cb_files_filter()

        return False

    # * => False
    def cb_files_sort_name(self, *args):
        trsrt = self['trsrt_files']
        col, sort = trsrt.get_sort_column_id()
        if col is None:
            col = int(ModelColsFiles.SortKey)
        if sort is None:
            sort = Gtk.SortType.ASCENDING
        elif sort == Gtk.SortType.ASCENDING:
            sort = Gtk.SortType.DESCENDING
        else:
            sort = Gtk.SortType.ASCENDING
        trsrt.set_sort_column_id(col, sort)

        return False

    # Gtk.TreeViewColumn => False
    def cb_store_only_start_toggle(self, tvcol):
        if tvcol.get_title() == '-':
            self.config.show_only_start = False
            tvcol.set_title('+')
        else:
            self.config.show_only_start = True
            tvcol.set_title('-')

        return False

    # Gtk.MenuItem => False
    def cb_store_show_all(self, mitm):
        self.config.store_show_kind = 'All'
        return False

    # Gtk.MenuItem => False
    def cb_store_show_global(self, mitm):
        self.config.store_show_kind = 'Globals'
        return False

    # Gtk.MenuItem => False
    def cb_store_show_heap(self, mitm):
        self.config.store_show_kind = 'Heap'
        return False

    # Gtk.MenuItem => False
    def cb_store_show_stack(self, mitm):
        self.config.store_show_kind = 'Stack'
        return False

    # Gtk.TreeView, Gtk.TreePath, Gtk.TreeViewColumn => False
    def cb_cell_show_details(self, trvw, path, column):
        model = trvw.get_model()
        trit = model.get_iter(path)

        self.populate_details(model[trit][ModelColsStore.Cell])

        return False;

    # Gtk.TreeModelFilter => False
    def cb_filter(self, trfltr):
        trfltr.refilter()
        return False

    # Gtk.ComboBox, Gtk.SearchEntry => False
    def cb_search_column_changed(self, *args):
        # FIXME: Need to deal with this properly
        model = cmb.get_model()
        cmpl = model[cmb.get_active_iter()][1]
        srch.set_completion(cmpl)
        txt = srch.get_text()
        srch.set_text('')
        srch.set_text(txt)

        return False

    # Gtk.Button, Gtk.TreeModelSort, int => False
    def cb_tvcol_sort(self, btn, trsrt, col):
        _, sort = trsrt.get_sort_column_id()
        trsrt.set_sort_column_id(col, self.get_opposite_sort_type(sort))
        return False

    # Gtk.TreeViewColumn => False
    def cb_tvcol_expand_all(self, tvcol):
        self.cb_tv_expand_collapse(tvcol, tvcol.get_title() == '+')

        return False
    # Gtk.MenuItem => False
    def cb_entities_show_all(self, mitm):
        self.config.entities_show_kind = 'All'
        return False

    # Gtk.MenuItem => False
    def cb_entities_show_classes(self, mitm):
        self.config.entities_show_kind = 'Classes'
        return False

    # Gtk.MenuItem => False
    def cb_entities_show_functions(self, mitm):
        self.config.entities_show_kind = 'Functions'
        return False

    # Gtk.MenuItem => False
    def cb_entities_show_globals(self, mitm):
        self.config.entities_show_kind = 'Globals'
        return False

    # Gtk.TreeView, Gtk.TreePath, Gtk.TreeViewColumn => bool
    def cb_entity_show_details(self, trvw, path, column):
        model = trvw.get_model()
        trit = model.get_iter(path)
        obj = model[trit][ModelColsEntities.Object]
        f = None
        if isinstance(obj, Function):
            f = obj
        elif isinstance(obj, Argument) or isinstance(obj, Instruction) or isinstance(obj, Local):
            f = obj.function

        if self.config.mode_llvm:
            self.populate_code(obj.loc_ir, f=f)
        else:
            self.populate_code(obj.loc_source)
        self.populate_details(obj)

        return False

    # * => False
    def cb_details_filter(self, *args):
        self['trfltr_details'].refilter()

    # Gtk.TreeViewColumn => False
    def cb_details_expand_toggle(self, tvcol):
        trvw = self['trvw_details']
        if tvcol.get_title() == '+':
            trvw.expand_all()
        else:
            trvw.collapse_all()
        return False

    # Gtk.TreeView, Gtk.TreePath, Gtk.TreeViewColumn => False
    def cb_details_activate(self, trvw, path, column):
        model = trvw.get_model()
        trit = model.get_iter(path)
        obj = model[trit][ModelColsDetails.Object]
        if isinstance(obj, Type):
            # The type could be part of a cell or an entity
            cell = model[trit][ModelColsDetails.Cell]
            self.populate_summary(cell, obj, '')
        elif isinstance(obj, SourceLoc):
            self.populate_code(obj)
        elif isinstance(obj, Instruction):
            self.populate_code(self.get_location(obj), f=obj.function)
        elif isinstance(obj, Function):
            self.populate_code(self.get_location(obj), f=obj)
            self.populate_details(obj)
        elif isinstance(obj, Argument) \
             or isinstance(obj, Local):
            self.populate_code(self.get_location(obj), f=obj.function)
            self.populate_details(obj)
            self.populate_summary(obj.cell, obj.type, obj.name)
        elif isinstance(obj, Global):
            self.populate_code(self.get_location(obj))
            self.populate_details(obj)
            self.populate_summary(obj.cell, obj.type, obj.name)
        elif isinstance(obj, Cell):
            self.populate_details(obj)
        return False

    def cb_details_go_back(self, btn: Gtk.Button) -> bool:
        self.state.queue.back()
        self.populate_details(self.state.queue.get(), False)
        return False

    def cb_details_go_forward(self, btn: Gtk.Button) -> bool:
        self.state.queue.forward()
        self.populate_details(self.state.queue.get(), False)
        return False

    # * => False
    def cb_summary_filter(self, *args):
        self['trfltr_summary'].refilter()

        return False

    # Gtk.TreeView, Gtk.TreePath, Gtk.TreeViewColumn => False
    def cb_summary_activate(self, trvw, path, column):
        def close(nodes: Set[Function]):
            old = len(nodes)
            for f in list(nodes):
                for caller in cg.callers[f]:
                    nodes.add(caller)
            if len(nodes) > old:
                close(nodes)

        model = trvw.get_model()
        summary = model[model.get_iter(path)][ModelColsSummary.Summary]
        cg = self.get_selected_callgraph()

        if summary.cell:
            self.populate_details(summary.cell)

        direct = set([cg.root])
        readers = [f for f in summary.readers if f in cg]
        writers = [f for f in summary.writers if f in cg]
        allocators = [f for f in summary.allocators if f in cg]
        deallocators = [f for f in summary.deallocators if f in cg]

        for accessors in (readers, writers, allocators, deallocators):
            direct.update(accessors)
        closed = set(direct) # Shallow copy
        close(closed)

        sg = cg.get_subgraph(closed)
        sg.add_subgraph([sg.get_node(f.id) for f in readers],
                        name='readers', rank='same')
        sg.add_subgraph([sg.get_node(f.id) for f in writers],
                        name='writers', rank='same')
        sg.add_subgraph([sg.get_node(f.id) for f in allocators],
                        name='allocators', rank='same')
        sg.add_subgraph([sg.get_node(f.id) for f in deallocators],
                        name='deallocators', rank='same')
        sg.graph_attr.update(
            rankdir='LR',
            bgcolor=GtkUI.color_to_html(),
            color=self.cg_color)
        sg.edge_attr.update(color=self.cg_color)
        for node in sg:
            f = self.analysis.get_function(int(node.attr['moya']))
            if f == cg.root:
                node.attr['shape'] = 'box'
            elif f in direct:
                node.attr['shape'] = 'ellipse'
            else:
                node.attr['shape'] = 'point'
            node.attr['fontname'] = 'Liberation Mono'
            node.attr['fontsize'] = 9
            node.attr['tooltip'] = f.name
            node.attr['color'] = self.cg_color
            if f in direct:
                node.attr['style'] = 'filled'
                node.attr['URL'] = '{}'.format(f.id)
            else:
                node.attr['style'] = 'solid'

            label = ''
            label += 'R' if f in summary.readers else ''
            label += 'W' if f in summary.writers else ''
            label += 'A' if f in summary.allocators else ''
            label += 'F' if f in summary.deallocators else ''
            node.attr['label'] = label
        for edge in sg.edges():
            edge.attr['color'] = self.cg_color
        sg.layout('dot')

        with io.BytesIO() as svg, io.BytesIO() as cmapx:
            sg.draw(svg, format='svg')
            sg.draw(cmapx, format='cmapx')

            html_base = '''
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Moya visualizer graph</title>
    <style>
      html, body {{
          margin: 0;
          padding: 0;
          width: 100%;
          height: 100%;
          max-width: 100%;
          max-height: 100%;
          overflow: auto;
          position: fixed;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
      }}
    </style>
  </head>
  <body>
      <img src="data:image/svg+xml;base64,{}" usemap="#moya" />
      {}
  </body>
</html>
'''
            html = html_base.format(b64encode(svg.getvalue()).decode('utf-8'),
                                    cmapx.getvalue().decode('utf-8'))

        web = self['web_cg']

        # This is a really lousy way of doing things, but there doesn't seem
        # to be an easy way of importing the default background of some widget
        # to appear as the background of the webview. The two options are
        # to hardcode a background color or to somehow get some approximation
        # of the theme color here. So the lousy way to do the latter is to
        # look at the pixel color of a widget that we know will be visible on
        # the screen and to just use that
        pixel = Gdk.pixbuf_get_from_window(
            self['scrw_cg'].get_window(), 1, 1, 1, 1).get_pixels()

        web.set_background_color(Gdk.RGBA(pixel[0]/255, pixel[1]/255, pixel[2]/255, 1.0))
        web.set_visible(True)
        web.load_html(html, None)

        return False

    def cb_cg_context_menu(self,
                           web: WebKit2.WebView,
                           menu: WebKit2.ContextMenu,
                           evt: Gdk.Event,
                           hit_test_result: WebKit2.HitTestResult) -> bool:
        # For now, we don't have a context menu and just suppress it. At some
        # point, we might want to show something useful here
        return True

    def cb_cg_decide_policy(self,
                            web: WebKit2.WebView,
                            decision: WebKit2.PolicyDecision,
                            typ: WebKit2.PolicyDecisionType) -> bool:
        if typ in [WebKit2.PolicyDecisionType.NAVIGATION_ACTION,
                   WebKit2.PolicyDecisionType.NEW_WINDOW_ACTION]:
            return True
        return False

    def cb_zoom_format(self, scl, val) -> str:
        return '{}%'.format(int(val * 100))


    def cb_code_clicked(self, srcvw, evt: Gdk.EventButton) -> bool:
        x, y = srcvw.window_to_buffer_coords(Gtk.TextWindowType.TEXT, evt.x, evt.y)
        over_text, txtit = srcvw.get_iter_at_location(x, y)
        if over_text:
            for tag in txtit.get_tags():
                if isinstance(tag, LinkTag):
                    self.populate_details(tag.instruction)
        return False

    # Gtk.ComboBox => None
    def cb_region_changed(self, cmb_regions):
        # Gtk.TreeModel, Gtk.TreePath, Gtk.TreeIter, Callgraph => False
        def update_status(model, path, trit, cg):
            summary = model[trit][ModelColsSummary.Summary]
            for col, fn, icon in zip(
                    ['IconRead', 'IconWritten',
                     'IconAllocated', 'IconFreed'],
                    [Status.is_read, Status.is_written,
                     Status.is_allocated, Status.is_freed],
                    [self.icon_read, self.icon_written,
                     self.icon_allocated, self.icon_freed]):
                model.set_value(trit, getattr(ModelColsSummary, col),
                                icon if fn(summary.status[cg]) else '')

        self['trst_summary'].foreach(update_status, self.get_selected_callgraph())

        self.cb_details_filter()
        self.cb_summary_filter()

        return False

    # Gtk.Widget => bool
    def cb_open(self, widget):
        dlg = self.builder.get_object('dlgfile_main')
        response = dlg.run()
        dlg.hide()
        if response == Gtk.ResponseType.OK:
            self.load_analysis(dlg.get_filename())
        return False

    # Gtk.Widget => bool
    def cb_open_recent(self, rct):
        filename, _ = GLib.filename_from_uri(rct.get_current_uri())
        self.load_analysis(filename)
        return False

    # Gtk.Widget => False
    def cb_close(self, widget):
        self.clear()
        return False

    # Gtk.AboutDialog => bool
    def cb_help_about(self, widget):
        dlg = self.builder.get_object('dlgabt_main')
        response = dlg.run()
        dlg.hide()

        return False

    # Gtk.Widget => int
    def cb_quit(self, widget):
        return Gtk.main_quit()

    # Gtk.Widget => False
    def cb_fullscreen_toggle(self, widget):
        if self['mitm_view_fullscreen'].get_active():
            self['win_main'].fullscreen()
        else:
            self['win_main'].unfullscreen()
    # Load an analysis. This can be called from the GUI or the command-line
    # os.path => False
    def load_analysis(self, filename):
        self.analysis.load_from_file(filename)

        self.populate_files()
        self.populate_regions()
        self.populate_entities()
        self.populate_store()

        return False



# None => ArgumentParser
def parse_commandline_args():
    def percentage(arg):
        try:
            value = int(arg)
        except ValueError as err:
            raise argparse.ArgumentTypeError(str(err))

        if not (0 <= value <= 100):
            msg = 'Expected 0 <= value <= 100. , Got {}'.format(value)
            raise argparse.ArgumentTypeError(msg)

        return value


    ap = argparse.ArgumentParser("Visualizer for Moya's analysis output")
    ap.add_argument('file', default=None, nargs='?')

    # Initial window size group
    grp = ap.add_mutually_exclusive_group()
    grp.add_argument('--maximize', action='store_true',
                     help='Maximize the window on startup')
    grp.add_argument('--fullscreen', action='store_true',
                     help='Make the window fullscreen on startup')
    grp.add_argument('--width', type=percentage, default=75,
                     help='Initial width of window as a percentage of display width')
    grp.add_argument('--height', type=percentage, default=75,
                     help='Initial height of window as a percentage of display height')

    return ap.parse_args()


# None => int
def main():
    args = parse_commandline_args()

    ui = GtkUI()
    ui.builder.connect_signals(ui)
    win = ui['win_main']
    win.show_all()
    if args.file:
        # FIXME: Add validation of file here
        ui.load_analysis(args.file)

    if args.maximize:
        win.maximize()
    elif args.fullscreen:
        win.fullscreen()
    else:
        display = Gdk.Display.get_default()
        monitor = display.get_monitor_at_window(win.get_window())
        g = monitor.get_geometry()
        # Width and height requested will be a percentage
        win.resize(args.width / 100 * g.width, args.height / 100 * g.height)

    return Gtk.main()


if __name__ == '__main__':
    exit(main())
