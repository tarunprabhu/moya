#!/usr/bin/env python3

import re

from moya_commandline_base import CommandLineBase

# Base class for OpenMPI command lines
class CommandLineOpenMPI(CommandLineBase):
    # [string], [string], [string], { string : string } => CommandLineOpenMPI
    def __init__(self, args, exts, custom_flags=[], custom_regexes=[]):
        super().__init__(args, exts, custom_flags, custom_regexes)

    # CommandLineBase => { string : void(*)() }
    def get_openmpi_flags(self) :
        return {
            '--showme' : self.act_information_only,
            '--showme:compile' : self.act_information_only,
            '--showme:link' : self.act_information_only,
            '--showme:command' : self.act_information_only,
            '--showme:incdirs' : self.act_information_only,
            '--showme:libdirs' : self.act_information_only,
            '--showme:libs' : self.act_information_only,
            '--showme:version' : self.act_information_only,
            '--showme:help' : self.act_information_only
    }

    # CommandLineBase => { re : void(*)() }
    def get_openmpi_regexes(self):
        return { }
