#!/usr/bin/env python3

from moya_commandline_base import CommandLineBase
from moya_commandline_flang import CommandLineFlang
from moya_commandline_mpich import CommandLineMPICH

import re

# This is a class for the mpifort-specific MPICH options
class CommandLineMPICHFort(CommandLineFlang, CommandLineMPICH):
    # [string], {string : void(*)()}, {re : void(*)()}
    def __init__(self, args, custom_flags={}, custom_regexes={}):
        specs_flags = {}
        specs_regexes = {
            re.compile('^-fc=.+$') : self.act_preprocessor_0
        }

        specs_flags.update(self.get_mpich_flags())
        specs_flags.update(custom_flags)

        specs_regexes.update(self.get_mpich_regexes())
        specs_regexes.update(custom_regexes)
        
        super().__init__(args, [], specs_flags, specs_regexes)
