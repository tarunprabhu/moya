#!/usr/bin/env python3

from moya_commandline_clang import CommandLineClang

from os import path
import re

class CommandLineFlang(CommandLineClang):
    # [string], [string], { string : void(*)() }, { string : void(*)() }
    def __init__(self, args, custom_exts=[], custom_flags={}, custom_regexes={}):
        specs_flags = {
            # Dialect options
            '-fall-intrinsics' : self.act_dialect_0,
            '-fbackslash' : self.act_dialect_0,
            '-fcray-pointer' : self.act_dialect_0,
            '-fd-lines-as-code' : self.act_dialect_0,
            '-fd-lines-as-comments' : self.act_dialect_0,
            '-fdefault-double-8' : self.act_dialect_0,
            '-fdefault-integer-8' : self.act_dialect_0,
            '-fdefault-real-8' : self.act_dialect_0,
            '-fdollar-ok' : self.act_dialect_0,
            '-ffixed-line-length-none' : self.act_dialect_0,
            '-fffree-form' : self.act_dialect_0,
            '-ffree-line-length-none' : self.act_dialect_0,
            '-fimplicit-none' : self.act_dialect_0,
            '-finteger-4-integer-8' : self.act_dialect_0,
            '-fmax-identifier-length' : self.act_dialect_0,
            '-fmodule-private' : self.act_dialect_0,
            '-ffixed-form' : self.act_dialect_0,
            '-fno-range-check' : self.act_dialect_0,
            '-freal-4-real-10' : self.act_dialect_0,
            '-freal-4-real-16' : self.act_dialect_0,
            '-freal-4-real-8' : self.act_dialect_0,
            '-freal-8-real-10' : self.act_dialect_0,
            '-freal-8-real-16' : self.act_dialect_0,
            '-freal-8-real-4' : self.act_dialect_0,

            # Linker options

            # Other general compiler options
            '-fintrinsic-module-path' : self.act_preprocessor_1,

            # Not sure why these are special-cased. Probably a hold-over from
            # the DragonEgg days but at some point, we should fix it
            '-g' : self.act_debug,
            '-g1' : self.act_debug,
            '-g2' : self.act_debug,
            '-g3' : self.act_debug,
            '-gdwarf-2' : self.act_debug,
            '-gdwarf-3' : self.act_debug,
            '-gdwarf-4' : self.act_debug,
            '-gdwarf-5' : self.act_debug,
        }

        specs_regexes = {
            # Dialect options
            re.compile('^-ffixed-line-length-.+$') : self.act_dialect_0,
            re.compile('^-ffree-line-length-.+$') : self.act_dialect_0,

            # Linker options

            # Other general compiler options
            re.compile('^-J.+$') : self.act_include_output,
        }

        specs_exts = ['FPP', 'F', 'FOR', 'FTN', 'F90', 'F95', 'F03', 'F08', \
                      'fpp', 'f', 'for', 'ftn', 'f90', 'f95', 'f03', 'f08']
        
        specs_flags.update(custom_flags)
        specs_regexes.update(custom_regexes)
        specs_exts.extend(custom_exts)
        
        super().__init__(args, specs_exts, specs_flags, specs_regexes)


    # void => void
    def act_debug(self):
        self.act_0(self.flags_compiler, '-g')


    # void => void
    def act_include_output(self):
        incl = self.peek()[2:]
        new_flag = '-J{}'.format(path.abspath(incl))
        self.act_0(self.flags_preprocessor, new_flag)
