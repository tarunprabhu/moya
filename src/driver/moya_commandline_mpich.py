#!/usr/bin/env python3

import re

from moya_commandline_base import CommandLineBase

# Base class for all MPICH commandlines
class CommandLineMPICH(CommandLineBase):
    # [string], [string], [string], { string : string } => CommandLineMPICH
    def __init__(self, args, exts, custom_flags=[], custom_regexes=[]):
        super().__init__(args, exts, custom_flags, custom_regexes)
    
    # CommandLineBase => { string : void(*)() }
    def get_mpich_flags(self) :
        return {
            '-show': self.act_information_only,
            '-help': self.act_information_only,
            '-compile_info': self.act_information_only,
            '-link_info': self.act_information_only,
            '-echo': self.act_compiler_0
        }

    # CommandLineBase => { re : void(*)() }
    def get_mpich_regexes(self):
        return {
            re.compile('^-config=.+$') : self.act_preprocessor_0,
            re.compile('^-profile=.+$') : self.act_preprocessor_0
        }
