#!/usr/bin/env python3

from moya_commandline_base import CommandLineBase
from moya_commandline_clang_cxx import CommandLineClangCXX
from moya_commandline_ampi import CommandLineAMPI

import re

# This is a class for the ampicxx-specific options
class CommandLineAMPICXX(CommandLineClangCXX, CommandLineAMPI):
    # [string], {string : void(*)()}, {re : void(*)()}
    def __init__(self, args, custom_flags={}, custom_regexes={}):
        specs_flags = {
            '-c++-option' : self.act_wrapper_1,
        }
        specs_regexes = {}

        specs_flags.update(self.get_ampi_flags())
        specs_flags.update(custom_flags)

        specs_regexes.update(self.get_ampi_regexes())
        specs_regexes.update(custom_regexes)
        
        super().__init__(args, [], specs_flags, specs_regexes)

