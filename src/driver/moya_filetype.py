#!/usr/bin/env python3

from enum import Enum, unique
from os import path
from moya_exec import call_capture
import moya_config as config

# This class encapsulate various file types
# string => string
def get_mime_type(filename):
    filecmd = config.get_file_cmd()
    cmdbase = "{} -b {}"
    cmd = cmdbase.format(filecmd, filename)
    mimetype, _, _ = call_capture(cmd, 'Getting MIME type')

    return mimetype.strip()


@unique
class FT(Enum):
    ELF = 1
    AR = 2
    SO = 3
    BC = 4
    TXT = 5
    UNKNOWN = 100

    
    # os.path, FT.Enum => bool
    @staticmethod
    def is_file_of_type(f, ft):
        return FT.get_file_type(f) == ft

    # os.path => bool
    @staticmethod
    def is_text(f):
        return FT.is_file_of_type(f, FT.TXT)
    
    # os.path => bool
    @staticmethod
    def is_elf(f):
        return FT.is_file_of_type(f, FT.ELF)
    
    # os.path => bool
    @staticmethod
    def is_archive(f):
        return FT.is_file_of_type(f, FT.AR)

    # string => bool
    @staticmethod
    def is_shared_object(f):
        return FT.is_file_of_type(f, FT.SO)

    # string => bool
    @staticmethod
    def is_bitcode(f):
        return FT.is_file_of_type(f, FT.BC)

    # os.path => FileType
    @staticmethod
    def get_file_type(f):
        if path.islink(f):
            return FT.get_file_type(path.realpath(f))

        s2ft = { 'LSB relocatable' : FT.ELF,
                 'LSB executable' : FT.SO,
                 'LSB pie executable' : FT.SO,
                 'LSB shared object' : FT.SO,
                 'LSB pie shared object' : FT.SO,
                 'ar archive' : FT.AR,
                 'LLVM IR bitcode' : FT.BC,
                 'ASCII' : FT.TXT,
                 'UTF-8 Unicode' : FT.TXT}

        mime = get_mime_type(f)
        # This sucks, but we are doing partial matches, so we have to loop
        # over all the possibilities
        for signature, ty in s2ft.items():
            if signature in mime:
                return ty
        return FT.UNKNOWN
