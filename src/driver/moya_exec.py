#!/usr/bin/env python3

from moya_error import EC
from os import chdir, getcwd
from subprocess import Popen, PIPE
from sys import stdout, stderr

# These functions here are used to execute sub-commands

# os.path => os.path
def enter_working_dir(working_dir):
    if working_dir is None:
        return None
    curdir = getcwd()
    chdir(working_dir)
    return curdir

# os.path => void
def exit_working_dir(return_dir):
    if return_dir is not None:
        chdir(return_dir)


# Returns the integer return code from the shell. If working_dir is provided,
# then the command is executed in that directory
# string => int
def call(cmd, descr='', working_dir=None, verbose=False):
    if verbose:
        print('--------', descr, '--------')
        print(cmd)
        print('----------------------------------------')
        
    return_dir = enter_working_dir(working_dir)
    try:
        proc = Popen(cmd, shell=True)
        _, _ = proc.communicate()
        ret = proc.returncode
        if ret != 0:
            raise Exception
    except:
        exit_working_dir(return_dir)
        EC.error(descr, cmd)
        return EC.FAILURE

    exit_working_dir(return_dir)
    return ret


# string, string, int => string
def call_capture(cmd, descr='', verbose=False):
    if verbose:
        print('--------', descr, '--------')
        print(cmd)
        print('----------------------------------------')
        
    proc = Popen(cmd, shell=True, stdout=PIPE, stderr=None)
    o, _ = proc.communicate()
    ret = proc.returncode

    out = None
    if o is not None:
        out = o.decode('utf-8')

    return out, _, ret
