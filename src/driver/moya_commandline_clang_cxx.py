#!/usr/bin/env python3

from moya_commandline_base import CommandLineBase
from moya_commandline_clang import CommandLineClang

from os import path
import re

# This is a class for the C++-specific clang. 
class CommandLineClangCXX(CommandLineClang):
    # [string], [string], {string : void(*)()}, {string : void(*)()}
    def __init__(self, args, custom_exts=[], custom_flags={}, custom_regexes={}):
        specs_flags = {
            # These are dialect options that are from C.
            # I assume that they also apply to C++
            '-ansi' : self.act_dialect_0,
            '-fgnu89-inline' : self.act_dialect_0,
            '-aux-info' : self.act_dialect_1,
            '-fallow-parameterless-variadic-functions' : self.act_dialect_0,
            '-fno-asm' : self.act_dialect_0,
            '-fno-builtin' : self.act_dialect_0,
            '-fhosted' : self.act_dialect_0,
            '-ffreestanding' : self.act_dialect_0,
            '-fms-extensions' : self.act_dialect_0,
            '-fplan9-extensions' : self.act_dialect_0,
            '-fallow-single-precision' : self.act_dialect_0,
            '-fcond-mismatch' : self.act_dialect_0,
            '-flax-vector-conversions' : self.act_dialect_0,
            '-fsigned-bitfields' : self.act_dialect_0,
            '-funsigned-bitfields' : self.act_dialect_0,
            '-fsigned-char' : self.act_dialect_0,
            '-funsigned-char' : self.act_dialect_0,
            '-traditional' : self.act_dialect_0,
            '-traditional-cpp' : self.act_dialect_0,

            # C++-specific
            '-fno-access-control' : self.act_dialect_0,
            '-fcheck-new' : self.act_dialect_0,
            '-ffriend-injection' : self.act_dialect_0,
            '-fno-elide-constructors' : self.act_dialect_0,
            '-fno-enforce-eh-specs' : self.act_dialect_0,
            '-ffor-scope' : self.act_dialect_0,
            '-fno-for-scope' : self.act_dialect_0,
            '-fno-gnu-keywords' : self.act_dialect_0,
            '-fno-implicit-templates' : self.act_dialect_0,
            '-fno-implicit-inline-templates' : self.act_dialect_0,
            '-fno-implement-inlines' : self.act_dialect_0,
            '-fno-ansi-builtins' : self.act_dialect_0,
            '-fno-throw-opt' : self.act_dialect_0,
            '-fno-operator-names' : self.act_dialect_0,
            '-fno-optional-diags' : self.act_dialect_0,
            '-fpermissive' : self.act_dialect_0,
            '-fno-pretty-templates' : self.act_dialect_0,
            '-frepo' : self.act_dialect_0,
            '-fno-rtti' : self.act_dialect_0,
            '-fsized-deallocation' : self.act_dialect_0,
            '-fno-threadsafe-static' : self.act_dialect_0,
            '-fuse-cxa-atexit' : self.act_dialect_0,
            '-fno-weak' : self.act_dialect_0,
            '-nostdinc++' : self.act_dialect_0,
            '-fvisiblity-inlines-hidden' : self.act_dialect_0,
            '-fvisibility-ms-compat' : self.act_dialect_0,
            '-fext-numeric-literals' : self.act_dialect_0,
        }

        specs_regexes = {
            re.compile('^-fno-builtin-.+$') : self.act_dialect_0,
            re.compile('^-fsso-struct=.+$') : self.act_dialect_0,
            re.compile('^-fabi-version=.+$') : self.act_dialect_0,
            re.compile('^-fconstexpr-depth=.+$') : self.act_dialect_0,
            re.compile('^-ftemplate-backtrace-limit=.+$') : self.act_dialect_0,
            re.compile('^-ftemplate-depth=.+$') : self.act_dialect_0,
            re.compile('^-Wabi=.+$') : self.act_dialect_0,
        }

        specs_exts = ['cc', 'C', 'cpp', 'CPP', 'c++', 'cp', 'cxx', \
                      'h', 'hh', 'hpp', 'H', \
                      'tcc', \
                      'ii']
        
        specs_flags.update(custom_flags)
        specs_regexes.update(custom_regexes)
        specs_exts.extend(custom_exts)
        
        super().__init__(args, specs_exts, specs_flags, specs_regexes)
            
