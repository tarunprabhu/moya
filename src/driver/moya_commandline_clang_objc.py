#!/usr/bin/env python3

from moya_commandline_base import CommandLineBase
from moya_commandline_clang import CommandLineClang

import re

# This is a class for the Objective C-specific clang. 
class CommandLineClangObjC(ClangCommandLine):
    # [string], {string : void(*)()}, {string : void(*)()}
    def __init__(self, args, custom_flags={}, custom_regexes={}):
        specs_flags = {
            # These are dialect options that are from C.
            # I assume that they also apply to Objective-C
            '-ansi' : CommandLineBase.act_dialect_0,
            '-fgnu89-inline' : CommandLineBase.act_dialect_0,
            '-aux-info' : CommandLineBase.act_dialect_1,
            '-fallow-parameterless-variadic-functions' : CommandLineBase.act_dialect_0,
            '-fno-asm' : CommandLineBase.act_dialect_0,
            '-fno-builtin' : CommandLineBase.act_dialect_0,
            '-fhosted' : CommandLineBase.act_dialect_0,
            '-ffreestanding' : CommandLineBase.act_dialect_0,
            '-fms-extensions' : CommandLineBase.act_dialect_0,
            '-fplan9-extensions' : CommandLineBase.act_dialect_0,
            '-fallow-single-precision' : CommandLineBase.act_dialect_0,
            '-fcond-mismatch' : CommandLineBase.act_dialect_0,
            '-flax-vector-conversions' : CommandLineBase.act_dialect_0,
            '-fsigned-bitfields' : CommandLineBase.act_dialect_0,
            '-funsigned-bitfields' : CommandLineBase.act_dialect_0,
            '-fsigned-char' : CommandLineBase.act_dialect_0.
            '-funsigned-char' : CommandLineBase.act_dialect_0,
            '-traditional' : CommandLineBase.act_dialect_0,
            '-traditional-cpp' : CommandLineBase.act_dialect_0,
            
            # Objective-C specific flags
            '-fgnu-runtime' : CommandLineBase.act_dialect_0,
            '-fnext-runtime' : CommandLineBase.act_dialect_0,
            '-fno-nil-receivers' : CommandLineBase.act_dialect_0,
            '-fobjc-call-cxx-cdtors' : CommandLineBase.act_dialect_0,
            '-fobjc-direct-dispatch' : CommandLineBase.act_dialect_0,
            '-fobjc-exceptions' : CommandLineBase.act_dialect_0,
            '-fobjc-gc' : CommandLineBase.act_dialect_0,
            '-fobjc-nilcheck' : CommandLineBase.act_dialect_0,
            '-fobjc-std=objc1' : CommandLineBase.act_dialect_0,
            '-fno-local-ivars' : CommandLineBase.act_dialect_0,
            '-fivar-visibility=public' : CommandLineBase.act_dialect_0,
            '-fivar-visibility=protected' : CommandLineBase.act_dialect_0,
            '-fivar-visibility=private' : CommandLineBase.act_dialect_0,
            '-fivar-visibility=package' : CommandLineBase.act_dialect_0,
            '-freplace-objc-classes' : CommandLineBase.act_dialect_0,
            '-fzero-link' : CommandLineBase.act_dialect_0,
            '-gen-decls' : CommandLineBase.act_dialect_0,
            '-Wassign-intercept' : CommandLineBase.act_dialect_0,
            '-Wno-protocol' : CommandLineBase.act_dialect_0,
            '-Wselector' : CommandLineBase.act_dialect_0,
            '-Wstrict-selector-match' : CommandLineBase.act_dialect_0,
            '-Wundeclared-selector' : CommandLineBase.act_dialect_0,
            '-ObjC++' : CommandLineBase.act_dialect_0,
            '-objcmt-atomic-property' : CommandLineBase.act_dialect_0,
            '-objcmt-migrate-all' : CommandLineBase.act_dialect_0,
            '-objcmt-migrate-annotation' : CommandLineBase.act_dialect_0,
            '-objcmt-migrate-designated-init' : CommandLineBase.act_dialect_0,
            '-objcmt-migrate-instancetype' : CommandLineBase.act_dialect_0,
            '-objcmt-migrate-literals' : CommandLineBase.act_dialect_0,
            '-objcmt-migrate-ns-macros' : CommandLineBase.act_dialect_0,
            '-objcmt-migrate-property-dot-syntax' : CommandLineBase.act_dialect_0,
            '-objcmt-migrate-property' : CommandLineBase.act_dialect_0,
            '-objcmt-migrate-protocol-conformance' : CommandLineBase.act_dialect_0,
            '-objcmt-migrate-readonly-property' : CommandLineBase.act_dialect_0,
            '-objcmt-migrate-readwrite-property' : CommandLineBase.act_dialect_0,
            '-objcmt-migrate-subscripting' : CommandLineBase.act_dialect_0,
            '-objcmt-ns-nonatomic-iosonly' : CommandLineBase.act_dialect_0,
            '-objcmt-returns-innerpointer-property' : CommandLineBase.act_dialect_0,
            '-ObjC' : CommandLineBase.act_dialect_0,
            '-rewrite-legacy-objc' : CommandLineBase.act_dialect_0,
            '-rewrite-objc' : CommandLineBase.act_dialect_0,
            '-fno-objc-infer-related-result-type' : CommandLineBase.act_dialect_0,
            '-fobjc-arc-exceptions' : CommandLineBase.act_dialect_0,
            '-fobjc-arc' : CommandLineBase.act_dialect_0,
            '-fobjc-exceptions' : CommandLineBase.act_dialect_0,
            '-fobjc-gc-only' : CommandLineBase.act_dialect_0,
            '-fobjc-gc' : CommandLineBase.act_dialect_0,
        }

        specs_regexes = {
            re.compile('^-fconstant-string-class=.+') : CommandLineBase.act_dialect_0,
            re.compile('^-fobjc-abi-version=.+$') : CommandLineBase.act_dialect_0,
            re.compile('^-objcmt-whitelist-dir-path=.+$') : CommandLineBase.act_dialect_0,
            re.compile('^-fobjc-runtime=.+$'), CommandLineBase.act_dialect_0,
        }
        
        specs_flags.update(custom_flags)
        specs_regexes.update(custom_regexes)
        
        super().__init__(args, specs_flags, specs_regexes)
