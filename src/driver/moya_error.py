#!/usr/bin/env python3

from enum import IntEnum
from sys import stderr
from traceback import print_stack

class EC(IntEnum):
    SUCCESS = 0
    FAILURE = 1

    # string => void
    @staticmethod
    def error(msg, cmd=None):
        print("\n *** ERROR: {}\n".format(msg), file=stderr)
        if cmd is not None:
            print("\nFailed command: {}\n\n".format(cmd))
        print_stack(file=stderr)
        exit(EC.FAILURE)

    # * => void
    @staticmethod
    def error_if(cond, msg):
        if cond:
            EC.error(msg)

    # * => void
    @staticmethod
    def error_if_not(cond, msg):
        if not cond:
            EC.error(msg)

    # string => void
    @staticmethod
    def warning(msg):
        print(" *** WARNING: {}".format(msg), file=stderr)

