#!/usr/bin/env python3

from moya_commandline_base import CommandLineBase
from moya_commandline_clang_c import CommandLineClangC
from moya_commandline_mpich import CommandLineMPICH

import re

# This is a class for the mpicc-specific MPICH options
class CommandLineMPICHC(CommandLineClangC, CommandLineMPICH):
    # [string], {string : void(*)()}, {re : void(*)()}
    def __init__(self, args, custom_flags={}, custom_regexes={}):
        specs_flags = {}
        specs_regexes = {
            re.compile('^-cc=.+$') : self.act_preprocessor_0
        }

        specs_flags.update(self.get_mpich_flags())
        specs_flags.update(custom_flags)

        specs_regexes.update(self.get_mpich_regexes())
        specs_regexes.update(custom_regexes)
        
        super().__init__(args, [], specs_flags, specs_regexes)
