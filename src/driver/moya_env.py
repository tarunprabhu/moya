#!/usr/bin/env python3

# This class is used as a parser to handle Moya arguments that are passed
# through environment variables. We might want to control Moya's functioning
# but not want to fiddle with Makefiles. Most of the parameters that are
# handled as commandline parameters can also be set through environment
# variables

import moya_config as config
from moya_error import EC

import os

class Env:
    # CommandLineBase => EnvBase
    def __init__(self, cmd_base):
        self.cmd_base = cmd_base
        self.env_vars = {
            config.get_moya_env_off() : self.act_moya_off,
            config.get_moya_env_verbose() : self.act_moya_verbose,
            config.get_moya_env_save() : self.act_moya_save_dir,
            config.get_moya_env_working_dir() : self.act_moya_working_dir,
            config.get_moya_env_payload_dir() : self.act_moya_payload_dir
        }

        
    # void => void
    def parse(self):
        for var, handler in self.env_vars.items():
            value = os.environ.get(var)
            if value is not None:
                handler(value)
        # I am not sure if I really need to add the variables that were seen
        # back to the environment. The other Moya passes ought to be made
        # robust enough to not depend on the values being set here

        
    # string => void
    def act_moya_off(self, param):
        if self.str_to_bool(param):
            # Moya was turned off through the commandline.
            # Calling act_moya_off on the CommandLineBase object with the
            # parameter false to indicate that the call came from the
            # environment parser object
            self.cmd_base.act_moya_off(True)
        # If Moya was not turned off from the commandline (there really is
        # no point in doing this, but who knows...), then do nothing


    # string => void
    def act_moya_verbose(self, param):
        if self.str_to_bool(param):
            # Moya was set to verbose through environment variables.
            # Calling act_moya_verbose with a parameter indicates that the
            # verbose mode was enabled from an environment variable
            self.cmd_base.act_verbose(True)

            
    # string => void
    def act_moya_save_dir(self, param):
        if len(param) > 0:
            self.cmd_base.act_moya_save_dir(param)

            
    # string => void
    def act_moya_working_dir(self, param):
        self.cmd_base.act_moya_working_dir(param)


    # string => void
    def act_moya_payload_dir(self, param):
        self.cmd_base.act_moya_payload_dir(param)
        
            
    # string => bool
    def str_to_bool(self, s_in):
        # string => bool
        def is_int(s_in):
            try:
                int(s_in)
            except ValueError:
                return False
            return True

        s = s_in.lower()
        true = set(["true", "yes", 'oui', 'si', 'ja', 'da',
                    'aye', 'ya', 'yea', 'yeah', 'yup'])
        false = set(["false", "no", 'non', 'no', 'nein', 'nah', 'nope',
                     'niet', 'ne', 'nee', 'nay', 'nej', 'nei', 'neniu', 'naahi'])
        if(is_int(s)):
            return bool(int(s))
        elif s in true:
            return True
        elif s in false:
            return False
        else:
            EC.error("Invalid boolean string {}\n" \
                     "Expected one of:\n" \
                     "  true:  [{}]\n" \
                     "  false: [{}]\n" \
                     .format(s_in,
                             ' '.join(list(true)),
                             ' '.join(list(false))))
            return False
