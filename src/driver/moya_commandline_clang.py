#!/usr/bin/env python3

from moya_commandline_base import CommandLineBase
import moya_config as config
from os import path
import re

from sys import stdout, stderr

class CommandLineClang(CommandLineBase):
    # [string], [string], {string : void(*)()}, {re : void(*)()}
    def __init__(self, args, exts, custom_flags={}, custom_regexes={}):
        specs_flags = {
            # Diagnostic flags
            '-R' : self.act_compiler_1,
            '-W' : self.act_compiler_1,
            '-w' : self.act_compiler_0,
            '-fansi-escape-codes' : self.act_dialect_0,
            '-fblocks' : self.act_dialect_0,
            '-fborland-extensions' : self.act_dialect_0,
            '-fcxx-exceptions' : self.act_dialect_0,
            '-fdollars-in-identifiers' : self.act_dialect_0,
            '-fgnu-keywords' : self.act_dialect_0,
            '-fgnu-runtime' : self.act_dialect_0,
            '-fgnu89-inline' : self.act_dialect_0,
            '-fno-access-control' : self.act_dialect_0,
            '-fno-constant-cfstrings' : self.act_dialect_0,
            '-fno-dollars-in-identifiers' : self.act_dialect_0,
            '-fno-operator-names' : self.act_dialect_0,
            '-fno-trigraphs' : self.act_dialect_0,
            '-fno-use-cxa-atexit' : self.act_dialect_0,
            '-fno-use-init-array' : self.act_dialect_0,
            '-fpascal-strings' : self.act_dialect_0,
            '-fshort-enums' : self.act_dialect_0,
            '-fshort-wchar' : self.act_dialect_0,
            '-ftrigraphs' : self.act_dialect_0,
            '-fvisibility-inlines-hidden' : self.act_dialect_0,
            '-fvisibility-ms-compat' : self.act_dialect_0,
            '-fwritable-strings' : self.act_dialect_0,
            '-traditional-cpp' : self.act_dialect_0,
            '-trigraphs' : self.act_dialect_0,

            # Preprocessor options
            '-cxx-isystem' : self.act_preprocessor_1,
            '-fno-short-wchar' : self.act_preprocessor_0,
            '-fno-signed-char' : self.act_preprocessor_0,
            '-iframework' : self.act_preprocessor_1,
            '-index-header-map' : self.act_preprocessor_0,
            '-nobuiltininc' : self.act_preprocessor_0,
            '-undef' : self.act_preprocessor_0,
            '-dependency-dot' : self.act_preprocessor_1,
            '-dependence-file' : self.act_preprocessor_1,

            # Linker options

            # Other general compiler options
            '-Xclang' : self.act_compiler_1,
            '-working-directory' : self.act_compiler_1,
            '-gline-tables-only' : self.act_compiler_0,
            '-save-temps' : self.act_preprocessor_0,
            '-ivfsoverlay' : self.act_compiler_1,
            '-module-dependency-dir' : self.act_compiler_1,
            '-mthread-model' : self.act_compiler_1,
            '-verify-debug-info' : self.act_compiler_0,
            '-help' : self.act_information_only,
            '-print-search-dirs' : self.act_information_only,
            '-time' : self.act_compiler_0,
            '-emit-ast' : self.act_compiler_0,

            # Debug symbols are disabled for Fortran because they mess up
            # DragonEgg. But there doesn't seem to be any reason to disable
            # it for C/C++ as well
            '-g' : self.act_compiler_0,
            '-g1' : self.act_compiler_0,
            '-g2' : self.act_compiler_0,
            '-g3' : self.act_compiler_0,
            '-gdwarf-2' : self.act_compiler_0,
            '-gdwarf-3' : self.act_compiler_0,
            '-gdwarf-4' : self.act_compiler_0,
            '-gdwarf-5' : self.act_compiler_0,
        }

        specs_regexes = {
            # Diagnostics regexes
            re.compile('^-Rpass-analysis=.+$') : self.act_compiler_0,
            re.compile('^-Rpass-missed=.+$') : self.act_compiler_0,
            re.compile('^-Rpass=.+$') : self.act_compiler_0,

            # Dialect regexes

            # Preprocessor regexes
            re.compile('^-fcomment-block-commands=.*$') : self.act_dialect_0,
            re.compile('^-fvisibility=.*$') : self.act_dialect_0,
            re.compile('^--gcc-toolchain=.+') : self.act_preprocessor_0,
            re.compile('^--no-system-header-prefix=.*$') : self.act_preprocessor_0,
            re.compile('^--system-header-prefix=.*$') : self.act_preprocessor_0,
            re.compile('^-stdlib=.*$') : self.act_preprocessor_0,

            # Linker regexes

            # Other compiler regexes
            re.compile('^-fbuild-session-file=.+$') : self.act_compiler_0,
            re.compile('^-fbuild-session-timestamp=.+$') : self.act_compiler_0,
        }

        specs_flags.update(custom_flags)
        specs_regexes.update(custom_regexes)
        
        super().__init__(args, exts, specs_flags, specs_regexes)
