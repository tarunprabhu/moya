#!/usr/bin/env python3

from moya_impl_base import MoyaImplBase
from moya_exec import call, call_out
import moya_config as config
from moya_filetype import FT

from os import path
from shutil import copyfile as cp

# This is a variant of MoyaImplClang but which links the correct PTX versions
# of the IR so we can analyze everything correctly
class MoyaImplCuda(MoyaImplBase):
    # os.path, CommandLineBase, {string : string}, string, string, string
    def __init__(self, compiler, opts, plugin, incl, libs):
        super().__init__(compiler, opts, env_vars, plugin, incl, libs)
        

    # void => void
    def filter_inputs(self):
        # In addition to what is normally done, we need to extract the Cuda
        # code that is stashed inside as well
        MoyaImplBase.filter_inputs(self)

        cuda_section = config.get_moya_elf_cuda_section_name()
        for src in self.opts.get_input_files():
            if FT.is_elf(src):
                bc = path.join(self.tmpd,
                               support.replace_ext(src, 'bc').replace('/', '_'))
                self.stasher.extract_bitcode_from_file(src, bc, cuda_section)
                self.bcs_cuda.append(bc)


    # void => void
    def generate_obj_functions(self):
        analyzed = self.build_filename(config.get_tmp_analyzed('bc'))
        linked_cuda = self.build_filename(config.get_tmp_linked_cuda('bc'))

        self.generate_obj_functions_impl([analyzed, linked_cuda])
        
                
    # void => void
    def generate_obj_globals(self):
        conf = [(self.build_filename(config.get_tmp_analyzed('bc')),
                 self.build_filename(config.get_tmp_host_globals('bc')),
                 self.build_filename(config.get_tmp_host_globals('o')),
                 'host_globals'),
                
                (self.build_filename(config.get_tmp_linked_cuda('bc')),
                 self.build_filename(config.get_tmp_cuda_globals('bc')),
                 self.build_filename(config.get_tmp_cuda_globals('o')),
                 'cuda_globals')]

        self.generate_obj_globals_impl(conf)

                
    # os.path, bool => os.path
    def generate_bitcode(self, src, device):
        base, _ = support.get_base_ext(path.basename(src))
        dst = path.join(self.tmpd, base + '.bc')
        target = ' --cuda-host-only '
        if device:
            dst = path.join(self.tmpd, base + '.device.bc')
            target = ' --cuda-device-only '
        
        cmdbase = ('{} {} ' 
                   ' {} {} {} {} {} -I{} --cuda-path={} -c -emit-llvm '
                   ' -Xclang -load '
                   ' -Xclang {} -Xclang -add-plugin -Xclang -moya '
                   ' -Xclang -load -Xclang {} '
                   ' -o {} '
                   ' {}')
        cmd = cmdbase.format(self.clang,
                             target,
                             self.opts.get_assembler_flags(),
                             self.opts.get_compiler_flags(self.moya_incl),
                             self.opts.get_dialect_flags(),
                             self.opts.get_preprocessor_flags(),
                             self.opts.get_makefile_flags(),
                             self.opts.get_invoke_dir(),
                             config.get_cuda_dir(),
                             config.get_moya_plugin_clang(),
                             config.get_moya_lib_instrument_clang_cuda(),
                             dst,
                             self.get_srcs())
        call(cmd, 'Generating bitcode (Cuda)', self.tmpd)
        return dst
    
        
    # void => void
    def generate_bitcode_and_stash(self):
        cuda_section = config.get_moya_elf_cuda_section_name() 

        # Stash all the generated bitcode files into their corresponding
        # object files
        if len(self.srcs) == 1:
            src = self.srcs[0]
            obj = self.opts.get_output_file()
            if obj is None:
                obj = path.join(self.opts.get_invoke_dir(),
                                path.basename(support.replace_ext(src, 'o')))
            bc_host = self.generate_bitcode(src, False)
            self.maybe_save(bc_host)
            self.stasher.insert_bitcode_into_elf(obj, bc_host)

            bc_cuda = self.generate_bitcode(src, True)
            self.maybe_save(bc_cuda)
            self.stasher.insert_bitcode_into_elf(obj, bc_cuda, cuda_section)
        else:
            for src in self.srcs:
                obj = support.replace_ext(src, 'o')
                
                bc_host = self.generate_bitcode(src, False)
                self.maybe_save(bc_host)
                self.stasher.insert_bitcode_into_elf(obj, bc_host)

                bc_cuda = self.generate_bitcode(src, True)
                self.maybe_save(bc_cuda)
                self.stasher.insert_bitcode_into_elf(obj, bc_cuda, cuda_section)
                
        
    # void => void
    def cleanup_bitcode(self):
        inp = self.build_filename(config.get_tmp_linked('bc'))
        outp = self.build_filename(config.get_tmp_cleaned('bc'))
        
        opt = config.get_llvm_opt()
        aeryn = config.get_moya_lib_aeryn()
        cmdbase = ('{} -load {} '
                   ' -mem2reg '
                   ' -disable-inlining '
                   ' -adce '
                   ' -o {} {}')
        cmd = cmdbase.format(opt, aeryn, outp, inp)
        call(cmd, 'Cleaning up bitcode (Cuda)')


    # os.path, bool => void
    def link_bitcode(self, ignore_shared=False):
        linked = self.build_filename(config.get_tmp_linked('bc'))
        linked_cuda = self.build_filename(config.get_tmp_linked_cuda('bc'))

        llvm_link = config.get_llvm_link()
        
        link_flags = self.determine_link_flags(ignore_shared)
        compile_flags = self.opts.get_compiler_flags(self.moya_incl)

        bcs_host = self.bcs_host
        bcs_cuda = self.bcs_cuda
        if len(self.srcs) > 0:
            for src in self.srcs:
                bc_host = self.generate_bitcode(src, False)
                bc_cuda = self.generate_bitcode(src, True)
                bcs_host.append(bc_host)
                bcs_cuda.append(bc_cuda)

        # We link all the Cuda bitcode separately because we need this to
        # generate the globals and code to be JIT'ted. We don't need other
        # libraries and things for this, so we just use the simple LLVM linker
        cmdbase = ('{} -o {} {}')
        cmd = cmdbase.format(llvm_link, linked_cuda, ' '.join(bcs_cuda))
        call(cmd, 'Linking bitcode files (Cuda)')
        self.maybe_save(linked_cuda)

        # Prepare the bitcode for linking into the file that will actually
        # be analyzed. We drop the host-side kernel launchers and instead
        # link in the device code because that is what will be executed.
        # We also drop the target-specific metadata to prevent LLVM from
        # complaining about metadata not being valid on the host
        for bc in bcs_host + bcs_cuda:
            cmdbase = ('{} -load {} '
                       ' -moya-drop-cuda-kernel-launchers '
                       ' -moya-drop-cuda-kernel-attributes '
                       ' -o {} '
                       ' {} ')
            cmd = cmdbase.format(config.get_llvm_opt(),
                                 config.get_moya_lib_aeryn(),
                                 bc, bc)
            call(cmd, 'Dropping Cuda kernel launchers')

        # When linking all the bitcode into the single file to be executed,
        # we are essentially forcing the bitcode from the Cuda module into
        # the host module. We suppress warnings about this
        cmdbase = ('{} -B{} '
                   ' {} {} {} {} '
                   ' -Wno-override-module '
                   ' -flto -O0 -fuse-ld=gold -Wl,-plugin-opt=emit-llvm ' 
                   ' {} -o {} -L{} {}')
        cmd = cmdbase.format(self.clang,
                             path.dirname(config.get_gold_linker()),
                             self.opts.get_assembler_flags(),
                             self.opts.get_dialect_flags(),
                             self.opts.get_preprocessor_flags(),
                             self.opts.get_compiler_flags(self.moya_incl),
                             ' '.join(bcs_host + bcs_cuda),
                             linked, self.tmpd, link_flags)
        call(cmd, 'Linking bitcode files (Cuda + Host)')
        self.maybe_save(linked)
