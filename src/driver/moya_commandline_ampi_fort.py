#!/usr/bin/env python3

from moya_commandline_base import CommandLineBase
from moya_commandline_gnu_fort import CommandLineGNUFort
from moya_commandline_ampi import CommandLineAMPI

import re

# This is a class for the mpifort-specific MPICH options
class CommandLineAMPIFort(CommandLineGNUFort, CommandLineAMPI):
    # [string], {string : void(*)()}, {re : void(*)()}
    def __init__(self, args, custom_flags={}, custom_regexes={}):
        specs_flags = {
            '-f90-option' : self.act_wrapper_1,
            '-fmoddir' : self.act_wrapper_1,
            '-flibs' : self.act_wrapper_0,
        }
        specs_regexes = {}

        specs_flags.update(self.get_ampi_flags())
        specs_flags.update(custom_flags)

        specs_regexes.update(self.get_ampi_regexes())
        specs_regexes.update(custom_regexes)
        
        super().__init__(args, [], specs_flags, specs_regexes)
