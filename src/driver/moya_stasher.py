#!/usr/bin/env python3

# This contains the methods used to stash and extract bitcode files from
# ELF objects, static archives and the like

from moya_filetype import FT
from moya_exec import call, call_capture
from moya_error import EC
import moya_config as config

from math import floor
from os import path, system
from tempfile import NamedTemporaryFile, TemporaryDirectory
from shutil import copyfile as cp

# We don't really need this abstract class, but it's here just to ensure that
# if we ever support OSX we will need to create a stasher class for Darwin
# In that case, we just need to reimplement the insert, extract and remove
# methods for the various file types
class BitcodeStasher:
    # os.path, os.path => [os.path]
    def extract_bitcode_from_file(self, obj, dest, silent = False):
        if path.islink(obj):
            return self.extract_bitcode_from_file(path.realpath(obj), dest)

        fns = { FT.ELF : self.extract_bitcode_from_elf,
                FT.AR : self.extract_bitcode_from_ar,
                FT.SO : self.extract_bitcode_from_elf }
        ft = FT.get_file_type(obj)
        if ft not in fns:
            if silent:
                return []
            else:
                EC.error('Cannot extract bitcode from {} of type {}'\
                         .format(obj, str(ft)))
        return fns[ft](obj, dest)


    # os.path => void
    def remove_bitcode_from_file(self, obj):
        if path.islink(obj):
            return self.remove_bitcode_from_file(path.realpath(obj))
        ft = FT.get_file_type(obj)
        if ft not in set([FT.ELF, FT.AR, FT.SO]):
            EC.error('Unsupported file type to remove bitcode: '  + str(ft))

        objcopy = config.get_objcopy()
        cmdbase = '{} --remove-section {} {}'

        section = config.get_elf_bitcode_section_name()
        cmd = cmdbase.format(objcopy, section, obj)
        system(cmd)

        section = config.get_elf_bitcode_size_section_name()
        cmd = cmdbase.format(objecopy, section, obj)
        system(cmd)


    # os.path => bool
    def lookup_bitcode_in_file(self, obj):
        if path.islink(obj):
            return self.lookup_bitcode_in_file(path.realpath(obj))
        ft = FT.get_file_type(obj)
        if ft not in set([FT.ELF, FT.AR, FT.SO]):
            EC.error('Unsupported file type to lookup bitcode: ' + str(ft))

        grep = config.get_grep()
        nm = config.get_nm()
        section = config.get_elf_bitcode_section_name()
        cmdbase = '{} {} | {} -q "{}"'
        cmd = cmdbase.format(nm, obj, grep, section)
        return system(cmd) == 0


# Bitcode specialized class for Linux
class BitcodeStasherLinux(BitcodeStasher):
    # os.path, os.path => [os.path]
    def extract_bitcode_from_elf(self, obj, dest):
        objcopy = config.get_objcopy()
        section_bc = config.get_elf_bitcode_section_name()

        bitcode = []
        with TemporaryDirectory() as tmpd:
            file = path.join(tmpd, 'bc')

            cmdbase = '{} -O binary --only-section {} {} {}'
            cmd = cmdbase.format(objcopy, section_bc, obj, file)
            call(cmd, 'Extracting bitcode from ELF')

            with open(file, 'rb') as f:
                c = f.read(1)
                while c:
                    # There might be some null bytes before the name
                    while c == b'\x00':
                        c = f.read(1)

                    # What follows will be the name of the file
                    name = ''
                    while c != b':':
                        name = name + c.decode('utf-8')
                        c = f.read(1)

                    # Now the size which is a human-readable string
                    # c = ':' at this point
                    ssize = ''
                    c = f.read(1)
                    while c != b':':
                        ssize = ssize + c.decode('utf-8')
                        c = f.read(1)
                    size = int(ssize)

                    # And now the bitcode
                    bc = f.read(size)

                    out = path.join(dest, name)
                    with open(out, 'wb') as outf:
                        outf.write(bc)

                    bitcode.append(out)
                    c = f.read(1)

        return bitcode


    # os.path, os.path => [os.path]
    def extract_bitcode_from_ar(self, obj, dest):
        ar = config.get_ar()
        with TemporaryDirectory() as tmpd:
            objs, _, _ = call_capture('ar -t {}'.format(obj))

            cmdbase = '{} x {}'
            cmd = cmdbase.format(ar, obj)
            call(cmd, 'Extracting bitcode from ar', tmpd)

            bitcode = []
            for obj in objs.split():
                f = path.join(tmpd, obj)
                bitcode.extend(self.extract_bitcode_from_file(f, dest))

            if len(bitcode) > 0:
                # Wrap all the bitcode into an archive and return this archive.
                # We are most likely going to be using this to link all of the
                # bitcode and we want to make it look as like the original as
                # possible
                #
                out = path.join(dest, path.basename(obj))
                cmdbase = ('{} --plugin={} -rcs {} {}')
                cmd = cmdbase.format(ar,
                                     config.get_llvm_gold(),
                                     out,
                                     ' '.join(bitcode))
                call(cmd, 'Making archive of extracted bitcode')
                return [out]
            return []
