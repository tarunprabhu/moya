#!/usr/bin/env python3

from moya_commandline_base import CommandLineBase
from moya_commandline_clang import CommandLineClang

from os import path
import re

# This is a class for the C-specific clang. 
class CommandLineClangC(CommandLineClang):
    # [string], [string], {string : void(*)()}, {string : void(*)()}
    def __init__(self, args, custom_exts=[], custom_flags={}, custom_regexes={}):
        specs_flags = {
            '-ansi' : CommandLineBase.act_dialect_0,
            '-fgnu89-inline' : CommandLineBase.act_dialect_0,
            '-aux-info' : CommandLineBase.act_dialect_1,
            '-fallow-parameterless-variadic-functions' : CommandLineBase.act_dialect_0,
            '-fno-asm' : CommandLineBase.act_dialect_0,
            '-fno-builtin' : CommandLineBase.act_dialect_0,
            '-fhosted' : CommandLineBase.act_dialect_0,
            '-ffreestanding' : CommandLineBase.act_dialect_0,
            '-fms-extensions' : CommandLineBase.act_dialect_0,
            '-fplan9-extensions' : CommandLineBase.act_dialect_0,
            '-fallow-single-precision' : CommandLineBase.act_dialect_0,
            '-fcond-mismatch' : CommandLineBase.act_dialect_0,
            '-flax-vector-conversions' : CommandLineBase.act_dialect_0,
            '-fsigned-bitfields' : CommandLineBase.act_dialect_0,
            '-funsigned-bitfields' : CommandLineBase.act_dialect_0,
            '-fsigned-char' : CommandLineBase.act_dialect_0,
            '-funsigned-char' : CommandLineBase.act_dialect_0,
            '-traditional' : CommandLineBase.act_dialect_0,
            '-traditional-cpp' : CommandLineBase.act_dialect_0,
        }

        specs_regexes = {
            re.compile('^-fno-builtin-.+$') : CommandLineBase.act_dialect_0,
            re.compile('^-fsso-struct=.+$') : CommandLineBase.act_dialect_0,
        }

        specs_exts = ['c', 'h', 'i']
        
        specs_flags.update(custom_flags)
        specs_regexes.update(custom_regexes)
        specs_exts.extend(custom_exts)
        
        super().__init__(args, specs_exts, specs_flags, specs_regexes)
            
