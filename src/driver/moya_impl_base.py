#!/usr/bin/env python3

import moya_config as config
from moya_filetype import FT
from moya_error import EC
from moya_exec import call, call_capture

from collections import OrderedDict
from datetime import datetime
from os import path, linesep, mkdir, listdir, environ
from tempfile import TemporaryDirectory
from sys import stdout, stderr, byteorder as endian

import re
from shutil import copyfile as cp, move as mv

# This is a sequence of passes that does a fairly decent job of cleaning up
# the code. When we clean up the bitcode prior to stashing, we only remove
# the unused entries from the module but don't actually erase it. This
# sequence of passes erases everything but without performing additional
# optimizations which might break Moya's analysis/optimization passses
llvm_cleanup_passes = (' -instcombine -simplifycfg -adce '
                       ' -early-cse -jump-threading -correlated-propagation '
                       ' -simplifycfg -instcombine -simplifycfg -reassociate '
                       ' -scalar-evolution -adce -simplifycfg -instcombine '
                       ' -globaldce')


# os.path => str
def get_file_hash(filename):
    from hashlib import sha512
    sig = sha512()
    with open(filename, 'rb') as f:
        for block in iter(lambda: f.read(65536), b''):
            sig.update(block)
    return sig.hexdigest()

# os.path => str
def get_file_modtime(filename):
    return datetime.now().replace(microsecond=0).isoformat()

# This class is the base class that implements the full Moya functionality.
# Specific functions can be overridden in subclasses for language-specific
# tasks
class MoyaImplBase:
    # os.path, CommandLineBase, {string : string}, string, string, string
    def __init__(self, compiler, opts, env_vars, plugin, incl, libs):
        self.compiler = compiler
        self.opts = opts
        self.stasher = config.get_stasher()
        self.env_vars = env_vars
        self.plugin = plugin
        self.moya_incl = incl
        self.moya_libs = libs

        # Bitcode files extracted from input object files
        self.bcs_host = []
        self.bcs_cuda = []

        self.working_directory = None
        self.payload_directory = None

        # Construct a temporary directory
        self.working_dirname = self.opts.get_moya_working_dir()
        if self.opts.get_moya_working_dir() is None:
            self.working_directory = TemporaryDirectory()
            self.working_dirname = self.working_directory.name

        # Construct a temporary payload directory
        self.payload_dirname = self.opts.get_moya_payload_dir()
        if self.opts.get_moya_payload_dir() is None:
            self.payload_directory = TemporaryDirectory()
            self.payload_dirname = self.payload_directory.name


    # void => void
    def __del__(self):
        if self.working_directory is not None:
            self.working_directory.cleanup()
        if self.payload_directory is not None:
            self.payload_directory.cleanup()


    # void => string
    def get_input_files(self):
        return ' '.join(self.opts.get_input_files())


    # void => string
    def build_filename(self, suffix=''):
        base = path.basename(self.opts.get_output_file(use_default=True))
        return path.join(self.working_dirname, base+suffix)


    # os.path => void
    def maybe_save(self, bc):
        if self.opts.get_moya_save_dir():
            llvm_dis = config.get_llvm_dis()

            base, _, ext = path.basename(bc).rpartition('.')
            ll = path.join(self.opts.get_moya_save_dir(),
                           '{}.{}'.format(base, 'll'))

            cmdbase = '{} -o {} {}'
            cmd = cmdbase.format(llvm_dis, ll, bc)
            if self.opts.is_verbose_on():
                print('Saving {}'.format(ll))
            call(cmd, 'Disassembling bitcode while saving')


    # string => [ os.path ], [ os.path ]
    def parse_linker_trace(self, report):
        # re, string, [re.match] => bool
        def match(regex, s, ms):
            m = regex.match(s)
            if m is not None:
                ms[0] = m
                return True
            return False

        # Seems like the linker trace file format has changed at some point
        # Not sure which version of ld caused this. Need to add a check here
        # and figure it out correctly
        #
        # re_shared = re.compile('^\s*([-]l.+)\s+[(](.+)[)]\s*$')
        # re_static = re.compile('^\s*[\(](.+)[\)].+$\s*')
        re_shared = re.compile('^\s*(.+[.]so)\s*$')
        re_static = re.compile('^\s*(.+[.]a)\s*$')

        # Python does not have an OrderedSet, so we make do with an OrderedDict
        shared = OrderedDict()
        static = OrderedDict()
        for line in report.split(linesep):
            ms = [None]
            if match(re_shared, line, ms):
                shared[path.abspath(ms[0].group(1))] = None
                # shared[ms[0].group(1)] = ms[0].group(2)
            elif match(re_static, line, ms):
                static[path.abspath(ms[0].group(1))] = None
                # static[ms[0].group(1)] = None
        return list(shared.keys()), list(static.keys())


    # [os.path], [os.path] => void
    def link_bitcode(self, bc_exec, bc_libs):
        linked = self.build_filename(config.get_tmp_linked('bc'))
        verbose = self.opts.is_verbose_on()

        cmdbase = ('{} '
                   ' --plugin {} -plugin-opt=emit-llvm '
                   ' {} '
                   ' -o {} '
                   ' -L{} '
                   ' --start-group {} --end-group')
        cmd = cmdbase.format(config.get_gold_linker(),
                             config.get_llvm_gold(),
                             ' '.join(bc_exec),
                             linked,
                             self.working_dirname,
                             ' '.join(bc_libs))
        call(cmd, 'Linking bitcode files', verbose=verbose)
        self.maybe_save(linked)


    # string => [os.path]
    def get_linked_libs(self, linker_trace):
        shared, static = self.parse_linker_trace(linker_trace)

        # We know all the libraries that are used and we reconstruct the
        # command line replacing all the libraries included with -l
        #
        # FIXME: This will only work with relatively simple link lines.
        # This should work for most cases, I hope
        #
        libs = []

        for f in self.opts.get_input_files():
            if FT.is_shared_object(f):
                libs.append(path.abspath(f))

        for lib in shared:
            if lib not in libs:
                libs.append(lib)
        for lib in static:
            if lib not in libs:
                libs.append(lib)

        return libs


    # void => void
    def prepare_bitcode(self):
        linked = self.build_filename(config.get_tmp_linked('bc'))
        cleaned = self.build_filename(config.get_tmp_cleaned('bc'))
        prepared = self.build_filename(config.get_tmp_prepared('bc'))
        verbose = self.opts.is_verbose_on()

        # Run the cleanup passes
        opt = config.get_llvm_opt()
        aeryn = config.get_moya_lib_aeryn()

        cmdbase = ('{} -load {} '
                   ' -moya-fix-linkage '
                   ' -moya-remove-modeled '
                   ' -mem2reg '
                   ' -disable-inlining '
                   ' -adce '
                   ' -o {} {}')
        cmd = cmdbase.format(opt, aeryn, cleaned, linked)
        call(cmd, 'Cleaning up bitcode', verbose=verbose)
        self.maybe_save(cleaned)

        # Run preparation passes
        cmdbase = ('{} -load {} '
                   # First run the passes that modify the code
                   ' -moya-canonicalize-metadata '
                   ' -adce '
                   ' -moya-restore-function-attrs '
                   ' -moya-fix-linkage '
                   ' -moya-eliminate-global-alias '
                   # ' -moya-name-structs '
                   ' -moya-set-model-names '
                   ' -inline -mem2reg -adce '
                   # Anything below this should not change any code. Things
                   # should only be added to metadata or other attributes
                   ' -moya-annotate-new-wrappers '
                   # This pass should always run last after all the other
                   # passes because we don't want the ID's to change
                   ' -moya-ids '
                   ' -o {} {}')
        cmd = cmdbase.format(opt, aeryn, prepared, cleaned)
        call(cmd, 'Preparing for Aeryn', verbose=verbose)
        self.maybe_save(prepared)

    # os.path => void
    def run_aeryn(self, output_file):
        opt = config.get_llvm_opt()
        aeryn = config.get_moya_lib_aeryn()
        prepared = self.build_filename(config.get_tmp_prepared('bc'))
        analyzed = self.build_filename(config.get_tmp_analyzed('bc'))
        verbose = self.opts.is_verbose_on()

        cmdbase = ('{} -load {} '
                   ' -moya-analyzed-prefix={} '
                   ' -moya-analyzed-sig={} '
                   ' -moya-analyzed-time={} '
                   ' -moya-analyzed-path={} '
                   ' -moya-payload-directory={} '
                   ' -moya-restore-function-attrs '
                   ' -moya-prepare-mutability '
                   ' -moya-sanity-check '
                   ' -moya-mutability-analysis '
                   ' -moya-summarize '
                   ' -moya-callgraph '
                   ' -moya-class-heirarchy '
                   ' -moya-annotate-noalias '
                   ' -moya-generate-payload-components '
                   ' -o {} {}')
        cmd = cmdbase.format(opt, aeryn,
                             path.basename(output_file),
                             get_file_hash(output_file),
                             get_file_modtime(output_file),
                             output_file,
                             self.payload_dirname,
                             analyzed, prepared)
        call(cmd, 'Running Aeryn passes', verbose=verbose)


    # os.path, => void
    def generate_payload(self, output_file):
        opt = config.get_llvm_opt()
        aeryn = config.get_moya_lib_aeryn()
        injector = config.get_moya_exe_payload_injector()
        verbose = self.opts.is_verbose_on()

        # First strip all the bitcode files
        # We know that each bitcode file in the payload directory will
        # correspond to one function to be JIT'ed
        bcs = []
        pkls = []
        for f in listdir(self.payload_dirname):
            if f.endswith('.bc'):
                bcs.append(path.join(self.payload_dirname, f))
            elif f.endswith('.pkl'):
                pkls.append(path.join(self.payload_dirname, f))

        for bc in bcs:
            sbc = bc + '.sbc'
            cmdbase = ('{} -load {} '
                       ' -moya-eliminate-redirect '
                       ' -moya-fix-dead-branches '
                       ' -adce '
                       ' -moya-strip-payload-module '
                       ' -strip-dead-prototypes '
                       ' {} -o {}')
            cmd = cmdbase.format(opt, aeryn,
                                 bc, sbc)
            call(cmd, 'Strip module', verbose=verbose)
            mv(sbc, bc)
            self.maybe_save(bc)

        cmdbase = ('{} {} {}')
        cmd = cmdbase.format(injector, output_file, self.payload_dirname)
        call(cmd, 'Injecting payload', verbose=verbose)


    # string, string => void
    def compile(self, output_flags, linker_flags):
        verbose = self.opts.is_verbose_on()

        cmdbase = ('{} -target {} '
                   ' {} {} {} {} {} {} '
                   ' -I{} '
                   ' {} '
                   ' {} '
                   ' {} '
                   ' {} ')
        cmd = cmdbase.format(self.compiler,
                             config.get_moya_host_triple(),
                             self.opts.get_assembler_flags(),
                             self.opts.get_compiler_flags(self.moya_incl),
                             self.opts.get_dialect_flags(),
                             self.opts.get_preprocessor_flags(),
                             self.opts.get_makefile_flags(),
                             self.opts.get_optimization_level_flags(),
                             self.opts.get_invoke_dir(),
                             self.plugin,
                             output_flags,
                             self.get_input_files(),
                             linker_flags)
        call(cmd, 'Compiling', verbose=verbose)


    # string, string => void
    def link_exec(self, output_file, linker_flags):
        verbose = self.opts.is_verbose_on()

        # While compiling/linking, also trace the output because we will need
        # it to grab the bitcode files for the shared libraries that were
        # linked into the executable
        #
        cmdbase = ('{} -target {} '
                   ' {} {} {} {} {} {} '
                   ' -I{} '
                   ' {} '
                   ' -o {} '
                   ' {} '
                   ' -rdynamic -Wl,--export-dynamic -Wl,--trace {} ')
        cmd = cmdbase.format(self.compiler,
                             config.get_moya_host_triple(),
                             self.opts.get_assembler_flags(),
                             self.opts.get_compiler_flags(self.moya_incl),
                             self.opts.get_dialect_flags(),
                             self.opts.get_preprocessor_flags(),
                             self.opts.get_makefile_flags(),
                             self.opts.get_optimization_level_flags(),
                             self.opts.get_invoke_dir(),
                             self.plugin,
                             output_file,
                             self.get_input_files(),
                             linker_flags)
        trace, _, ret = call_capture(cmd, 'Linking', verbose=verbose)
        if ret != 0:
            EC.error("Error while linking", cmd)

        # Parse the linker trace to get all the libraries that are linked
        linked_libs = self.get_linked_libs(trace)

        # Get the bitcode from the object files and libraries that were linked
        # and link it all together
        bc_exec = self.stasher.extract_bitcode_from_file(output_file,
                                                         self.working_dirname)
        # for bc in bc_exec:
        #     self.maybe_save(bc)

        bc_libs = []
        for lib in linked_libs:
            bc_libs.extend(self.stasher.extract_bitcode_from_file(lib,
                                                                  self.working_dirname,
                                                                  silent = True))
        # for bc in bc_libs:
        #     self.maybe_save(bc)

        # Before doing anything else, get rid of the moya section from
        # the executable. Now that we have all the bitcode, we
        # don't need it anymore and there's no reason to have a bloated
        # executable
        cmdbase = ('{} --remove-section {} {}')
        cmd = cmdbase.format(config.get_objcopy(),
                             config.get_elf_bitcode_section_name(),
                             output_file)
        call(cmd, 'Stripping bitcode from executable', verbose=verbose)

        if (not bc_exec) and (not bc_libs):
            if verbose:
                print('No bitcode files found', file=stderr)
            return

        self.link_bitcode(bc_exec, bc_libs)

        # Run those passes that must be run before Aeryn is actually
        # These are bitcode cleanup passes, sanity checks to make sure that
        # the front-end hasn't screwed anything up and some analysis passes
        # that pre-compute things that Aeryn needs
        self.prepare_bitcode()

        # Run the actual Aeryn analysis pass
        self.run_aeryn(output_file)
        self.generate_payload(output_file)


    # void => int
    def run(self):
        verbose = self.opts.is_verbose_on()
        environ[config.get_moya_env_verbose()] = str(verbose)

        # Run the regular compiler with all flags except the Moya flags
        # in certain cases when either:
        #
        #     Moya is off
        # OR  There are no input files (in which case, the compiler is probably
        #     being queried - we don't support reading input files from
        #     streams)
        # OR  We are only generating Makefile rules
        #
        # In all of these cases, the compiler will have been turned off
        if not self.opts.get_input_files():
            cmdbase = '{} {}'
            cmd = cmdbase.format(self.compiler,
                                 self.opts.get_all_flags())
            return call(cmd, 'Running base compiler', verbose=False)
        elif self.opts.is_assembler_on():
            cmdbase = '{} {} {} {} {}'
            cmd = cmdbase.format(self.compiler,
                                 self.plugin,
                                 self.opts.get_all_flags(),
                                 self.moya_incl,
                                 ' '.join(self.opts.get_input_files()))
            return call(cmd, 'Running assembler', verbose=False)
        elif self.opts.is_compiler_off():
            cmdbase = '{} {} {} {}'
            cmd = cmdbase.format(self.compiler,
                                 self.opts.get_all_flags(),
                                 self.moya_incl,
                                 ' '.join(self.opts.get_input_files()))
            return call(cmd, 'Running base compiler', verbose=False)
        elif self.opts.is_moya_off():
            link_flags = ''
            if not self.opts.is_linker_off():
                link_flags = self.moya_libs
            cmdbase = '{} {} {} {}'
            cmd = cmdbase.format(self.compiler,
                                 self.opts.get_all_args(),
                                 self.moya_incl,
                                 link_flags)
            return call(cmd, 'Running base compiler', verbose=verbose)


        output_flags = ''
        output_file = self.opts.get_output_file(use_default=False)
        if output_file is not None:
            output_flags = '-o {}'.format(output_file)
        linker_flags = self.opts.get_linker_flags(self.moya_libs)

        if self.opts.is_linker_off():
            self.compile(output_flags, '')
        elif self.opts.is_linking_shared():
            self.compile(output_flags, linker_flags)
        else:
            if output_file is None:
                output_file = self.opts.get_output_file(use_default=True)
            self.link_exec(output_file, linker_flags)

        return EC.SUCCESS
