#!/usr/bin/env python3

import re

from moya_commandline_base import CommandLineBase

# This only contains functions which return the common arguments used by
# all the AMPI compilers

class CommandLineAMPI(CommandLineBase):
    # [string], [string], [string], { string : string } => CommandLineAMPI
    def __init__(self, args, exts, custom_flags=[], custom_regexes=[]):
        super().__init__(args, exts, custom_flags, custom_regexes)

    # void => void
    def act_tlsglobals(self):
        self.act_0(self.flags_wrapper, '-mno-tls-direct-seg-refs')
        self.act_0(self.flags_linker, '-static', False)
        self.act_0(self.flags_linker, '-Wl,--allow-multiple-definition', False)

    # CommandLineBase => { string : void(*)() }
    def get_ampi_flags(self) :
        # TODO: Implement Charm++ options
        return {
            # '-machine' : self.act_wrapper_1,
            # '-seq' : self.act_wrapper_0,
            # '-host' : self.act_wrapper_0,
            # '-language' : self.act_wrapper_1,
            # '-balancer' : self.act_wrapper_1,
            # '-tracemode' : self.act_wrapper_1,
            # '-memory' : self.act_wrapper_1,
            # '-modules' : self.act_wrapper_1,
            # '-thread' : self.act_wrapper_0,
            # '-debug-script' : self.act_wrapper_0,
            # '-count-tokens' : self.act_wrapper_0,
            # '-default-to-aout' : self.act_wrapper_0,
            # '-verbose' : self.act_wrapper_0,
            # '-save' : self.act_wrapper_0,
            # '-purify' : self.act_wrapper_0,
            # '-cp' : self.act_wrapper_1,
            # '-cpmod' : self.act_wrapper_0,
            # '-gen-cpm' : self.act_wrapper_1,
            # '-swapglobals' : self.act_wrapper_0,
            # '-copyglobals' : self.act_wrapper_0,
            '-tlsglobals' : self.act_tlsglobals,
            # '-roseomptlsglobals' : self.act_wrapper_0,
            # '-use-reliable-cc' : self.act_wrapper_0,
            # '-use-fastest-cc' : self.act_wrapper_0,
            # '-use-new-std' : self.act_wrapper_0,
            # '-cpp-option' : self.act_preprocessor_1,
            # '-ldro-option' : self.act_linker_1,
            # '-ld-option' : self.act_linker_1,
            # '-ld++-option' : self.act_linker_1,
            # '-rpath' : self.act_linker_1,
            # '-custom-part' : self.act_wrapper_0,
            # '-touch-on-failure' : self.act_wrapper_0,
            # '-mpi' : self.act_wrapper_0,
            # '-openmp' : self.act_wrapper_0,
            # '-no-trace-mpi' : self.act_wrapper_0,
            # '-build-shared' : self.act_linker_0,
            # '-charm-shared' : self.act_linker_0,
            # '-no-charmrun' : self.act_wrapper_0,
            # '-no-preprocess-ci' : self.act_wrapper_0,
            # '-nomain-module' : self.act_wrapper_0,
            # '-nomain' : self.act_wrapper_0,
            # '-nof90main' : self.act_wrapper_0,
            # '-f90main' : self.act_wrapper_0
        }

    # CommandLineBase => { re : void(*)() }
    def get_ampi_regexes(self):
        return {}
 
