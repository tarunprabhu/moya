#!/usr/bin/env python3

from os import path, getcwd, mkdir, listdir as ls, remove as rm
import re

from moya_env import Env
from moya_error import EC
from moya_filetype import FT

from sys import stderr
from tempfile import gettempdir

# For Fortran, the base compiler has to be GFortran. Since Clang is
# command-line compatible with GCC, we'll use the GNU options as the base
# We also include the moya-specific options here
class CommandLineBase:
    # [string], [string], { string : void => void }, { re : void => void }
    def __init__(self, args, exts, custom_flags={}, custom_regexes={}):
        # Current state of the argument parser
        self.argno = 0
        self.args = []

        # These are the acceptable file extensions for input source files
        self.exts = []

        self.args.extend(args)
        self.exts.extend(exts)

        # Phases
        # The assembler is only on when we have to generate an assembly file
        self.assembler_on = False
        self.preprocessor_on = True
        self.compiler_on = True
        self.linker_on = True
        self.makefile_on = False
        self.verbose_on = False

        # Moya-specific flags
        self.moya_on = True

        # Moya-specific directories
        self.moya_save_directory = None
        self.moya_working_directory = None
        self.moya_payload_directory = None

        # This is the directory from which the compiler was invoked
        self.invoke_directory = path.abspath(getcwd())

        # Files
        self.input_files = []
        self.output_file = None

        # Other flags that govern behavior
        self.out_shared = False

        # Optimization levels. We need this because we need to ensure that
        # the LLVM code that we generate does not get optimized in any way
        self.optimization_speed = '0'
        self.optimization_size = None
        
        # All the flags grouped into categories. These are in the order in
        # which they appear on the command line
        self.flags_assembler = []
        self.flags_preprocessor = []
        self.flags_dialect = []
        self.flags_compiler = []
        # These are flags that are consumed by "wrapper" compilers like AMPI
        # The MPI compilers also technically belong in the same category, but
        # we are explicitly not dealing with those options for now
        self.flags_wrapper = []
        self.flags_linker = []
        self.flags_makefile = []
        self.flags_information = []

        # All the flags. This does NOT contain the input and output file names
        # or any of the Moya-specific switches
        self.flags_all = []

        # All the arguments including the input and output file names
        # but excluding any Moya-specific switches
        self.args_all = []
        
        # Specifications for the GNU flags which are shared by the C and
        # Fortran compilers
        specs_flags = {
            # Moya-specific options
            '-moya-off' : self.act_moya_off,
            '-moya-save-temps' : self.act_moya_save_dir,
            
            # Preprocessor options
            '-C' : self.act_preprocessor_0,
            '-CC' : self.act_preprocessor_0,
            '-cpp' : self.act_preprocessor_0,
            '-nocpp' : self.act_preprocessor_0,
            '-dD' : self.act_information_only,
            '-dI' : self.act_information_only,
            '-dM'  : self.act_information_only,
            '-dN' : self.act_information_only,
            '-E' : self.act_preprocess_only,
            '-H' : self.act_preprocessor_0,
            '-idirafter' : self.act_preprocessor_1,
            '-include' : self.act_preprocessor_1,
            '-imacros' : self.act_preprocessor_1,
            '-iprefix' : self.act_preprocessor_1,
            '-iwthprefix' : self.act_preprocessor_1,
            '-iwithprefixbefore' : self.act_preprocessor_1,
            '-isystem' : self.act_preprocessor_1,
            '-imultilib' : self.act_preprocessor_1,
            '-isysroot' : self.act_preprocessor_1,
            '-nostdinc' : self.act_preprocessor_0,
            '-P' : self.act_preprocessor_0,
            '-fdebug-cpp' : self.act_preprocessor_0,
            '-ftrack-macro-expansion' : self.act_preprocessor_0,
            '-fworking-directory' : self.act_preprocessor_0,
            '-remap' : self.act_preprocessor_0,
            '-trigraphs' : self.act_preprocessor_0,
            '-undef' : self.act_preprocessor_0,
            '-Xpreprocessor' : self.act_preprocessor_1,
            '-no-integrated-cpp' : self.act_preprocessor_0,
            '-no-canonical-prefixes' : self.act_preprocessor_0,
            '--no-sysroot-suffix' : self.act_preprocessor_0,

            # Makefile generation options
            '-M' : self.act_makefile_only_0,
            '-MM' : self.act_makefile_0,
            '-MF' : self.act_makefile_out,
            '-MG' : self.act_makefile_0,
            '-MP' : self.act_makefile_0,
            '-MT' : self.act_makefile_1,
            '-MQ' : self.act_makefile_1,
            '-MD' : self.act_makefile_0,
            '-MMD' : self.act_makefile_0,

            # The dialect options should mostly come from the individual
            # language wrappers. This should be as language-neutral as possible
            '-x' : self.act_dialect_1,
            '-fopenmp' : self.act_dialect_0,
            '-fopenmp-simd' : self.act_dialect_0,
            '-fopenacc' : self.act_dialect_0,
            '-pthread' : self.act_dialect_0,

            # Assembler options
            '-S' : self.act_assemble_only,
            '-emit-llvm' : self.act_assembler_0,
            '-Xassembler' : self.act_assembler_1,

            # Linker options
            '-nostartfiles' : self.act_linker_0,
            '-nodefaultlibs' : self.act_linker_0,
            '-nostdlib' : self.act_linker_0,
            '-pie' : self.act_linker_0,
            '-rdynamic' : self.act_linker_0,
            '-s' : self.act_linker_0,
            '-static' : self.act_linker_0,
            '-static-libgcc' : self.act_linker_0,
            '-static-libstdc++' : self.act_linker_0,
            '-static-libasan' : self.act_linker_0,
            '-static-libtsan' : self.act_linker_0,
            '-static-liblsan' : self.act_linker_0,
            '-static-libubsan' : self.act_linker_0,
            '-static-libmpx' : self.act_linker_0,
            '-static-libmpxwrappers' : self.act_linker_0,
            '-shared' : self.act_link_shared,
            '-shared-libgcc' : self.act_linker_0,
            '-symbolic' : self.act_linker_0,
            '-T' : self.act_linker_1,
            '-Xlinker' : self.act_linker_1,
            '-u' : self.act_linker_1,
            '-z' : self.act_linker_1,

            # Other general options to pass to the compiler
            '-###' : self.act_information_only,
            '-c' : self.act_compile_only,
            '-o' : self.act_output_file,
            '-v' : self.act_verbose,
            '--target-help' : self.act_information_only,
            '--version' : self.act_information_only,
            '-pass-exit-codes' : self.act_information_only,
            '-pipe' : self.act_compiler_0,
            '-wrapper' : self.act_compiler_1,
            '-pg' : self.act_compiler_0,
            '-fPIC' : self.act_compiler_0,
            '-fPIE' : self.act_compiler_0,
            '-pedantic' : self.act_compiler_0,
            '-pedantic-errors' : self.act_compiler_0,
            '-mllvm' : self.act_compiler_1
        }

        specs_regexes = {
            # Moya-specific regexes
            re.compile('^-moya-working-directory(=.+)$') : self.act_moya_working_dir,
            re.compile('^-moya-payload-directory(=.+)$') : self.act_moya_payload_dir,
            
            # Preprocessor-specific regexes
            re.compile('^(-A)(.+)=(.+)$') : self.act_macro,
            re.compile('^(-D)(.+)(=.+)?$') : self.act_macro,
            re.compile('^(-U)(.+)$') : self.act_macro,
            re.compile('^-Wp,.+$') : self.act_preprocessor_0,
            re.compile('^-iquote.+') : self.act_preprocessor_0,
            re.compile('^-B.+$') : self.act_preprocessor_0,
            re.compile('^-I.+$') : self.act_include,
            re.compile('^--sysroot=.+$') : self.act_preprocessor_0,
            re.compile('^-std=.+$') : self.act_preprocessor_0,

            # Assembler-specific regexes
            re.compile('^-Wa,.+$') : self.act_assembler_0,

            # Linker-specific regexes
            re.compile('^-L(.*)$') : self.act_link_dir,
            re.compile('^-l(.*)$') : self.act_linker_0,
            re.compile('^-fuse-ld=.+') : self.act_linker_0,
            re.compile('^-Wl,.+$') : self.act_linker_0,

            # These are typically compiler optimization options
            re.compile('^-f.+$') : self.act_compiler_0,

            # These are typically target-specific code generation options
            re.compile('^-m.+$') : self.act_compiler_0,

            # These are typically warning options
            re.compile('^-W.+$') : self.act_compiler_0,

            # Other options passed to the compiler
            re.compile('^-O(.*)$') : self.act_optimization_level,
            re.compile('^--help.*$') : self.act_information_only,
            re.compile('^--?print-prog-name=.+$') : self.act_information_only,
            re.compile('^-specs=.+$') : self.act_compiler_0,
            re.compile('^-fplugin=.+$') : self.act_compiler_0,
            re.compile('^-fplugin-arg-.+$') : self.act_compiler_0,
        }

        self.specs_flags = {}
        self.specs_flags.update(specs_flags)
        self.specs_flags.update(custom_flags)

        self.specs_regexes = {}
        self.specs_regexes.update(specs_regexes)
        self.specs_regexes.update(custom_regexes)


    # void => int
    def parse(self):
        # First, parse the environment variables. If the Moya parameters are
        # passed on the commandline as well, the commandline parameters should
        # get preference and override any environment variables settings
        Env(self).parse()
        
        # Parse the command-line
        while self.argno < len(self.args):
            arg = self.peek()
            handler = None
            if arg in self.specs_flags:
                handler = self.specs_flags[arg]
            else:
                for pattern, action in self.specs_regexes.items():
                    m = pattern.match(arg)
                    if m is not None:
                        handler = action
                        break
            if handler is None:
                # If the argument does not begin with a '-', then it is
                # most likely an input file
                if not arg.startswith('-'):
                    handler = self.act_input_file
                else:
                    EC.error('Unsupported command line argument: '  \
                                      + self.peek())
                    return EC.FAILURE
            # Process the argument
            handler()
        return EC.SUCCESS

    
    # void => void
    def act_input_file(self):
        filename = path.abspath(self.peek())
        if FT.is_text(filename):
            _, _, ext = filename.rpartition('.')
            if ext not in self.exts:
                EC.error('Unsupported source file: ' + filename)

        if path.isfile(filename):
            self.input_files.append(filename)
            self.args_all.append(filename)
        else:
            # It looks as if when you pass a directory to gfortran, it treats
            # it as the output directory into which to place the .mod files
            flag = '-J{}'.format(filename)
            self.flags_compiler.append(flag)
            self.flags_all.append(flag)
            self.args_all.append(flag)
        self.shift()


    # void => void
    def act_output_file(self):
        self.output_file = path.abspath(self.lookahead(1))
        self.act_1()

    # bool => void
    # shift is not None when this function is being used to handle
    # Moya arguments passed through environment variables
    def act_moya_off(self, off=None):
        if off is None:
            self.moya_on = False
            self.shift()
        else:
            self.moya_on = not off

    # string, string => void
    def get_moya_dir(self, working_dir, dirname):
        default_working_dir = path.join(gettempdir(), dirname)
        
        # If an = sign is present in the current argument, then a specific
        # directory has been specified. If not, then use
        # ${tempdir}/moya.build.dir
        if working_dir is None:
            _, _, dir = self.peek().partition('=')
            if dir is not None:
                moya_dir = path.abspath(dir)
            else:
                moya_dir = default_working_dir
            self.shift()
        else:
            if len(working_dir) == 0:
                moya_dir = default_working_dir
            else:
                moya_dir = path.abspath(working_dir)

        # If using the working directory and it does not exist, create it.
        # It is an error to specify a working directory other than the default
        # that does not exist
        if moya_dir == default_working_dir:
            if not path.exists(moya_dir):
                mkdir(moya_dir)
        else:
            if not path.exists(moya_dir):
                EC.error('{}: {}'.format('Working directory does not exist',
                                         moya_dir))
                
        # The working directory should always be cleared on every invocation
        if moya_dir is not None:
            for f in ls(moya_dir):
                rm(path.join(moya_dir, f))

        return moya_dir
                
    # string => void
    # working_dir is not None when this function is being used to handle
    # Moya arguments passed through environment variables
    def act_moya_working_dir(self, working_dir=None):
        self.moya_working_directory = self.get_moya_dir(working_dir,
                                                        'moya.build')

    # string => void
    # payload_dir is not None when this function is being used to handle
    # Moya arguments passed through environment variables
    def act_moya_payload_dir(self, payload_dir=None):
        self.moya_payload_directory = self.get_moya_dir(payload_dir,
                                                        'moya.payload')
        
    # string => void
    # save_dir is not None when this function is being used to handle Moya
    # arguments passed through environment variables
    def act_moya_save_dir(self, save_dir=None):
        if save_dir is None:
            self.moya_save_directory = path.abspath(self.shift())
            self.shift()
        else:
            self.moya_save_directory = path.abspath(save_dir)

        # The save directory must exist. It will not be created
        if not path.exists(self.moya_save_directory):
            EC.error('{}: {}'.format('Save directory does not exist',
                                     self.moya_save_directory))

    # void => void
    def act_information_only(self):
        self.preprocessor_on = False
        self.compiler_on = False
        self.linker_on = False
        self.moya_on = False
        self.act_0(self.flags_information)
        
    # void => void
    def act_preprocess_only(self):
        self.compiler_on = False
        self.linker_on = False
        self.act_0(self.flags_preprocessor)

    # void => void
    def act_assemble_only(self):
        self.assembler_on = True
        self.compiler_on = False
        self.linker_on = False
        self.act_0(self.flags_assembler)

    # void => void
    def act_compile_only(self):
        self.linker_on = False
        self.act_0(self.flags_compiler)

    # void => void
    def act_makefile_only_0(self):
        self.makefile_on = True
        self.compiler_on = False
        self.linker_on = False
        self.act_0(self.flags_makefile)

    # bool => void
    # verbose is not None if the verbosity was set using an environment variable
    def act_verbose(self, verbose=None):
        if verbose is not None:
            self.verbose_on = verbose
        else:
            self.verbose_on = True
            self.act_0(self.flags_compiler)
        
    # void => void
    def act_link_exec(self):
        pass

    # void => void
    def act_link_shared(self):
        self.out_shared = True
        self.act_0(self.flags_linker)

    # void => void
    def act_optimization_level(self):
        level = self.peek()[2:]
        if level in ['0', '1', '2', '3', 'fast', '', 'g']:
            self.optimization_speed = level
        elif level == 's':
            self.optimization_size = 's'
        else:
            EC.error('Unsupported optimization level: ' + self.peek())
        self.flags_all.append(self.peek())
        self.args_all.append(self.peek())
        # Because we need to keep ensure that the optimization level does not
        # get applied to the LLVM code, we require that the optimization level
        # be handled explicitly. This should hopefully reduce errors at the
        # risk of slightly more developer effort. So we don't add it to the
        # compiler flags
        self.shift()

    # void => void
    def act_macro(self):
        flag = self.peek()
        left, equal, right = flag.partition('=')
        if equal != '' and right != '':
            self.act_0(self.flags_preprocessor,
                       '{}=\'{}\''.format(left, right))
        else:
            self.act_0(self.flags_preprocessor)
        
    # void => void
    def act_include(self):
        incl = self.peek()[2:]
        new_flag = '-I{}'.format(path.abspath(incl))
        self.act_0(self.flags_preprocessor, new_flag)

    # void => void
    def act_link_dir(self):
        d = self.peek()[2:]
        new_flag = '-L{}'.format(path.abspath(d))
        self.act_0(self.flags_linker, new_flag)
        
    # void => void
    def act_preprocessor_0(self):
        self.act_0(self.flags_preprocessor)

    # void => void
    def act_preprocessor_1(self):
        self.act_1(self.flags_preprocessor)

    # void => void
    def act_dialect_0(self):
        self.act_0(self.flags_dialect)

    # void => void
    def act_dialect_1(self):
        self.act_1(self.flags_dialect)

    # void => void
    def act_assembler_0(self):
        self.act_0(self.flags_assembler)

    # void => void
    def act_assembler_1(self):
        self.act_1(self.flags_assembler)

    # void => void
    def act_compiler_0(self):
        self.act_0(self.flags_compiler)

    # void => void
    def act_compiler_1(self):
        self.act_1(self.flags_compiler)

    # void => void
    def act_wrapper_0(self):
        self.act_0(self.flags_wrapper)

    # void => void
    def act_wrapper_1(self):
        self.act_1(self.flags_wrapper)
        
    # void => void
    def act_linker_0(self):
        self.act_0(self.flags_linker)

    # void => void
    def act_linker_1(self):
        self.act_1(self.flags_linker)

    # void => void
    def act_makefile_0(self):
        self.act_0(self.flags_makefile)

    # void => void
    def act_makefile_1(self):
        self.act_1(self.flags_makefile)

    # void => void
    def act_makefile_out(self):
        self.act_1(self.flags_makefile,
                   self.peek(), path.abspath(self.lookahead()))
        
    # [string] => void
    def act_0(self, category=None, flag=None, shift=True):
        if flag is None:
            flag = self.peek()
        if shift:
            self.shift()
        self.flags_all.append(flag)
        self.args_all.append(flag)
        if category is not None:
            category.append(flag)

    # string, string, [string] => void
    def act_1(self, category=None, flag=None, arg=None, shift=True):
        if flag is None:
            flag = self.peek()
        if shift:
            self.shift()
        if arg is None:
            arg = self.peek()
        if shift:
            self.shift()
        self.flags_all.append(flag)
        self.flags_all.append(arg)
        self.args_all.append(flag)
        self.args_all.append(arg)
        if category is not None:
            category.append(flag)
            category.append(arg)

    # void => string
    def shift(self):
        self.argno += 1
        if self.argno < len(self.args):
            return self.peek()
        return None

    # void => string
    def peek(self):
        return self.args[self.argno]

    # void => string
    def lookahead(self, n=1):
        return self.args[self.argno+n]
    
    # void => bool
    def is_moya_on(self):
        return self.moya_on

    # void => bool
    def is_moya_off(self):
        return not self.moya_on

    # void => bool
    def is_verbose_on(self):
        return self.verbose_on
    
    # void => bool
    def is_assembler_on(self):
        return self.assembler_on

    # void => bool
    def is_compiler_on(self):
        return self.compiler_on

    # void => bool
    def is_compiler_off(self):
        return not self.is_compiler_on()
    
    # void => bool
    def is_preprocessor_on(self):
        return self.preprocessor_on

    # void => bool
    def is_linker_on(self):
        return self.linker_on

    # void => bool
    def is_linker_off(self):
        return not self.is_linker_on()
    
    # void => bool
    def is_linking_shared(self):
        return self.out_shared

    # string => string
    def get_all_args(self):
        return ' '.join(self.args_all)
    
    # string => string
    def get_all_flags(self, extra=''):
        return ' '.join(self.flags_all + [extra])

    # string => string
    def get_assembler_flags(self, extra=''):
        return ' '.join(self.flags_assembler + [extra])

    # string => [string]
    def list_assembler_flags(self, extra=[]):
        return self.flags_assembler + extra
    
    # string => string
    def get_information_flags(self, extra=''):
        return ' '.join(self.flags_information + [extra])

    # [string] => [string]
    def list_information_flags(self, extra=[]):
        return self.flags_information + extra
    
    # string => string
    def get_preprocessor_flags(self, extra=''):
        return ' '.join(self.flags_preprocessor + [extra])

    # [string] => [string]
    def list_preprocessor_flags(self, extra=[]):
        return self.flags_preprocessor + extra
    
    # string => string
    def get_linker_flags(self, extra=''):
        return ' '.join(self.flags_linker + [extra])

    # [string] => [string]
    def list_linker_flags(self, extra=[]):
        return self.flags_linker + extra
    
    # string => string
    def get_dialect_flags(self, extra=''):
        return ' '.join(self.flags_dialect + [extra])

    # [string] => [string]
    def list_dialect_flags(self, extra=[]):
        return self.flags_dialect + extra

    # string => string
    def get_optimization_level_flags(self):
        flags = '-O{} '.format(self.optimization_speed)
        if self.optimization_size is not None:
            flags += '-O{}'.format(self.optimization_size)
        return flags
    
    # string => string
    def get_compiler_flags(self, extra=''):
        return ' '.join(self.flags_compiler + [extra])

    # [string] => [string]
    def list_compiler_flags(self, extra=[]):
        return self.flags_compiler + extra

    # string => string
    def get_wrapper_flags(self, extra=''):
        return ' '.join(self.flags_wrapper + [extra])

    # [string] => string
    def list_wrapper_flags(self, extra=[]):
        return self.flags_wrapper + extra
    
    # string => string
    def get_makefile_flags(self, extra=''):
        return ' '.join(self.flags_makefile + [extra])

    # string => string
    def list_makefile_flags(self, extra=[]):
        return self.flags_makefile + extra
    
    # string => [os.path]
    def get_input_files(self):
        return self.input_files

    # The rules for determining the output file are followed in this order
    # - If an output file name was explicitly specified using -o, return that 
    # - If default is True, use the default name which is a.out
    # - If we are not linking and a single input file is provided, the output
    #   is this input with the file extension replaced with .o unless an
    #   alternate extension has been provided
    # - Return None if none of these conditions apply
    # string => string
    def get_output_file(self, use_default=False):
        if self.output_file is not None:
            return path.abspath(self.output_file)
        if use_default:
            return path.abspath('a.out')
        return None

    # void => string
    def get_moya_working_dir(self):
        return self.moya_working_directory

    # void => string
    def get_moya_payload_dir(self):
        return self.moya_payload_directory
    
    # void => string
    def get_moya_save_dir(self):
        return self.moya_save_directory

    # void => string
    def get_invoke_dir(self):
        return self.invoke_directory

