#!/usr/bin/env python3

from moya_impl_base import MoyaImplBase

class MoyaImplCXX(MoyaImplBase):
    # os.path, CommandLineBase, {string : string}, string, string, string
    def __init__(self, compiler, opts, env_vars, plugin, incl, libs):
        super().__init__(compiler, opts, env_vars, plugin, incl, libs)
        
