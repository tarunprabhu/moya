#include <llvm/IR/Function.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/raw_ostream.h>

#include "common/Config.h"
#include "common/IRInput.h"
#include "common/Metadata.h"

using namespace llvm;
using namespace std;

int main(int argc, char* argv[]) {
  if(argc < 2) {
    outs() << "moya-print-functions-to-jit <file> ..."
           << "\n";
    return 1;
  }

  LLVMContext context;
  for(int i = 1; i < argc; i++) {
    unique_ptr<Module> module = readModuleFromFile(argv[i], context);

    for(Function& f : module->functions())
      if(moya::Metadata::hasSpecialize(f)
         and not moya::Metadata::hasCudaKernelHost(f))
        outs() << f.getName() << "\n";
  }

  return 0;
}
