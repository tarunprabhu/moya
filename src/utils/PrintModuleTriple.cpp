#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/raw_ostream.h>

#include "common/IRInput.h"

using namespace llvm;
using namespace std;

int main(int argc, char* argv[]) {
  if(argc != 2) {
    outs() << "moya-print-triple <file>"
           << "\n";
    return 1;
  }

  LLVMContext context;
  unique_ptr<Module> module(readModuleFromFile(argv[1], context));
  outs() << module->getTargetTriple() << "\n";

  return 0;
}
