#!/usr/bin/env python3

from abc import ABC as AbstractBase, abstractmethod
import argparse
from operator import or_ as _or_, eq as _eq_
import os
import re
import readline
import subprocess
from sys import stdout, stderr
from termcolor import colored

import analyzer as moya
    

# str, *, ** => Command.Status
def w_stdout(msg='', *args, **kwargs):
    print(msg, *args, **kwargs)
    return Command.Status.Success


# str, *, ** => Command.Status
def w_stderr(msg, *args, **kwargs):
    print(msg, *args, **kwargs, file=stderr)
    return Command.Status.Success


# str, *, ** => Command.Status
def w_error(msg, *args, **kwargs):
    end = '\n'
    if args:
        msg = msg + ':'
        end = ' '
    w_stdout(colored(msg, 'red', attrs=['bold']), end=end)
    if args:
        w_stdout(*args, **kwargs)
    return Command.Status.Success


# Exception => Command.Status
def w_exception(err):
    w_stderr('Caught unhandled exception:', type(err))
    w_stderr(str(err))
    raise err
    return Command.Status.Success


# [*], str, bool, bool, str, CLOSURE(*) => [*]
def find_matches(objs, ours, regex, mangled, sortcol, fn_filter):
    def get_source_name(o):
        return o.name

    def get_llvm_name(o):
        return o.llvm

    def get_location_keyfn(o):
        loc = o.location
        if loc:
            return (str(loc.file), loc.line, loc.column)
        return ('', 0, 0)

    # re, str => bool
    def re_match(regex, s):
        return regex.match(s) is not None

    matches = []
    fn_name = get_source_name
    if ours:
        fn_match = _eq_
        if regex:
            fn_match = re_match
            if ours.startswith('"') and ours.endswith('"'):
                ours = re.compile(ours[1:-1])
            elif ours.startswith("'") and ours.endswith("'"):
                ours = re.compile(ours[1:-1])
            else:
                w_error('Format error', 'Regex must be quoted')
                raise Command.Parser.Error
        if mangled:
            fn_name = get_llvm_name

        for o in objs:
            if fn_match(ours, fn_name(o)) and fn_filter(o):
                matches.append(o)
    else:
        for o in objs:
            if fn_filter(o):
                matches.append(o)
    if sortcol == 'name':
        matches.sort(key=get_source_name)
    elif sortcol == 'llvm':
        matches.sort(key=get_llvm_name)
    elif sortcol == 'loc':
        matches.sort(key=get_location_keyfn)

    return matches


# Not really sure if we need a full fledged class for this, but in case
# we ever implement a pager ourselves instead of just calling "less", this
# might help
class Pager:
    def __init__(self):
        pass

    # str => None
    def show(self, s):
        try:
            # -R: Only output ANSI color characters in "raw" form
            # -#: Number of characters to shift on left/right arrow
            pager = subprocess.run(['less',
                                    '-R',
                                    '-#', '1',
                                    '--quit-if-one-screen',
                                    '--no-init',
                                    '--quit-on-intr'],
                                   input=s.encode('ascii'),
                                   stdout=stdout)
        except KeyboardInterrupt:
            # Keyboard interrupts will be caught by less
            pass


# A table structure to format the output and not have to worry about
# getting all the indentation right
class Table:
    # int, str, str, str
    def __init__(self, ncols, header='', csep='   ', rsep=None, lmargin=0):
        if ncols < 1:
            raise RuntimeError('ncols must be >= 1. Got {}'.format(ncols))
        self.header = header
        self.ncols = ncols
        self.csep = csep
        self.rows = []
        self.lmargin = ' '*lmargin
        self.default_colors = [None]*self.ncols
        if self.ncols > 1:
            self.default_attrs = [['bold']] + [[]]*(self.ncols - 1)
        else:
            self.default_attrs = [[]]
        self.attrs = {}
        self.colors = {}

    # str => None
    def __str__(self):
        def get_color(r, c):
            if r in self.colors:
                return self.colors[r][c]
            return self.default_colors[c]

        def get_attrs(r, c):
            if r in self.attrs:
                return self.attrs[r][c]
            return self.default_attrs[c]

        def pad(s, c):
            if len(s) < widths[c]:
                return s + ' '*(widths[c] - len(s))
            return s
        
        widths = []
        for i in range(0, self.ncols):
            widths.append(max([len(r[i]) for r in self.rows], default=0))

        rout = []
        if self.header:
            rout.append(colored(self.header, 'green', attrs=['bold']))
            rout.append('')
        if self.ncols > 1:
            for r, row in enumerate(self.rows):
                cout = []
                cout.append(colored(self.lmargin + pad(row[0], 0),
                                    color=get_color(r, 0),
                                    attrs=get_attrs(r, 0)))
                for c, col in enumerate(row[1:-1]):
                    cout.append(colored(pad(col, c + 1),
                                        color=get_color(r, c + 1),
                                        attrs=get_attrs(r, c + 1)))
                cout.append(colored(row[-1],
                                    color=get_color(r, self.ncols-1),
                                    attrs=get_attrs(r, self.ncols-1)))
                rout.append(self.csep.join(cout))
        else:
            for r, row in enumerate(self.rows):
                rout.append(colored(self.lmargin + row[0],
                                    color=get_color(r, 0),
                                    attrs=get_attrs(r, 0)))
        return '\n'.join(rout)

    # len(colors) and len(attrs) can be less than self.ncols. In that case,
    # the formatting for the remaining columns will be unchanged. Leading
    # colors/attrs can be None in which case the defaults will be used
    #
    # str..., [str], [[str]] => None
    def add_row(self, *data, colors=[], attrs=[]):
        if len(data) == 0:
            self.rows.append(['']*self.ncols)
            return
        elif len(data) < self.ncols:
            raise RuntimeError('Too few data elements in table row.'
                               'Expected {}. Got {}'.format(self.ncols,
                                                            len(data)))
        nrows = len(self.rows)
        if colors:
            self.colors[nrows] = colors
            if len(colors) < self.ncols:
                self.colors[nrows].extend([None]*(self.ncols - len(colors)))

        if attrs:
            self.attrs[nrows] = attrs
            if len(attrs) < self.ncols:
                self.attrs[nrows].extend([None]*(self.ncols - len(attrs)))

        cols = [str(t).split('\n') for t in data]
        nlines = max([len(c) for c in cols], default=0)
        for i in range(0, nlines):
            row = []
            for c in cols:
                if i >= len(c):
                    row.append('')
                else:
                    row.append(c[i])
            self.rows.append(row)


# Abstract base class for all commands
class Command(AbstractBase):
    class Status(moya.Enum):
        Success = 0
        Failure = 1
        Exit = -1

    class Error(Exception):
        pass

    class Parser(argparse.ArgumentParser):
        # str, str, str
        def __init__(self, long, descr, short=''):
            argparse.ArgumentParser.__init__(self, prog=long, description=descr)

        # str => None
        def error(self, message):
            w_error('Error parsing command')
            w_stdout(self.format_help())
            raise Error
        
    # str, str, str
    def __init__(self, descr, long, short=''):
        self.long = long
        self.short = short
        self.descr = descr
        self.parser = Command.Parser(self.long, self.descr)
        self.init_parser()

    # None => None
    @abstractmethod
    def init_parser():
        pass

    # Context, argparse.Namespace => Command.Status
    @abstractmethod
    def run(self, context, args):
        pass


# Print help
class CommandHelp(Command):
    # None
    def __init__(self):
        Command.__init__(
            self,
            'Prints the available commands',
            'help', 'h')

    # None => None
    def init_parser(self):
        self.parser.add_argument(
            'command', default=None, nargs='?',
            help='Specify a command for detailed help for it')

    # Context, argparse.Namespace => Command.Status
    def run(self, context, argv):
        cmd = argv.command
        if cmd is not None:
            cobj = None
            for cls in Command.__subclasses__():
                obj = cls()
                if (obj.long == cmd) or (obj.short == cmd):
                    cobj = obj
                    break
            if cobj:
                w_stdout(cobj.parser.format_help())
            else:
                w_error('Unknown command', cmd)
        else:
            objs = [cls() for cls in Command.__subclasses__()]
            names = [colored(cobj.long, 'white', attrs=['bold'])
                     for cobj in objs]
            width = max([len(name) for name in names])
            fmt = '{{:{}}}  {{:>2}}  {{}}'.format(width)
            for name, cobj in zip(names, objs):
                w_stdout(fmt.format(name, cobj.short, cobj.descr))

        return Command.Status.Success


# Quit the program
class CommandQuit(Command):
    # None
    def __init__(self):
        Command.__init__(
            self,
            'Quit',
            'quit', 'q')

    # None = None
    def init_parser(self):
        pass

    # Context, argparse.Namespace => Command.Status
    def run(self, context, argv):
        return Command.Status.Exit


# Open an analysis file
class CommandOpen(Command):
    # None
    def __init__(self):
        Command.__init__(
            self,
            'Load an analysis file',
            'open', 'o')

    # None => None
    def init_parser(self):
        self.parser.add_argument(
            'file',
            help='The file with the analysis results to load')

    # Context, argparse.Namespace => Command.Status
    def run(self, context, argv):
        w_stdout('Opening analysis file ... ', end='', flush=True)
        exc = context.read(os.path.abspath(argv.file))
        if not exc:
            w_stdout(colored('Done', 'green', attrs=['bold']))
            return Command.Status.Success
        else:
            w_error('Failed', exc)
            return Command.Status.Failure


# Print information about the currently loaded analysis
class CommandInfo(Command):
    # None
    def __init__(self):
        Command.__init__(
            self,
            'Display information about the currently loaded analysis',
            'info', 'i')

    # None => None
    def init_parser(self):
        pass

    # Context, argparse.Namespace => Command.Status
    def run(self, context, argv):
        def fmt_label(s):
            return colored(s, 'white', attrs=['bold'])

        # The space allocated for the label is unusually large because colored
        # adds all the terminal color and attributes which get included in the
        # label length. When displayed, it takes up far fewer columns
        fmt = '{:30} {}'
        if context.initialized:
            module = context.module
            store = context.store

            tbl = Table(ncols=2)
            tbl.add_row('Name', context.name)
            tbl.add_row('Analysis', context.analysis)
            tbl.add_row('Analyzed', context.analyzed)
            tbl.add_row('Signature', context.sig)
            tbl.add_row('Time', context.time_s)
            tbl.add_row('Language',
                        ', '.join(
                            sorted(list(set(
                                [str(f.lang)
                                 for f in context.sources.values()]
                            )))))
            tbl.add_row('Regions', len(context.regions) - 1)
            tbl.add_row('JIT\'ted',
                        len([f
                            for key, f in module.functions.items()
                             if moya.Function.Flags.MoyaJIT in f.flags]))
            tbl.add_row('Functions', len(module.functions))
            tbl.add_row('Globals', len(module.globals))

            Pager().show(str(tbl))
        else:
            w_error('Analysis not loaded.',
                    'Use the "open" command to load an analysis')
        return Command.Status.Success


# Prints details about the specified function argument
class CommandShowArgument(Command):
    # None
    def __init__(self):
        Command.__init__(
            self,
            'Show details for the specified argument',
            'show-argument', 'sa')

    # None => None
    def init_parser(self):
        self.parser.add_argument(
            'function', type=str,
            help='The function whose argument is to be printed')
        self.parser.add_argument(
            'arg', type=str,
            help='The name of the argument to print')
        self.parser.add_argument(
            'index', type=int, default=0, nargs='?',
            help='The index of the function')
        group = self.parser.add_mutually_exclusive_group()
        group.add_argument(
            '-n', default=False, dest='argno', action='store_true',
            help=('Interpret the argument positional argument '
                  'as the argument number'))
        group.add_argument(
            '-i', default=False, dest='case', action='store_true',
            help='Case insensitive comparison of the function argument')

    # Context, argparse.Namespace => Command.Status
    def run(self, context, argv):
        raise RuntimeError('Unimplemented run() in CommandPrintArgument')
        return Command.Status.Success


# Prints details about the specified function
class CommandShowFunctions(Command):
    # None
    def __init__(self):
        Command.__init__(
            self,
            'Show the functions matching the specified criteria',
            'show-function', 'sf')

    # None => None
    def init_parser(self):
        self.parser.add_argument(
            'function', type=str, default=None, nargs='?',
            help='Filter to limit the functions listed')
        self.parser.add_argument(
            '-a', '--all', default=False, action='store_true',
            help='Include artificial functions when listing')
        self.parser.add_argument(
            '-m', '--mangled', default=False, action='store_true',
            help='Lookup the function name in the LLVM names')
        self.parser.add_argument(
            '-s', '--sort', default='name', choices=['name', 'loc', 'llvm'],
            help='Control how the functions are sorted')
        self.parser.add_argument(
            '-v', '--verbose', default=False, action='store_true',
            help='Verbose output')
        group = self.parser.add_mutually_exclusive_group()
        group.add_argument(
            '-r', '--regex', default=False, action='store_true',
            help='Interpret the function name as a regex')
        group.add_argument(
            '-i', '--index', type=int, default=0,
            help='The index of the function to display')

    # Function, Table => None
    def format_verbose(self, f, tbl):
        if f.qualified:
            tbl.add_row('Qualified', f.qualified)
        if f.full:
            tbl.add_row('Full', f.full)
        if f.location:
            tbl.add_row('Defined', f.location)
        tbl.add_row('Flags', f.flags)
        tbl.add_row('Address', f.addr)
        tbl.add_row()

        tbl.add_row('Arguments', len(f.args))
        tbl.add_row()
        atbl = Table(ncols=2)
        for argno, arg in enumerate(f.args):
            atbl.add_row('#', argno)
            if arg.name:
                atbl.add_row('Name', arg.name)
            atbl.add_row('Type', arg.type.pretty)
            atbl.add_row('Flags', arg.flags)
            if moya.Argument.Flags.HasLinked in arg.flags:
                atbl.add_row('Linked', arg.linked.argno)
            atbl.add_row('Address', arg.addr)
            atbl.add_row()
        tbl.add_row('', str(atbl))

        tbl.add_row('Callsites', '\n'.join([str(loc) for loc in f.callsites]))
        tbl.add_row()
        
        tbl.add_row('Callers',
                    '\n'.join(['{}({})'.format(caller.name, caller.idx)
                               for caller in f.callers]))
        tbl.add_row()        
        
    # Context, argparse.Namespace => Command.Status
    def run(self, context, argv):
        def filter(f):
            if not argv.all:
                if moya.Function.Flags.Artificial in f.flags:
                    return False
            return True

        funcs = [f for f in context.module.get_functions()]
        show = find_matches(
            funcs, argv.function, argv.regex, argv.mangled, argv.sort, filter)

        if argv.verbose:
            out = []
            for f in show:
                tbl = Table(header='{}({})'.format(f.name, f.idx), 
                        ncols=2,
                        csep='  ',
                        lmargin=2)
                self.format_verbose(f, tbl)
                out.append(str(tbl))
            Pager().show('\n\n'.join(out))
        else:
            tbl = Table(ncols=3, csep=' ')
            for f in show:
                tbl.add_row(f.name, f.idx, str(f.location))
            Pager().show(str(tbl))

        return Command.Status.Success


# Prints details about the specified global
class CommandShowGlobal(Command):
    # None
    def __init__(self):
        Command.__init__(
            self,
            'Show the global variables matching the specified criteria',
            'show-globals', 'sg')

    # None => None
    def init_parser(self):
        self.parser.add_argument(
            'globalvar', type=str, default=None, nargs='?',
            help='Filter to limit the global variables listed')
        self.parser.add_argument(
            '-a', '--all', default=False, action='store_true',
            help='Include artificial globals when listing')
        self.parser.add_argument(
            '-m', '--mangled', default=False, action='store_true',
            help='Lookup the global variable name in the LLVM names')
        self.parser.add_argument(
            '-s', '--sort', default='name', choices=['name', 'loc', 'llvm'],
            help='Control how the globals are sorted')
        self.parser.add_argument(
            '-v', '--verbose', default=False, action='store_true',
            help='Verbose output')
        group = self.parser.add_mutually_exclusive_group()
        group.add_argument(
            '-r', '--regex', default=False, action='store_true',
            help='Interpret the global variable name as a regex')
        group.add_argument(
            '-i', '--index', type=int, default=0,
            help='The index of the global variable to display')

    # Context, argparse.Namespace => Command.Status
    def run(self, context, argv):
        def filter(g):
            if not argv.all:
                if moya.Global.Flags.Artificial in g.flags:
                    return False
            return True

        globals = [g for g in context.module.globals.values()]
        show = find_matches(
            globals, argv.globalvar, argv.regex, argv.mangled, argv.sort, filter)

        if argv.verbose:
            out = []
            for g in show:
                tbl = Table(header='{}({})'.format(f_gb(g.name), g.idx),
                            ncols=2,
                            lmargin=2)
                if g.qualified:
                    tbl.add_row('Qualified', g.qualified)
                if g.full:
                    tbl.add_row('Full', g.full)
                if g.location:
                    if moya.Global.Flags.Artificial in g.flags:
                        tbl.add_row('Defined', g.location, lbl_color='grey')
                    else:
                        tbl.add_row('Defined', g.location)
                tbl.add_row('Flags', g.flags)
                tbl.add_row('Address', g.addr)
                tbl.add_row('Type', g.type.pretty)
                out.append(str(tbl))
                out.append('')
            Pager.show('\n'.join(out))
        else:
            tbl = Table(ncols=3, csep=' ')
            for g in show:
                tbl.add_row(g.name, g.idx, g.location)
            Pager().show(str(tbl))

        return Command.Status.Success


# Prints details about the specified type
class CommandShowType(Command):
    # None
    def __init__(self):
        Command.__init__(
            self,
            'List the struct types matching the given criteria',
            'show-types', 'st')

    # None => None
    def init_parser(self):
        self.parser.add_argument(
            'type', type=str, default=None, nargs='?',
            help='Filter to limit the types listed')
        self.parser.add_argument(
            '-a', '--all', default=False, action='store_true',
            help='Include artificial types when listing')
        self.parser.add_argument(
            '-k', '--kind',
            choices=['struct', 'class', 'union', 'all'],
            default='all',
            help='Limit the types to the specified kind')
        self.parser.add_argument(
            '-l', '--llvm', default=False, action='store_true',
            help='Lookup the type name in the LLVM names')
        self.parser.add_argument(
            '-s', '--sort', default='name', choices=['name', 'loc', 'llvm'],
            help='Control how the types are sorted')
        self.parser.add_argument(
            '-v', '--verbose', default=False, action='store_true',
            help='Verbose output')
        group = self.parser.add_mutually_exclusive_group()
        group.add_argument(
            '-r', '--regex', default=False, action='store_true',
            help='Interpret the type name as a regex')
        group.add_argument(
            '-i', '--index', type=int, default=0,
            help='The index of the global variable to display')

    # Context, argparse.Namespace => Command.Status
    def run(self, context, argv):
        def filter(sty):
            if not argv.all:
                if sty.artificial:
                    return False
            if argv.kind == 'struct':
                if sty.kind != moya.StructKind.Struct:
                    return False
            elif argv.kind == 'class':
                if sty.kind != mooya.StructKind.Class:
                    return False
            elif argv.kind == 'union':
                if sty.kind != moya.StructKind.Union:
                    return False
            return True

        structs = []
        for slist in context.module.snames.values():
            structs.extend(slist)
        show = find_matches(
            structs, argv.type, argv.regex, argv.llvm, argv.sort, filter)

        if argv.verbose:
            store = context.store
            out = []
            for sty in show:
                tbl = Table(header='{}({})'.format(f_gb(sty.name), sty.idx),
                            ncols=2,
                            lmargin=2)
                if sty.name:
                    tbl.add_row('Name', sty.name)
                    tbl.add_row('Length', len(sty.name))
                tbl.add_row('Size', sty.size)
                if sty.location:
                    if sty.artificial:
                        tbl.add_row('Defined', sty.location, color='grey')
                    else:
                        tbl.add_row('Defined', sty.location)
                tbl.add_row('Flags', 'Artificial' if sty.artificial else '')

                tbl.add_row('Fields', len(sty.fields))
                tbl.add_row()
                ftbl = Table(ncols=4)
                for i, (name, typ, off) in enumerate(sty.fields):
                    ftbl.add_row(i, name, off, typ.pretty)
                tbl.add_row('', str(ftbl))
                tbl.add_row()

                store = context.store
                heap = [str(a)
                        for a, t in store.allocated.items()
                        if (t is sty) and (moya.Store.Flags.Heap in store[a].flags)]
                tbl.add_row('Heap', '\n'.join(heap))
                tbl.add_row()

                stack = [str(a)
                         for a, t in store.allocated.items()
                         if (t is sty) and (moya.Store.Flags.Stack in store[a].flags)]
                tbl.add_row('Stack', '\n'.join(stack))
                tbl.add_row

                out.append(str(tbl))
                out.append('')
            Pager().show('\n'.join(out))
        else:
            tbl = Table(ncols=3)
            for t in show:
                tbl.add_row(t.name, t.idx, t.location)
            Pager().show(str(tbl))

        return Command.Status.Success


# Prints the store cell
class CommandShowCell(Command):
    # None
    def __init__(self):
        Command.__init__(
            self,
            'Show details for the specified store cell',
            'show-cell', 'sc')

    # None => None
    def init_parser(self):
        self.parser.add_argument(
            'address', type=int,
            help='The address of the cell to print')
        self.parser.add_argument(
            'offset', type=int, default=0, nargs='?',
            help='The offset from the address')
        self.parser.add_argument(
            '--uses', type=str,
            choices=['func', 'loc', 'both'],
            default='both',
            help=('Print the accessors of the cell as '
                    'functions, locations or both'))

    # Context, argparse.Namespace => Command.Status
    def run(self, context, argv):
        def get_location_keyfn(loc):
            return str(loc.file), loc.line, loc.column
        
        store = context.store
        addr = argv.address + argv.offset
        if addr not in store:
            w_error('Address not in store', addr)
            return Command.Status.Failure

        cell = store[addr]
        tbl = Table(ncols=2, lmargin=2)
        tbl.add_row('Address', addr)
        tbl.add_row('Flags', cell.flags)
        tbl.add_row('Type', cell.type.pretty)
        if isinstance(cell.type, moya.PointerType):
            ttbl = Table(ncols=1)
            for target in cell.targets:
                ttbl.add_row(target)
            tbl.add_row('Targets', ttbl)

        tbl.add_row()
        accessors = ['readers', 'writers']
        if moya.Store.Flags.Implicit not in cell.flags:
            accessors.extend(['allocators', 'deallocators'])
        for a in accessors:
            atbl = Table(ncols=1)
            if argv.uses in ['both', 'func']:
                fa = getattr(cell, 'f' + a)
                if fa:
                    for f in sorted(fa, key=lambda f: f.name):
                        atbl.add_row('{}({})'.format(f.name, f.idx))
                    atbl.add_row()
            if argv.uses in ['both', 'loc']:
                ia = getattr(cell, 'i' + a)
                if ia:
                    for loc in sorted(ia, key=get_location_keyfn):
                        atbl.add_row(loc)
            tbl.add_row(str.upper(a[0]) + a[1:], atbl)
            tbl.add_row()
        Pager().show(str(tbl))
            
        return Command.Status.Success


class CommandShowObject(Command):
    # None
    def __init__(self):
        Command.__init__(
            self,
            'Summarize an object',
            'show-object', 'so')

    # None => None
    def init_parser(self):
        self.parser.add_argument(
            'address', type=int,
            help='The starting address of the object')
        self.parser.add_argument(
            '-r', '--region', type=int, default=None,
            help='The region for which to summarize the object')
        group = self.parser.add_mutually_exclusive_group()
        group.add_argument(
            '-a', '--argument', default=False, action='store_true',
            help='Look for a function argument at the address')
        group.add_argument(
            '-g', '--global', dest='gbl', default=False, action='store_true',
            help='Look for a global variable at the address')
        group.add_argument(
            '-l', '--allocated', default=False, action='store_true',
            help='Look for an object allocated at the address')
        group.add_argument(
            '-s', '--struct', default=False, action='store_true',
            help='Look for a struct allocated at the address')

    # Context, argparse.Namespace => Command.Status
    def run(self, context, argv):
        addr = argv.address
        module = context.module
        store = context.store
        interactive = not (argv.argument
                           or argv.gbl
                           or argv.allocated
                           or argv.struct)
        types = []
        if argv.argument or interactive:
            types.extend([a.type
                          for a in module.get_arguments()
                          if a.addr == addr])
        if argv.gbl or interactive:
            types.extend([g.type
                          for g in module.get_globals()
                          if g.addr == addr])
        if argv.allocated or interactive:
            if addr in store.allocated:
                types.append(store.allocated[addr])
        if argv.struct or interactive:
            pass
        if interactive:
            if addr in store:
                types.append(store[addr].type)

        if not types:
            if argv.argument:
                w_error('No argument found at address', addr)
            elif argv.gbl:
                w_error('No global found at address', addr)
            elif argv.allocated:
                w_error('No type allocated at address', addr)
            elif interactive:
                w_error('Address not found in store', addr)
            return Command.Status.Failure

        index = 0
        if interactive:
            if len(types) > 1:
                while True:
                    fmt = '{{:{}}}: {{}}'.format(len(str(len(types))))
                    for i, typ in enumerate(types):
                        print(fmt.format(i + 1, typ))
                    inp = raw_input('Select type to show: ')
                    try:
                        index = int(inp) - 1
                        if index < len(types):
                            break
                    except ValueError:
                        # Keep iterating until a valid index is entered
                        pass

        raise RuntimeError('Not implemented. Show object: ' + str(types[index]))

    
# List the functions that are to be JIT'ted
class CommandListJITFunctions(Command):
    # None
    def __init__(self):
        Command.__init__(
            self,
            "List the functions that are JIT'ted",
            'list-jit-functions', 'lf')

    # None => None
    def init_parser(self):
        pass

    # Context, argparse.Namespace => Command.Status
    def run(self, context, argv):
        funcs = [f for f in context.module.get_functions()
                 if moya.Function.Flags.MoyaJIT in f.flags]
        width = max([len(f.name) for f in funcs], default=0)
        fmt = '{{:{}}} {{:2}}  {{}}'.format(width)
        for f in funcs:
            w_stdout(fmt.format(f.name, f.idx, f.location))


# List the JIT regions defined in the code
class CommandListJITRegions(Command):
    # None
    def __init__(self):
        Command.__init__(
            self,
            'List the JIT regions that were defined',
            'list-jit-regions', 'lr')

    # None => None
    def init_parser(self):
        pass

    # Context, argparse.Namespace => Command.Status
    def run(self, context, argv):
        width = max([len(r.name) for r in context.regions_a], default=0)
        fmt = '{{:{}}} {{:2}} [{{}}, {{}}]'.format(width)
        for r in context.regions_a:
            w_stdout(fmt.format(r.name, r.idx, r.begin, r.end))


# Prints the callgraph in DOT format
class CommandShowCallgraph(Command):
    # None
    def __init__(self):
        Command.__init__(
            self,
            'Prints the callgraph in DOT format',
            'show-callgraph', 'cg')

    # None => None
    def init_parser(self):
        self.parser.add_argument(
            'index', type=int,
            help='The index of the region to display')
        self.parser.add_argument(
            '-i', '--id', default=False, action='store_true',
            help='Pass the region ID instead of the index')

    # Context, argparse.Namespace => Command.Status
    def run(self, context, argv):
        def get_name(f):
            if f.qualified:
                return f.qualified
            return f.name
        
        regions = context.get_regions()
        region = None
        if argv.id:
            tmp = [r for r in regions if r.id == argv.index]
            if not tmp:
                w_error('No region with id', argv.index)
                return Command.Status.Failure
            region = tmp[0]
        else:
            if argv.index > len(regions):
                w_error('Invalid region index', argv.index)
                return Command.Status.Failure
            region = [r for r in regions if r.idx == argv.index][0]
        
        out = ["digraph G{"]
        for caller, callees in context.cgs[3]:
            if not caller.fake:
                for callee in callees:
                    if not callee.fake:
                        out.append('  "{}" -> "{}"'.format(get_name(caller),
                                                           get_name(callee)))
        out.append('}')
        w_stdout('\n'.join(out))

            
# [Command] => bool
def validate_commands(cobjs):
    longs = set([])
    shorts = set([])
    for cobj in cobjs:
        if cobj.long in longs:
            w_error('Duplicate command found:', cobj.long)
            return False
        elif cobj.short in shorts:
            w_error('Duplicate command found:', cobj.short)
            return False
        longs.add(cobj.long)
        if cobj.short:
            shorts.add(cobj.short)
    return True


# None => int
def main():
    # Setup tab completion
    readline.parse_and_bind('tab: complete')

    # Setup available commands
    cmds = [cls() for cls in Command.__subclasses__()]
    if not validate_commands(cmds):
        raise RuntimeError(
            'Internal error. Could not validate available commands')
    cmds_long = {}
    cmds_short = {}
    for cobj in cmds:
        cmds_long[cobj.long] = cobj
        if cobj.short:
            cmds_short[cobj.short] = cobj

    # The context object contains all the data from an analysis file that
    # will be loaded
    context = moya.Context()

    # Parse command line arguments
    ap = argparse.ArgumentParser()
    ap.add_argument('file', nargs='?', default='')
    argv = ap.parse_args()
    if argv.file:
        CommandOpen().run(context, argv)

    # Main input loop
    while True:
        inp = input(colored('> ', 'white', attrs=['bold']))
        args = inp.split()
        status = None
        if args:
            cmd = args[0]
            if cmd in cmds_long:
                try:
                    cobj = cmds_long[cmd]
                    argv = cobj.parser.parse_args(args[1:])
                    status = cobj.run(context, argv)
                except Command.Parser.Error:
                    pass
            elif cmd in cmds_short:
                try:
                    cobj = cmds_short[cmd]
                    argv = cobj.parser.parse_args(args[1:])
                    status = cobj.run(context, argv)
                except Command.Parser.Error:
                    pass
            else:
                w_error('Unknown command', cmd)
                w_stdout('  help lists available commands')
                status = Command.Status.Failure
        if status == Command.Status.Exit:
            break

    return 0


if __name__ == '__main__':
    exit(main())
