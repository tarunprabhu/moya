#include "Config.h"
#include "Support.h"

#include <dlfcn.h>

#include <llvm/Bitcode/ReaderWriter.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IRReader/IRReader.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Support/MemoryBuffer.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/raw_ostream.h>

using namespace llvm;
using namespace std;

static string getSymbol(string base, string suffix) {
  stringstream ss;
  ss << "__" << base << "_" << suffix;
  return ss.str();
}

static Module* readModuleFromSymbol(void* handle, string symBase) {
  LLVMContext& context = getGlobalContext();

  string symStart = getSymbol(symBase, "start");
  string symEnd = getSymbol(symBase, "end");
  string symSize = getSymbol(symBase, "size");

  void* start = dlsym(handle, symStart.c_str());
  void* end = dlsym(handle, symEnd.c_str());
  size_t size = (size_t)end - (size_t)start;
  if(not size)
    return nullptr;

  StringRef data((const char*)start, size);
  unique_ptr<MemoryBuffer> buffer = MemoryBuffer::getMemBuffer(data, "", false);
  if(buffer->getBufferSize()) {
    SMDiagnostic err;
    unique_ptr<Module> module
        = parseIR(buffer->getMemBufferRef(), err, context);
    if(not module.get())
      moya_error("Error reading module: " << err.getMessage());

    return module.release();
  }
  return nullptr;
}

static void extractPayload(string lib, string bc) {
  string section = moya::Config::getPayloadModuleSymbolBase();

  /// XXX: There is a bug here because calling dlopen just once results in
  /// a segmentation fault.
  dlopen(lib.c_str(), RTLD_NOW);
  if(void* handle = dlopen(lib.c_str(), RTLD_NOW)) {
    if(Module* module = readModuleFromSymbol(handle, section)) {
      error_code ec;
      raw_fd_ostream fd(bc, ec, sys::fs::F_None);
      WriteBitcodeToFile(module, fd);
    } else {
      errs() << "No bitcode found in " << lib << "\n";
    }
    dlclose(handle);
  }
}

int raiseError(string msg) {
  string usage = "moya-extract-from-so <input> [-o <outfile>]";
  errs() << "ERROR: " << msg << "\n\n" << usage << "\n";
  return 1;
}

int main(int argc, char* argv[]) {
  string input, output = "a.bc";

  if(argc < 2)
    raiseError("Insufficient number of arguments");

  for(int i = 1; i < argc; i++) {
    string arg = argv[i];
    if(arg == "-o")
      output = argv[++i];
    else
      input = arg;
  }

  extractPayload(input, output);

  return 0;
}
