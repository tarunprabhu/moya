#include "Globals.h"
#include "common/Location.h"

// The interfaces are all extern "C" because I don't want to have to deal with
// C++'s ABI and strings

extern "C" void moya_module_parse_directive(const char* str,
                                            const char* filename,
                                            unsigned line,
                                            unsigned col) {
  getGlobalSingletonModule().parseDirective(str, filename, line, col);
}

extern "C" uint64_t
moya_module_get_region_id(const char* filename, unsigned line, unsigned col) {
  return getGlobalSingletonModule().getRegionId(
      moya::Location(filename, line, col));
}

extern "C" int moya_module_add_allocated_type(const char* func,
                                              const char* parent,
                                              const char* module,
                                              int type) {
  return getGlobalSingletonModule().addAllocatedType(
      func, parent, module, type);
}

extern "C" void moya_module_add_local_array_1(const char* func,
                                              const char* parent,
                                              const char* module,
                                              const char* alloca) {
  getGlobalSingletonModule().addLocalArray(func, parent, module, alloca);
}

extern "C" void moya_module_add_interface_param(const char* func,
                                                const char* parent,
                                                const char* module,
                                                unsigned argno,
                                                int dtype) {
  getGlobalSingletonModule().addInterfaceParam(
      func, parent, module, argno, dtype);
}

extern "C" void moya_module_add_global_category(const char* name,
                                                const char* module) {
  return getGlobalSingletonModule().addModuleGlobalCategory(name, module);
}
