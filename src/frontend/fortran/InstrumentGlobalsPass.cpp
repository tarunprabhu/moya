#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "Struct.h"
#include "common/DataLayout.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/StringUtils.h"
#include "common/StructLayout.h"

#include <llvm/IR/Constants.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

class InstrumentGlobalsPass : public ModulePass {
public:
  static char ID;

protected:
  void checkGlobal(GlobalVariable&, StructType*);

public:
  InstrumentGlobalsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

InstrumentGlobalsPass::InstrumentGlobalsPass() : ModulePass(ID) {
  llvm::initializeInstrumentGlobalsPassPass(*PassRegistry::getPassRegistry());
}

StringRef InstrumentGlobalsPass::getPassName() const {
  return "Moya Instrument Globals (Fortran)";
}

void InstrumentGlobalsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.setPreservesCFG();
}

void InstrumentGlobalsPass::checkGlobal(GlobalVariable& g, StructType* sty) {
  auto nelems = sty->getNumElements();
  Module& module = *g.getParent();
  moya::DataLayout layout(module);
  llvm::Type* gtype = g.getType()->getElementType();

  // The sizes of the struct and the "global variable" array are often
  // slightly misaligned probably because the extra bytes at the
  // end of the array needed to align it to an 8-byte boundary are not
  // included in the array size but they are in the LLVM struct.
  // To get at least a reasonable approximation of correctness, we just
  // look at the last element of the struct and see if its offset + size
  // aligns with the end of the array
  const moya::StructLayout& sl = layout.getStructLayout(sty);
  llvm::Type* field = sty->getElementType(nelems - 1);
  auto offset = sl.getElementOffset(nelems - 1);
  auto size = layout.getTypeSize(field);
  auto sizeGlobal = layout.getTypeSize(gtype);
  if(sizeGlobal != offset + size) {
    // FIXME: There's a bug in the way the subtypes for Flang's delightfully
    // whimsical hoisted globals are determined. For "STATICS.%d" variables,
    // the offset may not be an actual offset. Or at least, if it is, it
    // is strange because the numbers can get pretty massive. Plus, if there
    // is an allocatable within it, the size gets thrown off.
    // Since I don't give a $(*$#@% about all this any more, just ignore it if
    // it is safe to do so
    //
    if((not moya::StringUtils::startswith(g.getName(), ".STATICS"))
       or g.getNumUses()) {
      moya_error("Inconsistent global sizes:\n"
                 << "  global: " << g << "\n"
                 << "   gtype: " << *gtype << "\n"
                 << "   gsize: " << sizeGlobal << "\n"
                 << " type(n): " << *field << "\n"
                 << "  off(n): " << offset << "\n"
                 << " size(n): " << size << "\n"
                 << "    uses: " << g.getNumUses());
    }
  }
}

bool InstrumentGlobalsPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  LLVMContext& context = module.getContext();
  const DataLayout& dl = module.getDataLayout();

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  for(GlobalVariable& g : module.globals()) {
    if(feModule.hasGlobal(g.getName())) {
      Type* i8 = Type::getInt8Ty(context);
      const moya::GlobalVariable& feGlobal = feModule.getGlobal(g.getName());

      moya::Vector<std::string> names;
      moya::Vector<Type*> elems;
      moya::Set<unsigned> arrayIndices;
      uint64_t curr = 0;
      const moya::Struct& feStruct = feGlobal.getStruct();
      for(unsigned i = 0; i < feStruct.getNumFields(); i++) {
        uint64_t offset = feStruct.getOffset(i);
        const std::string& typeStr = feStruct.getTypeStr(i);

        if(offset < curr) {
          moya_error("Something weird happened. Curr = "
                     << curr << ". Offset = " << offset << " for " << typeStr);
        } else if(offset > curr) {
          elems.push_back(ArrayType::get(i8, offset - curr));
          curr = offset;
        }

        Type* type = moya::parse(typeStr, module);
        elems.push_back(type);
        names.push_back(feStruct.getName(i));
        curr = curr + dl.getTypeStoreSize(type);

        unsigned next = i + 2;
        if((next < feStruct.getNumFields()) and feStruct.isDescriptor(next))
          arrayIndices.insert(i);
      }

      std::stringstream ss;
      ss << "struct.__moya_" << feGlobal.getLLVMName();
      StructType* sty
          = moya::LLVMUtils::createStruct(module, elems, ss.str(), true);

      checkGlobal(g, sty);

      if(feGlobal.hasName())
        changed |= moya::Metadata::setSourceName(g, feGlobal.getName());
      if(feGlobal.hasArtificialName())
        changed |= moya::Metadata::setArtificialName(
            g, feGlobal.getArtificialName());
      if(feGlobal.hasLocation())
        changed |= moya::Metadata::setSourceLocation(g, feGlobal.getLocation());
      changed |= moya::Metadata::setSourceLang(g, feGlobal.getSourceLang());
      changed |= moya::Metadata::setArtificial(g);
      changed |= moya::Metadata::setAnalysisType(g, sty->getPointerTo());
      changed
          |= moya::Metadata::setFlangArrayIndices(sty, arrayIndices, module);
      changed |= moya::Metadata::setFieldNames(sty, names, module);
    }

    if(feModule.isLayoutDescriptor(g.getName())) {
      g.setConstant(true);
      changed |= moya::Metadata::setFortranTypeInfo(g);
      changed |= moya::Metadata::setNoAnalyze(g);
    }

    if(not g.hasPrivateLinkage())
      changed |= moya::Metadata::setLinkage(g, g.getLinkage());
  }

  return changed;
}

char InstrumentGlobalsPass::ID = 0;

static const char* name = "moya-instr-globals-fort";
static const char* descr = "Adds instrumentation to globals (Fortran)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(InstrumentGlobalsPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_END(InstrumentGlobalsPass, name, descr, cfg, analysis)

void registerInstrumentGlobalsPass(const PassManagerBuilder&,
                                   legacy::PassManagerBase& pm) {
  pm.add(new InstrumentGlobalsPass());
}

Pass* createInstrumentGlobalsPass() {
  return new InstrumentGlobalsPass();
}
