#include "GEP.h"
#include "Function.h"

#include "common/Deserializer.h"
#include "common/Serializer.h"

namespace moya {

GEP::GEP(Function& func) : func(func) {
  ;
}

GEP::GEP(Function& func,
         int64_t cumulative,
         int64_t offset,
         bool array,
         const int64_t* offsets,
         uint64_t count)
    : func(func), cumulative(cumulative), offset(offset), array(array),
      offsets(offsets, offsets + count) {
  ;
}

GEP::GEP(Function& func,
         int64_t cumulative,
         int64_t offset,
         bool array,
         const Vector<int64_t>& offsets)
    : func(func), cumulative(cumulative), offset(offset), array(array),
      offsets(offsets) {
  ;
}

const Function& GEP::getFunction() const {
  return func;
}

const Module& GEP::getModule() const {
  return func.getModule();
}

bool GEP::isConstGEP() const {
  return offsets.size();
}

int64_t GEP::getOffset() const {
  return offset;
}

int64_t GEP::getCumulative() const {
  return cumulative;
}

bool GEP::isArray() const {
  return array;
}

const Vector<int64_t>& GEP::getOffsets() const {
  return offsets;
}

llvm::GetElementPtrInst* GEP::getLLVM() const {
  return llvm;
}

void GEP::overrideOffsets(const Vector<int64_t>& offsets) {
  this->offsets = offsets;
}

void GEP::setLLVM(llvm::GetElementPtrInst* gep) {
  llvm = gep;
}

void GEP::serialize(Serializer& s) const {
  s.mapStart();
  s.add("cumulative", cumulative);
  s.add("offset", offset);
  s.add("array", array);
  s.add("offsets", offsets);
  s.mapEnd();
}

void GEP::deserialize(Deserializer& d) {
  d.mapStart();
  d.deserialize("cumulative", cumulative);
  d.deserialize("offset", offset);
  d.deserialize("array", array);
  d.deserialize("offsets", offsets);
  d.mapEnd();
}

} // namespace moya
