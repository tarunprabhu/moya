#include "Closure.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/Vector.h"

#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

class InstrumentTypesPass : public ModulePass {
public:
  static char ID;

protected:
  bool instrumentClosure(const moya::Closure&, Module&);
  bool instrumentStruct(const moya::Struct&, Module&);

public:
  InstrumentTypesPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

InstrumentTypesPass::InstrumentTypesPass() : ModulePass(ID) {
  llvm::initializeInstrumentTypesPassPass(*PassRegistry::getPassRegistry());
}

StringRef InstrumentTypesPass::getPassName() const {
  return "Moya Instrument Types (Fortran)";
}

void InstrumentTypesPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.setPreservesCFG();
}

bool InstrumentTypesPass::instrumentClosure(const moya::Closure& closure,
                                            Module& module) {
  bool changed = false;

  moya::Vector<Type*> elems;
  for(const moya::Type* feType : closure.getTypes())
    elems.push_back(moya::parse(feType->getLLVMStr(), module));

  std::stringstream ss;
  ss << "struct.__moya_closure_" << closure.getFunctionName();
  StructType* sty
      = moya::LLVMUtils::createStruct(module, elems, ss.str(), true);

  changed |= moya::Metadata::setArtificial(sty, module);
  changed |= moya::Metadata::setFieldNames(sty, closure.getNames(), module);
  changed |= moya::Metadata::setFlangClosure(closure.getLLVM(), sty, module);
  changed |= moya::Metadata::setFieldNames(
      closure.getLLVM(), closure.getNames(), module);

  return changed;
}

bool InstrumentTypesPass::instrumentStruct(const moya::Struct& feStruct,
                                           Module& module) {
  bool changed = false;

  // Flang's array descriptors have the format:
  //
  // type descriptor = { type*, i64, [n x i64] }
  //
  // The array of integers part of the descriptor will have the "DESCRIPTOR"
  // attribute set and this will be reflected in moya::Struct.
  // We need to know which pointers in the struct are actually pointers to
  // arrays as opposed to just pointers (this can happen for instance when
  // a local variable/parameter has the POINTER attribute but no array
  // specification
  //
  moya::Set<unsigned> indices;
  if(feStruct.getNumFields() > 2)
    for(unsigned i = 0; i < feStruct.getNumFields() - 2; i++)
      if(feStruct.isDescriptor(i + 2))
        indices.insert(i);

  moya::Vector<std::string> names;
  for(unsigned i = 0; i < feStruct.getNumFields(); i++)
    names.push_back(feStruct.getName(i));

  changed |= moya::Metadata::setFieldNames(feStruct.getLLVM(), names, module);
  changed |= moya::Metadata::setFlangArrayIndices(
      feStruct.getLLVM(), indices, module);
  if(feStruct.getLocation().isValid())
    changed |= moya::Metadata::setSourceLocation(
        feStruct.getLLVM(), feStruct.getLocation(), module);

  return changed;
}

bool InstrumentTypesPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();
  for(StructType* sty : module.getIdentifiedStructTypes())
    if(feModule.hasClosure(sty))
      changed |= instrumentClosure(feModule.getClosure(sty), module);

  for(const auto& feStruct : feModule.getStructs())
    if(feStruct->getLLVM())
      changed |= instrumentStruct(*feStruct, module);

  return changed;
}

char InstrumentTypesPass::ID = 0;

static const char* name = "moya-instr-types-fort";
static const char* descr = "Adds instrumentation to types (Fortran)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(InstrumentTypesPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_END(InstrumentTypesPass, name, descr, cfg, analysis)

void registerInstrumentTypesPass(const PassManagerBuilder&,
                                 legacy::PassManagerBase& pm) {
  pm.add(new InstrumentTypesPass());
}

Pass* createInstrumentTypesPass() {
  return new InstrumentTypesPass();
}
