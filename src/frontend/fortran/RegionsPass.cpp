#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/InstructionUtils.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/Region.h"
#include "common/Set.h"

#include <llvm/IR/Constants.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

class RegionsPass : public ModulePass {
public:
  static char ID;

public:
  RegionsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

RegionsPass::RegionsPass() : ModulePass(ID) {
  ;
}

StringRef RegionsPass::getPassName() const {
  return "Moya Regions (Fortran)";
}

void RegionsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.setPreservesCFG();
}

bool RegionsPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  for(const moya::Region& region : feModule.getRegions()) {
    changed |= moya::Metadata::addRegions(module, region);
  }

  // // Find the regions. These aren't in the info object currently because
  // flang
  // // calls clang as a separate process, so we need to extract the regions
  // // from the code. This should have been added to the metadata during the
  // // frontend parsing but until flang starts using the LLVM API to generate
  // // IR, that is not going to happen
  // moya::Vector<moya::Region> regions;
  // Function* fnEnter = moya::LLVMUtils::getEnterRegionFunction(module);
  // for(Function& f : module.functions())
  //   for(auto i = inst_begin(f); i != inst_end(f); i++)
  //     if(auto* call = dyn_cast<CallInst>(&*i))
  //       if(Function* callee = moya::LLVMUtils::getCalledFunction(call))
  //         if(callee == fnEnter)
  //           regions.emplace_back(
  //               moya::LLVMUtils::getArgOperandAs<RegionID>(call, 0),
  //               moya::LLVMUtils::getLocation(call).str(),
  //               moya::LLVMUtils::getLocation(call));

  // for(const moya::Region& region : regions)
  //   changed |= moya::Metadata::addRegions(module, region);

  return changed;
}

char RegionsPass::ID = 0;

static const char* name = "moya-regions-pass-fort";
static const char* descr = "Process identified regions (Fortran)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(RegionsPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_END(RegionsPass, name, descr, cfg, analysis)

void registerRegionsPass(const PassManagerBuilder&,
                         legacy::PassManagerBase& pm) {
  pm.add(new RegionsPass());
}

Pass* createRegionsPass() {
  return new RegionsPass();
}
