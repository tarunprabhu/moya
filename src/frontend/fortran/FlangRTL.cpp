#include "FlangRTL.h"
#include "common/Map.h"
#include "common/Set.h"

static const moya::Map<std::string, unsigned> allocs = {
    {"f90_alloc", 4},
    {"f90_alloc03", 4},
    {"f90_alloc04", 4},
    {"f90_alloc04a", 4},
    {"f90_alloc04a_i8", 4},
    {"f90_alloc03_chk", 4},
    {"f90_alloc04_chk", 4},
    {"f90_alloc04_chka", 4},
    {"f90_alloc04_chka_i8", 4},
    {"f90_kalloc", 4},
    {"f90_calloc", 4},
    {"f90_calloc03", 4},
    {"f90_calloc04", 4},
    {"f90_calloc04a", 4},
    {"f90_calloc04a_i8", 4},
    {"f90_kcalloc", 4},
    {"f90_ptr_alloc", 4},
    {"f90_ptr_alloc03", 4},
    {"f90_ptr_alloc04", 4},
    {"f90_ptr_alloc04a", 4},
    {"f90_ptr_alloc04a_i8", 4},
    {"f90_ptr_calloc", 4},
    {"f90_ptr_calloc03", 4},
    {"f90_ptr_calloc04", 4},
    {"f90_ptr_calloc04a", 4},
    {"f90_ptr_calloc04a_i8", 4},
    {"f90_ptr_kalloc", 4},
    {"f90_ptr_kalloc03", 4},
    {"f90_ptr_kalloc04", 4},
    {"f90_ptr_src_alloc03", 5},
    {"f90_ptr_src_alloc04", 5},
    {"f90_ptr_src_alloc04a", 5},
    {"f90_ptr_src_alloc04a_i8", 5},
    {"f90_ptr_src_calloc03", 5},
    {"f90_ptr_src_calloc04", 5},
    {"f90_ptr_src_calloc04a", 5},
    {"f90_ptr_src_calloc04a_i8", 5},
};

bool FlangRTL::isAllocatorFunction(const std::string& f) {
  return allocs.contains(f);
}

unsigned FlangRTL::getAllocatorFunctionArgNo(const std::string& f) {
  return allocs.at(f);
}

static const moya::Map<std::string, unsigned> deallocs = {
    {"f90_dealloc", 1},
    {"f90_dealloca", 1},
    {"f90_dealloca_i8", 1},
    {"f90_dealloc03", 1},
    {"f90_dealloc03a", 1},
    {"f90_dealloc03a_i8", 1},
    {"f90_dealloc_mbr", 1},
    {"f90_dealloc_mbr03", 1},
};

bool FlangRTL::isDeallocatorFunction(const std::string& f) {
  return deallocs.contains(f);
}

unsigned FlangRTL::getDeallocatorFunctionArgNo(const std::string& f) {
  return deallocs.at(f);
}

static const moya::Set<std::string> mallocs = {
    "f90_auto_allocv",
    "f90_auto_alloc",
    "f90_auto_alloc_i8",
    "f90_auto_alloc04",
    "f90_auto_alloc04_i8",
    "f90_auto_calloc",
    "f90_auto_calloc04",
    "f90_auto_calloc04_i8",
};

bool FlangRTL::isMallocLikeFunction(const std::string& f) {
  return mallocs.contains(f);
}

static const moya::Set<std::string> frees = {
    "f90_auto_dealloc",
    "f90_auto_dealloc_i8",
};

bool FlangRTL::isFreeLikeFunction(const std::string& f) {
  return frees.contains(f);
}

static const moya::Set<std::string> memcpys = {
    "f90_mcopy1",
    "f90_mcopy2",
    "f90_mcopy4",
    "f90_mcopy8",
    "f90_mcopyz8",
    "f90_mcopyz16",
    "f90_copy_f77_argw",
    "f90_copy_f77_argl",
    "f90_copy_f77_argsl",
    "f90_copy_f90_argw",
    "f90_copy_f90_argl",
    "f90_copy_f90_argsl",
};

bool FlangRTL::isMemcpyLikeFunction(const std::string& f) {
  return memcpys.contains(f);
}

static const moya::Set<std::string> memsets = {};

bool FlangRTL::isMemsetLikeFunction(const std::string& f) {
  return memsets.contains(f);
}
