#include "Closure.h"
#include "Module.h"
#include "common/Deserializer.h"
#include "common/Diagnostics.h"
#include "common/Serializer.h"

#include <llvm/Support/raw_ostream.h>

namespace moya {

Closure::Closure(Module& module,
                 const std::string& llvmName,
                 const std::string& fname)
    : module(module), llvmName(llvmName), fname(fname) {
  ;
}

Module& Closure::getModule() {
  return module;
}

const Module& Closure::getModule() const {
  return module;
}

const std::string& Closure::getFunctionName() const {
  return fname;
}

const std::string& Closure::getLLVMName() const {
  return llvmName;
}

const Vector<std::string>& Closure::getNames() const {
  return names;
}

const Vector<Type*>& Closure::getTypes() const {
  return types;
}

bool Closure::isDescriptor(unsigned idx) const {
  return descriptors.contains(idx);
}

llvm::StructType* Closure::getLLVM() const {
  return llvm;
}

void Closure::addType(const std::string& name,
                      const std::string& llvmStr,
                      bool ptr,
                      bool descriptor) {
  names.push_back(name);
  if(ptr)
    types.push_back(&getModule().addType(llvmStr + "*"));
  else
    types.push_back(&getModule().addType(llvmStr));

  if(descriptor)
    descriptors.insert(types.size() - 1);
}

void Closure::setLLVM(llvm::StructType* llvm) {
  this->llvm = llvm;
  getModule().setLLVM(*this, llvm);
}

void Closure::serialize(Serializer& s) const {
  s.mapStart();
  s.add("llvmName", llvmName);
  s.add("fname", fname);
  s.add("descriptors", descriptors);
  s.add("names", names);
  s.add("types", types);
  s.mapEnd();
}

void Closure::deserialize(Deserializer& d) {
  d.mapStart();
  d.deserialize("llvmName", llvmName);
  d.deserialize("fname", fname);
  d.deserialize("descriptors", descriptors);
  d.deserialize("names", names);
  d.arrStart("types");
  while(not d.arrEmpty()) {
    Type& type = getModule().addType<Type>();
    d.deserialize(type);
    types.push_back(&type);
  }
  d.arrEnd();
  d.mapEnd();
}

} // namespace moya
