#include "Function.h"
#include "GlobalVariable.h"
#include "Globals.h"
#include "Type.h"
#include "common/Location.h"

// The interfaces are all extern "C" because I don't want to have to deal with
// C++'s ABI and strings

extern "C" int64_t moya_const_get_array_index_marker(void) {
  return moya::Module::ArrayIndexMarker;
}

extern "C" void moya_module_add_fmodule(const char* name,
                                        const char* filename,
                                        unsigned line,
                                        unsigned col) {
  getGlobalSingletonModule().addFModule(name,
                                        moya::Location(filename, line, col));
}

extern "C" void moya_module_add_function(const char* llvmName,
                                         const char* qualName,
                                         const char* srcName,
                                         const char* filename,
                                         unsigned line,
                                         unsigned col,
                                         int internal) {
  getGlobalSingletonModule().addFunction(llvmName,
                                         qualName,
                                         srcName,
                                         moya::Location(filename, line, col),
                                         internal);
}

extern "C" int64_t moya_module_get_min_allocated_type_id(const char* func) {
  return getGlobalSingletonModule().getMinAllocatedTypeId(func);
}

extern "C" int64_t moya_module_get_max_allocated_type_id(const char* func) {
  return getGlobalSingletonModule().getMaxAllocatedTypeId(func);
}

extern "C" int moya_module_get_allocated_type(const char* func, int64_t id) {
  return getGlobalSingletonModule().getAllocatedType(func, id);
}

extern "C" void moya_function_add_arg(const char* llvmName,
                                      const char* argname,
                                      const char* llvmStr,
                                      int linked,
                                      int dummy,
                                      int array,
                                      int closure) {
  getGlobalSingletonModule().getFunction(llvmName).addArgument(
      argname, llvmStr, linked, dummy, array, closure);
}

extern "C" void moya_function_add_gep(const char* llvmName,
                                      int64_t cumulative,
                                      int64_t offset,
                                      bool array,
                                      const int64_t* offsets,
                                      uint64_t count) {
  getGlobalSingletonModule().getFunction(llvmName).addGEP(
      cumulative, offset, array, offsets, count);
}

extern "C" void moya_function_add_child(const char* func, const char* child) {
  moya::Module& module = getGlobalSingletonModule();
  module.getFunction(func).addChild(module.getFunction(child));
}

extern "C" void moya_module_add_global(const char* global,
                                       const char* generatedName,
                                       const char* typeName,
                                       const char* parent,
                                       unsigned count,
                                       const char* const* names,
                                       const unsigned* offsets,
                                       const char* const* types,
                                       const unsigned* descriptors) {
  getGlobalSingletonModule().addGlobal(global,
                                       generatedName,
                                       typeName,
                                       parent,
                                       count,
                                       names,
                                       offsets,
                                       types,
                                       descriptors);
}

extern "C" void moya_module_add_struct(const char* sname) {
  moya::Module& module = getGlobalSingletonModule();

  //    How cheerfully he seems to grin
  //    How neatly spreads his claws,
  //    And welcomes little fishes in
  //    With gently smiling jaws!
  //
  // The sanity check here was originally to not add duplicate structs.
  // But for some incredible reason, there are all sorts of duplicates in
  // Flang's list of "user structs". The best part is that some of the "user
  // structs" are actually Flang's trademark "hoisted byte arrays". In any case,
  // there are duplicates of structs so we just ignore the duplicates. This
  // could potentially cause problems because LLVM does a fair bit of
  // structural uniquing on structs, so it is possible that the first struct
  // that gets added doesn't actually make it into the IR. The better thing
  // to do in this case is to allow duplicates and then associate the only
  // one that can be associated. But you can imagine how enthusaistic I am
  // about doing that
  //
  //   moya_error("Struct " << sname << " already in module");
  //
  if(not module.hasStruct(sname))
    getGlobalSingletonModule().addStruct(sname);
}

extern "C" void moya_struct_add_type(const char* sname,
                                     const char* name,
                                     unsigned offset,
                                     const char* type,
                                     bool descriptor) {
  getGlobalSingletonModule().getStruct(sname).addType(
      name, offset, type, descriptor);
}

extern "C" void moya_module_add_closure(const char* tname, const char* fname) {
  getGlobalSingletonModule().addClosure(tname, fname);
}

extern "C" void moya_module_add_typedesc(const char* tname) {
  getGlobalSingletonModule().addTypeDesc(tname);
}

extern "C" void moya_module_add_layout_descriptor(const char* name) {
  getGlobalSingletonModule().addLayoutDescriptor(name);
}

extern "C" void moya_closure_add_type(const char* closure,
                                      const char* name,
                                      const char* type,
                                      bool ptr,
                                      bool descriptor) {
  getGlobalSingletonModule().getClosure(closure).addType(
      name, type, ptr, descriptor);
}

extern "C" void moya_function_add_allocated_type(const char* func,
                                                 int64_t id,
                                                 const char* llvm) {
  getGlobalSingletonModule().getFunction(func).addAllocatedType(id, llvm);
}

extern "C" int moya_module_is_global_category(const char* name) {
  return getGlobalSingletonModule().isModuleGlobalCategory(name);
}

extern "C" const char*
moya_module_get_global_category_module(const char* name) {
  return getGlobalSingletonModule().getModuleGlobalModule(name).c_str();
}

extern "C" void moya_module_add_local_array_2(const char* func,
                                              const char* base,
                                              const char* alloca) {
  return getGlobalSingletonModule().addLocalArray(func, base, alloca);
}

extern "C" unsigned moya_module_get_num_interface_params(const char* func,
                                                         unsigned argno) {
  return getGlobalSingletonModule().getInterfaceParams(func, argno).size();
}

extern "C" const int* moya_module_get_interface_params(const char* func,
                                                       unsigned argno) {
  return getGlobalSingletonModule().getInterfaceParams(func, argno).data();
}
