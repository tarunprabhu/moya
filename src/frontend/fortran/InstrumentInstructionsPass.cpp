#include "FlangRTL.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/DataLayout.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/IR/InstIterator.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

class InstrumentInstructionsPass : public ModulePass {
protected:
  moya::DataLayout layout;

protected:
  bool instrumentAllocator(CallInst*, const moya::Function&);
  bool instrumentDeallocator(CallInst*, const moya::Function&);
  bool instrumentMemcpy(CallInst*, const moya::Function&);
  bool instrumentMemset(CallInst*, const moya::Function&);
  bool instrumentMalloc(CallInst*, const moya::Function&);
  bool instrumentFree(CallInst*, const moya::Function&);
  bool instrument(IntToPtrInst*);
  int64_t getMarkerId(CallInst*);

public:
  InstrumentInstructionsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);

public:
  static char ID;
};

InstrumentInstructionsPass::InstrumentInstructionsPass() : ModulePass(ID) {
  initializeInstrumentInstructionsPassPass(*PassRegistry::getPassRegistry());
}

StringRef InstrumentInstructionsPass::getPassName() const {
  return "Moya Instrument Instructions (Fortran)";
}

void InstrumentInstructionsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.setPreservesCFG();
}

int64_t InstrumentInstructionsPass::getMarkerId(CallInst* call) {
  for(Instruction* inst = call; inst; inst = inst->getNextNonDebugInstruction())
    if(auto* call = dyn_cast<CallInst>(inst))
      if(Function* f = moya::LLVMUtils::getCalledFunction(call))
        if(f->getName() == "__moya_marker_func") {
          auto* arg = dyn_cast<CastInst>(call->getArgOperand(0));
          auto* g = dyn_cast<GlobalVariable>(arg->getOperand(0));
          return cast<ConstantInt>(g->getInitializer())->getLimitedValue();
        }

  return 0;
}

bool InstrumentInstructionsPass::instrumentAllocator(
    CallInst* call,
    const moya::Function& feFunc) {
  bool changed = false;

  if(int64_t id = getMarkerId(call)) {
    Type* type = feFunc.getAllocatedType(id).getLLVM();
    if(auto* aty = dyn_cast<ArrayType>(type))
      type = moya::LLVMUtils::getFlattenedElementType(aty);
    changed |= moya::Metadata::setCallAllocated(call, type);
  } else {
    moya_error("Could not find marker function for allocation: "
               << *call << " in " << feFunc.getLLVMName());
  }

  return changed;
}

bool InstrumentInstructionsPass::instrumentDeallocator(
    CallInst* call,
    const moya::Function& feFunc) {
  bool changed = false;

  if(int64_t id = getMarkerId(call)) {
    Type* type = feFunc.getAllocatedType(id).getLLVM();
    if(auto* aty = dyn_cast<ArrayType>(type))
      type = moya::LLVMUtils::getFlattenedElementType(aty);
    changed |= moya::Metadata::setCallFree(call, type);
  } else {
    moya_error("Could not find marker function for deallocation: "
               << *call << " in " << feFunc.getLLVMName());
  }

  return changed;
}

bool InstrumentInstructionsPass::instrumentMemcpy(
    CallInst* call,
    const moya::Function& feFunc) {
  bool changed = false;
  const std::string& fname
      = moya::LLVMUtils::getCalledFunction(call)->getName();
  LLVMContext& context = call->getContext();

  if(fname == "f90_mcopy1")
    changed |= moya::Metadata::setCallMemcpy(call, Type::getInt8Ty(context));
  else if(fname == "f90_mcopy2")
    changed |= moya::Metadata::setCallMemcpy(call, Type::getInt16Ty(context));
  else if((fname == "f90_mcopy4") or (fname == "f90_copy_f77_argw")
          or (fname == "f90_copy_f90_argw"))
    changed |= moya::Metadata::setCallMemcpy(call, Type::getInt32Ty(context));
  else if((fname == "f90_mcopy8") or (fname == "f90_copy_f77_argl")
          or (fname == "f90_copy_f90_argl"))
    changed |= moya::Metadata::setCallMemcpy(call, Type::getInt64Ty(context));
  else
    moya_error("Unsupported memcpy-like function: " << fname);

  return changed;
}

bool InstrumentInstructionsPass::instrumentMemset(
    CallInst* call,
    const moya::Function& feFunc) {
  bool changed = false;

  moya_error("Unimplemented: memset");

  return changed;
}

bool InstrumentInstructionsPass::instrumentMalloc(
    CallInst* call,
    const moya::Function& feFunc) {
  bool changed = false;

  if(int64_t id = getMarkerId(call)) {
    Type* type = feFunc.getAllocatedType(id).getLLVM();
    if(auto* aty = dyn_cast<ArrayType>(type))
      type = moya::LLVMUtils::getFlattenedElementType(aty);
    changed |= moya::Metadata::setAnalysisType(call, type->getPointerTo());

    // The memory allocation functions return an i64, but the analysis type
    // will be T*. The sanity checker will complain unless we add this
    changed |= moya::Metadata::setSane(call);
  } else {
    moya_error("Could not find marker function for allocation: "
               << *call << " in " << feFunc.getLLVMName());
  }

  return changed;
}

bool InstrumentInstructionsPass::instrumentFree(CallInst* call,
                                                const moya::Function& feFunc) {
  bool changed = false;
  Module& module = *moya::LLVMUtils::getModule(call);

  auto* pty = moya::LLVMUtils::getCanonicalTypeAs<PointerType>(
      moya::LLVMUtils::getAnalysisType(call->getArgOperand(0)), module);
  changed |= moya::Metadata::setCallFree(call, pty->getElementType());

  return changed;
}

bool InstrumentInstructionsPass::instrument(IntToPtrInst* inttoptr) {
  bool changed = false;

  for(Use& u : inttoptr->uses())
    if(auto* call = dyn_cast<CallInst>(u.getUser()))
      if(Function* f = moya::LLVMUtils::getCalledFunction(call))
        if(moya::Metadata::hasFlang(f))
          changed |= moya::Metadata::setIgnore(inttoptr);

  return changed;
}

bool InstrumentInstructionsPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  layout.reset(module);

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  for(Function& f : module.functions()) {
    // We don't use f.size() here because the "fake" function that is added
    // which represents a module will not have a moya::Function associated with
    // it but it does have an actual size.
    if(feModule.hasFunction(f)) {
      const moya::Function& feFunc = feModule.getFunction(f);
      for(auto i = inst_begin(f); i != inst_end(f); i++) {
        if(auto* inttoptr = dyn_cast<IntToPtrInst>(&*i)) {
          changed |= instrument(inttoptr);
        } else if(auto* call = dyn_cast<CallInst>(&*i)) {
          if(Function* rtlFn = moya::LLVMUtils::getCalledFunction(call)) {
            if(moya::Metadata::hasFlangAllocator(rtlFn))
              changed |= instrumentAllocator(call, feFunc);
            else if(moya::Metadata::hasFlangDeallocator(rtlFn))
              changed |= instrumentDeallocator(call, feFunc);
            else if(moya::Metadata::hasFlangMemcpy(rtlFn))
              changed |= instrumentMemcpy(call, feFunc);
            else if(moya::Metadata::hasFlangMemset(rtlFn))
              changed |= instrumentMemset(call, feFunc);
            else if(moya::Metadata::hasFlangMalloc(rtlFn))
              changed |= instrumentMalloc(call, feFunc);
            else if(moya::Metadata::hasSystemFree(rtlFn))
              changed |= instrumentFree(call, feFunc);
          }
        }
      }
    }
  }

  return changed;
}

char InstrumentInstructionsPass::ID = 0;

static const char* name = "moya-instr-insts-fort";
static const char* descr = "Adds instrumentation to instructions (Fortran)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(InstrumentInstructionsPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_END(InstrumentInstructionsPass, name, descr, cfg, analysis)

void registerInstrumentInstructionsPass(const PassManagerBuilder&,
                                        legacy::PassManagerBase& pm) {
  pm.add(new InstrumentInstructionsPass());
}

Pass* createInstrumentInstructionsPass() {
  return new InstrumentInstructionsPass();
}
