#include "Passes.h"
#include "common/ArrayRefUtils.h"
#include "common/Diagnostics.h"
#include "common/InstructionUtils.h"
#include "common/Set.h"
#include "common/TypeUtils.h"
#include "common/Vector.h"

#include <llvm/IR/Attributes.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

class FixLibFlangSignaturesPass : public ModulePass {
public:
  static char ID;

protected:
  bool
  process(Module&, Type*, const std::string&, const moya::Vector<Type*>&, bool);
  bool replace(CallInst*, Function*);
  bool stripRedundantCasts(Module&);

public:
  FixLibFlangSignaturesPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

FixLibFlangSignaturesPass::FixLibFlangSignaturesPass() : ModulePass(ID) {
  initializeFixLibFlangSignaturesPassPass(*PassRegistry::getPassRegistry());
}

StringRef FixLibFlangSignaturesPass::getPassName() const {
  return "Moya Fix libFlang Signatures";
}

void FixLibFlangSignaturesPass::getAnalysisUsage(AnalysisUsage& AU) const {
  ;
}

// More rubbish from Flang: Apparently, there is a difference between the code
// that gets generated in flang2 and the signatures of their own runtime
// library. So once we fix the signatures to match the library, we're may be
// left with invalid casts
bool FixLibFlangSignaturesPass::stripRedundantCasts(Module& module) {
  moya::Vector<CastInst*> casts;

  for(Function& f : module.functions())
    for(inst_iterator i = inst_begin(f); i != inst_end(f); i++)
      if(auto* cst = dyn_cast<CastInst>(&*i))
        if(cst->getOperand(0)->getType() == cst->getDestTy())
          casts.push_back(cst);
  for(CastInst* cst : casts) {
    cst->replaceAllUsesWith(cst->getOperand(0));
    cst->eraseFromParent();
  }

  return casts.size();
}

bool FixLibFlangSignaturesPass::replace(CallInst* call, Function* fn) {
  FunctionType* ftype = fn->getFunctionType();
  moya::Vector<Value*> args;
  for(unsigned i = 0; i < ftype->getNumParams(); i++) {
    Value* arg = call->getArgOperand(i);
    Type* type = ftype->getParamType(i);
    if(arg->getType() != type) {
      if(type->isPointerTy())
        args.push_back(CastInst::CreateBitOrPointerCast(arg, type, "", call));
      else if(auto* ity = dyn_cast<IntegerType>(type))
        args.push_back(CastInst::CreateIntegerCast(
            arg, type, ity->getSignBit(), "", call));
      else if(moya::LLVMUtils::isFPTy(type))
        args.push_back(CastInst::CreateFPCast(arg, type, "", call));
    } else {
      args.push_back(arg);
    }
  }
  for(unsigned i = ftype->getNumParams(); i < call->getNumArgOperands(); i++)
    args.push_back(call->getArgOperand(i));

  CallInst* newCall
      = CallInst::Create(fn, moya::LLVMUtils::makeArrayRef(args), "", call);
  newCall->copyMetadata(*call);
  call->replaceAllUsesWith(newCall);
  call->eraseFromParent();

  return true;
}

bool FixLibFlangSignaturesPass::process(Module& module,
                                        Type* ret,
                                        const std::string& name,
                                        const moya::Vector<Type*>& types,
                                        bool vararg) {
  Function* libf = module.getFunction(name);

  // If the function is not used, don't bother
  if(not libf)
    return false;

  // There may be a user function with the same name. I'm not sure if this
  // is actually possible, but if it does happen, ignore it.
  if(libf->size())
    return false;

  bool changed = false;

  LLVMContext& context = module.getContext();
  moya::Vector<CallInst*> calls;
  moya::Vector<Instruction*> uses;
  for(Use& u : libf->uses()) {
    if(auto* inst = dyn_cast<Instruction>(u.getUser()))
      uses.push_back(inst);
    if(auto* cst = dyn_cast<CastInst>(u.getUser()))
      for(Use& v : cst->uses())
        if(auto* call = dyn_cast<CallInst>(v.getUser()))
          calls.push_back(call);
  }

  FunctionType* ftype
      = FunctionType::get(ret, moya::LLVMUtils::makeArrayRef(types), vararg);
  std::string oldName = libf->getName();
  libf->setName(oldName + ".old");

  Function* newf = cast<Function>(module.getOrInsertFunction(name, ftype));
  newf->copyAttributesFrom(libf);
  // Unbelievable! Return types are actually inconsistent!
  AttributeList attrs = newf->getAttributes();
  attrs = attrs.removeAttribute(
      context, AttributeList::ReturnIndex, Attribute::AttrKind::SExt);
  attrs = attrs.removeAttribute(
      context, AttributeList::ReturnIndex, Attribute::AttrKind::ZExt);
  newf->setAttributes(attrs);

  for(CallInst* call : calls)
    changed |= replace(call, newf);

  for(Instruction* inst : uses)
    inst->eraseFromParent();

  libf->eraseFromParent();

  return changed;
}

bool FixLibFlangSignaturesPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;
  LLVMContext& context = module.getContext();
  Type* vd = Type::getVoidTy(context);
  Type* i8 = Type::getInt8Ty(context);
  Type* i16 = Type::getInt16Ty(context);
  Type* i32 = Type::getInt32Ty(context);
  Type* i64 = Type::getInt64Ty(context);
  Type* f32 = Type::getFloatTy(context);
  Type* f64 = Type::getDoubleTy(context);

  Type* pi8 = i8->getPointerTo();
  Type* pi16 = i16->getPointerTo();
  Type* pi32 = i32->getPointerTo();
  Type* pi64 = i64->getPointerTo();
  Type* pf32 = f32->getPointerTo();
  Type* pf64 = f64->getPointerTo();

  Type* ppi8 = pi8->getPointerTo();
  Type* ppi32 = pi32->getPointerTo();

  Type* pppi8 = ppi8->getPointerTo();

  Type* desc = ArrayType::get(i32, 0);
  Type* pdesc = desc->getPointerTo();

  Type* odesc = ArrayType::get(i32, 0);
  Type* podesc = odesc->getPointerTo();

  Type* nml = module.getTypeByName("NML_GROUP");
  Type* pnml = nml ? nml->getPointerTo() : nullptr;

  Type* sked = module.getTypeByName("sked");
  Type* psked = sked ? sked->getPointerTo() : nullptr;
  Type* ppsked = psked ? psked->getPointerTo() : nullptr;

  Type* cmplx = StructType::get(context, {f32, f32});
  Type* pcmplx = cmplx->getPointerTo();

  Type* dcmplx = StructType::get(context, {f64, f64});
  Type* pdcmplx = dcmplx->getPointerTo();

  Type* proc = ArrayType::get(i32, 0);
  Type* pproc = proc->getPointerTo();

  changed
      |= process(module, vd, "fort_ptr_offset", {pi64, ppi8, pi8, pi32}, false);
  changed |= process(module, i32, "ftn_allocated", {pi8}, false);
  changed |= process(module, i32, "f90_allocated", {pi8}, false);
  changed |= process(module, i64, "f90_kallocated", {pi8}, false);
  changed |= process(module, pi8, "ftn_allocate", {i32, pi32}, false);
  changed |= process(module, pi8, "ftn_alloc", {i32, pi32}, true);
  changed |= process(
      module, vd, "f90_alloc", {pi32, pi32, pi32, pi32, ppi8, pi64, pi8}, true);
  changed |= process(module,
                     vd,
                     "f90_alloc03",
                     {pi32, pi32, pi32, pi32, ppi8, pi64, pi32, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_alloc03_chk",
                     {pi32, pi32, pi32, pi32, ppi8, pi64, pi32, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_alloc04",
                     {pi64, pi32, pi32, pi32, ppi8, pi64, pi32, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_alloc04_chk",
                     {pi64, pi32, pi32, pi32, ppi8, pi64, pi32, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_alloc04a",
                     {pi64, pi32, pi32, pi32, ppi8, pi64, pi32, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_alloc04_chka",
                     {pi64, pi32, pi32, pi32, ppi8, pi64, pi32, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_kalloc",
                     {pi64, pi32, pi32, pi32, ppi8, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_calloc",
                     {pi32, pi32, pi32, pi32, ppi8, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_calloc03",
                     {pi32, pi32, pi32, pi32, ppi8, pi64, pi32, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_calloc04",
                     {pi32, pi32, pi32, pi32, ppi8, pi64, pi32, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_calloc04a",
                     {pi32, pi32, pi32, pi32, ppi8, pi64, pi32, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_kcalloc",
                     {pi64, pi32, pi32, pi32, ppi8, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_ptr_alloc",
                     {pi32, pi32, pi32, pi32, ppi8, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_ptr_alloc03",
                     {pi32, pi32, pi32, pi32, ppi8, pi64, pi32, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_ptr_alloc04",
                     {pi64, pi32, pi32, pi32, ppi8, pi64, pi32, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_ptr_alloc04a",
                     {pi64, pi32, pi32, pi32, ppi8, pi64, pi32, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_ptr_src_alloc03",
                     {pdesc, pi32, pi32, pi32, pi32, ppi8, pi64, pi32, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_ptr_src_calloc03",
                     {pdesc, pi32, pi32, pi32, pi32, ppi8, pi64, pi32, pi8},
                     true);
  changed
      |= process(module,
                 vd,
                 "f90_ptr_src_alloc04",
                 {pdesc, pi64, pi32, pi32, pi32, ppi8, pi64, pi32, pi64, pi8},
                 true);
  changed
      |= process(module,
                 vd,
                 "f90_ptr_src_alloc04a",
                 {pdesc, pi64, pi32, pi32, pi32, ppi8, pi64, pi32, pi64, pi8},
                 true);
  changed
      |= process(module,
                 vd,
                 "f90_ptr_src_calloc04",
                 {pdesc, pi64, pi32, pi32, pi32, ppi8, pi64, pi32, pi64, pi8},
                 true);
  changed
      |= process(module,
                 vd,
                 "f90_ptr_src_calloc04a",
                 {pdesc, pi64, pi32, pi32, pi32, ppi8, pi64, pi32, pi64, pi8},
                 true);
  changed |= process(module,
                     vd,
                     "f90_ptr_kalloc",
                     {pi64, pi32, pi32, pi32, ppi8, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_ptr_calloc",
                     {pi32, pi32, pi32, pi32, ppi8, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_ptr_calloc03",
                     {pi32, pi32, pi32, pi32, ppi8, pi64, pi32, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_ptr_calloc04",
                     {pi64, pi32, pi32, pi32, ppi8, pi64, pi32, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_ptr_calloc04a",
                     {pi64, pi32, pi32, pi32, ppi8, pi64, pi32, pi64, pi8},
                     true);
  changed |= process(module,
                     vd,
                     "f90_ptr_kcalloc",
                     {pi64, pi32, pi32, pi32, ppi8, pi64, pi8},
                     true);
  changed |= process(module, vd, "ftn_deallocate", {pi8, pi32}, false);
  changed |= process(module, pi8, "ftn_dealloc", {pi8, pi32}, false);
  changed |= process(module, vd, "f90_dealloc", {pi32, pi8}, true);
  changed |= process(module, vd, "f90_dealloc03", {pi32, pi8, pi32, pi8}, true);
  changed |= process(module, vd, "f90_dealloc_mbr", {pi32, pi8}, true);
  changed
      |= process(module, vd, "f90_dealloc_mbr03", {pi32, pi8, pi32, pi8}, true);
  changed |= process(module, vd, "f90_deallocx", {pi32, ppi8}, false);
  changed |= process(module, pi8, "f90_auto_allocv", {i64}, true);
  changed |= process(module, pi8, "f90_auto_alloc", {pi32, pi32}, false);
  changed |= process(module, pi8, "f90_auto_alloc04", {pi64, pi32}, false);
  changed |= process(module, pi8, "f90_auto_calloc", {pi32, pi32}, false);
  changed |= process(module, pi8, "f90_auto_calloc04", {pi64, pi32}, false);
  changed |= process(module, vd, "f90_auto_dealloc", {pi8}, false);
  changed |= process(module, i32, "f90io_backspace", {pi32, pi32, pi32}, false);
  changed
      |= process(module, i32, "f90io_swbackspace", {pi32, pi32, pi32}, false);
  changed |= process(module, i32, "f90io_close", {pi32, pi32, pi32, pi8}, true);
  changed |= process(
      module, pi8, "fort_comm_start", {ppsked, pi8, pdesc, pi8, pdesc}, false);
  changed |= process(module, vd, "fort_comm_finish", {pi8}, false);
  changed |= process(module,
                     vd,
                     "fort_comm_execute",
                     {ppsked, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module, vd, "fort_comm_free", {pi32}, true);
  changed |= process(
      module, vd, "fort_permute_section", {pi8, pi8, pdesc, pdesc}, true);
  changed |= process(
      module, vd, "fort_copy_section", {pi8, pi8, pdesc, pdesc}, false);
  changed |= process(
      module, psked, "fort_comm_copy", {pi8, pi8, pdesc, pdesc}, false);
  changed
      |= process(module, vd, "fort_transpose", {pi8, pi8, pdesc, pdesc}, false);
  changed |= process(module, vd, "fort_copy_scalar", {pi8, pdesc}, true);
  changed |= process(
      module, vd, "f90_mergec", {pcmplx, pcmplx, pcmplx, pi8, pi32}, false);
  changed |= process(
      module, vd, "f90_mergedc", {pdcmplx, pdcmplx, pdcmplx, pi8, pi32}, false);
  changed |= process(module,
                     vd,
                     "fort_cshifts",
                     {pi8, pi8, pi32, pi32, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_cshiftsc",
                     {pi8, pi8, pi32, pi32, pdesc, pdesc, pdesc, pdesc, i32},
                     true);
  changed |= process(module,
                     vd,
                     "fort_cshift",
                     {pi8, pi8, pi32, pi32, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_cshiftc",
                     {pi8, pi8, pi32, pi32, pdesc, pdesc, pdesc, pdesc, i32},
                     true);
  changed |= process(module, vd, "fort_set_test", {pi32}, false);
  changed |= process(module, vd, "fort_abort", {pi8, pdesc}, true);
  changed |= process(module, vd, "fort_show", {pi8, pdesc}, false);
  changed |= process(module, vd, "f90_show_", {pi8, pdesc}, false);
  changed |= process(module, vd, "fort_describe", {pi8, pdesc}, false);
  changed |= process(module, i32, "ftnio_fmt_read", {pi8, pdesc}, false);
  changed |= process(module, i32, "ftnio_fmt_write", {pi8, pdesc}, false);
  changed |= process(module, i32, "ftnio_ldr", {pi8, pdesc}, false);
  changed |= process(module, i32, "ftnio_ldw", {pi8, pdesc}, false);
  changed |= process(module, i32, "ftnio_usw_read", {pi8, pdesc}, false);
  changed |= process(module, i32, "ftnio_usw_write", {pi8, pdesc}, false);
  changed |= process(module, i32, "ftnio_unf_read", {pi8, pdesc}, false);
  changed |= process(module, i32, "ftnio_unf_write", {pi8, pdesc}, false);
  changed |= process(module, i32, "fort_len", {pi8}, true);
  changed |= process(module, i32, "fort_lenx", {pi8}, true);
  changed |= process(module, i64, "fort_klen", {pi8}, true);
  changed |= process(module, i64, "fort_klenx", {pi8}, true);
  changed |= process(module, i32, "fort_index", {pi8, pi8, i32}, true);
  changed |= process(module, i32, "fort_indexx", {pi8, pi8, i32}, true);
  changed |= process(module, i64, "fort_kindex", {pi8, pi8, i32}, true);
  changed |= process(module, i64, "fort_kindexx", {pi8, pi8, i32}, true);
  changed |= process(module, i32, "fort_owner", {pdesc}, true);
  changed
      |= process(module, i32, "fort_islocal_idx", {pdesc, pi32, pi32}, false);
  changed |= process(module, i32, "fort_islocal", {pdesc}, true);
  changed |= process(
      module, vd, "fort_localize_dim", {pdesc, pi32, pi32, pi32, pi32}, false);
  changed |= process(
      module, i32, "fort_localize_index", {pdesc, pi32, pi32}, false);
  changed
      |= process(module,
                 vd,
                 "fort_cyclic_loop",
                 {pdesc, pi32, pi32, pi32, pi32, pi32, pi32, pi32, pi32, pi32},
                 false);
  changed |= process(module,
                     vd,
                     "fort_block_loop",
                     {pdesc, pi32, pi32, pi32, pi32, pi32, pi32, pi32},
                     false);
  changed |= process(module,
                     vd,
                     "fort_localize_bounds",
                     {pdesc, pi32, pi32, pi32, pi32, pi32, pi32},
                     false);
  changed |= process(module, vd, "fort_process", {pproc, pi32}, true);
  changed |= process(module, vd, "fort_sect", {pdesc, pdesc}, true);
  changed |= process(module, vd, "f90_sect", {pdesc, pdesc, pi32}, true);
  changed |= process(module,
                     vd,
                     "f90_sect1",
                     {pdesc, pdesc, pi32, pi32, pi32, pi32, pi32},
                     false);
  changed |= process(
      module, vd, "f90_sect1v", {pdesc, pdesc, pi32, i32, i32, i32}, true);
  changed
      |= process(module,
                 vd,
                 "f90_sect2",
                 {pdesc, pdesc, pi32, pi32, pi32, pi32, pi32, pi32, pi32, pi32},
                 false);
  changed |= process(module,
                     vd,
                     "f90_sect2v",
                     {pdesc, pdesc, pi32, i32, i32, i32, i32, i32, i32},
                     true);
  changed |= process(module,
                     vd,
                     "f90_sect3",
                     {pdesc,
                      pdesc,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32},
                     false);
  changed |= process(
      module,
      vd,
      "f90_sect3v",
      {pdesc, pdesc, pi32, i32, i32, i32, i32, i32, i32, i32, i32, i32},
      true);
  changed |= process(module,
                     vd,
                     "fort_sect3",
                     {pdesc,
                      pdesc,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32},
                     false);
  changed
      |= process(module,
                 vd,
                 "fort_sect3v",
                 {pdesc, pdesc, i32, i32, i32, i32, i32, i32, i32, i32, i32},
                 true);
  changed |= process(module, i32, "fort_extent", {pdesc, pi32}, false);
  changed |= process(module, i32, "fort_glextent", {pdesc, pi32, pi32}, false);
  changed |= process(module, i32, "fort_lbound", {pi32, pdesc}, false);
  changed |= process(module, i64, "fort_klbound", {pi32, pdesc}, false);
  changed |= process(module, i32, "fort_ubound", {pi32, pdesc}, false);
  changed |= process(module, i64, "fort_kubound", {pi32, pdesc}, false);
  changed |= process(module, vd, "fort_lbounda", {pi32, pdesc}, false);
  changed |= process(module, vd, "fort_lbounda1", {pi8, pdesc}, false);
  changed |= process(module, vd, "fort_lbounda2", {pi16, pdesc}, false);
  changed |= process(module, vd, "fort_lbounda4", {pi32, pdesc}, false);
  changed |= process(module, vd, "fort_lbounda8", {pi64, pdesc}, false);
  changed |= process(module, vd, "fort_lboundaz", {pi32, pdesc}, false);
  changed |= process(module, vd, "fort_lboundaz1", {pi8, pdesc}, false);
  changed |= process(module, vd, "fort_lboundaz2", {pi16, pdesc}, false);
  changed |= process(module, vd, "fort_lboundaz4", {pi32, pdesc}, false);
  changed |= process(module, vd, "fort_lboundaz8", {pi64, pdesc}, false);
  changed |= process(module, vd, "fort_klbounda", {pi32, pdesc}, false);
  changed |= process(module, vd, "fort_klboundaz", {pi64, pdesc}, false);
  changed |= process(module, vd, "fort_ubounda", {pi32, pdesc}, false);
  changed |= process(module, vd, "fort_ubounda1", {pi8, pdesc}, false);
  changed |= process(module, vd, "fort_ubounda2", {pi16, pdesc}, false);
  changed |= process(module, vd, "fort_ubounda4", {pi32, pdesc}, false);
  changed |= process(module, vd, "fort_ubounda8", {pi64, pdesc}, false);
  changed |= process(module, vd, "fort_uboundaz", {pi32, pdesc}, false);
  changed |= process(module, vd, "fort_uboundaz1", {pi8, pdesc}, false);
  changed |= process(module, vd, "fort_uboundaz2", {pi16, pdesc}, false);
  changed |= process(module, vd, "fort_uboundaz4", {pi32, pdesc}, false);
  changed |= process(module, vd, "fort_uboundaz8", {pi64, pdesc}, false);
  changed |= process(module, vd, "fort_kubounda", {pi32, pdesc}, false);
  changed |= process(module, vd, "fort_kuboundaz", {pi64, pdesc}, false);
  changed |= process(module, i32, "fort_size", {pi32, pdesc}, false);
  changed |= process(module, i64, "fort_ksize", {pi32, pdesc}, false);
  changed |= process(module, vd, "fort_shape", {pi32, pdesc}, false);
  changed |= process(module, vd, "fort_kshape", {pi64, pdesc}, false);
  changed |= process(
      module, vd, "fort_realign", {pdesc, pi32, pi32, pdesc, pi32}, true);
  changed
      |= process(module, vd, "fort_redistribute", {pdesc, pi32, pi32}, true);
  changed |= process(module, i32, "f90io_encode_fmt", {pi32, pi32, pi8}, true);
  changed |= process(module, i32, "f90io_encode_fmtv", {ppi8}, false);
  changed |= process(module, i32, "f90io_endfile", {pi32, pi32, pi32}, false);
  changed |= process(
      module, vd, "fort_function_entry", {pi32, pi32, pi8, pi8, i32}, true);
  changed |= process(module, vd, "fort_tracecall", {pi8}, true);
  changed |= process(module,
                     vd,
                     "fort_eoshiftsz",
                     {pi8, pi8, pi32, pi32, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_eoshiftszc",
                     {pi8, pi8, pi32, pi32, pdesc, pdesc, pdesc, pdesc, i32},
                     true);
  changed
      |= process(module,
                 vd,
                 "fort_eoshiftss",
                 {pi8, pi8, pi32, pi32, pi8, pdesc, pdesc, pdesc, pdesc, pdesc},
                 false);
  changed |= process(
      module,
      vd,
      "fort_eoshiftssc",
      {pi8, pi8, pi32, pi32, pi8, pdesc, pdesc, pdesc, pdesc, pdesc, i32, i32},
      true);
  changed
      |= process(module,
                 vd,
                 "fort_eoshiftsa",
                 {pi8, pi8, pi32, pi32, pi8, pdesc, pdesc, pdesc, pdesc, pdesc},
                 false);
  changed |= process(
      module,
      vd,
      "fort_eoshiftsac",
      {pi8, pi8, pi32, pi32, pi8, pdesc, pdesc, pdesc, pdesc, pdesc, i32, i32},
      true);
  changed |= process(module,
                     vd,
                     "fort_eoshiftz",
                     {pi8, pi8, pi32, pi32, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_eoshiftzc",
                     {pi8, pi8, pi32, pi32, pdesc, pdesc, pdesc, pdesc, i32},
                     true);
  changed
      |= process(module,
                 vd,
                 "fort_eoshifts",
                 {pi8, pi8, pi32, pi32, pi8, pdesc, pdesc, pdesc, pdesc, pdesc},
                 false);
  changed |= process(
      module,
      vd,
      "fort_eoshiftsc",
      {pi8, pi8, pi32, pi32, pi8, pdesc, pdesc, pdesc, pdesc, pdesc, i32, i32},
      true);
  changed
      |= process(module,
                 vd,
                 "fort_eoshift",
                 {pi8, pi8, pi32, pi32, pi8, pdesc, pdesc, pdesc, pdesc, pdesc},
                 false);
  changed |= process(
      module,
      vd,
      "fort_eoshiftc",
      {pi8, pi8, pi32, pi32, pi8, pdesc, pdesc, pdesc, pdesc, pdesc, i32, i32},
      true);
  changed |= process(module, vd, "f90io_src_info03", {pi32, pi8}, true);
  changed |= process(module, vd, "f90io_src_infox03", {i32, pi8}, true);
  changed |= process(module, vd, "f90io_src_info", {pi32, pi8}, true);
  changed |= process(module, vd, "f90io_src_infox", {i32, pi8}, true);
  changed |= process(module, vd, "f90io_iomsg", {pi8}, true);
  changed |= process(module, vd, "f90io_iomsg_", {pi8}, true);
  changed |= process(module, vd, "f90io_aux_init", {i32}, true);
  changed |= process(module, i32, "f90io_flush", {pi32, pi32, pi32}, false);
  changed |= process(module,
                     i32,
                     "f90io_fmtr_init",
                     {pi32, pi32, pi32, pi32, pi32, pi32, pi8},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_fmtr_init2003",
                     {pi32, pi32, pi32, pi32, pi32, pi64, pi8},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_fmtr_init03",
                     {pi32, pi8, pi8, pi8, pi8, i32, i32, i32},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_fmtr_initv",
                     {pi32, pi32, pi32, pi32, ppi32, pi32, pi8},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_fmtr_initv2003",
                     {pi32, pi32, pi32, pi32, ppi32, pi64, pi8},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_fmtr_intern_init",
                     {pi8, pi32, pi32, pi32, pi32},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_fmtr_intern_initv",
                     {pi8, pi32, pi32, pi32, ppi32},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_fmtr_intern_inite",
                     {ppi8, pi32, pi32, pi32, pi32, pi32},
                     false);
  changed |= process(module,
                     i32,
                     "f90io_fmtr_intern_initev",
                     {ppi8, pi32, pi32, pi32, ppi32, pi32},
                     false);
  changed
      |= process(module, i32, "f90io_fmt_read", {pi32, pi32, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_fmt_read_a", {pi32, pi32, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_fmt_read64_a", {pi32, pi64, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_dts_fmtr", {ppi8, ppi8, pi32, pdesc, pi32}, false);
  changed |= process(module,
                     i32,
                     "f90io_fmtw_init",
                     {pi32, pi32, pi32, pi32, pi32, pi8},
                     true);
  changed |= process(
      module, i32, "f90io_fmtw_init03", {pi32, pi8, pi8, pi8, i32, i32}, true);
  changed |= process(module,
                     i32,
                     "f90io_fmtw_initv",
                     {pi32, pi32, pi32, pi32, ppi32, pi8},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_fmtw_intern_init",
                     {pi8, pi32, pi32, pi32, pi32},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_fmtw_intern_initv",
                     {pi8, pi32, pi32, pi32, ppi32},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_fmtw_intern_inite",
                     {ppi8, pi32, pi32, pi32, pi32, pi32},
                     false);
  changed |= process(module,
                     i32,
                     "f90io_fmtw_intern_initev",
                     {ppi8, pi32, pi32, pi32, ppi32, pi32},
                     false);
  changed
      |= process(module, i32, "f90io_fmt_write", {pi32, pi32, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_fmt_write_a", {pi32, pi32, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_fmt_write64_a", {pi32, pi64, pi32, pi8}, true);
  changed |= process(module, i32, "f90io_sc_fmt_write", {i32}, true);
  changed |= process(module, i32, "f90io_sc_i_fmt_write", {i32}, true);
  changed |= process(module, i32, "f90io_sc_l_fmt_write", {i64}, true);
  changed |= process(module, i32, "f90io_sc_f_fmt_write", {f32}, true);
  changed |= process(module, i32, "f90io_sc_d_fmt_write", {f64}, true);
  changed |= process(module, i32, "f90io_sc_cf_fmt_write", {f32, f32}, true);
  changed |= process(module, i32, "f90io_sc_cd_fmt_write", {f64, f64}, true);
  changed |= process(
      module, i32, "f90io_dts_fmtw", {ppi8, ppi8, pi32, pdesc, pi32}, false);
  changed |= process(module, f64, "ftn_i_not64", {f64}, false);
  changed |= process(module, f64, "ftn_i_and64", {f64}, true);
  changed |= process(module, f64, "ftn_i_or64", {f64}, true);
  changed |= process(module, f64, "ftn_i_xor64", {f64}, true);
  changed |= process(module, f64, "ftn_i_xnor64", {f64}, true);
  changed |= process(module, f64, "ftn_i_shift64", {f64}, true);
  changed |= process(module, i32, "ftn_i_dp2ir", {f64}, false);
  changed |= process(module, f32, "ftn_i_dp2sp", {f64}, false);
  changed |= process(module, f64, "ftn_i_ir2dp", {i32}, false);
  changed |= process(module, f64, "ftn_i_sp2dp", {f32}, false);
  changed |= process(module, i32, "ftn_ishftc", {i32, i32}, true);
  changed |= process(module, i32, "ftn_i_iishftc", {i32, i32}, true);
  changed |= process(module, i32, "ftn_i_i1shftc", {i32, i32}, true);
  changed |= process(module, vd, "ftn_jmvbits", {i32, i32, i32, pi32}, true);
  changed |= process(module, vd, "ftn_imvbits", {i32, i32, i32, pi16}, true);
  changed |= process(module, i32, "ftn_jzext", {i32}, true);
  changed |= process(module, i32, "ftn_izext", {i32}, true);
  changed |= process(
      module, vd, "fort_csend", {pi32, pi8, pi32, pi32, pi32}, false);
  changed
      |= process(module, vd, "fort_csendchar", {pi32, pi8, pi32, pi32}, true);
  changed |= process(
      module, vd, "fort_crecv", {pi32, pi8, pi32, pi32, pi32}, false);
  changed
      |= process(module, vd, "fort_crecvchar", {pi32, pi8, pi32, pi32}, true);
  changed |= process(module, vd, "f90_str_copy", {i32, pi8, i32}, true);
  changed |= process(module, vd, "f90_str_cpy1", {pi8, i32, pi8}, true);
  changed |= process(module, i32, "f90_str_index", {pi8, pi8, i32}, true);
  changed |= process(module, i32, "f90_strcmp", {pi8, pi8, i32}, true);
  changed |= process(module, ppi8, "f90_str_malloc", {i32, pppi8}, false);
  changed |= process(module, vd, "f90_str_free", {ppi8}, false);
  changed |= process(module, vd, "f90_str_copy_klen", {i32, pi8, i64}, true);
  changed |= process(module, vd, "f90_str_cpy1_klen", {pi8, i64, pi8}, true);
  changed |= process(module, i32, "f90_str_index_klen", {pi8, pi8, i64}, true);
  changed |= process(module, i32, "f90_strcmp_klen", {pi8, pi8, i64}, true);
  changed |= process(module, ppi8, "f90_str_malloc_klen", {i64, pppi8}, false);
  changed |= process(module, vd, "f90_exit", {pi32}, false);
  changed |= process(module, vd, "f90_stop08", {pi32, pi8}, true);
  changed |= process(module, vd, "f90_stop", {pi8}, true);
  changed |= process(module, vd, "f90_pause", {pi8}, true);
  changed |= process(module, i64, "ftn_i_kishftc", {i64, i32}, true);
  changed |= process(module, vd, "ftn_kmvbits", {pi32, i32, i32, pi32}, true);
  changed |= process(module, i64, "ftn_i_kibclr", {i32, i32}, true);
  changed |= process(module, i64, "ftn_i_kibits", {i32, i32, i32}, true);
  changed |= process(module, i64, "ftn_i_kibset", {i32, i32}, true);
  changed |= process(module, i64, "ftn_i_bktest", {i32, i32}, true);
  changed |= process(module, i64, "ftn_i_kishft", {i64}, true);
  changed |= process(module, i64, "ftn_i_xori64", {i32, i32, i32}, true);
  changed |= process(module, i64, "ftn_i_xnori64", {i32, i32, i32}, true);
  changed |= process(module, i32, "ftn_i_kr2ir", {i32}, true);
  changed |= process(module, f32, "ftn_i_kr2sp", {i32}, true);
  changed |= process(module, f64, "ftn_i_kr2dp", {i32}, true);
  changed |= process(module, i64, "ftn_str_kindex", {pi8, pi8, i32}, true);
  changed |= process(module, i64, "ftn_str_kindex_klen", {pi8, pi8, i64}, true);
  changed |= process(module, vd, "ftn_date", {pi8}, true);
  changed |= process(module, vd, "ftn_datew", {pi8}, false);
  changed |= process(module, vd, "ftn_jdate", {pi32, pi32, pi32}, false);
  changed |= process(module, vd, "ftn_idate", {pi16, pi16, pi16}, false);
  changed |= process(module, f32, "ftn_secnds", {f32}, false);
  changed |= process(module, f64, "ftn_dsecnds", {f64}, false);
  changed |= process(module, vd, "ftn_time", {pi8}, true);
  changed |= process(module, vd, "ftn_timew", {pi8}, false);
  changed |= process(module, f32, "ftn_ran", {pi32}, false);
  changed |= process(module, f64, "ftn_dran", {pi32}, false);
  changed |= process(module, vd, "f90_nstr_copy", {i32, pi16, i32}, true);
  changed |= process(module, i32, "f90_nstr_index", {pi16, pi16, i32}, true);
  changed |= process(module, i32, "f90_nstrcmp", {pi16, pi16, i32}, true);
  changed |= process(module, vd, "f90_nstr_copy_klen", {i32, pi16, i64}, true);
  changed
      |= process(module, i64, "f90_nstr_index_klen", {pi16, pi16, i64}, true);
  changed |= process(module, i32, "f90_nstrcmp_klen", {pi16, pi16, i64}, true);
  changed |= process(module, i32, "fullpathqq_", {pi8, pi8, i32}, true);
  changed |= process(
      module, vd, "f90_get_cmd_arg", {pi32, pi8, pi32, pi32, pi32}, true);
  changed |= process(module, vd, "f90_get_cmd", {pi8, pi32, pi32, pi32}, true);
  changed |= process(module,
                     vd,
                     "f90_get_env_var",
                     {pi8, pi8, pi32, pi32, pi32, pi32, i32},
                     true);
  changed |= process(module, i32, "ftn_i_i1shft", {i32}, true);
  changed |= process(module, i32, "ftn_i_iishft", {i32}, true);
  changed |= process(module, i32, "fort_tid", {pi32}, false);
  changed |= process(module, vd, "fort_init", {pi32}, false);
  changed |= process(module,
                     i32,
                     "f90io_inquire",
                     {pi32, pi8, pi32, pi32, pi32, pi32, pi32, pi32, pi8, pi8,
                      pi8,  pi8, pi8,  pi8,  pi8,  pi32, pi32, pi8,  pi8, pi8,
                      pi8,  pi8, pi8,  pi8,  pi8,  i32,  i32,  i32,  i32, i32,
                      i32,  i32, i32,  i32,  i32,  i32,  i32,  i32,  i32, i32},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_inquire2003",
                     {pi32, pi8, pi32, pi32, pi32, pi32, pi64, pi32, pi8, pi8,
                      pi8,  pi8, pi8,  pi8,  pi8,  pi64, pi64, pi8,  pi8, pi8,
                      pi8,  pi8, pi8,  pi8,  pi8,  i32,  i32,  i32,  i32, i32,
                      i32,  i32, i32,  i32,  i32,  i32,  i32,  i32,  i32, i32},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_inquire2",
                     {pi32,
                      pi32,
                      pi32,
                      pi64,
                      pi32,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      i32,
                      i32,
                      i32,
                      i32},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_inquire03",
                     {pi32, pi8, pi32, pi32, pi32, pi32, pi32, pi32, pi8,  pi8,
                      pi8,  pi8, pi8,  pi8,  pi8,  pi32, pi32, pi8,  pi8,  pi8,
                      pi8,  pi8, pi8,  pi8,  pi8,  pi32, pi32, pi64, pi32, pi8,
                      pi8,  pi8, pi8,  pi8,  pi8,  i32,  i32,  i32,  i32,  i32,
                      i32,  i32, i32,  i32,  i32,  i32,  i32,  i32,  i32,  i32,
                      i32,  i32, i32,  i32,  i32,  i32},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_inquire03_2",
                     {pi32, pi8, pi32, pi32, pi32, pi32, pi64, pi32, pi8,  pi8,
                      pi8,  pi8, pi8,  pi8,  pi8,  pi64, pi64, pi8,  pi8,  pi8,
                      pi8,  pi8, pi8,  pi8,  pi8,  pi32, pi32, pi64, pi64, pi8,
                      pi8,  pi8, pi8,  pi8,  pi8,  i32,  i32,  i32,  i32,  i32,
                      i32,  i32, i32,  i32,  i32,  i32,  i32,  i32,  i32,  i32,
                      i32,  i32, i32,  i32,  i32,  i32},
                     true);
  changed |= process(module, i32, "ftn_i_jishft", {i32}, true);
  changed |= process(module, i32, "ftn_i_shift", {i32}, true);
  changed |= process(module, f32, "ftn_i_rmin", {f32}, true);
  changed |= process(module, f32, "ftn_i_rmax", {f32}, true);
  changed |= process(module, f64, "ftn_i_dmax", {f64}, true);
  changed |= process(module, f64, "ftn_i_dmin", {f64}, true);
  changed |= process(module, i32, "ftn_i_isign", {i32}, true);
  changed |= process(module, f32, "ftn_i_sign", {f32}, true);
  changed |= process(module, f64, "ftn_i_dsign", {f64}, true);
  changed |= process(module, f32, "ftn_i_dim", {f32}, true);
  changed |= process(module, i32, "ftn_i_idim", {i32}, true);
  changed |= process(module, f64, "ftn_i_ddim", {f64}, true);
  changed |= process(module, i64, "ftn_i_kabs", {i64}, false);
  changed |= process(module, i64, "ftn_i_kidim", {i64}, true);
  changed |= process(module, i64, "ftn_i_kisign", {i64}, true);
  changed |= process(module, i64, "ftn_i_kmax", {i64}, true);
  changed |= process(module, i64, "ftn_i_kmin", {i64}, true);
  changed |= process(
      module, i32, "f90io_ldr_init", {pi32, pi32, pi32, pi32}, false);
  changed |= process(module,
                     i32,
                     "f90io_ldr_init03",
                     {pi32, pi8, pi8, pi8, pi8, i32, i32, i32},
                     true);
  changed |= process(
      module, i32, "f90io_ldr_intern_init", {pi8, pi32, pi32, pi32}, true);
  changed |= process(module,
                     i32,
                     "f90io_ldr_intern_inite",
                     {ppi8, pi32, pi32, pi32, pi32},
                     false);
  changed |= process(module, i32, "f90io_ldr", {pi32, pi32, pi32, pi8}, true);
  changed |= process(module, i32, "f90io_ldr_a", {pi32, pi32, pi32, pi8}, true);
  changed
      |= process(module, i32, "f90io_ldr64_a", {pi32, pi64, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_ldw_init", {pi32, pi32, pi32, pi32}, false);
  changed |= process(
      module, i32, "f90io_print_init", {pi32, pi32, pi32, pi32}, false);
  changed |= process(
      module, i32, "f90io_ldw_init03", {pi32, pi8, pi8, pi8, i32, i32}, true);
  changed |= process(
      module, i32, "f90io_ldw_intern_init", {pi8, pi32, pi32, pi32}, true);
  changed |= process(module,
                     i32,
                     "f90io_ldw_intern_inite",
                     {ppi8, pi32, pi32, pi32, pi32},
                     false);
  changed |= process(module, i32, "f90io_ldw", {pi32, pi32, pi32, pi8}, true);
  changed |= process(module, i32, "f90io_ldw_a", {pi32, pi32, pi32, pi8}, true);
  changed
      |= process(module, i32, "f90io_ldw64_a", {pi32, pi64, pi32, pi8}, true);
  changed |= process(module, i32, "f90io_sc_ldw", {i32}, true);
  changed |= process(module, i32, "f90io_sc_i_ldw", {i32}, true);
  changed |= process(module, i32, "f90io_sc_l_ldw", {i64}, true);
  changed |= process(module, i32, "f90io_sc_f_ldw", {f32}, true);
  changed |= process(module, i32, "f90io_sc_d_ldw", {f64}, true);
  changed |= process(module, i32, "f90io_sc_cf_ldw", {f32, f32}, true);
  changed |= process(module, i32, "f90io_sc_cd_ldw", {f64, f64}, true);
  changed |= process(module, i32, "f90io_sc_ch_ldw", {pi8, i32}, true);
  changed |= process(module, vd, "fort_mget_scalar", {pi32}, true);
  changed |= process(module, i32, "fort_ilen", {pi8, pi32}, false);
  changed |= process(module, i32, "f90_present", {pi8}, false);
  changed |= process(module, i32, "f90_present_ptr", {pi8}, false);
  changed |= process(module, i32, "f90_presentc", {pi8}, true);
  changed |= process(module, i64, "f90_kpresent", {pi8}, false);
  changed |= process(module, i64, "f90_kpresent_ptr", {pi8}, false);
  changed |= process(module, i64, "f90_kpresentc", {pi8}, true);
  changed |= process(module, i32, "f90_is_iostat_end", {i32}, false);
  changed |= process(module, i64, "f90_kis_iostat_end", {i32}, false);
  changed |= process(module, i32, "f90_is_iostat_eor", {i32}, false);
  changed |= process(module, i64, "f90_kis_iostat_eor", {i32}, false);
  changed |= process(module, pi8, "f90_loc", {pi8}, false);
  changed |= process(module, i32, "f90_imax", {i32}, true);
  changed |= process(module, vd, "f90_min", {pi32}, true);
  changed |= process(module, vd, "f90_max", {pi32}, true);
  changed |= process(module, i64, "f90_kichar", {pi8}, true);
  changed |= process(module, i32, "f90_len", {pi8}, true);
  changed |= process(module, i64, "f90_klen", {pi8}, true);
  changed |= process(module, i32, "f90_nlen", {pi8}, true);
  changed |= process(module, i32, "f90_adjustl", {pi8, pi8, i32}, true);
  changed |= process(module, i32, "f90_adjustr", {pi8, pi8, i32}, true);
  changed |= process(module, vd, "fort_date", {pi8, pdesc}, true);
  changed |= process(module, vd, "fort_datew", {pi8, pdesc}, false);
  changed |= process(
      module, vd, "fort_jdate", {pi32, pi32, pi32, pdesc, pdesc, pdesc}, false);
  changed |= process(
      module, vd, "fort_idate", {pi16, pi16, pi16, pdesc, pdesc, pdesc}, false);
  changed |= process(module, vd, "fort_cpu_time", {pf32}, false);
  changed |= process(module, vd, "fort_cpu_timed", {pf64}, false);
  changed |= process(module, f32, "fort_secnds", {pf32, pdesc}, false);
  changed |= process(module, f64, "fort_secndsd", {pf64, pdesc}, false);
  changed |= process(module, vd, "fort_ftime", {pi8, pdesc}, true);
  changed |= process(module, vd, "fort_ftimew", {pi8, pdesc}, false);
  changed
      |= process(module,
                 vd,
                 "fort_dandt",
                 {pi8, pi8, pi8, pi32, pdesc, pdesc, pdesc, pdesc, i32, i32},
                 true);
  changed |= process(module,
                     vd,
                     "fort_sysclk",
                     {pi32, pi32, pi32, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_mvbits",
                     {pi8, pi8, pi8, pi8, pi8, pi32, pi32, pi32, pi32},
                     false);
  changed |= process(module, i32, "f90_lb", {pi32, pi32}, true);
  changed |= process(module, i8, "f90_lb1", {pi32, pi32}, true);
  changed |= process(module, i16, "f90_lb2", {pi32, pi32}, true);
  changed |= process(module, i32, "f90_lb4", {pi32, pi32}, true);
  changed |= process(module, i64, "f90_lb8", {pi32, pi32}, true);
  changed |= process(module, i64, "f90_klb", {pi32, pi32}, true);
  changed |= process(module, i32, "f90_ub", {pi32, pi32}, true);
  changed |= process(module, i8, "f90_ub1", {pi32, pi32}, true);
  changed |= process(module, i16, "f90_ub2", {pi32, pi32}, true);
  changed |= process(module, i32, "f90_ub4", {pi32, pi32}, true);
  changed |= process(module, i64, "f90_ub8", {pi32, pi32}, true);
  changed |= process(module, i64, "f90_kub", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_lba", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_lba1", {pi8, pi32}, true);
  changed |= process(module, vd, "f90_lba2", {pi16, pi32}, true);
  changed |= process(module, vd, "f90_lba4", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_lba8", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_klba", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_uba", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_uba1", {pi8, pi32}, true);
  changed |= process(module, vd, "f90_uba2", {pi16, pi32}, true);
  changed |= process(module, vd, "f90_uba4", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_uba8", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_kuba", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_lbaz", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_lbaz1", {pi8, pi32}, true);
  changed |= process(module, vd, "f90_lbaz2", {pi16, pi32}, true);
  changed |= process(module, vd, "f90_lbaz4", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_lbaz8", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_klbaz", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_ubaz", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_ubaz1", {pi8, pi32}, true);
  changed |= process(module, vd, "f90_ubaz2", {pi16, pi32}, true);
  changed |= process(module, vd, "f90_ubaz4", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_ubaz8", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_kubaz", {pi64, pi32}, true);
  changed |= process(module, i32, "f90_lbound", {pi32, pi32}, true);
  changed |= process(module, i8, "f90_lbound1", {pi32, pi32}, true);
  changed |= process(module, i16, "f90_lbound2", {pi32, pi32}, true);
  changed |= process(module, i32, "f90_lbound4", {pi32, pi32}, true);
  changed |= process(module, i64, "f90_lbound8", {pi32, pi32}, true);
  changed |= process(module, i64, "f90_klbound", {pi32, pi32}, true);
  changed |= process(module, i32, "f90_ubound", {pi32, pi32}, true);
  changed |= process(module, i8, "f90_ubound1", {pi32, pi32}, true);
  changed |= process(module, i16, "f90_ubound2", {pi32, pi32}, true);
  changed |= process(module, i32, "f90_ubound4", {pi32, pi32}, true);
  changed |= process(module, i64, "f90_ubound8", {pi32, pi32}, true);
  changed |= process(module, i64, "f90_kubound", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_lbounda", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_lbounda1", {pi8, pi32}, true);
  changed |= process(module, vd, "f90_lbounda2", {pi16, pi32}, true);
  changed |= process(module, vd, "f90_lbounda4", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_lbounda8", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_klbounda", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_ubounda", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_ubounda1", {pi8, pi32}, true);
  changed |= process(module, vd, "f90_ubounda2", {pi16, pi32}, true);
  changed |= process(module, vd, "f90_ubounda4", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_ubounda8", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_kubounda", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_lboundaz", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_lboundaz1", {pi8, pi32}, true);
  changed |= process(module, vd, "f90_lboundaz2", {pi16, pi32}, true);
  changed |= process(module, vd, "f90_lboundaz4", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_lboundaz8", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_klboundaz", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_uboundaz", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_uboundaz1", {pi8, pi32}, true);
  changed |= process(module, vd, "f90_uboundaz2", {pi16, pi32}, true);
  changed |= process(module, vd, "f90_uboundaz4", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_uboundaz8", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_kuboundaz", {pi64, pi32}, true);
  changed |= process(module, i32, "f90_size", {pi32, pi32}, true);
  changed |= process(module, i64, "f90_ksize", {pi32, pi32}, true);
  changed |= process(module, i64, "f90_class_obj_size", {pdesc}, false);
  changed |= process(module, vd, "f90_shape", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_shape1", {pi8, pi32}, true);
  changed |= process(module, vd, "f90_shape2", {pi16, pi32}, true);
  changed |= process(module, vd, "f90_shape4", {pi32, pi32}, true);
  changed |= process(module, vd, "f90_shape8", {pi64, pi32}, true);
  changed |= process(module, vd, "f90_kshape", {pi64, pi32}, true);
  changed |= process(module, i32, "f90_achar", {pi8, pi8, pi32}, true);
  changed
      |= process(module, i32, "f90_repeat", {pi8, pi8, pi8, pi32, i32}, true);
  changed |= process(module, i32, "f90_trim", {pi8, pi8, i32}, true);
  changed |= process(module, i32, "f90_iachar", {pi8}, true);
  changed |= process(module, i64, "f90_kiachar", {pi8}, true);
  changed |= process(
      module, vd, "f90_mergech", {pi8, pi8, pi8, pi8, pi32, i32, i32}, true);
  changed |= process(
      module, vd, "f90_mergedt", {pi8, pi8, pi8, pi32, pi8, pi32}, false);
  changed |= process(module, i8, "f90_mergei1", {pi8, pi8, pi8, pi32}, false);
  changed
      |= process(module, i16, "f90_mergei2", {pi16, pi16, pi8, pi32}, false);
  changed |= process(module, i32, "f90_mergei", {pi32, pi32, pi8, pi32}, false);
  changed
      |= process(module, i64, "f90_mergei8", {pi64, pi64, pi8, pi32}, false);
  changed |= process(module, i8, "f90_mergel1", {pi8, pi8, pi8, pi32}, false);
  changed
      |= process(module, i16, "f90_mergel2", {pi16, pi16, pi8, pi32}, false);
  changed |= process(module, i32, "f90_mergel", {pi32, pi32, pi8, pi32}, false);
  changed
      |= process(module, i64, "f90_mergel8", {pi64, pi64, pi8, pi32}, false);
  changed |= process(module, f32, "f90_merger", {pf32, pf32, pi8, pi32}, false);
  changed |= process(module, f64, "f90_merged", {pf64, pf64, pi8, pi32}, false);
  changed |= process(module, f64, "f90_mergeq", {pf64, pf64, pi8, pi32}, false);
  changed |= process(module, i32, "f90_lentrim", {pi8}, true);
  changed |= process(module, i64, "f90_klentrim", {pi8}, true);
  changed |= process(module, i32, "f90_scan", {pi8, pi8, pi8, pi32, i32}, true);
  changed
      |= process(module, i64, "f90_kscan", {pi8, pi8, pi8, pi32, i32}, true);
  changed
      |= process(module, i32, "f90_verify", {pi8, pi8, pi8, pi32, i32}, true);
  changed
      |= process(module, i64, "f90_kverify", {pi8, pi8, pi8, pi32, i32}, true);
  changed
      |= process(module, i64, "f90_kindex", {pi8, pi8, pi8, pi32, i32}, true);
  changed
      |= process(module, i32, "f90_index", {pi8, pi8, pi8, pi32, i32}, true);
  changed |= process(module, i32, "fort_leadz", {pi8, pi32}, false);
  changed |= process(module, i32, "fort_popcnt", {pi8, pi32}, false);
  changed |= process(module, i32, "fort_poppar", {pi8, pi32}, false);
  changed |= process(module, i32, "f90_jmax0", {pi32, pi32}, false);
  changed |= process(module, i32, "f90_max0", {pi32, pi32}, false);
  changed |= process(module, i64, "f90_kmax", {pi64, pi64}, false);
  changed |= process(module, i32, "f90_min0", {pi32, pi32}, false);
  changed |= process(module, i32, "f90_mod", {pi32, pi32}, false);
  changed |= process(module, i32, "f90_int", {pi8, pi32}, false);
  changed |= process(module, i8, "f90_int1", {pi8, pi32}, false);
  changed |= process(module, i16, "f90_int2", {pi8, pi32}, false);
  changed |= process(module, i32, "f90_int4", {pi8, pi32}, false);
  changed |= process(module, i64, "f90_int8", {pi8, pi32}, false);
  changed |= process(module, i8, "f90_log1", {pi8, pi32}, false);
  changed |= process(module, i16, "f90_log2", {pi8, pi32}, false);
  changed |= process(module, i32, "f90_log4", {pi8, pi32}, false);
  changed |= process(module, i64, "f90_log8", {pi8, pi32}, false);
  changed |= process(module, f32, "f90_real", {pi8, pi32}, false);
  changed |= process(module, f64, "f90_dble", {pi8, pi32}, false);
  changed |= process(module, f32, "f90_real4", {pi8, pi32}, false);
  changed |= process(module, f64, "f90_real8", {pi8, pi32}, false);
  changed |= process(module, f32, "f90_amax1", {pf32, pf32}, false);
  changed |= process(module, f64, "f90_dmax1", {pf64, pf64}, false);
  changed |= process(module, f32, "f90_amin1", {pf32, pf32}, false);
  changed |= process(module, f64, "f90_dmin1", {pf64, pf64}, false);
  changed |= process(module, f32, "f90_amod", {pf32, pf32}, false);
  changed |= process(module, f64, "f90_dmod", {pf64, pf64}, false);
  changed |= process(module, i32, "f90_modulo", {pi32, pi32}, false);
  changed |= process(module, i64, "f90_i8modulo", {pi64, pi64}, false);
  changed |= process(module, i16, "f90_imodulo", {pi16, pi16}, false);
  changed |= process(module, f32, "f90_amodulo", {pf32, pf32}, false);
  changed |= process(module, f64, "f90_dmodulo", {pf64, pf64}, false);
  changed |= process(module, i32, "f90_modulov", {i32}, true);
  changed |= process(module, i64, "f90_i8modulov", {i64}, true);
  changed |= process(module, i16, "f90_imodulov", {i16, i16}, false);
  changed |= process(module, f32, "f90_amodulov", {f32}, true);
  changed |= process(module, f64, "f90_dmodulov", {f64}, true);
  changed |= process(module, i32, "f90_ceiling", {pf32}, false);
  changed |= process(module, i64, "f90_kceiling", {pf32}, false);
  changed |= process(module, i32, "f90_dceiling", {pf64}, false);
  changed |= process(module, i64, "f90_kdceiling", {pf64}, false);
  changed |= process(module, i32, "f90_ceilingv", {f32}, false);
  changed |= process(module, i64, "f90_kceilingv", {f32}, false);
  changed |= process(module, i32, "f90_dceilingv", {f64}, false);
  changed |= process(module, i64, "f90_kdceilingv", {f64}, false);
  changed |= process(module, i32, "f90_floor", {pf32}, false);
  changed |= process(module, i32, "f90_kfloor", {pf32}, false);
  changed |= process(module, i32, "f90_dfloor", {pf64}, false);
  changed |= process(module, i64, "f90_kdfloor", {pf64}, false);
  changed |= process(module, i32, "f90_floorv", {f32}, false);
  changed |= process(module, i32, "f90_kfloorv", {f32}, false);
  changed |= process(module, i32, "f90_dfloorv", {f64}, false);
  changed |= process(module, i64, "f90_kdfloorv", {f64}, false);
  changed |= process(module, i32, "f90_sel_int_kind", {pi8, pdesc}, false);
  changed |= process(module, i64, "f90_ksel_int_kind", {pi8, pdesc}, false);
  changed |= process(module, i32, "f90_sel_char_kind", {pi8, pdesc}, true);
  changed |= process(module, i64, "f90_ksel_char_kind", {pi8, pdesc}, true);
  changed |= process(
      module, i64, "f90_ksel_real_kind", {pi8, pi8, pdesc, pdesc}, false);
  changed |= process(
      module, i32, "f90_sel_real_kind", {pi8, pi8, pdesc, pdesc}, false);
  changed |= process(module, i32, "f90_exponx", {f32}, false);
  changed |= process(module, i32, "f90_expon", {pf32}, false);
  changed |= process(module, i32, "f90_expondx", {f64}, false);
  changed |= process(module, i32, "f90_expond", {pf64}, false);
  changed |= process(module, i64, "f90_kexponx", {f32}, false);
  changed |= process(module, i64, "f90_kexpon", {pf32}, false);
  changed |= process(module, i64, "f90_kexpondx", {f64}, false);
  changed |= process(module, i64, "f90_kexpond", {pf64}, false);
  changed |= process(module, f32, "f90_fracx", {f32}, false);
  changed |= process(module, f32, "f90_frac", {pf32}, false);
  changed |= process(module, f64, "f90_fracdx", {f64}, false);
  changed |= process(module, f64, "f90_fracd", {pf64}, false);
  changed |= process(module, f32, "f90_nearestx", {f32}, true);
  changed |= process(module, f32, "f90_nearest", {pf32, pi32}, false);
  changed |= process(module, f64, "f90_nearestdx", {f64}, true);
  changed |= process(module, f64, "f90_nearestd", {pf64, pi32}, false);
  changed |= process(module, f32, "f90_rrspacingx", {f32}, false);
  changed |= process(module, f32, "f90_rrspacing", {pf32}, false);
  changed |= process(module, f64, "f90_rrspacingdx", {f64}, false);
  changed |= process(module, f64, "f90_rrspacingd", {pf64}, false);
  changed |= process(module, f32, "f90_scalex", {f32}, true);
  changed |= process(module, f32, "f90_scale", {pf32, pi8, pi32}, false);
  changed |= process(module, f64, "f90_scaledx", {f64}, true);
  changed |= process(module, f64, "f90_scaled", {pf64, pi8, pi32}, false);
  changed |= process(module, f32, "f90_setexpx", {f32}, true);
  changed |= process(module, f32, "f90_setexp", {pf32, pi8, pi32}, false);
  changed |= process(module, f64, "f90_setexpdx", {f64}, true);
  changed |= process(module, f64, "f90_setexpd", {pf64, pi8, pi32}, false);
  changed |= process(module, f32, "f90_spacingx", {f32}, false);
  changed |= process(module, f32, "f90_spacing", {pf32}, false);
  changed |= process(module, f64, "f90_spacingdx", {f64}, false);
  changed |= process(module, f64, "f90_spacingd", {pf64}, false);
  changed |= process(module, vd, "f90_mzero1", {pi8}, true);
  changed |= process(module, vd, "f90_mzero2", {pi8}, true);
  changed |= process(module, vd, "f90_mzero4", {pi8}, true);
  changed |= process(module, vd, "f90_mzero8", {pi8}, true);
  changed |= process(module, vd, "f90_mzeroz8", {pi8}, true);
  changed |= process(module, vd, "f90_mzeroz16", {pi8}, true);
  changed |= process(module, vd, "f90_mset1", {pi8, pi8}, true);
  changed |= process(module, vd, "f90_mset2", {pi8, pi8}, true);
  changed |= process(module, vd, "f90_mset4", {pi8, pi8}, true);
  changed |= process(module, vd, "f90_mset8", {pi8, pi8}, true);
  changed |= process(module, vd, "f90_msetz8", {pi8, pi8}, true);
  changed |= process(module, vd, "f90_msetz16", {pi8, pi8}, true);
  changed |= process(module, vd, "f90_mcopy1", {pi8, pi8}, true);
  changed |= process(module, vd, "f90_mcopy2", {pi8, pi8}, true);
  changed |= process(module, vd, "f90_mcopy4", {pi8, pi8}, true);
  changed |= process(module, vd, "f90_mcopy8", {pi8, pi8}, true);
  changed |= process(module, vd, "f90_mcopyz8", {pi8, pi8}, true);
  changed |= process(module, vd, "f90_mcopyz16", {pi8, pi8}, true);
  changed |= process(module,
                     vd,
                     "f90_mmul_cmplx16",
                     {i32,
                      i32,
                      i64,
                      i64,
                      i64,
                      pdcmplx,
                      pdcmplx,
                      i64,
                      pdcmplx,
                      i64,
                      pdcmplx,
                      pdcmplx},
                     true);
  changed |= process(module,
                     vd,
                     "f90_mmul_cmplx8",
                     {i32,
                      i32,
                      i64,
                      i64,
                      i64,
                      pcmplx,
                      pcmplx,
                      i64,
                      pcmplx,
                      i64,
                      pcmplx,
                      pcmplx},
                     true);
  changed |= process(
      module,
      vd,
      "f90_mmul_real4",
      {i32, i32, i64, i64, i64, pf32, pf32, i64, pf32, i64, pf32, pf32},
      true);
  changed |= process(
      module,
      vd,
      "f90_mmul_real8",
      {i32, i32, i64, i64, i64, pf64, pf64, i64, pf64, i64, pf64, pf64},
      true);
  changed |= process(
      module, vd, "fort_dotpr", {pi8, pi8, pi8, pdesc, pdesc, pdesc}, false);
  changed |= process(
      module, vd, "fort_matmul", {pi8, pi8, pi8, pdesc, pdesc, pdesc}, false);
  changed |= process(module,
                     vd,
                     "f90_matmul_cplx16",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_cplx16mxv_t",
                     {pi8, pi8, pi8, pi32, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_cplx8",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_cplx8mxv_t",
                     {pi8, pi8, pi8, pi32, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_int1",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_int2",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_int4",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_int8",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_log1",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_log2",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_log4",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_log8",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_real4",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_real4mxv_t",
                     {pi8, pi8, pi8, pi32, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_real8",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "f90_matmul_real8mxv_t",
                     {pi8, pi8, pi8, pi32, pdesc, pdesc, pdesc},
                     false);
  changed |= process(
      module, i32, "f90io_nmlr_init", {pi32, pi32, pi32, pi32}, false);
  changed |= process(module,
                     i32,
                     "f90io_nmlr_init03",
                     {pi32, pi8, pi8, pi8, pi8, i32, i32, i32},
                     true);
  changed |= process(
      module, i32, "f90io_nmlr_intern_init", {pi8, pi32, pi32, pi32}, true);
  changed |= process(
      module, i32, "f90io_nml_read", {pi32, pi32, pi32, pnml}, false);
  changed |= process(module, i32, "f90io_nmlr", {pnml}, false);
  changed |= process(
      module, i32, "f90io_nmlw_init", {pi32, pi32, pi32, pi32}, false);
  changed |= process(
      module, i32, "f90io_nmlw_init03", {pi32, pi8, pi8, pi8, i32, i32}, true);
  changed |= process(
      module, i32, "f90io_nmlw_intern_init", {pi8, pi32, pi32, pi32}, true);
  changed |= process(
      module, i32, "f90io_nml_write", {pi32, pi32, pi32, pnml}, false);
  changed |= process(module, i32, "f90io_nmlw", {pnml}, false);
  changed |= process(module, psked, "fort_olap_shift", {pi8, pdesc}, true);
  changed |= process(module, psked, "fort_olap_cshift", {pi8, pdesc}, true);
  changed
      |= process(module, psked, "fort_olap_eoshift", {pi8, pdesc, pi8}, true);
  changed |= process(module, psked, "fort_comm_shift", {pi8, pdesc}, true);
  changed |= process(module,
                     i32,
                     "f90io_open",
                     {pi32, pi32, pi8, pi8,  pi8, pi8, pi8, pi8,
                      pi32, pi8,  pi8, pi32, pi8, pi8, i32, i32,
                      i32,  i32,  i32, i32,  i32, i32, i32},
                     true);
  changed |= process(module,
                     i32,
                     "f90io_open2003",
                     {pi32, pi32, pi8, pi8,  pi8, pi8, pi8, pi8,
                      pi32, pi8,  pi8, pi64, pi8, pi8, i32, i32,
                      i32,  i32,  i32, i32,  i32, i32, i32},
                     true);
  changed |= process(module, i32, "f90io_open_cvt", {pi32, pi8}, true);
  changed |= process(module, i32, "f90io_open_share", {pi32, pi8}, true);
  changed |= process(module, i32, "f90io_open_async", {pi32, pi8}, true);
  changed |= process(module,
                     i32,
                     "f90io_open03",
                     {pi32, pi8, pi8, pi8, pi8, i32, i32, i32},
                     true);
  changed |= process(module,
                     vd,
                     "fort_pack",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_packc",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc, i32, i32},
                     true);
  changed |= process(
      module, vd, "fort_packz", {pi8, pi8, pi8, pdesc, pdesc, pdesc}, false);
  changed |= process(module,
                     vd,
                     "fort_packzc",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc, i32},
                     true);
  changed |= process(module,
                     vd,
                     "fort_unpack",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_unpackc",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc, i32, i32},
                     true);
  changed |= process(module, vd, "fort_nullify", {pi8, pdesc}, false);
  changed |= process(module, vd, "fort_nullify_char", {pi8, pdesc}, true);
  changed |= process(module, vd, "fort_nullifyx", {ppi8, pdesc}, false);
  changed |= process(
      module, vd, "fort_ptr_asgn", {pi8, pdesc, pi8, pdesc, pi32}, false);
  changed |= process(module,
                     vd,
                     "fort_ptr_asgn_char",
                     {pi8, pdesc, pi8, pdesc, pi32, i32},
                     true);
  changed |= process(
      module, vd, "fort_ptr_assign", {pi8, pdesc, pi8, pdesc, pi32}, false);
  changed |= process(module,
                     vd,
                     "fort_ptr_assign_char",
                     {pi8, pdesc, pi8, pdesc, pi32, i32},
                     true);
  changed |= process(module,
                     vd,
                     "fort_ptr_assignx",
                     {pi8, pdesc, pi8, pdesc, pi32, pi32, pi32},
                     false);
  changed |= process(module,
                     vd,
                     "fort_ptr_assign_charx",
                     {pi8, pdesc, pi8, pdesc, pi32, pi32, pi32, i32},
                     true);
  changed |= process(module,
                     vd,
                     "fort_ptr_assign_assumeshp",
                     {pi8, pdesc, pi8, pdesc, pi32},
                     false);
  changed |= process(module,
                     vd,
                     "fort_ptr_assign_char_assumeshp",
                     {pi8, pdesc, pi8, pdesc, pi32, i32},
                     true);
  changed |= process(module, vd, "fort_ptr_fix_assumeshp1", {pdesc}, true);
  changed |= process(module, vd, "fort_ptr_fix_assumeshp2", {pdesc, i32}, true);
  changed |= process(
      module, vd, "fort_ptr_fix_assumeshp3", {pdesc, i32, i32}, true);
  changed |= process(module, vd, "fort_ptr_fix_assumeshp", {pdesc, i32}, true);
  changed |= process(module,
                     vd,
                     "fort_ptr_in",
                     {pi32, pi32, pi32, pi8, pdesc, pi8, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_ptr_in_char",
                     {pi32, pi32, pi32, pi8, pdesc, pi8, pdesc, i32},
                     true);
  changed |= process(
      module, pi8, "fort_ptr_assn", {pi8, pdesc, pi8, pdesc, pi32}, false);
  changed |= process(module,
                     pi8,
                     "fort_ptr_assn_char",
                     {pi8, pdesc, pi8, pdesc, pi32, i32},
                     true);
  changed |= process(module,
                     pi8,
                     "fort_ptr_assn_dchar",
                     {pi8, pdesc, pi8, pdesc, pi32, i32},
                     true);
  changed |= process(module,
                     pi8,
                     "fort_ptr_assnx",
                     {pi8, pdesc, pi8, pdesc, pi32, pi32, pi32},
                     false);
  changed |= process(module,
                     pi8,
                     "fort_ptr_assn_charx",
                     {pi8, pdesc, pi8, pdesc, pi32, pi32, pi32, i32},
                     true);
  changed |= process(module,
                     pi8,
                     "fort_ptr_assn_dcharx",
                     {pi8, pdesc, pi8, pdesc, pi32, pi32, pi32, i32},
                     true);
  changed |= process(module,
                     pi8,
                     "fort_ptr_assn_assumeshp",
                     {pi8, pdesc, pi8, pdesc, pi32},
                     false);
  changed |= process(module,
                     pi8,
                     "fort_ptr_assn_char_assumeshp",
                     {pi8, pdesc, pi8, pdesc, pi32, i32},
                     true);
  changed |= process(module,
                     pi8,
                     "fort_ptr_assn_dchar_assumeshp",
                     {pi8, pdesc, pi8, pdesc, pi32, i32},
                     true);
  changed |= process(module,
                     pi8,
                     "fort_ptr_shape_assnx",
                     {pi8, pdesc, pi8, pdesc, pi32, pi32, pi32, pi32},
                     true);
  changed |= process(module,
                     pi8,
                     "fort_ptr_shape_assn",
                     {pi8, pdesc, pi8, pdesc, pi32, pi32, pi32, pi32},
                     true);
  changed
      |= process(module, vd, "fort_ptr_out", {pi8, pdesc, pi8, pdesc}, false);
  changed |= process(
      module, vd, "fort_ptr_out_char", {pi8, pdesc, pi8, pdesc, i32}, true);
  changed |= process(
      module, i32, "fort_associated", {pi8, pdesc, pi8, pdesc}, false);
  changed |= process(
      module, i32, "fort_associated_t", {pi8, pdesc, pi8, pdesc}, false);
  changed |= process(
      module, i32, "fort_associated_char", {pi8, pdesc, pi8, pdesc, i32}, true);
  changed |= process(module,
                     i32,
                     "fort_associated_tchar",
                     {pi8, pdesc, pi8, pdesc, i32},
                     true);
  changed |= process(
      module, vd, "f90_subchk", {i32, i32, i32, i32, i32, pi8}, true);
  changed |= process(
      module, vd, "f90_subchk64", {i64, i64, i64, i32, i32, pi8}, true);
  changed |= process(module, vd, "f90_ptrchk", {i32, i32, pi8}, true);
  changed |= process(module, vd, "f90_ptrcp", {pi8}, true);
  changed |= process(
      module, vd, "f90_move_alloc", {ppi8, pdesc, ppi8, pdesc}, false);
  changed |= process(module,
                     vd,
                     "f90_c_f_ptr",
                     {ppi8, pi32, pi32, ppi8, pdesc, pi32, pi8, pi32},
                     false);
  changed |= process(module, vd, "f90_c_f_procptr", {ppi8, ppi8}, false);
  changed |= process(module,
                     vd,
                     "fort_dist_alignment",
                     {pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_dist_distribution",
                     {pi8,   pi8,   pi8,   pi8,   pi8,   pi8,   pi8,
                      pi8,   pi8,   pi8,   pdesc, pdesc, pdesc, pdesc,
                      pdesc, pdesc, pdesc, pdesc, pdesc, pdesc},
                     true);
  changed |= process(module,
                     vd,
                     "fort_dist_template",
                     {pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     true);
  changed |= process(module,
                     vd,
                     "fort_global_alignment",
                     {pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_global_distribution",
                     {pi8,   pi8,   pi8,   pi8,   pi8,   pi8,   pi8,
                      pi8,   pi8,   pi8,   pdesc, pdesc, pdesc, pdesc,
                      pdesc, pdesc, pdesc, pdesc, pdesc, pdesc},
                     true);
  changed |= process(module,
                     vd,
                     "fort_global_template",
                     {pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pi8,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     true);
  changed |= process(module,
                     vd,
                     "fort_global_lbound",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(
      module, vd, "fort_global_shape", {pi8, pi8, pdesc, pdesc}, false);
  changed |= process(module,
                     vd,
                     "fort_global_size",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_global_ubound",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_abstract_to_physical",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_physical_to_abstract",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_local_to_global",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(
      module,
      vd,
      "fort_global_to_local",
      {pi8, pi8, pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc, pdesc, pdesc},
      false);
  changed |= process(module,
                     vd,
                     "fort_local_blkcnt",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_local_lindex",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_local_uindex",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(
      module, vd, "fort_changed |= processors_shape", {pi32, pdesc}, false);
  changed |= process(
      module, i32, "fort_number_of_changed |= processors", {pi32, pi32}, false);
  changed |= process(module,
                     i64,
                     "fort_knumber_of_changed |= processors",
                     {pi32, pi32},
                     false);
  changed |= process(
      module, vd, "fort_procnum_to_coord", {pi32, pi32, pi32, pi32}, false);
  changed |= process(
      module, i32, "fort_coord_to_procnum", {pi32, pi32, pi32}, false);
  changed
      |= process(module,
                 vd,
                 "fort_qopy_in",
                 {ppi8, pi64, pi8, pdesc, pi8, pdesc, pi32, pi32, pi32, pi32},
                 true);
  changed |= process(
      module, vd, "fort_copy_out", {pi8, pi8, pdesc, pdesc, pi32}, false);
  changed |= process(module, vd, "fort_check_block_size", {pi8, pdesc}, false);
  changed |= process(module, vd, "fort_template", {pdesc, pi32, pi32}, true);
  changed |= process(
      module, vd, "f90_template", {pdesc, pi32, pi32, pi32, pi32}, true);
  changed |= process(module,
                     vd,
                     "f90_template1",
                     {pdesc, pi32, pi32, pi32, pi32, pi32},
                     false);
  changed |= process(module,
                     vd,
                     "f90_template2",
                     {pdesc, pi32, pi32, pi32, pi32, pi32, pi32, pi32},
                     false);
  changed
      |= process(module,
                 vd,
                 "f90_template3",
                 {pdesc, pi32, pi32, pi32, pi32, pi32, pi32, pi32, pi32, pi32},
                 false);
  changed |= process(
      module, vd, "f90_template1v", {pdesc, i32, i32, i32, i32}, true);
  changed |= process(module,
                     vd,
                     "f90_template2v",
                     {pdesc, i32, i32, i32, i32, i32, i32},
                     true);
  changed |= process(module,
                     vd,
                     "f90_template3v",
                     {pdesc, i32, i32, i32, i32, i32, i32, i32, i32},
                     true);
  changed |= process(
      module, vd, "fort_instance", {pdesc, pdesc, pi32, pi32, pi32}, true);
  changed |= process(module, vd, "fort_free", {pdesc}, false);
  changed |= process(module, vd, "fort_freen", {pi32}, true);
  changed |= process(
      module, vd, "f90_addr_1_dim_1st_elem", {pi8, pi8, ppi8}, false);
  changed |= process(
      module, vd, "f90_copy_f77_arg", {ppi8, pdesc, ppi8, pi32}, false);
  changed |= process(
      module, vd, "f90_copy_f77_argw", {ppi8, pdesc, pi8, ppi8, pi32}, false);
  changed |= process(module,
                     vd,
                     "f90_copy_f77_argl",
                     {ppi8, pdesc, pi8, ppi8, pi32, pi32},
                     false);
  changed |= process(module,
                     vd,
                     "f90_copy_f77_argsl",
                     {pi8, pdesc, pi8, ppi8, pi32, pi32},
                     false);
  changed |= process(
      module, vd, "f90_copy_f90_arg", {ppi8, pdesc, ppi8, pdesc, pi32}, false);
  changed |= process(module,
                     vd,
                     "f90_copy_f90_argl",
                     {ppi8, pdesc, ppi8, pdesc, pi32, pi32},
                     false);
  changed
      |= process(module, i32, "f90_conformable_dd", {pi8, pdesc, pdesc}, false);
  changed |= process(module, i32, "f90_conformable_dn", {pi8, pdesc}, true);
  changed |= process(module, i32, "f90_conformable_nd", {pi8, pdesc}, true);
  changed |= process(module, i32, "f90_conformable_nn", {pi8}, true);
  changed |= process(module, i32, "f90_is_contiguous", {pi8, pdesc}, false);
  changed |= process(module, vd, "fort_alls", {pi8, pi8, pdesc, pdesc}, false);
  changed |= process(
      module, vd, "fort_all", {pi8, pi8, pi8, pdesc, pdesc, pdesc}, false);
  changed |= process(module,
                     vd,
                     "fort_reduce_all",
                     {pi8, pi32, pi32, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_global_all",
                     {pi8, pi8, pi32, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module, vd, "fort_anys", {pi8, pi8, pdesc, pdesc}, false);
  changed |= process(
      module, vd, "fort_any", {pi8, pi8, pi8, pdesc, pdesc, pdesc}, false);
  changed |= process(module,
                     vd,
                     "fort_reduce_any",
                     {pi8, pi32, pi32, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_global_any",
                     {pi8, pi8, pi32, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_reduce_descriptor",
                     {pdesc, pi32, pi32, pdesc, pi32},
                     false);
  changed
      |= process(module, vd, "fort_counts", {pi8, pi8, pdesc, pdesc}, false);
  changed |= process(
      module, vd, "fort_count", {pi8, pi8, pi8, pdesc, pdesc, pdesc}, false);
  changed
      |= process(module,
                 vd,
                 "fort_findlocs",
                 {pi32, pi8, pi8, pi8, pi32, pdesc, pdesc, pdesc, pdesc, pdesc},
                 false);
  changed |= process(
      module,
      vd,
      "fort_findloc",
      {pi8, pi8, pi8, pi8, pi8, pi32, pdesc, pdesc, pdesc, pdesc, pdesc, pdesc},
      false);
  changed
      |= process(module,
                 vd,
                 "fort_kfindlocs",
                 {pi64, pi8, pi8, pi8, pi64, pdesc, pdesc, pdesc, pdesc, pdesc},
                 false);
  changed |= process(
      module,
      vd,
      "fort_kfindloc",
      {pi8, pi8, pi8, pi8, pi8, pi64, pdesc, pdesc, pdesc, pdesc, pdesc, pdesc},
      false);
  changed |= process(module,
                     vd,
                     "fort_findlocstrs",
                     {pi32,
                      pi8,
                      pi8,
                      pi32,
                      pi8,
                      pi32,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_findlocstr",
                     {pi8,
                      pi8,
                      pi8,
                      pi32,
                      pi8,
                      pi8,
                      pi32,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_kfindlocstrs",
                     {pi64,
                      pi8,
                      pi8,
                      pi64,
                      pi8,
                      pi64,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_kfindlocstr",
                     {pi8,
                      pi8,
                      pi8,
                      pi64,
                      pi8,
                      pi8,
                      pi64,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     false);
  changed |= process(
      module, vd, "fort_ianys", {pi8, pi8, pi8, pdesc, pdesc, pdesc}, false);
  changed |= process(module,
                     vd,
                     "fort_iany",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_reduce_iany",
                     {pi8, pi32, pi32, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_global_iany",
                     {pi8, pi8, pi32, pdesc, pdesc, pdesc},
                     false);
  changed |= process(
      module, vd, "fort_maxlocs", {pi32, pi8, pi8, pdesc, pdesc, pdesc}, false);
  changed |= process(module,
                     vd,
                     "fort_maxloc",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_kmaxlocs",
                     {pi64, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_kmaxloc",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(
      module, vd, "fort_maxvals", {pi8, pi8, pi8, pdesc, pdesc, pdesc}, false);
  changed |= process(module,
                     vd,
                     "fort_maxval",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_reduce_maxval",
                     {pi8, pi32, pi32, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_global_maxval",
                     {pi8, pi8, pi32, pdesc, pdesc, pdesc},
                     false);
  changed |= process(
      module, vd, "fort_minlocs", {pi32, pi8, pi8, pdesc, pdesc, pdesc}, false);
  changed |= process(module,
                     vd,
                     "fort_minloc",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_kminlocs",
                     {pi64, pi8, pi8, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_kminloc",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(
      module, vd, "fort_minvals", {pi8, pi8, pi8, pdesc, pdesc, pdesc}, false);
  changed |= process(module,
                     vd,
                     "fort_minval",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_reduce_minval",
                     {pi8, pi32, pi32, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_global_minval",
                     {pi8, pi8, pi32, pdesc, pdesc, pdesc},
                     false);
  changed |= process(
      module, vd, "fort_sums", {pi8, pi8, pi8, pdesc, pdesc, pdesc}, false);
  changed |= process(module,
                     vd,
                     "fort_sum",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_reduce_sum",
                     {pi8, pi32, pi32, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_global_sum",
                     {pi8, pi8, pi32, pdesc, pdesc, pdesc},
                     false);
  changed
      |= process(module,
                 vd,
                 "fort_reshape",
                 {pi8, pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc, pdesc},
                 false);
  changed |= process(
      module,
      vd,
      "fort_reshapec",
      {pi8, pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc, pdesc, i32, i32},
      true);
  changed |= process(module, i32, "f90io_rewind", {pi32, pi32, pi32}, false);
  changed |= process(module, vd, "fort_rnum", {pf32, pdesc}, false);
  changed |= process(module, vd, "fort_rnumd", {pf64, pdesc}, false);
  changed |= process(
      module, vd, "fort_rseed", {pi8, pi32, pi32, pdesc, pdesc, pdesc}, false);
  changed |= process(module, vd, "fort_get_scalar", {pi8, pi8, pdesc}, true);
  changed |= process(
      module, vd, "fort_bcst_scalar", {pi8, pi32, pi8, pi32, pi32}, false);
  changed |= process(module,
                     vd,
                     "fort_maxval_scatter",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     true);
  changed
      |= process(module,
                 vd,
                 "fort_maxval_scatterx",
                 {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc, pi32, pi32},
                 true);
  changed |= process(module,
                     psked,
                     "fort_comm_maxval_scatter",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc, pi32, pi32},
                     true);
  changed |= process(module,
                     psked,
                     "fort_comm_maxval_scatter_1",
                     {pi8, pi8, pi8, pi32, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed
      |= process(module,
                 psked,
                 "fort_comm_maxval_scatter_2",
                 {pi8, pi8, pi8, pi32, pi32, pdesc, pdesc, pdesc, pdesc, pdesc},
                 false);
  changed |= process(module,
                     psked,
                     "fort_comm_maxval_scatter_3",
                     {pi8,
                      pi8,
                      pi8,
                      pi32,
                      pi32,
                      pi32,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     false);
  changed |= process(module,
                     psked,
                     "fort_comm_maxval_scatter_4",
                     {pi8,
                      pi8,
                      pi8,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     false);
  changed |= process(module,
                     psked,
                     "fort_comm_maxval_scatter_5",
                     {pi8,
                      pi8,
                      pi8,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     false);
  changed |= process(module,
                     psked,
                     "fort_comm_maxval_scatter_6",
                     {pi8,
                      pi8,
                      pi8,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     false);
  changed |= process(module,
                     psked,
                     "fort_comm_maxval_scatter_7",
                     {pi8,   pi8,   pi8,   pi32,  pi32,  pi32,  pi32,
                      pi32,  pi32,  pi32,  pdesc, pdesc, pdesc, pdesc,
                      pdesc, pdesc, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_minval_scatter",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     true);
  changed
      |= process(module,
                 vd,
                 "fort_minval_scatterx",
                 {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc, pi32, pi32},
                 true);
  changed |= process(module,
                     psked,
                     "fort_comm_minval_scatter",
                     {pi8, pi8, pi8, pdesc, pdesc, pdesc, pi32, pi32},
                     true);
  changed |= process(module,
                     psked,
                     "fort_comm_minval_scatter_1",
                     {pi8, pi8, pi8, pi32, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed
      |= process(module,
                 psked,
                 "fort_comm_minval_scatter_2",
                 {pi8, pi8, pi8, pi32, pi32, pdesc, pdesc, pdesc, pdesc, pdesc},
                 false);
  changed |= process(module,
                     psked,
                     "fort_comm_minval_scatter_3",
                     {pi8,
                      pi8,
                      pi8,
                      pi32,
                      pi32,
                      pi32,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     false);
  changed |= process(module,
                     psked,
                     "fort_comm_minval_scatter_4",
                     {pi8,
                      pi8,
                      pi8,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     false);
  changed |= process(module,
                     psked,
                     "fort_comm_minval_scatter_5",
                     {pi8,
                      pi8,
                      pi8,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     false);
  changed |= process(module,
                     psked,
                     "fort_comm_minval_scatter_6",
                     {pi8,
                      pi8,
                      pi8,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pi32,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc,
                      pdesc},
                     false);
  changed |= process(module,
                     psked,
                     "fort_comm_minval_scatter_7",
                     {pi8,   pi8,   pi8,   pi32,  pi32,  pi32,  pi32,
                      pi32,  pi32,  pi32,  pdesc, pdesc, pdesc, pdesc,
                      pdesc, pdesc, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed |= process(module,
                     vd,
                     "fort_spread",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc},
                     false);
  changed
      |= process(module,
                 vd,
                 "fort_spreads",
                 {pi8, pi8, pi8, pi8, pi32, pdesc, pdesc, pdesc, pdesc, pdesc},
                 false);
  changed |= process(module,
                     vd,
                     "fort_spreadc",
                     {pi8, pi8, pi8, pi8, pdesc, pdesc, pdesc, pdesc, i32},
                     true);
  changed |= process(
      module,
      vd,
      "fort_spreadcs",
      {pi8, pi8, pi8, pi8, pi32, pdesc, pdesc, pdesc, pdesc, pdesc, i32},
      true);
  changed |= process(
      module, vd, "fort_spread_descriptor", {pdesc, pdesc, pi32, pi32}, false);
  changed |= process(module, vd, "fort_times", {pf64}, false);
  changed |= process(module, vd, "fort_msgstats", {pi32}, false);
  changed |= process(module,
                     vd,
                     "fort_transfer",
                     {pi8, pi8, pi32, pi32, pdesc, pdesc, pi8},
                     true);
  changed |= process(
      module, i32, "f90_same_type_as", {pi8, podesc, pi8, podesc, i32}, true);
  changed |= process(module,
                     i32,
                     "f90_extends_type_of",
                     {pi8, podesc, pi8, podesc, i32},
                     true);
  changed |= process(
      module, i64, "f90_ksame_type_as", {pi8, podesc, pi8, podesc, i32}, true);
  changed |= process(module,
                     i64,
                     "f90_kextends_type_of",
                     {pi8, podesc, pi8, podesc, i32},
                     true);
  changed |= process(module, vd, "f90_set_type", {pdesc, podesc}, false);
  changed
      |= process(module, vd, "f90_test_and_set_type", {pdesc, podesc}, false);
  changed |= process(module, i32, "f90_get_object_size", {pdesc}, false);
  changed |= process(module, i64, "f90_kget_object_size", {pdesc}, false);
  changed |= process(module, vd, "f90_finalize", {pi8, pdesc}, false);
  changed |= process(module,
                     vd,
                     "f90_dealloc_poly_mbr03",
                     {pdesc, pi32, pi8, pi32, pi8},
                     true);
  changed |= process(
      module, vd, "f90_dealloc_poly03", {pdesc, pi32, pi8, pi32, pi8}, true);
  changed
      |= process(module, vd, "f90_poly_asn", {pi8, pdesc, pi8, pdesc}, true);
  changed |= process(module, vd, "f90_set_intrin_type", {pdesc}, true);
  changed |= process(module,
                     i32,
                     "f90_same_intrin_type_as",
                     {pi8, podesc, pi8, i32, i32},
                     true);
  changed |= process(module,
                     i64,
                     "f90_ksame_intrin_type_as",
                     {pi8, podesc, pi8, i32, i32},
                     true);
  changed |= process(
      module, vd, "f90_poly_asn_src_intrin", {pi8, pdesc, pi8, i32}, true);
  changed |= process(
      module, vd, "f90_poly_asn_dest_intrin", {pi8, i32, pi8, pdesc}, true);
  changed
      |= process(module, vd, "f90_init_unl_poly_desc", {pdesc, pdesc}, true);
  changed |= process(module, vd, "f90_init_from_desc", {pi8, pdesc}, true);
  changed |= process(module, i32, "f90io_unf_async", {pi8, pi32}, true);
  changed |= process(
      module, i32, "f90io_unf_init", {pi32, pi32, pi32, pi32, pi32}, false);
  changed
      |= process(module, i32, "f90io_unf_read", {pi32, pi32, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_unf_read_a", {pi32, pi32, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_unf_read64_a", {pi32, pi64, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_byte_read", {pi32, pi32, pi8, pi32}, false);
  changed |= process(
      module, i32, "f90io_byte_read64", {pi64, pi32, pi8, pi32}, false);
  changed
      |= process(module, i32, "f90io_unf_write", {pi32, pi32, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_unf_write_a", {pi32, pi32, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_unf_write64_a", {pi32, pi64, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_byte_write", {pi32, pi32, pi8, pi32}, false);
  changed |= process(
      module, i32, "f90io_byte_write64", {pi64, pi32, pi8, pi32}, false);
  changed |= process(
      module, i32, "f90io_usw_init", {pi32, pi32, pi32, pi32, pi32}, false);
  changed
      |= process(module, i32, "f90io_usw_read", {pi32, pi32, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_usw_read_a", {pi32, pi32, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_usw_read64_a", {pi32, pi64, pi32, pi8}, true);
  changed
      |= process(module, i32, "f90io_usw_write", {pi32, pi32, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_usw_write_a", {pi32, pi32, pi32, pi8}, true);
  changed |= process(
      module, i32, "f90io_usw_write64_a", {pi32, pi64, pi32, pi8}, true);
  changed
      |= process(module, i32, "f90io_wait", {pi32, pi32, pi32, pi32}, false);

  if(changed)
    changed |= stripRedundantCasts(module);

  return changed;
}

char FixLibFlangSignaturesPass::ID = 0;

static const char* name = "moya-libflang-sigs";
static const char* descr = "Add concrete libflang declarations";
static const bool cfg = false;
static const bool analysis = false;

INITIALIZE_PASS(FixLibFlangSignaturesPass, name, descr, cfg, analysis)

Pass* createFixLibFlangSignaturesPass() {
  return new FixLibFlangSignaturesPass();
}

void registerFixLibFlangSignaturesPass(const PassManagerBuilder&,
                                       legacy::PassManagerBase& pm) {
  pm.add(new FixLibFlangSignaturesPass());
}
