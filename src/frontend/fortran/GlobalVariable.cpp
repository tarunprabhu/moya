#include "GlobalVariable.h"
#include "Module.h"
#include "common/Deserializer.h"
#include "common/Diagnostics.h"
#include "common/Serializer.h"

#include <llvm/Support/raw_ostream.h>

using namespace llvm;

namespace moya {

GlobalVariable::GlobalVariable(ModuleBase& moduleBase,
                               const std::string& llvmName,
                               const std::string& artificialName,
                               const std::string& typeName,
                               const std::string& parent,
                               unsigned count,
                               const char* const* names,
                               const unsigned* offsets,
                               const char* const* types,
                               const unsigned* descriptors)
    : GlobalVariableBase(moduleBase), llvmName(llvmName),
      strct(static_cast<Module&>(module).addStruct(typeName, true)) {
  setArtificialName(artificialName);
  for(unsigned i = 0; i < count; i++)
    strct.addType(names[i], offsets[i], types[i], descriptors[i]);
}

Module& GlobalVariable::getModule() {
  return static_cast<Module&>(GlobalVariableBase::getModule());
}

const Module& GlobalVariable::getModule() const {
  return static_cast<const Module&>(GlobalVariableBase::getModule());
}

const std::string& GlobalVariable::getLLVMName() const {
  return llvmName;
}

const Struct& GlobalVariable::getStruct() const {
  return strct;
}

void GlobalVariable::serialize(Serializer& s) const {
  s.mapStart();
  s.add("name", getName());
  s.add("artificialName", getArtificialName());
  s.add("llvmName", getLLVMName());
  s.add("artificial", isArtificial());
  s.add("loc", getLocation());
  s.add("struct", getStruct());
  s.mapEnd();
}

void GlobalVariable::deserialize(Deserializer& d) {
  std::string name;

  d.mapStart();
  d.deserialize("name", name);
  d.deserialize("artificialName", artificialName);
  d.deserialize("llvmName", llvmName);
  d.deserialize("artificial", artificial);
  d.deserialize("loc", loc);
  d.deserialize("struct", strct);
  d.mapEnd();

  setName(name);
}

} // namespace moya
