#include "Argument.h"
#include "Function.h"
#include "Module.h"

namespace moya {

Argument::Argument(FunctionBase& func)
    : ArgumentBase(func), type(func.getModule().addType<Type>()), linked(-1),
      array(false), closure(false) {
  ;
}

Argument::Argument(FunctionBase& func,
                   const std::string& name,
                   const std::string& llvmStr,
                   int linked,
                   bool artificial,
                   bool array,
                   bool closure)
    : ArgumentBase(func, name, func.getNumArgs()),
      type(func.getModule().addType<Type>(llvmStr)), linked(linked),
      array(array), closure(closure) {
  if(artificial) {
    setArtificial(artificial);
    setArtificialName(name);
    setName("");
  }
}

const Type& Argument::getType() const {
  return type;
}

const Function& Argument::getFunction() const {
  return static_cast<const Function&>(ArgumentBase::getFunction());
}

const Argument& Argument::getLinked() const {
  return getFunction().getArg(linked);
}

bool Argument::isArray() const {
  return array;
}

bool Argument::isLinked() const {
  return linked != -1;
}

bool Argument::isClosure() const {
  return closure;
}

llvm::Argument* Argument::getLLVM() const {
  return llvm;
}

void Argument::setLLVM(llvm::Argument* arg) {
  llvm = arg;
}

void Argument::serialize(Serializer& s) const {
  s.mapStart();
  s.add("name", name);
  s.add("artificialName", artificialName);
  s.add("type", type);
  s.add("num", num);
  s.add("linked", linked);
  s.add("artificial", artificial);
  s.add("array", array);
  s.add("closure", closure);
  s.mapEnd();
}

void Argument::deserialize(Deserializer& d) {
  d.mapStart();
  d.deserialize("name", name);
  d.deserialize("artificialName", artificialName);
  d.deserialize("type", type);
  d.deserialize("num", num);
  d.deserialize("linked", linked);
  d.deserialize("artificial", artificial);
  d.deserialize("array", array);
  d.deserialize("closure", closure);
  d.mapEnd();
}

} // namespace moya
