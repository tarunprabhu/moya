#include "Function.h"
#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/Config.h"
#include "common/DataLayout.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using llvm::cast;
using llvm::dyn_cast;
using llvm::isa;

class AssociatePass : public llvm::ModulePass {
protected:
  moya::DataLayout layout;

protected:
  void associateClosures(llvm::Module&, moya::Module&);
  void associateFunctions(llvm::Module&, moya::Module&);
  void associateGlobals(llvm::Module&, moya::Module&);
  void associateGEPs(llvm::Module&, moya::Module&);
  void associateStructs(llvm::Module&, moya::Module&);
  void associateTypes(llvm::Module&, moya::Module&);
  void associateDirectives(llvm::Module&, moya::Module&);

public:
  AssociatePass();

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);

public:
  static char ID;
};

AssociatePass::AssociatePass() : llvm::ModulePass(ID) {
  llvm::initializeAssociatePassPass(*llvm::PassRegistry::getPassRegistry());
}

llvm::StringRef AssociatePass::getPassName() const {
  return "Moya Associate (Fortran)";
}

void AssociatePass::getAnalysisUsage(llvm::AnalysisUsage& AU) const {
  AU.setPreservesAll();
  AU.addRequired<ModuleWrapperPass>();
}

void AssociatePass::associateClosures(llvm::Module& module,
                                      moya::Module& feModule) {
  for(llvm::StructType* sty : module.getIdentifiedStructTypes())
    if(feModule.hasClosure(sty->getName()))
      feModule.getClosure(sty->getName()).setLLVM(sty);
}

void AssociatePass::associateFunctions(llvm::Module& module,
                                       moya::Module& feModule) {
  for(llvm::Function& f : module.functions()) {
    if(feModule.hasFunction(f.getName())) {
      moya::Function& feFunc = feModule.getFunction(f.getName());
      feModule.setLLVM(feFunc, f);
      for(llvm::Argument& arg : f.args())
        feFunc.getArg(arg.getArgNo()).setLLVM(&arg);
    }
  }
}

static moya::Vector<llvm::GetElementPtrInst*>
getLookupsIntoClosure(llvm::Argument& arg) {
  moya::Vector<llvm::GetElementPtrInst*> geps;
  llvm::Type* pi8 = llvm::Type::getInt8PtrTy(arg.getContext());

  for(llvm::Use& u : arg.uses())
    if(auto* cst = dyn_cast<llvm::CastInst>(u.getUser()))
      if(cst->getDestTy() == pi8)
        for(llvm::Use& v : cst->uses())
          if(auto* gep = dyn_cast<llvm::GetElementPtrInst>(v.getUser()))
            if(gep->hasAllConstantIndices())
              geps.push_back(gep);

  return geps;
}

void AssociatePass::associateGEPs(llvm::Module& module,
                                  moya::Module& feModule) {
  for(llvm::Function& f : module.functions()) {
    if(feModule.hasFunction(f.getName())) {
      moya::Function& feFunc = feModule.getFunction(f.getName());
      int64_t idx = 0;
      for(auto i = llvm::inst_begin(f); i != llvm::inst_end(f); i++) {
        if(auto* gep = dyn_cast<llvm::GetElementPtrInst>(&*i)) {
          moya::GEP& feGEP = feFunc.getGEP(idx++);
          if(moya::Config::getEnvVarValueAsBool(getenv("MOYA_FLANG_VERBOSE")))
            llvm::errs() << *gep << "\n"
                         << "        idx: " << idx << "\n"
                         << "    offsets: " << feGEP.getOffsets().str() << "\n"
                         << "      cumul: " << feGEP.getCumulative() << "\n";
          if(gep->hasAllConstantIndices()) {
            if(not feGEP.isConstGEP())
              moya_error("Got non-constant GEP when constant GEP expected\n"
                         << "   gep: " << *gep << "\n"
                         << "  func: " << f.getName());
            else if(layout.getOffsetInGEP(gep) != feGEP.getCumulative())
              moya_error("Mismatch in offsets in GEP:\n"
                         << "    LLVM: " << layout.getOffsetInGEP(gep) << "\n"
                         << "   cumul: " << feGEP.getCumulative() << "\n"
                         << "  offset: " << feGEP.getOffset() << "\n"
                         << "     gep: " << *gep << "\n"
                         << "    func: " << f.getName());
          } else {
            if(feGEP.isConstGEP())
              moya_error("Got constant GEP when non-constant GEP expected\n"
                         << "  offset: " << feGEP.getCumulative() << "\n"
                         << "     gep: " << *gep << "\n"
                         << "    func: " << f.getName());
          }
          feFunc.setLLVM(feGEP, gep);
        }
      }

      // We have to special case closures (sits back and waits for the amazement
      // to subside). For some reason, when looking up an array descriptor
      // inside a closure, the lookup of the pointer to the descriptor in the
      // closure also "looks like" an array lookup. Obviously this causes a
      // failure in the type checker because the struct offset expects an offset
      // to a member.
      //
      for(llvm::Argument& arg : f.args()) {
        const moya::Argument& feArg = feFunc.getArg(arg.getArgNo());
        if(feArg.isClosure()) {
          const moya::Closure& feClosure = feModule.getClosure(f);
          llvm::StructType* closure = cast<llvm::StructType>(
              feArg.getType().getLLVMAs<llvm::PointerType>()->getElementType());
          const moya::StructLayout& sl = layout.getStructLayout(closure);

          // The first use of the argument will be a cast to i8* followed
          // by a GEP
          for(llvm::GetElementPtrInst* gep : getLookupsIntoClosure(arg)) {
            int64_t offset = layout.getOffsetInGEP(gep);
            unsigned idx = sl.getElementContainingOffset(offset);
            if((sl.getElementOffset(idx) == offset)
               and feClosure.isDescriptor(idx)) {
              moya::GEP& feGEP = feFunc.getGEP(gep);
              const moya::Vector<int64_t>& offsets = feGEP.getOffsets();
              if((offsets.size() == 1) and (offsets[0] == -1))
                feGEP.overrideOffsets({offset});
            }
          }
        }
      }
    }
  }
}

void AssociatePass::associateGlobals(llvm::Module& module,
                                     moya::Module& feModule) {
  for(llvm::GlobalVariable& g : module.globals())
    if(feModule.hasGlobal(g.getName()))
      feModule.setLLVM(feModule.getGlobal(g.getName()), g);
}

void AssociatePass::associateStructs(llvm::Module& module,
                                     moya::Module& feModule) {
  // These are for user-defined structs and the hoisted fake structs. There's
  // nothing we can do about the fake ones and it's too much of a pain to try
  // and get rid of it here.
  for(llvm::StructType* sty : module.getIdentifiedStructTypes()) {
    if(sty->hasName()) {
      if(feModule.hasStruct(sty->getName())) {
        moya::Struct& feStruct = feModule.getStruct(sty->getName());
        feStruct.setLLVM(sty);
        feStruct.finalizeTypes();
      } else if(not sty->getNumElements()) {
        // FIXME: This is a workaround. I can't find a reasonable way to
        // distinguish between empty structs that are actually created by
        // the user and those that are empty because Flang is a )(U#$)#($#@.
        // For now, we can get away with just ignoring these because
        // moya::Struct is used to keep additional information about the
        // struct members. But since we don't have any members ...
        ;
      } else if(not sty->isOpaque()
                and not feModule.isTypeDesc(sty->getName())) {
        moya_error("Could not find struct: " << sty->getName() << "\n");
      }
    }
  }

  // Because Flang lowers everything to byte arrays, sometimes, a struct type
  // has no uses in the code. The type still does appear in the metadata
  // because we will have put it there, but
  // llvm::Module::getIdentifiedStructTypes() will not return it. So we need a
  // second round to look for any such types that might have been missed
  for(auto& ptr : feModule.getStructs()) {
    moya::Struct& feStruct = *ptr;
    if(not feStruct.getLLVM()) {
      if(llvm::StructType* sty = module.getTypeByName(feStruct.getLLVMName())) {
        feStruct.setLLVM(sty);
        feStruct.finalizeTypes();
      }
    }
  }
}

void AssociatePass::associateTypes(llvm::Module& module,
                                   moya::Module& feModule) {
  for(auto& ptr : feModule.getTypes()) {
    moya::Type& type = static_cast<moya::Type&>(*ptr);
    type.setLLVM(moya::parse(type.getLLVMStr(), module));
  }
}

void AssociatePass::associateDirectives(llvm::Module& module,
                                        moya::Module& feModule) {
  feModule.associate();
}

bool AssociatePass::runOnModule(llvm::Module& module) {
  moya_message(getPassName());

  layout.reset(module);
  moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  if(moya::Config::getEnvVarValueAsBool(getenv("MOYA_FLANG_VERBOSE")))
    llvm::errs() << module << "\n";

  associateClosures(module, feModule);
  associateFunctions(module, feModule);
  associateGlobals(module, feModule);
  associateStructs(module, feModule);
  associateTypes(module, feModule);
  associateGEPs(module, feModule);
  associateDirectives(module, feModule);

  return false;
}

char AssociatePass::ID = 0;

using namespace llvm;

static const char* name = "moya-associate-fort";
static const char* descr = "Associate frontend elements with LLVM IR (Fortran)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(AssociatePass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_END(AssociatePass, name, descr, cfg, analysis)

void registerAssociatePass(const llvm::PassManagerBuilder&,
                           llvm::legacy::PassManagerBase& pm) {
  pm.add(new AssociatePass());
}

llvm::Pass* createAssociatePass() {
  return new AssociatePass();
}
