#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/StringUtils.h"

#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

ModuleWrapperPass::ModuleWrapperPass() : ModulePass(ID) {
  llvm::initializeModuleWrapperPassPass(*PassRegistry::getPassRegistry());
}

StringRef ModuleWrapperPass::getPassName() const {
  return "Module Wrapper (Fortran)";
}

void ModuleWrapperPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesAll();
}

moya::Module& ModuleWrapperPass::getModule() {
  return feModule;
}

const moya::Module& ModuleWrapperPass::getModule() const {
  return feModule;
}

bool ModuleWrapperPass::runOnModule(Module& module) {
  moya_message(getPassName());

  std::string file
      = moya::StringUtils::rpartition(module.getSourceFileName(), ".").first
        + ".json";
  feModule.initialize(file, false);

  // Delete the file when we're done because there is no other reasonable
  // place to do this
  std::remove(file.c_str());

  return false;
}

char ModuleWrapperPass::ID = 0;

static const char* name = "moya-module-wrapper";
static const char* descr
    = "Reads the serialized metadata written by flang from disk";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(ModuleWrapperPass, name, descr, cfg, analysis)

Pass* createModuleWrapperPass() {
  return new ModuleWrapperPass();
}

void registerModuleWrapperPass(const PassManagerBuilder&,
                               legacy::PassManagerBase& pm) {
  pm.add(new ModuleWrapperPass());
}
