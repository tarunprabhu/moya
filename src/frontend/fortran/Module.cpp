#include "Module.h"
#include "Globals.h"
#include "common/Config.h"
#include "common/Deserializer.h"
#include "common/Diagnostics.h"
#include "common/FileSystemUtils.h"
#include "common/Metadata.h"
#include "common/PathUtils.h"
#include "common/Serializer.h"
#include "common/Set.h"
#include "common/SourceLang.h"
#include "common/StringUtils.h"
#include "frontend/annotations/Parser.h"
#include "frontend/components/DirectiveContext.h"

#include <llvm/Support/raw_ostream.h>

#include <cstdlib>
#include <cstring>
#include <fstream>
#include <functional>

namespace moya {

static std::string mangle(const std::string& func,
                          const std::string& parent = "",
                          const std::string& module = "") {
  std::stringstream ss;

  if(module.length())
    ss << module << "_";
  if(parent.length())
    ss << parent << "_" << func;
  else
    ss << func << "_";

  return ss.str();
}

Module::Module() : ModuleBase() {
  ;
}

void Module::initialize(const std::string& filename,
                        bool flang1,
                        SourceLang lang) {
  serializedFile = filename;
  if(lang != SourceLang::UnknownLang)
    setSourceLang(lang);
  if(not flang1)
    deserialize();
}

int Module::parseDirective(const std::string& s,
                           const std::string& file,
                           unsigned line,
                           unsigned col) {
  if(directives.parse(s, Location(file, line, col)))
    return 0;
  return 1;
}

bool Module::hasFModule(const std::string& name) const {
  return mmap.contains(name);
}

FModule& Module::getFModule(const std::string& name) {
  return *mmap.at(name);
}

const FModule& Module::getFModule(const std::string& name) const {
  return *mmap.at(name);
}

bool Module::hasFunction(const std::string& id) const {
  return fmap.contains(id);
}

Function& Module::getFunction(const std::string& f) {
  return *fmap.at(f);
}

Function& Module::getFunction(llvm::Function& f) {
  return static_cast<Function&>(ModuleBase::getFunction(f));
}

const Function& Module::getFunction(const std::string& f) const {
  return *fmap.at(f);
}

const Function& Module::getFunction(llvm::Function& f) const {
  return static_cast<const Function&>(ModuleBase::getFunction(f));
}

GlobalVariable& Module::getGlobal(const std::string& g) {
  return *gmap.at(g);
}

GlobalVariable& Module::getGlobal(llvm::GlobalVariable& g) {
  return static_cast<GlobalVariable&>(ModuleBase::getGlobal(g));
}

const GlobalVariable& Module::getGlobal(const std::string& g) const {
  return *gmap.at(g);
}

const GlobalVariable& Module::getGlobal(llvm::GlobalVariable& g) const {
  return static_cast<const GlobalVariable&>(ModuleBase::getGlobal(g));
}

bool Module::hasGlobal(const std::string& g) const {
  return gmap.contains(g);
}

RegionID Module::getRegionId(const Location& loc) const {
  // Regions cannot currently be nested. But if they are allowed, we must match
  // return the id of the innermost region. Overlapping regions will definitely
  // never be allowed.
  for(const auto* region : directives.getDirectives<RegionDirective>())
    if(Location::isBetween(loc, region->getBeginLoc(), region->getEndLoc()))
      return region->getRegionID();
  return Config::getInvalidRegionID();
}

void Module::addLibFlangFunction(const std::string& f) {
  libFlangFuncs.insert(f);
}

bool Module::isLibFlangFunction(const std::string& f) const {
  return libFlangFuncs.contains(f);
}

void Module::addModuleGlobalCategory(const std::string& name,
                                     const std::string& module) {
  moduleGlobals.emplace(name, module);
}

bool Module::isModuleGlobalCategory(const std::string& name) const {
  return moduleGlobals.contains(name);
}

const std::string&
Module::getModuleGlobalModule(const std::string& name) const {
  return moduleGlobals.at(name);
}

bool Module::hasClosure(const std::string& name) const {
  return cmap.contains(name);
}

bool Module::hasClosure(llvm::Function& f) const {
  return cfmap.contains(f.getName());
}

bool Module::hasClosure(llvm::StructType* sty) const {
  return llvmClosureMap.contains(sty);
}

Closure& Module::getClosure(const std::string& name) {
  return *cmap.at(name);
}

Closure& Module::getClosure(llvm::Function& f) {
  return *cfmap.at(f.getName());
}

Closure& Module::getClosure(llvm::StructType* sty) {
  return *llvmClosureMap.at(sty);
}

const Closure& Module::getClosure(const std::string& name) const {
  return *cmap.at(name);
}

const Closure& Module::getClosure(llvm::Function& f) const {
  return *cfmap.at(f.getName());
}

const Closure& Module::getClosure(llvm::StructType* sty) const {
  return *llvmClosureMap.at(sty);
}

const Vector<std::unique_ptr<Closure>>& Module::getClosures() const {
  return closures;
}

bool Module::hasStruct(const std::string& name) const {
  return smap.contains(name);
}

bool Module::hasStruct(llvm::StructType* sty) const {
  return llvmStructMap.contains(sty);
}

Struct& Module::getStruct(const std::string& sname) {
  return *smap.at(sname);
}

Struct& Module::getStruct(llvm::StructType* sty) {
  return *llvmStructMap.at(sty);
}

const Struct& Module::getStruct(const std::string& sname) const {
  return *smap.at(sname);
}

const Struct& Module::getStruct(llvm::StructType* sty) const {
  return *llvmStructMap.at(sty);
}

const Vector<std::unique_ptr<Struct>>& Module::getStructs() const {
  return structs;
}

void Module::addTypeDesc(const std::string& s) {
  tds.insert(s);
}

bool Module::isTypeDesc(const std::string& s) const {
  return tds.contains(s);
}

void Module::setLLVM(Closure& closure, llvm::StructType* sty) {
  llvmClosureMap[sty] = &closure;
}

void Module::setLLVM(Struct& strct, llvm::StructType* sty) {
  llvmStructMap[sty] = &strct;
}

void Module::addLocalArray(const std::string& func,
                           const std::string& parent,
                           const std::string& module,
                           const std::string& alloca) {
  localArrays[mangle(func, parent, module)].insert(alloca);
}

void Module::addLocalArray(const std::string& func,
                           const std::string& base,
                           const std::string& alloca) {
  if(localArrays.contains(func))
    if(localArrays.at(func).contains(base))
      localArrays.at(func).insert(alloca);
}

bool Module::isLocalArray(const std::string& func,
                          const std::string& alloca) const {
  if(localArrays.contains(func))
    return localArrays.at(func).contains(alloca);
  return false;
}

void Module::addInterfaceParam(const std::string& func,
                               const std::string& parent,
                               const std::string& module,
                               unsigned argno,
                               int type) {
  std::string fn = mangle(func, parent, module);
  if(not ifaceParams.contains(fn))
    ifaceParams[fn] = Map<unsigned, Vector<int>>();
  if(not ifaceParams.at(fn).contains(argno))
    ifaceParams.at(fn).emplace(std::make_pair(argno, Vector<int>()));
  ifaceParams.at(fn).at(argno).push_back(type);
}

const Vector<int>& Module::getInterfaceParams(const std::string& fn,
                                              unsigned argno) const {
  return ifaceParams.at(fn).at(argno);
}

void Module::addLayoutDescriptor(const std::string& name) {
  layoutDescriptors.insert(name);
}

bool Module::isLayoutDescriptor(const std::string& name) const {
  return layoutDescriptors.contains(name);
}

const Set<std::string>& Module::getLayoutDescriptors() const {
  return layoutDescriptors;
}

int64_t Module::addAllocatedType(const std::string& func,
                                 const std::string& parent,
                                 const std::string& module,
                                 int type) {
  std::string llvm = mangle(func, parent, module);
  allocatedTypes[llvm].push_back(type);

  return allocatedTypes.at(llvm).size();
}

int64_t Module::getMinAllocatedTypeId(const std::string& func) const {
  if(allocatedTypes.contains(func))
    return 1;
  return 0;
}

int64_t Module::getMaxAllocatedTypeId(const std::string& func) const {
  if(allocatedTypes.contains(func))
    return allocatedTypes.at(func).size();
  return 0;
}

int Module::getAllocatedType(const std::string& func, int64_t id) const {
  return allocatedTypes.at(func).at(id - 1);
}

void Module::serialize() const {
  Serializer s;

  s.initialize(serializedFile);
  serialize(s);
  s.finalize();
}

void Module::serialize(Serializer& s) const {
  s.mapStart();

  s.add("lang", lang);
  s.add("allocated", allocatedTypes);
  s.add("directives", directives);
  s.add("libFlang", libFlangFuncs);
  s.add("moduleGlobals", moduleGlobals);

  s.arrStart("globals");
  for(const auto& it : gmap)
    s.serialize(*it.second);
  s.arrEnd();

  s.arrStart("functions");
  for(const auto& it : fmap)
    s.serialize(*it.second);
  s.arrEnd();

  s.arrStart("closures");
  for(const auto& it : cmap)
    s.serialize(*it.second);
  s.arrEnd();

  s.arrStart("structs");
  for(const auto& it : structs)
    s.serialize(*it);
  s.arrEnd();

  s.arrStart("fmodules");
  for(const auto& it : fmodules)
    s.serialize(*it);
  s.arrEnd();

  s.add("tds", tds);
  s.add("localArrays", localArrays);
  s.add("ifaceParams", ifaceParams);
  s.add("layoutDescriptors", layoutDescriptors);

  s.mapEnd();
}

void Module::deserialize() {
  Deserializer d;

  d.initialize(serializedFile);
  deserialize(d);
  d.finalize();
}

void Module::deserialize(Deserializer& d) {
  d.mapStart();

  d.deserialize("lang", lang);
  d.deserialize("allocated", allocatedTypes);
  d.deserialize("directives", directives);
  d.deserialize("libFlang", libFlangFuncs);
  d.deserialize("moduleGlobals", moduleGlobals);

  d.arrStart("globals");
  while(not d.arrEmpty()) {
    GlobalVariable& g = addGlobal();
    d.deserialize(g);
    gmap[g.getLLVMName()] = &g;
  }
  d.arrEnd();

  d.arrStart("functions");
  while(not d.arrEmpty()) {
    Function& f = addFunction();
    d.deserialize(f);
    fmap[f.getLLVMName()] = &f;
  }
  d.arrEnd();

  d.arrStart("closures");
  while(not d.arrEmpty()) {
    Closure& c = addClosure();
    d.deserialize(c);
    cmap[c.getLLVMName()] = &c;
    cfmap[c.getFunctionName()] = &c;
  }
  d.arrEnd();

  d.arrStart("structs");
  while(not d.arrEmpty()) {
    Struct& s = addStruct();
    d.deserialize(s);
    smap[s.getLLVMName()] = &s;
  }
  d.arrEnd();

  d.arrStart("fmodules");
  while(not d.arrEmpty()) {
    FModule& fmod = addFModule();
    d.deserialize(fmod);
    mmap[fmod.getName()] = &fmod;
  }
  d.arrEnd();

  d.deserialize("tds", tds);
  d.deserialize("localArrays", localArrays);
  d.deserialize("ifaceParams", ifaceParams);
  d.deserialize("layoutDescriptors", layoutDescriptors);

  d.mapEnd();
}

} // namespace moya
