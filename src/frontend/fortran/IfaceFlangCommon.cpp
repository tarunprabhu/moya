#include "Globals.h"

// The interfaces are all extern "C" because I don't want to have to deal with
// C++'s ABI and strings

extern "C" void
moya_module_initialize(int f77, int flang1, const char* serializedFile) {
  moya::Module& module = getGlobalSingletonModule();
  if(f77 < 0)
    module.initialize(serializedFile, flang1);
  else if(f77 == 0)
    module.initialize(serializedFile, flang1, SourceLang::Fort);
  else
    module.initialize(serializedFile, flang1, SourceLang::F77);
}

extern "C" void moya_module_serialize() {
  getGlobalSingletonModule().serialize();
}
