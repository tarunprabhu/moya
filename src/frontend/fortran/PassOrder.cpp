#include "RegisterFrontendPass.h"

using namespace llvm;

REGISTER_EARLY_PASS(EarlyInstrument);
REGISTER_PASS(ModuleWrapper);
REGISTER_PASS(Associate);
REGISTER_PASS(PromoteConstExprs);
REGISTER_PASS(InsertMoyaAPIDecls);
REGISTER_PASS(Regions);
REGISTER_PASS(PrepareInstrument);
// REGISTER_PASS(FixLibFlangSignatures);

// IMPORTANT: The types *MUST* be instrumented *BEFORE* the functions because
// the closure types will get correctly handled there and they need to be
// present before the functions are instruments
REGISTER_PASS(FlangBullshit);
REGISTER_PASS(InstrumentTypesCommon);
REGISTER_PASS(InstrumentTypes);
REGISTER_PASS(InstrumentGlobalsCommon);
REGISTER_PASS(InstrumentGlobals);
REGISTER_PASS(InstrumentFunctionsCommon);
REGISTER_PASS(InstrumentFunctions);
REGISTER_PASS(PropagateAnalysisTypes);
REGISTER_PASS(InstrumentInstructionsCommon);
REGISTER_PASS(InstrumentInstructions);
REGISTER_PASS(CleanupMarkers);
REGISTER_PASS(SanityCheck);
REGISTER_PASS(GenerateMoyaSignature);
REGISTER_PASS(GenerateGetters);
REGISTER_PASS(JITProfiling);
// The JIT profiling pass has to go before the file bitcode global is generated
// because it will ensure that when the payloads are generated, the profiling
// will be in the payload
REGISTER_PASS(GenerateFileBitcodeGlobal);
// We don't need the redirection code in the bitcode because it will complicate
// the analysis, but we do need it in the final object file that is generated
REGISTER_PASS(JITRedirect);
REGISTER_LLVM_PASS(UnifyFunctionExitNodes);
REGISTER_PASS(GenerateRedirect);
