#include "Function.h"
#include "Module.h"
#include "common/Deserializer.h"
#include "common/Diagnostics.h"
#include "common/Serializer.h"
#include "common/StringUtils.h"

#include <llvm/Support/raw_ostream.h>

#include <sstream>

using namespace llvm;

namespace moya {

Function::Function(ModuleBase& module,
                   const std::string& llvmName,
                   const std::string& qualName,
                   const std::string& srcName,
                   const Location& loc,
                   bool internal)
    : FunctionBase(module), llvmName(llvmName), qualName(qualName),
      internal(internal) {
  setName(srcName);
  setLocation(loc);
}

void Function::setParent(Function& parent) {
  if(this->parent.length() == 0) {
    this->parent = parent.getLLVMName();
    parent.addChild(*this);
  }
}

void Function::addChild(Function& child) {
  if(not children.contains(child.getLLVMName())) {
    this->children.push_back(child.getLLVMName());
    child.setParent(*this);
  }
}

Type& Function::addAllocatedType(int64_t id, const std::string& llvm) {
  Type& type = getModule().addType(llvm);
  allocatedTypes[id] = &type;

  return type;
}

GEP& Function::addGEP(int64_t cumulative,
                      int64_t offset,
                      bool array,
                      const int64_t* offsets,
                      int64_t count) {
  return *geps.emplace_back(
                  new GEP(*this, cumulative, offset, array, offsets, count))
              .get();
}

bool Function::isInternal() const {
  return internal;
}

bool Function::hasParent() const {
  return parent.length();
}

const Vector<std::string>& Function::getChildren() const {
  return children;
}

const Type& Function::getAllocatedType(int64_t id) const {
  return *allocatedTypes.at(id);
}

const std::string& Function::getParent() const {
  return parent;
}

const std::string& Function::getLLVMName() const {
  return llvmName;
}

const std::string& Function::getQualifiedName() const {
  return qualName;
}

Module& Function::getModule() {
  return static_cast<Module&>(FunctionBase::getModule());
}

Argument& Function::getArg(unsigned i) {
  return static_cast<Argument&>(FunctionBase::getArg(i));
}

Argument& Function::getArg(const std::string& name) {
  return static_cast<Argument&>(FunctionBase::getArg(StringUtils::lower(name)));
}

bool Function::hasArg(const std::string& arg) const {
  return FunctionBase::hasArg(StringUtils::lower(arg));
}

const Module& Function::getModule() const {
  return static_cast<const Module&>(FunctionBase::getModule());
}

const Argument& Function::getArg(unsigned i) const {
  return static_cast<const Argument&>(FunctionBase::getArg(i));
}

const Argument& Function::getArg(const std::string& name) const {
  return static_cast<const Argument&>(
      FunctionBase::getArg(StringUtils::lower(name)));
}

void Function::setLLVM(GEP& gep, llvm::GetElementPtrInst* llvm) {
  gep.setLLVM(llvm);
  gepMap[llvm] = &gep;
}

GEP& Function::getGEP(int64_t i) {
  return *geps.at(i).get();
}

GEP& Function::getGEP(llvm::GetElementPtrInst* gep) {
  return *gepMap.at(gep);
}

const GEP& Function::getGEP(int64_t i) const {
  return *geps.at(i).get();
}

const GEP& Function::getGEP(llvm::GetElementPtrInst* gep) const {
  return *gepMap.at(gep);
}

void Function::serialize(Serializer& s) const {
  s.mapStart();

  s.add("name", name);
  s.add("llvmName", llvmName);
  s.add("qualName", qualName);
  s.add("artificialName", artificialName);
  s.add("artificial", artificial);
  s.add("location", loc);
  s.add("children", children);
  s.add("parent", parent);
  s.add("internal", isInternal());

  s.arrStart("args");
  for(unsigned i = 0; i < getNumArgs(); i++)
    s.serialize(getArg(i));
  s.arrEnd();

  s.arrStart("directives");
  for(const Directive* directive : directives)
    s.serialize(*directive);
  s.arrEnd();

  s.add("allocated", allocatedTypes);
  s.add("geps", geps);

  s.mapEnd();
}

void Function::deserialize(Deserializer& d) {
  d.mapStart();

  d.deserialize("name", name);
  d.deserialize("llvmName", llvmName);
  d.deserialize("qualName", qualName);
  d.deserialize("artificialName", artificialName);
  d.deserialize("artificial", artificial);
  d.deserialize("location", loc);
  d.deserialize("children", children);
  d.deserialize("parent", parent);
  d.deserialize("internal", internal);

  d.arrStart("args");
  while(not d.arrEmpty())
    addArgument<Argument>().deserialize(d);
  d.arrEnd();

  d.arrStart("directives");
  while(not d.arrEmpty())
    addDirective(Directive::deserializeFrom(d));
  d.arrEnd();

  d.mapStart("allocated");
  while(not d.mapEmpty()) {
    int64_t id;
    Type& type = getModule().addType();
    d.deserialize(id, type);
    allocatedTypes[id] = &type;
  }
  d.mapEnd();

  d.arrStart("geps");
  while(not d.arrEmpty()) {
    geps.emplace_back(new GEP(*this));
    d.deserialize(*geps.back().get());
  }
  d.arrEnd();

  d.mapEnd();
}

} // namespace moya
