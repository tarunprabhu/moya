#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/Set.h"
#include "common/Vector.h"

#include <llvm/IR/InstIterator.h>

using namespace llvm;

class FlangBullshitPass : public ModulePass {
public:
  FlangBullshitPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);

public:
  static char ID;
};

FlangBullshitPass::FlangBullshitPass() : ModulePass(ID) {
  initializeFlangBullshitPassPass(*PassRegistry::getPassRegistry());
}

StringRef FlangBullshitPass::getPassName() const {
  return "Moya Flang Bullshit";
}

void FlangBullshitPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.setPreservesCFG();
}

static moya::Vector<CallInst*> getCalls(Argument& arg) {
  moya::Vector<CallInst*> calls;

  // Flang always casts the function pointer to the right type before calls
  for(Use& u : arg.uses())
    if(auto* cst = dyn_cast<CastInst>(u.getUser()))
      for(Use& v : cst->uses())
        if(auto* call = dyn_cast<CallInst>(v.getUser()))
          calls.push_back(call);

  return calls;
}

static void collectOperands(Instruction* curr, moya::Set<Instruction*>& insts) {
  // We want to ignore all instructions that lead to the argument of the call
  // that we want to ignore. But we shouldn't mark instructions that may be
  // used elsewhere. This risks failures anyway, but it's Flang. We don't
  // really expect any better now, do we?
  //
  if(curr->getNumUses() == 1) {
    if(insts.insert(curr))
      for(Use& op : curr->operands())
        if(auto* inst = dyn_cast<Instruction>(op.get()))
          collectOperands(inst, insts);
  }
}

static moya::Set<Instruction*> collectOperands(Value* v) {
  moya::Set<Instruction*> insts;
  if(auto* inst = dyn_cast<Instruction>(v))
    collectOperands(inst, insts);

  return insts;
}

bool FlangBullshitPass::runOnModule(Module& module) {
  bool changed = false;

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  for(Function& f : module.functions()) {
    if(feModule.hasFunction(f)) {
      // Because Flang has these flights of delightful whimsy, there are times
      // when Flang passes an "extra" argument to a function. This only seems to
      // happen when the callee is passed as an argument to the caller.
      //
      // By itself, this bit of insanity is not necessarily problematic. The
      // trouble seems to be that when it does so, it does the equivalent of an
      // inttoptr which Moya really doesn't like. Because this is a superflous
      // action in the first place, it helps to determine the correct number of
      // arguments expected by the callee and then ignore all the instructions
      // that are associated with generating this operand
      //
      const moya::Function& feFunc = feModule.getFunction(f);
      for(Argument& arg : f.args()) {
        Type* param = feFunc.getArg(arg.getArgNo()).getType().getLLVM();
        if(moya::LLVMUtils::isPtrToFunctionTy(param)) {
          FunctionType* fty
              = cast<FunctionType>(cast<PointerType>(param)->getElementType());
          for(CallInst* call : getCalls(arg))
            for(unsigned i = fty->getNumParams(); i < call->getNumArgOperands();
                i++)
              for(Instruction* inst : collectOperands(call->getArgOperand(i)))
                changed |= moya::Metadata::setIgnore(inst);
        }
      }
    }
  }

  return changed;
}

char FlangBullshitPass::ID = 0;

static const char* name = "moya-flang-bullshit";
static const char* descr
    = "Deal with some of the most spectacular garbage that Flang produces";
static const bool cfg = false;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(FlangBullshitPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_END(FlangBullshitPass, name, descr, cfg, analysis)

void registerFlangBullshitPass(const llvm::PassManagerBuilder&,
                               llvm::legacy::PassManagerBase& pm) {
  pm.add(new FlangBullshitPass());
}

llvm::Pass* createFlangBullshitPass() {
  return new FlangBullshitPass();
}
