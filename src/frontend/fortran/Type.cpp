#include "Type.h"
#include "Module.h"
#include "common/Deserializer.h"
#include "common/Diagnostics.h"
#include "common/Parse.h"
#include "common/Serializer.h"

#include <llvm/Support/raw_ostream.h>

namespace moya {

Type::Type(ModuleBase& module, const std::string& llvmStr)
    : TypeBase(module), llvmStr(llvmStr) {
  ;
}

const std::string& Type::getLLVMStr() const {
  return llvmStr;
}

Module& Type::getModule() {
  return static_cast<Module&>(TypeBase::getModule());
}

const Module& Type::getModule() const {
  return static_cast<const Module&>(TypeBase::getModule());
}

void Type::serialize(Serializer& s) const {
  s.mapStart();
  s.add("llvmstr", llvmStr);
  s.mapEnd();
}

void Type::deserialize(Deserializer& d) {
  d.mapStart();
  d.deserialize("llvmstr", llvmStr);
  d.mapEnd();
}

} // namespace moya
