#ifndef MOYA_FRONTEND_FORTRAN_MODULE_H
#define MOYA_FRONTEND_FORTRAN_MODULE_H

#include "Closure.h"
#include "FModule.h"
#include "Function.h"
#include "GlobalVariable.h"
#include "Struct.h"
#include "Type.h"
#include "common/APITypes.h"
#include "common/Location.h"
#include "common/Map.h"
#include "common/Set.h"
#include "common/Vector.h"
#include "frontend/components/ModuleBase.h"

#include <functional>
#include <memory>

namespace moya {

class Serializer;
class Deserializer;
class DirectiveContext;

class Module : public ModuleBase {
private:
  Vector<std::unique_ptr<Closure>> closures;
  Vector<std::unique_ptr<Struct>> structs;
  Vector<std::unique_ptr<FModule>> fmodules;

protected:
  std::string serializedFile;

  // Used in flang1 to record the DTYPE of the allocated/deallocated
  // in each function. In flang2, this will be converted into a suitable
  // LLVM type.
  // The key in the map is a string that uniquely identifies the function
  // being processed. It is the mangled LLVM name of the function.
  // A pair of marker functions will be added around each
  // allocation/deallocation site. The function will accept a single integer
  // argument (arg). (arg - 1) will be the index of the type allocated in the
  // vector below.
  Map<std::string, Vector<int>> allocatedTypes;

  // The variable names of the local variables in the functions that are
  // pointers to arrays. These will eventually become allocas in the LLVM
  //
  // In flang2, additional entries are added to this. The flang1 entries
  // will not be used in flang2 or in Clang. We should try to get rid of it
  // but who cares
  Map<std::string, Set<std::string>> localArrays;

  // Types for the parameter of a function that is an interface
  Map<std::string, Map<unsigned, Vector<int>>> ifaceParams;

  // Used in flang1 to keep track of the variables that correspond to the
  // hoisted byte arrays for module-level variables. The explanation for
  // the different categories is in flang/tools/flang2/flang2exe/moya.cpp
  Map<std::string, std::string> moduleGlobals;

  // The names of the structs that are type descriptors
  Set<std::string> tds;

  // Names of the global variables that are layout descriptors
  Set<std::string> layoutDescriptors;

  // Keeps track of the "fake" globals (STATIC{%d+} and BSS{%d+})
  Map<std::string, GlobalVariable*> gmap;
  Map<std::string, Function*> fmap;
  Map<std::string, Closure*> cmap;
  Map<std::string, FModule*> mmap;
  Map<std::string, Closure*> cfmap;
  Map<llvm::StructType*, Closure*> llvmClosureMap;

  // Keeps track of the structs
  Map<std::string, Struct*> smap;
  Map<llvm::StructType*, Struct*> llvmStructMap;

  // The functions defined in the Flang runtime library
  Set<std::string> libFlangFuncs;

protected:
  void serialize(Serializer&) const;
  void deserialize(Deserializer&);

public:
  Module();
  Module(const Module&) = delete;
  virtual ~Module() = default;

  void addLibFlangFunction(const std::string&);
  bool isLibFlangFunction(const std::string&) const;

  void
  initialize(const std::string&, bool, SourceLang = SourceLang::UnknownLang);

  // void addModule(const std::string&, const std::string&, unsigned, unsigned);

  template <typename... ArgsT>
  Function& addFunction(ArgsT&&... args) {
    Function& func = ModuleBase::addFunction<Function>(args...);
    fmap[func.getLLVMName()] = &func;

    return func;
  }

  template <typename... ArgsT>
  Function& addFunction() {
    return ModuleBase::addFunction<Function>();
  }

  template <typename... ArgsT>
  GlobalVariable& addGlobal(ArgsT&&... args) {
    GlobalVariable& global = ModuleBase::addGlobal<GlobalVariable>(args...);
    gmap[global.getLLVMName()] = &global;

    return global;
  }

  template <typename... ArgsT>
  GlobalVariable& addGlobal() {
    return ModuleBase::addGlobal<GlobalVariable>();
  }

  template <typename... ArgsT>
  Type& addType(ArgsT&&... args) {
    return ModuleBase::addType<Type>(args...);
  }

  template <typename... ArgsT>
  Type& addType() {
    return ModuleBase::addType<Type>();
  }

  template <typename... ArgsT>
  FModule& addFModule(ArgsT&&... args) {
    FModule& fmod = *fmodules.emplace_back(new FModule(*this, args...));
    if(fmod.getName().size())
      mmap[fmod.getName()] = &fmod;
    return fmod;
  }
  bool hasFModule(const std::string&) const;
  FModule& getFModule(const std::string&);
  const FModule& getFModule(const std::string&) const;

  void addTypeDesc(const std::string&);
  bool isTypeDesc(const std::string&) const;

  using ModuleBase::hasFunction;
  bool hasFunction(const std::string&) const;
  Function& getFunction(llvm::Function&);
  Function& getFunction(const std::string&);
  const Function& getFunction(llvm::Function&) const;
  const Function& getFunction(const std::string&) const;

  using ModuleBase::hasGlobal;
  bool hasGlobal(const std::string&) const;
  GlobalVariable& getGlobal(llvm::GlobalVariable&);
  GlobalVariable& getGlobal(const std::string&);
  const GlobalVariable& getGlobal(llvm::GlobalVariable&) const;
  const GlobalVariable& getGlobal(const std::string&) const;

  template <typename... ArgsT>
  Closure& addClosure(ArgsT&&... args) {
    Closure& closure = *closures.emplace_back(new Closure(*this, args...));
    if(closure.getLLVMName().size())
      cmap[closure.getLLVMName()] = &closure;
    if(closure.getFunctionName().size())
      cfmap[closure.getFunctionName()] = &closure;
    return closure;
  }
  bool hasClosure(const std::string&) const;
  bool hasClosure(llvm::Function&) const;
  bool hasClosure(llvm::StructType*) const;
  Closure& getClosure(const std::string&);
  Closure& getClosure(llvm::Function&);
  Closure& getClosure(llvm::StructType*);
  const Closure& getClosure(const std::string&) const;
  const Closure& getClosure(llvm::Function&) const;
  const Closure& getClosure(llvm::StructType*) const;
  const Vector<std::unique_ptr<Closure>>& getClosures() const;

  template <typename... ArgsT>
  Struct& addStruct(ArgsT&&... args) {
    Struct& strct = *structs.emplace_back(new Struct(*this, args...));
    if(strct.getLLVMName().size())
      smap[strct.getLLVMName()] = &strct;
    return strct;
  }
  bool hasStruct(const std::string&) const;
  bool hasStruct(llvm::StructType*) const;
  Struct& getStruct(const std::string&);
  Struct& getStruct(llvm::StructType*);
  const Struct& getStruct(const std::string&) const;
  const Struct& getStruct(llvm::StructType*) const;
  const Vector<std::unique_ptr<Struct>>& getStructs() const;

  using ModuleBase::setLLVM;
  void setLLVM(Closure&, llvm::StructType*);
  void setLLVM(Struct&, llvm::StructType*);

  void addLocalArray(const std::string&,
                     const std::string&,
                     const std::string&,
                     const std::string&);
  void
  addLocalArray(const std::string&, const std::string&, const std::string&);
  bool isLocalArray(const std::string&, const std::string&) const;

  void addInterfaceParam(const std::string&,
                         const std::string&,
                         const std::string&,
                         unsigned,
                         int);
  const Vector<int>& getInterfaceParams(const std::string&, unsigned) const;

  int64_t addAllocatedType(const std::string&,
                           const std::string&,
                           const std::string&,
                           int);
  int64_t getMinAllocatedTypeId(const std::string&) const;
  int64_t getMaxAllocatedTypeId(const std::string&) const;
  int getAllocatedType(const std::string&, int64_t) const;

  void addLayoutDescriptor(const std::string&);
  bool isLayoutDescriptor(const std::string&) const;
  const Set<std::string>& getLayoutDescriptors() const;

  void addModuleGlobalCategory(const std::string&, const std::string&);
  bool isModuleGlobalCategory(const std::string&) const;
  const std::string& getModuleGlobalModule(const std::string&) const;

  /* Right now, the serialize/deserialize functions only deal with the
     source file information. */
  void serialize() const;
  void deserialize();

  /* Used in flang1 */
  int parseDirective(const std::string&,
                     const std::string&,
                     unsigned,
                     unsigned);
  RegionID getRegionId(const Location&) const;

public:
  static const int64_t ArrayIndexMarker = -1;
};

} // namespace moya

#endif // MOYA_FRONTEND_FLANG_MOYA_INFO_H
