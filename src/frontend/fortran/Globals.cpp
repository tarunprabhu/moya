#include "Globals.h"
#include "Module.h"
#include "common/Location.h"
#include "frontend/components/DirectiveContext.h"

// A single instance of a module object which we have to keep around because
// for flang, there isn't a more reasonable place to put it and have it be
// carried around
static moya::Module module;

__attribute__((constructor)) static void MoyaFrontendFortran_init() {
  ;
}

__attribute__((destructor)) static void MoyaFrontendFortran_fini() {
  ;
}

extern "C" int moya_is_frontend_fortran() {
  return true;
}

moya::Module& getGlobalSingletonModule() {
  return module;
}
