#include "FlangRTL.h"
#include "Function.h"
#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/AerynConfig.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "frontend/all-langs/InstrumentUtils.h"

#include <llvm/IR/InstIterator.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

class InstrumentFunctionsPass : public ModulePass {
public:
  static char ID;

protected:
  bool instrumentDirectives(const moya::Function&, Function&);
  bool instrumentFunction(const moya::Function&, Function&);
  bool instrumentRTLFunction(Function&);

public:
  InstrumentFunctionsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

InstrumentFunctionsPass::InstrumentFunctionsPass() : ModulePass(ID) {
  llvm::initializeInstrumentFunctionsPassPass(*PassRegistry::getPassRegistry());
}

StringRef InstrumentFunctionsPass::getPassName() const {
  return "Moya Instrument Functions (Fortran)";
}

void InstrumentFunctionsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.setPreservesCFG();
}

bool InstrumentFunctionsPass::instrumentDirectives(const moya::Function& feFunc,
                                                   Function& f) {
  bool changed = false;

  changed |= moya::InstrumentUtils::instrumentDirectives(feFunc, f);

  for(const moya::FunctionDirective* dir : feFunc.getDirectives()) {
    if(const auto* specialize = dyn_cast<moya::SpecializeDirective>(dir)) {
      for(const std::string& argName : specialize->getIgnoreAllArgNames()) {
        if(feFunc.hasArg(argName)) {
          const moya::Argument& feArg = feFunc.getArg(argName);

          int argNum = feArg.getArgNum();
          llvm::Argument& arg = *moya::LLVMUtils::getArgument(f, argNum);
          changed |= moya::Metadata::setIgnore(arg);

          int linkedNum = feArg.getLinked().getArgNum();
          llvm::Argument& linked = *moya::LLVMUtils::getArgument(f, linkedNum);
          changed |= moya::Metadata::setIgnore(linked);
        }
      }
    }
  }

  return changed;
}

bool InstrumentFunctionsPass::instrumentFunction(const moya::Function& feFunc,
                                                 Function& f) {
  bool changed = false;
  Module& module = *moya::LLVMUtils::getModule(f);

  changed |= moya::Metadata::setSourceLang(f, feFunc.getSourceLang());
  changed |= moya::Metadata::setQualifiedName(f, feFunc.getQualifiedName());
  changed |= moya::Metadata::setSourceName(f, feFunc.getName());
  changed |= moya::Metadata::setSourceLocation(f, feFunc.getLocation());
  if(feFunc.hasBeginLoc() and feFunc.hasEndLoc()) {
    changed |= moya::Metadata::setBeginLocation(f, feFunc.getBeginLoc());
    changed |= moya::Metadata::setEndLocation(f, feFunc.getEndLoc());
  }

  if(feFunc.isInternal())
    changed |= moya::Metadata::setEnclosing(
        f, module.getFunction(feFunc.getParent()));

  for(unsigned i = 0; i < feFunc.getNumArgs(); i++) {
    const moya::Argument& feArg = feFunc.getArg(i);
    Argument& arg = *moya::LLVMUtils::getArgument(f, feArg.getArgNum());
    Type* type
        = moya::LLVMUtils::getCanonicalType(feArg.getType().getLLVM(), module);
    if(feFunc.isInternal() and (i == (feFunc.getNumArgs() - 1))) {
      // This argument will be a pointer to the closure.
      StructType* orig
          = dyn_cast<StructType>(dyn_cast<PointerType>(type)->getElementType());
      type = moya::Metadata::getFlangClosure(orig, module)->getPointerTo();
    }

    if(feArg.isArtificial()) {
      changed
          |= moya::Metadata::setArtificialName(arg, feArg.getArtificialName());
      changed |= moya::Metadata::setArtificial(arg);
    } else {
      changed |= moya::Metadata::setSourceName(arg, feArg.getName());
      changed |= moya::Metadata::setSourceLocation(arg, feFunc.getLocation());
    }
    changed |= moya::Metadata::setAnalysisType(arg, type);
    if(feArg.isArray())
      changed |= moya::Metadata::setFlangArray(arg);
    if(feArg.isLinked())
      changed
          |= moya::Metadata::setFlangLinked(arg, feArg.getLinked().getArgNum());
  }

  return changed;
}

bool InstrumentFunctionsPass::instrumentRTLFunction(Function& f) {
  bool changed = false;
  const std::string& fname = f.getName();

  if(FlangRTL::isAllocatorFunction(fname))
    changed |= moya::Metadata::setFlangAllocator(f);

  if(FlangRTL::isDeallocatorFunction(fname))
    changed |= moya::Metadata::setFlangDeallocator(f);

  if(FlangRTL::isMallocLikeFunction(fname))
    changed |= moya::Metadata::setFlangMalloc(f);

  if(FlangRTL::isFreeLikeFunction(fname))
    changed |= moya::Metadata::setSystemFree(f);

  if(FlangRTL::isMemcpyLikeFunction(fname))
    changed |= moya::Metadata::setFlangMemcpy(f);

  if(FlangRTL::isMemsetLikeFunction(fname))
    changed |= moya::Metadata::setFlangMemset(f);

  return changed;
}

bool InstrumentFunctionsPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  for(Function& f : module.functions()) {
    const std::string& fname = f.getName();
    if(fname == "MAIN_")
      changed |= moya::Metadata::setMain(f);

    changed |= instrumentRTLFunction(f);

    if(feModule.hasFunction(fname)) {
      const moya::Function& feFunc = feModule.getFunction(fname);

      changed |= instrumentFunction(feFunc, f);
      changed |= instrumentDirectives(feFunc, f);
    }

    if(f.size()) {
      if(not moya::Metadata::hasSourceLang(f))
        changed |= moya::Metadata::setSourceLang(f, feModule.getSourceLang());
    } else {
      // Because this is Fortran, there are no function prototypes and all
      // functions are vararg and cast before use. Try and figure out their
      // signatures based on their uses
      moya::Set<Type*> casts;
      for(Use& u : f.uses()) {
        if(auto* cst = dyn_cast<CastInst>(u.getUser()))
          casts.insert(cast<PointerType>(cst->getDestTy())->getElementType());
        else if(auto* call = dyn_cast<CallInst>(u.getUser()))
          ;
        else
          moya_error("Unexpected use of library function: " << *call);
      }
      if(casts.size() == 1) {
        FunctionType* fty = cast<FunctionType>(*casts.begin());
        Type* retTy = fty->getReturnType();
        bool pure = true;
        if(not(moya::LLVMUtils::isScalarTy(retTy)
               or moya::LLVMUtils::isScalarStructTy(retTy)
               or moya::LLVMUtils::isVoidTy(retTy)))
          pure = false;
        for(Type* argTy : fty->params())
          if(not moya::LLVMUtils::isScalarTy(argTy))
            pure = false;
        if(pure) {
          changed |= moya::Metadata::setPure(f);
          changed |= moya::Metadata::setModel(
              f, moya::AerynConfig::getPureFunctionModel());
        }
      }
    }
  }

  return changed;
}

char InstrumentFunctionsPass::ID = 0;

static const char* name = "moya-instr-funcs-fort";
static const char* descr = "Adds instrumentation to functions (Fortran)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(InstrumentFunctionsPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_END(InstrumentFunctionsPass, name, descr, cfg, analysis)

void registerInstrumentFunctionsPass(const PassManagerBuilder&,
                                     legacy::PassManagerBase& pm) {
  pm.add(new InstrumentFunctionsPass());
}

Pass* createInstrumentFunctionsPass() {
  return new InstrumentFunctionsPass();
}
