#include "Struct.h"
#include "Module.h"
#include "Type.h"
#include "common/Deserializer.h"
#include "common/Serializer.h"
#include "common/StringUtils.h"

namespace moya {

Struct::Field::Field(const std::string& name,
                     unsigned offset,
                     const std::string& str,
                     bool descriptor)
    : name(name), offset(offset), str(str), descriptor(descriptor),
      type(nullptr) {
  ;
}

void Struct::Field::serialize(Serializer& s) const {
  s.mapStart("field");
  s.add("name", name);
  s.add("offset", offset);
  s.add("type", str);
  s.add("descriptor", descriptor);
  s.mapEnd();
}

void Struct::Field::deserialize(Deserializer& d) {
  d.mapStart("field");
  d.deserialize("name", name);
  d.deserialize("offset", offset);
  d.deserialize("type", str);
  d.deserialize("descriptor", descriptor);
  d.mapEnd();
}

Struct::Struct(Module& module, const std::string& name, bool fake)
    : module(module), name(name), llvm(nullptr), fake(fake) {
  ;
}

Module& Struct::getModule() {
  return module;
}

const Module& Struct::getModule() const {
  return module;
}

const Location& Struct::getLocation() const {
  return loc;
}

llvm::StructType* Struct::getLLVM() const {
  return llvm;
}

const std::string& Struct::getLLVMName() const {
  return name;
}

size_t Struct::getNumFields() const {
  return fields.size();
}

bool Struct::isFake() const {
  return fake;
}

const std::string& Struct::getName(unsigned i) const {
  return fields.at(i).name;
}

unsigned Struct::getOffset(unsigned i) const {
  return fields.at(i).offset;
}

const std::string& Struct::getTypeStr(unsigned i) const {
  return fields.at(i).str;
}

const Type& Struct::getType(unsigned i) const {
  return *fields.at(i).type;
}

bool Struct::isDescriptor(unsigned i) const {
  return fields.at(i).descriptor;
}

void Struct::addType(const std::string& name,
                     unsigned offset,
                     const std::string& type,
                     bool descriptor) {
  // Don't create a moya::Type here. This is because Flang helpfully creates
  // an enormous number of structs, one from each module used in each
  // function. Obviously, they are all the same struct, but they end up with
  // different names. LLVM then additionally uniques them, but they all end up
  // here. This means that if create a moya::Type for all of them, we end up
  // with a number of errors because AssociatePass will attempt to find
  // structs for all of them in the module. Instead, we will only create types
  // for those structs that actually appear in the module
  //
  fields.emplace_back(name, offset, type, descriptor);
}

void Struct::finalizeTypes() {
  for(unsigned i = 0; i < getNumFields(); i++)
    fields.at(i).type = &getModule().addType(getTypeStr(i));
}

void Struct::setLLVM(llvm::StructType* llvm) {
  this->llvm = llvm;
  getModule().setLLVM(*this, llvm);
}

void Struct::serialize(Serializer& s) const {
  s.mapStart();

  s.add("name", name);
  s.add("fake", fake);
  s.add("fields", fields);

  s.mapEnd();
}

void Struct::deserialize(Deserializer& d) {
  d.mapStart();

  d.deserialize("name", name);
  d.deserialize("fake", fake);
  d.deserialize("fields", fields);

  d.mapEnd();
}

} // namespace moya
