#ifndef MOYA_FRONTEND_FORTRAN_FUNCTION_H
#define MOYA_FRONTEND_FORTRAN_FUNCTION_H

#include "Argument.h"
#include "GEP.h"
#include "Struct.h"
#include "common/Map.h"
#include "common/Set.h"
#include "common/Vector.h"
#include "frontend/components/FunctionBase.h"

#include <string>

namespace moya {

class Module;
class Serializer;
class Deserializer;

class Function : public FunctionBase {
protected:
  // The mangled name as seen in the LLVM source
  std::string llvmName;

  // The qualified name constructed from the module name and the parent
  // along with the function name (if any). This is a bit easier than
  // reading the mangled LLVM name
  std::string qualName;

  // If this is function is part of a module, the name of the module,
  // If this is an internal function, the name of the outer function
  std::string parent;

  // These are the inner functions if any of this function
  Vector<std::string> children;

  // Is this an internal function
  bool internal;

  // The types that are allocated or deallocated in this function
  // The key in the map is the argument to a marker function in the code
  // that will be processed by a pass to associate the type with the callsite
  Map<int64_t, Type*> allocatedTypes;

  // The GEP instructions that were seen with the offsets into the actual
  // type
  Vector<std::unique_ptr<GEP>> geps;
  Map<llvm::GetElementPtrInst*, GEP*> gepMap;

protected:
  Function(ModuleBase&,
           const std::string& = "",
           const std::string& = "",
           const std::string& = "",
           const Location& = Location(),
           bool = false);

public:
  Function(const Function&) = delete;
  virtual ~Function() = default;

  void setParent(Function&);
  void addChild(Function&);
  Type& addAllocatedType(int64_t, const std::string&);

  template <typename... ArgsT>
  Argument& addArgument(ArgsT&&... args) {
    return FunctionBase::addArgument<Argument>(args...);
  }

  template <typename... ArgsT>
  Argument& addArgument() {
    return FunctionBase::addArgument<Argument>();
  }

  GEP& addGEP(int64_t, int64_t, bool, const int64_t*, int64_t);

  bool isInternal() const;
  bool hasParent() const;

  Module& getModule();
  bool hasArg(const std::string&) const;
  Argument& getArg(unsigned);
  Argument& getArg(const std::string&);
  const Module& getModule() const;
  const Argument& getArg(unsigned) const;
  const Argument& getArg(const std::string&) const;

  const Type& getAllocatedType(int64_t) const;
  const std::string& getLLVMName() const;
  const std::string& getQualifiedName() const;
  const std::string& getParent() const;
  const Vector<std::string>& getChildren() const;

  GEP& getGEP(int64_t);
  GEP& getGEP(llvm::GetElementPtrInst*);
  const GEP& getGEP(int64_t) const;
  const GEP& getGEP(llvm::GetElementPtrInst*) const;

  void setLLVM(GEP&, llvm::GetElementPtrInst*);

  virtual void serialize(Serializer&) const;
  void deserialize(Deserializer&);

  friend class ModuleBase;
};

} // namespace moya

#endif // MOYA_FRONTEND_FORTRAN_FUNCTION_H
