#include "Passes.h"
#include "common/LLVMUtils.h"
#include "common/Vector.h"

#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>

using namespace llvm;

class CleanupMarkersPass : public ModulePass {
public:
  CleanupMarkersPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);

public:
  static char ID;
};

CleanupMarkersPass::CleanupMarkersPass() : ModulePass(ID) {
  initializeCleanupMarkersPassPass(*PassRegistry::getPassRegistry());
}

StringRef CleanupMarkersPass::getPassName() const {
  return "Cleanup Fortran Markers";
}

void CleanupMarkersPass::getAnalysisUsage(AnalysisUsage& AU) const {
  ;
}

bool CleanupMarkersPass::runOnModule(Module& module) {
  bool changed = false;

  moya::Vector<Instruction*> todelete;
  for(Function& f : module.functions())
    for(auto i = inst_begin(f); i != inst_end(f); i++)
      if(auto* call = dyn_cast<CallInst>(&*i))
        if(Function* callee = moya::LLVMUtils::getCalledFunction(call))
          if(callee->getName() == "__moya_marker_func")
            todelete.push_back(call);

  for(Instruction* inst : todelete)
    inst->eraseFromParent();
  changed |= todelete.size();

  return changed;
}

char CleanupMarkersPass::ID = 0;

static const char* name = "moya-cleanup-markers";
static const char* descr = "Remove any markers inserted during the parser";
static const bool cfg = false;
static const bool analysis = false;

INITIALIZE_PASS(CleanupMarkersPass, name, descr, cfg, analysis)

void registerCleanupMarkersPass(const PassManagerBuilder&,
                                legacy::PassManagerBase& pm) {
  pm.add(new CleanupMarkersPass());
}

Pass* createCleanupMarkersPass() {
  return new CleanupMarkersPass();
}
