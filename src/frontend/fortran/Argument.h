#ifndef MOYA_FRONTEND_FORTRAN_ARGUMENT_H
#define MOYA_FRONTEND_FORTRAN_ARGUMENT_H

#include "Type.h"
#include "frontend/components/ArgumentBase.h"

#include <string>

namespace moya {

class Function;
class Serializer;
class Deserializer;

class Argument : public ArgumentBase {
protected:
  Type& type;

  // If an argument has a POINTER or ALLOCATABLE attribute or if it is an
  // assumed length string, an additional "dummy" argument will be passed
  // which is the descriptor (for arrays/pointers) or the length for strings.
  // If this is the dummy argument, linked will be the argument number of the
  // argument in the source. If this is the original argument, then linked will
  // be the argument number of the corresponding dummy argument
  int linked;

  // FIXME: At the time of first adding this attribute, it is only true if
  // the argument is a pointer to an array (as opposed to a pointer to a single
  // instance of the type). I *think* it can be both assumed size and
  // assumed shape, but that's not really relevant. I am not sure if this
  // will be true for fixed length arrays or strings. It should get properly
  // sorted at some point, but by then, this should be dead and buried
  // (althoug knowing my luck, I think the putrefaction will slowly rise to
  // the surface and pollute everything)
  bool array;

  // This argument is a closure passed to an internal function
  bool closure;

  llvm::Argument* llvm;

public:
  Argument(FunctionBase&);
  Argument(FunctionBase&,
           const std::string&,
           const std::string&,
           int,
           bool,
           bool,
           bool);
  Argument(const Argument&) = delete;
  virtual ~Argument() = default;

  const Type& getType() const;
  const Argument& getLinked() const;
  const Function& getFunction() const;
  bool isArray() const;
  bool isLinked() const;
  bool isClosure() const;
  llvm::Argument* getLLVM() const;

  void setLLVM(llvm::Argument*);

  void serialize(Serializer&) const;
  void deserialize(Deserializer&);

  friend class FunctionBase;
};

} // namespace moya

#endif // MOYA_FRONTEND_FORTRAN_ARGUMENT_H
