#ifndef MOYA_FRONTEND_FORTRAN_TYPE_H
#define MOYA_FRONTEND_FORTRAN_TYPE_H

#include "frontend/components/TypeBase.h"

#include <llvm/IR/Module.h>

namespace moya {

class Module;
class Serializer;
class Deserializer;

class Type : public TypeBase {
protected:
  std::string llvmStr;

protected:
  Type(ModuleBase&, const std::string& = "");

public:
  Type(const Type&) = delete;
  virtual ~Type() = default;

  Module& getModule();
  const Module& getModule() const;
  const std::string& getLLVMStr() const;

  void serialize(Serializer&) const;
  void deserialize(Deserializer&);

  friend class ModuleBase;
};

} // namespace moya

#endif // MOYA_FRONTEND_FORTRAN_TYPE_H
