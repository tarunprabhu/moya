#include "FModule.h"
#include "GlobalVariable.h"
#include "Module.h"
#include "common/Deserializer.h"
#include "common/Serializer.h"

namespace moya {

FModule::FModule(Module& module, const std::string& name, const Location& loc)
    : module(module), name(name), loc(loc) {
  ;
}

void FModule::addFakeGlobal(const GlobalVariable& g) {
  fakeGlobals.insert(&g);
}

const std::string& FModule::getName() const {
  return name;
}

const Location& FModule::getLocation() const {
  return loc;
}

const moya::Set<const GlobalVariable*>& FModule::getFakeGlobals() const {
  return fakeGlobals;
}

void FModule::serialize(Serializer& s) const {
  s.mapStart();
  s.add("name", name);
  s.add("loc", loc);
  s.arrStart("fakes");
  for(const GlobalVariable* g : fakeGlobals)
    s.serialize(g->getName());
  s.arrEnd();
  s.mapEnd();
}

void FModule::deserialize(Deserializer& d) {
  d.mapStart();
  d.deserialize("name", name);
  d.deserialize("loc", loc);
  d.arrStart("fakes");
  while(not d.arrEmpty()) {
    std::string gname;
    d.deserialize(gname);
    addFakeGlobal(module.getGlobal(gname));
  }
  d.arrEnd();
  d.mapEnd();
}

} // namespace moya
