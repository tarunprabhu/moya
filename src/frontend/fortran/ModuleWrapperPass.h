#ifndef MOYA_FRONTEND_FORTRAN_MODULE_WRAPPER_PASS_H
#define MOYA_FRONTEND_FORTRAN_MODULE_WRAPPER_PASS_H

#include "Module.h"
#include "frontend/components/DirectiveContext.h"

#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

class ModuleWrapperPass : public llvm::ModulePass {
public:
  static char ID;

protected:
  moya::Module feModule;

public:
  ModuleWrapperPass();

  moya::Module& getModule();
  const moya::Module& getModule() const;

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);
};

#endif // MOYA_FRONTEND_FORTRAN_MODULE_WRAPPER_PASS_H
