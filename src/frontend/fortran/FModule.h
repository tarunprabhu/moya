#ifndef MOYA_FRONTEND_FORTRAN_FMODULE_H
#define MOYA_FRONTEND_FORTRAN_FMODULE_H

#include "common/Location.h"
#include "common/Set.h"

#include <string>

namespace moya {

class GlobalVariable;
class Module;
class Serializer;
class Deserializer;

class FModule {
private:
  Module& module;
  std::string name;
  Location loc;
  moya::Set<const GlobalVariable*> fakeGlobals;

public:
  FModule(Module&, const std::string& = "", const Location& loc = Location());
  virtual ~FModule() = default;

  void addFakeGlobal(const GlobalVariable&);

  const std::string& getName() const;
  const Location& getLocation() const;
  const moya::Set<const GlobalVariable*>& getFakeGlobals() const;

  void serialize(Serializer&) const;
  void deserialize(Deserializer&);
};

} // namespace moya

#endif // MOYA_FRONTEND_FORTRAN_FMODULE_H
