#ifndef MOYA_FRONTEND_FORTRAN_CLOSURE_H
#define MOYA_FRONTEND_FORTRAN_CLOSURE_H

#include "Type.h"
#include "common/Set.h"
#include "common/Vector.h"

#include <llvm/IR/Type.h>

#include <string>

namespace moya {

class Module;
class Deserializer;
class Serializer;

class Closure {
private:
  Module& module;

  // Name of the closure type
  std::string llvmName;

  // The name of the function with which this closure is associated
  std::string fname;

  // The LLVM struct type for which this closure is an analysis type
  llvm::StructType* llvm;

  // The indices of the closure that are array descriptors
  Set<unsigned> descriptors;

  // The names of the elements of the closure. They are useful in figuring
  // out which of the parent's locals they belong to
  Vector<std::string> names;

  // The subtypes of the closure. These types exist in LLVM too, but they
  // are lowered (I'll try to contain my amazement!), and the types that
  // are added here will be the more concrete ones
  Vector<Type*> types;

protected:
  Closure(Module&, const std::string& = "", const std::string& = "");

public:
  Closure(const Closure&) = delete;
  virtual ~Closure() = default;

  Module& getModule();
  const Module& getModule() const;

  const std::string& getFunctionName() const;
  const std::string& getLLVMName() const;
  const Vector<std::string>& getNames() const;
  const Vector<Type*>& getTypes() const;
  llvm::StructType* getLLVM() const;
  bool isDescriptor(unsigned) const;

  void addType(const std::string&, const std::string&, bool, bool);
  void setLLVM(llvm::StructType*);

  void serialize(Serializer&) const;
  void deserialize(Deserializer&);

  friend class Module;
};

} // namespace moya

#endif // MOYA_FRONTEND_FORTRAN_CLOSURE_H
