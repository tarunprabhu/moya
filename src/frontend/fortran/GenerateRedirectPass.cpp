#include "Passes.h"
#include <llvm/IR/Attributes.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

#include "common/ArgumentUtils.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Map.h"
#include "common/Metadata.h"
#include "common/TypeUtils.h"
#include "common/ValueUtils.h"
#include "frontend/all-langs/GenerateRedirectImplBase.h"

using namespace llvm;

class GenerateRedirect : public GenerateRedirectImplBase {
public:
  GenerateRedirect(Function&);

  // Register all the function arguments
  virtual void insertRegisterArgs();
};

GenerateRedirect::GenerateRedirect(Function& f) : GenerateRedirectImplBase(f) {
  ;
}

void GenerateRedirect::insertRegisterArgs() {
  Type* i8 = builder.getInt8Ty();
  Type* pi8 = i8->getPointerTo();
  Type* ppi8 = pi8->getPointerTo();

  Function* beginCall = moya::LLVMUtils::getBeginCallFunction(module);
  builder.CreateCall(beginCall, id);

  // FIXME: Deal with pointers to structs correctly
  // Right now, when a struct is passed by pointer, we just use the pointer
  // as the key. Ideally, we should be using the elements of the struct as
  // the key because if we iterate over an array of structs, the pointer will
  // get marked as variable and we won't be able to use any of the values of
  // the struct
  for(Argument& farg : f->args()) {
    Function* registerFn = nullptr;
    SmallVector<Value*, 8> registerArgs;
    registerArgs.push_back(id);
    registerArgs.push_back(builder.getInt32(farg.getArgNo()));

    Type* argTy = moya::LLVMUtils::getAnalysisType(farg);
    if(moya::LLVMUtils::isScalarTy(argTy)) {
      registerFn = moya::LLVMUtils::getRegisterArgFunction(module, argTy);
      registerArgs.push_back(&farg);
    } else if(auto* pty = dyn_cast<PointerType>(argTy)) {
      Type* ety = pty->getElementType();
      if(moya::LLVMUtils::isScalarTy(ety)) {
        registerFn = moya::LLVMUtils::getRegisterArgFunction(module, argTy);
        registerArgs.push_back(builder.CreateBitOrPointerCast(&farg, argTy));
      } else if(ety->isPointerTy()) {
        registerFn = moya::LLVMUtils::getRegisterArgFunction(module, ppi8);
        registerArgs.push_back(builder.CreateBitOrPointerCast(&farg, ppi8));
      } else if(auto* aty = dyn_cast<ArrayType>(ety)) {
        Type* aety = moya::LLVMUtils::getFlattenedElementType(aty);
        uint64_t elems = moya::LLVMUtils::getNumFlattenedElements(aty);
        if(moya::LLVMUtils::isScalarTy(aety)) {
          registerFn = moya::LLVMUtils::getRegisterArgFunction(module, aty);
          registerArgs.push_back(
              builder.CreateBitOrPointerCast(&farg, aety->getPointerTo()));
          registerArgs.push_back(builder.getInt32(elems));
        }
      } else if(ety->isStructTy()) {
        registerFn = moya::LLVMUtils::getRegisterArgPtrFunction(module);
        registerArgs.push_back(builder.CreateBitOrPointerCast(&farg, pi8));
      }
    }

    if(registerArgs.size() < 3)
      moya_error("Unsupported type to JIT:\n"
                 << "  type: " << *argTy << "\n"
                 << "  func: " << f->getName() << "\n"
                 << "   arg: " << farg.getArgNo());

    registerArgs.push_back(builder.getInt32(moya::Metadata::hasIgnore(farg)));
    builder.CreateCall(registerFn, registerArgs);
  }
}

// This pass generates the redirection functions which have been inserted into
// the code either by another pass or by Pilot
class GenerateRedirectPass : public ModulePass {
public:
  static char ID;

public:
  GenerateRedirectPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

GenerateRedirectPass::GenerateRedirectPass() : ModulePass(ID) {
  llvm::initializeGenerateRedirectPassPass(*PassRegistry::getPassRegistry());
}

StringRef GenerateRedirectPass::getPassName() const {
  return "Moya Generate Redirect (Fortran)";
}

void GenerateRedirectPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

// This pass generates the redirection function for all functions to be
// JIT'ted. The redirection code is inserted already
bool GenerateRedirectPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  for(Function& f : module.functions())
    if(f.size() and moya::Metadata::hasSpecialize(f))
      changed |= GenerateRedirect(f).run();

  return changed;
}

char GenerateRedirectPass::ID = 0;

static const char* name = "moya-generate-redirect-fort";
static const char* descr = "Generate redirects (Fortran)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(GenerateRedirectPass, name, descr, cfg, analysis)

void registerGenerateRedirectPass(const PassManagerBuilder&,
                                  legacy::PassManagerBase& pm) {
  pm.add(new GenerateRedirectPass());
}

Pass* createGenerateRedirectPass() {
  return new GenerateRedirectPass();
}
