#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/AerynConfig.h"
#include "common/Config.h"
#include "common/DataLayout.h"
#include "common/Diagnostics.h"
#include "common/InstructionUtils.h"
#include "common/Metadata.h"
#include "common/StringUtils.h"
#include "common/TypeUtils.h"
#include "common/ValueUtils.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/GlobalValue.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

class PropagateAnalysisTypesPass : public ModulePass {
public:
  static char ID;

protected:
  moya::DataLayout layout;
  moya::Set<Instruction*> arrays;

protected:
  Type*
  getNextType(PointerType*, int64_t, const moya::GEP&, GetElementPtrInst*);
  Type* getNextType(ArrayType*, int64_t, const moya::GEP&, GetElementPtrInst*);
  Type* getNextType(StructType*, int64_t, const moya::GEP&, GetElementPtrInst*);
  Type* getNextType(Type*, int64_t, const moya::GEP&, GetElementPtrInst*);

  Type* getAnalysisType(Type*,
                        const moya::GEP&,
                        GetElementPtrInst*); // Constant GEP
  Type* getAnalysisType(Type*,
                        GetElementPtrInst*); // Non-constant GEP

  bool propagate(PHINode*, Value*, Type*, const moya::Module&);
  bool propagate(LoadInst*, Value*, Type*, const moya::Module&);
  bool propagate(CastInst*, Value*, Type*, const moya::Module&);
  bool propagate(GetElementPtrInst*, Value*, Type*, const moya::Module&);
  bool propagate(Value*, Value*, Type*, const moya::Module&);

  bool isGEPPointerOperandArray(Value*, const moya::Module&);

public:
  PropagateAnalysisTypesPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

PropagateAnalysisTypesPass::PropagateAnalysisTypesPass() : ModulePass(ID) {
  initializePropagateAnalysisTypesPassPass(*PassRegistry::getPassRegistry());
}

StringRef PropagateAnalysisTypesPass::getPassName() const {
  return "Moya Propagate Analysis Types (Fortran)";
}

void PropagateAnalysisTypesPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.setPreservesCFG();
}

bool PropagateAnalysisTypesPass::isGEPPointerOperandArray(
    Value* v,
    const moya::Module& feModule) {
  if(auto* load = dyn_cast<LoadInst>(v)) {
    return isGEPPointerOperandArray(load->getPointerOperand(), feModule);
  } else if(auto* cst = dyn_cast<CastInst>(v)) {
    return isGEPPointerOperandArray(cst->getOperand(0), feModule);
  } else if(auto* gep = dyn_cast<GetElementPtrInst>(v)) {
    return arrays.contains(gep);
  } else if(auto* alloca = dyn_cast<AllocaInst>(v)) {
    return arrays.contains(alloca);
  } else if(auto* arg = dyn_cast<Argument>(v)) {
    Function& f = *moya::LLVMUtils::getFunction(arg);
    const moya::Argument& feArg
        = feModule.getFunction(f).getArg(arg->getArgNo());
    return feArg.isArray();
  }

  return false;
}

bool PropagateAnalysisTypesPass::propagate(Value* v,
                                           Value* src,
                                           Type* type,
                                           const moya::Module& feModule) {
  bool changed = false;

  if(auto* inst = dyn_cast<Instruction>(v)) {
    if(not moya::Metadata::hasAnalysisType(inst)
       and not moya::Metadata::hasIgnore(inst)) {
      if(auto* load = dyn_cast<LoadInst>(inst))
        changed |= propagate(load, src, type, feModule);
      else if(auto* cst = dyn_cast<CastInst>(inst))
        changed |= propagate(cst, src, type, feModule);
      else if(auto* gep = dyn_cast<GetElementPtrInst>(inst))
        changed |= propagate(gep, src, type, feModule);
      else if(auto* phi = dyn_cast<PHINode>(inst))
        changed |= propagate(phi, src, type, feModule);
    }
  }

  return changed;
}

bool PropagateAnalysisTypesPass::propagate(PHINode* phi,
                                           Value*,
                                           Type* type,
                                           const moya::Module& feModule) {
  bool changed = false;

  // All the incoming values should have the same analysis type. But they
  // may not have been propagated yet. There could be a use cycle here because
  // the phi node may get used by the instruction that will be one of the
  // incoming values of this node
  //
  if(not moya::Metadata::hasAnalysisType(phi)) {
    changed |= moya::Metadata::setAnalysisType(phi, type);
  } else {
    if(moya::Metadata::getAnalysisType(phi) != type)

      moya_error("Inconsistent propagated types\n"
                 << "  inst: " << *phi << "\n"
                 << "   old: " << *moya::Metadata::getAnalysisType(phi) << "\n"
                 << "   new: " << *type);
  }

  for(Use& u : phi->uses())
    changed |= propagate(u.getUser(), phi, type, feModule);

  return changed;
}

static StructType* getInnermostStruct(StructType* sty) {
  if(sty->getNumElements())
    if(auto* tty = dyn_cast<StructType>(sty->getElementType(0)))
      return getInnermostStruct(tty);
  return sty;
}

bool PropagateAnalysisTypesPass::propagate(CastInst* cst,
                                           Value* src,
                                           Type* type,
                                           const moya::Module& feModule) {
  bool changed = false;

  Type* dstTy = type;
  if(auto* pty = dyn_cast<PointerType>(type)) {
    Type* ety = pty->getElementType();
    if(moya::LLVMUtils::getPointerDepth(type) == 1
       and moya::LLVMUtils::getPointerDepth(cst->getDestTy()) == 2) {
      // If a pointer is cast to an i8**, then the incoming type must be a
      // pointer to a struct and the this is a prelude to loading the first
      // element of the struct which will be a pointer to something
      // TODO: Run a sanity check to confirm that this assumption is correct
      //
      if(auto* sty = dyn_cast<StructType>(ety)) {
        Type* elem0 = getInnermostStruct(sty)->getElementType(0);
        if(isa<PointerType>(elem0))
          dstTy = elem0->getPointerTo();
        else
          moya_error("Expected pointer type in first element of struct:\n"
                     << "  inst: " << *cst << "\n"
                     << "  type: " << *type << "\n"
                     << " strct: " << *sty);
      } else {
        moya_error("Expected struct type in cast:\n"
                   << "  inst: " << *cst << "\n"
                   << "  type: " << *type);
      }
    } else if(auto* aty = dyn_cast<ArrayType>(ety)) {
      // If the incoming type is a pointer to an array and the destination of
      // the type is an array element, then propagate the element type
      //
      if(moya::LLVMUtils::getFlattenedElementType(aty)->getPointerTo()
         == cst->getDestTy())
        dstTy = cst->getDestTy();
    }
  }

  changed |= moya::Metadata::setAnalysisType(cst, dstTy);
  for(Use& u : cst->uses())
    changed |= propagate(u.getUser(), cst, dstTy, feModule);

  return changed;
}

bool PropagateAnalysisTypesPass::propagate(LoadInst* load,
                                           Value* src,
                                           Type* type,
                                           const moya::Module& feModule) {
  bool changed = false;
  const Module& module = *moya::LLVMUtils::getModule(load);

  if(not type->isPointerTy())
    moya_error("Expected pointer type propagated to load. Got "
               << *type << " in " << *load << " from "
               << moya::LLVMUtils::getFunctionName(load));

  // Based on the type coming in, we can determine all possible types that
  // can be loaded from the current location. If we can find a unique type
  // based on this, just return it
  Type* result = load->getType();
  moya::Set<Type*> valid;
  moya::Set<Type*> loadable = moya::LLVMUtils::getLoadableTypes(type);
  for(Type* ty : loadable) {
    if(result == ty)
      valid.insert(ty);
    else if(moya::LLVMUtils::isScalarTy(result)
            and moya::LLVMUtils::isScalarTy(ty))
      valid.insert(result);
    else if(isa<PointerType>(result) and isa<PointerType>(ty))
      valid.insert(ty);
    else if(isa<StructType>(result) and isa<StructType>(ty))
      if(cast<StructType>(result)->isLayoutIdentical(cast<StructType>(ty)))
        valid.insert(result);
  }

  if(valid.size() == 1) {
    Type* next = moya::LLVMUtils::getCanonicalType(*valid.begin(), module);
    changed |= moya::Metadata::setAnalysisType(load, next);
    for(Use& u : load->uses())
      changed |= propagate(u.getUser(), load, next, feModule);
  } else if(valid.size() == 0) {
    std::string buf;
    raw_string_ostream ss(buf);
    for(Type* ty : loadable)
      ss << "        " << *ty << "\n";
    moya_error("Could not find any type to load:\n"
               << "  inst: " << *load << "\n"
               << "  type: " << *type << "\n"
               << "   res: " << *result << "\n"
               << "  load:\n"
               << ss.str());
  } else {
    std::string buf;
    raw_string_ostream ss(buf);
    for(Type* ty : valid)
      ss << "        " << *ty << "\n";
    moya_error("Could not find unique type to load:\n"
               << "  inst: " << *load << "\n"
               << "  type: " << *type << "\n"
               << "   res: " << *result << "\n"
               << " valid:\n"
               << ss.str());
  }

  return changed;
}

Type* PropagateAnalysisTypesPass::getNextType(PointerType* pty,
                                              int64_t offset,
                                              const moya::GEP& feGEP,
                                              GetElementPtrInst* gep) {
  if(offset != moya::Module::ArrayIndexMarker)
    moya_error("Expected zero offset from pointer:\n"
               << "    type: " << *pty << "\n"
               << "  offset: " << offset << "\n"
               << "     gep: " << *gep << "\n"
               << "    func: " << moya::LLVMUtils::getFunctionName(gep));
  return pty->getElementType();
}

Type* PropagateAnalysisTypesPass::getNextType(ArrayType* aty,
                                              int64_t offset,
                                              const moya::GEP& feGEP,
                                              GetElementPtrInst* gep) {
  // The offset of -1 is really meant to be zero, but is here to distinguish
  // it from a zero used to lookup the field of a struct. It is this way
  // because although the type of the pointer operand of GEP instruction is
  // a pointer, it really could be an array underneath with the pointer
  // being passed to the start of the array
  //
  if(offset != moya::Module::ArrayIndexMarker)
    moya_error("Unexpected offset when looking up array:\n"
               << "    type: " << *aty << "\n"
               << "  offset: " << offset << "\n"
               << "     gep: " << *gep << "\n"
               << "    func: " << moya::LLVMUtils::getFunctionName(gep));

  return moya::LLVMUtils::getFlattenedElementType(aty);
}

Type* PropagateAnalysisTypesPass::getNextType(StructType* sty,
                                              int64_t offset,
                                              const moya::GEP& feGEP,
                                              GetElementPtrInst* gep) {
  Module& module = *moya::LLVMUtils::getModule(gep);
  const moya::StructLayout& sl = layout.getStructLayout(sty);

  if(feGEP.isArray())
    return sty;

  if(offset < 0) {
    // Because Flang is an absolutely glorious piece of software, we can't
    // just assume that this is an error. A "legitimate" case is for a case
    // like this:
    //
    //   type mytype
    //     real :: pad
    //     integer :: field
    //   end mytype
    //
    //   type fake
    //     integer :: pad
    //     type(mytype) :: array(:)
    //   end type fake
    //
    //   t = fake % array(i) % field
    //
    // In this situation, instead of first computing the offset of the i'th
    // element of array first and then computing the offset of field, Flang
    // first computes the offset of field and then the offset to the i'th
    // element. The result of this is that the indices appear "backwards"
    // when computing them and end up backwards here as well.
    //
    // There is almost certainly a better way of doing this, but whatever...
    //
    // These are the LLVM equivalents of the types from above:
    //
    //   mytype = type { float, i32 }
    //   fake = type { i32, mytype*, i64, [16 x i64] }
    //
    // In this case, mytype* is really a pointer to [n x mytype]. The only
    // way we know this is if there is another GEP instruction that computes
    // the offset to array (mytype*) and the pointer in this GEP can be traced
    // back to that one.
    //
    if((offset == moya::Module::ArrayIndexMarker)
       and isGEPPointerOperandArray(gep->getPointerOperand(),
                                    feGEP.getModule()))
      return sty;
    else
      moya_error("Expected positive offset into struct:\n"
                 << "  struct: " << *sty << "\n"
                 << "  offset: " << offset << "\n"
                 << "     gep: " << *gep << "\n"
                 << "    func: " << moya::LLVMUtils::getFunctionName(gep));
  }

  int64_t idx = sl.getElementContainingOffset(offset);
  int64_t field = sl.getElementOffset(idx);

  if(offset != field)
    moya_error("Expected offset to the start of a field of a struct:\n"
               << "  struct: " << *sty << "\n"
               << "   field: " << field << "\n"
               << "  offset: " << offset << "\n"
               << "     gep: " << *gep << "\n"
               << "    func: " << moya::LLVMUtils::getFunctionName(gep));

  if(moya::Metadata::hasFlangArrayIndices(sty, module)
     and moya::Metadata::getFlangArrayIndices(sty, module).contains(idx))
    arrays.insert(gep);

  return sty->getElementType(idx);
}

Type* PropagateAnalysisTypesPass::getNextType(Type* type,
                                              int64_t offset,
                                              const moya::GEP& feGEP,
                                              GetElementPtrInst* gep) {
  if(moya::LLVMUtils::isScalarTy(type)) {
    // A scalar type usually means that we are taking an offset from a pointer
    // which can happen when looking up an array. In that case, the offset
    // must reflect this
    if(offset != moya::Module::ArrayIndexMarker)
      moya_error("Cannot lookup non-zero offset into scalar\n"
                 << "    type: " << *type << "\n"
                 << "  offset: " << offset << "\n"
                 << "     gep: " << *gep << "\n"
                 << "    func: " << moya::LLVMUtils::getFunctionName(gep));
    return type;
  } else if(auto* pty = dyn_cast<PointerType>(type)) {
    return getNextType(pty, offset, feGEP, gep);
  } else if(auto* aty = dyn_cast<ArrayType>(type)) {
    return getNextType(aty, offset, feGEP, gep);
  } else if(auto* sty = dyn_cast<StructType>(type)) {
    return getNextType(sty, offset, feGEP, gep);
  }

  moya_error("Unexpected type in GEP\n"
             << "    type: " << *type << "\n"
             << "  offset: " << offset << "\n"
             << "     gep: " << *gep << "\n"
             << "    func: " << moya::LLVMUtils::getFunctionName(gep));
  return nullptr;
}

Type* PropagateAnalysisTypesPass::getAnalysisType(Type* type,
                                                  const moya::GEP& feGEP,
                                                  GetElementPtrInst* gep) {
  Type* nextType = type;
  for(int64_t offset : feGEP.getOffsets()) {
    nextType = getNextType(nextType, offset, feGEP, gep);
    if(moya::Config::getEnvVarValueAsBool(getenv("MOYA_FLANG_VERBOSE")))
      llvm::errs() << "    next: " << *nextType << "\n";
  }

  return nextType;
}

Type* PropagateAnalysisTypesPass::getAnalysisType(Type* type,
                                                  GetElementPtrInst* gep) {
  if(auto* aty = dyn_cast<ArrayType>(type))
    return aty->getElementType();
  else
    return type;
}

bool PropagateAnalysisTypesPass::propagate(GetElementPtrInst* gep,
                                           Value* src,
                                           Type* type,
                                           const moya::Module& feModule) {
  bool changed = false;

  if(gep->getPointerOperand() != src)
    return changed;

  if(auto* pty = dyn_cast<PointerType>(type)) {
    const moya::Function& feFunc
        = feModule.getFunction(*moya::LLVMUtils::getFunction(gep));
    const moya::GEP& feGEP = feFunc.getGEP(gep);
    if(moya::Config::getEnvVarValueAsBool(getenv("MOYA_FLANG_VERBOSE")))
      llvm::errs() << "gep: " << *gep << "\n"
                   << "  offset: " << feGEP.getOffsets().str() << "\n"
                   << "    type: " << *pty->getElementType() << "\n"
                   << "   array: " << feGEP.isArray() << "\n";
    if(gep->hasAllConstantIndices()) {
      Type* nextType = getAnalysisType(pty->getElementType(), feGEP, gep);
      Type* analysisType = nextType->getPointerTo();
      changed |= moya::Metadata::setGEPIndex(gep, feGEP.getOffset());
      changed |= moya::Metadata::setAnalysisType(gep, analysisType);
      for(Use& u : gep->uses())
        changed |= propagate(u.getUser(), gep, analysisType, feModule);
    } else {
      Type* nextType = getAnalysisType(pty->getElementType(), gep);
      Type* analysisType = nextType->getPointerTo();
      changed |= moya::Metadata::setAnalysisType(gep, analysisType);
      for(Use& u : gep->uses())
        changed |= propagate(u.getUser(), gep, pty, feModule);
    }
  } else if(auto* sty = dyn_cast<StructType>(type)) {
    moya_error("Unimplemented struct type passed to gep:\n"
               << "   gep: " << *gep << "\n"
               << "  type: " << *type << "\n"
               << "  func: " << moya::LLVMUtils::getFunctionName(gep));
  } else {
    moya_error("Expected pointer type passed to gep:\n"
               << "   gep: " << *gep << "\n"
               << "  type: " << *type << "\n"
               << "   func: " << moya::LLVMUtils::getFunctionName(gep));
  }

  return changed;
}

bool PropagateAnalysisTypesPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();
  layout.reset(module);

  if(moya::Config::getEnvVarValueAsBool(getenv("MOYA_FLANG_VERBOSE"))) {
    llvm::errs() << module << "\n";
    for(StructType* sty : module.getIdentifiedStructTypes())
      if(moya::Metadata::hasFlangArrayIndices(sty, module))
        llvm::errs() << sty->getName() << " => "
                     << moya::Metadata::getFlangArrayIndices(sty, module).str()
                     << "\n";
  }

  // Find the alloca instructions that are actually arrays
  for(Function& f : module.functions())
    if(feModule.hasFunction(f))
      for(inst_iterator i = inst_begin(f); i != inst_end(f); i++)
        if(auto* alloca = dyn_cast<AllocaInst>(&*i))
          if(alloca->hasName())
            if(feModule.isLocalArray(f.getName(), alloca->getName()))
              arrays.insert(alloca);

  for(GlobalVariable& g : module.globals()) {
    // The hoisted globals are byte arrays in the IR. But the analysis
    // type is a struct where each field of the struct corresponds to one
    // of the function or module-level variables that have been smashed
    // together to create the hoisted variable.
    //
    // The variables will only ever be used to look up the fields. So the
    // only uses of these globals will be one of the following:
    //
    // There shouldn't by any other global variables with an analysis type here
    // The hoisted globals will definitely have analysis type, so this
    // condition catches everything.
    //
    if(moya::Metadata::hasAnalysisType(g)) {
      Type* type = moya::LLVMUtils::getAnalysisType(g);
      for(Use& u : g.uses())
        changed |= propagate(u.getUser(), &g, type, feModule);
    }
  }

  for(Function& f : module.functions()) {
    if(not f.size())
      continue;

    if(moya::Config::getEnvVarValueAsBool(getenv("MOYA_FLANG_VERBOSE")))
      llvm::errs() << "f: " << f.getName() << "\n";

    for(Argument& a : f.args()) {
      if(moya::Metadata::hasAnalysisType(a)) {
        Type* analysisType = moya::Metadata::getAnalysisType(a);
        for(Use& u : a.uses())
          changed |= propagate(u.getUser(), &a, analysisType, feModule);
      }
    }

    for(inst_iterator i = inst_begin(f); i != inst_end(f); i++) {
      if(auto* alloca = dyn_cast<AllocaInst>(&*i)) {
        // Although not strictly an analysis type, propagating the types of
        // allocas can make life easier elsewhere especially since Flang likes
        // to cast everything down to byte arrays before operating on them
        //
        Type* analysisType = moya::LLVMUtils::getCanonicalType(
                                 alloca->getAllocatedType(), module)
                                 ->getPointerTo();
        if(auto* sty = dyn_cast<StructType>(alloca->getAllocatedType()))
          if(moya::Metadata::hasFlangClosure(sty, module))
            analysisType
                = moya::Metadata::getFlangClosure(sty, module)->getPointerTo();

        changed |= moya::Metadata::setAnalysisType(alloca, analysisType);
        for(Use& u : alloca->uses())
          changed |= propagate(u.getUser(), alloca, analysisType, feModule);
      } else if(auto* call = dyn_cast<CallInst>(&*i)) {
        // Call instructions can have an analysis type mostly for the
        // functions that allocate memory and return it
        //
        if(moya::Metadata::hasAnalysisType(call)) {
          Type* analysisType = moya::Metadata::getAnalysisType(call);
          for(Use& u : call->uses())
            changed |= propagate(u.getUser(), call, analysisType, feModule);
        }
      }
    }
  }

  return changed;
}

char PropagateAnalysisTypesPass::ID = 0;

static const char* name = "moya-propagate-analysis-types-fort";
static const char* descr = "Propagate higher-level types (Fortran)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(PropagateAnalysisTypesPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_END(PropagateAnalysisTypesPass, name, descr, cfg, analysis)

void registerPropagateAnalysisTypesPass(const PassManagerBuilder&,
                                        legacy::PassManagerBase& pm) {
  pm.add(new PropagateAnalysisTypesPass());
}

Pass* createPropagateAnalysisTypesPass() {
  return new PropagateAnalysisTypesPass();
}
