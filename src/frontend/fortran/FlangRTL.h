#ifndef MOYA_FRONTEND_FORTRAN_FLANG_RTL_H
#define MOYA_FRONTEND_FORTRAN_FLANG_RTL_H

#include <string>

class FlangRTL {
public:
  static bool isAllocatorFunction(const std::string&);
  static unsigned getAllocatorFunctionArgNo(const std::string&);
  static bool isDeallocatorFunction(const std::string&);
  static unsigned getDeallocatorFunctionArgNo(const std::string&);
  static bool isMallocLikeFunction(const std::string&);
  static bool isFreeLikeFunction(const std::string&);
  static bool isMemcpyLikeFunction(const std::string&);
  static bool isMemsetLikeFunction(const std::string&);
};

#endif // MOYA_FRONTEND_FORTRAN_FLANG_RTL_H
