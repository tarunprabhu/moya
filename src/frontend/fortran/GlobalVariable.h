#ifndef MOYA_FRONTEND_FORTRAN_GLOBAL_VARIABLE_H
#define MOYA_FRONTEND_FORTRAN_GLOBAL_VARIABLE_H

#include "Struct.h"
#include "common/Map.h"

#include "frontend/components/GlobalVariableBase.h"

#include <string>

namespace moya {

class ModuleBase;
class Deserializer;
class Serializer;

class GlobalVariable : public GlobalVariableBase {
private:
  // Name of the global in LLVM
  std::string llvmName;

  // The only global variables we really care about are the "fake" ones
  // that constitute the single biggest disgrace in the already wretched
  // piece of software called flang. The type is treated as a struct because
  // that is how it essentially behaves and the Struct object already has
  // flags to indicate allocatables and pointers that will need in the
  // later analyses
  //
  Struct& strct;

protected:
  GlobalVariable(ModuleBase&,
                 const std::string& = "",
                 const std::string& = "",
                 const std::string& = "",
                 const std::string& = "",
                 unsigned = 0,
                 const char* const* = nullptr,
                 const unsigned* = nullptr,
                 const char* const* = nullptr,
                 const unsigned* = nullptr);

public:
  GlobalVariable(const GlobalVariable&) = delete;
  virtual ~GlobalVariable() = default;

  Module& getModule();
  const Module& getModule() const;

  const std::string& getLLVMName() const;
  const Struct& getStruct() const;

  void serialize(Serializer&) const;
  void deserialize(Deserializer&);

  friend class ModuleBase;
};

} // namespace moya

#endif // MOYA_FRONTEND_FORTRAN_GLOBAL_VARIABLE_H
