#ifndef MOYA_FRONTEND_FORTRAN_REGISTER_FRONTEND_PASS_H
#define MOYA_FRONTEND_FORTRAN_REGISTER_FRONTEND_PASS_H

#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

#define REGISTER_LLVM_PASS(passName)                                        \
  void register##passName(const llvm::PassManagerBuilder&,                  \
                          llvm::legacy::PassManagerBase&);                  \
  static llvm::RegisterStandardPasses passName##NoOpt(                      \
      llvm::PassManagerBuilder::EP_EnabledOnOptLevel0, register##passName); \
  static llvm::RegisterStandardPasses passName##Opt(                        \
      llvm::PassManagerBuilder::EP_ModuleOptimizerEarly, register##passName);

#define REGISTER_PASS(passName)                                  \
  void register##passName##Pass(const llvm::PassManagerBuilder&, \
                                llvm::legacy::PassManagerBase&); \
  static llvm::RegisterStandardPasses passName##NoOpt(           \
      llvm::PassManagerBuilder::EP_EnabledOnOptLevel0,           \
      register##passName##Pass);                                 \
  static llvm::RegisterStandardPasses passName##Opt(             \
      llvm::PassManagerBuilder::EP_ModuleOptimizerEarly,         \
      register##passName##Pass);

#define REGISTER_EARLY_PASS(passName)                            \
  void register##passName##Pass(const llvm::PassManagerBuilder&, \
                                llvm::legacy::PassManagerBase&); \
  static llvm::RegisterStandardPasses passName##NoOpt(           \
      llvm::PassManagerBuilder::EP_EarlyAsPossible, register##passName##Pass);

#endif // MOYA_FRONTEND_FORTRAN_REGISTER_FRONTEND_PASS_H
