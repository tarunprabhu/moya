#ifndef MOYA_FRONTEND_FORTRAN_STRUCT_H
#define MOYA_FRONTEND_FORTRAN_STRUCT_H

#include "common/Bitset.h"
#include "common/Location.h"
#include "common/Vector.h"

#include <llvm/IR/DerivedTypes.h>

namespace moya {

class Function;
class Module;
class Type;
class Serializer;
class Deserializer;

class Struct {
public:
  struct Field {
    std::string name;
    unsigned offset;
    std::string str;
    bool descriptor;
    Type* type;

    Field(const std::string& = "",
          unsigned = 0,
          const std::string& = "",
          bool = false);
    void serialize(Serializer&) const;
    void deserialize(Deserializer&);
  };

protected:
  Module& module;
  std::string name;
  Vector<Struct::Field> fields;
  Location loc;
  llvm::StructType* llvm;
  // True if this is a struct corresponding to a fake global and not an actual
  // user-defined struct or type descriptor
  bool fake;

protected:
  Struct(Module&, const std::string& = "", bool = false);

public:
  Struct(const Struct&) = delete;
  virtual ~Struct() = default;

  Module& getModule();
  const Module& getModule() const;

  const Location& getLocation() const;
  llvm::StructType* getLLVM() const;
  const std::string& getLLVMName() const;
  size_t getNumFields() const;
  bool isFake() const;
  const std::string& getName(unsigned) const;
  unsigned getOffset(unsigned) const;
  const std::string& getTypeStr(unsigned) const;
  const Type& getType(unsigned) const;
  bool isDescriptor(unsigned) const;

  void addType(const std::string&, unsigned, const std::string&, bool);
  void finalizeTypes();
  void setLLVM(llvm::StructType*);

  void serialize(Serializer&) const;
  void deserialize(Deserializer&);

  friend class Module;
};

} // namespace moya

#endif // MOYA_FRONTEND_FORTRAN_STRUCT_H
