#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Pass.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/Transforms/Utils/UnifyFunctionExitNodes.h>

using namespace llvm;

// Definitions for LLVM pass registration functions that may not be present in
// libLLVM
void registerUnifyFunctionExitNodes(const PassManagerBuilder&,
                                    legacy::PassManagerBase& pm) {
  pm.add(new UnifyFunctionExitNodes());
}
