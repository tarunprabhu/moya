#ifndef MOYA_FRONTEND_FORTRAN_GEP_H
#define MOYA_FRONTEND_FORTRAN_GEP_H

#include "common/Vector.h"

#include <llvm/IR/Instructions.h>

namespace moya {

class Function;
class Module;
class Serializer;
class Deserializer;

class GEP {
private:
  Function& func;

  // This is the total of all the offsets including any adjustments for the
  // arrays or anything else. Basically, this is what appears in the LLVM IR
  // It's here for sanity checks
  int64_t cumulative;

  // This is the actual offset needed for the analysis
  // It is the sum of the positive elements in the offsets array
  // (there should never be a negative offset because this is Fortran and
  // that kind of madness is not allowed. A negative value of -1 indicates
  // that that offset is into an array, and we never want to look up anything
  // but the first element of an array. The use of -1 is to distinguish it
  // from an actual 0 offset which is intended for the first element of a
  // struct)
  //
  int64_t offset;

  // This flag is true if the GEP is created to treat the struct as an array
  // and does not look at a field of the struct
  bool array;

  // The offsets as determined from Flang. They are still kept around
  // because the analysis type propagation pass will use them to perform
  // sanity checks as well in the process of propagating
  Vector<int64_t> offsets;

  // The GEP instruction in the function to which this is attached
  llvm::GetElementPtrInst* llvm;

public:
  GEP(Function&);
  GEP(Function&, int64_t, int64_t, bool, const int64_t*, uint64_t);
  GEP(Function&, int64_t, int64_t, bool, const Vector<int64_t>&);
  virtual ~GEP() = default;

  const Function& getFunction() const;
  const Module& getModule() const;
  bool isConstGEP() const;
  int64_t getCumulative() const;
  int64_t getOffset() const;
  bool isArray() const;
  const Vector<int64_t>& getOffsets() const;
  llvm::GetElementPtrInst* getLLVM() const;

  void overrideOffsets(const Vector<int64_t>&);
  void setLLVM(llvm::GetElementPtrInst*);

  void serialize(Serializer&) const;
  void deserialize(Deserializer&);
};

} // namespace moya

#endif // MOYA_FRONTEND_FORTRAN_GEP_H
