#ifndef MOYA_FRONTEND_FORTRAN_GLOBALS_H
#define MOYA_FRONTEND_FORTRAN_GLOBALS_H

#include "Module.h"

moya::Module& getGlobalSingletonModule();

#endif // MOYA_FRONTEND_FORTRAN_GLOBALS_H
