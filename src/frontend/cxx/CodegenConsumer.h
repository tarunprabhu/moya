#ifndef MOYA_FRONTEND_CXX_CODEGEN_CONSUMER_H
#define MOYA_FRONTEND_CXX_CODEGEN_CONSUMER_H

#include "CodeGenerator.h"
#include "CodegenVisitor.h"
#include "frontend/c-family/CodegenConsumerClang.h"

namespace moya {

class Module;

class CodegenConsumer : public CodegenConsumerClang {
protected:
  CodegenVisitor visitor;
  CodeGenerator& cg;

public:
  explicit CodegenConsumer(Module&, CodeGenerator&, clang::CompilerInstance&);
  CodegenConsumer(const CodegenConsumer&) = delete;
  virtual ~CodegenConsumer() = default;

  virtual CodegenVisitor& getVisitor() override;

  virtual void
  HandleCXXImplicitFunctionInstantiation(clang::FunctionDecl*) override;
  virtual void AssignInheritanceModel(clang::CXXRecordDecl*) override;
  virtual void HandleCXXStaticMemberVarInstantiation(clang::VarDecl*) override;
  virtual void HandleVTable(clang::CXXRecordDecl*) override;
};

} // namespace moya

#endif // MOYA_FRONTEND_CXX_CODEGEN_CONSUMER_H
