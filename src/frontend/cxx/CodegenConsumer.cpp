#include "CodegenConsumer.h"

namespace moya {

CodegenConsumer::CodegenConsumer(Module& module,
                                 CodeGenerator& cg,
                                 clang::CompilerInstance& compiler)
    : CodegenConsumerClang(cg, compiler), visitor(module), cg(cg) {
  ;
}

CodegenVisitor& CodegenConsumer::getVisitor() {
  return visitor;
}

void CodegenConsumer::HandleCXXImplicitFunctionInstantiation(
    clang::FunctionDecl* f) {
  cg.HandleCXXImplicitFunctionInstantiation(f);
}

void CodegenConsumer::AssignInheritanceModel(clang::CXXRecordDecl* record) {
  cg.AssignInheritanceModel(record);
}

void CodegenConsumer::HandleCXXStaticMemberVarInstantiation(
    clang::VarDecl* decl) {
  cg.HandleCXXStaticMemberVarInstantiation(decl);
}

void CodegenConsumer::HandleVTable(clang::CXXRecordDecl* record) {
  cg.HandleVTable(record);
}

} // namespace moya
