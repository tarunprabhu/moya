#ifndef MOYA_FRONTEND_CXX_FUNCTION_H
#define MOYA_FRONTEND_CXX_FUNCTION_H

#include "common/STDConfig.h"
#include "common/Vector.h"
#include "frontend/c-family/FunctionClang.h"

#include <clang/AST/DeclCXX.h>

namespace moya {

class Module;
class Argument;

class Function : public FunctionClang {
protected:
  // The qualified name of the function includes any namespaces and classes
  // which may be used to identify this function. However, any template
  // parameters of the function itself or of the surrounding clasess will
  // have been stripped.
  std::string qualifiedName;

  // The full name of the function including any template parameters associated
  // with the function or any surrounding classes. This is *NOT* the mangled
  // name of the function.
  std::string fullName;

  STDKind kind;

  const clang::CXXMethodDecl* method;
  const clang::CXXRecordDecl* clss;

  // Some STL functions take iterators as arguments. Others return iterators
  // These are the decls for those iterators
  const clang::CXXRecordDecl* iteratorRet;
  const clang::CXXRecordDecl* iteratorArg;

protected:
  const clang::CXXRecordDecl* getIteratorDecl(const clang::Type*);

  std::string modelCreate();
  std::string modelCreateParams();
  std::string modelCreateParam(const clang::ParmVarDecl*);

  std::string getModelType(const clang::Type*) const;
  const clang::Type* getUnderlyingType(const clang::Type*) const;

protected:
  Function(ModuleBase&, const clang::FunctionDecl*, llvm::Function&);

public:
  Function(const Function&) = delete;
  virtual ~Function() = default;

  void initParams();

  const std::string& getQualifiedName() const;
  const std::string& getFullName() const;
  STDKind getSTDKind() const;
  const clang::CXXMethodDecl* getMethodDecl() const;
  const clang::CXXRecordDecl* getClassDecl() const;
  const clang::CXXRecordDecl* getReturnedIteratorDecl() const;
  const clang::CXXRecordDecl* getArgumentIteratorDecl() const;

  bool hasQualifiedName() const;
  bool hasFullName() const;
  bool isMethod() const;
  bool isConstructor() const;
  bool isDestructor() const;
  bool isVirtual() const;

  bool isPublic() const;
  bool isPrivate() const;
  bool isProtected() const;

  bool isSTLContainer() const;
  bool isSTLIterator() const;
  bool isSTDString() const;

  Module& getModule();
  Argument& getArg(unsigned);
  Argument& getArg(const std::string&);

  const Module& getModule() const;
  const Argument& getArg(unsigned) const;
  const Argument& getArg(const std::string&) const;

  friend class ModuleBase;
};

} // namespace moya

#endif // MOYA_FRONTEND_CXX_FUNCTION_H
