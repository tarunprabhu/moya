#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "Type.h"
#include "common/Diagnostics.h"
#include "common/Graph.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "frontend/all-langs/InstrumentTypesCommonPass.h"
#include "frontend/c-family/ClangUtils.h"

#include <clang/AST/DeclCXX.h>
#include <clang/AST/Type.h>

#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class InstrumentTypesPass : public ModulePass {
public:
  static char ID;

protected:
  int getLastVtableIndex(StructType*);

public:
  InstrumentTypesPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

InstrumentTypesPass::InstrumentTypesPass() : ModulePass(ID) {
  initializeInstrumentTypesPassPass(*PassRegistry::getPassRegistry());
}

StringRef InstrumentTypesPass::getPassName() const {
  return "Moya Instrument Types (C++)";
}

void InstrumentTypesPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.addRequired<InstrumentTypesCommonPass>();
  AU.setPreservesCFG();
}

int InstrumentTypesPass::getLastVtableIndex(StructType* sty) {
  Type* i32 = Type::getInt32Ty(sty->getContext());
  Type* vtType = FunctionType::get(i32, ArrayRef<Type*>(), true)
                     ->getPointerTo()
                     ->getPointerTo();
  int last = -1;
  for(Type* field : sty->elements())
    if(field == vtType)
      last++;
    else
      break;
  return last;
}

bool InstrumentTypesPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  for(const moya::Type* feType : feModule.getStructs()) {
    auto* sty = feType->getLLVMAs<StructType>();

    changed
        |= moya::Metadata::setSourceName(sty, feType->getFullName(), module);
    if(feType->isUnion())
      changed |= moya::Metadata::setUnion(sty, module);
    if(feType->getLocation().isValid())
      changed |= moya::Metadata::setSourceLocation(
          sty, feType->getLocation(), module);
  }

  for(const moya::Type* clss : feModule.getClasses()) {
    auto* sty = clss->getLLVMAs<StructType>();

    moya::Vector<llvm::StructType*> bases;
    if(clss->hasBases())
      for(const moya::Type* base : clss->getBases())
        bases.push_back(base->getLLVMAs<StructType>());
    if(bases.size())
      changed |= moya::Metadata::setBases(sty, bases, module);

    moya::Vector<llvm::StructType*> vbases;
    if(clss->hasVirtualBases())
      for(const moya::Type* base : clss->getVirtualBases())
        vbases.push_back(base->getLLVMAs<StructType>());
    if(vbases.size()) {
      const std::string& sname = sty->getName().str();
      if(StructType* vbase = module.getTypeByName(sname + ".base")) {
        changed |= moya::Metadata::setVirtualHeader(sty, vbase, module);
        changed |= moya::Metadata::setVirtualBody(vbase, sty, module);
      }
      changed |= moya::Metadata::setVirtualBases(sty, vbases, module);
    }

    if(clss->isPolymorphic())
      changed |= moya::Metadata::setPolymorphic(sty, module);

    if(sty->getNumElements()) {
      int last = getLastVtableIndex(sty);
      if(last != -1)
        changed |= moya::Metadata::setLastVtableIndex(sty, last, module);
    }
  }

  return changed;
}

char InstrumentTypesPass::ID = 0;

static const char* name = "moya-instr-types-c";
static const char* descr = "Adds instrumentation to types (C)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(InstrumentTypesPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_DEPENDENCY(InstrumentTypesCommonPass)
INITIALIZE_PASS_END(InstrumentTypesPass, name, descr, cfg, analysis)

Pass* createInstrumentTypesPass() {
  return new InstrumentTypesPass();
}
