#ifndef MOYA_FRONTEND_CXX_TYPE_H
#define MOYA_FRONTEND_CXX_TYPE_H

#include "common/STDConfig.h"
#include "common/Vector.h"
#include "frontend/c-family/TypeClang.h"

#include <clang/AST/DeclCXX.h>

namespace moya {

class Module;

class Type : public TypeClang {
protected:
  std::string qualifiedName;
  std::string fullName;
  STDKind kind;
  bool polymorphic;
  llvm::StructType* canonical;

  Vector<const Type*> bases;
  Vector<const Type*> vbases;
  Vector<const Type*> derived;

protected:
  bool hasBase(const Type*) const;
  bool hasDerived(const Type*) const;

protected:
  Type(ModuleBase&, const clang::Type*, llvm::Type*);

public:
  Type(const Type&) = delete;
  virtual ~Type() = default;

  Module& getModule();
  const Module& getModule() const;

  STDKind getSTDKind() const;

  void addBase(Type*);
  void addVirtualBase(Type*);
  void addDerived(Type*);
  void setCanonical(llvm::StructType*);

  bool hasQualifiedName() const;
  bool hasFullName() const;
  bool isClass() const;
  bool isSTLContainer() const;
  bool isSTLIterator() const;
  bool isPolymorphic() const;
  bool hasCanonical() const;

  const clang::CXXRecordDecl* getCXXDecl() const;
  bool hasCXXDecl() const;

  bool hasBases() const;
  bool hasVirtualBases() const;
  bool hasDerived() const;

  const std::string& getQualifiedName() const;
  const std::string& getFullName() const;
  const Vector<const Type*>& getBases() const;
  const Vector<const Type*>& getVirtualBases() const;
  const Vector<const Type*>& getDerived() const;
  llvm::StructType* getCanonical() const;

  friend class ModuleBase;
};

} // namespace moya

#endif // MOYA_FRONTEND_CXX_TYPE_H
