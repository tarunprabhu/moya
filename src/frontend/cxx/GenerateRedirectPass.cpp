#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Map.h"
#include "common/Metadata.h"
#include "frontend/all-langs/GenerateRedirectImplBase.h"

#include <llvm/IR/Attributes.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class GenerateRedirect : public GenerateRedirectImplBase {
public:
  GenerateRedirect(Function&);

  // Register all the function arguments
  virtual void insertRegisterArgs();
};

GenerateRedirect::GenerateRedirect(Function& f) : GenerateRedirectImplBase(f) {
  ;
}

void GenerateRedirect::insertRegisterArgs() {
  Type* i8 = builder.getInt8Ty();
  Type* pi8 = i8->getPointerTo();
  Type* ppi8 = pi8->getPointerTo();
  Type* i32 = builder.getInt32Ty();

  Function* beginCall = moya::LLVMUtils::getBeginCallFunction(module);
  builder.CreateCall(beginCall, id);

  // Byref arguments will be correctly handled by the argument registration
  // functions
  for(Argument& farg : f->args()) {
    Function* registerFn = nullptr;
    SmallVector<Value*, 8> registerArgs;
    registerArgs.push_back(id);
    registerArgs.push_back(builder.getInt32(farg.getArgNo()));

    Type* argTy = moya::LLVMUtils::getAnalysisType(farg);
    if(moya::LLVMUtils::isScalarTy(argTy)) {
      registerFn = moya::LLVMUtils::getRegisterArgFunction(module, argTy);
      registerArgs.push_back(&farg);
    } else if(auto* pty = dyn_cast<PointerType>(argTy)) {
      Type* ety = pty->getElementType();
      if(moya::LLVMUtils::isScalarTy(ety)) {
        if(moya::Metadata::hasFixedArray(farg)) {
          unsigned size = moya::Metadata::getFixedArray(farg);
          ArrayType* aty = ArrayType::get(ety, size);
          registerFn = moya::LLVMUtils::getRegisterArgFunction(module, aty);
          registerArgs.push_back(builder.CreateBitOrPointerCast(&farg, argTy));
          registerArgs.push_back(builder.getInt32(size));
        } else if(moya::Metadata::hasDynArray(farg)) {
          unsigned sizeArgNum = moya::Metadata::getDynArray(farg);
          Argument& sizeArg = *moya::LLVMUtils::getArgument(f, sizeArgNum);
          ArrayType* aty = ArrayType::get(ety, 0);
          registerFn = moya::LLVMUtils::getRegisterArgFunction(module, aty);
          registerArgs.push_back(builder.CreateBitOrPointerCast(&farg, argTy));
          registerArgs.push_back(builder.CreateBitOrPointerCast(&sizeArg, i32));
        } else {
          registerFn = moya::LLVMUtils::getRegisterArgFunction(module, argTy);
          registerArgs.push_back(builder.CreateBitOrPointerCast(&farg, argTy));
        }
      } else if(ety->isPointerTy()) {
        registerFn = moya::LLVMUtils::getRegisterArgFunction(module, ppi8);
        registerArgs.push_back(builder.CreateBitOrPointerCast(&farg, ppi8));
      } else if(auto* aty = dyn_cast<ArrayType>(ety)) {
        Type* ety = moya::LLVMUtils::getFlattenedElementType(aty);
        uint64_t num = moya::LLVMUtils::getNumFlattenedElements(aty);
        if(moya::LLVMUtils::isScalarTy(ety)) {
          registerFn = moya::LLVMUtils::getRegisterArgFunction(module, aty);
          registerArgs.push_back(
              builder.CreateBitOrPointerCast(&farg, ety->getPointerTo()));
          registerArgs.push_back(builder.getInt32(num));
        }
      } else if(ety->isStructTy()) {
        registerFn = moya::LLVMUtils::getRegisterArgPtrFunction(module);
        registerArgs.push_back(builder.CreateBitOrPointerCast(&farg, pi8));
      }
    }

    if(registerArgs.size() < 3)
      moya_error("Unsupported type to JIT:\n"
                 << "  type: " << *argTy << "\n"
                 << "  func: " << f->getName() << "\n"
                 << "   arg: " << farg.getArgNo());

    registerArgs.push_back(builder.getInt32(moya::Metadata::hasIgnore(farg)));
    builder.CreateCall(registerFn, registerArgs);
  }
}

// This pass generates the redirection functions which have been inserted into
// the code either by another pass or by Pilot
class GenerateRedirectPass : public ModulePass {
public:
  static char ID;

public:
  GenerateRedirectPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

GenerateRedirectPass::GenerateRedirectPass() : ModulePass(ID) {
  initializeGenerateRedirectPassPass(*PassRegistry::getPassRegistry());
}

StringRef GenerateRedirectPass::getPassName() const {
  return "Moya Generate Redirect (C++)";
}

void GenerateRedirectPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

// This pass generates the redirection function for all functions to be
// JIT'ted. The redirection code is inserted already
bool GenerateRedirectPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  for(Function& f : module.functions())
    if(f.size() and moya::Metadata::hasSpecialize(f)
       and isCXX(moya::Metadata::getSourceLang(f)))
      changed |= GenerateRedirect(f).run();

  return changed;
}

char GenerateRedirectPass::ID = 0;

static const char* name = "moya-generate-redirect-cxx";
static const char* descr = "Generate redirects (C++)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(GenerateRedirectPass, name, descr, cfg, analysis)

Pass* createGenerateRedirectPass() {
  return new GenerateRedirectPass();
}
