#ifndef MOYA_FRONTEND_CXX_ANNOTATOR_H
#define MOYA_FRONTEND_CXX_ANNOTATOR_H

#include "common/Vector.h"
#include "frontend/c-family/AnnotatorClang.h"

#include <clang/AST/Stmt.h>
#include <clang/Rewrite/Core/Rewriter.h>

namespace moya {

class Annotator : public AnnotatorClang {
protected:
  bool cxx11;

protected:
  virtual std::string generateFunctionCall(const std::string&,
                                           const Vector<std::string>&,
                                           const std::string&,
                                           NewLine) override;

public:
  Annotator();
  virtual ~Annotator() = default;

  virtual void initialize(clang::CompilerInstance&) override;
};

} // namespace moya

#endif // MOYA_FRONTEND_CXX_ANNOTATOR_H
