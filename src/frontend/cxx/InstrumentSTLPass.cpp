#include "Container.h"
#include "Function.h"
#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/StringUtils.h"
#include "frontend/c-family/ClangUtils.h"

#include <clang/AST/DeclTemplate.h>

#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class InstrumentSTLPass : public ModulePass {
public:
  static char ID;

protected:
  bool setSTLContainerMetadata(const moya::Function&, const moya::Container&);
  bool setSTLContainerMetadata(const moya::Function&);
  bool setSTLIteratorMetadata(const moya::Function&);
  bool setSTDStreamMetadata(const moya::Function&);
  bool setSTDStringMetadata(const moya::Function&);
  bool setSTDAllocatorMetadata(const moya::Function&);
  bool setSTDInitializerMetadata(const moya::Function&);
  bool setSTDImplMetadata(const moya::Function&);
  bool setSTDPublicMetadata(const moya::Function&);

  StructType* getSTLContainerFor(AllocaInst*);
  Function* getDefaultConstructor(const clang::Type*);

public:
  InstrumentSTLPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

InstrumentSTLPass::InstrumentSTLPass() : ModulePass(ID) {
  initializeInstrumentSTLPassPass(*PassRegistry::getPassRegistry());
}

StringRef InstrumentSTLPass::getPassName() const {
  return "Moya Instrument STL (C++)";
}

void InstrumentSTLPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.setPreservesCFG();
}

Function* InstrumentSTLPass::getDefaultConstructor(const clang::Type* cty) {
  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  if(const clang::CXXRecordDecl* clss = cty->getAsCXXRecordDecl()) {
    for(const clang::Decl* decl : clss->decls()) {
      if(const auto* t = dyn_cast<clang::FunctionTemplateDecl>(decl)) {
        for(const clang::FunctionDecl* s : t->specializations())
          if(const auto* ctor = dyn_cast<clang::CXXConstructorDecl>(s))
            if(ctor->getNumParams() == 0)
              if(feModule.hasFunction(ctor))
                return &feModule.getFunction(ctor).getLLVM();
      } else if(const auto* ctor = dyn_cast<clang::CXXConstructorDecl>(decl)) {
        if(ctor->getNumParams() == 0)
          // We could still get here for a string class which obviously
          // will not have a definition, and the default constructor will
          // not be in feModule
          if(feModule.hasFunction(ctor))
            return &feModule.getFunction(ctor).getLLVM();
      }
    }
  } else if(const clang::RecordType* record = cty->getAsStructureType()) {
    moya_error("Boom!");
    // llvm::errs() << "struct: " << getQualifiedDeclName(record->getDecl())
    //              << "\n";
  }

  return nullptr;
}

bool InstrumentSTLPass::setSTLContainerMetadata(const moya::Function& feFunc,
                                                const moya::Container& cont) {
  bool changed = false;
  Function& f = feFunc.getLLVM();
  moya::STDKind kind = feFunc.getSTDKind();
  const moya::Module& feModule = feFunc.getModule();

  if(Type* key = cont.getKeyType())
    changed |= moya::Metadata::setSTLKeyType(f, key);

  // This is only used in maps and is the value type
  if(Type* mapped = cont.getMappedType())
    changed |= moya::Metadata::setSTLMappedType(f, mapped);

  // This is used in most containers and is the contained type. In the
  // case of maps, this will be a pair of the <key, val> in the map
  // std::pair will not have a ValueType
  if(Type* val = cont.getValueType())
    changed |= moya::Metadata::setSTLValueType(f, val);

  if(Type* first = cont.getFirstType())
    changed |= moya::Metadata::setSTLFirstType(f, first);

  if(Type* second = cont.getSecondType())
    changed |= moya::Metadata::setSTLSecondType(f, second);

  // This function will get called for both containers and iterators,
  // so we can't assume that a container type will be set
  if(StructType* contTy = cont.getContainerType())
    changed |= moya::Metadata::setSTLContainerType(f, contTy);

  if(StructType* iterTy = cont.getIteratorType())
    changed |= moya::Metadata::setSTLIteratorType(f, iterTy);

  if(StructType* base = cont.getBaseType())
    changed |= moya::Metadata::setSTLBaseType(f, base);

  if(StructType* node = cont.getNodeType())
    changed |= moya::Metadata::setSTLNodeType(f, node);

  changed |= moya::Metadata::setSTLValueOffset(f, cont.getValueOffset());
  if(moya::STDConfig::isMapLikeContainer(kind)) {
    changed |= moya::Metadata::setSTLKeyOffset(f, cont.getKeyOffset());
    changed |= moya::Metadata::setSTLMappedOffset(f, cont.getMappedOffset());
  }

  // The "sret" attribute could be on either the first or second argument
  // (or so the documentation says. I am not sure when it can be on the second
  // argument.
  if(not f.hasStructRetAttr()) {
    llvm::StructType* contTy = cont.getContainerType();
    if(moya::STDConfig::isSTLIterator(cont.getKind()))
      contTy = cont.getIteratorType();

    llvm::Argument& arg0 = *moya::LLVMUtils::getArgument(f, 0);
    changed |= moya::Metadata::setAnalysisType(arg0, contTy->getPointerTo());

    // The parameters that we care about (iterators, containers)
    // will always be either a single pointer or a
    // reference. This is unlikely to change
    for(unsigned i = 0; i < feFunc.getDecl()->getNumParams(); i++) {
      const clang::ParmVarDecl* param = feFunc.getDecl()->getParamDecl(i);
      llvm::Argument& arg = *moya::LLVMUtils::getArgument(f, i + 1);
      const clang::Type* type = moya::ClangUtils::getInnermost(
          param->getType()->getUnqualifiedDesugaredType());
      if(cont.getClangType() == type) {
        changed |= moya::Metadata::setAnalysisType(arg, contTy->getPointerTo());
      } else if((cont.getClangMember("iterator") == type)
                or (cont.getClangMember("const_iterator") == type)
                or (cont.getClangMember("reverse_iterator") == type)
                or (cont.getClangMember("reverse_const_iterator") == type)) {
        changed |= moya::Metadata::setAnalysisType(
            arg, cont.getIteratorType()->getPointerTo());
        // The argument may be an iterator from some other container
      } else if(feModule.hasSTLContainer(type)
                and not moya::STDConfig::isSTLIterator(cont.getKind())) {
        const moya::Container& iter = feModule.getSTLContainer(type);
        if(moya::STDConfig::isSTLIterator(iter.getKind()))
          changed |= moya::Metadata::setSTLIteratorOffset(
              f, feModule.getSTLContainer(type).getValueOffset());
      }
    }
  }

  // If an STL iterator offset has not already been set, there is still a case
  // where one of the arguments to the function could be an iterator. Raw
  // pointers satisfy all the requirements for RandomAccessIterator, but
  // they would not get caught by any of the conditions in the loop before
  // this. So just set the iterator offset to zero in this case because it
  // will be correct if it the iterator is a pointer, and if there is no
  // iterator expected, it will be ignored.
  //
  // FIXME: Check that at least one of the arguments to the functions is
  // expected to be an iterator.
  //
  if(not moya::Metadata::hasSTLIteratorOffset(f))
    changed |= moya::Metadata::setSTLIteratorOffset(f, 0);

  // FIXME: There has to be a way to do this in a non-hacky way
  // Who am I kidding. This entire exercise is a gigantic hack!
  //
  // unsigned long iteratorOffset = 0;
  // if(const clang::CXXRecordDecl* iteratorDecl
  //    = feFunc.getArgumentIteratorDecl())
  //   if(const clang::CXXRecordDecl* cont
  //      = stlContext.getContainerDeclForIteratorDecl(iteratorDecl))
  //     iteratorOffset = stlContext.getValueOffset(cont);
  // if(not moya::Metadata::hasSTLIteratorOffset(f))
  //   changed |= moya::Metadata::setSTLIteratorOffset(f, iteratorOffset);

  return changed;
}

bool InstrumentSTLPass::setSTLContainerMetadata(const moya::Function& feFunc) {
  const moya::Module& feModule = feFunc.getModule();
  Function& f = feFunc.getLLVM();

  moya::Metadata::setNoAnalyze(f);
  moya::Metadata::setSTL(f);
  moya::Metadata::setStd(f);

  if(feFunc.isPublic()) {
    const clang::CXXRecordDecl* clss = feFunc.getClassDecl();
    const moya::Container& cont = feModule.getSTLContainer(clss);

    moya::Metadata::setModel(f, feFunc.getModel());
    moya::Metadata::setSTLKind(f, feFunc.getSTDKind());

    setSTLContainerMetadata(feFunc, cont);
    moya::Metadata::setSTLPointerOffset(f, cont.getPointerOffset());

    // Some STL container functions (e.g. resize) allocate an object implicitly,
    // others do so explicitly (e.g. emplace). In either case, we need to
    // determine which constructor to call for the object being allocated.
    // We always add the default constructor because, why not? For the
    // non-default constructors, we need to find the constructor which matches
    //
    // NOTE: This has to be called after STLValue has been set
    //
    if(const clang::Type* cty = cont.getClangMember("mapped_type")) {
      if(Function* ctor = getDefaultConstructor(cty))
        moya::Metadata::setSTLMappedConstructor(f, ctor);
    } else if(const clang::Type* cty = cont.getClangMember("value_type")) {
      if(Function* ctor = getDefaultConstructor(cty))
        moya::Metadata::setSTLValueConstructor(f, ctor);
    }
  } else {
    moya::Metadata::setStdImpl(f);
  }

  return true;
}

bool InstrumentSTLPass::setSTLIteratorMetadata(const moya::Function& feFunc) {
  const moya::Module& feModule = feFunc.getModule();
  Function& f = feFunc.getLLVM();

  moya::Metadata::setNoAnalyze(f);
  moya::Metadata::setSTL(f);
  moya::Metadata::setStd(f);
  if(feFunc.isPublic()) {
    const clang::CXXRecordDecl* clss = feFunc.getClassDecl();
    const moya::Container& cont = feModule.getSTLContainer(clss);

    moya::Metadata::setSTLKind(f, feFunc.getSTDKind());
    moya::Metadata::setModel(f, feFunc.getModel());

    setSTLContainerMetadata(feFunc, cont);
  } else {
    moya::Metadata::setStdImpl(f);
  }

  return true;
}

bool InstrumentSTLPass::setSTDStringMetadata(const moya::Function& feFunc) {
  const moya::Module& feModule = feFunc.getModule();
  Function& f = feFunc.getLLVM();

  moya::Metadata::setNoAnalyze(f);
  moya::Metadata::setStd(f);
  if(feFunc.isPublic()) {
    const clang::CXXRecordDecl* clss = feFunc.getClassDecl();
    const moya::Container& cont = feModule.getSTLContainer(clss);
    Type* i8 = Type::getInt8Ty(f.getContext());

    moya::Metadata::setSTLKind(f, moya::STDKind::STD_String);
    moya::Metadata::setModel(f, feFunc.getModel());

    if(StructType* contTy = cont.getContainerType())
      moya::Metadata::setSTLContainerType(f, contTy);

    if(StructType* iterTy = cont.getIteratorType())
      moya::Metadata::setSTLIteratorType(f, iterTy);

    moya::Metadata::setSTLValueType(f, i8);
    moya::Metadata::setSTLValueOffset(f, 0);
    moya::Metadata::setSTLIteratorOffset(f, 0);
    moya::Metadata::setSTLPointerOffset(f, 0);
  } else {
    moya::Metadata::setStdImpl(f);
  }

  return true;
}

bool InstrumentSTLPass::setSTDStreamMetadata(const moya::Function& feFunc) {
  Function& f = feFunc.getLLVM();

  moya::Metadata::setNoAnalyze(f);
  moya::Metadata::setStd(f);
  if(feFunc.isPublic()) {
    moya::Metadata::setSTLKind(f, feFunc.getSTDKind());
    moya::Metadata::setModel(f, feFunc.getModel());
  } else {
    moya::Metadata::setStdImpl(f);
  }

  return true;
}

bool InstrumentSTLPass::setSTDAllocatorMetadata(const moya::Function& feFunc) {
  Function& f = feFunc.getLLVM();

  moya::Metadata::setNoAnalyze(f);
  moya::Metadata::setStdImpl(f);
  if(feFunc.isPublic())
    moya::Metadata::setModel(f, feFunc.getModel());

  return true;
}

bool InstrumentSTLPass::setSTDInitializerMetadata(
    const moya::Function& feFunc) {
  const moya::Module& feModule = feFunc.getModule();
  Function& f = feFunc.getLLVM();

  moya::Metadata::setNoAnalyze(f);
  moya::Metadata::setSTL(f);

  if(feFunc.isPublic()) {
    const clang::CXXRecordDecl* clss = feFunc.getClassDecl();
    const moya::Container& cont = feModule.getSTLContainer(clss);

    moya::Metadata::setSTLKind(f, feFunc.getSTDKind());
    moya::Metadata::setModel(f, feFunc.getModel());

    if(Type* val = cont.getValueType())
      moya::Metadata::setSTLValueType(f, val);

    if(StructType* contTy = cont.getContainerType())
      moya::Metadata::setSTLContainerType(f, contTy);
  }

  return true;
}

bool InstrumentSTLPass::setSTDImplMetadata(const moya::Function& feFunc) {
  Function& f = feFunc.getLLVM();

  moya::Metadata::setNoAnalyze(f);
  moya::Metadata::setStdImpl(f);

  return true;
}

bool InstrumentSTLPass::setSTDPublicMetadata(const moya::Function& feFunc) {
  Function& f = feFunc.getLLVM();

  moya::Metadata::setNoAnalyze(f);
  moya::Metadata::setStd(f);
  if(moya::LLVMUtils::isPureFunction(f))
    moya::Metadata::setModel(f, "moya.pure.function");
  else
    moya::Metadata::setModel(f, feFunc.getModel());

  return true;
}

bool InstrumentSTLPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  for(const moya::Container& cont : feModule.getSTLContainers()) {
    // This function will get called for both containers and iterators,
    // so we can't assume that a container type will be set
    if(StructType* contTy = cont.getContainerType()) {
      const moya::Set<StructType*>& equivs = feModule.getEquivalent(contTy);
      for(StructType* sty : equivs)
        if(not moya::Metadata::hasEquivalent(sty, module))
          changed |= moya::Metadata::setEquivalent(sty, equivs, module);
    }

    if(StructType* iterTy = cont.getIteratorType()) {
      const moya::Set<StructType*>& equivs = feModule.getEquivalent(iterTy);
      for(StructType* sty : equivs)
        if(not moya::Metadata::hasEquivalent(sty, module))
          changed |= moya::Metadata::setEquivalent(sty, equivs, module);
    }
  }

  for(Function& f : module.functions()) {
    if(feModule.hasFunction(f)) {
      const moya::Function& feFunc = feModule.getFunction(f);
      moya::STDKind kind = feFunc.getSTDKind();
      if(moya::STDConfig::isSTLContainer(kind))
        changed |= setSTLContainerMetadata(feFunc);
      else if(moya::STDConfig::isSTLIterator(kind))
        changed |= setSTLIteratorMetadata(feFunc);
      else if(moya::STDConfig::isSTDStream(kind))
        changed |= setSTDStreamMetadata(feFunc);
      else if(moya::STDConfig::isSTDString(kind))
        changed |= setSTDStringMetadata(feFunc);
      else if(moya::STDConfig::isSTDAllocator(kind))
        changed |= setSTDAllocatorMetadata(feFunc);
      else if(moya::STDConfig::isSTDInitializer(kind))
        changed |= setSTDInitializerMetadata(feFunc);
      else if(moya::STDConfig::isSTDImpl(kind))
        changed |= setSTDImplMetadata(feFunc);
      else if(moya::STDConfig::isSTDPublic(kind))
        changed |= setSTDPublicMetadata(feFunc);
      else if(kind != moya::STDKind::STD_None)
        moya_error("Unsupported STD kind: " << kind << " for "
                                            << feFunc.getFullName());
    } else if(feModule.hasDeclaration(f)) {
      const moya::Function& feFunc = feModule.getDeclaration(f);
      moya::STDKind kind = feFunc.getSTDKind();
      if(moya::STDConfig::isSTDString(kind))
        changed |= setSTDStringMetadata(feFunc);
    }
  }

  return changed;
}

char InstrumentSTLPass::ID = 0;

static const char* name = "moya-instr-stl";
static const char* descr = "Add instrumentation to STL functions";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(InstrumentSTLPass, name, descr, cfg, analysis);
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass);
INITIALIZE_PASS_END(InstrumentSTLPass, name, descr, cfg, analysis);

Pass* createInstrumentSTLPass() {
  return new InstrumentSTLPass();
}
