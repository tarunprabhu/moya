#include "STLContext.h"
#include "Module.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "frontend/c-family/ClangUtils.h"

namespace moya {

STLContext::STLContext(Module& module) : module(module) {
  ;
}

unsigned long STLContext::getOffset(llvm::StructType* sty, unsigned index) {
  return layout.getStructLayout(sty).getElementOffset(index);
}

void STLContext::initStringContainer(const clang::CXXRecordDecl* stl,
                                     llvm::Module& module) {
  // std::string = type { std::string::_Alloc_hider, i64, %union }
  // std::string::_Alloc_hider = type { i8* }
  // %union = type { i64, [8 x i8] }
  //
  ContainerTypes& spec = containers.at(stl);
  llvm::StructType* contTy = spec.contTy;
  llvm::StructType* iterTy = spec.iterTy;
  llvm::LLVMContext& context = module.getContext();
  llvm::Type* i8 = llvm::Type::getInt8Ty(context);
  llvm::Type* i64 = llvm::Type::getInt64Ty(context);
  llvm::Type* pi8 = i8->getPointerTo();
  llvm::ArrayType* aty = llvm::ArrayType::get(i8, 8);

  moya::Vector<llvm::Type*> elemsCont = {pi8, i64, i64, aty};
  contTy->setBody(moya::LLVMUtils::makeArrayRef(elemsCont), false);
  moya::Metadata::setAnalysisType(contTy, module);

  moya::Vector<llvm::Type*> elemsIter = {pi8};
  iterTy->setBody(moya::LLVMUtils::makeArrayRef(elemsIter), false);
  moya::Metadata::setAnalysisType(iterTy, module);

  addIterator(stl, "iterator");
  addIterator(stl, "const_iterator");
  addIterator(stl, "reverse_iterator");
  addIterator(stl, "const_reverse_iterator");
}

void STLContext::initVectorContainer(const clang::CXXRecordDecl* stl,
                                     llvm::Module& module) {
  // std::vector = type { std::_Vector_base }
  // std::_Vector_base = type { std::_Vector_impl }
  // std::_Vector_impl = type { VALUE_TYPE*, VALUE_TYPE*, VALUE_TYPE* }
  //
  ContainerTypes& spec = containers.at(stl);
  llvm::StructType* contTy = spec.contTy;
  llvm::StructType* iterTy = spec.iterTy;

  if(llvm::Type* elemTy = getMemberLLVMType(stl, "value_type")) {
    llvm::PointerType* pElemTy = elemTy->getPointerTo();

    moya::Vector<llvm::Type*> elemsCont = {pElemTy, pElemTy, pElemTy};
    contTy->setBody(moya::LLVMUtils::makeArrayRef(elemsCont), false);
    moya::Metadata::setAnalysisType(contTy, module);

    moya::Vector<llvm::Type*> elemsIter = {pElemTy};
    iterTy->setBody(moya::LLVMUtils::makeArrayRef(elemsIter), false);
    moya::Metadata::setAnalysisType(iterTy, module);
  }

  addIterator(stl, "iterator");
  addIterator(stl, "const_iterator");
  addIterator(stl, "reverse_iterator");
  addIterator(stl, "const_reverse_iterator");
}

void STLContext::initBitVectorContainer(const clang::CXXRecordDecl* stl,
                                        llvm::Module& module) {
  // std::vector<bool> = type { std::_Bvector_base }
  // std::_Bvector_base = type { std::_Bvector_impl }
  // std::_Bvector_impl = type { std::_Bit_iterator, std::_Bit_iterator, i64* }
  // std::_Bit_iterator = type { std::_Bit_iterator_base, [4 x i8] }
  // std::_Bit_iterator_base = type { i64*, i32 }
  //
  ContainerTypes& spec = containers.at(stl);
  llvm::StructType* contTy = spec.contTy;
  llvm::StructType* iterTy = spec.iterTy;
  llvm::LLVMContext& context = module.getContext();
  llvm::Type* pi64 = llvm::Type::getInt64PtrTy(context);
  llvm::Type* i32 = llvm::Type::getInt32Ty(context);

  moya::Vector<llvm::Type*> elemsCont = {iterTy, iterTy, pi64};
  contTy->setBody(moya::LLVMUtils::makeArrayRef(elemsCont), false);
  moya::Metadata::setAnalysisType(contTy, module);

  moya::Vector<llvm::Type*> elemsIter = {pi64, i32};
  iterTy->setBody(moya::LLVMUtils::makeArrayRef(elemsIter), false);
  moya::Metadata::setAnalysisType(iterTy, module);

  addIterator(stl, "iterator");
  addIterator(stl, "const_iterator");
  addIterator(stl, "reverse_iterator");
  addIterator(stl, "const_reverse_iterator");
}

void STLContext::initTreeContainer(const clang::CXXRecordDecl* stl,
                                   llvm::Module& module) {
  // std::set" = type { std::_Rb_tree }
  // std::_Rb_tree = type { std::_Rb_tree_impl }
  // std::_Rb_tree_impl = type { std::_Rb_tree_key_compare,
  //                             std::_Rb_tree_header }
  // std::_Rb_tree_header = type { std::_Rb_tree_node_base, i64 }
  // std::_Rb_tree_node_base = type { i32,
  //                                  std::_Rb_tree_node_base*,
  //                                  std::_Rb_tree_node_base*,
  //                                  std::_Rb_tree_node_base* }
  //
  // For sets:
  //
  // std::_Rb_tree_node = type { std::_Rb_tree_node_base, VALUE_TYPE }
  //
  //
  // For maps:
  //
  // std::_Rb_tree_node = type { std::_Rb_tree_node_base, std::pair }
  // std::pair = type { KEY_TYPE, MAPPED_TYPE }
  //
  llvm::LLVMContext& context = module.getContext();
  ContainerTypes& spec = containers.at(stl);
  STDKind kind = spec.kind;
  llvm::StructType* contTy = spec.contTy;
  llvm::StructType* iterTy = spec.iterTy;
  llvm::StructType* baseTy = spec.baseTy;
  llvm::StructType* nodeTy = spec.nodeTy;
  llvm::StructType* mapValueTy = spec.mapValueTy;
  llvm::PointerType* pBaseTy = baseTy->getPointerTo();
  llvm::Type* elemTy = getMemberLLVMType(stl, "value_type");
  llvm::Type* keyTy = getMemberLLVMType(stl, "key_type");
  llvm::Type* mappedTy = getMemberLLVMType(stl, "mapped_type");
  llvm::Type* i8 = llvm::Type::getInt8Ty(context);
  llvm::Type* i32 = llvm::Type::getInt32Ty(context);
  llvm::Type* i64 = llvm::Type::getInt64Ty(context);

  moya::Vector<llvm::Type*> elemsCont = {i8, baseTy, i64};
  contTy->setBody(moya::LLVMUtils::makeArrayRef(elemsCont), false);
  moya::Metadata::setAnalysisType(contTy, module);

  moya::Vector<llvm::Type*> elemsIter = {pBaseTy};
  iterTy->setBody(moya::LLVMUtils::makeArrayRef(elemsIter), false);
  moya::Metadata::setAnalysisType(iterTy, module);

  moya::Vector<llvm::Type*> elemsBase = {i32, pBaseTy, pBaseTy, pBaseTy};
  baseTy->setBody(moya::LLVMUtils::makeArrayRef(elemsBase), false);
  moya::Metadata::setAnalysisType(baseTy, module);

  if(elemTy or (keyTy and mappedTy)) {
    moya::Vector<llvm::Type*> elemsNode = {baseTy};
    if((kind == STDKind::STD_STL_set) or (kind == STDKind::STD_STL_multiset)) {
      elemsNode.push_back(elemTy);
    } else {
      moya::Vector<llvm::Type*> elemsMapValue = {keyTy, mappedTy};
      mapValueTy->setBody(moya::LLVMUtils::makeArrayRef(elemsMapValue), false);
      moya::Metadata::setAnalysisType(mapValueTy, module);
      elemsNode.push_back(mapValueTy);
      llvm::errs() << "mapped: " << *mapValueTy << " [" << mapValueTy << "] "
                   << module.getTypeByName(mapValueTy->getName()) << "\n";
      spec.offsetMapped = getOffset(mapValueTy, 1);
    }
    nodeTy->setBody(moya::LLVMUtils::makeArrayRef(elemsNode), false);
    moya::Metadata::setAnalysisType(nodeTy, module);

    spec.offsetPtr = getOffset(contTy, 1) + getOffset(baseTy, 1);
    spec.offsetVal = getOffset(nodeTy, 1);
    if((kind == STDKind::STD_STL_map) or (kind == STDKind::STD_STL_multimap)) {
      spec.offsetKey = getOffset(nodeTy, 1);
      spec.offsetMapped += getOffset(nodeTy, 1);
    }
  }

  addIterator(stl, "iterator");
  addIterator(stl, "const_iterator");
  addIterator(stl, "reverse_iterator");
  addIterator(stl, "const_reverse_iterator");
}

void STLContext::initDequeContainer(const clang::CXXRecordDecl* stl,
                                    llvm::Module& module) {
  // std::deque" = type { %"class.std::_Deque_base" }
  // std::_Deque_base = type { std::_Deque_impl" }
  // std::_Deque_impl = type { VALUE_TYPE**,
  //                           i64,
  //                           std::_Deque_iterator,
  //                           std::_Deque_iterator }
  // std::_Deque_iterator = type { VALUE_TYPE*,
  //                               VALUE_TYPE*,
  //                               VALUE_TYPE*,
  //                               VALUE_TYPE** }
  //
  llvm::LLVMContext& context = module.getContext();
  ContainerTypes& spec = containers.at(stl);
  llvm::StructType* contTy = spec.contTy;
  llvm::StructType* iterTy = spec.iterTy;

  if(llvm::Type* elemTy = getMemberLLVMType(stl, "value_type")) {
    llvm::Type* pElemTy = elemTy->getPointerTo();
    llvm::Type* ppElemTy = elemTy->getPointerTo()->getPointerTo();
    llvm::Type* i64 = llvm::Type::getInt64Ty(context);

    moya::Vector<llvm::Type*> elemsCont = {ppElemTy, i64, iterTy, iterTy};
    contTy->setBody(moya::LLVMUtils::makeArrayRef(elemsCont), false);
    moya::Metadata::setAnalysisType(contTy, module);

    moya::Vector<llvm::Type*> elemsIter = {pElemTy, pElemTy, pElemTy, ppElemTy};
    iterTy->setBody(moya::LLVMUtils::makeArrayRef(elemsIter), false);
    moya::Metadata::setAnalysisType(iterTy, module);

    spec.offsetPtr = getOffset(contTy, 2);
  }

  addIterator(stl, "iterator");
  addIterator(stl, "const_iterator");
  addIterator(stl, "reverse_iterator");
  addIterator(stl, "const_reverse_iterator");
}

void STLContext::initHashtableContainer(const clang::CXXRecordDecl* stl,
                                        llvm::Module& module) {
  // std::unordered_set = type { std::_Hashtable }
  // std::_Hashtable = type { std::_Hash_node_base**,
  //                          i64,
  //                          std::_Hash_node_base,
  //                          i64,
  //                          std::_Prime_rehash_policy,
  //                          std::_Hash_node_base* }
  // std::_Hash_node_base = type { std::_Hash_node_base* }
  //
  // For sets:
  //
  // std::_Hash_node = type { std::_Hash_node_base, VALUE_TYPE }
  //
  //
  // For maps:
  //
  // std::_Hash_node = type { std::_Hash_node_base, std::pair }
  // std::pair = type { KEY_TYPE, MAPPED_TYPE }
  //
  llvm::LLVMContext& context = module.getContext();
  ContainerTypes& spec = containers.at(stl);
  STDKind kind = spec.kind;
  llvm::StructType* contTy = spec.contTy;
  llvm::StructType* iterTy = spec.iterTy;
  llvm::StructType* baseTy = spec.baseTy;
  llvm::StructType* nodeTy = spec.nodeTy;
  llvm::StructType* mapValueTy = spec.mapValueTy;
  llvm::Type* elemTy = getMemberLLVMType(stl, "mapped_type");
  llvm::Type* keyTy = getMemberLLVMType(stl, "key_type");
  llvm::Type* mappedTy = getMemberLLVMType(stl, "mapped_type");
  llvm::PointerType* pBaseTy = baseTy->getPointerTo();
  llvm::PointerType* ppBaseTy = baseTy->getPointerTo()->getPointerTo();
  llvm::Type* f32 = llvm::Type::getFloatTy(context);
  llvm::Type* i64 = llvm::Type::getInt64Ty(context);
  llvm::StructType* rehashTy
      = llvm::StructType::create("struct.moya.stl.rehash", f32, i64);

  moya::Vector<llvm::Type*> elemsCont
      = {ppBaseTy, i64, baseTy, i64, rehashTy, pBaseTy};
  contTy->setBody(moya::LLVMUtils::makeArrayRef(elemsCont), false);
  moya::Metadata::setAnalysisType(contTy, module);

  moya::Vector<llvm::Type*> elemsIter = {baseTy};
  iterTy->setBody(moya::LLVMUtils::makeArrayRef(elemsIter), false);
  moya::Metadata::setAnalysisType(iterTy, module);

  moya::Vector<llvm::Type*> elemsBase = {pBaseTy};
  baseTy->setBody(moya::LLVMUtils::makeArrayRef(elemsBase), false);
  moya::Metadata::setAnalysisType(baseTy, module);

  if(elemTy or (keyTy and mappedTy)) {
    moya::Vector<llvm::Type*> elemsNode = {pBaseTy};
    if((kind == STDKind::STD_STL_unordered_set)
       or (kind == STDKind::STD_STL_unordered_multiset)) {
      elemsNode.push_back(elemTy);
    } else {
      moya::Vector<llvm::Type*> elemsMapValue = {keyTy, mappedTy};
      mapValueTy->setBody(moya::LLVMUtils::makeArrayRef(elemsMapValue), false);
      moya::Metadata::setAnalysisType(mapValueTy, module);
      elemsNode.push_back(mapValueTy);
      spec.offsetMapped = getOffset(mapValueTy, 1);
    }
    nodeTy->setBody(moya::LLVMUtils::makeArrayRef(elemsNode), false);
    moya::Metadata::setAnalysisType(nodeTy, module);

    spec.offsetPtr = getOffset(contTy, 2);
    spec.offsetVal = getOffset(nodeTy, 1);
    if((kind == STDKind::STD_STL_unordered_map)
       or (kind == STDKind::STD_STL_unordered_multimap)) {
      spec.offsetKey = getOffset(nodeTy, 1);
      spec.offsetMapped += getOffset(nodeTy, 1);
    }
  }

  addIterator(stl, "iterator");
  addIterator(stl, "const_iterator");
}

void STLContext::initListContainer(const clang::CXXRecordDecl* stl,
                                   llvm::Module& module) {
  // std::list = type { std::_List_base }
  // std::_List_base = type { std::_List_impl }
  // std::_List_impl = type { std::_List_node }
  // std::_List_node = type { std_List_node_base, VALUE_TYPE }
  // std::_List_node_base = type { std::_List_node_base*, std::_List_node_base*}
  //
  ContainerTypes& spec = containers.at(stl);
  llvm::StructType* contTy = spec.contTy;
  llvm::StructType* iterTy = spec.iterTy;
  llvm::StructType* baseTy = spec.baseTy;
  llvm::StructType* nodeTy = spec.nodeTy;

  if(llvm::Type* elemTy = getMemberLLVMType(stl, "value_type")) {
    llvm::PointerType* pBaseTy = baseTy->getPointerTo();
    llvm::Type* i64 = llvm::Type::getInt64Ty(module.getContext());

    moya::Vector<llvm::Type*> elemsCont = {baseTy, i64};
    contTy->setBody(moya::LLVMUtils::makeArrayRef(elemsCont), false);
    moya::Metadata::setAnalysisType(contTy, module);

    moya::Vector<llvm::Type*> elemsIter = {pBaseTy};
    iterTy->setBody(moya::LLVMUtils::makeArrayRef(elemsIter), false);
    moya::Metadata::setAnalysisType(iterTy, module);

    moya::Vector<llvm::Type*> elemsBase = {pBaseTy, pBaseTy};
    baseTy->setBody(moya::LLVMUtils::makeArrayRef(elemsBase), false);
    moya::Metadata::setAnalysisType(baseTy, module);

    moya::Vector<llvm::Type*> elemsNode = {baseTy, elemTy};
    nodeTy->setBody(moya::LLVMUtils::makeArrayRef(elemsNode), false);
    moya::Metadata::setAnalysisType(nodeTy, module);

    llvm::errs() << "node: " << *nodeTy << " => [" << nodeTy << "]\n";
    spec.offsetVal = getOffset(nodeTy, 1);
  }

  addIterator(stl, "iterator");
  addIterator(stl, "const_iterator");
  addIterator(stl, "reverse_iterator");
  addIterator(stl, "const_reverse_iterator");
}

void STLContext::initForwardListContainer(const clang::CXXRecordDecl* stl,
                                          llvm::Module& module) {
  // std::forward_list = type { std::_Fwd_list_base }
  // std::_Fwd_list_base = type { std::_Fwd_list_impl }
  // std::_Fwd_list_impl = type { std::_Fwd_list_node_base }
  // std::_Fwd_list_node_base = type { struct.std::_Fwd_list_node_base* }
  // std::_Fwd_list_node = type { std::_Fwd_list_node_base, VALUE_TYPE }
  //
  ContainerTypes& spec = containers.at(stl);
  llvm::StructType* contTy = spec.contTy;
  llvm::StructType* iterTy = spec.iterTy;
  llvm::StructType* baseTy = spec.baseTy;
  llvm::StructType* nodeTy = spec.nodeTy;

  if(llvm::Type* elemTy = getMemberLLVMType(stl, "value_type")) {
    llvm::PointerType* pBaseTy = baseTy->getPointerTo();

    moya::Vector<llvm::Type*> elemsCont = {baseTy};
    contTy->setBody(moya::LLVMUtils::makeArrayRef(elemsCont), false);
    moya::Metadata::setAnalysisType(contTy, module);

    moya::Vector<llvm::Type*> elemsIter = {pBaseTy};
    iterTy->setBody(moya::LLVMUtils::makeArrayRef(elemsIter), false);
    moya::Metadata::setAnalysisType(iterTy, module);

    moya::Vector<llvm::Type*> elemsBase = {pBaseTy};
    baseTy->setBody(moya::LLVMUtils::makeArrayRef(elemsBase), false);
    moya::Metadata::setAnalysisType(baseTy, module);

    moya::Vector<llvm::Type*> elemsNode = {baseTy, elemTy};
    nodeTy->setBody(moya::LLVMUtils::makeArrayRef(elemsNode), false);
    moya::Metadata::setAnalysisType(nodeTy, module);

    spec.offsetVal = getOffset(nodeTy, 1);
  }

  addIterator(stl, "iterator");
  addIterator(stl, "const_iterator");
}

void STLContext::initInitializerListContainer(const clang::CXXRecordDecl* stl,
                                              llvm::Module& module) {
  // std::initializer_list = type { VALUE_TYPE*, i64 }
  //
  llvm::LLVMContext& context = module.getContext();
  ContainerTypes& spec = containers.at(stl);
  llvm::StructType* contTy = spec.contTy;

  if(llvm::Type* elemTy = getMemberLLVMType(stl, "value_type")) {
    llvm::PointerType* pElemTy = elemTy->getPointerTo();
    llvm::Type* i64 = llvm::Type::getInt64Ty(context);

    moya::Vector<llvm::Type*> elemsCont = {pElemTy, i64};
    contTy->setBody(moya::LLVMUtils::makeArrayRef(elemsCont), false);
    moya::Metadata::setAnalysisType(contTy, module);
  }
}

void STLContext::initPairContainer(const clang::CXXRecordDecl* stl,
                                   llvm::Module& module) {
  // std::pair = type { FIRST_TYPE, SECOND_TYPE }
  //
  ContainerTypes& spec = containers.at(stl);
  llvm::StructType* contTy = spec.contTy;
  llvm::Type* first = getMemberLLVMType(stl, "first_type");
  llvm::Type* second = getMemberLLVMType(stl, "second_type");

  if(first and second) {
    moya::Vector<llvm::Type*> elemsCont = {first, second};
    contTy->setBody(moya::LLVMUtils::makeArrayRef(elemsCont), false);
    moya::Metadata::setAnalysisType(contTy, module);
  }
}

void STLContext::addIterator(const clang::CXXRecordDecl* stl,
                             const std::string& member) {
  if(const auto* decl = getMemberClangType(stl, member)->getAsCXXRecordDecl()) {
    if(const clang::CXXRecordDecl* iterator = ClangUtils::getDefinition(decl)) {
      iterators[iterator] = stl;

      // For some reason, both "std::_Rb_tree::iterator" and
      // "std::_Rb_tree::const_iterator" both get set to
      // "struct._Rb_tree_const_iterator". But the const iterator struct
      // has a field "iterator" which we need to find the non-const
      // iterator so we can correctly annotate all the iterator methods
      for(const clang::Decl* d : iterator->decls())
        if(const auto* typdef = llvm::dyn_cast<clang::TypedefNameDecl>(d))
          if(typdef->getName() == "iterator")
            if(const auto* it
               = ClangUtils::getUnderlyingType(typdef)->getAsCXXRecordDecl())
              if(const auto* defn = ClangUtils::getDefinition(it))
                iterators[defn] = stl;
    }
  }
}

void STLContext::addContainer(const clang::CXXRecordDecl* clss,
                              llvm::LLVMContext& context) {
  if(containers.contains(clss))
    return;

  llvm::StructType* contTy = nullptr;
  llvm::StructType* iterTy = nullptr;
  llvm::StructType* nodeTy = nullptr;
  llvm::StructType* baseTy = nullptr;
  llvm::StructType* mapValueTy = nullptr;
  STDKind kind = ClangUtils::getSTDKind(clss);
  containers.emplace(std::make_pair(clss, kind));

  if(kind == STDKind::STD_STL_pair) {
    contTy = llvm::StructType::create(context, "struct.moya.stl.pair");
  } else if(STDConfig::isSTDInitializer(kind)) {
    contTy
        = llvm::StructType::create(context, "struct.moya.stl.initializer_list");
  } else if(STDConfig::isVectorContainer(kind)) {
    contTy = llvm::StructType::create(context, "struct.moya.stl.vector");
    iterTy
        = llvm::StructType::create(context, "struct.moya.stl.vector.iterator");
  } else if(STDConfig::isBitVectorContainer(kind)) {
    contTy = llvm::StructType::create(context, "struct.moya.stl.bitvector");
    iterTy = llvm::StructType::create(context,
                                      "struct.moya.stl.bitvector.iterator");
  } else if(STDConfig::isTreeContainer(kind)) {
    contTy = llvm::StructType::create(context, "struct.moya.stl.tree");
    iterTy = llvm::StructType::create(context, "struct.moya.stl.tree.iterator");
    nodeTy = llvm::StructType::create(context, "struct.moya.stl.tree.node");
    baseTy
        = llvm::StructType::create(context, "struct.moya.stl.tree.node.base");
    if(STDConfig::isMapLikeContainer(kind)) {
      mapValueTy = llvm::StructType::create(context, "struct.moya.stl.pair");
      llvm::errs() << "CREATED: " << mapValueTy->getName() << " => "
                   << mapValueTy << "\n";
    }
  } else if(STDConfig::isHashtableContainer(kind)) {
    contTy = llvm::StructType::create(context, "struct.moya.stl.hashtable");
    iterTy = llvm::StructType::create(context,
                                      "struct.moya.stl.hashtable.iterator");
    nodeTy
        = llvm::StructType::create(context, "struct.moya.stl.hashtable.node");
    baseTy = llvm::StructType::create(context,
                                      "struct.moya.stl.hashtable.node.base");
  } else if(STDConfig::isDequeContainer(kind)) {
    contTy = llvm::StructType::create(context, "struct.moya.stl.deque");
    iterTy
        = llvm::StructType::create(context, "struct.moya.stl.deque.iterator");
  } else if(STDConfig::isListContainer(kind)) {
    contTy = llvm::StructType::create(context, "struct.moya.stl.list");
    iterTy = llvm::StructType::create(context, "struct.moya.stl.list.iterator");
    nodeTy = llvm::StructType::create(context, "struct.moya.stl.list.node");
    baseTy
        = llvm::StructType::create(context, "struct.moya.stl.list.node.base");
  } else if(STDConfig::isForwardListContainer(kind)) {
    contTy = llvm::StructType::create(context, "struct.moya.stl.fwd_list");
    iterTy = llvm::StructType::create(context,
                                      "struct.moya.stl.fwd_list.iterator");
    nodeTy = llvm::StructType::create(context, "struct.moya.stl.fwd_list.node");
    baseTy = llvm::StructType::create(context,
                                      "struct.moya.stl.fwd_list.node.base");
  } else if(STDConfig::isSTDString(kind)) {
    contTy = llvm::StructType::create(context, "struct.moya.string");
    iterTy = llvm::StructType::create(context, "struct.moya.string.iterator");
  } else {
    moya_error("Unsupported container type: " << kind);
  }

  ContainerTypes& spec = containers.at(clss);
  spec.contTy = contTy;
  spec.iterTy = iterTy;
  spec.baseTy = baseTy;
  spec.nodeTy = nodeTy;
  spec.mapValueTy = mapValueTy;
  for(const clang::Decl* d : clss->decls()) {
    if(const auto* typdef = llvm::dyn_cast<clang::TypedefNameDecl>(d)) {
      const clang::Type* type = ClangUtils::getUnderlyingType(typdef);
      llvm::StringRef name = typdef->getName();
      if(const clang::RecordDecl* decl = type->getAsRecordDecl())
        if(const clang::RecordDecl* defn = ClangUtils::getDefinition(decl))
          spec.members[name] = defn->getTypeForDecl();
        else
          spec.members[name] = decl->getTypeForDecl();
      else
        spec.members[name] = type;
    }
  }
}

void STLContext::associate(llvm::Module& module) {
  layout.reset(module);

  // This has to be done in two passes because a container could contain
  // another container, so we first find all the containers in the first pass
  // and initialize them in the second
  for(const Type* type : this->module.getClasses()) {
    STDKind kind = type->getSTDKind();
    if(STDConfig::isSTLContainer(kind) or STDConfig::isSTDInitializer(kind)
       or STDConfig::isSTDString(kind))
      addContainer(type->getCXXDecl(), module.getContext());
  }

  // Second pass
  for(const auto& it : containers) {
    const clang::CXXRecordDecl* stl = it.first;
    STDKind kind = it.second.kind;
    if(STDConfig::isSTDInitializer(kind))
      initInitializerListContainer(stl, module);
    else if(kind == STDKind::STD_STL_pair)
      initPairContainer(stl, module);
    else if(STDConfig::isVectorContainer(kind))
      initVectorContainer(stl, module);
    else if(STDConfig::isBitVectorContainer(kind))
      initBitVectorContainer(stl, module);
    else if(STDConfig::isDequeContainer(kind))
      initDequeContainer(stl, module);
    else if(STDConfig::isTreeContainer(kind))
      initTreeContainer(stl, module);
    else if(STDConfig::isHashtableContainer(kind))
      initHashtableContainer(stl, module);
    else if(STDConfig::isForwardListContainer(kind))
      initForwardListContainer(stl, module);
    else if(STDConfig::isListContainer(kind))
      initListContainer(stl, module);
    else if(STDConfig::isSTDString(kind))
      initStringContainer(stl, module);
  }
}

const clang::CXXRecordDecl* STLContext::getContainerDeclForIteratorDecl(
    const clang::CXXRecordDecl* iteratorDecl) const {
  return iterators.at(iteratorDecl);
}

unsigned long
STLContext::getPointerOffset(const clang::CXXRecordDecl* stl) const {
  return containers.at(stl).offsetPtr;
}

unsigned long
STLContext::getValueOffset(const clang::CXXRecordDecl* stl) const {
  return containers.at(stl).offsetVal;
}

unsigned long STLContext::getKeyOffset(const clang::CXXRecordDecl* stl) const {
  return containers.at(stl).offsetKey;
}

unsigned long
STLContext::getMappedOffset(const clang::CXXRecordDecl* stl) const {
  return containers.at(stl).offsetMapped;
}

bool STLContext::hasMemberType(const clang::CXXRecordDecl* stl,
                               const std::string& name) const {
  return containers.at(stl).members.contains(name);
}

const clang::Type*
STLContext::getMemberClangType(const clang::CXXRecordDecl* stl,
                               const std::string& name) const {
  return containers.at(stl).members.at(name);
}

llvm::Type* STLContext::getMemberLLVMType(const clang::CXXRecordDecl* stl,
                                          const std::string& name) const {
  const clang::Type* astType = getMemberClangType(stl, name);
  if(const clang::CXXRecordDecl* clss = astType->getAsCXXRecordDecl()) {
    if(llvm::StructType* contTy = getContainerType(clss))
      return contTy;
    else if(llvm::StructType* iterTy = getIteratorType(clss))
      return iterTy;
    else if(llvm::StructType* baseTy = getBaseType(clss))
      return baseTy;
    else if(llvm::StructType* nodeTy = getNodeType(clss))
      return nodeTy;
    else if(llvm::StructType* mapValueTy = getMapValueType(clss))
      return mapValueTy;
  }

  // The internal type may not actually be exported to the LLVM file
  // Not really sure how that can happen with forward declarations in a
  // template expansion, but what can I say
  if(module.hasType(astType))
    return module.getType(astType).getLLVM();

  return nullptr;
}

llvm::StructType*
STLContext::getContainerType(const clang::CXXRecordDecl* clss) const {
  if(containers.contains(clss))
    return containers.at(clss).contTy;
  return nullptr;
}

llvm::StructType*
STLContext::getIteratorType(const clang::CXXRecordDecl* clss) const {
  if(containers.contains(clss))
    return containers.at(clss).iterTy;
  return nullptr;
}

llvm::StructType*
STLContext::getBaseType(const clang::CXXRecordDecl* clss) const {
  if(containers.contains(clss))
    return containers.at(clss).baseTy;
  return nullptr;
}

llvm::StructType*
STLContext::getNodeType(const clang::CXXRecordDecl* clss) const {
  if(containers.contains(clss))
    return containers.at(clss).nodeTy;
  return nullptr;
}

llvm::StructType*
STLContext::getMapValueType(const clang::CXXRecordDecl* clss) const {
  if(containers.contains(clss))
    return containers.at(clss).mapValueTy;
  return nullptr;
}

} // namespace moya
