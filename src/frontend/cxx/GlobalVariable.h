#ifndef MOYA_FRONTEND_CXX_GLOBAL_VARIABLE_H
#define MOYA_FRONTEND_CXX_GLOBAL_VARIABLE_H

#include "frontend/c-family/GlobalVariableClang.h"

namespace moya {

class Module;

class GlobalVariable : public GlobalVariableClang {
protected:
  std::string fullName;
  std::string qualName;

protected:
  GlobalVariable(ModuleBase&, const clang::VarDecl*, llvm::GlobalVariable&);

public:
  GlobalVariable(const GlobalVariable&) = delete;
  virtual ~GlobalVariable() = default;

  Module& getModule();
  const Module& getModule() const;
  bool hasFullName() const;
  const std::string& getFullName() const;
  bool hasQualifiedName() const;
  const std::string& getQualifiedName() const;

  friend class ModuleBase;
};

} // namespace moya

#endif // MOYA_FRONTEND_CXX_GLOBAL_VARIABLE_H
