#ifndef MOYA_FRONTEND_CXX_CODEGEN_CONTEXT_H
#define MOYA_FRONTEND_CXX_CODEGEN_CONTEXT_H

#include "frontend/c-family/CodeGeneratorClang.h"

namespace moya {

class Annotator;

class CodeGenerator : public CodeGeneratorClang {
public:
  CodeGenerator(Annotator&);
  virtual ~CodeGenerator() = default;

  CG_HANDLE_(void, HandleCXXImplicitFunctionInstantiation, clang::FunctionDecl*)
  CG_HANDLE_(void, AssignInheritanceModel, clang::CXXRecordDecl*);
  CG_HANDLE_(void, HandleCXXStaticMemberVarInstantiation, clang::VarDecl*);
  CG_HANDLE_(void, HandleVTable, clang::CXXRecordDecl*);
};

} // namespace moya

#endif // MOYA_FRONTEND_CXX_CODEGEN_CONTEXT_H
