#ifndef MOYA_FRONTEND_CXX_STL_CONTEXT_H
#define MOYA_FRONTEND_CXX_STL_CONTEXT_H

#include "Function.h"
#include "Type.h"
#include "common/DataLayout.h"
#include "common/Map.h"
#include "common/STDConfig.h"

#include <clang/AST/DeclCXX.h>
#include <clang/AST/Type.h>
#include <clang/CodeGen/CodeGenABITypes.h>

#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>

#include <string>

namespace moya {

class Module;

class STLContext {
private:
  typedef struct ContainerTypesT {
    moya::Map<std::string, const clang::Type*> members;
    STDKind kind;

    llvm::StructType* contTy;
    llvm::StructType* iterTy;
    llvm::StructType* baseTy;
    llvm::StructType* nodeTy;
    // The value type in a STL map is a pair for which we create a custom
    // struct because the STL pair that will be created by the compiler will
    // have explicit buffer space included in the struct
    llvm::StructType* mapValueTy;

    unsigned long offsetPtr;
    unsigned long offsetVal;
    unsigned long offsetKey;
    unsigned long offsetMapped;
    unsigned long offsetIterator;

    ContainerTypesT(STDKind kind)
        : kind(kind), contTy(nullptr), iterTy(nullptr), baseTy(nullptr),
          nodeTy(nullptr), offsetPtr(0), offsetVal(0), offsetKey(0),
          offsetMapped(0), offsetIterator(0) {
      ;
    }
  } ContainerTypes;

private:
  Module& module;
  moya::DataLayout layout;

  moya::Map<const clang::CXXRecordDecl*, const clang::CXXRecordDecl*> iterators;
  moya::Map<const clang::CXXRecordDecl*, ContainerTypes> containers;

private:
  unsigned long getOffset(llvm::StructType*, unsigned);

  void initPairContainer(const clang::CXXRecordDecl*, llvm::Module&);
  void initInitializerListContainer(const clang::CXXRecordDecl*, llvm::Module&);
  void initVectorContainer(const clang::CXXRecordDecl*, llvm::Module&);
  void initBitVectorContainer(const clang::CXXRecordDecl*, llvm::Module&);
  void initDequeContainer(const clang::CXXRecordDecl*, llvm::Module&);
  void initTreeContainer(const clang::CXXRecordDecl*, llvm::Module&);
  void initHashtableContainer(const clang::CXXRecordDecl*, llvm::Module&);
  void initListContainer(const clang::CXXRecordDecl*, llvm::Module&);
  void initForwardListContainer(const clang::CXXRecordDecl*, llvm::Module&);
  void initStringContainer(const clang::CXXRecordDecl*, llvm::Module&);

  void addContainer(const clang::CXXRecordDecl*, llvm::LLVMContext&);
  void addIterator(const clang::CXXRecordDecl*, const std::string&);

public:
  STLContext(Module&);

  const clang::CXXRecordDecl*
  getContainerDeclForIteratorDecl(const clang::CXXRecordDecl*) const;

  llvm::StructType* getContainerType(const clang::CXXRecordDecl*) const;
  llvm::StructType* getIteratorType(const clang::CXXRecordDecl*) const;
  llvm::StructType* getBaseType(const clang::CXXRecordDecl*) const;
  llvm::StructType* getNodeType(const clang::CXXRecordDecl*) const;
  llvm::StructType* getMapValueType(const clang::CXXRecordDecl*) const;
  unsigned long getPointerOffset(const clang::CXXRecordDecl*) const;
  unsigned long getValueOffset(const clang::CXXRecordDecl*) const;
  unsigned long getKeyOffset(const clang::CXXRecordDecl*) const;
  unsigned long getMappedOffset(const clang::CXXRecordDecl*) const;
  unsigned long getIteratorOffset(const clang::CXXRecordDecl*) const;

  bool hasMemberType(const clang::CXXRecordDecl*, const std::string&) const;
  const clang::Type* getMemberClangType(const clang::CXXRecordDecl*,
                                        const std::string&) const;
  llvm::Type* getMemberLLVMType(const clang::CXXRecordDecl*,
                                const std::string&) const;

  void associate(llvm::Module&);
};

} // namespace moya

#endif // MOYA_FRONTEND_ELEMENTS_TYPES_H
