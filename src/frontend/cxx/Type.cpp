#include "Type.h"
#include "Module.h"
#include "frontend/c-family/ClangUtils.h"

using llvm::dyn_cast;
using llvm::dyn_cast_or_null;

namespace moya {

Type::Type(ModuleBase& moduleBase, const clang::Type* type, llvm::Type* llvm)
    : TypeClang(moduleBase, type), qualifiedName(""), fullName(""),
      kind(ClangUtils::getSTDKind(type)), polymorphic(false),
      canonical(nullptr) {
  if(const auto* clss = llvm::dyn_cast_or_null<clang::CXXRecordDecl>(decl)) {
    qualifiedName = ClangUtils::getQualifiedDeclName(clss);
    fullName = ClangUtils::getFullDeclName(clss);
    polymorphic = clss->isPolymorphic();
  }
  setLLVM(llvm);
}

bool Type::hasQualifiedName() const {
  return qualifiedName.length();
}

bool Type::hasFullName() const {
  return fullName.length();
}

bool Type::isClass() const {
  return getCXXDecl();
}

bool Type::isSTLContainer() const {
  return STDConfig::isSTLContainer(kind);
}

bool Type::isSTLIterator() const {
  return STDConfig::isSTLIterator(kind);
}

bool Type::isPolymorphic() const {
  return polymorphic;
}

void Type::addBase(Type* base) {
  bases.push_back(base);
  if(not base->hasDerived(this))
    base->addDerived(this);
}

void Type::addVirtualBase(Type* base) {
  vbases.push_back(base);
  if(not base->hasDerived(this))
    base->addDerived(this);
}

void Type::addDerived(Type* d) {
  derived.push_back(d);
  if(not d->hasBase(this))
    d->addBase(this);
}

void Type::setCanonical(llvm::StructType* sty) {
  canonical = sty;
}

const clang::CXXRecordDecl* Type::getCXXDecl() const {
  return dyn_cast_or_null<clang::CXXRecordDecl>(decl);
}

bool Type::hasCXXDecl() const {
  return getCXXDecl();
}

bool Type::hasBase(const Type* base) const {
  return bases.contains(base);
}

bool Type::hasDerived(const Type* d) const {
  return derived.contains(d);
}

bool Type::hasBases() const {
  return bases.size();
}

bool Type::hasVirtualBases() const {
  return vbases.size();
}

bool Type::hasDerived() const {
  return derived.size();
}

bool Type::hasCanonical() const {
  return canonical;
}

const std::string& Type::getFullName() const {
  return fullName;
}

const std::string& Type::getQualifiedName() const {
  return qualifiedName;
}

STDKind Type::getSTDKind() const {
  return kind;
}

const Vector<const Type*>& Type::getBases() const {
  return bases;
}

const Vector<const Type*>& Type::getVirtualBases() const {
  return vbases;
}

const Vector<const Type*>& Type::getDerived() const {
  return derived;
}

Module& Type::getModule() {
  return static_cast<Module&>(TypeClang::getModule());
}

const Module& Type::getModule() const {
  return static_cast<const Module&>(TypeClang::getModule());
}

llvm::StructType* Type::getCanonical() const {
  return canonical;
}

} // namespace moya
