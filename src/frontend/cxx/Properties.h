#ifndef MOYA_FRONTEND_CXX_PROGRAM_ELEMENTS_H
#define MOYA_FRONTEND_CXX_PROGRAM_ELEMENTS_H

#include "frontend/c-family/PropertiesBase.h"

class Properties : public PropertiesBase {
public:
  Properties(clang::CompilerInstance&);
  virtual ~Properties() = default;
};

#endif // MOYA_FRONTEND_CXX_PROGRAM_ELEMENTS_H
