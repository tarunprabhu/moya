#include "Passes.h"
#include "common/Metadata.h"

#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Instructions.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>

using namespace llvm;

class InstrumentExceptionBlocksPass : public ModulePass {
public:
  static char ID;

protected:
  BasicBlock& getBlockContainingResume(BasicBlock&);
  void getOperandsToResume(Instruction*, moya::Vector<Instruction*>&);
  moya::Vector<Instruction*> getOperandsToResume(ResumeInst*);

public:
  InstrumentExceptionBlocksPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

InstrumentExceptionBlocksPass::InstrumentExceptionBlocksPass()
    : ModulePass(ID) {
  initializeInstrumentExceptionBlocksPassPass(*PassRegistry::getPassRegistry());
}

StringRef InstrumentExceptionBlocksPass::getPassName() const {
  return "Moya Instrument Exception Blocks";
}

void InstrumentExceptionBlocksPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

BasicBlock&
InstrumentExceptionBlocksPass::getBlockContainingResume(BasicBlock& bb) {
  if(BasicBlock* succ = bb.getUniqueSuccessor()) {
    Instruction* term = succ->getTerminator();
    if(isa<ResumeInst>(term))
      return *succ;
    else if(isa<UnreachableInst>(term))
      return *succ;
    return getBlockContainingResume(*succ);
  }
  return bb;
}

void InstrumentExceptionBlocksPass::getOperandsToResume(
    Instruction* inst,
    moya::Vector<Instruction*>& insts) {
  if(isa<InsertValueInst>(inst)) {
    insts.push_back(inst);
    if(auto* next = dyn_cast<Instruction>(inst->getOperand(0)))
      insts.push_back(next);
  }
}

moya::Vector<Instruction*>
InstrumentExceptionBlocksPass::getOperandsToResume(ResumeInst* resume) {
  moya::Vector<Instruction*> insts({resume});

  if(auto* inst = dyn_cast<Instruction>(resume->getOperand(0)))
    getOperandsToResume(inst, insts);

  return insts;
}

bool InstrumentExceptionBlocksPass::runOnModule(Module& module) {
  bool changed = false;

  for(Function& f : module.functions()) {
    if(not moya::Metadata::hasNoAnalyze(f)) {
      for(auto i = inst_begin(f); i != inst_end(f); i++)
        if(auto* resume = dyn_cast<ResumeInst>(&*i))
          for(Instruction* inst : getOperandsToResume(resume))
            changed |= moya::Metadata::setIgnore(inst);
      for(BasicBlock& bb : f.getBasicBlockList()) {
        if(bb.isLandingPad()) {
          for(Instruction& inst : bb.getInstList())
            changed |= moya::Metadata::setIgnore(inst);
          for(Instruction& inst : getBlockContainingResume(bb).getInstList())
            changed |= moya::Metadata::setIgnore(inst);
        }
      }
    }
  }

  return changed;
}

char InstrumentExceptionBlocksPass::ID = 0;

static const char* name = "moya-instr-exc-blks";
static const char* descr = "Instruments the exception handling blocks";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(InstrumentExceptionBlocksPass, name, descr, cfg, analysis);

Pass* createInstrumentExceptionBlocksPass() {
  return new InstrumentExceptionBlocksPass();
}
