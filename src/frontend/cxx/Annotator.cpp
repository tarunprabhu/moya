#include "Annotator.h"

#include <sstream>

using namespace clang;

namespace moya {

Annotator::Annotator() : AnnotatorClang(SourceLang::CXX), cxx11(false) {
  ;
}

void Annotator::initialize(CompilerInstance& compiler) {
  SourceManager& srcMgr = compiler.getSourceManager();
  LangOptions& langOpts = compiler.getLangOpts();

  AnnotatorClang::initialize(srcMgr, langOpts);
  if(langOpts.CPlusPlus11 or langOpts.CPlusPlus14 or langOpts.CPlusPlus17)
    cxx11 = true;
}

std::string
Annotator::generateFunctionCall(const std::string& func,
                                const moya::Vector<std::string>& args,
                                const std::string& sig,
                                NewLine newl) {
  std::stringstream ss;

  if(static_cast<int>(newl & NewLine::Prepend))
    ss << "\n";
  ss << "{";
  ss << " " << sig;
  if(cxx11)
    ss << "noexcept";
  else
    ss << "throw()";
  ss << " "
     << "asm(\"" << func << "\")";
  ss << ";";
  ss << " " << func << "(";
  if(args.size()) {
    ss << args.at(0);
    for(unsigned i = 1; i < args.size(); i++)
      ss << ", " << args.at(i);
  }
  ss << "); }";
  if(static_cast<int>(newl & NewLine::Append))
    ss << "\n";

  return ss.str();
}

} // namespace moya
