#include "GlobalVariable.h"
#include "Module.h"
#include "frontend/c-family/ClangUtils.h"

namespace moya {

GlobalVariable::GlobalVariable(ModuleBase& module,
                               const clang::VarDecl* decl,
                               llvm::GlobalVariable& llvm)
    : GlobalVariableClang(module, decl, llvm),
      fullName(ClangUtils::getFullDeclName(decl)),
      qualName(ClangUtils::getQualifiedDeclName(decl)) {
  ;
}

Module& GlobalVariable::getModule() {
  return static_cast<Module&>(GlobalVariableClang::getModule());
}

const Module& GlobalVariable::getModule() const {
  return static_cast<const Module&>(GlobalVariableClang::getModule());
}

bool GlobalVariable::hasFullName() const {
  return fullName.size();
}

const std::string& GlobalVariable::getFullName() const {
  return fullName;
}

bool GlobalVariable::hasQualifiedName() const {
  return qualName.size();
}

const std::string& GlobalVariable::getQualifiedName() const {
  return qualName;
}

} // namespace moya
