#include "Module.h"
#include "CodeGenerator.h"
#include "Function.h"
#include "GlobalVariable.h"
#include "Type.h"
#include "common/StringUtils.h"
#include "frontend/c-family/ClangUtils.h"
#include "frontend/components/DirectiveContext.h"

namespace moya {

static const moya::Set<llvm::StructType*> emptySet;

Module::Module(CodeGenerator& cg) : ModuleClang(SourceLang::CXX, cg), cg(cg) {
  ;
}

const Set<Type*>& Module::getStructs() const {
  return structs;
}

const Set<Type*>& Module::getClasses() const {
  return classes;
}

Function& Module::getDeclaration(llvm::Function& llvm) {
  return static_cast<Function&>(ModuleClang::getDeclaration(llvm));
}

Function& Module::getDeclaration(llvm::Function* llvm) {
  return getDeclaration(*llvm);
}

Function& Module::getDeclaration(const clang::FunctionDecl* decl) {
  return static_cast<Function&>(ModuleClang::getDeclaration(decl));
}

const Function& Module::getDeclaration(llvm::Function& llvm) const {
  return static_cast<const Function&>(ModuleClang::getDeclaration(llvm));
}

const Function& Module::getDeclaration(llvm::Function* llvm) const {
  return getDeclaration(*llvm);
}

const Function& Module::getDeclaration(const clang::FunctionDecl* decl) const {
  return static_cast<const Function&>(ModuleClang::getDeclaration(decl));
}

Function& Module::getFunction(llvm::Function& llvm) {
  return static_cast<Function&>(ModuleClang::getFunction(llvm));
}

Function& Module::getFunction(llvm::Function* llvm) {
  return getFunction(*llvm);
}

Function& Module::getFunction(const clang::FunctionDecl* decl) {
  return static_cast<Function&>(ModuleClang::getFunction(decl));
}

const Function& Module::getFunction(llvm::Function& llvm) const {
  return static_cast<const Function&>(ModuleClang::getFunction(llvm));
}

const Function& Module::getFunction(llvm::Function* llvm) const {
  return getFunction(*llvm);
}

const Function& Module::getFunction(const clang::FunctionDecl* decl) const {
  return static_cast<const Function&>(ModuleClang::getFunction(decl));
}

GlobalVariable& Module::getGlobal(llvm::GlobalVariable& llvm) {
  return static_cast<GlobalVariable&>(ModuleClang::getGlobal(llvm));
}

GlobalVariable& Module::getGlobal(llvm::GlobalVariable* llvm) {
  return getGlobal(*llvm);
}

GlobalVariable& Module::getGlobal(const clang::NamedDecl* decl) {
  return static_cast<GlobalVariable&>(ModuleClang::getGlobal(decl));
}

const GlobalVariable& Module::getGlobal(llvm::GlobalVariable& llvm) const {
  return static_cast<const GlobalVariable&>(ModuleClang::getGlobal(llvm));
}

const GlobalVariable& Module::getGlobal(llvm::GlobalVariable* llvm) const {
  return getGlobal(*llvm);
}

const GlobalVariable& Module::getGlobal(const clang::NamedDecl* decl) const {
  return static_cast<const GlobalVariable&>(ModuleClang::getGlobal(decl));
}

Type& Module::getType(llvm::Type* llvm) {
  return static_cast<Type&>(ModuleClang::getType(llvm));
}

Type& Module::getType(const clang::Type* type) {
  return static_cast<Type&>(ModuleClang::getType(type));
}

Type& Module::getType(const clang::RecordDecl* decl) {
  return static_cast<Type&>(ModuleClang::getType(decl));
}

const Type& Module::getType(llvm::Type* llvm) const {
  return static_cast<const Type&>(ModuleClang::getType(llvm));
}

const Type& Module::getType(const clang::Type* type) const {
  return static_cast<const Type&>(ModuleClang::getType(type));
}

const Type& Module::getType(const clang::RecordDecl* decl) const {
  return static_cast<const Type&>(ModuleClang::getType(decl));
}

void Module::addSTLContainer(const clang::Type* astType, STDKind kind) {
  stlContainers.emplace_back(astType, kind);
  stlMap[astType] = stlContainers.size() - 1;
}

Container& Module::getSTLContainer(const clang::Type* astType) {
  return stlContainers.at(stlMap.at(astType));
}

Container& Module::getSTLContainer(const clang::CXXRecordDecl* decl) {
  return getSTLContainer(decl->getTypeForDecl());
}

const Container& Module::getSTLContainer(const clang::Type* astType) const {
  return stlContainers.at(stlMap.at(astType));
}

const Container&
Module::getSTLContainer(const clang::CXXRecordDecl* decl) const {
  return getSTLContainer(decl->getTypeForDecl());
}

bool Module::hasSTLContainer(const clang::Type* astType) const {
  return stlMap.contains(astType);
}

bool Module::hasSTLContainer(const clang::CXXRecordDecl* decl) const {
  return stlMap.contains(decl->getTypeForDecl());
}

Vector<Container>& Module::getSTLContainers() {
  return stlContainers;
}

const Vector<Container>& Module::getSTLContainers() const {
  return stlContainers;
}

void Module::addEquivalent(llvm::StructType* s1, llvm::StructType* s2) {
  if(equivMap.contains(s1) and equivMap.contains(s2)) {
    moya_error_if(equivMap.at(s1) != equivMap.at(s2),
                  "Inconsistent equivalences:\n"
                      << "  s1: " << *s1 << "\n"
                      << "s2: " << *s2);
  } else if(equivMap.contains(s1)) {
    equivMap[s2] = equivMap.at(s1);
    equivs[equivMap.at(s1)].insert(s2);
  } else if(equivMap.contains(s2)) {
    equivMap[s1] = equivMap.at(s2);
    equivs[equivMap.at(s2)].insert(s1);
  } else {
    unsigned size = equivs.size();
    equivMap[s1] = size;
    equivMap[s2] = size;
    moya::Set<llvm::StructType*> s = {s1, s2};
    equivs.push_back(s);
  }
}

const moya::Set<llvm::StructType*>&
Module::getEquivalent(llvm::StructType* sty) const {
  if(equivMap.contains(sty))
    return equivs.at(equivMap.at(sty));
  return emptySet;
}

} // namespace moya
