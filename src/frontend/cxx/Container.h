#ifndef MOYA_FRONTEND_CXX_CONTAINER
#define MOYA_FRONTEND_CXX_CONTAINER

#include "common/Map.h"
#include "common/STDConfig.h"

#include <clang/AST/DeclCXX.h>
#include <clang/AST/Type.h>
#include <llvm/IR/Type.h>

namespace moya {

class Container {
protected:
  STDKind kind;
  const clang::Type* type;
  const clang::CXXRecordDecl* clss;
  llvm::StructType* containerTy;
  llvm::StructType* iteratorTy;
  llvm::StructType* nodeTy;
  llvm::StructType* baseTy;

  // The type contained in this container (sigh). For map-like containers,
  // this will be a pair of (keyTy, mappedTy). std::pair will not have a
  // valueTy at all
  llvm::Type* valueTy;
  llvm::Type* keyTy;
  llvm::Type* mappedTy;

  // Only relevant for std::pair
  llvm::Type* firstTy;
  llvm::Type* secondTy;

  // This is primarily true for std::array
  uint64_t nelems;

  // The index of the element of the node that contains the value
  // (only relevant for containers based on trees, lists or hashtables)
  uint64_t valOffset;

  // The index of the element in the container that contains a pointer to the
  // data (or the node)
  uint64_t ptrOffset;

  // Only relevant for map-like containers. The offset of the key and the
  // mapped value within the node. (The value type is a pair, of
  // { KEY_TYPE and MAPPED_TYPE })
  uint64_t keyOffset;
  uint64_t mappedOffset;

  // The named member types in the container.
  Map<std::string, const clang::Type*> members;

public:
  Container(const clang::Type*, STDKind);

  STDKind getKind() const;
  llvm::StructType* getContainerType() const;
  llvm::StructType* getIteratorType() const;
  llvm::StructType* getNodeType() const;
  llvm::StructType* getBaseType() const;
  llvm::Type* getValueType() const;
  llvm::Type* getKeyType() const;
  llvm::Type* getMappedType() const;
  llvm::Type* getFirstType() const;
  llvm::Type* getSecondType() const;
  uint64_t getElementCount() const;
  uint64_t getValueOffset() const;
  uint64_t getPointerOffset() const;
  uint64_t getKeyOffset() const;
  uint64_t getMappedOffset() const;
  const clang::Type* getClangType() const;
  const clang::CXXRecordDecl* getClangDecl() const;
  const clang::Type* getClangMember(const std::string&) const;

  void setContainerType(llvm::StructType*);
  void setIteratorType(llvm::StructType*);
  void setNodeType(llvm::StructType*);
  void setBaseType(llvm::StructType*);
  void setKeyType(llvm::Type*);
  void setMappedType(llvm::Type*);
  void setValueType(llvm::Type*);
  void setFirstType(llvm::Type*);
  void setSecondType(llvm::Type*);
  void setElementCount(uint64_t);
  void setValueOffset(uint64_t);
  void setPointerOffset(uint64_t);
  void setKeyOffset(uint64_t);
  void setMappedOffset(uint64_t);
};

} // namespace moya

#endif // MOYA_FRONTEND_CXX_CONTAINER
