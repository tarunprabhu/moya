#ifndef MOYA_FRONTEND_CXX_MODULE_H
#define MOYA_FRONTEND_CXX_MODULE_H

#include "CodeGenerator.h"
#include "Container.h"
#include "Function.h"
#include "GlobalVariable.h"
#include "Type.h"
#include "common/Set.h"
#include "frontend/c-family/ModuleClang.h"

namespace moya {

class Module : public ModuleClang {
protected:
  CodeGenerator& cg;
  Set<Type*> structs;
  Set<Type*> classes;
  Vector<Container> stlContainers;
  Map<const clang::Type*, size_t> stlMap;

  // These are the equivalent struct types. Typically they are equivalences
  // between STL types but that's not always necessary
  moya::Vector<moya::Set<llvm::StructType*>> equivs;
  moya::Map<llvm::StructType*, unsigned> equivMap;

public:
  Module(CodeGenerator&);
  Module(const Module&) = delete;
  virtual ~Module() = default;

  template <typename... ArgsT>
  Function& addDeclaration(ArgsT&&... args) {
    return ModuleClang::addDeclaration<Function>(args...);
  }

  template <typename... ArgsT>
  Function& addFunction(ArgsT&&... args) {
    return ModuleClang::addFunction<Function>(args...);
  }

  template <typename... ArgsT>
  GlobalVariable& addGlobal(ArgsT&&... args) {
    return ModuleClang::addGlobal<GlobalVariable>(args...);
  }

  template <typename... ArgsT>
  Type& addType(const clang::Type* type, ArgsT&&... args) {
    Type& t = ModuleClang::addType<Type>(type, args...);

    // The only clang::RecordDecl that will be added must be
    //
    //   - Definitions
    //   - Complete (no partial specializations)
    //   - Independent (Templated types should *NOT* be added)
    //
    if(not t.isUnion()) {
      if(type->getAsRecordDecl())
        structs.insert(&t);
      if(type->getAsCXXRecordDecl())
        classes.insert(&t);
    }

    return t;
  }

  void addSTLContainer(const clang::Type*, STDKind);

  Container& getSTLContainer(const clang::Type*);
  Container& getSTLContainer(const clang::CXXRecordDecl*);
  const Container& getSTLContainer(const clang::Type*) const;
  const Container& getSTLContainer(const clang::CXXRecordDecl*) const;
  bool hasSTLContainer(const clang::Type*) const;
  bool hasSTLContainer(const clang::CXXRecordDecl*) const;
  Vector<Container>& getSTLContainers();
  const Vector<Container>& getSTLContainers() const;

  const Set<Type*>& getStructs() const;
  const Set<Type*>& getClasses() const;

  using ModuleClang::hasDeclaration;
  Function& getDeclaration(llvm::Function&);
  Function& getDeclaration(llvm::Function*);
  Function& getDeclaration(const clang::FunctionDecl*);
  const Function& getDeclaration(llvm::Function&) const;
  const Function& getDeclaration(llvm::Function*) const;
  const Function& getDeclaration(const clang::FunctionDecl*) const;

  using ModuleClang::hasFunction;
  Function& getFunction(llvm::Function&);
  Function& getFunction(llvm::Function*);
  Function& getFunction(const clang::FunctionDecl*);
  const Function& getFunction(llvm::Function&) const;
  const Function& getFunction(llvm::Function*) const;
  const Function& getFunction(const clang::FunctionDecl*) const;

  using ModuleClang::hasGlobal;
  GlobalVariable& getGlobal(llvm::GlobalVariable&);
  GlobalVariable& getGlobal(llvm::GlobalVariable*);
  GlobalVariable& getGlobal(const clang::NamedDecl*);
  const GlobalVariable& getGlobal(llvm::GlobalVariable&) const;
  const GlobalVariable& getGlobal(llvm::GlobalVariable*) const;
  const GlobalVariable& getGlobal(const clang::NamedDecl*) const;

  using ModuleClang::hasType;
  Type& getType(llvm::Type*);
  Type& getType(const clang::Type*);
  Type& getType(const clang::RecordDecl*);
  const Type& getType(llvm::Type*) const;
  const Type& getType(const clang::Type*) const;
  const Type& getType(const clang::RecordDecl*) const;

  void addEquivalent(llvm::StructType*, llvm::StructType*);
  const moya::Set<llvm::StructType*>& getEquivalent(llvm::StructType*) const;
};

} // namespace moya

#endif // MOYA_FRONTEND_CXX_MODULE_H
