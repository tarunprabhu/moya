#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/ClassHeirarchies.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/Vector.h"
#include "frontend/all-langs/InstrumentInstructionsCommonPass.h"

#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>

using namespace llvm;

class InstrumentInstructionsPass : public ModulePass {
public:
  static char ID;

protected:
  moya::ClassHeirarchies heirarchies;

protected:
  bool instrument(CastInst*);
  bool instrument(GetElementPtrInst*);
  bool instrument(LoadInst*);
  bool instrument(ResumeInst*);
  bool instrument(AllocaInst*, const moya::Module&);

  int64_t getAdjustedGEPIndex(GetElementPtrInst*, const APInt&);

public:
  InstrumentInstructionsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

InstrumentInstructionsPass::InstrumentInstructionsPass() : ModulePass(ID) {
  initializeInstrumentInstructionsPassPass(*PassRegistry::getPassRegistry());
}

StringRef InstrumentInstructionsPass::getPassName() const {
  return "Moya Instrument Instructions (C++)";
}

void InstrumentInstructionsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.addRequired<InstrumentInstructionsCommonPass>();
  AU.setPreservesCFG();
}

int64_t InstrumentInstructionsPass::getAdjustedGEPIndex(GetElementPtrInst* gep,
                                                        const APInt& offset) {
  Type* srcTy
      = dyn_cast<PointerType>(gep->getPointerOperandType())->getElementType();

  // If the source is an array of scalars, it will be truncated
  if(moya::LLVMUtils::isScalarTy(srcTy)) {
    if(auto* cst = dyn_cast<CastInst>(gep->getPointerOperand()))
      // If a constant offset is taken into a struct that has been cast to
      // void* it is either a field access or in the case of virtually
      // inherited classes, an offset to get to the vtable of a base class
      if(moya::Metadata::hasCastStructToBytes(cst))
        return offset.getLimitedValue();
    return 0;
  } else if(auto* aty = dyn_cast<ArrayType>(srcTy)) {
    // If the source type is an array of primitives or structs,
    // it will always be truncated.
    Type* elemTy = aty->getElementType();
    if(moya::LLVMUtils::isScalarTy(elemTy)
       or moya::LLVMUtils::isStructTy(elemTy))
      return 0;
  }

  return offset.getLimitedValue();
}

bool InstrumentInstructionsPass::instrument(GetElementPtrInst* gep) {
  bool changed = false;
  const DataLayout& dl = moya::LLVMUtils::getModule(gep)->getDataLayout();

  // Some of the GEP instructions have constant offsets that *must* be exact.
  // But figuring out which ones can be tricky.
  // FIXME: Try to use the AST nodes to figure out exactly which ones need
  // exact index computations
  if(gep->getNumIndices() <= 2) {
    APInt offset(64, 0, true);
    if(gep->accumulateConstantOffset(dl, offset)) {
      bool preComputedIndex = false;
      if(moya::Metadata::hasGEPFieldLookup(gep)) {
        preComputedIndex = true;
      } else if(auto* inst = dyn_cast<Instruction>(gep->getPointerOperand())) {
        Type* ptrTy = gep->getPointerOperandType();
        if(moya::Metadata::hasLoadVtable(inst)
           or moya::Metadata::hasCastToVtable(inst)
           or moya::LLVMUtils::isVtableTy(ptrTy)) {
          // Get the pointer to the slot in the vtable
          // If the lookup is into a vtable, then it *may* be safe. The only
          // time it is not safe is if we are looking up the vtable of a base
          // class with an offset that is only intended to be used in derived
          // classes. This happens because the base class constructors populate
          // the vtable. In the actual program execution, the entries of the
          // base class constrcutor are overwritten by those of the derived
          // class, but in the static analysis, they will co-exist.
          changed |= moya::Metadata::setGEPOverflowIgnore(gep);
          changed |= moya::Metadata::setGEPVtableLookup(gep);
          preComputedIndex = true;
        } else if(moya::Metadata::hasCastStructToBytes(inst)) {
          preComputedIndex = true;
        }
      }

      if(preComputedIndex)
        // We may need to reset the offset for the analysis in certain cases.
        // If we are looking into an array, it might be truncated in which
        // case the offset should always be zero. In that case, we reset the
        // offset variable.
        changed |= moya::Metadata::setGEPIndex(
            gep, getAdjustedGEPIndex(gep, offset));
    }
  }

  return changed;
}

bool InstrumentInstructionsPass::instrument(CastInst* cst) {
  bool changed = false;
  Module& module = *moya::LLVMUtils::getModule(cst);

  Type* srcType = moya::LLVMUtils::getAnalysisType(cst->getOperand(0));
  Type* dstType = cst->getDestTy();
  StructType* srcStruct = nullptr;
  StructType* dstStruct = nullptr;

  if(auto* pty = dyn_cast<PointerType>(srcType))
    srcStruct = dyn_cast<StructType>(pty->getElementType());

  if(auto* pty = dyn_cast<PointerType>(dstType))
    dstStruct = dyn_cast<StructType>(pty->getElementType());

  if(srcStruct and dstStruct) {
    if(heirarchies.isDerivedFrom(srcStruct, dstStruct))
      changed |= moya::Metadata::setCastToBase(cst);
    else if(heirarchies.isDerivedFrom(dstStruct, srcStruct))
      changed |= moya::Metadata::setCastToDerived(cst);
  }

  if(srcStruct) {
    if(moya::Metadata::hasUnion(srcStruct, module))
      changed |= moya::Metadata::setCastFromUnion(cst);

    Type* pi8 = Type::getInt8PtrTy(cst->getContext());
    if(dstType == pi8) {
      // If the instruction already has an analysis type, then it probably
      // came from a function argument and will likely be just as useful
      if(not moya::Metadata::hasAnalysisType(cst)) {
        changed
            |= moya::Metadata::setAnalysisType(cst, srcStruct->getPointerTo());
        changed |= moya::Metadata::setCastStructToBytes(cst);
      }
    }

    if(moya::Metadata::hasPolymorphic(srcStruct, module)
       and (moya::LLVMUtils::isPtrToVtableTy(dstType)
            or moya::LLVMUtils::isPtrToPtrTy(dstType)))
      changed |= moya::Metadata::setCastToVtable(cst);
  }

  return changed;
}

// Identify the vtable lookups and filter the types that are allowed.
// The trouble is that when a derived class object is created, it contains
// pointers to the vtables of *ALL* the base classes. Only the vtables of
// the current class and all derived classes should be loaded
bool InstrumentInstructionsPass::instrument(LoadInst* load) {
  bool changed = false;

  bool loadVtable = false;
  if(auto* inst = dyn_cast<Instruction>(load->getPointerOperand()))
    if(moya::Metadata::hasCastToVtable(inst))
      loadVtable = true;
  if(not loadVtable)
    if(moya::LLVMUtils::isVtableTy(load->getType()))
      loadVtable = true;

  if(loadVtable)
    changed |= moya::Metadata::setLoadVtable(load);

  return changed;
}

bool InstrumentInstructionsPass::instrument(ResumeInst* resume) {
  bool changed = false;

  changed |= moya::Metadata::setIgnore(resume);
  if(auto* insert1 = dyn_cast<InsertValueInst>(resume->getOperand(0))) {
    changed |= moya::Metadata::setIgnore(insert1);
    if(auto* insert2 = dyn_cast<InsertValueInst>(insert1->getOperand(0)))
      changed |= moya::Metadata::setIgnore(insert2);
  }

  return changed;
}

bool InstrumentInstructionsPass::instrument(AllocaInst* alloca,
                                            const moya::Module& feModule) {
  bool changed = false;

  Type* allocated = alloca->getAllocatedType();
  if(feModule.hasType(allocated)) {
    const moya::Type& feType = feModule.getType(allocated);
    const clang::Type* astType = feType.getType();
    moya::STDKind kind = feType.getSTDKind();
    if(moya::STDConfig::isSTLContainer(kind)
       or (kind == moya::STDKind::STD_String)
       or (kind == moya::STDKind::STD_Initializer)) {
      changed |= moya::Metadata::resetAnalysisType(alloca);
      changed |= moya::Metadata::setAnalysisType(
          alloca,
          feModule.getSTLContainer(astType).getContainerType()->getPointerTo());
    } else if(moya::STDConfig::isSTLIterator(kind)) {
      changed |= moya::Metadata::resetAnalysisType(alloca);
      changed |= moya::Metadata::setAnalysisType(
          alloca,
          feModule.getSTLContainer(astType).getIteratorType()->getPointerTo());
    }
  }

  return changed;
}

bool InstrumentInstructionsPass::runOnModule(Module& module) {
  bool changed = false;

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();
  heirarchies.initialize(module);

  for(Function& f : module.functions()) {
    if(not moya::Metadata::hasNoAnalyze(f)) {
      for(auto i = inst_begin(f); i != inst_end(f); i++) {
        Instruction* inst = &*i;
        if(auto* gep = dyn_cast<GetElementPtrInst>(inst))
          changed |= instrument(gep);
        else if(auto* cst = dyn_cast<CastInst>(inst))
          changed |= instrument(cst);
        else if(auto* load = dyn_cast<LoadInst>(inst))
          changed |= instrument(load);
        else if(auto* resume = dyn_cast<ResumeInst>(inst))
          changed |= instrument(resume);
        else if(auto* alloca = dyn_cast<AllocaInst>(inst))
          changed |= instrument(alloca, feModule);
      }
    }
  }

  return changed;
}

char InstrumentInstructionsPass::ID = 0;

static const char* name = "moya-instr-insts-cxx";
static const char* descr = "Adds instrumentation to instructions (C++)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(InstrumentInstructionsPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_DEPENDENCY(InstrumentInstructionsCommonPass)
INITIALIZE_PASS_END(InstrumentInstructionsPass, name, descr, cfg, analysis)

Pass* createInstrumentInstructionsPass() {
  return new InstrumentInstructionsPass();
}
