#include "Function.h"
#include "Argument.h"
#include "Module.h"
#include "common/StringUtils.h"
#include "common/Stringify.h"
#include "frontend/c-family/ClangUtils.h"

using llvm::cast;
using llvm::dyn_cast;
using llvm::isa;

namespace moya {

static const std::string prefixBIS
    = STDConfig::getSTDName(STDKind::STD_Stream_basic_istream)
      + "::operator>>(";
static const std::string prefixBOS
    = STDConfig::getSTDName(STDKind::STD_Stream_basic_ostream)
      + "::operator<<(";
static const std::string prefixOpIn = "std::operator>>(std::basic_istream,";
static const std::string prefixOpOut = "std::operator<<(std::basic_ostream,";

Function::Function(ModuleBase& module,
                   const clang::FunctionDecl* decl,
                   llvm::Function& f)
    : FunctionClang(module, decl, f),
      qualifiedName(ClangUtils::getQualifiedDeclName(decl)),
      fullName(ClangUtils::getFullDeclName(decl)),
      // kind(STDConfig::getSTDKind_Alt(fullName)),
      kind(ClangUtils::getSTDKind(decl)),
      method(dyn_cast<clang::CXXMethodDecl>(decl)), clss(nullptr),
      iteratorRet(nullptr), iteratorArg(nullptr) {
  if(f.hasStructRetAttr())
    for(llvm::Argument& arg : f.args())
      if(arg.hasStructRetAttr())
        addArgument<Argument>(".sret");
  if(method) {
    clss = ClangUtils::getDefinition(method->getParent());
    if(not method->isStatic())
      addArgument<Argument>("this");
  }

  for(const clang::ParmVarDecl* param : decl->parameters()) {
    // If the current type is a struct/class and is passed by value,
    // it will be packed and passed as a sequence of 64-bit integers.
    // I won't try to figure out which of the arguments correspond to the
    // passed struct, so for the moment will just bail out
    if(param->getType()->getAsRecordDecl()) {
      structParam = true;
      break;
    }
    if(isDefined() and param->getName().size())
      addArgument<Argument>(param);
  }

  if(not((kind == STDKind::STD_None) or (kind == STDKind::STD_Impl)))
    setModel(modelCreate());

  initParams();
}

bool Function::hasQualifiedName() const {
  return qualifiedName.length();
}

const std::string& Function::getQualifiedName() const {
  return qualifiedName;
}

bool Function::hasFullName() const {
  return fullName.length();
}

const std::string& Function::getFullName() const {
  return fullName;
}

STDKind Function::getSTDKind() const {
  return kind;
}

void Function::initParams() {
  const clang::Type* ret = decl->getReturnType().getTypePtr();
  if((ret->isStructureType() or ret->isClassType())
     and llvm->hasStructRetAttr())
    if(const clang::CXXRecordDecl* d = getIteratorDecl(ret))
      iteratorRet = ClangUtils::getDefinition(d);

  for(const clang::ParmVarDecl* param : decl->parameters()) {
    const clang::Type* type
        = param->getOriginalType().getNonReferenceType().getTypePtr();
    // Some STL functions take iterators to two different containers - the
    // first will be the to the container itself and all subsequent ones will
    // be to some other container. So we don't mind if iteratorArg is
    // overwritten
    if(const clang::CXXRecordDecl* d = getIteratorDecl(type))
      iteratorArg = ClangUtils::getDefinition(d);
  }
}

const clang::CXXRecordDecl* Function::getIteratorDecl(const clang::Type* ty) {
  if(const auto* t = dyn_cast<clang::TypedefType>(ty)) {
    // For STL containers, we need to know the name of the typedef'd decl
    // to identify those functions which take one of the template parameter
    // types as an argument. So we can't just get rid of the typedef here
    if(isSTLContainer()) {
      const std::string& tname = t->getDecl()->getName();
      if((tname == "iterator") or (tname == "const_iterator")
         or (tname == "reverse_iterator")
         or (tname == "const_reverse_iterator"))
        return t->getUnqualifiedDesugaredType()->getAsCXXRecordDecl();
    }
    return getIteratorDecl(t->desugar().getTypePtr());
  } else if(auto* e = dyn_cast<clang::ElaboratedType>(ty)) {
    return getIteratorDecl(e->getNamedType().getTypePtr());
  } else if(auto* tts = dyn_cast<clang::SubstTemplateTypeParmType>(ty)) {
    return getIteratorDecl(tts->getReplacementType().getTypePtr());
  } else if(auto* tp = dyn_cast<clang::TemplateTypeParmType>(ty)) {
    return getIteratorDecl(tp->getDecl()->getTypeForDecl());
  }

  const clang::Type* type = ty->getUnqualifiedDesugaredType();
  if(isSTLContainer() or isSTDString())
    if(STDConfig::isSTLIterator(ClangUtils::getSTDKind(type)))
      return type->getAsCXXRecordDecl();

  return nullptr;
}

std::string Function::getModelType(const clang::Type* ty) const {
  if(auto* t = dyn_cast<clang::TypedefType>(ty)) {
    // For STL containers, we need to know the name of the typedef'd decl
    // to identify those functions which take one of the template parameter
    // types as an argument. So we can't just get rid of the typedef here
    if(isSTLContainer() or isSTLIterator() or isSTDString()) {
      const std::string& tname = t->getDecl()->getName();
      if((tname == "value_type") or (tname == "key_type")
         or (tname == "mapped_type"))
        return "*";
      else if(tname == "allocator_type")
        return "allocator";
      else if((tname == "iterator") or (tname == "const_iterator")
              or (tname == "reverse_iterator")
              or (tname == "const_reverse_iterator"))
        return "iterator";
    }
    return getModelType(t->desugar().getTypePtr());
  } else if(auto* p = dyn_cast<clang::PackExpansionType>(ty)) {
    return "...";
  } else if(auto* e = dyn_cast<clang::ElaboratedType>(ty)) {
    return getModelType(e->getNamedType().getTypePtr());
  } else if(auto* tts = dyn_cast<clang::SubstTemplateTypeParmType>(ty)) {
    return getModelType(tts->getReplacementType().getTypePtr());
  } else if(auto* tp = dyn_cast<clang::TemplateTypeParmType>(ty)) {
    return "T";
  }

  const clang::Type* type = ty->getUnqualifiedDesugaredType();
  STDKind kind = ClangUtils::getSTDKind(type);
  if(STDConfig::isSTLContainer(kind))
    return STDConfig::getSTDName(kind);
  else if(STDConfig::isSTLIterator(kind))
    return "iterator";
  else if(STDConfig::isSTDAllocator(kind))
    return "allocator";
  else if(STDConfig::isSTDInitializer(kind))
    return STDConfig::getSTDName(kind);
  else if(STDConfig::isSTDString(kind))
    return STDConfig::getSTDName(kind);

  std::stringstream ss;
  unsigned depth = ClangUtils::getTypeDepth(type);
  const clang::Type* inner = ClangUtils::getInnermost(type);
  ss << ClangUtils::getQualifiedTypeName(inner, decl->getASTContext());
  for(unsigned i = 0; i < depth; i++)
    ss << "*";
  return ss.str();
}

std::string Function::modelCreateParam(const clang::ParmVarDecl* param) {
  std::stringstream ss;
  const clang::QualType& q = param->getOriginalType().getNonReferenceType();

  return getModelType(q.getTypePtr());
}

std::string Function::modelCreateParams() {
  std::stringstream ss;
  const clang::FunctionDecl* f = decl;
  const clang::FunctionDecl* func = decl;
  if(clang::FunctionDecl* t = decl->getInstantiatedFromMemberFunction())
    func = t;

  if(STDConfig::isSTLContainer(kind) or STDConfig::isSTDString(kind))
    if(const clang::FunctionTemplateSpecializationInfo* spec
       = decl->getTemplateSpecializationInfo())
      f = spec->getTemplate()->getCanonicalDecl()->getTemplatedDecl();
  ss << "(";
  bool comma = false;
  for(const clang::ParmVarDecl* param : f->parameters()) {
    if(comma)
      ss << ", ";
    ss << modelCreateParam(param);
    comma = true;
  }

  ss << ")";

  return ss.str();
}

std::string Function::modelCreate() {
  std::stringstream ss;
  STDKind kind = getSTDKind();

  if(isMethod()) {
    if(isPublic() or isProtected()) {
      if(STDConfig::isSTDPublic(kind))
        ss << ClangUtils::getQualifiedDeclName(clss) << "::" << getName()
           << modelCreateParams();
      else {
        ss << STDConfig::getSTDName(getSTDKind()) << "::" << getName()
           << modelCreateParams();
      }
    }
  } else {
    if(STDConfig::isSTDPublic(getSTDKind())) {
      if(STDConfig::isSTDModel(getQualifiedName()))
        ss << STDConfig::getSTDModel(getQualifiedName());
      else
        ss << getQualifiedName() << modelCreateParams();
    }
  }

  std::string model = ss.str();
  if(StringUtils::startswith(model, prefixBIS))
    model = "std::basic_istream::operator>>(*)";
  else if(StringUtils::startswith(model, prefixBOS))
    model = "std::basic_ostream::operator<<(*)";
  else if(StringUtils::startswith(model, prefixOpIn))
    model = "std::operator>>(std::basic_istream, *)";
  else if(StringUtils::startswith(model, prefixOpOut))
    model = "std::operator<<(std::basic_ostream, *)";

  return model;
}

const clang::CXXMethodDecl* Function::getMethodDecl() const {
  return method;
}

const clang::CXXRecordDecl* Function::getClassDecl() const {
  return clss;
}

const clang::CXXRecordDecl* Function::getReturnedIteratorDecl() const {
  return iteratorRet;
}

const clang::CXXRecordDecl* Function::getArgumentIteratorDecl() const {
  return iteratorArg;
}

bool Function::isMethod() const {
  return clss;
}

bool Function::isConstructor() const {
  if(method)
    return isa<clang::CXXConstructorDecl>(method);
  return false;
}

bool Function::isDestructor() const {
  if(method)
    return isa<clang::CXXDestructorDecl>(method);
  return false;
}

bool Function::isVirtual() const {
  if(method)
    return method->isVirtual();
  return false;
}

bool Function::isPublic() const {
  return ((decl->getAccess() == clang::AccessSpecifier::AS_none)
          or (decl->getAccess() == clang::AccessSpecifier::AS_public));
}

bool Function::isPrivate() const {
  return decl->getAccess() == clang::AccessSpecifier::AS_private;
}

bool Function::isProtected() const {
  return decl->getAccess() == clang::AccessSpecifier::AS_protected;
}

bool Function::isSTLContainer() const {
  return STDConfig::isSTLContainer(getSTDKind());
}

bool Function::isSTLIterator() const {
  return STDConfig::isSTLIterator(getSTDKind());
}

bool Function::isSTDString() const {
  return STDConfig::isSTDString(getSTDKind());
}

Module& Function::getModule() {
  return static_cast<Module&>(FunctionClang::getModule());
}

Argument& Function::getArg(unsigned i) {
  return static_cast<Argument&>(FunctionClang::getArg(i));
}

Argument& Function::getArg(const std::string& name) {
  return static_cast<Argument&>(FunctionClang::getArg(name));
}

const Module& Function::getModule() const {
  return static_cast<const Module&>(FunctionClang::getModule());
}

const Argument& Function::getArg(unsigned i) const {
  return static_cast<const Argument&>(FunctionClang::getArg(i));
}

const Argument& Function::getArg(const std::string& name) const {
  return static_cast<const Argument&>(FunctionClang::getArg(name));
}

} // namespace moya
