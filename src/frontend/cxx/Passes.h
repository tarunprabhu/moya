#ifndef MOYA_FRONTEND_CXX_PASSES_H
#define MOYA_FRONTEND_CXX_PASSES_H

#include "Module.h"
#include "frontend/all-langs/Passes.h"

#include <llvm/Pass.h>

llvm::Pass* createAssociatePass();
llvm::Pass* createGenerateRedirectPass();
llvm::Pass* createInstrumentExceptionBlocksPass();
llvm::Pass* createInstrumentFunctionsPass();
llvm::Pass* createInstrumentGlobalsPass();
llvm::Pass* createInstrumentInstructionsPass();
llvm::Pass* createInstrumentSTLPass();
llvm::Pass* createInstrumentTypesPass();
llvm::Pass* createModuleWrapperPass(moya::Module&);
llvm::Pass* createRegionsPass();

namespace llvm {
void initializeAssociatePassPass(llvm::PassRegistry&);
void initializeGenerateRedirectPassPass(llvm::PassRegistry&);
void initializeInstrumentExceptionBlocksPassPass(llvm::PassRegistry&);
void initializeInstrumentFunctionsPassPass(llvm::PassRegistry&);
void initializeInstrumentGlobalsPassPass(llvm::PassRegistry&);
void initializeInstrumentInstructionsPassPass(llvm::PassRegistry&);
void initializeInstrumentSTLPassPass(llvm::PassRegistry&);
void initializeInstrumentTypesPassPass(llvm::PassRegistry&);
void initializeModuleWrapperPassPass(llvm::PassRegistry&);
void initializeRegionsPassPass(llvm::PassRegistry&);
} // namespace llvm

#endif // MOYA_FRONTEND_CXX_PASSES_H
