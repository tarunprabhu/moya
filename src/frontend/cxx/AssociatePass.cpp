#include "Container.h"
#include "Function.h"
#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Map.h"
#include "common/Metadata.h"
#include "common/Set.h"
#include "common/Vector.h"
#include "frontend/c-family/ClangUtils.h"

#include <llvm/IR/DataLayout.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using llvm::dyn_cast;

class AssociatePass : public llvm::ModulePass {
protected:
  llvm::DataLayout layout;
  moya::Set<const clang::Type*> types;

protected:
  llvm::Type* canonicalize(llvm::Type*, moya::Module&, llvm::Module&);
  llvm::PointerType*
  canonicalize(llvm::PointerType*, moya::Module&, llvm::Module&);
  llvm::ArrayType* canonicalize(llvm::ArrayType*, moya::Module&, llvm::Module&);
  llvm::StructType*
  canonicalize(llvm::StructType*, moya::Module&, llvm::Module&);

  uint64_t getOffset(llvm::StructType*, unsigned);
  uint64_t getListValueOffset(llvm::StructType*);
  uint64_t getTreeValueOffset(llvm::StructType*);
  uint64_t getTreePointerOffset(llvm::StructType*);
  uint64_t getHashtableValueOffset(llvm::StructType*);
  uint64_t getHashtablePointerOffset(llvm::StructType*);

  llvm::Type* makeLLVMType(const clang::Type*, llvm::Module&, moya::Module&);
  llvm::Type* getLLVMType(const clang::Type*, llvm::Module&, moya::Module&);
  llvm::StructType* getLLVMPairType(llvm::Module&, llvm::Type*, llvm::Type*);
  llvm::StructType* getLLVMStringType(llvm::Module&);
  llvm::StructType* getLLVMStringIteratorType(llvm::Module&);
  llvm::StructType* getLLVMInitializerListType(llvm::Module&, llvm::Type*);
  llvm::StructType* getLLVMInitializerListIteratorType(llvm::Module&,
                                                       llvm::Type*);
  llvm::StructType* getLLVMArrayType(llvm::Module&, llvm::Type*);
  llvm::StructType* getLLVMVectorType(llvm::Module&, llvm::Type*);
  llvm::StructType* getLLVMVectorIteratorType(llvm::Module&, llvm::Type*);
  llvm::StructType* getLLVMBitVectorType(llvm::Module&);
  llvm::StructType* getLLVMBitVectorIteratorType(llvm::Module&);
  llvm::StructType* getLLVMDequeType(llvm::Module&, llvm::Type*);
  llvm::StructType* getLLVMDequeIteratorType(llvm::Module&, llvm::Type*);
  llvm::StructType* getLLVMListType(llvm::Module&, llvm::Type*);
  llvm::StructType* getLLVMListBaseType(llvm::Module&);
  llvm::StructType* getLLVMListNodeType(llvm::Module&, llvm::Type*);
  llvm::StructType* getLLVMListIteratorType(llvm::Module&, llvm::Type*);
  llvm::StructType*
  getLLVMTreeType(llvm::Module&, llvm::Type*, const std::string&);
  llvm::StructType* getLLVMTreeBaseType(llvm::Module&);
  llvm::StructType* getLLVMTreeNodeType(llvm::Module&, llvm::Type*);
  llvm::StructType* getLLVMTreeIteratorType(llvm::Module&, llvm::Type*);
  llvm::StructType*
  getLLVMHashtableType(llvm::Module&, llvm::Type*, const std::string&);
  llvm::StructType* getLLVMHashtableBaseType(llvm::Module&);
  llvm::StructType* getLLVMHashtableNodeType(llvm::Module&, llvm::Type*);
  llvm::StructType* getLLVMHashtableIteratorType(llvm::Module&, llvm::Type*);
  llvm::StructType* getLLVMRandomAccessIteratorType(llvm::Module&, llvm::Type*);

  std::string getTypeName(llvm::Type*);
  std::string
  getNodeTypeName(const std::string&, llvm::Type*, llvm::Type* = nullptr);

  void orderSTLType(const clang::Type*,
                    moya::Set<const clang::CXXRecordDecl*>&,
                    moya::Vector<const clang::CXXRecordDecl*>&,
                    moya::Module&);

  void associateSTDString(moya::Container&, llvm::Module&, moya::Module&);
  void associateSTDInitializer(moya::Container&, llvm::Module&, moya::Module&);
  void associateSTLPair(moya::Container&, llvm::Module&, moya::Module&);
  void associateSTLArray(moya::Container&, llvm::Module&, moya::Module&);
  void associateSTLVector(moya::Container&, llvm::Module&, moya::Module&);
  void associateSTLBitVector(moya::Container&, llvm::Module&, moya::Module&);
  void associateSTLDeque(moya::Container&, llvm::Module&, moya::Module&);
  void associateSTLList(moya::Container&, llvm::Module&, moya::Module&);
  void associateSTLForwardList(moya::Container&, llvm::Module&, moya::Module&);
  void associateSTLTree1(moya::Container&, llvm::Module&, moya::Module&);
  void associateSTLTree2(moya::Container&, llvm::Module&, moya::Module&);
  void associateSTLHash1(moya::Container&, llvm::Module&, moya::Module&);
  void associateSTLHash2(moya::Container&, llvm::Module&, moya::Module&);

  void
  associateSTLIteratorNormal(moya::Container&, llvm::Module&, moya::Module&);
  void
  associateSTLIteratorDeque(moya::Container&, llvm::Module&, moya::Module&);
  void associateSTLIteratorForwardList(moya::Container&,
                                       llvm::Module&,
                                       moya::Module&);
  void associateSTLIteratorList(moya::Container&, llvm::Module&, moya::Module&);
  void associateSTLIteratorTree(moya::Container&, llvm::Module&, moya::Module&);
  void
  associateSTLIteratorHashtable(moya::Container&, llvm::Module&, moya::Module&);
  void
  associateSTLIteratorBucket(moya::Container&, llvm::Module&, moya::Module&);
  void
  associateSTLIteratorBitvector(moya::Container&, llvm::Module&, moya::Module&);
  void
  associateSTLReverseIterator(moya::Container&, llvm::Module&, moya::Module&);

  void associateDirectives(llvm::Module&, moya::Module&);
  void associateFunctions(llvm::Module&, moya::Module&);
  void associateGlobals(llvm::Module&, moya::Module&);
  void associateSTLTypes(llvm::Module&, moya::Module&);
  void associateStructTypes(llvm::Module&, moya::Module&);
  void associateTypes(llvm::Module&, moya::Module&);

public:
  AssociatePass();

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);

public:
  static char ID;
};

AssociatePass::AssociatePass() : ModulePass(ID), layout("") {
  llvm::initializeAssociatePassPass(*llvm::PassRegistry::getPassRegistry());
}

llvm::StringRef AssociatePass::getPassName() const {
  return "Moya Associate (C++)";
}

void AssociatePass::getAnalysisUsage(llvm::AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.setPreservesAll();
}

unsigned long AssociatePass::getOffset(llvm::StructType* sty, unsigned index) {
  return layout.getStructLayout(sty)->getElementOffset(index);
}

llvm::PointerType* AssociatePass::canonicalize(llvm::PointerType* pty,
                                               moya::Module& feModule,
                                               llvm::Module& module) {
  return canonicalize(pty->getElementType(), feModule, module)->getPointerTo();
}

llvm::ArrayType* AssociatePass::canonicalize(llvm::ArrayType* aty,
                                             moya::Module& feModule,
                                             llvm::Module& module) {
  return llvm::ArrayType::get(
      canonicalize(aty->getElementType(), feModule, module),
      aty->getNumElements());
}

llvm::StructType* AssociatePass::canonicalize(llvm::StructType* sty,
                                              moya::Module& feModule,
                                              llvm::Module& module) {
  moya::Type& feType = feModule.getType(sty);
  const clang::Type* clangType = feType.getType();
  llvm::StructType* canonical = sty;

  if(feModule.hasSTLContainer(clangType)) {
    const moya::Container& cont = feModule.getSTLContainer(clangType);
    moya::STDKind kind = cont.getKind();
    if(moya::STDConfig::isSTLContainer(kind))
      canonical = cont.getContainerType();
    else if(moya::STDConfig::isSTLIterator(kind))
      canonical = cont.getIteratorType();
    else if(moya::STDConfig::isSTDString(kind))
      canonical = cont.getContainerType();
    else
      moya_error("Unexpected STL container in canonicalizing struct:\n"
                 << "  clss: " << sty->getName() << "\n"
                 << "  kind: " << moya::str(kind));
  } else {
    // Don't create a new struct unless at least one of the elements of the
    // struct is different from the original
    bool modified = false;
    moya::Vector<llvm::Type*> types;
    for(llvm::Type* field : sty->elements()) {
      llvm::Type* newType = canonicalize(field, feModule, module);
      if(newType != field)
        modified = true;
      types.push_back(newType);
    }
    if(modified) {
      std::string s;
      llvm::raw_string_ostream ss(s);
      ss << "__moya_canonical_" << sty->getName();
      canonical = moya::LLVMUtils::createStruct(
          module, types, ss.str(), sty->isPacked());
    }
  }

  return canonical;
}

llvm::Type* AssociatePass::canonicalize(llvm::Type* type,
                                        moya::Module& feModule,
                                        llvm::Module& module) {
  if(moya::LLVMUtils::isScalarTy(type))
    return type;
  else if(auto* pty = dyn_cast<llvm::PointerType>(type))
    return canonicalize(pty, feModule, module);
  else if(auto* aty = dyn_cast<llvm::ArrayType>(type))
    return canonicalize(aty, feModule, module);
  else if(auto* sty = dyn_cast<llvm::StructType>(type))
    return canonicalize(sty, feModule, module);
  else if(auto* fty = dyn_cast<llvm::FunctionType>(type))
    return canonicalize(fty, feModule, module);
  else
    moya_error("Unexpected type to canonicalize");

  return nullptr;
}

void AssociatePass::associateFunctions(llvm::Module& module,
                                       moya::Module& feModule) {
  for(llvm::Function& f : module.functions()) {
    if(const clang::Decl* decl = feModule.getDecl(f.getName())) {
      if(const auto* fdecl = llvm::dyn_cast<clang::FunctionDecl>(decl)) {
        if(const auto* defn = moya::ClangUtils::getDefinition(fdecl))
          feModule.addFunction(defn, f);
        else
          feModule.addDeclaration(fdecl, f);
      }
    }
  }
}

void AssociatePass::associateGlobals(llvm::Module& module,
                                     moya::Module& feModule) {
  for(llvm::GlobalVariable& g : module.globals())
    if(const clang::Decl* decl = feModule.getDecl(g.getName()))
      if(const auto* vdecl = dyn_cast<clang::VarDecl>(decl))
        feModule.addGlobal(vdecl, g);
}

llvm::Type* AssociatePass::makeLLVMType(const clang::Type* astType,
                                        llvm::Module& module,
                                        moya::Module& feModule) {
  return feModule.convertToLLVM(astType);
}

llvm::Type* AssociatePass::getLLVMType(const clang::Type* astType,
                                       llvm::Module& module,
                                       moya::Module& feModule) {
  if(feModule.hasSTLContainer(astType)) {
    moya::Container& cont = feModule.getSTLContainer(astType);
    moya::STDKind kind = cont.getKind();
    if(moya::STDConfig::isSTLContainer(kind)
       or (kind == moya::STDKind::STD_String)
       or (kind == moya::STDKind::STD_Initializer))
      return cont.getContainerType();
    else
      return cont.getIteratorType();
  }
  return makeLLVMType(astType, module, feModule);
}

std::string AssociatePass::getTypeName(llvm::Type* type) {
  std::string s;
  llvm::raw_string_ostream ss(s);

  if(type->isIntegerTy(1))
    ss << "i1";
  if(type->isIntegerTy(8))
    ss << "i8";
  else if(type->isIntegerTy(16))
    ss << "i16";
  else if(type->isIntegerTy(32))
    ss << "i32";
  else if(type->isIntegerTy(64))
    ss << "i64";
  else if(type->isFloatTy())
    ss << "f32";
  else if(type->isDoubleTy())
    ss << "f64";
  else if(type->isX86_FP80Ty())
    ss << "f80";
  else if(auto* pty = dyn_cast<llvm::PointerType>(type))
    ss << "p" << moya::LLVMUtils::getTypeDepth(pty)
       << getTypeName(pty->getElementType());
  else if(auto* aty = dyn_cast<llvm::ArrayType>(type))
    ss << "a" << moya::LLVMUtils::getNumFlattenedElements(aty)
       << getTypeName(moya::LLVMUtils::getFlattenedElementType(aty));
  else if(auto* sty = dyn_cast<llvm::StructType>(type))
    if(sty->hasName())
      ss << sty->getName();
    else
      ss << *sty;
  else
    ss << *type;

  return ss.str();
}

std::string AssociatePass::getNodeTypeName(const std::string& kind,
                                           llvm::Type* type,
                                           llvm::Type* type2) {
  std::stringstream ss;

  ss << "moya.stl." << kind << "." << getTypeName(type);
  if(type2)
    ss << "." << getTypeName(type2);

  return ss.str();
}

// std::string = type { i8*, i64, i64, [8 x i8]}
//
llvm::StructType* AssociatePass::getLLVMStringType(llvm::Module& module) {
  std::string name = "moya.std.string";
  llvm::StructType* contTy = module.getTypeByName(name);
  if(not contTy) {
    llvm::LLVMContext& context = module.getContext();

    llvm::Type* i8 = llvm::Type::getInt8Ty(context);
    llvm::Type* i64 = llvm::Type::getInt64Ty(context);
    llvm::Type* pi8 = i8->getPointerTo();
    llvm::Type* aty = llvm::ArrayType::get(i8, 8);

    contTy = moya::LLVMUtils::createStruct(
        module, {pi8, i64, i64, aty}, name, false);

    moya::Metadata::setArtificial(contTy, module);
  }

  return contTy;
}

// std::string::iterator = type { i8* }
//
llvm::StructType*
AssociatePass::getLLVMStringIteratorType(llvm::Module& module) {
  std::string name = "moya.std.string.iterator";
  llvm::StructType* iterTy = module.getTypeByName(name);
  if(not iterTy) {
    llvm::LLVMContext& context = module.getContext();

    llvm::Type* i8 = llvm::Type::getInt8Ty(context);

    iterTy = moya::LLVMUtils::createStruct(
        module, {i8->getPointerTo()}, name, false);

    moya::Metadata::setArtificial(iterTy, module);
  }

  return iterTy;
}

// std::initializer_list = type { VALUE_TYPE*, i64 }
//
llvm::StructType*
AssociatePass::getLLVMInitializerListType(llvm::Module& module,
                                          llvm::Type* valueTy) {
  std::string name = getNodeTypeName("initializer_list", valueTy);
  llvm::StructType* contTy = module.getTypeByName(name);
  if(not contTy) {
    llvm::LLVMContext& context = module.getContext();

    llvm::Type* i64 = llvm::Type::getInt64Ty(context);

    contTy = moya::LLVMUtils::createStruct(
        module, {valueTy->getPointerTo(), i64}, name, false);

    moya::Metadata::setArtificial(contTy, module);
  }

  return contTy;
}

// std::initializer_list::iterator = type { VALUE_TYPE* }
//
llvm::StructType*
AssociatePass::getLLVMInitializerListIteratorType(llvm::Module& module,
                                                  llvm::Type* valueTy) {
  std::string name = getNodeTypeName("initializer_list.iterator", valueTy);
  llvm::StructType* iterTy = module.getTypeByName(name);
  if(not iterTy) {
    iterTy = moya::LLVMUtils::createStruct(
        module, {valueTy->getPointerTo()}, name, false);

    moya::Metadata::setArtificial(iterTy, module);
  }

  return iterTy;
}

// std::array = type { [n x VALUE_TYPE] }

// std::vector = type { VALUE_TYPE*, VALUE_TYPE*, VALUE_TYPE* }
//
llvm::StructType* AssociatePass::getLLVMVectorType(llvm::Module& module,
                                                   llvm::Type* valueTy) {
  std::string name = getNodeTypeName("vector", valueTy);
  llvm::StructType* contTy = module.getTypeByName(name);
  if(not contTy) {
    llvm::Type* pValueTy = valueTy->getPointerTo();
    contTy = moya::LLVMUtils::createStruct(
        module, {pValueTy, pValueTy, pValueTy}, name);

    moya::Metadata::setArtificial(contTy, module);
  }

  return contTy;
}

// std::vector::iterator = type { VALUE_TYPE* }
//
llvm::StructType*
AssociatePass::getLLVMVectorIteratorType(llvm::Module& module,
                                         llvm::Type* valueTy) {
  std::string name = getNodeTypeName("vector.iterator", valueTy);
  llvm::StructType* iterTy = module.getTypeByName(name);
  if(not iterTy) {
    iterTy = moya::LLVMUtils::createStruct(
        module, {valueTy->getPointerTo()}, name, false);

    moya::Metadata::setArtificial(iterTy, module);
  }

  return iterTy;
}

// std::vector<bool> = type { <{ i64*, i32 }>, i64* }
//
llvm::StructType* AssociatePass::getLLVMBitVectorType(llvm::Module& module) {
  std::string name = "moya.stl.bvector";
  llvm::StructType* contTy = module.getTypeByName(name);

  if(not contTy) {
    llvm::LLVMContext& context = module.getContext();

    llvm::Type* i64 = llvm::Type::getInt64Ty(context);
    llvm::Type* iterTy = getLLVMBitVectorIteratorType(module);

    contTy = moya::LLVMUtils::createStruct(
        module, {iterTy, iterTy, i64->getPointerTo()}, name, false);

    moya::Metadata::setArtificial(contTy, module);
  }

  return contTy;
}

// std::vector<bool>::iterator = <{ i64*, i32 }>
llvm::StructType*
AssociatePass::getLLVMBitVectorIteratorType(llvm::Module& module) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* i64 = llvm::Type::getInt64Ty(context);
  llvm::Type* i32 = llvm::Type::getInt32Ty(context);

  llvm::StructType* iterTy = llvm::StructType::get(
      context,
      moya::LLVMUtils::makeArrayRef<llvm::Type*>({i64->getPointerTo(), i32}),
      false);

  return iterTy;
}

// std::deque = type { VALUE_TYPE**,
//                     i64,
//                     std::deque::iterator,
//                     std::deque::iterator }
//
llvm::StructType* AssociatePass::getLLVMDequeType(llvm::Module& module,
                                                  llvm::Type* valueTy) {
  std::string name = getNodeTypeName("deque", valueTy);
  llvm::StructType* contTy = module.getTypeByName(name);
  if(not contTy) {
    llvm::LLVMContext& context = module.getContext();

    llvm::Type* i64 = llvm::Type::getInt64Ty(context);
    llvm::Type* ppValueTy = valueTy->getPointerTo()->getPointerTo();
    llvm::Type* iterTy = getLLVMDequeIteratorType(module, valueTy);

    contTy = moya::LLVMUtils::createStruct(
        module, {ppValueTy, i64, iterTy, iterTy}, name, false);

    moya::Metadata::setArtificial(contTy, module);
  }

  return contTy;
}

// std::deque::iterator = type { VALUE_TYPE*,
//                               VALUE_TYPE*,
//                               VALUE_TYPE*,
//                               VALUE_TYPE** }
//
llvm::StructType* AssociatePass::getLLVMDequeIteratorType(llvm::Module& module,
                                                          llvm::Type* valueTy) {
  std::string name = getNodeTypeName("deque.iterator", valueTy);
  llvm::StructType* iterTy = module.getTypeByName(name);
  if(not iterTy) {
    llvm::Type* pValueTy = valueTy->getPointerTo();
    iterTy = moya::LLVMUtils::createStruct(
        module, {pValueTy, pValueTy, pValueTy, pValueTy}, name, false);

    moya::Metadata::setArtificial(iterTy, module);
  }

  return iterTy;
}

// std::tree = type { i8, std::tree::node.base, i64 }
//
llvm::StructType* AssociatePass::getLLVMTreeType(llvm::Module& module,
                                                 llvm::Type* valueTy,
                                                 const std::string& kind) {
  std::string name = getNodeTypeName(kind, valueTy);
  llvm::StructType* contTy = module.getTypeByName(name);
  if(not contTy) {
    llvm::LLVMContext& context = module.getContext();

    llvm::Type* i8 = llvm::Type::getInt8Ty(context);
    llvm::Type* i64 = llvm::Type::getInt64Ty(context);
    llvm::Type* baseTy = getLLVMTreeBaseType(module);

    contTy
        = moya::LLVMUtils::createStruct(module, {i8, baseTy, i64}, name, false);

    moya::Metadata::setArtificial(contTy, module);
  }

  return contTy;
}

// std::tree::node.base = type { i32,
//                               std::tree::node.base*,
//                               std::tree::node.base*,
//                               std::tree::node.base* }
//
llvm::StructType* AssociatePass::getLLVMTreeBaseType(llvm::Module& module) {
  std::string name = "moya.stl.set.node.base";
  llvm::StructType* baseTy = module.getTypeByName(name);
  if(not baseTy) {
    llvm::LLVMContext& context = module.getContext();
    llvm::Type* i32 = llvm::Type::getInt32Ty(context);

    baseTy = llvm::StructType::create(context, name);
    llvm::Type* pBaseTy = baseTy->getPointerTo();
    baseTy->setBody({i32, pBaseTy, pBaseTy, pBaseTy}, false);

    moya::Metadata::setArtificial(baseTy, module);
  }

  return baseTy;
}

// std::tree::node = type { std::tree::node.base, VALUE_TYPE }
//
llvm::StructType* AssociatePass::getLLVMTreeNodeType(llvm::Module& module,
                                                     llvm::Type* valueTy) {
  std::string name = getNodeTypeName("tree.node", valueTy);
  llvm::StructType* nodeTy = module.getTypeByName(name);
  if(not nodeTy) {
    llvm::StructType* baseTy = getLLVMTreeBaseType(module);
    nodeTy
        = moya::LLVMUtils::createStruct(module, {baseTy, valueTy}, name, false);
    moya::Metadata::setBases(nodeTy, {baseTy}, module);

    moya::Metadata::setArtificial(nodeTy, module);
  }

  return nodeTy;
}

// std::tree::iterator = type { std::tree::node.base* }
//
llvm::StructType* AssociatePass::getLLVMTreeIteratorType(llvm::Module& module,
                                                         llvm::Type* valueTy) {
  std::string name = getNodeTypeName("tree.iterator", valueTy);
  llvm::StructType* iterTy = module.getTypeByName(name);
  if(not iterTy) {
    iterTy = moya::LLVMUtils::createStruct(
        module, {getLLVMTreeBaseType(module)->getPointerTo()}, name, false);

    moya::Metadata::setArtificial(iterTy, module);
  }

  return iterTy;
}

uint64_t AssociatePass::getTreePointerOffset(llvm::StructType* contTy) {
  auto* baseTy = llvm::cast<llvm::StructType>(contTy->getElementType(1));
  return getOffset(contTy, 1) + getOffset(baseTy, 1);
}

uint64_t AssociatePass::getTreeValueOffset(llvm::StructType* nodeTy) {
  return getOffset(nodeTy, 1);
}

// std::list = type { std::list::node }
//
llvm::StructType* AssociatePass::getLLVMListType(llvm::Module& module,
                                                 llvm::Type* valueTy) {
  std::string name = getNodeTypeName("list", valueTy);
  llvm::StructType* contTy = module.getTypeByName(name);
  if(not contTy) {
    llvm::Type* baseTy = getLLVMListBaseType(module);
    contTy = moya::LLVMUtils::createStruct(module, {baseTy}, name, false);

    moya::Metadata::setArtificial(contTy, module);
  }

  return contTy;
}

// std::list::node.base = type { std::list::node.base*, std::list::node.base*}
//
llvm::StructType* AssociatePass::getLLVMListBaseType(llvm::Module& module) {
  std::string baseName = "moya.stl.list.node.base";
  llvm::StructType* baseTy = module.getTypeByName(baseName);
  if(not baseTy) {
    llvm::LLVMContext& context = module.getContext();

    baseTy = llvm::StructType::create(context, baseName);
    llvm::Type* pBaseTy = baseTy->getPointerTo();
    baseTy->setBody({pBaseTy, pBaseTy}, false);

    moya::Metadata::setArtificial(baseTy, module);
  }

  return baseTy;
}

// std::list::node = type { std::list::node.base, VALUE_TYPE }
//
llvm::StructType* AssociatePass::getLLVMListNodeType(llvm::Module& module,
                                                     llvm::Type* valueTy) {
  std::string name = getNodeTypeName("list.node", valueTy);
  llvm::StructType* nodeTy = module.getTypeByName(name);
  if(not nodeTy) {
    llvm::StructType* baseTy = getLLVMListBaseType(module);
    nodeTy
        = moya::LLVMUtils::createStruct(module, {baseTy, valueTy}, name, false);
    moya::Metadata::setBases(nodeTy, {baseTy}, module);

    moya::Metadata::setArtificial(nodeTy, module);
  }

  return nodeTy;
}

// std::list::iterator = type { std::_List_node_base* }
//
llvm::StructType* AssociatePass::getLLVMListIteratorType(llvm::Module& module,
                                                         llvm::Type* valueTy) {
  std::string name = getNodeTypeName("list.iterator", valueTy);
  llvm::StructType* iterTy = module.getTypeByName(name);
  if(not iterTy) {
    llvm::Type* pBaseTy = getLLVMListBaseType(module)->getPointerTo();
    iterTy = moya::LLVMUtils::createStruct(module, {pBaseTy}, name, false);

    moya::Metadata::setArtificial(iterTy, module);
  }

  return iterTy;
}

uint64_t AssociatePass::getListValueOffset(llvm::StructType* nodeTy) {
  return getOffset(nodeTy, 1);
}

// std::hashtable = type { std::hashtable::node.base**,
//                         i64,
//                         std::hashtable::node.base,
//                         i64,
//                         std::hashtable::rehash,
//                         std::hashtable::node.base* }
// std::hashtable::rehash = { float, i64 }
//
llvm::StructType* AssociatePass::getLLVMHashtableType(llvm::Module& module,
                                                      llvm::Type* valueTy,
                                                      const std::string& kind) {
  std::string name = getNodeTypeName(kind, valueTy);
  llvm::StructType* contTy = module.getTypeByName(name);
  if(not contTy) {
    llvm::LLVMContext& context = module.getContext();

    llvm::Type* i64 = llvm::Type::getInt64Ty(context);
    llvm::Type* f32 = llvm::Type::getFloatTy(context);
    llvm::StructType* rehashTy
        = moya::LLVMUtils::createStruct(module, {f32, i64}, name, false);
    llvm::StructType* baseTy = getLLVMHashtableBaseType(module);
    llvm::Type* pBaseTy = baseTy->getPointerTo();
    llvm::Type* ppBaseTy = pBaseTy->getPointerTo();

    contTy = moya::LLVMUtils::createStruct(
        module, {ppBaseTy, i64, baseTy, i64, rehashTy, pBaseTy}, name, false);

    moya::Metadata::setArtificial(contTy, module);
  }

  return contTy;
}

// std::hashtable::node.base = type { std::hashtable::node.base* }
//
llvm::StructType*
AssociatePass::getLLVMHashtableBaseType(llvm::Module& module) {
  std::string name = "moya.stl.hashtable.node.base";
  llvm::StructType* baseTy = module.getTypeByName(name);
  if(not baseTy) {
    llvm::LLVMContext& context = module.getContext();

    baseTy = llvm::StructType::create(context, name);
    baseTy->setBody({baseTy->getPointerTo()}, false);

    moya::Metadata::setArtificial(baseTy, module);
  }

  return baseTy;
}

// std::hashtable::node = type { std::hashtable::node.base, VALUE_TYPE }
//
llvm::StructType* AssociatePass::getLLVMHashtableNodeType(llvm::Module& module,
                                                          llvm::Type* valueTy) {
  std::string name = getNodeTypeName("hashtable.node", valueTy);
  llvm::StructType* nodeTy = module.getTypeByName(name);
  if(not nodeTy) {
    llvm::StructType* baseTy = getLLVMHashtableBaseType(module);
    nodeTy = moya::LLVMUtils::createStruct(
        module, {baseTy, valueTy}, name, valueTy);

    moya::Metadata::setBases(nodeTy, {baseTy}, module);
    moya::Metadata::setArtificial(nodeTy, module);
  }

  return nodeTy;
}

// std::hashtable::iterator = { std::hashtable::node.base* }
llvm::StructType*
AssociatePass::getLLVMHashtableIteratorType(llvm::Module& module,
                                            llvm::Type* valueTy) {
  std::string name = getNodeTypeName("hashtable.iterator", valueTy);
  llvm::StructType* iterTy = module.getTypeByName(name);
  if(not iterTy) {
    iterTy = moya::LLVMUtils::createStruct(
        module,
        {getLLVMHashtableBaseType(module)->getPointerTo()},
        name,
        false);

    moya::Metadata::setArtificial(iterTy, module);
  }

  return iterTy;
}

uint64_t AssociatePass::getHashtablePointerOffset(llvm::StructType* contTy) {
  return getOffset(contTy, 5);
}

uint64_t AssociatePass::getHashtableValueOffset(llvm::StructType* nodeTy) {
  return getOffset(nodeTy, 1);
}

// std::pair = { FIRST_TYPE, SECOND_TYPE }
//
llvm::StructType* AssociatePass::getLLVMPairType(llvm::Module& module,
                                                 llvm::Type* firstTy,
                                                 llvm::Type* secondTy) {
  std::string name = getNodeTypeName("pair", firstTy, secondTy);
  llvm::StructType* contTy = module.getTypeByName(name);
  if(not contTy) {
    contTy = moya::LLVMUtils::createStruct(
        module, {firstTy, secondTy}, name, false);

    moya::Metadata::setArtificial(contTy, module);
  }

  return contTy;
}

// __gnu_cxx::__normal_iterator = { VALUE_TYPE* }
//
llvm::StructType*
AssociatePass::getLLVMRandomAccessIteratorType(llvm::Module& module,
                                               llvm::Type* valueTy) {
  std::string name = getNodeTypeName("iterator", valueTy);
  llvm::StructType* contTy = module.getTypeByName(name);
  if(not contTy) {
    contTy = moya::LLVMUtils::createStruct(
        module, {valueTy->getPointerTo()}, name, false);

    moya::Metadata::setArtificial(contTy, module);
  }

  return contTy;
}

void AssociatePass::associateSTDString(moya::Container& cont,
                                       llvm::Module& module,
                                       moya::Module& feModule) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* i8 = llvm::Type::getInt8Ty(context);
  llvm::StructType* contTy = getLLVMStringType(module);
  llvm::StructType* iterTy = getLLVMStringIteratorType(module);

  cont.setContainerType(contTy);
  cont.setIteratorType(iterTy);
  cont.setValueType(i8);
}

void AssociatePass::associateSTDInitializer(moya::Container& cont,
                                            llvm::Module& module,
                                            moya::Module& feModule) {
  llvm::Type* valueTy
      = getLLVMType(cont.getClangMember("value_type"), module, feModule);

  llvm::StructType* contTy = getLLVMInitializerListType(module, valueTy);
  llvm::StructType* iterTy
      = getLLVMInitializerListIteratorType(module, valueTy);

  cont.setContainerType(contTy);
  cont.setValueType(valueTy);
  cont.setIteratorType(iterTy);
}

void AssociatePass::associateSTLPair(moya::Container& cont,
                                     llvm::Module& module,
                                     moya::Module& feModule) {
  llvm::Type* firstTy
      = getLLVMType(cont.getClangMember("first_type"), module, feModule);
  llvm::Type* secondTy
      = getLLVMType(cont.getClangMember("second_type"), module, feModule);

  llvm::StructType* contTy = getLLVMPairType(module, firstTy, secondTy);

  moya::Metadata::setArtificial(contTy, module);

  cont.setContainerType(contTy);
  cont.setFirstType(firstTy);
  cont.setSecondType(secondTy);
}

void AssociatePass::associateSTLArray(moya::Container& cont,
                                      llvm::Module& module,
                                      moya::Module& feModule) {
  llvm::Type* valueTy
      = getLLVMType(cont.getClangMember("value_type"), module, feModule);

  auto* sty
      = feModule.convertToLLVMAs<llvm::StructType>(cont.getClangType(), true);
  llvm::StructType* contTy = module.getTypeByName(sty->getName());

  cont.setContainerType(contTy);
  cont.setValueType(valueTy);
}

void AssociatePass::associateSTLVector(moya::Container& cont,
                                       llvm::Module& module,
                                       moya::Module& feModule) {
  llvm::Type* valueTy
      = getLLVMType(cont.getClangMember("value_type"), module, feModule);

  llvm::StructType* contTy = getLLVMVectorType(module, valueTy);
  llvm::StructType* iterTy = getLLVMVectorIteratorType(module, valueTy);

  cont.setContainerType(contTy);
  cont.setIteratorType(iterTy);
  cont.setValueType(valueTy);
}

void AssociatePass::associateSTLBitVector(moya::Container& cont,
                                          llvm::Module& module,
                                          moya::Module& feModule) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* i64 = llvm::Type::getInt64Ty(context);
  llvm::StructType* contTy = getLLVMBitVectorType(module);
  llvm::StructType* iterTy = getLLVMBitVectorIteratorType(module);

  cont.setContainerType(contTy);
  cont.setIteratorType(iterTy);
  cont.setValueType(i64);
}

void AssociatePass::associateSTLDeque(moya::Container& cont,
                                      llvm::Module& module,
                                      moya::Module& feModule) {
  llvm::Type* valueTy
      = getLLVMType(cont.getClangMember("value_type"), module, feModule);
  llvm::StructType* contTy = getLLVMDequeType(module, valueTy);
  llvm::StructType* iterTy = getLLVMDequeIteratorType(module, valueTy);

  cont.setContainerType(contTy);
  cont.setIteratorType(iterTy);
  cont.setValueType(valueTy);
  cont.setPointerOffset(getOffset(contTy, 2));
}

void AssociatePass::associateSTLForwardList(moya::Container& cont,
                                            llvm::Module& module,
                                            moya::Module& feModule) {
  moya_error("Unimplemented: STL Forward List");
}

void AssociatePass::associateSTLList(moya::Container& cont,
                                     llvm::Module& module,
                                     moya::Module& feModule) {
  llvm::Type* valueTy
      = getLLVMType(cont.getClangMember("value_type"), module, feModule);

  llvm::StructType* contTy = getLLVMListType(module, valueTy);
  llvm::StructType* baseTy = getLLVMListBaseType(module);
  llvm::StructType* nodeTy = getLLVMListNodeType(module, valueTy);
  llvm::StructType* iterTy = getLLVMListIteratorType(module, valueTy);

  cont.setContainerType(contTy);
  cont.setNodeType(nodeTy);
  cont.setBaseType(baseTy);
  cont.setValueType(valueTy);
  cont.setIteratorType(iterTy);
  cont.setValueOffset(getListValueOffset(nodeTy));
}

void AssociatePass::associateSTLTree1(moya::Container& cont,
                                      llvm::Module& module,
                                      moya::Module& feModule) {
  llvm::Type* valueTy
      = getLLVMType(cont.getClangMember("value_type"), module, feModule);

  llvm::StructType* baseTy = getLLVMTreeBaseType(module);
  llvm::StructType* nodeTy = getLLVMTreeNodeType(module, valueTy);
  llvm::StructType* iterTy = getLLVMTreeIteratorType(module, valueTy);
  llvm::StructType* contTy = getLLVMTreeType(module, valueTy, "set");

  cont.setContainerType(contTy);
  cont.setNodeType(nodeTy);
  cont.setBaseType(baseTy);
  cont.setIteratorType(iterTy);
  cont.setValueType(valueTy);

  cont.setValueOffset(getTreeValueOffset(nodeTy));
  cont.setPointerOffset(getTreePointerOffset(contTy));
}

void AssociatePass::associateSTLTree2(moya::Container& cont,
                                      llvm::Module& module,
                                      moya::Module& feModule) {
  llvm::Type* keyTy
      = getLLVMType(cont.getClangMember("key_type"), module, feModule);
  llvm::Type* mappedTy
      = getLLVMType(cont.getClangMember("mapped_type"), module, feModule);

  llvm::StructType* valueTy = getLLVMPairType(module, keyTy, mappedTy);
  llvm::StructType* baseTy = getLLVMTreeBaseType(module);
  llvm::StructType* nodeTy = getLLVMTreeNodeType(module, valueTy);
  llvm::StructType* iterTy = getLLVMTreeIteratorType(module, valueTy);
  llvm::StructType* contTy = getLLVMTreeType(module, valueTy, "map");

  cont.setContainerType(contTy);
  cont.setNodeType(nodeTy);
  cont.setBaseType(baseTy);
  cont.setIteratorType(iterTy);
  cont.setKeyType(keyTy);
  cont.setMappedType(mappedTy);
  cont.setValueType(valueTy);

  uint64_t valueOffset = getTreeValueOffset(nodeTy);
  cont.setValueOffset(valueOffset);
  cont.setKeyOffset(valueOffset + getOffset(valueTy, 0));
  cont.setMappedOffset(valueOffset + getOffset(valueTy, 1));
  cont.setPointerOffset(getTreePointerOffset(contTy));
}

void AssociatePass::associateSTLHash1(moya::Container& cont,
                                      llvm::Module& module,
                                      moya::Module& feModule) {
  llvm::Type* valueTy
      = getLLVMType(cont.getClangMember("value_type"), module, feModule);

  llvm::StructType* baseTy = getLLVMHashtableBaseType(module);
  llvm::StructType* nodeTy = getLLVMHashtableNodeType(module, valueTy);
  llvm::StructType* iterTy = getLLVMHashtableIteratorType(module, valueTy);
  llvm::StructType* contTy = getLLVMHashtableType(module, valueTy, "hashset");

  cont.setContainerType(contTy);
  cont.setNodeType(nodeTy);
  cont.setBaseType(baseTy);
  cont.setIteratorType(iterTy);
  cont.setValueType(valueTy);

  cont.setValueOffset(getHashtableValueOffset(nodeTy));
  cont.setPointerOffset(getHashtablePointerOffset(contTy));
}

void AssociatePass::associateSTLHash2(moya::Container& cont,
                                      llvm::Module& module,
                                      moya::Module& feModule) {
  llvm::Type* keyTy
      = getLLVMType(cont.getClangMember("key_type"), module, feModule);
  llvm::Type* mappedTy
      = getLLVMType(cont.getClangMember("mapped_type"), module, feModule);

  llvm::StructType* valueTy = getLLVMPairType(module, keyTy, mappedTy);
  llvm::StructType* baseTy = getLLVMHashtableBaseType(module);
  llvm::StructType* nodeTy = getLLVMHashtableNodeType(module, valueTy);
  llvm::StructType* iterTy = getLLVMHashtableIteratorType(module, valueTy);
  llvm::StructType* contTy = getLLVMHashtableType(module, valueTy, "hashmap");

  cont.setContainerType(contTy);
  cont.setNodeType(nodeTy);
  cont.setBaseType(baseTy);
  cont.setIteratorType(iterTy);
  cont.setKeyType(keyTy);
  cont.setMappedType(mappedTy);
  cont.setValueType(valueTy);

  uint64_t valueOffset = getHashtableValueOffset(nodeTy);
  cont.setValueOffset(valueOffset);
  cont.setKeyOffset(valueOffset + getOffset(valueTy, 0));
  cont.setMappedOffset(valueOffset + getOffset(valueTy, 1));
  cont.setPointerOffset(getHashtablePointerOffset(contTy));
}

void AssociatePass::associateSTLIteratorNormal(moya::Container& cont,
                                               llvm::Module& module,
                                               moya::Module& feModule) {
  llvm::Type* valueTy
      = getLLVMType(cont.getClangMember("value_type"), module, feModule);

  llvm::StructType* iterTy = getLLVMRandomAccessIteratorType(module, valueTy);

  cont.setIteratorType(iterTy);
  cont.setValueType(valueTy);
}

void AssociatePass::associateSTLIteratorDeque(moya::Container& cont,
                                              llvm::Module& module,
                                              moya::Module& feModule) {
  llvm::Type* valueTy
      = getLLVMType(cont.getClangMember("value_type"), module, feModule);

  llvm::StructType* iterTy = getLLVMDequeIteratorType(module, valueTy);

  cont.setIteratorType(iterTy);
  cont.setValueType(valueTy);
}

void AssociatePass::associateSTLIteratorForwardList(moya::Container& cont,
                                                    llvm::Module& module,
                                                    moya::Module& feModule) {
  moya_error("UNIMPLEMENTED");
}

void AssociatePass::associateSTLIteratorList(moya::Container& cont,
                                             llvm::Module& module,
                                             moya::Module& feModule) {
  llvm::Type* valueTy
      = getLLVMType(cont.getClangMember("value_type"), module, feModule);

  llvm::StructType* iterTy = getLLVMListIteratorType(module, valueTy);
  llvm::StructType* baseTy = getLLVMListBaseType(module);
  llvm::StructType* nodeTy = getLLVMListNodeType(module, valueTy);

  cont.setIteratorType(iterTy);
  cont.setBaseType(baseTy);
  cont.setNodeType(nodeTy);
  cont.setValueType(valueTy);
  cont.setValueOffset(getListValueOffset(nodeTy));
}

void AssociatePass::associateSTLIteratorTree(moya::Container& cont,
                                             llvm::Module& module,
                                             moya::Module& feModule) {
  llvm::Type* valueTy
      = getLLVMType(cont.getClangMember("value_type"), module, feModule);

  llvm::StructType* iterTy = getLLVMTreeIteratorType(module, valueTy);
  llvm::StructType* baseTy = getLLVMTreeBaseType(module);
  llvm::StructType* nodeTy = getLLVMTreeNodeType(module, valueTy);

  cont.setIteratorType(iterTy);
  cont.setBaseType(baseTy);
  cont.setNodeType(nodeTy);
  cont.setValueType(valueTy);
  cont.setValueOffset(getTreeValueOffset(nodeTy));
}

void AssociatePass::associateSTLIteratorHashtable(moya::Container& cont,
                                                  llvm::Module& module,
                                                  moya::Module& feModule) {
  llvm::Type* valueTy
      = getLLVMType(cont.getClangMember("value_type"), module, feModule);

  llvm::StructType* iterTy = getLLVMHashtableIteratorType(module, valueTy);
  llvm::StructType* baseTy = getLLVMHashtableBaseType(module);
  llvm::StructType* nodeTy = getLLVMHashtableNodeType(module, valueTy);

  cont.setIteratorType(iterTy);
  cont.setBaseType(baseTy);
  cont.setNodeType(nodeTy);
  cont.setValueType(valueTy);
  cont.setValueOffset(getHashtableValueOffset(nodeTy));
}

void AssociatePass::associateSTLIteratorBucket(moya::Container& cont,
                                               llvm::Module& module,
                                               moya::Module& feModule) {
  llvm::Type* valueTy
      = getLLVMType(cont.getClangMember("value_type"), module, feModule);

  cont.setValueType(valueTy);
}

void AssociatePass::associateSTLIteratorBitvector(moya::Container& cont,
                                                  llvm::Module& module,
                                                  moya::Module& feModule) {
  llvm::LLVMContext& context = module.getContext();

  llvm::Type* i64 = llvm::Type::getInt64Ty(context);

  cont.setIteratorType(getLLVMBitVectorIteratorType(module));
  cont.setValueType(i64);
}

void AssociatePass::associateSTLReverseIterator(moya::Container& cont,
                                                llvm::Module& module,
                                                moya::Module& feModule) {
  moya::Container& iter
      = feModule.getSTLContainer(cont.getClangMember("iterator_type"));

  cont.setIteratorType(iter.getIteratorType());
  cont.setValueType(iter.getValueType());
  cont.setValueOffset(iter.getValueOffset());
}

void AssociatePass::orderSTLType(
    const clang::Type* type,
    moya::Set<const clang::CXXRecordDecl*>& seen,
    moya::Vector<const clang::CXXRecordDecl*>& order,
    moya::Module& feModule) {
  if(feModule.hasSTLContainer(type)
     and not seen.contains(feModule.getSTLContainer(type).getClangDecl())) {
    moya::Container& cont = feModule.getSTLContainer(type);
    const clang::CXXRecordDecl* clss = cont.getClangDecl();
    switch(cont.getKind()) {
    case moya::STDKind::STD_Initializer:
      orderSTLType(cont.getClangMember("value_type"), seen, order, feModule);
      break;
    case moya::STDKind::STD_String:
    case moya::STDKind::STD_STL_bitvector:
      // The dependent types are fixed and not other containers
      break;
    case moya::STDKind::STD_STL_pair:
      orderSTLType(cont.getClangMember("first_type"), seen, order, feModule);
      orderSTLType(cont.getClangMember("second_type"), seen, order, feModule);
      break;
    case moya::STDKind::STD_STL_vector:
    case moya::STDKind::STD_STL_deque:
    case moya::STDKind::STD_STL_forward_list:
    case moya::STDKind::STD_STL_list:
    case moya::STDKind::STD_STL_set:
    case moya::STDKind::STD_STL_multiset:
    case moya::STDKind::STD_STL_unordered_set:
    case moya::STDKind::STD_STL_unordered_multiset:
      orderSTLType(cont.getClangMember("value_type"), seen, order, feModule);
      break;
    case moya::STDKind::STD_STL_map:
    case moya::STDKind::STD_STL_multimap:
    case moya::STDKind::STD_STL_unordered_map:
    case moya::STDKind::STD_STL_unordered_multimap:
      orderSTLType(cont.getClangMember("key_type"), seen, order, feModule);
      orderSTLType(cont.getClangMember("mapped_type"), seen, order, feModule);
      break;
    case moya::STDKind::STD_STL_iterator_bitvector:
    case moya::STDKind::STD_STL_const_iterator_bitvector:
      // The value type is known and fixed and not a container type
      break;
    case moya::STDKind::STD_STL_iterator_normal:
    case moya::STDKind::STD_STL_iterator_deque:
    case moya::STDKind::STD_STL_iterator_list:
    case moya::STDKind::STD_STL_iterator_forward_list:
    case moya::STDKind::STD_STL_iterator_tree:
    case moya::STDKind::STD_STL_iterator_hashtable:
    case moya::STDKind::STD_STL_iterator_bucket:
    case moya::STDKind::STD_STL_const_iterator_list:
    case moya::STDKind::STD_STL_const_iterator_tree:
    case moya::STDKind::STD_STL_const_iterator_hashtable:
    case moya::STDKind::STD_STL_const_iterator_forward_list:
      orderSTLType(cont.getClangMember("value_type"), seen, order, feModule);
      break;
    case moya::STDKind::STD_STL_iterator_reverse:
      orderSTLType(cont.getClangMember("iterator_type"), seen, order, feModule);
      break;
    default:
      break;
    }
    order.push_back(clss);
    seen.insert(clss);
  }
}

void AssociatePass::associateSTLTypes(llvm::Module& module,
                                      moya::Module& feModule) {
  for(const clang::Type* astType : types) {
    if(const clang::Type* defn = moya::ClangUtils::getDefinition(astType)) {
      moya::STDKind kind = moya::ClangUtils::getSTDKind(defn);
      if(moya::STDConfig::isSTLContainer(kind) or (kind == moya::STD_String)
         or moya::STDConfig::isSTLIterator(kind)
         or (kind == moya::STD_Initializer))
        feModule.addSTLContainer(defn, kind);
    }
  }

  moya::Set<const clang::CXXRecordDecl*> seen;
  moya::Vector<const clang::CXXRecordDecl*> order;
  for(moya::Container& cont : feModule.getSTLContainers())
    orderSTLType(cont.getClangType(), seen, order, feModule);

  for(const clang::CXXRecordDecl* clss : order) {
    moya::Container& cont = feModule.getSTLContainer(clss);
    switch(cont.getKind()) {
    case moya::STDKind::STD_Initializer:
      associateSTDInitializer(cont, module, feModule);
      break;
    case moya::STDKind::STD_String:
      associateSTDString(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_pair:
      associateSTLPair(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_array:
      associateSTLArray(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_vector:
      associateSTLVector(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_bitvector:
      associateSTLBitVector(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_deque:
      associateSTLDeque(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_forward_list:
      associateSTLForwardList(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_list:
      associateSTLList(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_set:
    case moya::STDKind::STD_STL_multiset:
      associateSTLTree1(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_map:
    case moya::STDKind::STD_STL_multimap:
      associateSTLTree2(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_unordered_set:
    case moya::STDKind::STD_STL_unordered_multiset:
      associateSTLHash1(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_unordered_map:
    case moya::STDKind::STD_STL_unordered_multimap:
      associateSTLHash2(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_iterator_normal:
      associateSTLIteratorNormal(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_iterator_tree:
    case moya::STDKind::STD_STL_const_iterator_tree:
      associateSTLIteratorTree(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_iterator_hashtable:
    case moya::STDKind::STD_STL_const_iterator_hashtable:
      associateSTLIteratorHashtable(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_iterator_deque:
      associateSTLIteratorDeque(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_iterator_list:
    case moya::STDKind::STD_STL_const_iterator_list:
      associateSTLIteratorList(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_iterator_forward_list:
    case moya::STDKind::STD_STL_const_iterator_forward_list:
      associateSTLIteratorForwardList(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_iterator_bucket:
      associateSTLIteratorBucket(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_iterator_bitvector:
    case moya::STDKind::STD_STL_const_iterator_bitvector:
      associateSTLIteratorBitvector(cont, module, feModule);
      break;
    case moya::STDKind::STD_STL_iterator_reverse:
      associateSTLReverseIterator(cont, module, feModule);
      break;
    default:
      moya_error("Unknown STL kind: " << moya::str(cont.getKind()));
      break;
    }
  }
}

void AssociatePass::associateStructTypes(llvm::Module& module,
                                         moya::Module& feModule) {
  for(const clang::Type* astType : types) {
    if(const clang::Type* defn = moya::ClangUtils::getDefinition(astType)) {
      auto* sty = feModule.convertToLLVMAs<llvm::StructType>(defn, true);
      // The struct may not have been created in the actual CodgenModule
      // because it is not needed by any of the code here. If that is the
      // case, we don't create it either because we don't want to pollute
      // the module with an enormous number of types that won't ever be used
      if(llvm::StructType* llvm = module.getTypeByName(sty->getName())) {
        // Unlike LLVM, Clang's types are not uniqued, but for struct types
        // with a definition, we should have only a single LLVM type for a
        // given Clang type
        if(not feModule.hasType(llvm))
          feModule.addType(defn, llvm);
        else
          moya_error("Should never happen");
      }
    }
  }

  // Now that we have all the types, fix any inheritance relationships.
  // clang::ASTContext::getTypes() will return types in proper dependence
  // order, so it may not be safe to do this before hand
  for(moya::Type* type : feModule.getClasses()) {
    const clang::CXXRecordDecl* clss = type->getCXXDecl();

    for(const clang::CXXBaseSpecifier spec : clss->bases())
      if(const auto* base = spec.getType()->getAsCXXRecordDecl())
        if(const clang::RecordDecl* def = moya::ClangUtils::getDefinition(base))
          // The base type may not have a separate LLVM type materialized
          if(feModule.hasType(def))
            type->addBase(&feModule.getType(def));

    for(const clang::CXXBaseSpecifier spec : clss->vbases())
      if(const auto* base = spec.getType()->getAsCXXRecordDecl())
        if(const clang::RecordDecl* def = moya::ClangUtils::getDefinition(base))
          if(feModule.hasType(def))
            type->addVirtualBase(&feModule.getType(def));

    auto* sty = type->getLLVMAs<llvm::StructType>();
    moya::STDKind kind = type->getSTDKind();
    if(moya::STDConfig::isSTLContainer(kind)
       or moya::STDConfig::isSTDString(kind)) {
      const moya::Container& cont = feModule.getSTLContainer(type->getType());
      feModule.addEquivalent(cont.getContainerType(), sty);
    } else if(moya::STDConfig::isSTLIterator(kind)) {
      const moya::Container& cont = feModule.getSTLContainer(type->getType());
      feModule.addEquivalent(cont.getIteratorType(), sty);
    }
  }
}

void AssociatePass::associateTypes(llvm::Module& module,
                                   moya::Module& feModule) {
  // const clang::ASTContext& astContext = feModule.getASTContext();

  // for(const clang::Type* astType : astContext.getTypes()) {
  //   if(not astType->isDependentType()) {
  //     if(astType->isScalarType()) {
  //       llvm::Type* llvm = feModule.convertToLLVM(astType);
  //       if(not hasType(llvm))
  //         addType(astType, llvm);
  //       else
  //         addTypeMapping(astType, feModule.getType(llvm));
  //     } else if(astType->isPointerType()) {
  //       uint64_t depth = moya::ClangUtils::getTypeDepth(astType);
  //       const clang::Type* inner = moya::ClangUtils::getInnermost(astType);

  //       // Even if the inner struct is opaque, we can create a pointer to it
  //       // and it shouldn't be a problem.
  //       llvm::Type* elemTy = nullptr;
  //       if(analysisTypes.contains(inner))
  //         addType(
  //             astType,
  //             moya::LLVMUtils::createPointer(analysisTypes.at(inner),
  //             depth));
  //       else if(not ignore.contains(inner))
  //         addType(astType,
  //                 moya::LLVMUtils::createPoiner(
  //                     feModule.convertToLLVM(inner), depth));
  //     } else if(auto* aty = astType->getAs<ConstantArrayType>()) {
  //       // Flatten the array type because we don't really need the LLVM
  //       // type to be accurate. In any case, we flatten all arrays at some
  //       // point during the analysis anyway
  //       //
  //       auto* num = moya::ClangUtils::getNumFlattenedElements(astType);
  //       const clang::Type* inner = moya::ClangUtils::getInnermost(astType);

  //       // If it is a struct type, we may have created an analysis type for
  //       // it. This will be the case for STL containers and may be true for
  //       // other structs as well. We shouldn't have to worry about opaque
  //       // types here because they would not be created unless the inner
  //       // type was fully defined. This might end up creating extra types
  //       // Hopefully, it will be removed at some point
  //       if(analysisTypes.contains(inner))
  //         addType(astType, llvm::ArrayType::get(analysisTypes.at(inner),
  //         num));
  //       else
  //         addType(
  //             astType,
  //             llvm::ArrayType::get(feModule.convertToLLVM(inner), num));
  //     }
  //   }
  // }
}

void AssociatePass::associateDirectives(llvm::Module& module,
                                        moya::Module& feModule) {
  feModule.associate();
}

bool AssociatePass::runOnModule(llvm::Module& module) {
  moya_message(getPassName());

  layout.reset(module.getDataLayout().getStringRepresentation());

  moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();
  const clang::ASTContext& astContext = feModule.getASTContext();

  // First, we need to isolate the types that we actually need to deal with
  // Types that are only forward declarations and those with sugar should
  // be ignored
  for(const clang::Type* astType : astContext.getTypes()) {
    if(not astType->isDependentType()) {
      const clang::Type* type = astType->getUnqualifiedDesugaredType();
      if(not types.contains(type)) {
        if(type->isScalarType() or type->isPointerType())
          types.insert(type);
        else if(type->isArrayType())
          types.insert(type);
        else if(const clang::Type* defn = moya::ClangUtils::getDefinition(type))
          types.insert(defn);
        // else
        //   ignore.insert(type);
      }
    }
  }

  // The order in which everything is associated is important.
  // STL containers must be associated before structs which must be
  // associated before any other types
  associateFunctions(module, feModule);
  associateGlobals(module, feModule);
  associateSTLTypes(module, feModule);
  associateStructTypes(module, feModule);
  associateTypes(module, feModule);
  associateDirectives(module, feModule);

  return false;
}

char AssociatePass::ID = 0;

using namespace llvm;

static const char* name = "moya-associate-cxx";
static const char* descr = "Associate frontend elements with LLVM IR (C++)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(AssociatePass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_END(AssociatePass, name, descr, cfg, analysis)

Pass* createAssociatePass() {
  return new AssociatePass();
}
