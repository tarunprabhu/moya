#include "Container.h"
#include "frontend/c-family/ClangUtils.h"

#include <clang/AST/Decl.h>

namespace moya {

Container::Container(const clang::Type* type, STDKind kind)
    : kind(kind), type(type), clss(type->getAsCXXRecordDecl()),
      containerTy(nullptr), iteratorTy(nullptr), nodeTy(nullptr),
      baseTy(nullptr), valueTy(nullptr), keyTy(nullptr), mappedTy(nullptr),
      firstTy(nullptr), secondTy(nullptr), nelems(0), valOffset(0),
      ptrOffset(0), mappedOffset(0) {
  for(const clang::Decl* decl : clss->decls()) {
    if(const auto* typdef = llvm::dyn_cast<clang::TypedefNameDecl>(decl)) {
      const clang::Type* type = ClangUtils::getUnderlyingType(typdef);
      if(const clang::Type* defn = ClangUtils::getDefinition(type))
        type = defn;
      members[typdef->getName()] = type;
    }
  }
}

STDKind Container::getKind() const {
  return kind;
}

llvm::StructType* Container::getContainerType() const {
  return containerTy;
}

llvm::StructType* Container::getIteratorType() const {
  return iteratorTy;
}

llvm::StructType* Container::getNodeType() const {
  return nodeTy;
}

llvm::StructType* Container::getBaseType() const {
  return baseTy;
}

llvm::Type* Container::getValueType() const {
  return valueTy;
}

llvm::Type* Container::getKeyType() const {
  return keyTy;
}

llvm::Type* Container::getMappedType() const {
  return mappedTy;
}

llvm::Type* Container::getFirstType() const {
  return firstTy;
}

llvm::Type* Container::getSecondType() const {
  return secondTy;
}

uint64_t Container::getElementCount() const {
  return nelems;
}

uint64_t Container::getValueOffset() const {
  return valOffset;
}

uint64_t Container::getPointerOffset() const {
  return ptrOffset;
}

uint64_t Container::getKeyOffset() const {
  return keyOffset;
}

uint64_t Container::getMappedOffset() const {
  return mappedOffset;
}

const clang::Type* Container::getClangType() const {
  return type;
}

const clang::CXXRecordDecl* Container::getClangDecl() const {
  return clss;
}

const clang::Type* Container::getClangMember(const std::string& name) const {
  if(members.contains(name))
    return members.at(name);
  return nullptr;
}

void Container::setContainerType(llvm::StructType* sty) {
  containerTy = sty;
}

void Container::setIteratorType(llvm::StructType* sty) {
  iteratorTy = sty;
}

void Container::setNodeType(llvm::StructType* sty) {
  nodeTy = sty;
}

void Container::setBaseType(llvm::StructType* sty) {
  baseTy = sty;
}

void Container::setKeyType(llvm::Type* ty) {
  keyTy = ty;
}

void Container::setMappedType(llvm::Type* ty) {
  mappedTy = ty;
}

void Container::setValueType(llvm::Type* ty) {
  valueTy = ty;
}

void Container::setFirstType(llvm::Type* ty) {
  firstTy = ty;
}

void Container::setSecondType(llvm::Type* ty) {
  secondTy = ty;
}

void Container::setElementCount(uint64_t count) {
  nelems = count;
}

void Container::setValueOffset(uint64_t offset) {
  valOffset = offset;
}

void Container::setPointerOffset(uint64_t offset) {
  ptrOffset = offset;
}

void Container::setKeyOffset(uint64_t offset) {
  keyOffset = offset;
}

void Container::setMappedOffset(uint64_t offset) {
  mappedOffset = offset;
}

} // namespace moya
