#include "CodegenVisitor.h"
#include "Module.h"

namespace moya {

CodegenVisitor::CodegenVisitor(Module& module) : module(module) {
  ;
}

Module& CodegenVisitor::getModule() {
  return module;
}

const Module& CodegenVisitor::getModule() const {
  return module;
}

bool CodegenVisitor::VisitVarDecl(clang::VarDecl*) {
  // if(const clang::RecordDecl* defn = getDefnDecl(decl->getType()))
  //   getModule().addType(defn);

  return true;
}

bool CodegenVisitor::VisitFunctionDecl(clang::FunctionDecl*) {
  // for(const clang::ParmVarDecl* param : decl->parameters())
  //   if(const clang::RecordDecl* defn = getDefnDecl(param->getType()))
  //     getModule().addType(defn);
  // if(const clang::RecordDecl* defn = getDefnDecl(decl->getReturnType()))
  //   getModule().addType(defn);

  return true;
}

bool CodegenVisitor::VisitRecordDecl(clang::RecordDecl*) {
  // // If this is a class, it will be handled in VisitCXXRecordDecl
  // if(not clang::isa<clang::CXXRecordDecl>(decl))
  //   if(const clang::RecordDecl* defn = getDefnDecl(decl->getTypeForDecl()))
  //     getModule().addType(defn);

  return true;
}

bool CodegenVisitor::VisitCXXRecordDecl(clang::CXXRecordDecl*) {
  // for(const clang::CXXBaseSpecifier spec : clss->bases())
  //   if(const clang::CXXRecordDecl* base =
  //   spec.getType()->getAsCXXRecordDecl())
  //     if(const clang::RecordDecl* defn = getDefnDecl(base->getTypeForDecl()))
  //       getModule().addType(defn);

  // for(const clang::CXXBaseSpecifier spec : clss->vbases())
  //   if(const clang::CXXRecordDecl* base =
  //   spec.getType()->getAsCXXRecordDecl())
  //     if(const clang::RecordDecl* defn = getDefnDecl(base->getTypeForDecl()))
  //       getModule().addType(defn);

  // if(const clang::RecordDecl* defn = getDefnDecl(clss->getTypeForDecl()))
  //   getModule().addType(defn);

  return true;
}

// bool CodegenVisitor::VisitClassTemplateDecl(ClassTemplateDecl* templ) {
//   for(const CXXRecordDecl* clss : templ->specializations())
//     module.getTypeContext().addType(clss->getTypeForDecl());

//   return true;
// }

bool CodegenVisitor::VisitCXXMemberCallExpr(clang::CXXMemberCallExpr*) {
  // clang::Expr* object = call->getImplicitObjectArgument();
  // if(not object)
  //   object = call;

  // if(CXXMethodDecl* method = call->getMethodDecl())
  //   if(method->isVirtual())
  //     module.addVirtualCall(call);

  return true;
}

// bool CodegenVisitor::VisitFunctionTemplateDecl(FunctionTemplateDecl* templ) {
//   for(const FunctionDecl* f : templ->specializations()) {
//     for(const ParmVarDecl* param : f->parameters())
//       module.getTypeContext().addType(param->getType());
//     module.getTypeContext.addType(f->getReturnType());
//   }

//   return true;
// }

} // namespace moya
