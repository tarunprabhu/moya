#include "Argument.h"
#include "Function.h"
#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/AerynConfig.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Map.h"
#include "common/Metadata.h"
#include "common/Set.h"
#include "common/StringUtils.h"
#include "frontend/all-langs/InstrumentFunctionsCommonPass.h"
#include "frontend/all-langs/InstrumentUtils.h"
#include "frontend/c-family/ClangUtils.h"

#include <clang/AST/Decl.h>
#include <clang/Basic/Builtins.h>

#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Intrinsics.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using llvm::cast;
using llvm::dyn_cast;
using llvm::isa;

class InstrumentFunctionsPass : public llvm::ModulePass {
protected:
  moya::Set<llvm::Function*> stdImplFuncs;
  moya::Set<llvm::Function*> stdIfaceFuncs;
  moya::Map<const clang::CXXRecordDecl*, llvm::StructType*> clangTypeMap;
  moya::Set<llvm::Constant*> ginits;

protected:
  void identifySysFuncs(llvm::Module&, const moya::Module&);

  // These are functions with declarations but not definition. These are
  // likely non-templated functions that may or may not have a definition
  // at link-time
  bool instrLibFunction(llvm::Function&, const moya::Function&);

  // These are functions that are not STL containers but other functions
  // from the C++ standard library
  bool instrSTDFunction(llvm::Function&, const moya::Function&);

  bool instrUserFunction(llvm::Function&, const moya::Function&);

  bool allUsesInSystemFunctions(llvm::Function&,
                                const moya::Set<llvm::Function*>&);

  bool isGlobalInitializer(llvm::Value*);
  void addGlobalInits(llvm::Constant*);

public:
  InstrumentFunctionsPass();

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);

public:
  static char ID;
};

InstrumentFunctionsPass::InstrumentFunctionsPass() : ModulePass(ID) {
  llvm::initializeInstrumentFunctionsPassPass(
      *llvm::PassRegistry::getPassRegistry());
}

llvm::StringRef InstrumentFunctionsPass::getPassName() const {
  return "Moya Instrument Functions (C++)";
}

void InstrumentFunctionsPass::getAnalysisUsage(llvm::AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.addRequired<InstrumentFunctionsCommonPass>();
  AU.setPreservesCFG();
}

// Identify all the entry points into libc++ from STL expansions.
// This will almost certainly have a cascade effect with a bunch of other
// implementation templates showing up in the source. To identify the
// "entry points", first find the functions that are instantiated from
// template expansions
//
// FIXME: This will probably break if other templated libraries are included
// and are considered "system headers". Need to understand what Clang means
// by "system location".
//
static moya::Set<llvm::Function*>
getInterfaceFunctions(llvm::Module& module, const moya::Module& feModule) {
  moya::Set<llvm::Function*> funcs;
  const clang::SourceManager& srcMgr = feModule.getSourceManager();

  for(llvm::Function& f : module.functions()) {
    if(f.size() and feModule.hasFunction(f)) {
      const moya::Function& feFunc = feModule.getFunction(f);
      const clang::FunctionDecl* decl = feFunc.getDecl();

      // FIXME: This is not a great way to do things. STDKind is determined
      // by demangling the function name and looking namespaces and prefixes
      // That will only work for certain libc++ implementations because the
      // namespaces may not be std:: (for instance, for GNU libstdc++, the
      // namespace for random access iterators is __gnu_cxx::__normal_iterator
      // which never appears directly in the code). Ideally, we should not
      // rely on the demangled names to correctly identify the standard C++
      // library entry functions. For the moment, this hack will do
      if(feFunc.getSTDKind() != moya::STDKind::STD_None)
        // if(const auto* templ = moya::ClangUtils::getTemplateDecl(decl))
        // clang::SourceManager::isInSystemHeader(clang::SourceLocation) may
        // not be the best way of doing this because I am not sure what will
        // happen if some other template library unrelated to the C++
        // standard library is used and is deemed by Clang to be a
        // "system header".
        // // if(srcMgr.isInSystemHeader(templ->getBeginLoc())) {
        // //   llvm::errs() << moya::ClangUtils::getQualifiedDeclName(decl)
        // //                << " => "
        // //                <<
        // moya::ClangUtils::getQualifiedDeclName(templ)
        // //                << "\n";
        funcs.insert(&f);
    }
  }

  return funcs;
}

bool InstrumentFunctionsPass::isGlobalInitializer(llvm::Value* v) {
  if(auto* cnst = dyn_cast<llvm::Constant>(v))
    return ginits.contains(cnst);
  return false;
}

// This is only meant to be used on functions that are themselves the result
// of template expansions from a "system header". At that point we can
// be reasonably certain that they are not going to show up in vtables
// or anything funky like that.
//
bool InstrumentFunctionsPass::allUsesInSystemFunctions(
    llvm::Function& f,
    const moya::Set<llvm::Function*>& sysFuncs) {
  if(f.getNumUses() == 0)
    return false;

  unsigned sysFuncUses = 0;
  for(llvm::Use& u : f.uses()) {
    llvm::Value* user = u.getUser();
    if(isa<llvm::CallInst>(user) or isa<llvm::InvokeInst>(user)
       or isa<llvm::CastInst>(user)) {
      if(not sysFuncs.contains(moya::LLVMUtils::getFunction(user)))
        return false;
      else
        sysFuncUses++;
    } else if(isa<llvm::GlobalAlias>(user) or isGlobalInitializer(user)) {
      ;
    } else if(not user->getNumUses()) {
      // This check is here because sometimes a use seems to be found in an
      // "orphaned" constexpr. This is probably a side-effect of an earlier
      // pass not cleaning up after itself properly.
    } else {
      moya_error("Unexpected use of function: " << f.getName() << " => "
                                                << *user);
    }
  }

  return sysFuncUses;
}

bool InstrumentFunctionsPass::instrLibFunction(llvm::Function& f,
                                               const moya::Function& feFunc) {
  bool changed = false;

  const clang::FunctionDecl* decl = feFunc.getDecl();

  if((feFunc.getSTDKind() != moya::STDKind::STD_None) and feFunc.isPublic()) {
    if(moya::LLVMUtils::isPureFunction(f))
      changed |= moya::Metadata::setModel(
          f, moya::AerynConfig::getPureFunctionModel());
    else if(f.getName() == decl->getQualifiedNameAsString())
      changed |= moya::Metadata::setModel(f, f.getName().str());
    else
      changed |= moya::Metadata::setModel(f, feFunc.getModel());
  }

  return changed;
}

bool InstrumentFunctionsPass::instrSTDFunction(llvm::Function& f,
                                               const moya::Function& feFunc) {
  bool changed = false;

  const clang::FunctionDecl* decl = feFunc.getDecl();

  // This is a function that is probably an STL function that is
  // directly called from the user code. It needs a model because we
  // really don't want to analyze it
  if(moya::LLVMUtils::isPureFunction(f))
    changed |= moya::Metadata::setModel(
        f, moya::AerynConfig::getPureFunctionModel());
  else if(f.getName() == decl->getQualifiedNameAsString())
    changed |= moya::Metadata::setModel(f, f.getName().str());

  return changed;
}

bool InstrumentFunctionsPass::instrUserFunction(llvm::Function& f,
                                                const moya::Function& feFunc) {
  bool changed = false;

  const moya::Module& feModule = feFunc.getModule();

  changed |= moya::Metadata::setSourceLang(f, feModule.getSourceLang());
  if(f.getName() == "main")
    changed |= moya::Metadata::setMain(f);

  // This is a user-defined function. It may be the result of template
  // expansion, but we don't care
  if(feFunc.getFullName() != f.getName())
    changed |= moya::Metadata::setMangled(f);

  // FIXME: This is a shitty workaround for the moment because I don't want
  // to have debug this.
  //
  // There are several reasons why there could be a disagreement between
  // the number of arguments in the frontend function and the LLVM IR function.
  //
  //   - If one of the arguments  is a struct, it gets "flattened" and packed
  //     into arguments when calling the function.
  //
  //   - If the function has been generated by clang (because it was either
  //     implicitly allowed to or explicitly directed to (with the default
  //     specifier), then there may only be declaration with a mismatched
  //     number of parameters
  //
  // At some point, it would be nice to have everything match up correctly
  //
  if(feFunc.getNumArgs() == f.getFunctionType()->getNumParams()) {
    for(unsigned i = 0; i < feFunc.getNumArgs(); i++) {
      llvm::Argument* arg = moya::LLVMUtils::getArgument(f, i);
      const moya::Argument& feArg = feFunc.getArg(i);
      if(feArg.hasLocation()) {
        changed |= moya::Metadata::setSourceName(arg, feArg.getName());
        changed |= moya::Metadata::setSourceLocation(arg, feArg.getLocation());
      } else {
        changed |= moya::Metadata::setArtificialName(arg, feArg.getName());
        changed |= moya::Metadata::setArtificial(arg);
      }
    }
  }

  if(feFunc.hasName())
    changed |= moya::Metadata::setSourceName(f, feFunc.getName());

  if(feFunc.hasQualifiedName())
    changed |= moya::Metadata::setQualifiedName(f, feFunc.getQualifiedName());

  if(feFunc.hasFullName())
    changed |= moya::Metadata::setFullName(f, feFunc.getFullName());

  if(feFunc.isMemcpy())
    changed |= moya::Metadata::setSystemMemcpy(f);

  if(feFunc.isMemset())
    changed |= moya::Metadata::setSystemMemset(f);

  if(feFunc.isConstructor())
    changed |= moya::Metadata::setConstructor(f);

  if(feFunc.isDestructor())
    changed |= moya::Metadata::setDestructor(f);

  if(feFunc.isVirtual())
    changed |= moya::Metadata::setVirtual(f);

  if(feFunc.getSTDKind() == moya::STDKind::STD_None) {
    if(const clang::CXXRecordDecl* clss = feFunc.getClassDecl()) {
      // Not sure why some class definitions don't show up in the module.
      // It probably has to do with template expansions, but exactly what
      // I don't know.
      //
      // FIXME: Figure out why this condition is necessary
      if(feModule.hasType(clss->getTypeForDecl())) {
        changed |= moya::Metadata::setClass(
            f,
            feModule.getType(clss->getTypeForDecl())
                .template getLLVMAs<llvm::StructType>());
      }
    }
  }

  if(feFunc.getLocation().isValid())
    changed |= moya::Metadata::setSourceLocation(f, feFunc.getLocation());
  if(feFunc.hasBeginLoc() and feFunc.hasEndLoc()) {
    changed |= moya::Metadata::setBeginLocation(f, feFunc.getBeginLoc());
    changed |= moya::Metadata::setEndLocation(f, feFunc.getEndLoc());
  }

  changed |= moya::InstrumentUtils::instrumentDirectives(feFunc, f);

  // C++ has similar problems to C where arguments could be passed by pointer
  // and we have no way of determining whether the argument is simply a pointer
  // to a scalar or if it is an array. If the argument has dereferenceable
  // bytes, it was passed by reference. We assume that any scalar that is
  // passed by reference is to be treated as a scalar. As in C, if an argument
  // is passed by pointer and it is not a pointer to a fixed-length array
  // and it has not been annotated as being an array, we just ignore it
  //
  // FIXME: Should check what happens when pointers to scalars are passed
  // by reference. Does the argument get passed as a pointer to a pointer in
  // that case?
  if(moya::Metadata::hasSpecialize(f)) {
    for(llvm::Argument& arg : f.args()) {
      if(auto* pty
         = moya::LLVMUtils::getAnalysisTypeAs<llvm::PointerType>(arg)) {
        if(not(arg.getDereferenceableBytes()
               or moya::Metadata::hasFixedArray(arg)
               or moya::Metadata::hasDynArray(arg)
               or isa<llvm::ArrayType>(pty->getElementType())))
          changed |= moya::Metadata::setIgnore(arg);
      }
    }
  }

  return changed;
}

void InstrumentFunctionsPass::identifySysFuncs(llvm::Module& module,
                                               const moya::Module& feModule) {
  moya::Set<llvm::Function*> sysFuncs = getInterfaceFunctions(module, feModule);
  // FIXME: This a workaround that should almost certainly belong elsewhere
  // If a class is defined within function, the qualified name doesn't
  // include the name of the containing function (makes sense since it isn't
  // visible outside the function). The STDKind in that case gets set to
  // STD_None, when it probably ought to be STD_Impl (we're only really
  // concerned with STL for this case). But we still want to add it to the
  // list if impl functions so it doesn't get processed as a user function
  for(llvm::Function& f : module.functions())
    if(f.size() and (not sysFuncs.contains(&f))
       and allUsesInSystemFunctions(f, sysFuncs))
      stdImplFuncs.insert(&f);

  for(llvm::Function* f : sysFuncs)
    if(not allUsesInSystemFunctions(*f, sysFuncs))
      stdIfaceFuncs.insert(f);
    else
      stdImplFuncs.insert(f);
}

void InstrumentFunctionsPass::addGlobalInits(llvm::Constant* c) {
  if(ginits.insert(c))
    for(llvm::Use& op : c->operands())
      if(auto* cnst = dyn_cast<llvm::Constant>(op.get()))
        if(not isa<llvm::Function>(cnst))
          addGlobalInits(cnst);
}

bool InstrumentFunctionsPass::runOnModule(llvm::Module& module) {
  moya_message(getPassName());

  bool changed = false;

  moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  for(llvm::GlobalVariable& g : module.globals())
    if(not isa<llvm::Function>(g) and g.hasInitializer())
      addGlobalInits(g.getInitializer());

  for(llvm::Function& f : module.functions())
    if(llvm::Function* personality = moya::LLVMUtils::getPersonalityFn(f))
      changed |= moya::Metadata::setNoAnalyze(personality);

  identifySysFuncs(module, feModule);

  for(llvm::Function& f : module.functions()) {
    if(feModule.hasDeclaration(f)) {
      changed |= instrLibFunction(f, feModule.getDeclaration(f));
    } else if(feModule.hasFunction(f)) {
      const moya::Function& feFunc = feModule.getFunction(f);
      if(stdImplFuncs.contains(&f))
        // If this is a function that is the result of a template expansion
        // that is never used in user code, then it is probably a result of
        // the implementation of something in libc++. We don't want to have to
        // deal with that at all
        changed |= moya::Metadata::setNoAnalyze(f);
      else if(stdIfaceFuncs.contains(&f))
        // This will be instrumented in another pass
        ;
      else if(f.size())
        changed |= instrUserFunction(f, feFunc);
    } else if(f.isVarArg() and not moya::Metadata::hasNoAnalyze(f)) {
      moya_error("vararg functions not supported: " << f.getName());
    } else if(f.size()) {
      // This is probably a default constructor/destructor/other function that
      // has been generated
    } else {
      // This is also a default constructor/destructor but for a library
      // function that will become available at link time
    }
  }

  return changed;
}

using namespace llvm;

char InstrumentFunctionsPass::ID = 0;

static const char* name = "moya-instr-funcs-cxx";
static const char* descr = "Adds instrumentation to functions (C++)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(InstrumentFunctionsPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_DEPENDENCY(InstrumentFunctionsCommonPass)
INITIALIZE_PASS_END(InstrumentFunctionsPass, name, descr, cfg, analysis)

llvm::Pass* createInstrumentFunctionsPass() {
  return new InstrumentFunctionsPass();
}
