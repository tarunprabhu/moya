#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/DemanglerCXX.h"
#include "common/Diagnostics.h"
#include "common/Metadata.h"
#include "common/StringUtils.h"
#include "frontend/all-langs/InstrumentGlobalsCommonPass.h"
#include "frontend/c-family/ClangUtils.h"

#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class InstrumentGlobalsPass : public ModulePass {
public:
  static char ID;

public:
  InstrumentGlobalsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

InstrumentGlobalsPass::InstrumentGlobalsPass() : ModulePass(ID) {
  initializeInstrumentGlobalsPassPass(*PassRegistry::getPassRegistry());
}

StringRef InstrumentGlobalsPass::getPassName() const {
  return "Moya Instrument Globals (C++)";
}

void InstrumentGlobalsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.addRequired<InstrumentGlobalsCommonPass>();
  AU.setPreservesCFG();
}

bool InstrumentGlobalsPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;
  moya::DemanglerCXX demangler;

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  for(GlobalVariable& g : module.globals()) {
    if(g.hasName()) {
      changed |= moya::Metadata::setSourceLang(g, feModule.getSourceLang());
      if(feModule.hasGlobal(g)) {
        const clang::VarDecl* decl = feModule.getGlobal(g).getDecl();
        const clang::Type* type
            = moya::ClangUtils::getDefinition(decl->getType().getTypePtr());
        changed |= moya::Metadata::setSourceName(
            g, moya::ClangUtils::getDeclName(decl));
        changed |= moya::Metadata::setQualifiedName(
            g, moya::ClangUtils::getQualifiedDeclName(decl));
        changed |= moya::Metadata::setFullName(
            g, moya::ClangUtils::getFullDeclName(decl));
        changed |= moya::Metadata::setSourceLocation(
            g, moya::ClangUtils::getLocation(decl));
        if(feModule.hasSTLContainer(type))
          changed
              |= moya::Metadata::setAnalysisType(g,
                                                 feModule.getSTLContainer(type)
                                                     .getContainerType()
                                                     ->getPointerTo());
        else if(feModule.hasType(type))
          changed |= moya::Metadata::setAnalysisType(
              g, feModule.getType(type).getLLVM()->getPointerTo());
      }

      std::string gname = demangler.demangle(g, moya::Demangler::Action::Full);
      if(moya::StringUtils::startswith(gname, "vtable for"))
        changed |= moya::Metadata::setCXXVtable(g);
      else if(moya::StringUtils::startswith(gname, "VTT for"))
        changed |= moya::Metadata::setCXXVTT(g);
      else if(moya::StringUtils::startswith(gname, "typeinfo for"))
        changed |= moya::Metadata::setCXXTypeInfo(g);
      else if(moya::StringUtils::startswith(gname, "typeinfo name for"))
        changed |= moya::Metadata::setCXXTypeInfoName(g);
    }
  }

  return changed;
}

char InstrumentGlobalsPass::ID = 0;

static const char* name = "moya-instr-globals-cxx";
static const char* descr = "Adds instrumentation to globals (C++)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(InstrumentGlobalsPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_DEPENDENCY(InstrumentGlobalsCommonPass)
INITIALIZE_PASS_END(InstrumentGlobalsPass, name, descr, cfg, analysis)

Pass* createInstrumentGlobalsPass() {
  return new InstrumentGlobalsPass();
}
