#include "TypeBase.h"
#include "ModuleBase.h"

namespace moya {

TypeBase::TypeBase(ModuleBase& module) : llvm(nullptr), module(module) {
  ;
}

void TypeBase::setLLVM(llvm::Type* type) {
  llvm = type;
  getModule().setLLVM(*this, type);
}

SourceLang TypeBase::getSourceLang() const {
  return module.getSourceLang();
}

ModuleBase& TypeBase::getModule() {
  return module;
}

const ModuleBase& TypeBase::getModule() const {
  return module;
}

const Location& TypeBase::getLocation() const {
  return loc;
}

bool TypeBase::hasLLVM() const {
  return llvm;
}

llvm::Type* TypeBase::getLLVM() const {
  return llvm;
}

} // namespace moya
