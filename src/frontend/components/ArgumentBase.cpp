#include "ArgumentBase.h"

namespace moya {

ArgumentBase::ArgumentBase(const FunctionBase& func,
                           const std::string& name,
                           int num)
    : func(func), name(name), artificialName(""), num(num), artificial(false) {
  ;
}

void ArgumentBase::setName(const std::string& name) {
  this->name = name;
}

void ArgumentBase::setArtificialName(const std::string& artificial) {
  this->artificialName = artificial;
  this->artificial = artificialName.length();
}

void ArgumentBase::setLocation(const Location& loc) {
  this->loc = loc;
}

void ArgumentBase::setArtificial(bool artificial) {
  this->artificial = artificial;
}

const std::string& ArgumentBase::getName() const {
  return name;
}

const std::string& ArgumentBase::getArtificialName() const {
  return artificialName;
}

int ArgumentBase::getArgNum() const {
  return num;
}

const Location& ArgumentBase::getLocation() const {
  return loc;
}

const FunctionBase& ArgumentBase::getFunction() const {
  return func;
}

bool ArgumentBase::hasName() const {
  return name.length();
}

bool ArgumentBase::hasArtificialName() const {
  return artificialName.length();
}

bool ArgumentBase::hasLocation() const {
  return loc.isValid();
}

bool ArgumentBase::isArtificial() const {
  return artificial;
}

} // namespace moya
