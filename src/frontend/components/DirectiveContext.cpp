#include "DirectiveContext.h"
#include "FunctionBase.h"
#include "ModuleBase.h"

namespace moya {

DirectiveContext::DirectiveContext(ModuleBase& module)
    : parser(*this), module(module) {
  ;
}

Directive* DirectiveContext::create(Directive::Kind kind, const Location& loc) {
  Directive* directive = nullptr;
  switch(kind) {
  case Directive::Kind::Specialize:
    directive = new SpecializeDirective(loc);
    break;
  case Directive::Kind::Declare:
    directive = new DeclareDirective(loc);
    break;
  case Directive::Kind::JIT:
    directive = new JITDirective(loc);
    break;
  case Directive::Kind::Model:
    directive = new ModelDirective(loc);
    break;
  case Directive::Kind::Optimize:
    directive = new OptimizeDirective(loc);
    break;
  default:
    return nullptr;
  }

  add(directive);

  return directive;
}

void DirectiveContext::add(Directive* directive) {
  Directive::Kind kind = directive->getKind();

  allocated.emplace_back(directive);
  directives[kind].push(directive);

  if(auto* func = llvm::dyn_cast<FunctionDirective>(directive))
    std::get<Vector<FunctionDirective*>>(groups).push_back(func);
  else if(auto* region = llvm::dyn_cast<RegionDirective>(directive))
    std::get<Vector<RegionDirective*>>(groups).push_back(region);
  else if(auto* loop = llvm::dyn_cast<LoopDirective>(directive))
    std::get<Vector<LoopDirective*>>(groups).push_back(loop);
}

Directive* DirectiveContext::getMatchingDirective(Directive::Kind kind) {
  if(directives.contains(kind))
    if(directives.at(kind).size())
      return directives.at(kind).back();
  return nullptr;
}

void DirectiveContext::associate() {
  // First group the functions and directives by file. Locations that are
  // from different files are not comparable and the binary return value of the
  // sort function is not sufficient to capture this distinction
  Map<std::string, Vector<FunctionBase*>> fmap;
  Map<std::string, Vector<FunctionDirective*>> dmap;

  for(auto& ptr : module.getFunctions()) {
    FunctionBase* f = ptr.get();
    fmap[f->getLocation().getFile()].push_back(f);
  }
  for(FunctionDirective* d : getDirectives<FunctionDirective>()) {
    dmap[d->getLocation().getFile()].push_back(d);
  }

  for(auto& it : fmap) {
    const std::string& file = it.first;
    if(dmap.contains(file)) {
      Vector<FunctionBase*>& funcs = it.second;
      std::sort(funcs.begin(),
                funcs.end(),
                [](const FunctionBase* f1, const FunctionBase* f2) {
                  return Location::compare(f1->getLocation(), f2->getLocation())
                         == Location::Cmp::Before;
                });
      // The directives will already be in order

      const Vector<FunctionDirective*>& functionDirectives = dmap.at(file);
      size_t i = 0, j = 0;
      while(i < functionDirectives.size() and j < funcs.size()) {
        FunctionDirective* directive = functionDirectives.at(i);
        const Location& loc = directive->getLocation();

        // Find the first function after the current function directive.
        // The directive will be associated with this function
        //
        // FIXME: There is a shitty bug where the directive could be well away
        // from
        //        the function and not by its definition and it would still get
        //        associated. This ought to be done with attributes, not pragmas
        while((j < funcs.size())
              and (Location::compare(funcs.at(j)->getLocation(), loc)
                   != Location::Cmp::After))
          j++;
        if(j < funcs.size())
          funcs.at(j)->addDirective(directive);
        else
          moya_warning("Could not associate directive \""
                       << directive->str() << "\" at "
                       << directive->getLocation().str()
                       << " with any function");
        i++;
      }
    }
  }
}

Directive* DirectiveContext::parse(const std::string& text,
                                   const Location& loc) {
  return parser.parse(text, loc);
}

SourceLang DirectiveContext::getSourceLang() const {
  return module.getSourceLang();
}

void DirectiveContext::serialize(Serializer& s) const {
  s.arrStart();
  for(const auto& ptr : allocated)
    ptr.get()->serialize(s);
  s.arrEnd();
}

void DirectiveContext::deserialize(Deserializer& d) {
  d.arrStart();
  while(not d.arrEmpty())
    add(Directive::deserializeFrom(d));
  d.arrEnd();
}

} // namespace moya
