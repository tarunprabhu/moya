#include "ModuleBase.h"
#include "ArgumentBase.h"
#include "FunctionBase.h"
#include "GlobalVariableBase.h"
#include "TypeBase.h"
#include "frontend/annotations/Directives.h"

namespace moya {

ModuleBase::ModuleBase(SourceLang lang) : lang(lang), directives(*this) {
  ;
}

SourceLang ModuleBase::getSourceLang() const {
  return lang;
}

Region& ModuleBase::addRegion(RegionID id,
                              const std::string& name,
                              const Location& loc,
                              const Location& beginLoc,
                              const Location& endLoc) {
  return regions.emplace_back(id, name, loc, beginLoc, endLoc, getSourceLang());
}

void ModuleBase::setSourceLang(SourceLang lang) {
  this->lang = lang;
}

bool ModuleBase::hasDeclaration(llvm::Function& llvm) const {
  return llvmDeclMap.contains(&llvm);
}

FunctionBase& ModuleBase::getDeclaration(llvm::Function& llvm) {
  return *llvmDeclMap.at(&llvm);
}

FunctionBase& ModuleBase::getDeclaration(llvm::Function* llvm) {
  return getDeclaration(*llvm);
}

const FunctionBase& ModuleBase::getDeclaration(llvm::Function& llvm) const {
  return *llvmDeclMap.at(&llvm);
}

const FunctionBase& ModuleBase::getDeclaration(llvm::Function* llvm) const {
  return getDeclaration(*llvm);
}

void ModuleBase::setLLVM(FunctionBase& func, llvm::Function& llvm) {
  if(func.isDefined())
    llvmFunctionMap[&llvm] = &func;
  else
    llvmDeclMap[&llvm] = &func;
}

bool ModuleBase::hasFunction(llvm::Function& llvm) const {
  return llvmFunctionMap.contains(&llvm);
}

bool ModuleBase::hasFunction(llvm::Function* llvm) const {
  return hasFunction(*llvm);
}

FunctionBase& ModuleBase::getFunction(llvm::Function& llvm) {
  return *llvmFunctionMap.at(&llvm);
}

FunctionBase& ModuleBase::getFunction(llvm::Function* llvm) {
  return getFunction(*llvm);
}

const FunctionBase& ModuleBase::getFunction(llvm::Function& llvm) const {
  return *llvmFunctionMap.at(&llvm);
}

const FunctionBase& ModuleBase::getFunction(llvm::Function* llvm) const {
  return getFunction(*llvm);
}

void ModuleBase::setLLVM(GlobalVariableBase& global,
                         llvm::GlobalVariable& llvm) {
  llvmGlobalMap[&llvm] = &global;
}

bool ModuleBase::hasGlobal(llvm::GlobalVariable& llvm) const {
  return llvmGlobalMap.contains(&llvm);
}

bool ModuleBase::hasGlobal(llvm::GlobalVariable* llvm) const {
  return hasGlobal(*llvm);
}

GlobalVariableBase& ModuleBase::getGlobal(llvm::GlobalVariable& llvm) {
  return *llvmGlobalMap.at(&llvm);
}

GlobalVariableBase& ModuleBase::getGlobal(llvm::GlobalVariable* llvm) {
  return getGlobal(*llvm);
}

const GlobalVariableBase&
ModuleBase::getGlobal(llvm::GlobalVariable& llvm) const {
  return *llvmGlobalMap.at(&llvm);
}

const GlobalVariableBase&
ModuleBase::getGlobal(llvm::GlobalVariable* llvm) const {
  return getGlobal(*llvm);
}

void ModuleBase::setLLVM(TypeBase& type, llvm::Type* llvm) {
  llvmTypeMap[llvm] = &type;
}

bool ModuleBase::hasType(llvm::Type* llvm) const {
  return llvmTypeMap.contains(llvm);
}

TypeBase& ModuleBase::getType(llvm::Type* type) {
  return *llvmTypeMap.at(type);
}

const TypeBase& ModuleBase::getType(llvm::Type* type) const {
  return *llvmTypeMap.at(type);
}

void ModuleBase::associate() {
  directives.associate();
  for(auto* region : directives.getDirectives<RegionDirective>())
    addRegion(region->getRegionID(),
              region->getRegionName(),
              region->getLocation(),
              region->getBeginLoc(),
              region->getEndLoc());
}

const Vector<Region>& ModuleBase::getRegions() const {
  return regions;
}

const Vector<std::unique_ptr<FunctionBase>>&
ModuleBase::getDeclarations() const {
  return decls;
}

const Vector<std::unique_ptr<FunctionBase>>& ModuleBase::getFunctions() const {
  return funcs;
}

const Vector<std::unique_ptr<GlobalVariableBase>>&
ModuleBase::getGlobals() const {
  return globals;
}

const Vector<std::unique_ptr<TypeBase>>& ModuleBase::getTypes() const {
  return types;
}

DirectiveContext& ModuleBase::getDirectiveContext() {
  return directives;
}

const DirectiveContext& ModuleBase::getDirectiveContext() const {
  return directives;
}

} // namespace moya
