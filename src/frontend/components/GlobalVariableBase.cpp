#include "GlobalVariableBase.h"
#include "ModuleBase.h"

namespace moya {

GlobalVariableBase::GlobalVariableBase(ModuleBase& module)
    : name(""), artificialName(""), artificial(false), llvm(nullptr),
      module(module) {
  ;
}

void GlobalVariableBase::setName(const std::string& name) {
  this->name = name;
}

void GlobalVariableBase::setArtificialName(const std::string& artificial) {
  this->artificialName = artificial;
  this->artificial = artificialName.length();
}

void GlobalVariableBase::setArtificial(bool artificial) {
  this->artificial = artificial;
}

void GlobalVariableBase::setLocation(const Location& loc) {
  this->loc = loc;
}

void GlobalVariableBase::setLLVM(llvm::GlobalVariable& llvm) {
  this->llvm = &llvm;
  getModule().setLLVM(*this, llvm);
}

bool GlobalVariableBase::hasName() const {
  return name.length();
}

bool GlobalVariableBase::hasArtificialName() const {
  return artificialName.length();
}

bool GlobalVariableBase::hasLocation() const {
  return loc.isValid();
}

bool GlobalVariableBase::hasLLVM() const {
  return llvm;
}

bool GlobalVariableBase::isArtificial() const {
  return artificial;
}

ModuleBase& GlobalVariableBase::getModule() {
  return module;
}

const ModuleBase& GlobalVariableBase::getModule() const {
  return module;
}

SourceLang GlobalVariableBase::getSourceLang() const {
  return module.getSourceLang();
}

const std::string& GlobalVariableBase::getName() const {
  return name;
}

const std::string& GlobalVariableBase::getArtificialName() const {
  return artificialName;
}

const Location& GlobalVariableBase::getLocation() const {
  return loc;
}

llvm::GlobalVariable& GlobalVariableBase::getLLVM() const {
  return *llvm;
}

} // namespace moya
