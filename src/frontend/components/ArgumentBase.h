#ifndef MOYA_FRONTEND_COMPONENTS_ARGUMENT_BASE_H
#define MOYA_FRONTEND_COMPONENTS_ARGUMENT_BASE_H

#include "common/Location.h"
#include "common/SourceLang.h"

namespace moya {

class FunctionBase;

class ArgumentBase {
protected:
  const FunctionBase& func;
  std::string name;
  std::string artificialName;
  int num;
  Location loc;
  bool artificial;

protected:
  ArgumentBase(const FunctionBase&, const std::string& = "", int = -1);

public:
  ArgumentBase(const ArgumentBase&) = delete;
  virtual ~ArgumentBase() = default;

  void setName(const std::string&);
  void setArtificialName(const std::string&);
  void setLocation(const Location&);
  void setArtificial(bool);

  const std::string& getName() const;
  const std::string& getArtificialName() const;
  int getArgNum() const;
  const Location& getLocation() const;
  const FunctionBase& getFunction() const;
  bool hasName() const;
  bool hasArtificialName() const;
  bool hasLocation() const;
  bool isArtificial() const;

  friend class FunctionBase;
};

} // namespace moya

#endif // MOYA_FRONTEND_COMPONENTS_ARGUMENT_BASE_H
