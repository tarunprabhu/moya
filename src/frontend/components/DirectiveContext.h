#ifndef MOYA_FRONTEND_COMPONENTS_DIRECTIVE_CONTEXT_H
#define MOYA_FRONTEND_COMPONENTS_DIRECTIVE_CONTEXT_H

#include "common/Map.h"
#include "common/Queue.h"
#include "common/SourceLang.h"
#include "common/Vector.h"
#include "frontend/annotations/Directives.h"
#include "frontend/annotations/Parser.h"

namespace moya {

class ModuleBase;

// A container for all the directives that are parsed. One of these is
// contained within every module because the module object is what is used
// to associate directives with functions
class DirectiveContext {
protected:
  Parser parser;

  // These directives will never be deleted until the object goes out of scope
  Vector<std::unique_ptr<Directive>> allocated;

  std::tuple<Vector<RegionDirective*>,
             Vector<FunctionDirective*>,
             Vector<LoopDirective*>>
      groups;

  // This will be used to keep track of the directives during the parsing
  // and directives may be removed from it
  Map<Directive::Kind, Queue<Directive*>> directives;

  ModuleBase& module;

protected:
  Directive* create(Directive::Kind, const Location&);
  Directive* getMatchingDirective(Directive::Kind);

  void add(Directive*);

public:
  DirectiveContext(ModuleBase&);
  DirectiveContext(const DirectiveContext&) = delete;

  template <typename T>
  Vector<T*> popNearest(const Location& loc) {
    static_assert(std::is_base_of<Directive, T>::value, "Not a directive");
    Vector<T*> ret;
    for(Directive::Kind kind : Directive::getKinds<T>()) {
      if(directives.contains(kind) and directives.at(kind).size()) {
        T* directive = llvm::cast<T>(directives.at(kind).front());
        const Location& directiveLoc = directive->getLocation();
        if(Location::compare(directiveLoc, loc) == Location::Cmp::Before)
          ret.push_back(directive);
      }
    }
    for(T* directive : ret)
      directives.at(directive->getKind()).pop();
    return ret;
  }

  template <typename T>
  const Vector<T*>& getDirectives() const {
    static_assert(std::is_base_of<Directive, T>::value, "Not a directive");
    return std::get<Vector<T*>>(groups);
  }

  Directive* parse(const std::string&, const Location&);
  void associate();

  SourceLang getSourceLang() const;

  virtual void serialize(Serializer&) const;
  virtual void deserialize(Deserializer&);

  friend class Parser;
};

} // namespace moya

#endif // MOYA_FRONTEND_COMPONENTS_DIRECTIVE_CONTEXT_H
