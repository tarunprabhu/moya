#ifndef MOYA_FRONTEND_COMPONENTS_MODULE_BASE_H
#define MOYA_FRONTEND_COMPONENTS_MODULE_BASE_H

#include "DirectiveContext.h"
#include "FunctionBase.h"
#include "GlobalVariableBase.h"
#include "TypeBase.h"
#include "common/Map.h"
#include "common/Region.h"
#include "common/SourceLang.h"
#include "common/Vector.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/GlobalVariable.h>

namespace moya {

// A module corresponds to a single source file
class ModuleBase {
protected:
  Vector<Region> regions;
  Vector<std::unique_ptr<FunctionBase>> funcs;
  Vector<std::unique_ptr<FunctionBase>> decls;
  Vector<std::unique_ptr<GlobalVariableBase>> globals;
  Vector<std::unique_ptr<TypeBase>> types;
  Map<llvm::Function*, FunctionBase*> llvmDeclMap;
  Map<llvm::Function*, FunctionBase*> llvmFunctionMap;
  Map<llvm::GlobalVariable*, GlobalVariableBase*> llvmGlobalMap;
  Map<llvm::Type*, TypeBase*> llvmTypeMap;
  SourceLang lang;
  DirectiveContext directives;

public:
  ModuleBase(SourceLang lang = SourceLang::UnknownLang);
  virtual ~ModuleBase() = default;

  void setSourceLang(SourceLang);
  Region& addRegion(RegionID,
                    const std::string&,
                    const Location&,
                    const Location&,
                    const Location&);

  template <typename FunctionT, typename... ArgsT>
  FunctionT& addDeclaration(ArgsT&&... args) {
    static_assert(std::is_base_of<FunctionBase, FunctionT>::value,
                  "Declaration type must be subclass of FunctionBase");
    return static_cast<FunctionT&>(
        *decls.emplace_back(new FunctionT(*this, args...)));
  }

  template <typename FunctionT, typename... ArgsT>
  FunctionT& addFunction(ArgsT&&... args) {
    static_assert(std::is_base_of<FunctionBase, FunctionT>::value,
                  "Function type must be subclass of FunctionBase");
    return static_cast<FunctionT&>(
        *funcs.emplace_back(new FunctionT(*this, args...)));
  }

  template <typename GlobalT, typename... ArgsT>
  GlobalT& addGlobal(ArgsT&&... args) {
    static_assert(std::is_base_of<GlobalVariableBase, GlobalT>::value,
                  "Global type must be subclass of GlobalBase");
    return static_cast<GlobalT&>(
        *globals.emplace_back(new GlobalT(*this, args...)));
  }

  template <typename TypeT, typename... ArgsT>
  TypeT& addType(ArgsT&&... args) {
    static_assert(std::is_base_of<TypeBase, TypeT>::value,
                  "Type type must be subclass of TypeBase");
    return static_cast<TypeT&>(*types.emplace_back(new TypeT(*this, args...)));
  }

  SourceLang getSourceLang() const;

  bool hasDeclaration(llvm::Function&) const;
  FunctionBase& getDeclaration(llvm::Function&);
  FunctionBase& getDeclaration(llvm::Function*);
  const FunctionBase& getDeclaration(llvm::Function&) const;
  const FunctionBase& getDeclaration(llvm::Function*) const;

  void setLLVM(FunctionBase&, llvm::Function&);
  bool hasFunction(llvm::Function&) const;
  bool hasFunction(llvm::Function*) const;
  FunctionBase& getFunction(llvm::Function&);
  FunctionBase& getFunction(llvm::Function*);
  const FunctionBase& getFunction(llvm::Function&) const;
  const FunctionBase& getFunction(llvm::Function*) const;

  void setLLVM(GlobalVariableBase&, llvm::GlobalVariable&);
  bool hasGlobal(llvm::GlobalVariable&) const;
  bool hasGlobal(llvm::GlobalVariable*) const;
  GlobalVariableBase& getGlobal(llvm::GlobalVariable&);
  GlobalVariableBase& getGlobal(llvm::GlobalVariable*);
  const GlobalVariableBase& getGlobal(llvm::GlobalVariable&) const;
  const GlobalVariableBase& getGlobal(llvm::GlobalVariable*) const;

  void setLLVM(TypeBase&, llvm::Type*);
  bool hasType(llvm::Type*) const;
  TypeBase& getType(llvm::Type*);
  const TypeBase& getType(llvm::Type*) const;

  const Vector<Region>& getRegions() const;
  const Vector<std::unique_ptr<FunctionBase>>& getDeclarations() const;
  const Vector<std::unique_ptr<FunctionBase>>& getFunctions() const;
  const Vector<std::unique_ptr<GlobalVariableBase>>& getGlobals() const;
  const Vector<std::unique_ptr<TypeBase>>& getTypes() const;

  DirectiveContext& getDirectiveContext();
  const DirectiveContext& getDirectiveContext() const;

  void associate();
};

} // namespace moya

#endif // MOYA_FRONTEND_COMPONENTS_MODULE_BASE_H
