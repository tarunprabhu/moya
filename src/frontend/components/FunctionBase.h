#ifndef MOYA_FRONTEND_COMPONENTS_FUNCTION_BASE_H
#define MOYA_FRONTEND_COMPONENTS_FUNCTION_BASE_H

#include "ArgumentBase.h"
#include "common/Location.h"
#include "common/SourceLang.h"
#include "frontend/annotations/Directives.h"

#include <llvm/IR/Function.h>

#include <string>

namespace moya {

class ModuleBase;
class Serializer;

class FunctionBase {
protected:
  // The unqualified name of the function as it appears in the source code.
  std::string name;

  // If this is a compiler-generated function, the name set by Moya.
  std::string artificialName;

  // The model name if this function needs one
  std::string model;

  // Moya-specific annotations associated with this function
  Vector<const FunctionDirective*> directives;

  // beginLoc and loc are likely to be the same. At some point, maybe
  // beginLoc will only be valid if the function has a definition
  Location loc;
  Location beginLoc;
  Location endLoc;
  bool artificial;
  bool defined;
  llvm::Function* llvm;
  Vector<std::unique_ptr<ArgumentBase>> args;
  ModuleBase& module;

protected:
  FunctionBase(ModuleBase&);

public:
  FunctionBase(const FunctionBase&) = delete;
  virtual ~FunctionBase() = default;

  void setName(const std::string&);
  void setArtificialName(const std::string&);
  void setArtificial(bool);
  void setDefined(bool);
  void setLocation(const Location&);
  void setBeginLoc(const Location&);
  void setEndLoc(const Location&);
  void setModel(const std::string&);
  void setLLVM(llvm::Function&);
  void addDirective(const Directive*);
  template <typename ArgumentT, typename... ArgsT>
  ArgumentT& addArgument(ArgsT&&... params) {
    static_assert(std::is_base_of<ArgumentBase, ArgumentT>::value,
                  "Argument must be a subclass of ArgumentBase");
    return static_cast<ArgumentT&>(
        *args.emplace_back(new ArgumentT(*this, params...)));
  }

  ModuleBase& getModule();
  ArgumentBase& getArg(unsigned);
  ArgumentBase& getArg(const std::string&);

  const ModuleBase& getModule() const;
  SourceLang getSourceLang() const;
  llvm::Function& getLLVM() const;
  const std::string& getName() const;
  const std::string& getArtificialName() const;
  const std::string& getModel() const;
  const Location& getLocation() const;
  const Location& getBeginLoc() const;
  const Location& getEndLoc() const;
  uint64_t getNumArgs() const;
  bool hasArg(const std::string&) const;
  const ArgumentBase& getArg(unsigned) const;
  const ArgumentBase& getArg(const std::string&) const;

  const Vector<const FunctionDirective*>& getDirectives() const;
  template <typename T>
  const T* getDirective() const {
    for(const FunctionDirective* directive : directives)
      if(const T* ret = llvm::dyn_cast<T>(directive))
        return ret;
    return nullptr;
  }

  bool hasName() const;
  bool hasArtificialName() const;
  bool hasModel() const;
  bool hasLocation() const;
  bool hasBeginLoc() const;
  bool hasEndLoc() const;
  bool hasLLVM() const;
  template <typename T>
  bool hasDirective() const {
    for(const FunctionDirective* directive : directives)
      if(llvm::isa<T>(directive))
        return true;
    return false;
  }
  bool isArtificial() const;
  bool isDefined() const;

public:
  friend class ModuleBase;
};

} // namespace moya

#endif // MOYA_FRONTEND_COMPONENTS_FUNCTION_BASE_H
