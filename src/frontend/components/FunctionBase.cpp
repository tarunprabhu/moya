#include "FunctionBase.h"
#include "ArgumentBase.h"
#include "ModuleBase.h"
#include "common/Diagnostics.h"

namespace moya {

FunctionBase::FunctionBase(ModuleBase& module)
    : name(""), artificialName(""), model(""), artificial(false), defined(true),
      llvm(nullptr), module(module) {
  ;
}

bool FunctionBase::hasName() const {
  return name.length();
}

bool FunctionBase::hasArtificialName() const {
  return artificialName.length();
}

bool FunctionBase::hasModel() const {
  return model.length();
}

bool FunctionBase::hasLocation() const {
  return loc.isValid();
}

bool FunctionBase::hasBeginLoc() const {
  return beginLoc.isValid();
}

bool FunctionBase::hasEndLoc() const {
  return endLoc.isValid();
}

bool FunctionBase::hasLLVM() const {
  return llvm;
}

SourceLang FunctionBase::getSourceLang() const {
  return module.getSourceLang();
}

const std::string& FunctionBase::getName() const {
  return name;
}

const std::string& FunctionBase::getArtificialName() const {
  return artificialName;
}

const std::string& FunctionBase::getModel() const {
  return model;
}

const Location& FunctionBase::getLocation() const {
  return loc;
}

const Location& FunctionBase::getBeginLoc() const {
  return beginLoc;
}

const Location& FunctionBase::getEndLoc() const {
  return endLoc;
}

llvm::Function& FunctionBase::getLLVM() const {
  return *llvm;
}

const Vector<const FunctionDirective*>& FunctionBase::getDirectives() const {
  return directives;
}

void FunctionBase::setName(const std::string& name) {
  this->name = name;
}

void FunctionBase::setArtificialName(const std::string& name) {
  this->artificialName = name;
  this->artificial = name.length();
}

void FunctionBase::setModel(const std::string& model) {
  this->model = model;
}

void FunctionBase::setLocation(const Location& loc) {
  this->loc = loc;
}

void FunctionBase::setBeginLoc(const Location& loc) {
  this->beginLoc = loc;
}

void FunctionBase::setEndLoc(const Location& loc) {
  this->endLoc = loc;
}

void FunctionBase::setArtificial(bool artificial) {
  this->artificial = artificial;
}

void FunctionBase::setDefined(bool defined) {
  this->defined = defined;
}

void FunctionBase::setLLVM(llvm::Function& llvm) {
  this->llvm = &llvm;
  getModule().setLLVM(*this, llvm);
}

void FunctionBase::addDirective(const Directive* directive) {
  if(const auto* f = llvm::dyn_cast<FunctionDirective>(directive))
    directives.push_back(f);
  else
    moya_error("Unknown directive kind: " << directive->getKind());
}

ModuleBase& FunctionBase::getModule() {
  return module;
}

const ModuleBase& FunctionBase::getModule() const {
  return module;
}

bool FunctionBase::hasArg(const std::string& name) const {
  for(const auto& arg : args)
    if(arg->getName() == name)
      return true;
  return false;
}

ArgumentBase& FunctionBase::getArg(const std::string& name) {
  for(auto& arg : args)
    if(arg->getName() == name)
      return *arg;
  moya_error("No argument with name: " << name);

  // Lousy way to silence compiler warning about no argument being returned
  return *args[-1];
}

const ArgumentBase& FunctionBase::getArg(const std::string& name) const {
  for(const auto& arg : args)
    if(arg->getName() == name)
      return *arg;
  moya_error("No argument with name: " << name);

  // Lousy way to silence compiler warning about no argument being returned
  return *args[-1];
}

ArgumentBase& FunctionBase::getArg(unsigned i) {
  return *args[i];
}

const ArgumentBase& FunctionBase::getArg(unsigned i) const {
  return *args[i];
}

uint64_t FunctionBase::getNumArgs() const {
  return args.size();
}

bool FunctionBase::isArtificial() const {
  return artificial;
}

bool FunctionBase::isDefined() const {
  return defined;
}

} // namespace moya
