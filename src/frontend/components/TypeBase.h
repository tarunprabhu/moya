#ifndef MOYA_FRONTEND_COMPONENTS_TYPE_BASE_H
#define MOYA_FRONTEND_COMPONENTS_TYPE_BASE_H

#include "common/Location.h"
#include "common/SourceLang.h"

#include <llvm/IR/Type.h>

namespace moya {

class ModuleBase;

class TypeBase {
protected:
  llvm::Type* llvm;
  ModuleBase& module;
  Location loc;

protected:
  TypeBase(ModuleBase&);

public:
  TypeBase(const TypeBase&) = delete;
  virtual ~TypeBase() = default;

  void setLLVM(llvm::Type*);

  SourceLang getSourceLang() const;
  ModuleBase& getModule();
  const ModuleBase& getModule() const;
  const Location& getLocation() const;

  bool hasLLVM() const;
  llvm::Type* getLLVM() const;
  template <typename T>
  T* getLLVMAs() const {
    return llvm::dyn_cast<T>(llvm);
  }

  friend class ModuleBase;
};

} // namespace moya

#endif // MOYA_FRONTEND_COMPONENTS_TYPE_BASE_H
