#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Map.h"
#include "common/Metadata.h"
#include "common/Set.h"
#include "common/Trace.h"

#include <llvm/Analysis/MemoryBuiltins.h>
#include <llvm/Analysis/TargetLibraryInfo.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class InstrumentMallocsPass : public ModulePass {
public:
  static char ID;

protected:
  const moya::Set<StringRef> mallocLikeFunctions;

protected:
  bool isMallocLikeCall(Instruction*, const TargetLibraryInfo&);
  bool hasMalloc(Value* v);
  moya::Vector<Value*> getReturnedValues(Function& f);
  bool allReturnsMalloced(const moya::Vector<Value*>& fronts);
  bool allReturnsMalloced(const moya::Vector<std::unique_ptr<moya::Trace>>&);

  bool instrumentSystemMallocs(Module&);
  bool instrumentUserMallocs(Module&);

  void getUses(Value* v, moya::Set<Instruction*>& uses);
  moya::Set<Instruction*> getUses(Function* f);

  Type* getMallocedType(Instruction*, Type*);
  Type* getFlattenedElementType(Type*);

public:
  InstrumentMallocsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

InstrumentMallocsPass::InstrumentMallocsPass()
    : ModulePass(ID),
      mallocLikeFunctions({"__cxa_allocate_exception",
                           "aligned_alloc",
                           "memalign",
                           "hwloc_alloc",
                           "hwloc_alloc_membind",
                           "hwloc_alloc_membind_nodeset",
                           "hwloc_alloc_membind_policy",
                           "hwloc_alloc_membind_policy_nodeset"}) {
  llvm::initializeInstrumentMallocsPassPass(*PassRegistry::getPassRegistry());
}

StringRef InstrumentMallocsPass::getPassName() const {
  return "Moya Instrument Mallocs";
}

void InstrumentMallocsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
  AU.addRequired<TargetLibraryInfoWrapperPass>();
}

Type* InstrumentMallocsPass::getFlattenedElementType(Type* ty) {
  if(auto* aty = dyn_cast<ArrayType>(ty))
    return getFlattenedElementType(aty);
  return ty;
}

void InstrumentMallocsPass::getUses(Value* v, moya::Set<Instruction*>& uses) {
  for(Use& u : v->uses()) {
    User* user = u.getUser();
    if(auto* call = dyn_cast<CallInst>(user)) {
      if(not moya::Metadata::hasNoAnalyze(moya::LLVMUtils::getFunction(call)))
        uses.insert(call);
    } else if(auto* invoke = dyn_cast<InvokeInst>(user)) {
      if(not moya::Metadata::hasNoAnalyze(moya::LLVMUtils::getFunction(invoke)))
        uses.insert(invoke);
    } else if(auto* cst = dyn_cast<CastInst>(user)) {
      getUses(cst, uses);
    } else if(auto* cexpr = dyn_cast<ConstantExpr>(user)) {
      getUses(cexpr, uses);
    } else if(isa<ConstantArray>(user) or isa<ConstantDataArray>(user)) {
      ;
    } else {
      moya_error("Unexpected use of " << *v << ": " << *u.getUser());
    }
  }
}

moya::Set<Instruction*> InstrumentMallocsPass::getUses(Function* f) {
  moya::Set<Instruction*> uses;
  getUses(f, uses);
  return uses;
}

Type* InstrumentMallocsPass::getMallocedType(Instruction* inst, Type* def) {
  Type* pi8 = Type::getInt8PtrTy(inst->getContext());
  moya::Set<Type*> result;

  // The result of a malloc can be used in several ways:
  //
  // 1. An object is created and immediately cast to the right type.
  //    This is "easy" because we can look at the cast and construct the
  //    an object of the right type from that
  //
  // 2. If not, then we're either constructing
  //    a byte array or string, or we are in a malloc-wrapper. In either case,
  //    we construct a byte array object and return it.
  //
  //    NOTE: In the case of a malloc wrapper function, it would make sense to
  //    return null because the "correct" object will be recreated by the
  //    MutabilityAnalysis pass.
  //    However, the created pointer might still be used
  //    in the wrapper function and returning null would break that. So we
  //    end up wasting memory by creating an object which will not actually
  //    be used
  //

  // Iterating over the uses should take care of cases where one of several
  // different types of objects could be created
  moya::Vector<CastInst*> casts;
  moya::Vector<StoreInst*> stores;
  moya::Vector<Value*> others;
  for(Use& u : inst->uses())
    if(auto* cst = dyn_cast<BitCastInst>(u.getUser()))
      casts.push_back(cst);
    else if(auto* store = dyn_cast<StoreInst>(u.getUser()))
      stores.push_back(store);
    else
      others.push_back(u.getUser());

  // What is this all about?
  for(CastInst* cst : casts) {
    if(auto* pty = dyn_cast<PointerType>(cst->getDestTy())) {
      if(pty != pi8) {
        result.insert(getFlattenedElementType(pty->getElementType()));
      } else {
        if(auto* pty = dyn_cast<PointerType>(cst->getOperand(0)->getType()))
          if(auto* aty = dyn_cast<ArrayType>(pty->getElementType()))
            result.insert(moya::LLVMUtils::getFlattenedElementType(aty));
      }
    }
  }

  // C/C++
  //
  //    %malloced = call malloc(...)
  //    %cast = bitcast i8* to <type>*
  //    store %cast, <type>** %ptr
  //
  for(StoreInst* store : stores) {
    Value* dst = store->getPointerOperand();
    if(auto* gep0 = dyn_cast<GetElementPtrInst>(dst)) {
      result.insert(
          moya::LLVMUtils::getElementType(gep0->getResultElementType()));
    } else if(auto* g = dyn_cast<GlobalVariable>(dst)) {
      result.insert(
          moya::LLVMUtils::getElementType(g->getType()->getElementType()));
    } else {
      if(store->getValueOperand() == inst)
        result.insert(moya::LLVMUtils::getElementType(
            store->getValueOperand()->getType()));
      else
        result.insert(moya::LLVMUtils::getElementType(
            store->getPointerOperand()->getType()));
    }
  }

  for(Value* v : others)
    if(auto* ret = dyn_cast<ReturnInst>(v))
      if(auto* pty = dyn_cast<PointerType>(ret->getReturnValue()->getType()))
        if(auto* aty = dyn_cast<ArrayType>(pty->getElementType()))
          result.insert(moya::LLVMUtils::getFlattenedElementType(aty));

  // The returned value could just be passed as is to a function which accepts
  // a void*. That is just an optimization to avoid casting and recasting.
  // This also occurs in C++ where the object is automatically destructed
  // if an exception is raised. To avoid creating a "bogus" i8* object, we
  // only create it if an explicit cast or store does not exist. The latter
  // two are stronger indicators of the type being created.
  if(others.size() and (casts.size() == 0 and stores.size() == 0))
    result.insert(def);

  if(result.size() != 1) {
    std::string s;
    raw_string_ostream ss(s);
    ss << "Cannot determine unique object created at callsite: " << *inst
       << " from " << inst->getParent()->getParent()->getName() << "\n";
    ss << "Got {\n";
    for(Type* type : result)
      ss << " -- " << *type << "\n";
    ss << "}\n";
    ss << "casts: {\n";
    for(Instruction* inst : casts)
      ss << *inst << "\n";
    ss << "}\n";
    ss << "stores: {\n";
    for(Instruction* inst : stores)
      ss << *inst << "\n";
    ss << "}\n";
    ss << "others: {\n";
    for(Value* v : others)
      ss << *v << "\n";
    ss << "}\n";
    moya_error(ss.str());
  }

  return *result.begin();
}

bool InstrumentMallocsPass::isMallocLikeCall(Instruction* inst,
                                             const TargetLibraryInfo& tli) {
  if(isCallocLikeFn(inst, &tli, true) or isMallocLikeFn(inst, &tli, true))
    return true;
  return false;
}

bool InstrumentMallocsPass::instrumentSystemMallocs(Module& module) {
  bool changed = false;
  moya::Set<Function*> mallocs;
  TargetLibraryInfo& tli = getAnalysis<TargetLibraryInfoWrapperPass>().getTLI();
  Type* i8 = Type::getInt8Ty(module.getContext());

  for(Function& f : module.functions())
    if(not f.size() and mallocLikeFunctions.contains(f.getName()))
      mallocs.insert(&f);

  for(Function& f : module.functions())
    for(inst_iterator i = inst_begin(f); i != inst_end(f); i++)
      if(auto* call = dyn_cast<CallInst>(&*i)) {
        if(isMallocLikeCall(call, tli)) {
          Function* callee = moya::LLVMUtils::getCalledFunction(call);
          if(not callee->size())
            mallocs.insert(callee);
        }
      } else if(auto* call = dyn_cast<InvokeInst>(&*i)) {
        if(isMallocLikeCall(call, tli)) {
          Function* callee = moya::LLVMUtils::getCalledFunction(call);
          if(not callee->size())
            mallocs.insert(callee);
        }
      }

  for(Function* f : mallocs) {
    changed |= moya::Metadata::setSystemMalloc(f);
    for(Instruction* call : getUses(f))
      if(not moya::Metadata::hasAnalysisType(call))
        changed |= moya::Metadata::setAnalysisType(
            call, getMallocedType(call, i8)->getPointerTo());
  }

  return changed;
}

moya::Vector<Value*> InstrumentMallocsPass::getReturnedValues(Function& f) {
  moya::Vector<Value*> ret;
  for(BasicBlock& bb : f.getBasicBlockList())
    if(auto* r = dyn_cast<ReturnInst>(&bb.back()))
      ret.push_back(r);
  return ret;
}

bool InstrumentMallocsPass::hasMalloc(Value* v) {
  if(auto* call = dyn_cast<CallInst>(v)) {
    if(Function* f = moya::LLVMUtils::getCalledFunction(call))
      if(moya::Metadata::hasUserMalloc(f) or moya::Metadata::hasSystemMalloc(f))
        return true;
  } else if(auto* call = dyn_cast<InvokeInst>(v)) {
    if(Function* f = moya::LLVMUtils::getCalledFunction(call))
      if(moya::Metadata::hasUserMalloc(f) or moya::Metadata::hasSystemMalloc(f))
        return true;
  } else if(auto* f = dyn_cast<Function>(v)) {
    if(moya::Metadata::hasUserMalloc(f) or moya::Metadata::hasSystemMalloc(f))
      return true;
  }
  return false;
}

bool InstrumentMallocsPass::allReturnsMalloced(
    const moya::Vector<Value*>& fronts) {
  if(fronts.size() == 1) {
    if(hasMalloc(fronts[0]))
      return true;
  } else {
    for(Value* front : fronts)
      if(not(hasMalloc(front) or isa<ConstantPointerNull>(front)))
        return false;
    return true;
  }
  return false;
}

bool InstrumentMallocsPass::allReturnsMalloced(
    const moya::Vector<std::unique_ptr<moya::Trace>>& traces) {
  // If all the returned values originate from a malloc,
  // then it is likely a malloc wrapper. If the return type is not an i8*,
  // then it is a generator for objects of a specific type.
  for(const auto& trace : traces)
    if(allReturnsMalloced(trace->fronts()))
      return true;
  return false;
}

bool InstrumentMallocsPass::instrumentUserMallocs(Module& module) {
  bool changed = false;
  moya::Set<Function*> mallocs;

  moya::Map<Function*, moya::Vector<std::unique_ptr<moya::Trace>>> returns;
  for(Function& f : module.functions())
    if(f.size())
      if(f.getReturnType()->isPointerTy())
        for(Value* ret : getReturnedValues(f))
          returns[&f].emplace_back(new moya::Trace(ret));

  bool converged = false;
  while(not converged) {
    bool wrapper = false;

    for(const auto& it : returns)
      if(Function* f = it.first)
        if(not mallocs.contains(f))
          if(allReturnsMalloced(it.second))
            wrapper |= mallocs.insert(f);

    changed |= wrapper;
    converged = not wrapper;
  }

  for(Function* f : mallocs) {
    changed |= moya::Metadata::setUserMalloc(f);
    for(Instruction* call : getUses(f))
      if(not moya::Metadata::hasAnalysisType(call)) {
        // If t is null, then it likely means that the returned value was not
        // cast to another type. In this case, the returned type of the
        // function tells us the type being allocated
        if(Type* t = getMallocedType(call, nullptr))
          changed |= moya::Metadata::setAnalysisType(call, t->getPointerTo());
        else
          changed |= moya::Metadata::setAnalysisType(
              call, f->getFunctionType()->getReturnType());
      }
  }

  // We just mark all the values as being returned values and leave it to
  // later passes to use this information as they like
  for(auto& it : returns)
    if(moya::Metadata::hasUserMalloc(it.first))
      for(std::unique_ptr<moya::Trace>& trace : it.second)
        for(Value* front : trace->fronts())
          if(auto* inst = dyn_cast<Instruction>(front))
            changed |= moya::Metadata::setReturn(inst, nullptr);

  return changed;
}

bool InstrumentMallocsPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  changed |= instrumentSystemMallocs(module);
  changed |= instrumentUserMallocs(module);

  return changed;
}

char InstrumentMallocsPass::ID = 0;

static const char* name = "moya-instrument-mallocs";
static const char* descr = "Instruments dynamic memory allocation functions";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(InstrumentMallocsPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(TargetLibraryInfoWrapperPass)
INITIALIZE_PASS_END(InstrumentMallocsPass, name, descr, cfg, analysis)

void registerInstrumentMallocsPass(const PassManagerBuilder&,
                                   legacy::PassManagerBase& pm) {
  pm.add(new InstrumentMallocsPass());
}

Pass* createInstrumentMallocsPass() {
  return new InstrumentMallocsPass();
}
