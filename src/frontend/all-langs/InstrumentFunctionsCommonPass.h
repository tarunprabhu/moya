#ifndef MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_FUNCTIONS_COMMON_PASS
#define MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_FUNCTIONS_COMMON_PASS

#include "common/Map.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

class InstrumentFunctionsCommonPass : public llvm::ModulePass {
public:
  static char ID;

protected:
  moya::Map<llvm::StringRef, llvm::StringRef> llvmFuncs;

protected:
  bool isLLVMFunction(const llvm::Function&);
  bool isLLVMDebugFunction(const llvm::Function&);
  llvm::StringRef getLLVMModel(const llvm::Function&);

  bool isMemcpyFunction(const llvm::Function&);
  bool isMemsetFunction(const llvm::Function&);

public:
  InstrumentFunctionsCommonPass();

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);
};

#endif // MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_FUNCTIONS_COMMON_PASS
