#ifndef MOYA_FRONTEND_ALL_LANGS_GENERATE_REDIRECT_IMPL_BASE_H
#define MOYA_FRONTEND_ALL_LANGS_GENERATE_REDIRECT_IMPL_BASE_H

#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Type.h>

#include "common/APITypes.h"
#include "common/Map.h"
#include "common/Set.h"

class GenerateRedirectImplBase {
protected:
  llvm::LLVMContext& context;
  llvm::Module& module;
  llvm::IRBuilder<> builder;

  // The original function
  llvm::Function* src;

  // The redirecter function being built
  llvm::Function* f;

  // JIT ID of the redirecter function
  llvm::Constant* id;

  // Pointer to the JIT'ed function that will be executed
  llvm::Value* fnptr;

  // Returned value from call to the JIT'ed function
  llvm::Value* ret;

protected:
  // Register all the function arguments
  virtual void insertRegisterArgs() = 0;

  // (Maybe) compile the function and return a pointer to the compiled function
  virtual void insertCompile();

  // Execute the (possibly) JIT'ed code
  virtual void insertExecute();

  // Insert a return instruction
  virtual void insertReturn();

public:
  GenerateRedirectImplBase(llvm::Function&);

  virtual bool run();
};

#endif // MOYA_FRONTEND_ALL_LANGS_GENERATE_REDIRECT_IMPL_BASE_H
