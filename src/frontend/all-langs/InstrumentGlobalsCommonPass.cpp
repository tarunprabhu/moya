#include "InstrumentGlobalsCommonPass.h"
#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/IR/GlobalVariable.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

InstrumentGlobalsCommonPass::InstrumentGlobalsCommonPass() : ModulePass(ID) {
  llvm::initializeInstrumentGlobalsCommonPassPass(
      *PassRegistry::getPassRegistry());
}

StringRef InstrumentGlobalsCommonPass::getPassName() const {
  return "Moya Instrument Globals (All languages)";
}

void InstrumentGlobalsCommonPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool InstrumentGlobalsCommonPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  for(GlobalVariable& g : module.globals()) {
    if(not g.hasPrivateLinkage())
      changed |= moya::Metadata::setLinkage(g, g.getLinkage());

    // Arrays of scalars and structs will always be flattened
    if(not moya::Metadata::hasAnalysisType(g)) {
      if(auto* aty = dyn_cast<ArrayType>(g.getType()->getElementType())) {
        Type* ety = moya::LLVMUtils::getFlattenedElementType(aty);
        if(moya::LLVMUtils::isScalarTy(ety)
           or moya::LLVMUtils::isStructTy(ety)) {
          const Module& module = *moya::LLVMUtils::getModule(g);
          Type* type = moya::LLVMUtils::getCanonicalType(
              moya::LLVMUtils::getFlattened(aty), module);
          changed |= moya::Metadata::setTruncated(g);
          changed |= moya::Metadata::setAnalysisType(g, type->getPointerTo());
        }
      }
    }
  }

  return changed;
}

char InstrumentGlobalsCommonPass::ID = 0;

static const char* name = "moya-instr-globals";
static const char* descr = "Adds metadata to globals (all langauges)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(InstrumentGlobalsCommonPass, name, descr, cfg, analysis)

Pass* createInstrumentGlobalsCommonPass() {
  return new InstrumentGlobalsCommonPass();
}

void registerInstrumentGlobalsCommonPass(const PassManagerBuilder&,
                                         legacy::PassManagerBase& pm) {
  pm.add(new InstrumentGlobalsCommonPass());
}
