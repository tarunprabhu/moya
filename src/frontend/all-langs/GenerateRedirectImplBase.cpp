#include "GenerateRedirectImplBase.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/IR/Module.h>
#include <llvm/Support/raw_ostream.h>

using namespace llvm;

GenerateRedirectImplBase::GenerateRedirectImplBase(Function& orig)
    : context(orig.getContext()), module(*orig.getParent()), builder(context),
      src(nullptr), f(nullptr), id(nullptr), fnptr(nullptr), ret(nullptr) {
  id = moya::LLVMUtils::getJITIDConstant(context,
                                         moya::Metadata::getJITID(orig));

  src = &orig;

  std::string fname = moya::Metadata::getRedirecter(orig);
  FunctionType* fty = orig.getFunctionType();
  f = dyn_cast<Function>(module.getOrInsertFunction(fname, fty));

  // Copy argument names and Moya attributes
  for(Argument& origArg : orig.args()) {
    unsigned argno = origArg.getArgNo();
    Argument& arg = *moya::LLVMUtils::getArgument(f, argno);

    if(origArg.hasName())
      arg.setName(origArg.getName());

    // FIXME: Find a way to copy all Moya attributes automatically
    if(moya::Metadata::hasIgnore(origArg))
      moya::Metadata::setIgnore(arg);
    if(moya::Metadata::hasForce(origArg))
      moya::Metadata::setForce(arg);
    if(moya::Metadata::hasFixedArray(origArg))
      moya::Metadata::setFixedArray(arg,
                                    moya::Metadata::getFixedArray(origArg));
    if(moya::Metadata::hasDynArray(origArg))
      moya::Metadata::setDynArray(arg, moya::Metadata::getDynArray(origArg));
    if(moya::Metadata::hasAnalysisType(origArg))
      moya::Metadata::setAnalysisType(arg,
                                      moya::Metadata::getAnalysisType(origArg));
  }

  // Copy any other attributes from the original function to the redirecter
  f->copyAttributesFrom(&orig);

  // This will have copied all the attributes including all the Moya
  // attributes which we should remove
  moya::Metadata::resetSpecialize(f);
  moya::Metadata::resetMemSafe(f);
}

void GenerateRedirectImplBase::insertCompile() {
  Function* compileFn = moya::LLVMUtils::getCompileFunction(module);
  fnptr = builder.CreateCall(compileFn, {id});
}

void GenerateRedirectImplBase::insertExecute() {
  moya::Vector<Value*> args;
  for(Argument& arg : f->args())
    args.push_back(&arg);

  Type* ftype = f->getFunctionType();
  Value* fn = builder.CreateBitOrPointerCast(fnptr, ftype->getPointerTo());
  ret = builder.CreateCall(fn, moya::LLVMUtils::makeArrayRef(args));
}

void GenerateRedirectImplBase::insertReturn() {
  if(f->getReturnType()->isVoidTy())
    builder.CreateRetVoid();
  else
    builder.CreateRet(ret);
}

bool GenerateRedirectImplBase::run() {
  BasicBlock* bbExit = BasicBlock::Create(context, "", f);
  BasicBlock* bbExecute = BasicBlock::Create(context, "", f, bbExit);
  BasicBlock* bbCompile = BasicBlock::Create(context, "", f, bbExecute);
  BasicBlock* bbEntry = BasicBlock::Create(context, "", f, bbCompile);

  // Register function arguments
  builder.SetInsertPoint(bbEntry);
  insertRegisterArgs();
  builder.CreateBr(bbCompile);

  // Get the function to execute
  builder.SetInsertPoint(bbCompile);
  insertCompile();
  builder.CreateBr(bbExecute);

  // Execute the function
  builder.SetInsertPoint(bbExecute);
  insertExecute();
  builder.CreateBr(bbExit);

  // Done
  builder.SetInsertPoint(bbExit);
  insertReturn();

  return true;
}
