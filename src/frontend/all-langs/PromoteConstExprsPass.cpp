#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/HashSet.h"
#include "common/LLVMUtils.h"
#include "common/Map.h"
#include "common/Set.h"

#include <llvm/IR/CFG.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

// Promotes all constant expressions to instructions. This makes it
// simplify types pass easier
class PromoteConstExprsPass : public ModulePass {
private:
public:
  static char ID;

public:
  PromoteConstExprsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

static BasicBlock* getBasicBlockForValue(PHINode* phi, Value* v) {
  for(unsigned i = 0; i < phi->getNumIncomingValues(); i++)
    if(phi->getIncomingValue(i) == v)
      return phi->getIncomingBlock(i);
  return nullptr;
}

PromoteConstExprsPass::PromoteConstExprsPass() : ModulePass(ID) {
  llvm::initializePromoteConstExprsPassPass(*PassRegistry::getPassRegistry());
}

StringRef PromoteConstExprsPass::getPassName() const {
  return "Moya Promote Constant Exprs";
}

void PromoteConstExprsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool PromoteConstExprsPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  moya::Map<Instruction*, ConstantExpr*> cexprs;
  moya::Map<Instruction*, std::pair<Instruction*, unsigned>> insert;
  do {
    insert.clear();
    for(Function& f : module.functions()) {
      for(inst_iterator i = inst_begin(f); i != inst_end(f); i++) {
        for(unsigned j = 0; j < i->getNumOperands(); j++) {
          if(auto* c = dyn_cast<ConstantExpr>(i->getOperand(j))) {
            // If this is a landing pad instruction, then it expects a constant
            // operand. So we don't replace the constant expression in it
            if(not isa<LandingPadInst>(&*i)) {
              Instruction* instr = c->getAsInstruction();
              insert[instr] = std::make_pair(&*i, j);
              cexprs[instr] = c;
            }
          }
        }
      }
    }

    for(auto it : insert) {
      Instruction* ins = it.first;
      ConstantExpr* cexpr = cexprs[ins];
      Instruction* at = it.second.first;
      unsigned op = it.second.second;
      // If this is a PHI node, we cannot insert the instruction before the
      // instruction. Instead, we insert it at the end of thge basic blocks
      // from which we can reach this PHI node
      if(PHINode* phi = dyn_cast<PHINode>(at)) {
        BasicBlock* block = getBasicBlockForValue(phi, cexpr);
        Instruction* terminator = block->getTerminator();
        ins->insertBefore(terminator);
      } else {
        ins->insertBefore(at);
      }
      at->setOperand(op, ins);
      changed |= true;
    }
  } while(insert.size() > 0);

  // Contains all the cexprs deleted so far. We need this because the map
  // is keyed by the instruction and there could be more than one instruction
  // per constexpr.
  moya::HashSet<ConstantExpr*> destroyed;

  // The while loop deletes the cexprs in the correct order i.e. bottom-up
  // in terms of uses.
  unsigned long oldsize = ULONG_MAX;
  do {
    // If nothing could be deleted, there's a cycle and we break.
    // It might be impossible to break these because you could have constexprs
    // which are used to define other constexprs, and those cannot be deleted
    if(oldsize == cexprs.size())
      break;
    oldsize = cexprs.size();
    moya::HashSet<Instruction*> processed;
    for(auto it : cexprs) {
      ConstantExpr* cexpr = it.second;
      Instruction* instr = it.first;
      if(cexpr->getNumUses() == 0)
        processed.insert(instr);
    }
    for(Instruction* instr : processed) {
      ConstantExpr* cexpr = cexprs.at(instr);
      if(not destroyed.contains(cexpr)) {
        cexpr->destroyConstant();
        destroyed.insert(cexpr);
      }
      cexprs.erase(instr);
    }
  } while(cexprs.size());

  return changed;
}

char PromoteConstExprsPass::ID = 0;

static const char* name = "moya-promote-constexprs";
static const char* descr = "Removes the bitcast constexpr from function calls";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(PromoteConstExprsPass, name, descr, cfg, analysis);

void registerPromoteConstExprsPass(const PassManagerBuilder&,
                                   legacy::PassManagerBase& pm) {
  pm.add(new PromoteConstExprsPass());
}

Pass* createPromoteConstExprsPass() {
  return new PromoteConstExprsPass();
}
