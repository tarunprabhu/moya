#include "InstrumentFunctionsCommonPass.h"
#include "Passes.h"
#include "common/AerynConfig.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "common/StringUtils.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Intrinsics.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

InstrumentFunctionsCommonPass::InstrumentFunctionsCommonPass()
    : ModulePass(ID) {
  llvmFuncs = {{"llvm.lifetime.end", "llvm.lifetime.end"},
               {"llvm.lifetime.start", "llvm.lifetime.start"},
               {"llvm.dbg.declare", "llvm.dbg.declare"},
               {"llvm.dbg.value", "llvm.dbg.value"},
               {"llvm.memcpy", "llvm.memcpy"},
               {"llvm.memset", "llvm.memset"},
               {"llvm.memmove", "llvm.memmove"},
               {"llvm.stacksave", "llvm.stacksave"},
               {"llvm.stackrestore", "llvm.stackrestore"},
               {"llvm.prefetch", "llvm.prefetch"},
               {"llvm.va_start", "llvm.va_start"},
               {"llvm.va_end", "llvm.va_end"},
               {"llvm.va_copy", "llvm.va_copy"},
               {"llvm.eh.typeid.for", "llvm.eh.typeid.for"},
               {"llvm.eh.begincatch", "llvm.eh.begincatch"},
               {"llvm.eh.endcatch", "llvm.eh.endcatch"},
               {"llvm.eh.exceptionpointer", "llvm.eh.exceptionpointer"},
               {"llvm.eh.sjlj.setjmp", "llvm.eh.sjlj.setjmp"},
               {"llvm.eh.sjlj.longjmp", "llvm.eh.sjlj.longjmp"},
               {"llvm.eh.sjlj.lsda", "llvm.eh.sjlj.lsda"},
               {"llvm.eh.sjlj.callsite", "llvm.eh.sjlj.callsite"},
               {"llvm.init.trampoline", "llvm.init.trampoline"},
               {"llvm.adjust.trampoline", "llvm.adjust.trampoline"}};
  llvm::initializeInstrumentFunctionsCommonPassPass(
      *PassRegistry::getPassRegistry());
}

StringRef InstrumentFunctionsCommonPass::getPassName() const {
  return "Moya Instrument Functions (All languages)";
}

void InstrumentFunctionsCommonPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool InstrumentFunctionsCommonPass::isLLVMDebugFunction(const Function& f) {
  return moya::StringUtils::startswith(f.getName(), "llvm.dbg");
}

bool InstrumentFunctionsCommonPass::isLLVMFunction(const Function& f) {
  for(const auto& llvm : llvmFuncs)
    if(f.getName().find(llvm.first) == 0)
      return true;
  return false;
}

llvm::StringRef InstrumentFunctionsCommonPass::getLLVMModel(const Function& f) {
  for(const auto& llvm : llvmFuncs)
    if(f.getName().find(llvm.first) == 0)
      return llvm.second;
  return "";
}

bool InstrumentFunctionsCommonPass::isMemcpyFunction(const Function& f) {
  if(f.isIntrinsic())
    if((f.getIntrinsicID() == llvm::Intrinsic::ID::memcpy)
       or (f.getIntrinsicID() == llvm::Intrinsic::ID::memmove))
      return true;
  return false;
}

bool InstrumentFunctionsCommonPass::isMemsetFunction(const Function& f) {
  if(f.isIntrinsic())
    if(f.getIntrinsicID() == llvm::Intrinsic::ID::memset)
      return true;
  return false;
}

bool InstrumentFunctionsCommonPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  // These show up here because it could be a file in some other language
  // which still includes headers that might have C++ code in it.
  if(GlobalVariable* g = module.getNamedGlobal("llvm.global_ctors"))
    if(auto* carray = dyn_cast<ConstantArray>(g->getInitializer()))
      for(Value* op : carray->operands())
        if(auto* cstruct = dyn_cast<ConstantStruct>(op))
          if(auto* f = dyn_cast<Function>(cstruct->getOperand(1)))
            changed |= moya::Metadata::setGlobalConstructor(f);

  if(GlobalVariable* g = module.getNamedGlobal("llvm.global_dtors"))
    if(auto* carray = dyn_cast<ConstantArray>(g->getInitializer()))
      for(Value* op : carray->operands())
        if(auto* cstruct = dyn_cast<ConstantStruct>(op))
          if(auto* f = dyn_cast<Function>(cstruct->getOperand(1)))
            changed |= moya::Metadata::setGlobalDestructor(f);

  for(Function& f : module.functions()) {
    if(not f.size()) {
      if(isLLVMFunction(f)) {
        changed |= moya::Metadata::setLLVM(f);
        changed |= moya::Metadata::setModel(f, getLLVMModel(f));
      }

      if(isLLVMDebugFunction(f)) {
        changed |= moya::Metadata::setLLVMDebug(f);
        changed |= moya::Metadata::setNoAnalyze(f);
      }

      if(isMemcpyFunction(f))
        changed |= moya::Metadata::setSystemMemcpy(f);

      if(isMemsetFunction(f))
        changed |= moya::Metadata::setSystemMemset(f);

      if(moya::LLVMUtils::isPureFunction(f)) {
        changed |= moya::Metadata::setPure(f);
        changed |= moya::Metadata::setModel(
            f, moya::AerynConfig::getPureFunctionModel());
      }
    } else {
      changed |= moya::Metadata::setLinkage(f, f.getLinkage());
    }
  }

  return changed;
}

char InstrumentFunctionsCommonPass::ID = 0;

static const char* name = "moya-instr-funcs";
static const char* descr = "Adds metadata to functions (all languages)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(InstrumentFunctionsCommonPass, name, descr, cfg, analysis)

Pass* createInstrumentFunctionsCommonPass() {
  return new InstrumentFunctionsCommonPass();
}

void registerInstrumentFunctionsCommonPass(const PassManagerBuilder&,
                                           legacy::PassManagerBase& pm) {
  pm.add(new InstrumentFunctionsCommonPass());
}
