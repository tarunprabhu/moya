#include "InstrumentUtils.h"
#include "Passes.h"
#include "common/APIDecls.h"
#include "common/Config.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/ADT/SmallVector.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/Transforms/Utils/UnifyFunctionExitNodes.h>

using namespace llvm;

// I keep getting rid of this but it keeps coming back. Ridiculous!
//
class JITRedirectPass : public ModulePass {
protected:
  bool generateRedirect(Function&);

public:
  JITRedirectPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);

public:
  static char ID;
};

JITRedirectPass::JITRedirectPass() : ModulePass(ID) {
  initializeJITRedirectPassPass(*PassRegistry::getPassRegistry());
}

StringRef JITRedirectPass::getPassName() const {
  return "Moya Redirect Pass";
}

void JITRedirectPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<UnifyFunctionExitNodes>();
}

bool JITRedirectPass::generateRedirect(Function& f) {
  Module& module = *moya::LLVMUtils::getModule(f);
  LLVMContext& context = module.getContext();
  IntegerType* i32 = Type::getInt32Ty(context);
  ConstantInt* zero = ConstantInt::get(i32, 0);
  FunctionType* fty = f.getFunctionType();

  // Just create the basic blocks that we need at the start of the function
  // and insert everything in there. Subsequent passes will clean these up
  BasicBlock* bbFirst = &*f.begin();
  BasicBlock* bbRedirect
      = BasicBlock::Create(context, ".moya.redirect", &f, bbFirst);
  BasicBlock* bbCheck
      = BasicBlock::Create(context, ".moya.check", &f, bbRedirect);

  // Instruction* first = moya::InstrumentUtils::getFirstNonAllocaInst(f);
  // BasicBlock* bbFirst = first->getParent();

  // // Split the basic block containing the if
  // BasicBlock* bbCheck = bbFirst->splitBasicBlock(first);
  // // At this point bbCheck still contains other instructions starting with
  // // the first non-alloca instruction. Splitting it
  // // again will empty it out and move all subsequent instructions into a new
  // // basic block.
  // BasicBlock* bbRedirect = bbCheck->splitBasicBlock(first);
  // // Again, bbRedirect still contains other instructions starting with the
  // // first non-alloca instruction. As before, splitting it will empty it
  // BasicBlock* bbRest = bbRedirect->splitBasicBlock(first);

  // Create the basic block where the call to the redirect function will take
  // place.
  // BasicBlock* bbRedirect = BasicBlock::Create(context, "", &f, bbRest);
  std::string rname = moya::Metadata::getRedirecter(f);
  auto* redirect = dyn_cast<Function>(module.getOrInsertFunction(rname, fty));

  SmallVector<Value*, 16> args;
  for(Argument& arg : f.args())
    args.push_back(&arg);

  Value* retVal = CallInst::Create(redirect, args, "", bbRedirect);
  if(fty->getReturnType()->isVoidTy())
    ReturnInst::Create(context, bbRedirect);
  else
    ReturnInst::Create(context, retVal, bbRedirect);

  // Create a basic block in which the test for whether we are in a JIT
  // region will be performed
  // BasicBlock* bbCheck = BasicBlock::Create(context, "", &f, bbRedirect);
  Function* fInRegion
      = module.getFunction(moya::Config::getIsInRegionFunctionName());
  Value* vInRegion = CallInst::Create(fInRegion, {}, "", bbCheck);
  Value* cmp = CmpInst::Create(Instruction::OtherOps::ICmp,
                               CmpInst::Predicate::ICMP_NE,
                               vInRegion,
                               zero,
                               "",
                               bbCheck);
  BranchInst::Create(bbRedirect, bbFirst, cmp, bbCheck);

  return true;
}

bool JITRedirectPass::runOnModule(Module& module) {
  bool changed = false;

  for(Function& f : module.functions())
    if(moya::Metadata::hasSpecialize(f))
      changed |= generateRedirect(f);

  return changed;
}

char JITRedirectPass::ID = 0;

static const char* name = "moya-redirect";
static const char* descr = "Generate calls to redirecters for JIT'ed functions";
static const bool cfg = false;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(JITRedirectPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(UnifyFunctionExitNodes)
INITIALIZE_PASS_END(JITRedirectPass, name, descr, cfg, analysis)

void registerJITRedirectPass(const PassManagerBuilder&,
                             legacy::PassManagerBase& pm) {
  pm.add(new JITRedirectPass());
}

Pass* createJITRedirectPass() {
  return new JITRedirectPass();
}
