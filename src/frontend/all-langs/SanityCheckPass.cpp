#include "Passes.h"
#include "common/AerynConfig.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

// This pass runs some sanity checks before the analysis to make sure that
// all constraints are satisfied (for instance, |regions| < |maxregions|)
class SanityCheckPass : public ModulePass {
public:
  static char ID;

public:
  SanityCheckPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

SanityCheckPass::SanityCheckPass() : ModulePass(ID) {
  llvm::initializeSanityCheckPassPass(*PassRegistry::getPassRegistry());
}

StringRef SanityCheckPass::getPassName() const {
  return "Moya Sanity Check";
}

void SanityCheckPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesAll();
}

bool SanityCheckPass::runOnModule(Module& module) {
  moya_message(getPassName());

  unsigned long regions = moya::Metadata::getRegions(module).size();
  if(regions > moya::AerynConfig::getMaxJITRegions())
    moya_error("Exceeded maximum supported regions"
               << "(" << moya::AerynConfig::getMaxJITRegions() << "). Got "
               << regions << "\n");

  for(Function& f : module.functions()) {
    if(moya::Metadata::hasSpecialize(f)) {
      for(Argument& arg : f.args()) {
        bool ignore = moya::Metadata::hasIgnore(arg);
        bool force = moya::Metadata::hasForce(arg);
        if(ignore and force)
          moya_error("Function cannot have both ignore and force attributes: "
                     << f.getName());
        if(moya::Metadata::hasFixedArray(arg)) {
          if(not arg.getType()->isPointerTy())
            moya_error("Argument with array attribute must be pointer type:\n"
                       << "   arg: " << moya::Metadata::getSourceName(arg)
                       << "  func: " << moya::Metadata::getSourceName(f));
          if(auto* pty = dyn_cast<PointerType>(arg.getType()))
            if(not moya::LLVMUtils::isScalarTy(pty->getElementType()))
              moya_warning(
                  "Argument is not pointer to scalar type. Array "
                  "annotation will be ignored\n"
                  << "   arg: " << moya::Metadata::getSourceName(arg) << "\n"
                  << "  func: " << moya::Metadata::getSourceName(f));
        }
      }
    }
  }

  return false;
}

char SanityCheckPass::ID = 0;

static const char* name = "moya-sanity-check";
static const char* descr
    = "Run Moya-specific sanity checks before the main analysis";
static const bool cfg = true;
static const bool analysis = true;

INITIALIZE_PASS(SanityCheckPass, name, descr, cfg, analysis);

Pass* createSanityCheckPass() {
  return new SanityCheckPass();
}

void registerSanityCheckPass(const PassManagerBuilder&,
                             legacy::PassManagerBase& pm) {
  pm.add(new SanityCheckPass());
}
