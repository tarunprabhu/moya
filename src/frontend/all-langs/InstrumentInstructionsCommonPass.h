#ifndef MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_INSTRUCTIONS_COMMON_PASS_H
#define MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_INSTRUCTIONS_COMMON_PASS_H

#include <llvm/IR/Instruction.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>

class InstrumentInstructionsCommonPass : public llvm::ModulePass {
public:
  static char ID;

protected:
  template <typename T>
  bool instrumentIgnore(T*);
  template <typename T>
  bool instrumentAsmCall(T*);
  template <typename T>
  bool instrumentMemcpy(T*);
  template <typename T>
  bool instrumentMemset(T*);
  bool instrumentRealloc(llvm::IntToPtrInst*);
  bool instrumentAlloca(llvm::CallInst*);
  bool instrument(llvm::GetElementPtrInst*);
  bool instrument(llvm::AllocaInst*);
  bool instrument(llvm::PHINode*);

public:
  InstrumentInstructionsCommonPass();

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);
};

#endif // MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_INSTRUCTIONS_COMMON_PASS_H
