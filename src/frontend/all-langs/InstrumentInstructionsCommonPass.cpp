#include "InstrumentInstructionsCommonPass.h"
#include "Passes.h"
#include "common/AerynConfig.h"
#include "common/ArgumentUtils.h"
#include "common/ClassHeirarchies.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/IR/DebugInfoMetadata.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/InlineAsm.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Metadata.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

InstrumentInstructionsCommonPass::InstrumentInstructionsCommonPass()
    : ModulePass(ID) {
  llvm::initializeInstrumentInstructionsCommonPassPass(
      *PassRegistry::getPassRegistry());
}

StringRef InstrumentInstructionsCommonPass::getPassName() const {
  return "Moya Instrument Instructions (All languages)";
}

void InstrumentInstructionsCommonPass::getAnalysisUsage(
    AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

template <typename T>
bool InstrumentInstructionsCommonPass::instrumentIgnore(T* call) {
  bool changed = false;

  if(Function* f = moya::LLVMUtils::getCalledFunction(call))
    if(moya::Metadata::hasLLVMDebug(f))
      changed |= moya::Metadata::setIgnore(call);

  return changed;
}

template <typename T>
bool InstrumentInstructionsCommonPass::instrumentAsmCall(T* call) {
  bool changed = false;

  if(auto* asmCode = dyn_cast<InlineAsm>(call->getCalledValue()))
    for(const InlineAsm::ConstraintInfo& c : asmCode->ParseConstraints())
      if(c.Type == InlineAsm::ConstraintPrefix::isClobber)
        for(const std::string& s : c.Codes)
          if(s == "{memory}")
            changed |= moya::Metadata::setAsmClobbersMemory(call);

  return changed;
}

template <typename T>
bool InstrumentInstructionsCommonPass::instrumentMemcpy(T* call) {
  bool changed = false;

  if(Function* f = moya::LLVMUtils::getCalledFunction(call))
    if(moya::Metadata::hasSystemMemcpy(f))
      changed |= moya::Metadata::setCallMemcpy(
          call, moya::LLVMUtils::getAnalysisType(call->getArgOperand(0)));

  return changed;
}

template <typename T>
bool InstrumentInstructionsCommonPass::instrumentMemset(T* call) {
  bool changed = false;

  if(Function* f = moya::LLVMUtils::getCalledFunction(call))
    if(moya::Metadata::hasSystemMemset(f))
      changed |= moya::Metadata::setCallMemset(
          call, moya::LLVMUtils::getAnalysisType(call->getArgOperand(0)));

  return changed;
}

// There is a bizarre issue with realloc() where the declaration
// of the function in the IR seems to be the reverse of the declaration of
// the function in libc:
//
// libc: realloc(void*, size_t)
// LLVM: realloc(i64, i8*)
//
// In this case, with LLVM > 3.6, we see inttoptr's there and since
// realloc is effectively a NOP (execpt for marking the pointers as being
// used), we don't really want to go to TOP
//
bool InstrumentInstructionsCommonPass::instrumentRealloc(
    IntToPtrInst* inttoptr) {
  bool changed = false;

  bool isRealloc = true;
  for(Use& u : inttoptr->uses()) {
    if(auto* call = dyn_cast<CallInst>(u.getUser()))
      if(Function* f = moya::LLVMUtils::getCalledFunction(call))
        if(f->getName() == "realloc") {
          isRealloc &= true;
          continue;
        }
    isRealloc = false;
  }

  if(isRealloc)
    changed |= moya::Metadata::setIgnore(inttoptr);

  return changed;
}

// If this is a call to LLVM debug metadata, pull the data out of there
// and put it in our Metadata. There is a lot of wheel reinvention here.
// Thanks a lot, Fortran!
bool InstrumentInstructionsCommonPass::instrumentAlloca(CallInst* call) {
  bool changed = false;

  if(const llvm::Function* fn = moya::LLVMUtils::getCalledFunction(call)) {
    if(fn->getName() == "llvm.dbg.declare") {
      auto* mdv = cast<llvm::MetadataAsValue>(call->getArgOperand(0));
      auto* vmd = cast<llvm::ValueAsMetadata>(mdv->getMetadata());
      if(auto* alloca = dyn_cast<llvm::AllocaInst>(vmd->getValue())) {
        changed |= moya::Metadata::setSourceLocation(
            alloca, moya::LLVMUtils::getLocation(call->getDebugLoc()));
        if(const auto* di = dyn_cast<DILocalVariable>(
               cast<llvm::MetadataAsValue>(call->getArgOperand(1))
                   ->getMetadata())) {
          // FIXME: This is a shitty hack for Flang
          // Basically, the instrument instructions path will have set the
          // source name but it's not necessarily correct
          if(moya::Metadata::hasSourceName(alloca))
            moya::Metadata::resetSourceName(alloca);
          if(di->getName().size())
            changed |= moya::Metadata::setSourceName(alloca, di->getName());
        }
      }
    }
  }

  return changed;
}

bool InstrumentInstructionsCommonPass::instrument(GetElementPtrInst* gep) {
  bool changed = false;

  if(gep->hasAllConstantIndices()) {
    Type* ptrTy = gep->getPointerOperandType();
    if(gep->getNumIndices() == 1) {
      // If we have an array of bytes, we can't be sure if it is looking up
      // an array of bytes/characters or if it is the result of a struct
      // being cast to bytes and then having a field accessed
      if(moya::LLVMUtils::isPtrToScalarTy(ptrTy)) {
        if(not moya::LLVMUtils::isPtrToInt8Ty(ptrTy))
          changed |= moya::Metadata::setGEPIndex(gep, 0);
      } else if(moya::LLVMUtils::isPtrToStructTy(ptrTy)) {
        changed |= moya::Metadata::setGEPIndex(gep, 0);
      }
    } else if(gep->getNumIndices() == 2) {
      if(moya::LLVMUtils::isPtrToStructTy(gep->getPointerOperandType()))
        changed |= moya::Metadata::setGEPFieldLookup(gep);
    }
  }

  return changed;
}

bool InstrumentInstructionsCommonPass::instrument(AllocaInst* alloca) {
  bool changed = false;

  // Arrays of scalars and structs will always be flattened
  if(not moya::Metadata::hasAnalysisType(alloca)) {
    if(auto* aty = dyn_cast<ArrayType>(alloca->getAllocatedType())) {
      Type* ety = moya::LLVMUtils::getFlattenedElementType(aty);
      if(moya::LLVMUtils::isScalarTy(ety) or moya::LLVMUtils::isStructTy(ety)) {
        const Module& module = *moya::LLVMUtils::getModule(alloca);
        Type* allocated = moya::LLVMUtils::getCanonicalType(
            moya::LLVMUtils::getFlattened(aty), module);

        // FIXME: This is a really shitty hack!
        // There are situations where LLVM allocates a 0-length array on the
        // stack I haven't the foggiest what this means or why it happens, but
        // it seems to happen mostly in templated functions. In any case, a
        // consequence of this seems to be that GEP functions that access these
        // allocas end up with an extra index that causes the type checker to
        // fail because the canonicalized type of [0 x type] is type.
        //
        if(aty->getNumElements() == 0)
          allocated
              = ArrayType::get(allocated, moya::AerynConfig::getArrayLength());

        changed |= moya::Metadata::setTruncated(alloca);
        changed |= moya::Metadata::setAnalysisType(alloca,
                                                   allocated->getPointerTo());
      }
    }
  }

  // Save the alloca name in case LLVM decides that it's not worth keeping
  // around and gets rid of it later
  if(alloca->hasName())
    changed |= moya::Metadata::setSourceName(alloca, alloca->getName());

  return changed;
}

static bool isLoopVariant(Value* v) {
  if(isa<PHINode>(v))
    return true;
  else if(auto* gep = dyn_cast<GetElementPtrInst>(v))
    if(isLoopVariant(gep->getPointerOperand()))
      return true;
  return false;
}

bool InstrumentInstructionsCommonPass::instrument(PHINode* phi) {
  bool changed = false;

  if(phi->getType()->isPointerTy())
    for(unsigned j = 0; j < phi->getNumIncomingValues(); j++)
      if(auto* inst = dyn_cast<Instruction>(phi->getIncomingValue(j)))
        if(isLoopVariant(inst) and not isa<PHINode>(inst)
           and not moya::Metadata::hasLoopVariant(inst))
          changed |= moya::Metadata::setLoopVariant(inst);

  return changed;
}

bool InstrumentInstructionsCommonPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  for(Function& f : module.functions()) {
    if(not moya::Metadata::hasNoAnalyze(f)) {
      for(auto i = inst_begin(f); i != inst_end(f); i++) {
        Instruction* inst = &*i;
        if(auto* call = dyn_cast<CallInst>(inst)) {
          changed |= instrumentIgnore(call);
          changed |= instrumentAsmCall(call);
          changed |= instrumentMemcpy(call);
          changed |= instrumentMemset(call);
          changed |= instrumentAlloca(call);
        } else if(auto* invoke = dyn_cast<InvokeInst>(inst)) {
          changed |= instrumentIgnore(invoke);
          changed |= instrumentAsmCall(invoke);
          changed |= instrumentMemcpy(invoke);
          changed |= instrumentMemset(invoke);
        } else if(auto* inttoptr = dyn_cast<IntToPtrInst>(inst)) {
          changed |= instrumentRealloc(inttoptr);
        } else if(auto* gep = dyn_cast<GetElementPtrInst>(inst)) {
          changed |= instrument(gep);
        } else if(auto* alloca = dyn_cast<AllocaInst>(inst)) {
          changed |= instrument(alloca);
        } else if(auto* phi = dyn_cast<PHINode>(inst)) {
          changed |= instrument(phi);
        }
      }
    }
  }

  return changed;
}

char InstrumentInstructionsCommonPass::ID = 0;

static const char* name = "moya-instr-insts-common";
static const char* descr = "Adds metadata to instructions";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(InstrumentInstructionsCommonPass, name, descr, cfg, analysis)

Pass* createInstrumentInstructionsCommonPass() {
  return new InstrumentInstructionsCommonPass();
}

void registerInstrumentInstructionsCommonPass(const PassManagerBuilder&,
                                              legacy::PassManagerBase& pm) {
  pm.add(new InstrumentInstructionsCommonPass());
}
