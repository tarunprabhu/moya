#ifndef MOYA_FRONTEND_ALL_LANGS_PASSES_H
#define MOYA_FRONTEND_ALL_LANGS_PASSES_H

#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Pass.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

llvm::Pass* createEarlyInstrumentPass();
llvm::Pass* createGenerateFileBitcodeGlobalPass();
llvm::Pass* createGenerateGettersPass();
llvm::Pass* createGenerateMoyaSignaturePass();
llvm::Pass* createInsertMoyaAPIDeclsPass();
llvm::Pass* createInstrumentMallocsPass();
llvm::Pass* createJITProfilingPass();
llvm::Pass* createJITRedirectPass();
llvm::Pass* createPromoteConstExprsPass();
llvm::Pass* createPrepareInstrumentPass();
llvm::Pass* createInstrumentFunctionsCommonPass();
llvm::Pass* createInstrumentGlobalsCommonPass();
llvm::Pass* createInstrumentInstructionsCommonPass();
llvm::Pass* createInstrumentTypesCommonPass();
llvm::Pass* createPropagateAnalysisTypesPass();
llvm::Pass* createSanityCheckPass();

namespace llvm {
void initializeEarlyInstrumentPassPass(PassRegistry&);
void initializeGenerateFileBitcodeGlobalPassPass(PassRegistry&);
void initializeGenerateGettersPassPass(PassRegistry&);
void initializeGenerateMoyaSignaturePassPass(PassRegistry&);
void initializeInsertMoyaAPIDeclsPassPass(PassRegistry&);
void initializeInstrumentFunctionsCommonPassPass(PassRegistry&);
void initializeInstrumentGlobalsCommonPassPass(PassRegistry&);
void initializeInstrumentInstructionsCommonPassPass(PassRegistry&);
void initializeInstrumentMallocsPassPass(PassRegistry&);
void initializeInstrumentTypesCommonPassPass(PassRegistry&);
void initializeJITProfilingPassPass(PassRegistry&);
void initializeJITRedirectPassPass(PassRegistry&);
void initializePrepareInstrumentPassPass(PassRegistry&);
void initializePromoteConstExprsPassPass(PassRegistry&);
void initializePropagateAnalysisTypesPassPass(PassRegistry&);
void initializeSanityCheckPassPass(PassRegistry&);
} // namespace llvm

#endif // MOYA_FRONTEND_ALL_LANGS_PASSES_H
