#include "Passes.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"

#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

class InsertMoyaAPIDeclsPass : public ModulePass {
public:
  static char ID;

public:
  InsertMoyaAPIDeclsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

InsertMoyaAPIDeclsPass::InsertMoyaAPIDeclsPass() : ModulePass(ID) {
  llvm::initializeInsertMoyaAPIDeclsPassPass(*PassRegistry::getPassRegistry());
}

StringRef InsertMoyaAPIDeclsPass::getPassName() const {
  return "Moya Insert API Decls";
}

void InsertMoyaAPIDeclsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool InsertMoyaAPIDeclsPass::runOnModule(Module& module) {
  moya_message(getPassName());

  LLVMContext& context = module.getContext();

  IntegerType* i1 = Type::getInt1Ty(context);
  IntegerType* i8 = Type::getInt8Ty(context);
  IntegerType* i16 = Type::getInt16Ty(context);
  IntegerType* i32 = Type::getInt32Ty(context);
  IntegerType* i64 = Type::getInt64Ty(context);
  Type* f32 = Type::getFloatTy(context);
  Type* f64 = Type::getDoubleTy(context);
  Type* f80 = Type::getX86_FP80Ty(context);

#ifdef ENABLED_CUDA
  if(StructType* cuStream
     = module.getTypeByName(Config::getCudaStreamStructName()))
    moya::LLVMUtils::getExecuteCudaFunction(module);
#endif // ENABLED_CUDA

  moya::LLVMUtils::getIsInRegionFunction(module);
  moya::LLVMUtils::getCompileFunction(module);
  moya::LLVMUtils::getBeginCallFunction(module);

  moya::LLVMUtils::getRegisterArgFunction(module, i1);
  moya::LLVMUtils::getRegisterArgFunction(module, i8);
  moya::LLVMUtils::getRegisterArgFunction(module, i16);
  moya::LLVMUtils::getRegisterArgFunction(module, i32);
  moya::LLVMUtils::getRegisterArgFunction(module, i64);
  moya::LLVMUtils::getRegisterArgFunction(module, f32);
  moya::LLVMUtils::getRegisterArgFunction(module, f64);
  moya::LLVMUtils::getRegisterArgFunction(module, f80);

  moya::LLVMUtils::getRegisterArgFunction(module, i1->getPointerTo());
  moya::LLVMUtils::getRegisterArgFunction(module, i8->getPointerTo());
  moya::LLVMUtils::getRegisterArgFunction(module, i16->getPointerTo());
  moya::LLVMUtils::getRegisterArgFunction(module, i32->getPointerTo());
  moya::LLVMUtils::getRegisterArgFunction(module, i64->getPointerTo());
  moya::LLVMUtils::getRegisterArgFunction(module, f32->getPointerTo());
  moya::LLVMUtils::getRegisterArgFunction(module, f64->getPointerTo());
  moya::LLVMUtils::getRegisterArgFunction(module, f80->getPointerTo());

  moya::LLVMUtils::getRegisterArgFunction(module, ArrayType::get(i1, 1));
  moya::LLVMUtils::getRegisterArgFunction(module, ArrayType::get(i8, 1));
  moya::LLVMUtils::getRegisterArgFunction(module, ArrayType::get(i16, 1));
  moya::LLVMUtils::getRegisterArgFunction(module, ArrayType::get(i32, 1));
  moya::LLVMUtils::getRegisterArgFunction(module, ArrayType::get(i64, 1));
  moya::LLVMUtils::getRegisterArgFunction(module, ArrayType::get(f32, 1));
  moya::LLVMUtils::getRegisterArgFunction(module, ArrayType::get(f64, 1));
  moya::LLVMUtils::getRegisterArgFunction(module, ArrayType::get(f80, 1));

  moya::LLVMUtils::getRegisterArgPtrFunction(module);
  moya::LLVMUtils::getRegisterArgFunction(module,
                                          i8->getPointerTo()->getPointerTo());

  // moya::LLVMUtils::getStatsStartCallFunction(module);
  // moya::LLVMUtils::getStatsStopCallFunction(module);
  // moya::LLVMUtils::getStatsStartCompileFunction(module);
  // moya::LLVMUtils::getStatsStopCompileFunction(module);
  moya::LLVMUtils::getStatsStartExecuteFunction(module);
  moya::LLVMUtils::getStatsStopExecuteFunction(module);

  moya::LLVMUtils::getTopErrorFunction(module);

  return true;
}

char InsertMoyaAPIDeclsPass::ID = 0;

static const char* name = "moya-api-decls";
static const char* descr = "Inserts decls for the Moya API functions";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(InsertMoyaAPIDeclsPass, name, descr, cfg, analysis)

void registerInsertMoyaAPIDeclsPass(const PassManagerBuilder&,
                                    legacy::PassManagerBase& pm) {
  pm.add(new InsertMoyaAPIDeclsPass());
}

Pass* createInsertMoyaAPIDeclsPass() {
  return new InsertMoyaAPIDeclsPass();
}
