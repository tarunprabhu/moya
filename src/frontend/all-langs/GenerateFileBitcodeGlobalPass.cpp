#include "Passes.h"
#include "common/Config.h"
#include "common/Diagnostics.h"

#include <llvm/Bitcode/BitcodeWriter.h>
#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

#include <regex>
#include <sstream>

using namespace llvm;

class GenerateFileBitcodeGlobalPass : public ModulePass {
public:
  static char ID;

protected:
  std::string fixSourceFilename(const std::string&);

public:
  GenerateFileBitcodeGlobalPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

GenerateFileBitcodeGlobalPass::GenerateFileBitcodeGlobalPass()
    : ModulePass(ID) {
  llvm::initializeGenerateFileBitcodeGlobalPassPass(
      *PassRegistry::getPassRegistry());
}

StringRef GenerateFileBitcodeGlobalPass::getPassName() const {
  return "Moya Generate File Bitcode Global";
}

void GenerateFileBitcodeGlobalPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

std::string
GenerateFileBitcodeGlobalPass::fixSourceFilename(const std::string& base) {
  std::smatch m;
  std::regex re("^(/tmp/)(.+)-(.+)\\.([A-Za-z]+)$",
                std::regex_constants::ECMAScript);

  if(not std::regex_match(base, m, re)) {
    moya_error("Could not match source filename");
    return "";
  }

  std::stringstream ss;
  ss << m[2].str() << "." << m[4].str();
  return ss.str();
}

bool GenerateFileBitcodeGlobalPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  // Clang seems to add a optnone function attribute if no optimization flag
  // was passed. This inhibits the cleanup passes which will run mem2reg and
  // adce among other things. Since we have full control of the bitcode and
  // know what we are doing, just get rid of that attribute.
  // FIXME?: We also want to
  // avoid inlining too much because it messes up the analysis. So add a
  // noinline flag as well. The noinline flag is only meant for the code
  // that will be analyzed. Otherwise, we definitely want inlining to take
  // place when appropriate. So it will be added back once the bitcode has
  // been serialized
  for(Function& f : module.functions()) {
    if(f.size()) {
      if(not f.hasFnAttribute(Attribute::AttrKind::AlwaysInline))
        f.addFnAttr(Attribute::AttrKind::NoInline);
      f.removeFnAttr(Attribute::AttrKind::OptimizeNone);
    }
  }

  LLVMContext& context = module.getContext();
  std::string filename = module.getSourceFileName();
  std::string bcName = moya::Config::getFileBitcodeFileName(filename);

  std::string s;
  raw_string_ostream ss(s);
  WriteBitcodeToFile(module, ss);
  std::string bcCode = ss.str();

  // The format of this global variable is as follows:
  //
  // [n x i8]:[p x i8]:[m x i8]
  //
  // The variable itself is just a formatted array of bytes. The different
  // "fields" within the array are delimited by ':'
  //
  // [n x 18]: is the name of the file into which the bitcode should be
  // extracted. The name is guaranteed to only contain alphabets, digits and
  // underscores
  //
  // [p x i8]: is the size in bytes of the bitcode.
  // NOTE: This is a human-readable std::string of bytes so it is ASCII encoded
  //
  // [m x i8]: is the LLVM bitcode
  //

  std::string gs;
  raw_string_ostream gss(gs);
  gss << bcName << ":" << bcCode.length() << ":" << bcCode;
  std::string bc = gss.str();

  std::string name = moya::Config::getFileBitcodeGlobalName(filename);
  Type* type = ArrayType::get(Type::getInt8Ty(context), bc.length());
  Constant* init = ConstantDataArray::getString(context, bc, false);
  auto* g = dyn_cast<GlobalVariable>(module.getOrInsertGlobal(name, type));
  g->setConstant(true);
  g->setInitializer(init);
  g->setLinkage(GlobalValue::LinkageTypes::ExternalLinkage);
  g->setSection(moya::Config::getELFBitcodeSectionName());

  // Restore inline attribute
  for(Function& f : module.functions())
    if(f.size())
      f.removeFnAttr(Attribute::AttrKind::NoInline);

  return changed;
}

char GenerateFileBitcodeGlobalPass::ID = 0;

static const char* name = "moya-file-bitcode-global";
static const char* descr
    = "Inserts a global variable with the bitcode for the module";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(GenerateFileBitcodeGlobalPass, name, descr, cfg, analysis)

void registerGenerateFileBitcodeGlobalPass(const PassManagerBuilder&,
                                           legacy::PassManagerBase& pm) {
  pm.add(new GenerateFileBitcodeGlobalPass());
}

Pass* createGenerateFileBitcodeGlobalPass() {
  return new GenerateFileBitcodeGlobalPass();
}
