#include "InstrumentUtils.h"
#include "common/Config.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "frontend/components/FunctionBase.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instructions.h>

using llvm::dyn_cast;
using llvm::isa;

namespace moya {

namespace InstrumentUtils {

static bool instrumentDirective(const SpecializeDirective& specialize,
                                const FunctionBase& feFunc,
                                llvm::Function& f) {
  bool changed = false;

  const std::string& name = f.getName();
  const Location& loc = feFunc.getLocation();

  std::string redir = Config::generateRedirecterName(name, loc);
  JITID id = Config::generateJITID(name, loc);

  changed |= Metadata::setSpecialize(f);
  changed |= Metadata::setRedirecter(f, redir);
  changed |= Metadata::setJITID(f, id);

  for(const SpecializeDirective::FixedArray& arr :
      specialize.getFixedArrayArgs()) {
    const std::string& argName = arr.car();
    if(feFunc.hasArg(argName)) {
      int argNum = feFunc.getArg(argName).getArgNum();
      llvm::Argument& arg = *LLVMUtils::getArgument(f, argNum);
      if(not arg.getType()->isPointerTy())
        moya_error("Argument declared as an array must have pointer type\n"
                   << "   arg: " << argName << "\n"
                   << "  func: " << feFunc.getName());
      changed |= Metadata::setFixedArray(arg, arr.cdr());
    } else {
      moya_error("No argument named "
                   << argName << " in function " << feFunc.getName()
                   << ". Ignoring \"array\" clause element");
    }
  }

  for(const SpecializeDirective::DynArray& arr : specialize.getDynArrayArgs()) {
    const std::string& argName = arr.car();
    if(feFunc.hasArg(argName)) {
      const std::string& sizeArgName = arr.cdr();
      // FIXME: All the array length to be a global variable
      if(feFunc.hasArg(sizeArgName)) {
        int argNum = feFunc.getArg(argName).getArgNum();
        int sizeArgNum = feFunc.getArg(sizeArgName).getArgNum();
        llvm::Argument& arg = *LLVMUtils::getArgument(f, argNum);
        if(not arg.getType()->isPointerTy())
          moya_error("Argument declared as an array must have pointer type\n"
                     << "   arg: " << argName << "\n"
                     << "  func: " << feFunc.getName());
        if(not LLVMUtils::getArgument(f, sizeArgNum)->getType()->isIntegerTy())
          moya_error("Argument declared array size must have integral type\n"
                     << "   arg: " << sizeArgName << "\n"
                     << "  func: " << feFunc.getName());
        changed |= Metadata::setDynArray(arg, sizeArgNum);
      } else {
        moya_error("No argument named "
                     << sizeArgName << " in function " << feFunc.getName()
                     << ". Ignoring \"array\" clause element " << argName);
      }
    } else {
      moya_error("No argument named "
                   << argName << " in function " << feFunc.getName()
                   << ". Ignoring \"array\" clause element");
    }
  }

  for(const std::string& argName : specialize.getForceArgNames()) {
    if(feFunc.hasArg(argName)) {
      int argNum = feFunc.getArg(argName).getArgNum();
      llvm::Argument& arg = *LLVMUtils::getArgument(f, argNum);
      changed |= Metadata::setForce(arg);
    } else {
      moya_error("No argument named " << argName << " in function "
                                      << feFunc.getName()
                                      << ". Ignoring \"force\" clause");
    }
  }

  for(const std::string& argName : specialize.getIgnoreArgNames()) {
    if(feFunc.hasArg(argName)) {
      int argNum = feFunc.getArg(argName).getArgNum();
      llvm::Argument& arg = *LLVMUtils::getArgument(f, argNum);
      changed |= Metadata::setIgnore(arg);
    } else {
      moya_error("No argument named " << argName << " in function "
                                      << feFunc.getName()
                                      << ". Ignoring \"ignore\" clause");
    }
  }

  changed |= Metadata::setTuningLevel(f, specialize.getTuningLevel());

  return changed;
}

static bool instrumentDirective(const DeclareDirective& declare,
                                const FunctionBase&,
                                llvm::Function& f) {
  bool changed = false;

  if(declare.getMemsafe())
    changed |= Metadata::setMemSafe(f);
  if(declare.getAllocator())
    changed |= Metadata::setUserMalloc(f);
  if(declare.getNoAnalyze())
    changed |= Metadata::setNoAnalyze(f);

  return changed;
}

static bool instrumentDirective(const ModelDirective& model,
                                const FunctionBase&,
                                llvm::Function& f) {
  bool changed = false;

  changed |= Metadata::setForceModel(f);
  if(model.hasModelName())
    changed |= Metadata::setModel(f, model.getModelName());

  return changed;
}

bool instrumentDirectives(const FunctionBase& feFunc, llvm::Function& f) {
  bool changed = false;

  // Add instrumentation from Moya annotations
  for(const FunctionDirective* d : feFunc.getDirectives())
    if(const auto* specialize = dyn_cast<SpecializeDirective>(d))
      changed |= instrumentDirective(*specialize, feFunc, f);
    else if(const auto* declare = dyn_cast<DeclareDirective>(d))
      changed |= instrumentDirective(*declare, feFunc, f);
    else if(const auto* model = dyn_cast<ModelDirective>(d))
      changed |= instrumentDirective(*model, feFunc, f);

  return changed;
}

static llvm::BasicBlock* getBasicBlockContainingReturn(llvm::Function& f) {
  for(auto bb = f.begin(); bb != f.end(); bb++)
    if(isa<llvm::ReturnInst>(bb->getTerminator()))
      return &*bb;
  return nullptr;
}

llvm::Instruction* getFirstNonAllocaInst(llvm::Function& f) {
  for(llvm::inst_iterator i = inst_begin(f); i != inst_end(f); i++)
    if(not isa<llvm::AllocaInst>(&*i))
      return &*i;
  return nullptr;
}

llvm::Instruction* getLastNonReturnInst(llvm::Function& f) {
  llvm::BasicBlock& bbLast = *getBasicBlockContainingReturn(f);
  bbLast.splitBasicBlock(&bbLast.back());

  // bbLast is now the "original" terminator block. The actual return
  // instruction is in the new block that was created. The last instruction
  // of bbLast will be the unconditional branch to the terminator block
  return &bbLast.back();
}

} // namespace InstrumentUtils

} // namespace moya
