#include "InstrumentTypesCommonPass.h"
#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

InstrumentTypesCommonPass::InstrumentTypesCommonPass() : ModulePass(ID) {
  llvm::initializeInstrumentTypesCommonPassPass(
      *PassRegistry::getPassRegistry());
}

StringRef InstrumentTypesCommonPass::getPassName() const {
  return "Moya Instrument Types (All languages)";
}

void InstrumentTypesCommonPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool InstrumentTypesCommonPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  for(StructType* sty : moya::LLVMUtils::getRecursiveStructs(module))
    changed |= moya::Metadata::setRecursive(sty, module);

  return changed;
}

char InstrumentTypesCommonPass::ID = 0;

static const char* name = "moya-instr-types";
static const char* descr = "Adds metadata to types (all languages)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(InstrumentTypesCommonPass, name, descr, cfg, analysis)

Pass* createInstrumentTypesCommonPass() {
  return new InstrumentTypesCommonPass();
}

void registerInstrumentTypesCommonPass(const PassManagerBuilder&,
                                       legacy::PassManagerBase& pm) {
  pm.add(new InstrumentTypesCommonPass());
}
