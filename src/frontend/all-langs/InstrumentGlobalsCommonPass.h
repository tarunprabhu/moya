#ifndef MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_GLOBALS_COMMON_PASS
#define MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_GLOBALS_COMMON_PASS

#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

class InstrumentGlobalsCommonPass : public llvm::ModulePass {
public:
  static char ID;

public:
  InstrumentGlobalsCommonPass();

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);
};

#endif // MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_GLOBALS_COMMON_PASS
