#ifndef MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_TYPES_COMMON_PASS_H
#define MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_TYPES_COMMON_PASS_H

#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

class InstrumentTypesCommonPass : public llvm::ModulePass {
public:
  static char ID;

public:
  InstrumentTypesCommonPass();

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);
};

#endif // MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_TYPES_COMMON_PASS_H
