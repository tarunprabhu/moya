#include "Passes.h"
#include <llvm/IR/Attributes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

#include "common/Diagnostics.h"
#include "common/Metadata.h"

using namespace llvm;

// Prepare the module for instrumentation. This is not merged with the
// sanity checks pass because the sanity checks pass is deliberately intended
// to not modify the module at all
class PrepareInstrumentPass : public ModulePass {
public:
  static char ID;

public:
  PrepareInstrumentPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

PrepareInstrumentPass::PrepareInstrumentPass() : ModulePass(ID) {
  llvm::initializePrepareInstrumentPassPass(*PassRegistry::getPassRegistry());
}

StringRef PrepareInstrumentPass::getPassName() const {
  return "Moya Prepare Instrument";
}

void PrepareInstrumentPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool PrepareInstrumentPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  // Undo any disable optimization annotations that may have been added by
  // the EarlyInstrumentPass
  for(Function& f : module.functions())
    if(f.size() and moya::Metadata::hasDisableOptimizations(f)) {
      f.removeFnAttr(Attribute::OptimizeNone);
      changed |= moya::Metadata::resetDisableOptimizations(f);
    }

  return changed;
}

char PrepareInstrumentPass::ID = 0;

static const char* name = "moya-instr-prepare";
static const char* descr = "Prepare the code for the instrumentation passes";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(PrepareInstrumentPass, name, descr, cfg, analysis);

Pass* createPrepareInstrumentPass() {
  return new PrepareInstrumentPass();
}

void registerPrepareInstrumentPass(const PassManagerBuilder&,
                                   legacy::PassManagerBase& pm) {
  pm.add(new PrepareInstrumentPass());
}
