#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/GlobalVariableUtils.h"
#include "common/InstructionUtils.h"
#include "common/LangConfig.h"
#include "common/Metadata.h"
#include "common/TypeUtils.h"

#include <llvm/IR/Function.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>

using namespace llvm;

// This is *ONLY* valid for C and C++. It should be ok for Fortran too but
// there is separate propagation pass for that because the code is a lot
// messier
class PropagateAnalysisTypesPass : public ModulePass {
public:
  static char ID;

protected:
  Type* propagateFwd(AllocaInst*);
  Type* propagateFwd(CastInst*, Type*);
  bool propagateFwd(Instruction*, Type*);

  bool propagateBack(CallInst*);
  bool propagateBack(InvokeInst*);
  bool propagateBack(AllocaInst*, Type*);
  bool propagateBack(CastInst*, Type*);
  bool propagateBack(GetElementPtrInst*, Type*);
  bool propagateBack(LoadInst*, Type*);
  bool propagateBack(Value*, Type*);

  void sanityCheck(Instruction*, Type*);

public:
  PropagateAnalysisTypesPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

PropagateAnalysisTypesPass::PropagateAnalysisTypesPass() : ModulePass(ID) {
  llvm::initializePropagateAnalysisTypesPassPass(
      *PassRegistry::getPassRegistry());
}

StringRef PropagateAnalysisTypesPass::getPassName() const {
  return "Moya Propagate Analysis Types (C++)";
}

void PropagateAnalysisTypesPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

Type* PropagateAnalysisTypesPass::propagateFwd(CastInst* cst, Type* type) {
  Type* next = nullptr;

  if(moya::LLVMUtils::areIdentical(cst->getOperand(0)->getType(),
                                   cst->getDestTy()))
    if(type and type != cst->getDestTy()) {
      moya::Metadata::setAnalysisType(cst, type);
      next = type;
    }

  return next;
}

Type* PropagateAnalysisTypesPass::propagateFwd(AllocaInst* alloca) {
  Type* allocated = alloca->getType();
  if(moya::Metadata::hasAnalysisType(alloca))
    allocated = moya::Metadata::getAnalysisType(alloca);

  return allocated;
}

bool PropagateAnalysisTypesPass::propagateFwd(Instruction* inst, Type* type) {
  bool changed = false;
  Type* next = nullptr;

  if(auto* alloca = dyn_cast<AllocaInst>(inst))
    next = propagateFwd(alloca);
  else if(auto* cst = dyn_cast<CastInst>(inst))
    next = propagateFwd(cst, type);

  if(next)
    for(Use& u : inst->uses())
      if(auto* user = dyn_cast<Instruction>(u.getUser()))
        changed |= propagateFwd(user, next);

  return changed;
}

void PropagateAnalysisTypesPass::sanityCheck(Instruction* inst, Type* type) {
  if(moya::Metadata::hasAnalysisType(inst))
    if(not moya::LLVMUtils::areIdentical(moya::Metadata::getAnalysisType(inst),
                                         type))
      moya_error("Inconsistent analysis types\n"
                 << "  inst: " << *inst << "\n"
                 << "  func: " << moya::LLVMUtils::getFunctionName(inst) << "\n"
                 << "   old: " << *moya::Metadata::getAnalysisType(inst) << "\n"
                 << "   new: " << *type);
}

bool PropagateAnalysisTypesPass::propagateBack(AllocaInst* alloca, Type* type) {
  bool changed = false;

  sanityCheck(alloca, type);
  changed |= moya::Metadata::setAnalysisType(alloca, type);

  return changed;
}

bool PropagateAnalysisTypesPass::propagateBack(CastInst* cst, Type* type) {
  bool changed = false;

  sanityCheck(cst, type);
  if(moya::LLVMUtils::areIdentical(cst->getOperand(0)->getType(),
                                   cst->getDestTy())) {
    if(type and type != cst->getDestTy()) {
      if(not moya::Metadata::hasAnalysisType(cst)
         or moya::Metadata::hasArtificial(type,
                                          *moya::LLVMUtils::getModule(cst))) {
        changed |= moya::Metadata::setAnalysisType(cst, type);
        changed |= propagateBack(cst->getOperand(0), type);
      }
    }
  }

  return changed;
}

bool PropagateAnalysisTypesPass::propagateBack(LoadInst* load, Type* type) {
  bool changed = false;

  sanityCheck(load, type);
  changed |= moya::Metadata::setAnalysisType(load, type);
  changed |= propagateBack(load->getPointerOperand(), type->getPointerTo());

  return changed;
}

bool PropagateAnalysisTypesPass::propagateBack(GetElementPtrInst* gep,
                                               Type* type) {
  bool changed = false;

  sanityCheck(gep, type);
  changed |= moya::Metadata::setAnalysisType(gep, type);

  return changed;
}

bool PropagateAnalysisTypesPass::propagateBack(CallInst* call) {
  bool changed = false;

  if(Function* f = moya::LLVMUtils::getCalledFunction(call))
    for(Argument& arg : f->args())
      if(moya::Metadata::hasAnalysisType(arg))
        changed |= propagateBack(call->getArgOperand(arg.getArgNo()),
                                 moya::Metadata::getAnalysisType(arg));

  return changed;
}

bool PropagateAnalysisTypesPass::propagateBack(InvokeInst* invoke) {
  bool changed = false;

  if(Function* f = moya::LLVMUtils::getCalledFunction(invoke))
    for(Argument& arg : f->args())
      if(moya::Metadata::hasAnalysisType(arg))
        changed |= propagateBack(invoke->getArgOperand(arg.getArgNo()),
                                 moya::Metadata::getAnalysisType(arg));

  return changed;
}

bool PropagateAnalysisTypesPass::propagateBack(Value* inst, Type* type) {
  bool changed = false;

  if(auto* cst = dyn_cast<CastInst>(inst))
    changed |= propagateBack(cst, type);
  else if(auto* alloca = dyn_cast<AllocaInst>(inst))
    changed |= propagateBack(alloca, type);
  else if(auto* load = dyn_cast<LoadInst>(inst))
    changed |= propagateBack(load, type);
  else if(auto* gep = dyn_cast<GetElementPtrInst>(inst))
    changed |= propagateBack(gep, type);

  return changed;
}

bool PropagateAnalysisTypesPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  const std::string& srcFile = module.getSourceFileName();
  if(not(LangConfigC::isValidSourceFile(srcFile)
         or LangConfigCXX::isValidSourceFile(srcFile)))
    moya_error("PropagateAnalysisTypesPass should only be called for C or C++ "
               "source. Got "
               << srcFile);

  for(GlobalVariable& g : module.globals())
    for(Use& u : g.uses())
      if(auto* inst = dyn_cast<Instruction>(u.getUser()))
        changed |= propagateFwd(inst, moya::LLVMUtils::getAnalysisType(g));

  for(Function& f : module.functions()) {
    for(auto i = inst_begin(f); i != inst_end(f); i++) {
      Instruction* inst = &*i;
      changed |= propagateFwd(inst, nullptr);
      // if(auto* call = dyn_cast<CallInst>(inst))
      //   changed |= propagateBack(call);
      // else if(auto* invoke = dyn_cast<InvokeInst>(inst))
      //   changed |= propagateBack(invoke);
    }
  }

  return changed;
}

char PropagateAnalysisTypesPass::ID = 0;

static const char* name = "moya-propagate-analysis-types";
static const char* descr
    = "Propagates analysis types identified by previous passes";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(PropagateAnalysisTypesPass, name, descr, cfg, analysis);

void registerPropagateAnalysisTypesPass(const PassManagerBuilder&,
                                        legacy::PassManagerBase& pm) {
  pm.add(new PropagateAnalysisTypesPass());
}

Pass* createPropagateAnalysisTypesPass() {
  return new PropagateAnalysisTypesPass();
}
