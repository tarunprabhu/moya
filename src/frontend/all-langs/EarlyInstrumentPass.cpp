#include "Passes.h"
#include <llvm/IR/Attributes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/Metadata.h"

using namespace llvm;

// This performs some early function-level preparatory steps as soon as the
// code comes out of the front-end.
class EarlyInstrumentPass : public FunctionPass {
public:
  static char ID;

public:
  EarlyInstrumentPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnFunction(Function&);
};

EarlyInstrumentPass::EarlyInstrumentPass() : FunctionPass(ID) {
  llvm::initializeEarlyInstrumentPassPass(*PassRegistry::getPassRegistry());
}

StringRef EarlyInstrumentPass::getPassName() const {
  return "Moya Early Instrument (Fortran)";
}

void EarlyInstrumentPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool EarlyInstrumentPass::runOnFunction(Function& f) {
  moya_message(getPassName());

  bool changed = false;

  // The main behavior is disabling optimizations for the function.
  // Before the module passes are run, there are some optimization
  // passes like early CSE that break the instrumentation passes. So stop the
  // optimizations from running on the function. Most of those passes will be
  // run during the module-level optimizations anyway, so no harm done here

  // Unless the programmer has disabled optimizations for this function, we
  // will want to reset the OptimizeNone attribute in a later pass
  // If that is not the case, we will want to remove this attribute in a later
  // pass.
  if(not f.hasFnAttribute(Attribute::OptimizeNone)) {
    changed |= moya::Metadata::setDisableOptimizations(f);
    f.addFnAttr(Attribute::OptimizeNone);
  }

  return changed;
}

char EarlyInstrumentPass::ID = 0;

static const char* name = "moya-instr-early";
static const char* descr = "Instrumentation pass that runs before any other";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(EarlyInstrumentPass, name, descr, cfg, analysis)

void registerEarlyInstrumentPass(const PassManagerBuilder&,
                                 legacy::PassManagerBase& pm) {
  pm.add(new EarlyInstrumentPass());
}

Pass* createEarlyInstrumentPass() {
  return new EarlyInstrumentPass();
}
