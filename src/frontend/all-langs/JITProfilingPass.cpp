#include "InstrumentUtils.h"
#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/GlobalValue.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/Transforms/Utils/UnifyFunctionExitNodes.h>

using namespace llvm;

class JITProfilingPass : public ModulePass {
private:
  bool isCudaKernel(Function&);
  ReturnInst* getUniqueReturnInst(Function&);

public:
  JITProfilingPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);

public:
  static char ID;
};

JITProfilingPass::JITProfilingPass() : ModulePass(ID) {
  initializeJITProfilingPassPass(*PassRegistry::getPassRegistry());
}

StringRef JITProfilingPass::getPassName() const {
  return "Moya JIT Profiling";
}

void JITProfilingPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<UnifyFunctionExitNodes>();
  AU.setPreservesCFG();
}

bool JITProfilingPass::isCudaKernel(Function& f) {
  return moya::Metadata::hasCudaKernelDevice(f);
}

ReturnInst* JITProfilingPass::getUniqueReturnInst(Function& f) {
  ReturnInst* retInst = nullptr;
  for(inst_iterator i = inst_begin(f); i != inst_end(f); i++) {
    if(auto* ret = dyn_cast<ReturnInst>(&*i)) {
      if(retInst)
        moya_error("No unique return inst in: " << f.getName());
      else
        retInst = ret;
    }
  }

  return retInst;
}

bool JITProfilingPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  LLVMContext& context = module.getContext();

  Type* tyKey = moya::LLVMUtils::getJITIDType(context);
  Type* tySig = moya::LLVMUtils::getFunctionSignatureBasicType(context);
  Type* tyVer = moya::LLVMUtils::getFunctionSignatureTuningType(context);

  Function* fnStatsStart
      = moya::LLVMUtils::getStatsStartExecuteFunction(module);
  Function* fnStatsStop = moya::LLVMUtils::getStatsStopExecuteFunction(module);
  for(Function& f : module.functions()) {
    // We don't insert the instrumentation into Cuda kernels because we cannot
    // call the timing functions from device code.
    if(f.size() and moya::Metadata::hasSpecialize(f) and not isCudaKernel(f)) {
      JITID id = moya::Metadata::getJITID(f);
      Value* cid = ConstantInt::get(tyKey, id, false);
      Value* csig = ConstantInt::get(tySig, 0, false);
      Value* cver = ConstantInt::get(tyVer, 0, false);

      Value* argsRecord[] = {cid, csig, cver};

      Instruction* first = moya::InstrumentUtils::getFirstNonAllocaInst(f);
      BasicBlock* bbStart = first->getParent()->splitBasicBlock(first);
      // bbStart will now contain the first non-alloca instruction along with
      // everything else. Splitting it again will empty it out except for
      // a single non-terminator instruction
      bbStart->splitBasicBlock(first);
      CallInst::Create(fnStatsStart, argsRecord, "", bbStart->getTerminator());

      ReturnInst* ret = getUniqueReturnInst(f);
      BasicBlock* bbStop = ret->getParent();
      // The return instruction will be the only instruction in bbStop.
      // After splitting, bbStop will be an empty block containing a single
      // terminator instruction to the new block which will contain the
      // return instruction
      bbStop->splitBasicBlock(ret);
      CallInst::Create(fnStatsStop, argsRecord, "", bbStop->getTerminator());

      changed |= true;
    }
  }

  return changed;
}

char JITProfilingPass::ID = 0;

static const char* name = "moya-jit-profiling";
static const char* descr
    = "Adds instrumentation to collect runtime execution statistics";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(JITProfilingPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(UnifyFunctionExitNodes)
INITIALIZE_PASS_END(JITProfilingPass, name, descr, cfg, analysis)

void registerJITProfilingPass(const PassManagerBuilder&,
                              legacy::PassManagerBase& pm) {
  pm.add(new JITProfilingPass());
}

Pass* createJITProfilingPass() {
  return new JITProfilingPass();
}
