#include "Passes.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/Metadata.h"

#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

#include <sstream>

using namespace llvm;

class GenerateMoyaSignaturePass : public ModulePass {
public:
  static char ID;

public:
  GenerateMoyaSignaturePass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

GenerateMoyaSignaturePass::GenerateMoyaSignaturePass() : ModulePass(ID) {
  llvm::initializeGenerateMoyaSignaturePassPass(
      *PassRegistry::getPassRegistry());
}

StringRef GenerateMoyaSignaturePass::getPassName() const {
  return "Moya Generate Signature";
}

void GenerateMoyaSignaturePass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesAll();
}

bool GenerateMoyaSignaturePass::runOnModule(Module& module) {
  moya_message(getPassName());

  LLVMContext& context = module.getContext();

  std::stringstream ss;
  ss << "Moya " << moya::Config::getVersionString();

  NamedMDNode* nmd = moya::Metadata::getOrInsertSignatureNode(module);
  nmd->addOperand(MDNode::get(context, MDString::get(context, ss.str())));

  return true;
}

char GenerateMoyaSignaturePass::ID = 0;

static const char* name = "moya-generate-signature";
static const char* descr = "Generates Moya signature metadata for the module";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(GenerateMoyaSignaturePass, name, descr, cfg, analysis)

void registerGenerateMoyaSignaturePass(const PassManagerBuilder&,
                                       legacy::PassManagerBase& pm) {
  pm.add(new GenerateMoyaSignaturePass());
}

Pass* createGenerateMoyaSignaturePass() {
  return new GenerateMoyaSignaturePass();
}
