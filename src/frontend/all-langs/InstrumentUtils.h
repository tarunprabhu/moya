#ifndef MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_UTILS_H
#define MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_UTILS_H

#include <llvm/IR/Function.h>

namespace moya {

class FunctionBase;

namespace InstrumentUtils {

bool instrumentDirectives(const FunctionBase&, llvm::Function&);

llvm::Instruction* getFirstNonAllocaInst(llvm::Function&);
llvm::Instruction* getLastNonReturnInst(llvm::Function&);

} // namespace InstrumentUtils

} // namespace moya

#endif // MOYA_FRONTEND_ALL_LANGS_INSTRUMENT_UTILS_H
