#include "Passes.h"
#include "common/Config.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/IR/Constant.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

using namespace llvm;

class GenerateGettersPass : public ModulePass {
public:
  static char ID;

protected:
  bool generateGetter(GlobalObject&);
  bool shouldGenerateGetter(GlobalObject&);

public:
  GenerateGettersPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

GenerateGettersPass::GenerateGettersPass() : ModulePass(ID) {
  llvm::initializeGenerateGettersPassPass(*PassRegistry::getPassRegistry());
}

StringRef GenerateGettersPass::getPassName() const {
  return "Moya Generate Getters";
}

void GenerateGettersPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesCFG();
}

bool GenerateGettersPass::generateGetter(GlobalObject& g) {
  bool changed = false;

  Module& module = *g.getParent();
  LLVMContext& context = module.getContext();

  Type* pi8 = Type::getInt8PtrTy(context);

  std::string gname = moya::Config::generateGetterName(g);
  auto* getter = dyn_cast<GlobalVariable>(module.getOrInsertGlobal(gname, pi8));
  getter->setInitializer(ConstantExpr::getPointerCast(&g, pi8));
  getter->setLinkage(GlobalValue::LinkageTypes::ExternalLinkage);
  changed |= moya::Metadata::setMoyaGetter(getter);
  changed |= moya::Metadata::setNoAnalyze(getter);

  if(auto* func = dyn_cast<Function>(&g))
    changed |= moya::Metadata::setGetter(func, gname);
  else if(auto* gv = dyn_cast<GlobalVariable>(&g))
    changed |= moya::Metadata::setGetter(gv, gname);

  return changed;
}

bool GenerateGettersPass::shouldGenerateGetter(GlobalObject& g) {
  for(Use& u : g.uses())
    if(auto* inst = dyn_cast<Instruction>(u.getUser())) {
      if(moya::Metadata::hasSpecialize(moya::LLVMUtils::getFunction(inst)))
        return true;
    } else if(auto* cexpr = dyn_cast<ConstantExpr>(u.getUser())) {
      for(Use& v : cexpr->uses())
        if(auto* inst = dyn_cast<Instruction>(v.getUser()))
          if(moya::Metadata::hasSpecialize(moya::LLVMUtils::getFunction(inst)))
            return true;
    }

  return false;
}

bool GenerateGettersPass::runOnModule(Module& module) {
  bool changed = false;

  // We only need to add getters for those global variables and functions
  // which are directly used by functions that are JIT'ed
  for(Function& f : module.functions())
    if(f.size() and f.hasInternalLinkage())
      if(shouldGenerateGetter(f))
        changed |= generateGetter(f);

  for(GlobalVariable& g : module.globals())
    if(g.hasInternalLinkage() and g.hasName() and not g.isConstant())
      if(shouldGenerateGetter(g))
        changed |= generateGetter(g);

  return changed;
}

char GenerateGettersPass::ID = 0;

static const char* name = "moya-generate-getters";
static const char* descr
    = "Generates global variables that wrap around other global vales "
      "so that the address can be queried easily";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(GenerateGettersPass, name, descr, cfg, analysis)

void registerGenerateGettersPass(const PassManagerBuilder&,
                                 legacy::PassManagerBase& pm) {
  pm.add(new GenerateGettersPass());
}

Pass* createGenerateGettersPass() {
  return new GenerateGettersPass();
}
