#include "CodegenVisitorClang.h"
#include "ClangUtils.h"
#include "ModuleClang.h"

namespace moya {

const clang::RecordDecl*
CodegenVisitorClang::getDefnDecl(const clang::Type* type) const {
  return getDefnDecl(clang::QualType(type, 0));
}

const clang::RecordDecl*
CodegenVisitorClang::getDefnDecl(const clang::QualType& type) const {
  const clang::Type* inner
      = ClangUtils::getInnermost(type->getUnqualifiedDesugaredType());
  if(not inner->isDependentType())
    if(const clang::TagDecl* tag = inner->getAsTagDecl())
      if(const auto* record = llvm::dyn_cast<clang::RecordDecl>(tag))
        if(const clang::RecordDecl* defn = ClangUtils::getDefinition(record))
          if(not getModule().hasType(defn))
            return defn;
  return nullptr;
}

} // namespace moya
