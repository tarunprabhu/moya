#include "FunctionClang.h"
#include "ArgumentClang.h"
#include "ClangUtils.h"
#include "ModuleClang.h"

#include <clang/Basic/Builtins.h>

namespace moya {

FunctionClang::FunctionClang(ModuleBase& module,
                             const clang::FunctionDecl* decl,
                             llvm::Function& llvm)
    : FunctionBase(module), decl(decl), structParam(false) {
  setName(ClangUtils::getDeclName(decl));
  setModel(getName());
  setLocation(ClangUtils::getLocation(decl));
  if(const auto* defn = ClangUtils::getDefinition(decl)) {
    setDefined(defn);
    clang::SourceManager& srcMgr = defn->getASTContext().getSourceManager();
    setBeginLoc(
        ClangUtils::getLocation(decl->getSourceRange().getBegin(), srcMgr));
    setEndLoc(ClangUtils::getLocation(decl->getSourceRange().getEnd(), srcMgr));
  }
  setLLVM(llvm);
}

ModuleClang& FunctionClang::getModule() {
  return static_cast<ModuleClang&>(FunctionBase::getModule());
}

ArgumentClang& FunctionClang::getArg(unsigned i) {
  return static_cast<ArgumentClang&>(FunctionBase::getArg(i));
}

ArgumentClang& FunctionClang::getArg(const std::string& name) {
  return static_cast<ArgumentClang&>(FunctionBase::getArg(name));
}

const ModuleClang& FunctionClang::getModule() const {
  return static_cast<const ModuleClang&>(FunctionBase::getModule());
}

const ArgumentClang& FunctionClang::getArg(unsigned i) const {
  return static_cast<const ArgumentClang&>(FunctionBase::getArg(i));
}

const ArgumentClang& FunctionClang::getArg(const std::string& name) const {
  return static_cast<const ArgumentClang&>(FunctionBase::getArg(name));
}

const clang::FunctionDecl* FunctionClang::getDecl() const {
  return decl;
}

bool FunctionClang::hasStructParam() const {
  return structParam;
}

bool FunctionClang::isMemcpy() const {
  if(unsigned bi = decl->getMemoryFunctionKind())
    if((bi == clang::Builtin::BImemcpy) or (bi == clang::Builtin::BImemmove))
      return true;
  return false;
}

bool FunctionClang::isMemset() const {
  if(unsigned bi = decl->getMemoryFunctionKind())
    if(bi == clang::Builtin::BImemset)
      return true;
  return false;
}

} // namespace moya
