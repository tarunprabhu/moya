#include "AnnotatorClang.h"
#include "common/Config.h"

using namespace clang;

namespace moya {

static std::string cconst(MoyaID id) {
  std::stringstream ss;

  ss << id << "ULL";

  return ss.str();
}

AnnotatorClang::NewLine operator&(const AnnotatorClang::NewLine& op1,
                                  const AnnotatorClang::NewLine& op2) {
  return static_cast<AnnotatorClang::NewLine>(static_cast<int>(op1)
                                              & static_cast<int>(op2));
}

AnnotatorClang::AnnotatorClang(SourceLang lang) : lang(lang) {
  ;
}

void AnnotatorClang::initialize(SourceManager& srcMgr, LangOptions& langOpts) {
  rewriter.setSourceMgr(srcMgr, langOpts);
}

Rewriter& AnnotatorClang::getRewriter() {
  return rewriter;
}

std::string AnnotatorClang::genSentinelStartCall(MoyaID id) {
  std::string func = Config::getSentinelStartFunctionName();
  std::string sig = Config::getSentinelStartFunctionSignature();
  return generateFunctionCall(func, {cconst(id)}, sig, NewLine::Append);
}

std::string AnnotatorClang::genSentinelStopCall(MoyaID id) {
  std::string func = Config::getSentinelStopFunctionName();
  std::string sig = Config::getSentinelStopFunctionSignature();
  return generateFunctionCall(func, {cconst(id)}, sig, NewLine::Prepend);
}

void AnnotatorClang::generateEnterRegion(const Stmt* stmt, RegionID regionID) {
  rewriter.InsertText(
      stmt->getBeginLoc(), genSentinelStartCall(regionID), true, true);
}

void AnnotatorClang::generateExitRegion(const Stmt* stmt, RegionID regionID) {
  rewriter.InsertText(stmt->getEndLoc().getLocWithOffset(1),
                      genSentinelStopCall(regionID),
                      true,
                      true);
}

void AnnotatorClang::generateBeginLoop(const SourceLocation& loc,
                                       LoopID loopID) {
  rewriter.InsertText(loc, genSentinelStartCall(loopID), false, true);
}

void AnnotatorClang::generateEndLoop(const SourceLocation& loc, LoopID loopID) {
  rewriter.InsertText(loc, genSentinelStopCall(loopID), true, true);
}

} // namespace moya
