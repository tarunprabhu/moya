#ifndef MOYA_FRONTEND_C_FAMILY_CLANG_STD_UTILS_H
#define MOYA_FRONTEND_C_FAMILY_CLANG_STD_UTILS_H

#include "common/STDConfig.h"

#include <clang/AST/Decl.h>
#include <clang/AST/DeclCXX.h>

namespace moya {

namespace ClangUtils {

STDKind getSTDKind(const clang::RecordDecl*);
STDKind getSTDKind(const clang::CXXRecordDecl*);
STDKind getSTDKind(const clang::FunctionDecl*);
STDKind getSTDKind(const clang::TemplateDecl*);
STDKind getSTDKind(const clang::Type*);
STDKind getSTDKind(const clang::QualType&);

} // namespace ClangUtils

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_CLANG_STD_UTILS_H
