#include "ClangStdUtils.h"
#include "ClangUtils.h"

using clang::cast;
using clang::dyn_cast;
using clang::isa;

namespace moya {

namespace ClangUtils {

static STDKind getSTDKindImpl(const clang::NamedDecl* decl) {
  STDKind kind = STDConfig::getSTDKind(getQualifiedDeclName(decl));
  if(kind == STDKind::STD_STL_vector) {
    if(STDConfig::getSTDKind(ClangUtils::getFullDeclName(decl))
       == STDKind::STD_STL_bitvector)
      return STDKind::STD_STL_bitvector;
    else
      return STDKind::STD_STL_vector;
  }
  return kind;
}

STDKind getSTDKind(const clang::RecordDecl* decl) {
  if(const auto* clss = clang::dyn_cast<clang::CXXRecordDecl>(decl))
    return getSTDKind(clss);
  return getSTDKindImpl(decl);
}

STDKind getSTDKind(const clang::CXXRecordDecl* decl) {
  if(const clang::CXXRecordDecl* tmpl = decl->getTemplateInstantiationPattern())
    return getSTDKindImpl(tmpl);
  return getSTDKindImpl(decl);
}

STDKind getSTDKind(const clang::FunctionDecl* decl) {
  if(const auto* method = dyn_cast<clang::CXXMethodDecl>(decl))
    return getSTDKind(method->getParent());
  return getSTDKindImpl(decl);
}

STDKind getSTDKind(const clang::TemplateDecl* t) {
  return getSTDKindImpl(t);
}

STDKind getSTDKind(const clang::Type* type) {
  return getSTDKind(clang::QualType(type->getUnqualifiedDesugaredType(), 0));
}

STDKind getSTDKind(const clang::QualType& q) {
  if(const auto* clss = q->getAsCXXRecordDecl())
    return getSTDKind(clss);
  return STDConfig::getSTDKind(q.getAsString());
}

} // namespace ClangUtils

} // namespace moya
