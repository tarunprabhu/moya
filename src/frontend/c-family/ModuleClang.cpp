#include "ModuleClang.h"
#include "FunctionClang.h"
#include "GlobalVariableClang.h"
#include "TypeClang.h"

#include <clang/AST/ASTContext.h>

namespace moya {

ModuleClang::ModuleClang(SourceLang lang, CodeGeneratorClang& cg)
    : ModuleBase(lang), cg(cg) {
  ;
}

bool ModuleClang::hasDeclaration(const clang::FunctionDecl* decl) const {
  return clangDeclMap.contains(decl);
}

FunctionClang& ModuleClang::getDeclaration(llvm::Function& llvm) {
  return static_cast<FunctionClang&>(ModuleBase::getDeclaration(llvm));
}

FunctionClang& ModuleClang::getDeclaration(llvm::Function* llvm) {
  return getDeclaration(*llvm);
}

const FunctionClang& ModuleClang::getDeclaration(llvm::Function& llvm) const {
  return static_cast<const FunctionClang&>(ModuleBase::getDeclaration(llvm));
}

const FunctionClang& ModuleClang::getDeclaration(llvm::Function* llvm) const {
  return getDeclaration(*llvm);
}

bool ModuleClang::hasFunction(const clang::FunctionDecl* decl) const {
  return clangFunctionMap.contains(decl);
}

FunctionClang& ModuleClang::getFunction(llvm::Function& llvm) {
  return static_cast<FunctionClang&>(ModuleBase::getFunction(llvm));
}

const FunctionClang& ModuleClang::getFunction(llvm::Function& llvm) const {
  return static_cast<const FunctionClang&>(ModuleBase::getFunction(llvm));
}

FunctionClang& ModuleClang::getFunction(const clang::FunctionDecl* decl) {
  return *clangFunctionMap.at(decl);
}

const FunctionClang&
ModuleClang::getFunction(const clang::FunctionDecl* decl) const {
  return *clangFunctionMap.at(decl);
}

bool ModuleClang::hasGlobal(const clang::NamedDecl* decl) const {
  return clangGlobalMap.contains(decl);
}

GlobalVariableClang& ModuleClang::getGlobal(llvm::GlobalVariable& g) {
  return static_cast<GlobalVariableClang&>(ModuleBase::getGlobal(g));
}

GlobalVariableClang& ModuleClang::getGlobal(llvm::GlobalVariable* g) {
  return getGlobal(*g);
}

const GlobalVariableClang&
ModuleClang::getGlobal(llvm::GlobalVariable& g) const {
  return static_cast<const GlobalVariableClang&>(ModuleBase::getGlobal(g));
}

const GlobalVariableClang&
ModuleClang::getGlobal(llvm::GlobalVariable* g) const {
  return getGlobal(*g);
}

GlobalVariableClang& ModuleClang::getGlobal(const clang::NamedDecl* decl) {
  return *clangGlobalMap.at(decl);
}

const GlobalVariableClang&
ModuleClang::getGlobal(const clang::NamedDecl* decl) const {
  return *clangGlobalMap.at(decl);
}

void ModuleClang::addTypeMapping(const clang::Type* astType, TypeClang& type) {
  clangTypeMap[astType] = &type;
  if(const clang::RecordDecl* decl = astType->getAsRecordDecl())
    clangTypeDeclMap[decl] = &type;
}

bool ModuleClang::hasType(const clang::Type* type) const {
  return clangTypeMap.contains(type);
}

bool ModuleClang::hasType(const clang::RecordDecl* decl) const {
  return clangTypeDeclMap.contains(decl);
}

TypeClang& ModuleClang::getType(llvm::Type* llvm) {
  return static_cast<TypeClang&>(ModuleBase::getType(llvm));
}

TypeClang& ModuleClang::getType(const clang::Type* type) {
  return *clangTypeMap.at(type);
}

TypeClang& ModuleClang::getType(const clang::RecordDecl* decl) {
  return *clangTypeDeclMap.at(decl);
}

const TypeClang& ModuleClang::getType(llvm::Type* llvm) const {
  return static_cast<const TypeClang&>(ModuleBase::getType(llvm));
}

const TypeClang& ModuleClang::getType(const clang::Type* type) const {
  return *clangTypeMap.at(type);
}

const TypeClang& ModuleClang::getType(const clang::RecordDecl* decl) const {
  return *clangTypeDeclMap.at(decl);
}

llvm::Type* ModuleClang::convertToLLVM(const clang::Type* type, bool shadow) {
  clang::QualType qt(type, 0);
  return clang::CodeGen::convertTypeForMemory(cg.getCodegenModule(shadow), qt);
}

const clang::Decl* ModuleClang::getDecl(const std::string& mangled) {
  return cg.getCodeGenerator().GetDeclForMangledName(mangled);
}

const clang::ASTContext& ModuleClang::getASTContext() const {
  return cg.getASTContext();
}

const clang::SourceManager& ModuleClang::getSourceManager() const {
  return cg.getASTContext().getSourceManager();
}

} // namespace moya
