#include "PragmaHandler.h"
#include "ClangUtils.h"
#include "frontend/annotations/Sentinel.h"

#include <clang/Lex/Preprocessor.h>
#include <clang/Lex/Token.h>

namespace moya {

PragmaHandler::PragmaHandler(SourceLang lang, DirectiveContext& directives)
    : clang::PragmaHandler(Sentinel::get(lang)), lang(lang),
      directives(directives) {
  ;
}

void PragmaHandler::HandlePragma(clang::Preprocessor& pp,
                                 clang::PragmaIntroducerKind,
                                 clang::Token& token) {
  // The first token will be the moya sentinel which we don't deal with.
  // But this is the location that we record because in the GCC plugin, this
  // is the token whose location we can get easily and it is REQUIRED that
  // the locations match in this plugin and that one.
  Location loc
      = ClangUtils::getLocation(token.getLocation(), pp.getSourceManager());
  pp.Lex(token);

  // TODO: Instead of parsing the directive separately as a text string,
  // we should probably generate a token stream here and send stream to the
  // annotation parser. Since we use the preprocessor for the Fortran source
  // now anyway, there doesn't seem to be a good reason to use text here
  clang::SourceManager& srcMgr = pp.getSourceManager();
  unsigned offStart = srcMgr.getFileOffset(token.getLocation());
  while(token.isNot(clang::tok::eod))
    pp.Lex(token);
  unsigned offEnd = srcMgr.getFileOffset(token.getLocation());

  clang::FileID id = srcMgr.getFileID(token.getLocation());
  llvm::MemoryBuffer* buffer = srcMgr.getBuffer(id);
  std::string text = buffer->getBuffer().substr(offStart, offEnd - offStart);

  directives.parse(text, loc);
}

} // namespace moya
