#ifndef MOYA_FRONTEND_C_FAMILY_CODEGEN_CONTEXT_CLANG_H
#define MOYA_FRONTEND_C_FAMILY_CODEGEN_CONTEXT_CLANG_H

#include "AnnotatorClang.h"
#include "common/SourceLang.h"

#include <clang/CodeGen/ModuleBuilder.h>
#include <clang/Frontend/CompilerInstance.h>

#include <llvm/IR/LLVMContext.h>

#define CG_HANDLE_(ret, fn, atyp)  \
  ret fn(atyp arg) {               \
    codegen->fn(arg);              \
    return shadowCodegen->fn(arg); \
  }

namespace moya {

class CodeGeneratorClang {
protected:
  SourceLang lang;
  AnnotatorClang& annotator;
  llvm::LLVMContext context;
  llvm::LLVMContext shadowContext;

  // The AST Context containing all the clang declarations
  // If we have this, do we really need the visitors?
  clang::ASTContext* astContext;

  // The main codegenerator object used to generate the LLVM module
  std::unique_ptr<clang::CodeGenerator> codegen;

  // We generate two modules simultaneously because we can't figure out which
  // types are actually used in the LLVM and we can't get the clang::Decl from
  // the LLVM type. By keeping a second copy of the LLVMContext object and
  // module around, once we LLVM-gen a clang::Type, we can check in the shadow
  // module if it exists there
  std::unique_ptr<clang::CodeGenerator> shadowCodegen;

public:
  CodeGeneratorClang(SourceLang, AnnotatorClang&);
  virtual ~CodeGeneratorClang() = default;

  void initialize(clang::CompilerInstance&);

  SourceLang getSourceLang() const;
  clang::CodeGenerator& getCodeGenerator(bool = false);
  clang::CodeGen::CodeGenModule& getCodegenModule(bool = false);
  llvm::LLVMContext& getLLVMContext();
  llvm::Module& getLLVMModule();
  clang::ASTContext& getASTContext();
  const clang::ASTContext& getASTContext() const;

  CG_HANDLE_(void, HandleTranslationUnit, clang::ASTContext&)

  // These are actions that the consumer performs. We add it here because we
  // need to perform the same actions on both the main code generator and
  // the shadow code generator
  CG_HANDLE_(void, Initialize, clang::ASTContext&)
  CG_HANDLE_(bool, HandleTopLevelDecl, clang::DeclGroupRef)
  CG_HANDLE_(void, HandleInlineFunctionDefinition, clang::FunctionDecl*)
  CG_HANDLE_(void, HandleInterestingDecl, clang::DeclGroupRef)
  CG_HANDLE_(void, HandleTagDeclDefinition, clang::TagDecl*)
  CG_HANDLE_(void, HandleTagDeclRequiredDefinition, const clang::TagDecl*)
  CG_HANDLE_(void, HandleImplicitImportDecl, clang::ImportDecl*)
  CG_HANDLE_(void, CompleteTentativeDefinition, clang::VarDecl*)
};

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_CODEGEN_CONTEXT_CLANG_H
