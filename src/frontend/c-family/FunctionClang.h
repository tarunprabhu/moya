#ifndef MOYA_FRONTEND_C_FAMILY_FUNCTION_CLANG_H
#define MOYA_FRONTEND_C_FAMILY_FUNCTION_CLANG_H

#include "frontend/components/FunctionBase.h"

#include <clang/AST/Decl.h>

#include <llvm/IR/Function.h>

namespace moya {

class ArgumentClang;
class ModuleClang;

class FunctionClang : public FunctionBase {
protected:
  const clang::FunctionDecl* decl;
  bool structParam;

protected:
  FunctionClang(ModuleBase&, const clang::FunctionDecl*, llvm::Function&);

public:
  FunctionClang(const FunctionClang&) = delete;
  virtual ~FunctionClang() = default;

  template <typename ArgumentT, typename... ArgsT>
  ArgumentT& addArgument(ArgsT&&... args) {
    return FunctionBase::addArgument<ArgumentT>(args..., getNumArgs());
  }

  ModuleClang& getModule();
  ArgumentClang& getArg(unsigned);
  ArgumentClang& getArg(const std::string&);

  const ModuleClang& getModule() const;
  const ArgumentClang& getArg(unsigned) const;
  const ArgumentClang& getArg(const std::string&) const;
  const clang::FunctionDecl* getDecl() const;
  bool isMemcpy() const;
  bool isMemset() const;
  bool hasStructParam() const;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_FUNCTION_CLANG_H
