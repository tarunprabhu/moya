#include "GlobalVariableClang.h"
#include "ClangUtils.h"
#include "ModuleClang.h"

namespace moya {

GlobalVariableClang::GlobalVariableClang(ModuleBase& module,
                                         const clang::VarDecl* decl,
                                         llvm::GlobalVariable& g)
    : GlobalVariableBase(module), decl(decl) {
  setName(ClangUtils::getDeclName(decl));
  setLocation(ClangUtils::getLocation(decl));
  setLLVM(g);
}

ModuleClang& GlobalVariableClang::getModule() {
  return static_cast<ModuleClang&>(GlobalVariableBase::getModule());
}

const ModuleClang& GlobalVariableClang::getModule() const {
  return static_cast<const ModuleClang&>(GlobalVariableBase::getModule());
}

const clang::VarDecl* GlobalVariableClang::getDecl() const {
  return decl;
}

} // namespace moya
