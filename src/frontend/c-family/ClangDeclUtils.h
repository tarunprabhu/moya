#ifndef MOYA_FRONTEND_C_FAMILY_DECL_UTILS_H
#define MOYA_FRONTEND_C_FAMILY_DECL_UTILS_H

#include "common/STDConfig.h"

#include <clang/AST/ASTContext.h>
#include <clang/AST/Decl.h>
#include <clang/AST/DeclCXX.h>
#include <clang/AST/DeclTemplate.h>

namespace moya {

namespace ClangUtils {

template <typename T,
          std::enable_if_t<std::is_base_of<clang::Decl, T>::value, int> = 0>
const T* getDefinition(const T* decl) {
  return llvm::dyn_cast_or_null<T>(decl->getDefinition());
}

const clang::Type* getDefinition(const clang::Type*);
const clang::FunctionDecl* getTemplateDecl(const clang::FunctionDecl*);

std::string getDeclName(const clang::NamedDecl*);
std::string getQualifiedDeclName(const clang::NamedDecl*);
std::string getFullDeclName(const clang::NamedDecl*);

} // namespace ClangUtils

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_DECL_UTILS_H
