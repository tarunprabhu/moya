#include "ArgumentClang.h"
#include "ClangUtils.h"
#include "FunctionClang.h"

namespace moya {

ArgumentClang::ArgumentClang(const FunctionBase& func,
                             const std::string& name,
                             unsigned num)
    : ArgumentBase(func, name, num), decl(nullptr) {
  ;
}

ArgumentClang::ArgumentClang(const FunctionBase& func,
                             const clang::ParmVarDecl* decl,
                             unsigned num)
    : ArgumentBase(func, decl->getName(), num), decl(decl) {
  setLocation(ClangUtils::getLocation(decl));
}

const FunctionClang& ArgumentClang::getFunction() const {
  return static_cast<const FunctionClang&>(ArgumentBase::getFunction());
}

} // namespace moya
