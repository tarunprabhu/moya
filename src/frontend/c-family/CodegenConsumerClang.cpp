#include "CodegenConsumerClang.h"

using namespace clang;

namespace moya {

#define CC_HANDLE_(ret, fn, atyp)          \
  ret CodegenConsumerClang::fn(atyp arg) { \
    return cg.fn(arg);                     \
  }

CodegenConsumerClang::CodegenConsumerClang(CodeGeneratorClang& cg,
                                           CompilerInstance& compiler)
    : cg(cg), compiler(compiler) {
  ;
}

void CodegenConsumerClang::HandleTranslationUnit(clang::ASTContext& context) {
  getVisitor().TraverseDecl(context.getTranslationUnitDecl());

  cg.HandleTranslationUnit(context);
}

void CodegenConsumerClang::Initialize(clang::ASTContext& context) {
  cg.initialize(compiler);
  cg.Initialize(context);
}

CC_HANDLE_(bool, HandleTopLevelDecl, clang::DeclGroupRef)
CC_HANDLE_(void, HandleInlineFunctionDefinition, clang::FunctionDecl*)
CC_HANDLE_(void, HandleInterestingDecl, clang::DeclGroupRef)
CC_HANDLE_(void, HandleTagDeclDefinition, clang::TagDecl*)
CC_HANDLE_(void, HandleTagDeclRequiredDefinition, const clang::TagDecl*)
CC_HANDLE_(void, HandleImplicitImportDecl, clang::ImportDecl*)
CC_HANDLE_(void, CompleteTentativeDefinition, clang::VarDecl*)

} // namespace moya
