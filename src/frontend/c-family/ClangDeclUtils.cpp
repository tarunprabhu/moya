#include "ClangUtils.h"
#include "common/StringUtils.h"

#include <llvm/Support/raw_ostream.h>

using clang::cast;
using clang::dyn_cast;
using clang::isa;

namespace moya {

namespace ClangUtils {

std::string getDeclName(const clang::NamedDecl* decl) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  decl->printName(ss);

  return ss.str();
}

static std::string stripImplNamespaces(const std::string& name) {
  if(moya::StringUtils::contains(name, "::__cxx11::"))
    return moya::StringUtils::replace(name, "::__cxx11::", "::");
  else if(moya::StringUtils::contains(name, "::__detail::"))
    return moya::StringUtils::replace(name, "::__detail::", "::");
  return name;
}

static std::string getQualifiedDeclName(const clang::VarDecl*);
static std::string getQualifiedDeclName(const clang::FunctionDecl*);
static std::string getQualifiedDeclName(const clang::CXXMethodDecl*);
static std::string getQualifiedDeclName(const clang::CXXRecordDecl*);
static std::string getQualifiedDeclName(const clang::RecordDecl*);
static std::string getQualifiedDeclName(const clang::FunctionTemplateDecl*);
static std::string getQualifiedDeclName(const clang::ClassTemplateDecl*);
static std::string getQualifiedDeclName(const clang::VarTemplateDecl*);

static std::string getQualifiedName(const clang::NamedDecl* decl) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  decl->printQualifiedName(ss);

  return stripImplNamespaces(ss.str());
}

static std::string getQualifiedDeclName(const clang::FunctionTemplateDecl* t) {
  return getQualifiedDeclName(t->getTemplatedDecl());
}

static std::string getQualifiedDeclName(const clang::ClassTemplateDecl* t) {
  return getQualifiedDeclName(t->getTemplatedDecl());
}

static std::string getQualifiedDeclName(const clang::VarTemplateDecl* t) {
  return getQualifiedDeclName(t->getTemplatedDecl());
}

static std::string getQualifiedDeclName(const clang::VarDecl* var) {
  if(const auto* t = var->getTemplateInstantiationPattern())
    return getQualifiedDeclName(t->getDescribedVarTemplate());
  return getQualifiedName(var);
}

static std::string getQualifiedDeclName(const clang::FunctionDecl* f) {
  if(const clang::MemberSpecializationInfo* m
     = f->getMemberSpecializationInfo())
    return getQualifiedName(m->getInstantiatedFrom());
  else if(const clang::FunctionTemplateSpecializationInfo* t
          = f->getTemplateSpecializationInfo())
    return getQualifiedDeclName(t->getTemplate());

  return getQualifiedName(f);
}

static std::string getQualifiedDeclName(const clang::CXXMethodDecl* method) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  ss << getQualifiedDeclName(method->getParent())
     << "::" << method->getNameAsString();

  return ss.str();
}

static std::string getQualifiedDeclName(const clang::CXXRecordDecl* clss) {
  return getQualifiedName(clss);
}

static std::string getQualifiedDeclName(const clang::RecordDecl* record) {
  return getQualifiedName(record);
}

std::string getQualifiedDeclName(const clang::NamedDecl* decl) {
  if(const auto* method = dyn_cast<clang::CXXMethodDecl>(decl))
    return getQualifiedDeclName(method);
  else if(const auto* f = dyn_cast<clang::FunctionDecl>(decl))
    return getQualifiedDeclName(f);
  else if(const auto* clss = dyn_cast<clang::CXXRecordDecl>(decl))
    return getQualifiedDeclName(clss);
  else if(const auto* record = dyn_cast<clang::RecordDecl>(decl))
    return getQualifiedDeclName(record);
  else
    return getQualifiedName(decl);
}

std::string getFullDeclName(const clang::NamedDecl* decl) {
  std::string s;
  llvm::raw_string_ostream ss(s);
  decl->getNameForDiagnostic(
      ss, decl->getASTContext().getPrintingPolicy(), true);
  return ss.str();
}

const clang::Type* getDefinition(const clang::Type* type) {
  if(const clang::RecordDecl* decl = type->getAsRecordDecl())
    if(const clang::RecordDecl* defn = getDefinition(decl))
      return defn->getTypeForDecl();
  return nullptr;
}

const clang::FunctionDecl* getTemplateDecl(const clang::FunctionDecl* decl) {
  if(decl->getMemberSpecializationInfo())
    return decl->getInstantiatedFromMemberFunction();
  else if(const auto* templInfo = decl->getTemplateSpecializationInfo())
    return templInfo->getTemplate()->getTemplatedDecl();
  return nullptr;
}

} // namespace ClangUtils

} // namespace moya
