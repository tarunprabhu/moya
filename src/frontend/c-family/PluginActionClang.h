#ifndef MOYA_FRONTEND_C_FAMILY_PLUGIN_ACTION_CLANG_H
#define MOYA_FRONTEND_C_FAMILY_PLUGIN_ACTION_CLANG_H

#include "common/SourceLang.h"

#include "common/Map.h"

#include <clang/Frontend/CompilerInstance.h>
#include <clang/Frontend/FrontendAction.h>

#include <llvm/Support/raw_ostream.h>

#include <string>
#include <vector>

namespace moya {

class CodeGeneratorClang;
class PragmaConsumer;

class PluginActionClang : public clang::PluginASTAction {
private:
  // This needs to be around for the duration of the action
  std::shared_ptr<clang::CompilerInvocation> ptrInvocation;

protected:
  SourceLang lang;
  CodeGeneratorClang& cg;

protected:
  PluginActionClang() = delete;
  explicit PluginActionClang(SourceLang, CodeGeneratorClang&);

  void initialize(clang::CompilerInstance&);
  void parse1(clang::CompilerInstance&);
  void parse2(clang::CompilerInstance&);
  void codegen(clang::CompilerInstance&);
  void execute(clang::CompilerInstance&);

  bool ParseArgs(const clang::CompilerInstance&,
                 const std::vector<std::string>&) override;
  void PrintHelp(llvm::raw_ostream&);
  std::unique_ptr<clang::ASTConsumer>
  CreateASTConsumer(clang::CompilerInstance&, llvm::StringRef) override;

  virtual clang::PragmaHandler* createPragmaHandler() = 0;
  virtual PragmaConsumer* createPragmaConsumer(clang::CompilerInstance&) = 0;
  virtual clang::ASTConsumer* createCodegenConsumer(clang::CompilerInstance&)
      = 0;
  virtual llvm::Module& runPasses() = 0;

public:
  virtual ~PluginActionClang() = default;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_PLUGIN_ACTION_CLANG_H
