#include "ClangUtils.h"

#include <clang/AST/ASTContext.h>
#include <clang/Basic/SourceManager.h>
#include <clang/Lex/Preprocessor.h>

namespace moya {

namespace ClangUtils {

Location getLocation(const clang::SourceLocation& srcLoc,
                     clang::SourceManager& srcMgr) {
  clang::PresumedLoc loc = srcMgr.getPresumedLoc(srcLoc);
  if(not loc.isValid())
    return Location();

  std::string filename = loc.getFilename();
  unsigned line = loc.getLine();
  unsigned column = loc.getColumn();

  return Location(filename, line, column);
}

Location getLocation(const clang::NamedDecl* decl) {
  return getLocation(decl->getLocation(),
                     decl->getASTContext().getSourceManager());
}

Location getLocation(const clang::Expr* expr, clang::SourceManager& srcMgr) {
  return getLocation(expr->getExprLoc(), srcMgr);
}

Location getLocation(const clang::Stmt* stmt, clang::SourceManager& srcMgr) {
  return getLocation(stmt->getBeginLoc(), srcMgr);
}

Location getLocation(clang::Preprocessor& pp, clang::Token& tok) {
  clang::SourceManager& srcMgr = pp.getSourceManager();
  clang::PresumedLoc loc = srcMgr.getPresumedLoc(tok.getLocation());
  unsigned line = loc.getLine();
  unsigned column = loc.getColumn();
  std::string filename = loc.getFilename();

  return Location(filename, line, column);
}

} // namespace ClangUtils

} // namespace moya
