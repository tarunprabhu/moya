#ifndef MOYA_FRONTEND_C_FAMILY_CODEGEN_VISITOR_CLANG_H
#define MOYA_FRONTEND_C_FAMILY_CODEGEN_VISITOR_CLANG_H

#include "ModuleClang.h"

#include <clang/AST/ASTContext.h>
#include <clang/AST/Decl.h>
#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/AST/Stmt.h>
#include <clang/AST/Type.h>

namespace moya {

class ModuleClang;

// The visitor class is used to identify those statements for which we can
// get extra type information. The main examples of these are calls to new[],
// delete[] where we can easily determine which type is being allocated and
// freed. The other are functions like memcpy() and memset() so we can figure
// out which type are actually begin copied or set.
class CodegenVisitorClang
    : public clang::RecursiveASTVisitor<CodegenVisitorClang> {
protected:
  const clang::RecordDecl* getDefnDecl(const clang::Type*) const;
  const clang::RecordDecl* getDefnDecl(const clang::QualType&) const;

public:
  CodegenVisitorClang() = default;
  CodegenVisitorClang(const CodegenVisitorClang&) = delete;
  virtual ~CodegenVisitorClang() = default;

  virtual ModuleClang& getModule() = 0;
  virtual const ModuleClang& getModule() const = 0;

  virtual bool VisitVarDecl(clang::VarDecl*) = 0;
  virtual bool VisitFunctionDecl(clang::FunctionDecl*) = 0;
  virtual bool VisitRecordDecl(clang::RecordDecl*) = 0;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_CODEGEN_VISITOR_CLANG_H
