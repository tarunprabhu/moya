#ifndef MOYA_FRONTEND_C_FAMILY_PRAGMA_VISITOR_H
#define MOYA_FRONTEND_C_FAMILY_PRAGMA_VISITOR_H

#include "common/Map.h"
#include "common/Set.h"
#include "common/Vector.h"
#include "frontend/annotations/Directives.h"

#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/AST/Stmt.h>
#include <clang/Frontend/CompilerInstance.h>
#include <clang/Rewrite/Core/Rewriter.h>

namespace moya {

class Directive;
class AnnotatorClang;
class DirectiveContext;

class PragmaVisitor : public clang::RecursiveASTVisitor<PragmaVisitor> {
protected:
  moya::Set<const clang::FileEntry*> modified;
  clang::SourceManager& srcMgr;
  AnnotatorClang& annotator;
  DirectiveContext& directives;

protected:
  void addModified(const clang::SourceLocation&);
  bool isDirectiveBefore(const Directive*, const clang::Stmt*);

public:
  explicit PragmaVisitor(clang::CompilerInstance&,
                         AnnotatorClang&,
                         DirectiveContext&);

  moya::Map<const clang::FileEntry*, std::string> getModified() const;

  virtual bool VisitCompoundStmt(clang::CompoundStmt*);
  virtual bool VisitForStmt(clang::ForStmt*);
  virtual bool VisitDoStmt(clang::DoStmt*);
  virtual bool VisitWhileStmt(clang::WhileStmt*);
};

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_PRAGMA_VISITOR_H
