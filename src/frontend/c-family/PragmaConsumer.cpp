#include "PragmaConsumer.h"
#include "common/Diagnostics.h"

using namespace clang;

namespace moya {

PragmaConsumer::PragmaConsumer(CompilerInstance& compiler,
                               AnnotatorClang& annotator,
                               DirectiveContext& directives)
    : compiler(compiler), visitor(compiler, annotator, directives) {
  ;
}

void PragmaConsumer::HandleTranslationUnit(ASTContext& context) {
  visitor.TraverseDecl(context.getTranslationUnitDecl());

  // XXX: If there were any errors while parsing, we shouldn't proceed further.
  // Probably because of the hacky way I have set things up, it causes an
  // error in Sema which I can't figure out. To work around it, we abort
  // right here, but we need to clean up before we do that.
  if(context.getDiagnostics().hasErrorOccurred()) {
    compiler.clearOutputFiles(true);
    exit(1);
  }
}

Map<const FileEntry*, std::string> PragmaConsumer::getModifiedFiles() const {
  return visitor.getModified();
}

} // namespace moya
