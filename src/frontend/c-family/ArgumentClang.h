#ifndef MOYA_FRONTEND_C_FAMILY_ARGUMENT_CLANG_H
#define MOYA_FRONTEND_C_FAMILY_ARGUMENT_CLANG_H

#include "frontend/components/ArgumentBase.h"

#include <clang/AST/Decl.h>

namespace moya {

class FunctionClang;

class ArgumentClang : public ArgumentBase {
protected:
  const clang::ParmVarDecl* decl;

protected:
  ArgumentClang(const FunctionBase&, const std::string&, unsigned);
  ArgumentClang(const FunctionBase&, const clang::ParmVarDecl*, unsigned);

public:
  ArgumentClang(const ArgumentClang&) = delete;
  virtual ~ArgumentClang() = default;

  const FunctionClang& getFunction() const;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_ARGUMENT_CLANG_H
