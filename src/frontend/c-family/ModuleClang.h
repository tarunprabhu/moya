#ifndef MOYA_FRONTEND_C_FAMILY_MODULE_CLANG_H
#define MOYA_FRONTEND_C_FAMILY_MODULE_CLANG_H

#include "CodeGeneratorClang.h"
#include "common/Map.h"
#include "frontend/components/ModuleBase.h"

#include <clang/AST/Decl.h>
#include <clang/AST/Expr.h>
#include <clang/CodeGen/CodeGenABITypes.h>
#include <clang/CodeGen/ModuleBuilder.h>

#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>

namespace moya {

class ArgumentClang;
class FunctionClang;
class GlobalVariableClang;
class TypeClang;

class ModuleClang : public ModuleBase {
private:
  CodeGeneratorClang& cg;

protected:
  Map<const clang::FunctionDecl*, FunctionClang*> clangDeclMap;
  Map<const clang::FunctionDecl*, FunctionClang*> clangFunctionMap;
  Map<const clang::NamedDecl*, GlobalVariableClang*> clangGlobalMap;
  Map<const clang::Type*, TypeClang*> clangTypeMap;
  Map<const clang::RecordDecl*, TypeClang*> clangTypeDeclMap;

protected:
  ModuleClang(SourceLang, CodeGeneratorClang&);

public:
  ModuleClang(const ModuleClang&) = delete;
  virtual ~ModuleClang() = default;

  template <typename FunctionT, typename... ArgsT>
  FunctionT& addDeclaration(const clang::FunctionDecl* decl, ArgsT&&... args) {
    static_assert(std::is_base_of<FunctionClang, FunctionT>::value,
                  "Function type must be subclass of FunctionClang");
    FunctionT& f = ModuleBase::addDeclaration<FunctionT>(decl, args...);
    clangDeclMap[f.getDecl()] = &f;

    return f;
  }

  template <typename FunctionT, typename... ArgsT>
  FunctionT& addFunction(const clang::FunctionDecl* decl, ArgsT&&... args) {
    static_assert(std::is_base_of<FunctionClang, FunctionT>::value,
                  "Function type must be subclass of FunctionClang");
    FunctionT& f = ModuleBase::addFunction<FunctionT>(decl, args...);
    clangFunctionMap[f.getDecl()] = &f;

    return f;
  }

  using ModuleBase::hasDeclaration;
  bool hasDeclaration(const clang::FunctionDecl*) const;
  FunctionClang& getDeclaration(llvm::Function&);
  FunctionClang& getDeclaration(llvm::Function*);
  FunctionClang& getDeclaration(const clang::FunctionDecl*);
  const FunctionClang& getDeclaration(llvm::Function&) const;
  const FunctionClang& getDeclaration(llvm::Function*) const;
  const FunctionClang& getDeclaration(const clang::FunctionDecl*) const;

  using ModuleBase::hasFunction;
  bool hasFunction(const clang::FunctionDecl*) const;
  FunctionClang& getFunction(llvm::Function&);
  FunctionClang& getFunction(llvm::Function*);
  FunctionClang& getFunction(const clang::FunctionDecl*);
  const FunctionClang& getFunction(llvm::Function&) const;
  const FunctionClang& getFunction(llvm::Function*) const;
  const FunctionClang& getFunction(const clang::FunctionDecl*) const;

  template <typename GlobalT, typename... ArgsT>
  GlobalT& addGlobal(const clang::VarDecl* decl, ArgsT&&... args) {
    static_assert(
        std::is_base_of<GlobalVariableClang, GlobalT>::value,
        "Global variable type must be subclass of GlobalVariableClang");
    GlobalT& g = ModuleBase::addGlobal<GlobalT>(decl, args...);
    clangGlobalMap[g.getDecl()] = &g;

    return g;
  }

  using ModuleBase::hasGlobal;
  bool hasGlobal(const clang::NamedDecl*) const;
  GlobalVariableClang& getGlobal(llvm::GlobalVariable&);
  GlobalVariableClang& getGlobal(llvm::GlobalVariable*);
  GlobalVariableClang& getGlobal(const clang::NamedDecl*);
  const GlobalVariableClang& getGlobal(llvm::GlobalVariable&) const;
  const GlobalVariableClang& getGlobal(llvm::GlobalVariable*) const;
  const GlobalVariableClang& getGlobal(const clang::NamedDecl*) const;

  template <typename TypeT, typename... ArgsT>
  TypeT& addType(const clang::Type* type, ArgsT&&... args) {
    static_assert(std::is_base_of<TypeClang, TypeT>::value,
                  "Type type must be subclass of TypeClang");

    TypeT& t = ModuleBase::addType<TypeT>(type, args...);
    addTypeMapping(type, t);

    return t;
  }

  void addTypeMapping(const clang::Type*, TypeClang&);

  using ModuleBase::hasType;
  bool hasType(const clang::Type* type) const;
  bool hasType(const clang::RecordDecl* decl) const;
  TypeClang& getType(llvm::Type* type);
  TypeClang& getType(const clang::Type* type);
  TypeClang& getType(const clang::RecordDecl* decl);
  const TypeClang& getType(llvm::Type* type) const;
  const TypeClang& getType(const clang::Type* type) const;
  const TypeClang& getType(const clang::RecordDecl* decl) const;

  llvm::Type* convertToLLVM(const clang::Type*, bool = false);
  template <typename T>
  T* convertToLLVMAs(const clang::Type* type, bool shadow) {
    return llvm::dyn_cast<T>(convertToLLVM(type, shadow));
  }

  const clang::Decl* getDecl(const std::string&);

  const clang::ASTContext& getASTContext() const;
  const clang::SourceManager& getSourceManager() const;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_PROGRAM_ELEMENTS_CLANG_H
