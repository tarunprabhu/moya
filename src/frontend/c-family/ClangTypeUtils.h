#ifndef MOYA_FRONTEND_C_FAMILY_CLANG_TYPE_UTILS_H
#define MOYA_FRONTEND_C_FAMILY_CLANG_TYPE_UTILS_H

#include "common/STDConfig.h"
#include "common/Vector.h"

#include <clang/AST/ASTContext.h>
#include <clang/AST/Expr.h>
#include <clang/AST/Stmt.h>
#include <clang/AST/Type.h>

namespace moya {

namespace ClangUtils {

std::string getFullTypeName(const clang::Type*, clang::ASTContext&);
std::string getQualifiedTypeName(const clang::Type*, clang::ASTContext&);

moya::Vector<const clang::Type*> flatten(const clang::Type*);
unsigned getTypeDepth(const clang::QualType&);
unsigned getTypeDepth(const clang::Type*);
const clang::Type* getInnermost(const clang::QualType&);
const clang::Type* getInnermost(const clang::Type*);
const clang::Type* getUnderlyingType(const clang::TypedefNameDecl*);

} // namespace ClangUtils

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_CLANG_TYPE_UTILS_H
