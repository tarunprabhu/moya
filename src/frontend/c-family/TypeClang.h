#ifndef MOYA_FRONTEND_C_FAMILY_TYPE_CLANG_H
#define MOYA_FRONTEND_C_FAMILY_TYPE_CLANG_H

#include "frontend/components/TypeBase.h"

#include <clang/AST/Decl.h>

#include <string>

namespace moya {

class ModuleBase;
class ModuleClang;

class TypeClang : public TypeBase {
protected:
  const clang::RecordDecl* decl;
  const clang::Type* type;
  const clang::QualType qt;
  std::string name;

protected:
  TypeClang(ModuleBase&, const clang::Type*);

public:
  TypeClang(const TypeClang&) = delete;
  virtual ~TypeClang() = default;

  ModuleClang& getModule();
  const ModuleClang& getModule() const;

  bool hasDecl() const;
  bool hasName() const;
  bool isStruct() const;
  bool isUnion() const;
  const clang::Type* getType() const;
  const clang::QualType& getQualType() const;
  const clang::RecordDecl* getDecl() const;
  const std::string& getName() const;

  std::string str() const;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_TYPE_CLANG_H
