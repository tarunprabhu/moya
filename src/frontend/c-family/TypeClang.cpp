#include "TypeClang.h"
#include "ClangUtils.h"
#include "ModuleClang.h"

namespace moya {

TypeClang::TypeClang(ModuleBase& module, const clang::Type* type)
    : TypeBase(module), decl(type->getAsRecordDecl()), type(type), qt(type, 0),
      name("") {
  if(const clang::RecordDecl* record = type->getAsRecordDecl()) {
    name = record->getNameAsString();
    loc = ClangUtils::getLocation(record);
  }
}

ModuleClang& TypeClang::getModule() {
  return static_cast<ModuleClang&>(TypeBase::getModule());
}

const ModuleClang& TypeClang::getModule() const {
  return static_cast<const ModuleClang&>(TypeBase::getModule());
}

bool TypeClang::hasDecl() const {
  return decl;
}

bool TypeClang::hasName() const {
  return name.length();
}

bool TypeClang::isStruct() const {
  return not isUnion();
}

bool TypeClang::isUnion() const {
  return type->isUnionType();
}

const clang::Type* TypeClang::getType() const {
  return type;
}

const clang::QualType& TypeClang::getQualType() const {
  return qt;
}

const clang::RecordDecl* TypeClang::getDecl() const {
  return decl;
}

const std::string& TypeClang::getName() const {
  return name;
}

std::string TypeClang::str() const {
  return qt.getAsString();
}

} // namespace moya
