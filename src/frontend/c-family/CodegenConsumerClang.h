#ifndef MOYA_FRONTEND_C_FAMILY_CODEGEN_CONSUMER_CLANG_H
#define MOYA_FRONTEND_C_FAMILY_CODEGEN_CONSUMER_CLANG_H

#include "CodeGeneratorClang.h"
#include "CodegenVisitorClang.h"
#include "ModuleClang.h"

#include <clang/AST/ASTConsumer.h>
#include <clang/Frontend/CompilerInstance.h>

namespace moya {

class CodegenConsumerClang : public clang::ASTConsumer {
private:
  CodeGeneratorClang& cg;
  clang::CompilerInstance& compiler;

public:
  CodegenConsumerClang(CodeGeneratorClang&, clang::CompilerInstance&);
  CodegenConsumerClang(const CodegenConsumerClang&) = delete;
  virtual ~CodegenConsumerClang() = default;

  virtual CodegenVisitorClang& getVisitor() = 0;

  virtual void HandleTranslationUnit(clang::ASTContext&);
  virtual void Initialize(clang::ASTContext&);
  virtual bool HandleTopLevelDecl(clang::DeclGroupRef);
  virtual void HandleInlineFunctionDefinition(clang::FunctionDecl*);
  virtual void HandleInterestingDecl(clang::DeclGroupRef);
  virtual void HandleTagDeclDefinition(clang::TagDecl*);
  virtual void HandleTagDeclRequiredDefinition(const clang::TagDecl*);
  virtual void HandleImplicitImportDecl(clang::ImportDecl*);
  virtual void CompleteTentativeDefinition(clang::VarDecl*);
};

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_CODEGEN_CONSUMER_CLANG_H
