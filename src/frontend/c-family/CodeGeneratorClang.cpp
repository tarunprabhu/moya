#include "CodeGeneratorClang.h"

#include <clang/Frontend/CompilerInstance.h>

using namespace llvm;

namespace moya {

CodeGeneratorClang::CodeGeneratorClang(SourceLang lang,
                                       AnnotatorClang& annotator)
    : lang(lang), annotator(annotator), astContext(nullptr), codegen(nullptr),
      shadowCodegen(nullptr) {
  ;
}

void CodeGeneratorClang::initialize(clang::CompilerInstance& compiler) {
  clang::DiagnosticsEngine& diags = compiler.getDiagnostics();
  llvm::StringRef file = compiler.getFrontendOpts().Inputs.at(0).getFile();
  clang::HeaderSearchOptions& optsHeader = compiler.getHeaderSearchOpts();
  clang::PreprocessorOptions& optsPP = compiler.getPreprocessorOpts();
  clang::CodeGenOptions& optsCodegen = compiler.getCodeGenOpts();

  astContext = &compiler.getASTContext();
  codegen.reset(
      CreateLLVMCodeGen(diags, file, optsHeader, optsPP, optsCodegen, context));
  shadowCodegen.reset(CreateLLVMCodeGen(
      diags, file, optsHeader, optsPP, optsCodegen, shadowContext));

  annotator.initialize(compiler);
}

SourceLang CodeGeneratorClang::getSourceLang() const {
  return lang;
}

llvm::LLVMContext& CodeGeneratorClang::getLLVMContext() {
  return context;
}

llvm::Module& CodeGeneratorClang::getLLVMModule() {
  return *codegen->GetModule();
}

clang::CodeGenerator& CodeGeneratorClang::getCodeGenerator(bool shadow) {
  if(shadow)
    return *shadowCodegen.get();
  else
    return *codegen.get();
}

clang::CodeGen::CodeGenModule&
CodeGeneratorClang::getCodegenModule(bool shadow) {
  if(shadow)
    return shadowCodegen->CGM();
  else
    return codegen->CGM();
}

clang::ASTContext& CodeGeneratorClang::getASTContext() {
  return *astContext;
}

const clang::ASTContext& CodeGeneratorClang::getASTContext() const {
  return *astContext;
}

} // namespace moya
