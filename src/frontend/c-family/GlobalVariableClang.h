#ifndef MOYA_FRONTEND_C_FAMILY_GLOBAL_VARIABLE_CLANG_H
#define MOYA_FRONTEND_C_FAMILY_GLOBAL_VARIABLE_CLANG_H

#include "frontend/components/GlobalVariableBase.h"

#include <clang/AST/Decl.h>

#include <llvm/IR/Function.h>

namespace moya {

class ModuleBase;
class ModuleClang;

class GlobalVariableClang : public GlobalVariableBase {
protected:
  const clang::VarDecl* decl;

protected:
  GlobalVariableClang(ModuleBase&,
                      const clang::VarDecl*,
                      llvm::GlobalVariable&);

public:
  GlobalVariableClang(const GlobalVariableClang&) = delete;
  virtual ~GlobalVariableClang() = default;

  ModuleClang& getModule();
  const ModuleClang& getModule() const;
  const clang::VarDecl* getDecl() const;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_GLOBAL_VARIABLE_CLANG_H
