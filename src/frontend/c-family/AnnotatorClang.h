#ifndef MOYA_FRONTEND_C_FAMILY_ANNOTATOR_CLANG_H
#define MOYA_FRONTEND_C_FAMILY_ANNOTATOR_CLANG_H

#include "common/APITypes.h"
#include "common/Config.h"
#include "common/SourceLang.h"
#include "common/Vector.h"

#include <clang/AST/Stmt.h>
#include <clang/Frontend/CompilerInstance.h>
#include <clang/Rewrite/Core/Rewriter.h>

namespace moya {

class AnnotatorClang {
public:
  enum class NewLine : int {
    // These values also function as bitfields
    None = 0x0,
    Append = 0x1,
    Prepend = 0x2,
    BothEnds = 0x3,
  };

protected:
  SourceLang lang;

  // Because clang's AST is immutable, we would have to write the
  // the file with Moya's pragmas replaced with calls to its runtime API
  // to disk and re-read. One workaround is to replace the memory buffer
  // with the file contents with the new memory buffer containing the annotated
  // file.
  clang::Rewriter rewriter;

protected:
  AnnotatorClang(SourceLang);

  virtual std::string generateFunctionCall(const std::string&,
                                           const Vector<std::string>&,
                                           const std::string&,
                                           NewLine = NewLine::None)
      = 0;
  void initialize(clang::SourceManager&, clang::LangOptions&);

  std::string genSentinelStartCall(MoyaID);
  std::string genSentinelStopCall(MoyaID);

public:
  virtual ~AnnotatorClang() = default;

  clang::Rewriter& getRewriter();

  void generateEnterRegion(const clang::Stmt*, RegionID);
  void generateExitRegion(const clang::Stmt*, RegionID);
  void generateBeginLoop(const clang::SourceLocation&, LoopID);
  void generateEndLoop(const clang::SourceLocation&, LoopID);

  virtual void initialize(clang::CompilerInstance&) = 0;
};

AnnotatorClang::NewLine operator&(const AnnotatorClang::NewLine&,
                                  const AnnotatorClang::NewLine&);

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_ANNOTATOR_CLANG_H
