#ifndef MOYA_FRONTEND_C_FAMILY_PRAGMA_CONSUMER_H
#define MOYA_FRONTEND_C_FAMILY_PRAGMA_CONSUMER_H

#include "AnnotatorClang.h"
#include "PragmaVisitor.h"
#include "frontend/components/DirectiveContext.h"

#include <clang/AST/ASTConsumer.h>
#include <clang/Frontend/CompilerInstance.h>

namespace moya {

class PragmaConsumer : public clang::ASTConsumer {
private:
  clang::CompilerInstance& compiler;
  PragmaVisitor visitor;

public:
  explicit PragmaConsumer(clang::CompilerInstance&,
                          AnnotatorClang&,
                          DirectiveContext&);
  virtual ~PragmaConsumer() = default;

  Map<const clang::FileEntry*, std::string> getModifiedFiles() const;
  virtual void HandleTranslationUnit(clang::ASTContext&);
};

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_PRAGMA_CONSUMER_H
