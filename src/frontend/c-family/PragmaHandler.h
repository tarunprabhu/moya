#ifndef MOYA_FRONTEND_C_FAMILY_PRAGMA_HANDLER_H
#define MOYA_FRONTEND_C_FAMILY_PRAGMA_HANDLER_H

#include "frontend/components/DirectiveContext.h"

#include <clang/Lex/Pragma.h>

namespace moya {

class PragmaHandler : public clang::PragmaHandler {
private:
  SourceLang lang;
  DirectiveContext& directives;

public:
  PragmaHandler(SourceLang, DirectiveContext&);
  virtual ~PragmaHandler() = default;

  void HandlePragma(clang::Preprocessor&,
                    clang::PragmaIntroducerKind,
                    clang::Token&);
};

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_PRAGMA_HANDLER_H
