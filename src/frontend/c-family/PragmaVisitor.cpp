#include "PragmaVisitor.h"
#include "AnnotatorClang.h"
#include "ClangUtils.h"
#include "frontend/components/DirectiveContext.h"

using llvm::cast_or_null;

namespace moya {

PragmaVisitor::PragmaVisitor(clang::CompilerInstance& compiler,
                             AnnotatorClang& annotator,
                             DirectiveContext& directives)
    : srcMgr(compiler.getSourceManager()), annotator(annotator),
      directives(directives) {
  ;
}

Map<const clang::FileEntry*, std::string> PragmaVisitor::getModified() const {
  Map<const clang::FileEntry*, std::string> ret;
  clang::Rewriter& rewriter = annotator.getRewriter();
  for(const clang::FileEntry* entry : modified) {
    std::string s;
    llvm::raw_string_ostream ss(s);
    clang::FileID id = srcMgr.translateFile(entry);
    rewriter.getRewriteBufferFor(id)->write(ss);
    ret[entry] = ss.str();
  }

  return ret;
}

void PragmaVisitor::addModified(const clang::SourceLocation& loc) {
  modified.insert(srcMgr.getFileEntryForID(srcMgr.getFileID(loc)));
}

bool PragmaVisitor::isDirectiveBefore(const Directive* directive,
                                      const clang::Stmt* stmt) {
  const Location& l1 = directive->getLocation();
  Location l2 = ClangUtils::getLocation(stmt, srcMgr);
  if(Location::compare(l1, l2) == Location::Cmp::Before)
    return true;
  return false;
}

bool PragmaVisitor::VisitCompoundStmt(clang::CompoundStmt* stmt) {
  for(auto* region : directives.popNearest<RegionDirective>(
          ClangUtils::getLocation(stmt, srcMgr))) {
    annotator.generateEnterRegion(stmt, region->getRegionID());
    annotator.generateExitRegion(stmt, region->getRegionID());
    region->setBeginLoc(ClangUtils::getLocation(stmt->getBeginLoc(), srcMgr));
    region->setEndLoc(ClangUtils::getLocation(stmt->getEndLoc(), srcMgr));
    addModified(stmt->getBeginLoc());
  }

  return true;
}

bool PragmaVisitor::VisitForStmt(clang::ForStmt* stmt) {
  for(auto* loop : directives.popNearest<LoopDirective>(
          ClangUtils::getLocation(stmt, srcMgr))) {
    annotator.generateBeginLoop(stmt->getBeginLoc(), loop->getLoopID());
    annotator.generateEndLoop(stmt->getEndLoc().getLocWithOffset(1),
                              loop->getLoopID());
    addModified(stmt->getBeginLoc());
  }

  return true;
}

bool PragmaVisitor::VisitDoStmt(clang::DoStmt* stmt) {
  for(auto* loop : directives.popNearest<LoopDirective>(
          ClangUtils::getLocation(stmt, srcMgr))) {
    // FIXME: Support do-while loops.
    //
    // The problem here is that I can't figure out how to find the
    // end of the statement in a reasonable way. All of the functions in
    // clang::DoStmt at best get you the location of the right parenthesis
    // in the trailing while statement but not the location of the
    // following semicolon. It can be done, but not now.
    moya_warning("Loop directives for do-while loops are not supported");
  }

  return true;
}

bool PragmaVisitor::VisitWhileStmt(clang::WhileStmt* stmt) {
  for(auto* loop : directives.popNearest<LoopDirective>(
          ClangUtils::getLocation(stmt, srcMgr))) {
    annotator.generateBeginLoop(stmt->getBeginLoc(), loop->getLoopID());
    annotator.generateEndLoop(stmt->getBody()->getEndLoc().getLocWithOffset(1),
                              loop->getLoopID());
    addModified(stmt->getBeginLoc());
  }

  return true;
}

} // namespace moya
