#ifndef MOYA_FRONTEND_C_FAMILY_CLANG_LOCATION_UTILS_H
#define MOYA_FRONTEND_C_FAMILY_CLANG_LOCATION_UTILS_H

#include "common/Location.h"

#include <clang/AST/Decl.h>
#include <clang/AST/Expr.h>
#include <clang/AST/Stmt.h>
#include <clang/Basic/SourceManager.h>

namespace moya {

namespace ClangUtils {

Location getLocation(const clang::SourceLocation&, clang::SourceManager&);
Location getLocation(const clang::NamedDecl*);
Location getLocation(const clang::Expr*, clang::SourceManager&);
Location getLocation(const clang::Stmt*, clang::SourceManager&);

} // namespace ClangUtils

} // namespace moya

#endif // MOYA_FRONTEND_C_FAMILY_CLANG_LOCATION_UTILS_H
