#include "PluginActionClang.h"
#include "CodeGeneratorClang.h"
#include "PragmaConsumer.h"
#include "common/Diagnostics.h"
#include "common/PassManager.h"
#include "frontend/annotations/Sentinel.h"

#include <clang/Basic/Diagnostic.h>
#include <clang/CodeGen/BackendUtil.h>
#include <clang/Frontend/CompilerInstance.h>
#include <clang/Frontend/TextDiagnosticPrinter.h>
#include <clang/Lex/Preprocessor.h>
#include <clang/Lex/PreprocessorOptions.h>
#include <clang/Parse/ParseAST.h>
#include <clang/Sema/Sema.h>

#include <sys/stat.h>

#include <vector>

namespace moya {

static clang::BackendAction getBackendAction(clang::frontend::ActionKind kind) {
  switch(kind) {
  case clang::frontend::ActionKind::EmitObj:
    return clang::BackendAction::Backend_EmitObj;
  case clang::frontend::ActionKind::EmitBC:
    return clang::BackendAction::Backend_EmitBC;
  case clang::frontend::ActionKind::EmitLLVM:
    return clang::BackendAction::Backend_EmitLL;
  case clang::frontend::ActionKind::EmitAssembly:
    return clang::BackendAction::Backend_EmitAssembly;
  case clang::frontend::ActionKind::EmitCodeGenOnly:
    return clang::BackendAction::Backend_EmitMCNull;
  default:
    moya_error("Unsupported frontend action: " << kind);
  }

  return clang::BackendAction::Backend_EmitNothing;
}

PluginActionClang::PluginActionClang(SourceLang lang, CodeGeneratorClang& cg)
    : ptrInvocation(nullptr), lang(lang), cg(cg) {
  ;
}

void PluginActionClang::initialize(clang::CompilerInstance& ci) {
  cg.initialize(ci);
}

void PluginActionClang::parse1(clang::CompilerInstance& compiler) {
  moya_message("PluginActionClang::parse1");

  clang::Preprocessor& pp = compiler.getPreprocessor();
  pp.AddPragmaHandler(createPragmaHandler());
  pp.getBuiltinInfo().initializeBuiltins(pp.getIdentifierTable(),
                                         pp.getLangOpts());

  auto* pragmaConsumer = createPragmaConsumer(compiler);
  compiler.setASTConsumer(std::unique_ptr<clang::ASTConsumer>(pragmaConsumer));
  compiler.createSema(getTranslationUnitKind(), nullptr);

  compiler.getDiagnosticClient().BeginSourceFile(compiler.getLangOpts(), &pp);

  ParseAST(compiler.getSema(),
           compiler.getFrontendOpts().ShowStats,
           compiler.getFrontendOpts().SkipFunctionBodies);

  compiler.getDiagnosticClient().EndSourceFile();

  // Reset compiler state before parse2()
  compiler.takeSema();
  compiler.createPreprocessor(getTranslationUnitKind());
  compiler.createASTContext();

  clang::SourceManager& srcMgr = compiler.getSourceManager();
  clang::FileManager& fileMgr = srcMgr.getFileManager();
  clang::FileID mainId = srcMgr.getMainFileID();

  // Not sure why SourceManager::overrideFileContents isn't working as I expect
  // but it causes all the source locations to turn to garbage. Maybe there is a
  // cache entry somewhere that isn't getting cleared. Anyway, to get around it,
  // invalidate all the FileEntry's for the modified files, get rid of them
  // and recreate the them
  Vector<std::tuple<const clang::FileEntry*,
                    std::string,
                    clang::SrcMgr::CharacteristicKind,
                    bool>>
      newIds;
  const Map<const clang::FileEntry*, std::string>& modified
      = pragmaConsumer->getModifiedFiles();
  for(const clang::FileEntry* file : modified.keys()) {
    clang::FileID oldId = srcMgr.translateFile(file);
    bool isMain = (oldId == mainId);
    auto kind
        = srcMgr.getFileCharacteristic(srcMgr.getLocForStartOfFile(oldId));

    // The clang::FileEntry* object will be invalid after calling
    // invalidateCache() but the value of the pointer is what we need and that
    // is only to lookup the modified map
    newIds.emplace_back(file, file->getName(), kind, isMain);

    fileMgr.invalidateCache(file);
  }

  for(const auto& it : newIds) {
    const auto* file = std::get<const clang::FileEntry*>(it);
    const std::string& contents = modified.at(file);
    const auto& name = std::get<std::string>(it);
    const auto& kind = std::get<clang::SrcMgr::CharacteristicKind>(it);
    bool isMain = std::get<bool>(it);

    auto memBuf(llvm::MemoryBuffer::getMemBufferCopy(contents, name));
    clang::FileID newId = srcMgr.createFileID(std::move(memBuf), kind);
    if(isMain)
      srcMgr.setMainFileID(newId);
  }
}

void PluginActionClang::parse2(clang::CompilerInstance& compiler) {
  moya_message("PluginActionClang::parse2");

  // Any diagnostics will have already been printed in parse1().
  // We don't want to print them again. Besides which, the line numbers will
  // likely be "off" because the code will have been annotated, so definitely
  // don't want the user seeing that confusing gunk
  compiler.getDiagnostics().setClient(new clang::IgnoringDiagConsumer(), true);

  // In the first parse, we will have handled all the Moya directives
  // The region directives will have been translated into sentinel calls
  // The function and loop directives will have been collected and will be used
  // by the CodegenConsumer, so we don't need to handle the pragma's here again
  clang::Preprocessor& pp = compiler.getPreprocessor();
  pp.AddPragmaHandler(new clang::EmptyPragmaHandler(Sentinel::get(lang)));
  pp.getBuiltinInfo().initializeBuiltins(pp.getIdentifierTable(),
                                         pp.getLangOpts());

  compiler.setASTConsumer(
      std::unique_ptr<clang::ASTConsumer>(createCodegenConsumer(compiler)));
  compiler.createSema(getTranslationUnitKind(), nullptr);

  compiler.getDiagnosticClient().BeginSourceFile(pp.getLangOpts(), &pp);

  ParseAST(compiler.getSema(),
           compiler.getFrontendOpts().ShowStats,
           compiler.getFrontendOpts().SkipFunctionBodies);

  compiler.getDiagnosticClient().EndSourceFile();
}

void PluginActionClang::codegen(clang::CompilerInstance& compiler) {
  moya_message("PluginActionClang::codegen");

  llvm::Module& llvmModule = runPasses();

  EmitBackendOutput(
      compiler.getDiagnostics(),
      compiler.getHeaderSearchOpts(),
      compiler.getCodeGenOpts(),
      compiler.getTargetOpts(),
      compiler.getLangOpts(),
      llvmModule.getDataLayout(),
      &llvmModule,
      getBackendAction(compiler.getFrontendOpts().ProgramAction),
      compiler.createOutputFile(
          compiler.getFrontendOpts().OutputFile, true, true, "", "", false));
}

void PluginActionClang::execute(clang::CompilerInstance& compiler) {
  initialize(compiler);

  // The first parse takes care of the Moya pragmas and correctly deals with the
  // annotated regions. At the end of this, the file will be rewritten in
  // memory with the region annotations replaced by calls to sentinel functions
  // But because the AST is immutable, the sentinel calls will not appear there
  // The Moya pragmas will also have been collected
  parse1(compiler);

  // To get the sentinel calls into the AST, we need to reparse the files.
  // This is slightly different from the first parse because We need to
  // force debug information to be emitted. The context object inside the
  // compiler instance will contain the updated AST which will be used to
  // instrument the LLVM-IR in the codegen step
  parse2(compiler);

  // The parses will have collected data about the program elements from the
  // AST. This will be used to add metadata to the LLVM code which will be
  // used in the static analysis. The backend emitter will also be run to
  // write output (if required) in the right format
  codegen(compiler);
}

std::unique_ptr<clang::ASTConsumer>
PluginActionClang::CreateASTConsumer(clang::CompilerInstance& compiler,
                                     llvm::StringRef) {
  execute(compiler);

  // We have emitted everything correctly at this point, so just terminate
  exit(0);

  // Will never get here but need to keep the compiler happy.
  return nullptr;
}

bool PluginActionClang::ParseArgs(const clang::CompilerInstance&,
                                  const std::vector<std::string>&) {
  return true;
}

void PluginActionClang::PrintHelp(llvm::raw_ostream& ros) {
  ros << "Need to add -moya flag to parse Moya annotations\n";
}

} // namespace moya
