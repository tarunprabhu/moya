#include "ClangUtils.h"

#include <llvm/Support/raw_ostream.h>

using llvm::cast;
using llvm::dyn_cast;
using llvm::isa;

namespace moya {

namespace ClangUtils {

std::string getFullTypeName(const clang::Type* type,
                            clang::ASTContext& context) {
  std::string s;
  llvm::raw_string_ostream ss(s);

  if(const clang::CXXRecordDecl* clss = type->getAsCXXRecordDecl())
    ss << getFullDeclName(clss);
  else if(const clang::RecordType* rt = type->getAsStructureType())
    ss << getFullDeclName(rt->getDecl());
  else
    clang::QualType(type, 0).print(ss, context.getPrintingPolicy());

  return ss.str();
}

std::string getQualifiedTypeName(const clang::Type* type,
                                 clang::ASTContext& context) {
  std::string buf;
  llvm::raw_string_ostream ss(buf);

  if(const clang::CXXRecordDecl* clss = type->getAsCXXRecordDecl())
    ss << getQualifiedDeclName(clss);
  else if(const clang::RecordType* rt = type->getAsStructureType())
    ss << getQualifiedDeclName(rt->getDecl());
  else
    clang::QualType(type, 0).print(ss, context.getPrintingPolicy());

  return ss.str();
}

static void flatten(const clang::Type* type,
                    moya::Vector<const clang::Type*>& flattened) {
  if(const auto* recordTy = type->getAs<clang::RecordType>()) {
    const clang::RecordDecl* record = recordTy->getDecl();
    unsigned numFields = flattened.size();
    for(const clang::FieldDecl* field : record->fields())
      flatten(field->getType().getTypePtr(), flattened);
    // Empty structs will not have added any fields, in which case we will need
    // to add the struct itself
    if(numFields == flattened.size())
      flattened.push_back(type);
    return;
  }
  flattened.push_back(type);
}

moya::Vector<const clang::Type*> flatten(const clang::Type* type) {
  moya::Vector<const clang::Type*> flattened;

  flatten(type, flattened);

  return flattened;
}

static unsigned getTypeDepth(const clang::Type* type, unsigned depth) {
  if(type->isPointerType() or type->isArrayType()) {
    const clang::Type* inner = type->getPointeeOrArrayElementType();
    return getTypeDepth(inner->getUnqualifiedDesugaredType(), depth + 1);
  } else if(type->isReferenceType()) {
    auto q = clang::QualType(type, 0).getNonReferenceType();
    return getTypeDepth(q->getUnqualifiedDesugaredType(), depth + 1);
  }
  return depth;
}

static unsigned getTypeDepth(const clang::QualType& q, unsigned depth) {
  return getTypeDepth(q->getUnqualifiedDesugaredType(), depth);
}

unsigned getTypeDepth(const clang::Type* type) {
  return getTypeDepth(type, 0);
}

unsigned getTypeDepth(const clang::QualType& q) {
  return getTypeDepth(q, 0);
}

const clang::Type* getInnermost(const clang::Type* type) {
  if(type->isPointerType() or type->isArrayType()) {
    const clang::Type* inner = type->getPointeeOrArrayElementType();
    return getInnermost(inner->getUnqualifiedDesugaredType());
  } else if(type->isReferenceType()) {
    auto q = clang::QualType(type, 0).getNonReferenceType();
    return getInnermost(q->getUnqualifiedDesugaredType());
  }
  return type;
}

const clang::Type* getInnermost(const clang::QualType& q) {
  return getInnermost(q->getUnqualifiedDesugaredType());
}

uint64_t getNumFlattenedElements(const clang::Type* type) {
  if(auto* aty = type->getAsArrayTypeUnsafe())
    if(auto* cty = dyn_cast<clang::ConstantArrayType>(aty))
      return cty->getSize().getLimitedValue()
             * getNumFlattenedElements(aty->getPointeeOrArrayElementType());
  return 1;
}

const clang::Type* getUnderlyingType(const clang::TypedefNameDecl* td) {
  return td->getUnderlyingType()
      .getCanonicalType()
      ->getUnqualifiedDesugaredType();
}

} // namespace ClangUtils

} // namespace moya
