#include "Type.h"
#include "Module.h"
#include "frontend/c-family/ClangUtils.h"

namespace moya {

Type::Type(ModuleBase& moduleBase, const clang::Type* type, llvm::Type* llvm)
    : TypeClang(moduleBase, type) {
  setLLVM(llvm);
}

Module& Type::getModule() {
  return static_cast<Module&>(TypeClang::getModule());
}

const Module& Type::getModule() const {
  return static_cast<const Module&>(TypeClang::getModule());
}

} // namespace moya
