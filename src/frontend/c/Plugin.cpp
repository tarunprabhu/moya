#include "PluginAction.h"

#include <clang/Frontend/FrontendPluginRegistry.h>

static clang::FrontendPluginRegistry::Add<moya::PluginAction>
    X("-moya", "Moya plugin (C)");
