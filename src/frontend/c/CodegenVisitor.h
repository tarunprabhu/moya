#ifndef MOYA_FRONTEND_C_CODEGEN_VISITOR_H
#define MOYA_FRONTEND_C_CODEGEN_VISITOR_H

#include "Module.h"
#include "frontend/c-family/CodegenVisitorClang.h"

namespace moya {

class CodegenVisitor : public CodegenVisitorClang {
protected:
  Module& module;

public:
  explicit CodegenVisitor(Module&);
  CodegenVisitor(const CodegenVisitor&) = delete;
  virtual ~CodegenVisitor() = default;

  virtual Module& getModule() override;
  virtual const Module& getModule() const override;

  virtual bool VisitVarDecl(clang::VarDecl*) override;
  virtual bool VisitFunctionDecl(clang::FunctionDecl*) override;
  virtual bool VisitRecordDecl(clang::RecordDecl*) override;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_CODEGEN_VISITOR_H
