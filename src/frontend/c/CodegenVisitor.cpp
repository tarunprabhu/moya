#include "CodegenVisitor.h"
#include "Module.h"

namespace moya {

CodegenVisitor::CodegenVisitor(Module& module) : module(module) {
  ;
}

Module& CodegenVisitor::getModule() {
  return module;
}

const Module& CodegenVisitor::getModule() const {
  return module;
}

bool CodegenVisitor::VisitVarDecl(clang::VarDecl*) {
  // if(const clang::RecordDecl* defn = getDefnDecl(decl->getType()))
  //   getModule().addType(defn);

  return true;
}

bool CodegenVisitor::VisitFunctionDecl(clang::FunctionDecl*) {
  // for(const clang::ParmVarDecl* param : decl->parameters())
  //   if(const clang::RecordDecl* defn = getDefnDecl(param->getType()))
  //     getModule().addType(defn);
  // if(const clang::RecordDecl* defn = getDefnDecl(decl->getReturnType()))
  //   getModule().addType(defn);

  return true;
}

bool CodegenVisitor::VisitRecordDecl(clang::RecordDecl*) {
  // if(const clang::RecordDecl* defn = getDefnDecl(decl->getTypeForDecl()))
  //   getModule().addType(defn);

  return true;
}

} // namespace moya
