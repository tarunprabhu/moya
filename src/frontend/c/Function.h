#ifndef MOYA_FRONTEND_C_FUNCTION_H
#define MOYA_FRONTEND_C_FUNCTION_H

#include "frontend/c-family/FunctionClang.h"

#include <clang/AST/Decl.h>

namespace moya {

class Argument;
class Module;

class Function : public FunctionClang {
protected:
  Function(ModuleBase&, const clang::FunctionDecl*, llvm::Function&);

public:
  Function(const Function&) = delete;
  virtual ~Function() = default;

  Module& getModule();
  Argument& getArg(unsigned);
  Argument& getArg(const std::string&);

  const Module& getModule() const;
  const Argument& getArg(unsigned) const;
  const Argument& getArg(const std::string&) const;

public:
  friend class ModuleBase;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_FUNCTION_H
