#ifndef MOYA_FRONTEND_C_CODEGEN_CONSUMER_H
#define MOYA_FRONTEND_C_CODEGEN_CONSUMER_H

#include "CodeGenerator.h"
#include "CodegenVisitor.h"
#include "frontend/c-family/CodegenConsumerClang.h"

namespace moya {

class Module;

class CodegenConsumer : public CodegenConsumerClang {
protected:
  CodegenVisitor visitor;

public:
  explicit CodegenConsumer(Module&, CodeGenerator&, clang::CompilerInstance&);
  CodegenConsumer(const CodegenConsumer&) = delete;
  virtual ~CodegenConsumer() = default;

  virtual CodegenVisitor& getVisitor() override;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_CODEGEN_CONSUMER_H
