#include "Argument.h"
#include "Function.h"
#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/AerynConfig.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "frontend/all-langs/InstrumentFunctionsCommonPass.h"
#include "frontend/all-langs/InstrumentUtils.h"

#include <clang/AST/ASTContext.h>
#include <clang/AST/Decl.h>
#include <clang/Basic/Builtins.h>

#include <llvm/IR/Function.h>
#include <llvm/IR/Intrinsics.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using llvm::cast;
using llvm::dyn_cast;
using llvm::isa;

class InstrumentFunctionsPass : public llvm::ModulePass {
protected:
  bool instrUserFunction(llvm::Function&, const moya::Function&);
  bool instrLibFunction(llvm::Function&, const moya::Function&);

public:
  InstrumentFunctionsPass();

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);

public:
  static char ID;
};

InstrumentFunctionsPass::InstrumentFunctionsPass() : ModulePass(ID) {
  llvm::initializeInstrumentFunctionsPassPass(
      *llvm::PassRegistry::getPassRegistry());
}

llvm::StringRef InstrumentFunctionsPass::getPassName() const {
  return "Moya Instrument Functions (C)";
}

void InstrumentFunctionsPass::getAnalysisUsage(llvm::AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.addRequired<InstrumentFunctionsCommonPass>();
  AU.setPreservesCFG();
}

bool InstrumentFunctionsPass::instrLibFunction(llvm::Function& f,
                                               const moya::Function& feFunc) {
  bool changed = false;

  if(feFunc.isMemcpy())
    changed |= moya::Metadata::setSystemMemcpy(f);

  if(feFunc.isMemset())
    changed |= moya::Metadata::setSystemMemset(f);

  return changed;
}

bool InstrumentFunctionsPass::instrUserFunction(llvm::Function& f,
                                                const moya::Function& feFunc) {
  bool changed = false;

  const moya::Module& feModule = feFunc.getModule();
  const clang::FunctionDecl* decl = feFunc.getDecl();

  changed |= moya::Metadata::setSourceLang(f, feModule.getSourceLang());
  if(f.getName() == "main")
    changed |= moya::Metadata::setMain(f);

  for(unsigned i = 0; i < feFunc.getNumArgs(); i++) {
    llvm::Argument* arg = moya::LLVMUtils::getArgument(f, i);
    const moya::Argument& feArg = feFunc.getArg(i);
    if(feArg.hasLocation()) {
      changed |= moya::Metadata::setSourceName(arg, feArg.getName());
      changed |= moya::Metadata::setSourceLocation(arg, feArg.getLocation());
    } else {
      changed |= moya::Metadata::setArtificialName(arg, feArg.getName());
      changed |= moya::Metadata::setArtificial(arg);
    }
  }

  clang::SourceManager& srcMgr = decl->getASTContext().getSourceManager();
  clang::SourceLocation srcLoc = decl->getBeginLoc();
  if(srcMgr.isInSystemHeader(srcLoc) and decl->isInlineSpecified()) {
    if(moya::LLVMUtils::isPureFunction(f))
      changed |= moya::Metadata::setModel(
          f, moya::AerynConfig::getPureFunctionModel());
    else if(f.getName() == decl->getQualifiedNameAsString())
      changed |= moya::Metadata::setModel(f, f.getName().str());
  }
  changed |= moya::Metadata::setSourceLocation(f, feFunc.getLocation());
  if(feFunc.hasBeginLoc() and feFunc.hasEndLoc()) {
    changed |= moya::Metadata::setBeginLocation(f, feFunc.getBeginLoc());
    changed |= moya::Metadata::setEndLocation(f, feFunc.getEndLoc());
  }
  changed |= moya::InstrumentUtils::instrumentDirectives(feFunc, f);

  // There is some ambiguity in C functions because arguments passed by
  // pointer could be arrays or just scalars that happen to be passed by
  // pointer (can also happen when interoperating C with Fortran, for
  // instance). In such cases, it is impossible to figure out correctly
  // what to do with the pointer when JIT'ing. So we just disable any
  // arguments passed by pointer unless they have been explicitly annotated
  // as being arrays or if they are pointers to fixed-size array types.
  if(moya::Metadata::hasSpecialize(f)) {
    for(llvm::Argument& arg : f.args()) {
      if(auto* pty
         = moya::LLVMUtils::getAnalysisTypeAs<llvm::PointerType>(arg)) {
        if(not(moya::Metadata::hasFixedArray(arg)
               or moya::Metadata::hasDynArray(arg)
               or isa<llvm::ArrayType>(pty->getElementType())))
          changed |= moya::Metadata::setIgnore(arg);
      }
    }
  }

  return changed;
}

bool InstrumentFunctionsPass::runOnModule(llvm::Module& module) {
  moya_message(getPassName());

  bool changed = false;

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  for(llvm::Function& f : module.functions()) {
    if(feModule.hasDeclaration(f)) {
      changed |= instrLibFunction(f, feModule.getDeclaration(f));
    } else if(feModule.hasFunction(f)) {
      if(f.size())
        changed |= instrUserFunction(f, feModule.getFunction(f));
    } else if(f.isVarArg() and not moya::Metadata::hasNoAnalyze(f)) {
      moya_error("vararg functions not supported: " << f.getName());
    }
  }

  return changed;
}

using namespace llvm;

char InstrumentFunctionsPass::ID = 0;

static const char* name = "moya-instr-funcs-c";
static const char* descr = "Adds instrumentation to functions (C)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(InstrumentFunctionsPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_DEPENDENCY(InstrumentFunctionsCommonPass)
INITIALIZE_PASS_END(InstrumentFunctionsPass, name, descr, cfg, analysis)

Pass* createInstrumentFunctionsPass() {
  return new InstrumentFunctionsPass();
}
