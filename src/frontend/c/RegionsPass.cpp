#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"

#include <llvm/IR/Constants.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class RegionsPass : public ModulePass {
public:
  static char ID;

private:
  bool processSentinel(CallInst*, const Function&);
  bool instrumentSentinel(CallInst*, bool);
  bool instrumentRegion(CallInst*, Function*);

public:
  RegionsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

RegionsPass::RegionsPass() : ModulePass(ID) {
  initializeRegionsPassPass(*PassRegistry::getPassRegistry());
}

StringRef RegionsPass::getPassName() const {
  return "Moya Regions (C)";
}

void RegionsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.setPreservesCFG();
}

bool RegionsPass::instrumentRegion(CallInst* call, Function* newCallee) {
  auto regionID = moya::LLVMUtils::getArgOperandAs<RegionID>(call, 0);

  call->setCalledFunction(newCallee);
  if(not moya::Config::isInvalidRegionID(regionID)) {
    Type* idTy = moya::LLVMUtils::getRegionIDType(call->getContext());
    call->setArgOperand(0, ConstantInt::get(idTy, regionID, false));
  }

  return true;
}

bool RegionsPass::instrumentSentinel(CallInst* call, bool start) {
  bool changed = false;

  Module& module = *moya::LLVMUtils::getModule(call);
  auto id = moya::LLVMUtils::getArgOperandAs<MoyaID>(call, 0);
  if(moya::Config::getIDKind(id) == IDKind::JITRegion) {
    if(start)
      changed |= instrumentRegion(
          call, moya::LLVMUtils::getEnterRegionFunction(module));
    else
      changed |= instrumentRegion(
          call, moya::LLVMUtils::getExitRegionFunction(module));
  }

  return changed;
}

bool RegionsPass::processSentinel(CallInst* call, const Function& f) {
  bool changed = false;

  if(f.getName() == moya::Config::getSentinelStartFunctionName())
    changed |= instrumentSentinel(call, true);
  else if(f.getName() == moya::Config::getSentinelStopFunctionName())
    changed |= instrumentSentinel(call, false);

  return changed;
}

bool RegionsPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  for(const moya::Region& region : feModule.getRegions())
    changed |= moya::Metadata::addRegions(module, region);

  // Apart from adding the regions to the Metadata, we also need to adjust the
  // sentinel function. This lets us get around having to include declarations
  // of Moya functions. These versions of the sentinel functions should never
  // escape from here
  for(Function& f : module.functions())
    for(auto i = inst_begin(f); i != inst_end(f); i++)
      if(auto* call = dyn_cast<CallInst>(&*i))
        if(Function* callee = call->getCalledFunction())
          if(moya::Config::isSentinelFunction(callee))
            changed |= processSentinel(call, *callee);

  return changed;
}

char RegionsPass::ID = 0;

static const char* name = "moya-regions-pass-c";
static const char* descr = "Process identified regions (C)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(RegionsPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_END(RegionsPass, name, descr, cfg, analysis)

Pass* createRegionsPass() {
  return new RegionsPass();
}
