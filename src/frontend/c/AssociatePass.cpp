#include "Function.h"
#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/Set.h"
#include "frontend/c-family/ClangUtils.h"

#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using llvm::dyn_cast;

class AssociatePass : public llvm::ModulePass {
protected:
  moya::Set<const clang::Type*> structs;

protected:
  void associateDirectives(llvm::Module&, moya::Module&);
  void associateFunctions(llvm::Module&, moya::Module&);
  void associateGlobals(llvm::Module&, moya::Module&);
  void associateStructTypes(llvm::Module&, moya::Module&);

public:
  AssociatePass();

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);

public:
  static char ID;
};

AssociatePass::AssociatePass() : ModulePass(ID) {
  llvm::initializeAssociatePassPass(*llvm::PassRegistry::getPassRegistry());
}

llvm::StringRef AssociatePass::getPassName() const {
  return "Moya Associate (C)";
}

void AssociatePass::getAnalysisUsage(llvm::AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.setPreservesAll();
}

void AssociatePass::associateFunctions(llvm::Module& module,
                                       moya::Module& feModule) {
  for(llvm::Function& f : module.functions()) {
    if(const clang::Decl* decl = feModule.getDecl(f.getName())) {
      if(const auto* fdecl = llvm::dyn_cast<clang::FunctionDecl>(decl)) {
        if(const auto* defn = moya::ClangUtils::getDefinition(fdecl))
          feModule.addFunction(defn, f);
        else
          feModule.addDeclaration(fdecl, f);
      }
    }
  }
}

void AssociatePass::associateGlobals(llvm::Module& module,
                                     moya::Module& feModule) {
  for(llvm::GlobalVariable& g : module.globals())
    if(const clang::Decl* decl = feModule.getDecl(g.getName()))
      if(const auto* vdecl = dyn_cast<clang::VarDecl>(decl))
        feModule.addGlobal(vdecl, g);
}

void AssociatePass::associateStructTypes(llvm::Module& module,
                                         moya::Module& feModule) {
  for(const clang::Type* astType : structs) {
    auto* sty = feModule.convertToLLVMAs<llvm::StructType>(astType, true);
    // The struct may not have been created in the actual CodgenModule
    // because it is not needed by any of the code here. If that is the
    // case, we don't create it either because we don't want to pollute
    // the module with an enormous number of types that won't ever be used
    if(llvm::StructType* llvm = module.getTypeByName(sty->getName()))
      // Unlike LLVM, Clang's types are not uniqued, but for struct types
      // with a definition, we should have only a single LLVM type for a
      // given Clang type
      feModule.addType(astType, llvm);
  }
}

void AssociatePass::associateDirectives(llvm::Module& module,
                                        moya::Module& feModule) {
  feModule.associate();
}

bool AssociatePass::runOnModule(llvm::Module& module) {
  moya_message(getPassName());

  moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();
  const clang::ASTContext& astContext = feModule.getASTContext();

  // First, we need to isolate the types that we actually need to deal with
  // Types that are only forward declarations and those with sugar should
  // be ignored
  for(const clang::Type* astType : astContext.getTypes()) {
    if(not astType->isDependentType()) {
      const clang::Type* type = astType->getUnqualifiedDesugaredType();
      if(const clang::Type* defn = moya::ClangUtils::getDefinition(type))
        structs.insert(defn);
    }
  }

  // The order in which everything is associated is important.
  // STL containers must be associated before structs which must be
  // associated before any other types
  associateFunctions(module, feModule);
  associateGlobals(module, feModule);
  associateStructTypes(module, feModule);
  associateDirectives(module, feModule);

  return false;
}

char AssociatePass::ID = 0;

using namespace llvm;

static const char* name = "moya-associate-c";
static const char* descr = "Associate frontend elements with LLVM IR (C)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(AssociatePass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_END(AssociatePass, name, descr, cfg, analysis)

Pass* createAssociatePass() {
  return new AssociatePass();
}
