#ifndef MOYA_FRONTEND_C_TYPE_H
#define MOYA_FRONTEND_C_TYPE_H

#include "frontend/c-family/TypeClang.h"

namespace moya {

class Module;

class Type : public TypeClang {
protected:
  Type(ModuleBase&, const clang::Type*, llvm::Type*);

public:
  Type(const Type&) = delete;
  virtual ~Type() = default;

  Module& getModule();
  const Module& getModule() const;

  friend class ModuleBase;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_TYPE_H
