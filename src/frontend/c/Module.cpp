#include "Module.h"
#include "CodeGenerator.h"
#include "frontend/c-family/ClangUtils.h"
#include "frontend/components/DirectiveContext.h"

namespace moya {

Module::Module(CodeGenerator& cg) : ModuleClang(SourceLang::C, cg), cg(cg) {
  ;
}

const Set<Type*>& Module::getStructs() const {
  return structs;
}

Function& Module::getDeclaration(llvm::Function& llvm) {
  return static_cast<Function&>(ModuleClang::getDeclaration(llvm));
}

Function& Module::getDeclaration(llvm::Function* llvm) {
  return getDeclaration(*llvm);
}

Function& Module::getDeclaration(const clang::FunctionDecl* decl) {
  return static_cast<Function&>(ModuleClang::getDeclaration(decl));
}

const Function& Module::getDeclaration(llvm::Function& llvm) const {
  return static_cast<const Function&>(ModuleClang::getDeclaration(llvm));
}

const Function& Module::getDeclaration(llvm::Function* llvm) const {
  return getDeclaration(*llvm);
}

const Function& Module::getDeclaration(const clang::FunctionDecl* decl) const {
  return static_cast<const Function&>(ModuleClang::getDeclaration(decl));
}

Function& Module::getFunction(llvm::Function& llvm) {
  return static_cast<Function&>(ModuleClang::getFunction(llvm));
}

Function& Module::getFunction(llvm::Function* llvm) {
  return getFunction(*llvm);
}

Function& Module::getFunction(const clang::FunctionDecl* decl) {
  return static_cast<Function&>(ModuleClang::getFunction(decl));
}

const Function& Module::getFunction(llvm::Function& llvm) const {
  return static_cast<const Function&>(ModuleClang::getFunction(llvm));
}

const Function& Module::getFunction(llvm::Function* llvm) const {
  return getFunction(*llvm);
}

const Function& Module::getFunction(const clang::FunctionDecl* decl) const {
  return static_cast<const Function&>(ModuleClang::getFunction(decl));
}

GlobalVariable& Module::getGlobal(llvm::GlobalVariable& llvm) {
  return static_cast<GlobalVariable&>(ModuleClang::getGlobal(llvm));
}

GlobalVariable& Module::getGlobal(llvm::GlobalVariable* llvm) {
  return getGlobal(*llvm);
}

GlobalVariable& Module::getGlobal(const clang::NamedDecl* decl) {
  return static_cast<GlobalVariable&>(ModuleClang::getGlobal(decl));
}

const GlobalVariable& Module::getGlobal(llvm::GlobalVariable& llvm) const {
  return static_cast<const GlobalVariable&>(ModuleClang::getGlobal(llvm));
}

const GlobalVariable& Module::getGlobal(llvm::GlobalVariable* llvm) const {
  return getGlobal(*llvm);
}

const GlobalVariable& Module::getGlobal(const clang::NamedDecl* decl) const {
  return static_cast<const GlobalVariable&>(ModuleClang::getGlobal(decl));
}

Type& Module::getType(llvm::Type* llvm) {
  return static_cast<Type&>(ModuleClang::getType(llvm));
}

Type& Module::getType(const clang::Type* type) {
  return static_cast<Type&>(ModuleClang::getType(type));
}

Type& Module::getType(const clang::RecordDecl* decl) {
  return static_cast<Type&>(ModuleClang::getType(decl));
}

const Type& Module::getType(llvm::Type* llvm) const {
  return static_cast<const Type&>(ModuleClang::getType(llvm));
}

const Type& Module::getType(const clang::Type* type) const {
  return static_cast<const Type&>(ModuleClang::getType(type));
}

const Type& Module::getType(const clang::RecordDecl* decl) const {
  return static_cast<const Type&>(ModuleClang::getType(decl));
}

} // namespace moya
