#include "GlobalVariable.h"
#include "Module.h"

namespace moya {

GlobalVariable::GlobalVariable(ModuleBase& module,
                               const clang::VarDecl* decl,
                               llvm::GlobalVariable& llvm)
    : GlobalVariableClang(module, decl, llvm) {
  ;
}

Module& GlobalVariable::getModule() {
  return static_cast<Module&>(GlobalVariableClang::getModule());
}

const Module& GlobalVariable::getModule() const {
  return static_cast<const Module&>(GlobalVariableClang::getModule());
}

} // namespace moya
