#include "Argument.h"
#include "Function.h"

namespace moya {

Argument::Argument(const FunctionBase& func,
                   const std::string& name,
                   unsigned num)
    : ArgumentClang(func, name, num) {
  ;
}

Argument::Argument(const FunctionBase& func,
                   const clang::ParmVarDecl* param,
                   unsigned num)
    : ArgumentClang(func, param, num) {
  ;
}

const Function& Argument::getFunction() const {
  return static_cast<const Function&>(ArgumentClang::getFunction());
}

} // namespace moya
