#include "CodegenConsumer.h"

namespace moya {

CodegenConsumer::CodegenConsumer(Module& module,
                                 CodeGenerator& cg,
                                 clang::CompilerInstance& compiler)
    : CodegenConsumerClang(cg, compiler), visitor(module) {
  ;
}

CodegenVisitor& CodegenConsumer::getVisitor() {
  return visitor;
}

} // namespace moya
