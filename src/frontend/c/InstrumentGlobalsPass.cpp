#include "GlobalVariable.h"
#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/Metadata.h"
#include "frontend/all-langs/InstrumentGlobalsCommonPass.h"

#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class InstrumentGlobalsPass : public ModulePass {
public:
  static char ID;

public:
  InstrumentGlobalsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

InstrumentGlobalsPass::InstrumentGlobalsPass() : ModulePass(ID) {
  initializeInstrumentGlobalsPassPass(*PassRegistry::getPassRegistry());
}

StringRef InstrumentGlobalsPass::getPassName() const {
  return "Moya Instrument Globals (C)";
}

void InstrumentGlobalsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.addRequired<InstrumentGlobalsCommonPass>();
  AU.setPreservesCFG();
}

bool InstrumentGlobalsPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  for(GlobalVariable& g : module.globals()) {
    if(g.hasName()) {
      changed |= moya::Metadata::setSourceLang(g, feModule.getSourceLang());
      if(feModule.hasGlobal(g)) {
        const moya::GlobalVariable& myg = feModule.getGlobal(g);
        changed |= moya::Metadata::setSourceLocation(g, myg.getLocation());
      }
    }
  }

  return changed;
}

char InstrumentGlobalsPass::ID = 0;

static const char* name = "moya-instr-globals-c";
static const char* descr = "Adds instrumentation to globals (C)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(InstrumentGlobalsPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_DEPENDENCY(InstrumentGlobalsCommonPass)
INITIALIZE_PASS_END(InstrumentGlobalsPass, name, descr, cfg, analysis)

Pass* createInstrumentGlobalsPass() {
  return new InstrumentGlobalsPass();
}
