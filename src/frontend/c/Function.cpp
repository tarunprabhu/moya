#include "Function.h"
#include "Argument.h"
#include "Module.h"

namespace moya {

Function::Function(ModuleBase& module,
                   const clang::FunctionDecl* decl,
                   llvm::Function& f)
    : FunctionClang(module, decl, f) {
  if(f.hasStructRetAttr())
    for(llvm::Argument& arg : f.args())
      if(arg.hasStructRetAttr())
        addArgument<Argument>(".sret");
  for(const clang::ParmVarDecl* param : decl->parameters()) {
    // If the current type is a struct/class and is passed by value,
    // it will be packed and passed as a sequence of 64-bit integers.
    // I won't try to figure out which of the arguments correspond to the
    // passed struct, so for the moment will just bail out
    if(param->getType()->getAsRecordDecl()) {
      structParam = true;
      break;
    }
    if(isDefined() and param->getName().size())
      addArgument<Argument>(param);
  }
}

Module& Function::getModule() {
  return static_cast<Module&>(FunctionClang::getModule());
}

Argument& Function::getArg(unsigned i) {
  return static_cast<Argument&>(FunctionClang::getArg(i));
}

Argument& Function::getArg(const std::string& name) {
  return static_cast<Argument&>(FunctionClang::getArg(name));
}

const Module& Function::getModule() const {
  return static_cast<const Module&>(FunctionClang::getModule());
}

const Argument& Function::getArg(unsigned i) const {
  return static_cast<const Argument&>(FunctionClang::getArg(i));
}

const Argument& Function::getArg(const std::string& name) const {
  return static_cast<const Argument&>(FunctionClang::getArg(name));
}

} // namespace moya
