#ifndef MOYA_FRONTEND_C_GLOBAL_VARIABLE_H
#define MOYA_FRONTEND_C_GLOBAL_VARIABLE_H

#include "frontend/c-family/GlobalVariableClang.h"

namespace moya {

class Module;

class GlobalVariable : public GlobalVariableClang {
protected:
  GlobalVariable(ModuleBase&, const clang::VarDecl*, llvm::GlobalVariable&);

public:
  GlobalVariable(const GlobalVariable&) = delete;
  virtual ~GlobalVariable() = default;

  Module& getModule();
  const Module& getModule() const;

  friend class ModuleBase;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_GLOBAL_VARIABLE_H
