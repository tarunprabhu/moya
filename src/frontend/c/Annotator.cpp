#include "Annotator.h"

#include <clang/Basic/LangOptions.h>

#include <sstream>

using namespace clang;

namespace moya {

Annotator::Annotator() : AnnotatorClang(SourceLang::C) {
  ;
}

void Annotator::initialize(CompilerInstance& compiler) {
  SourceManager& srcMgr = compiler.getSourceManager();
  LangOptions& langOpts = compiler.getLangOpts();

  AnnotatorClang::initialize(srcMgr, langOpts);
}

std::string
Annotator::generateFunctionCall(const std::string& func,
                                const moya::Vector<std::string>& args,
                                const std::string& sig,
                                NewLine newl) {
  std::stringstream ss;

  if(static_cast<int>(newl & NewLine::Prepend))
    ss << "\n";
  ss << "{";
  ss << " " << sig << ";";
  ss << " " << func << "(";
  if(args.size()) {
    ss << args.at(0);
    for(unsigned i = 1; i < args.size(); i++)
      ss << ", " << args.at(i);
  }
  ss << "); }";
  if(static_cast<int>(newl & NewLine::Append))
    ss << "\n";

  return ss.str();
}

} // namespace moya
