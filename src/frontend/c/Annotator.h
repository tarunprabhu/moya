#ifndef MOYA_FRONTEND_C_ANNOTATOR_H
#define MOYA_FRONTEND_C_ANNOTATOR_H

#include "frontend/c-family/AnnotatorClang.h"

#include <clang/AST/Stmt.h>

namespace moya {

class Annotator : public AnnotatorClang {
protected:
  virtual std::string generateFunctionCall(const std::string&,
                                           const Vector<std::string>&,
                                           const std::string&,
                                           NewLine) override;

public:
  Annotator();
  virtual ~Annotator() = default;

  virtual void initialize(clang::CompilerInstance&) override;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_ANNOTATOR_H
