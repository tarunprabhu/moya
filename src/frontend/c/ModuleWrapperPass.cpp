#include "ModuleWrapperPass.h"
#include "Passes.h"

using namespace llvm;

ModuleWrapperPass::ModuleWrapperPass() : ModulePass(ID), feModule(nullptr) {
  initializeModuleWrapperPassPass(*PassRegistry::getPassRegistry());
}

ModuleWrapperPass::ModuleWrapperPass(moya::Module& module)
    : ModulePass(ID), feModule(&module) {
  initializeModuleWrapperPassPass(*PassRegistry::getPassRegistry());
}

moya::Module& ModuleWrapperPass::getModule() {
  return *feModule;
}

const moya::Module& ModuleWrapperPass::getModule() const {
  return *feModule;
}

StringRef ModuleWrapperPass::getPassName() const {
  return "Moya Module Wrapper (C)";
}

void ModuleWrapperPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.setPreservesAll();
}

bool ModuleWrapperPass::runOnModule(Module&) {
  moya_message(getPassName());

  return false;
}

char ModuleWrapperPass::ID = 0;

static const char* name = "moya-module-wrapper-c";
static const char* descr = "Wraps a Moya frontend module (C)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS(ModuleWrapperPass, name, descr, cfg, analysis)

Pass* createModuleWrapperPass(moya::Module& module) {
  return new ModuleWrapperPass(module);
}
