#ifndef MOYA_FRONTEND_C_ARGUMENT_H
#define MOYA_FRONTEND_C_ARGUMENT_H

#include "frontend/c-family/ArgumentClang.h"

namespace moya {

class Function;

class Argument : public ArgumentClang {
protected:
  Argument(const FunctionBase&, const std::string&, unsigned);
  Argument(const FunctionBase&, const clang::ParmVarDecl*, unsigned);

public:
  Argument(const Argument&) = delete;
  virtual ~Argument() = default;

  const Function& getFunction() const;

  friend class FunctionBase;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_ARGUMENT_H
