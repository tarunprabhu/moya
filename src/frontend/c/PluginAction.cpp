#include "PluginAction.h"
#include "CodegenConsumer.h"
#include "common/PassManager.h"
#include "frontend/c-family/PragmaConsumer.h"
#include "frontend/c-family/PragmaHandler.h"
#include "frontend/c-family/PragmaVisitor.h"

#include "Passes.h"

namespace moya {

PluginAction::PluginAction()
    : PluginActionClang(SourceLang::C, cg), annotator(), cg(annotator),
      module(cg) {
  ;
}

clang::PragmaHandler* PluginAction::createPragmaHandler() {
  return new PragmaHandler(SourceLang::C, module.getDirectiveContext());
}

PragmaConsumer*
PluginAction::createPragmaConsumer(clang::CompilerInstance& compiler) {
  return new PragmaConsumer(compiler, annotator, module.getDirectiveContext());
}

clang::ASTConsumer*
PluginAction::createCodegenConsumer(clang::CompilerInstance& compiler) {
  return new CodegenConsumer(module, cg, compiler);
}

llvm::Module& PluginAction::runPasses() {
  llvm::Module& llvmModule = cg.getLLVMModule();
  PassManager pm(llvmModule, false);

  pm.addPass(createEarlyInstrumentPass());
  pm.addPass(createModuleWrapperPass(module));
  pm.addPass(createPromoteConstExprsPass());
  pm.addPass(createAssociatePass());
  pm.addPass(createInsertMoyaAPIDeclsPass());
  pm.addPass(createPrepareInstrumentPass());
  pm.addPass(createRegionsPass());
  pm.addPass(createInstrumentFunctionsPass());
  pm.addPass(createInstrumentGlobalsPass());
  pm.addPass(createInstrumentTypesPass());
  pm.addPass(createInstrumentMallocsPass());
  pm.addPass(createPropagateAnalysisTypesPass());
  pm.addPass(createInstrumentInstructionsPass());
  pm.addPass(createSanityCheckPass());
  pm.addPass(createGenerateMoyaSignaturePass());
  pm.addPass(createGenerateGettersPass());
  pm.addPass(createJITProfilingPass());
  pm.addPass(createGenerateFileBitcodeGlobalPass());
  pm.addPass(createJITRedirectPass());
  pm.addPass(createGenerateRedirectPass());

  pm.run(llvmModule);

  return llvmModule;
}

} // namespace moya
