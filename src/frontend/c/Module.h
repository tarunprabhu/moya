#ifndef MOYA_FRONTEND_C_MODULE_H
#define MOYA_FRONTEND_C_MODULE_H

#include "CodeGenerator.h"
#include "Function.h"
#include "GlobalVariable.h"
#include "Type.h"
#include "common/Set.h"
#include "frontend/c-family/ModuleClang.h"

namespace moya {

class Module : public ModuleClang {
protected:
  CodeGenerator& cg;
  Set<Type*> structs;

public:
  Module(CodeGenerator&);
  Module(const Module&) = delete;
  virtual ~Module() = default;

  template <typename... ArgsT>
  Function& addDeclaration(ArgsT&&... args) {
    return ModuleClang::addDeclaration<Function>(args...);
  }

  template <typename... ArgsT>
  Function& addFunction(ArgsT&&... args) {
    return ModuleClang::addFunction<Function>(args...);
  }

  template <typename... ArgsT>
  GlobalVariable& addGlobal(ArgsT&&... args) {
    return ModuleClang::addGlobal<GlobalVariable>(args...);
  }

  template <typename... ArgsT>
  Type& addType(const clang::Type* type, ArgsT&&... args) {
    Type& t = ModuleClang::addType<Type>(type, args...);

    // The only clang::RecordDecl that will be added must be
    //
    //   - Definitions
    //   - Complete (no partial specializations)
    //   - Independent (Templated types should *NOT* be added)
    //
    if(not t.isUnion()) {
      if(type->getAsRecordDecl())
        structs.insert(&t);
    }

    return t;
  }

  const Set<Type*>& getStructs() const;

  using ModuleClang::hasDeclaration;
  Function& getDeclaration(llvm::Function&);
  Function& getDeclaration(llvm::Function*);
  Function& getDeclaration(const clang::FunctionDecl*);
  const Function& getDeclaration(llvm::Function&) const;
  const Function& getDeclaration(llvm::Function*) const;
  const Function& getDeclaration(const clang::FunctionDecl*) const;

  using ModuleClang::hasFunction;
  Function& getFunction(llvm::Function&);
  Function& getFunction(llvm::Function*);
  Function& getFunction(const clang::FunctionDecl*);
  const Function& getFunction(llvm::Function&) const;
  const Function& getFunction(llvm::Function*) const;
  const Function& getFunction(const clang::FunctionDecl*) const;

  using ModuleClang::hasGlobal;
  GlobalVariable& getGlobal(llvm::GlobalVariable&);
  GlobalVariable& getGlobal(llvm::GlobalVariable*);
  GlobalVariable& getGlobal(const clang::NamedDecl*);
  const GlobalVariable& getGlobal(llvm::GlobalVariable&) const;
  const GlobalVariable& getGlobal(llvm::GlobalVariable*) const;
  const GlobalVariable& getGlobal(const clang::NamedDecl*) const;

  using ModuleClang::hasType;
  Type& getType(llvm::Type*);
  Type& getType(const clang::Type*);
  Type& getType(const clang::RecordDecl*);
  const Type& getType(llvm::Type*) const;
  const Type& getType(const clang::Type*) const;
  const Type& getType(const clang::RecordDecl*) const;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_MODULE_H
