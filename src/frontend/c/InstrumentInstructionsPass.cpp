#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "common/Diagnostics.h"
#include "common/LLVMUtils.h"
#include "common/Metadata.h"
#include "frontend/all-langs/InstrumentInstructionsCommonPass.h"

#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class InstrumentInstructionsPass : public ModulePass {
public:
  static char ID;

protected:
  bool instrument(GetElementPtrInst*);
  bool instrument(CastInst*);

  int64_t getAdjustedGEPIndex(GetElementPtrInst*, const APInt&);

public:
  InstrumentInstructionsPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

InstrumentInstructionsPass::InstrumentInstructionsPass() : ModulePass(ID) {
  initializeInstrumentInstructionsPassPass(*PassRegistry::getPassRegistry());
}

StringRef InstrumentInstructionsPass::getPassName() const {
  return "Moya Instrument Instructions (C)";
}

void InstrumentInstructionsPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.addRequired<InstrumentInstructionsCommonPass>();
  AU.setPreservesCFG();
}

int64_t InstrumentInstructionsPass::getAdjustedGEPIndex(GetElementPtrInst* gep,
                                                        const APInt& offset) {
  Value* ptrOp = gep->getPointerOperand();
  Type* srcTy = cast<PointerType>(ptrOp->getType())->getElementType();

  if(auto* cst = dyn_cast<CastInst>(gep->getPointerOperand()))
    // If a constant offset is taken into a struct that has been cast to
    // void* it is either a field access or in the case of virtually
    // inherited classes, an offset to get to the vtable of a base class
    if(moya::Metadata::hasCastStructToBytes(cst))
      return offset.getLimitedValue();

  // If the source is an array of scalars, it will be truncated
  if(moya::LLVMUtils::isScalarTy(srcTy)) {
    return 0;
  } else if(auto* aty = dyn_cast<ArrayType>(srcTy)) {
    // If the source type is an array of primitives or structs,
    // it will always be truncated.
    Type* elemTy = aty->getElementType();
    if(moya::LLVMUtils::isScalarTy(elemTy)
       or moya::LLVMUtils::isStructTy(elemTy))
      return 0;
  }

  return offset.getLimitedValue();
}

bool InstrumentInstructionsPass::instrument(GetElementPtrInst* gep) {
  bool changed = false;
  const DataLayout& dl = moya::LLVMUtils::getModule(gep)->getDataLayout();

  // Some of the GEP instructions have constant offsets that *must* be exact.
  // But figuring out which ones can be tricky.
  // FIXME: Try to use the AST nodes to figure out exactly which ones need
  // exact index computations
  if(gep->getNumIndices() <= 2) {
    APInt offset(64, 0, true);
    if(gep->accumulateConstantOffset(dl, offset)) {
      bool preComputedIndex = false;
      if(moya::Metadata::hasGEPFieldLookup(gep)) {
        preComputedIndex = true;
      } else if(auto* inst = dyn_cast<Instruction>(gep->getPointerOperand())) {
        if(moya::Metadata::hasCastStructToBytes(inst)) {
          preComputedIndex = true;
        }
      }

      if(preComputedIndex)
        // We may need to reset the offset for the analysis in certain cases.
        // If we are looking into an array, it might be truncated in which
        // case the offset should always be zero. In that case, we reset the
        // offset variable.
        changed |= moya::Metadata::setGEPIndex(
            gep, getAdjustedGEPIndex(gep, offset));
    }
  }

  return changed;
}

bool InstrumentInstructionsPass::instrument(CastInst* cst) {
  bool changed = false;
  Type* pi8 = Type::getInt8PtrTy(cst->getContext());
  Type* srcType = moya::LLVMUtils::getAnalysisType(cst->getOperand(0));
  Type* dstType = cst->getDestTy();
  StructType* srcStruct = nullptr;

  if(auto* pty = dyn_cast<PointerType>(srcType))
    srcStruct = dyn_cast<StructType>(pty->getElementType());

  if(srcStruct) {
    if(dstType == pi8) {
      changed
          |= moya::Metadata::setAnalysisType(cst, srcStruct->getPointerTo());
      changed |= moya::Metadata::setCastStructToBytes(cst);
    }
  }

  return changed;
}

bool InstrumentInstructionsPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  for(Function& f : module.functions()) {
    for(auto i = inst_begin(f); i != inst_end(f); i++) {
      Instruction* inst = &*i;
      if(auto* gep = dyn_cast<GetElementPtrInst>(inst))
        changed |= instrument(gep);
      else if(auto* cst = dyn_cast<CastInst>(inst))
        changed |= instrument(cst);
    }
  }

  return changed;
}

char InstrumentInstructionsPass::ID = 0;

static const char* name = "moya-instr-insts-c";
static const char* descr = "Adds instrumentation to instructions (C)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(InstrumentInstructionsPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_DEPENDENCY(InstrumentInstructionsCommonPass)
INITIALIZE_PASS_END(InstrumentInstructionsPass, name, descr, cfg, analysis)

Pass* createInstrumentInstructionsPass() {
  return new InstrumentInstructionsPass();
}
