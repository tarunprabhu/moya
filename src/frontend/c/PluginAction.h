#ifndef MOYA_FRONTEND_C_PLUGIN_ACTION_H
#define MOYA_FRONTEND_C_PLUGIN_ACTION_H

#include "Annotator.h"
#include "CodegenVisitor.h"
#include "Module.h"
#include "frontend/c-family/PluginActionClang.h"
#include "frontend/components/DirectiveContext.h"

namespace moya {

class PragmaConsumer;

class PluginAction : public PluginActionClang {
protected:
  Annotator annotator;
  CodeGenerator cg;
  Module module;

protected:
  virtual clang::PragmaHandler* createPragmaHandler() override;
  virtual PragmaConsumer*
  createPragmaConsumer(clang::CompilerInstance&) override;
  virtual clang::ASTConsumer*
  createCodegenConsumer(clang::CompilerInstance&) override;
  virtual llvm::Module& runPasses() override;

public:
  PluginAction();
  PluginAction(const PluginAction&) = delete;
  virtual ~PluginAction() = default;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_PLUGIN_ACTION_H
