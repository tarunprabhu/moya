#ifndef MOYA_FRONTEND_C_MODULE_WRAPPER_PASS_H
#define MOYA_FRONTEND_C_MODULE_WRAPPER_PASS_H

#include "Module.h"

#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

class ModuleWrapperPass : public llvm::ModulePass {
protected:
  moya::Module* feModule;

public:
  ModuleWrapperPass();
  explicit ModuleWrapperPass(moya::Module&);

  moya::Module& getModule();
  const moya::Module& getModule() const;

  virtual llvm::StringRef getPassName() const;
  virtual void getAnalysisUsage(llvm::AnalysisUsage&) const;
  virtual bool runOnModule(llvm::Module&);

public:
  static char ID;
};

#endif // MOYA_FRONTEND_C_MODULE_WRAPPER_PASS_H
