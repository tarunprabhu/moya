#ifndef MOYA_FRONTEND_C_CODEGEN_CONTEXT_H
#define MOYA_FRONTEND_C_CODEGEN_CONTEXT_H

#include "frontend/c-family/CodeGeneratorClang.h"

namespace moya {

class Annotator;

class CodeGenerator : public CodeGeneratorClang {
public:
  CodeGenerator(Annotator&);
  virtual ~CodeGenerator() = default;
};

} // namespace moya

#endif // MOYA_FRONTEND_C_CODEGEN_CONTEXT_H
