#include "Module.h"
#include "ModuleWrapperPass.h"
#include "Passes.h"
#include "Type.h"
#include "common/ArrayRefUtils.h"
#include "common/Diagnostics.h"
#include "common/Metadata.h"
#include "frontend/all-langs/InstrumentTypesCommonPass.h"

#include <clang/AST/Type.h>

#include <llvm/IR/Module.h>
#include <llvm/Pass.h>

using namespace llvm;

class InstrumentTypesPass : public ModulePass {
public:
  static char ID;

protected:
  int getLastVtableIndex(StructType*);

public:
  InstrumentTypesPass();

  virtual StringRef getPassName() const;
  virtual void getAnalysisUsage(AnalysisUsage&) const;
  virtual bool runOnModule(Module&);
};

InstrumentTypesPass::InstrumentTypesPass() : ModulePass(ID) {
  initializeInstrumentTypesPassPass(*PassRegistry::getPassRegistry());
}

StringRef InstrumentTypesPass::getPassName() const {
  return "Moya Instrument Types (C)";
}

void InstrumentTypesPass::getAnalysisUsage(AnalysisUsage& AU) const {
  AU.addRequired<ModuleWrapperPass>();
  AU.addRequired<InstrumentTypesCommonPass>();
  AU.setPreservesCFG();
}

bool InstrumentTypesPass::runOnModule(Module& module) {
  moya_message(getPassName());

  bool changed = false;

  const moya::Module& feModule = getAnalysis<ModuleWrapperPass>().getModule();

  for(const moya::Type* feType : feModule.getStructs()) {
    auto* sty = feType->getLLVMAs<StructType>();
    changed |= moya::Metadata::setSourceName(sty, feType->getName(), module);
    if(feType->isUnion())
      changed |= moya::Metadata::setUnion(sty, module);
    if(feType->getLocation().isValid())
      changed |= moya::Metadata::setSourceLocation(
          sty, feType->getLocation(), module);
  }

  return changed;
}

char InstrumentTypesPass::ID = 0;

static const char* name = "moya-instr-types-c";
static const char* descr = "Adds instrumentation to types (C)";
static const bool cfg = true;
static const bool analysis = false;

INITIALIZE_PASS_BEGIN(InstrumentTypesPass, name, descr, cfg, analysis)
INITIALIZE_PASS_DEPENDENCY(ModuleWrapperPass)
INITIALIZE_PASS_DEPENDENCY(InstrumentTypesCommonPass)
INITIALIZE_PASS_END(InstrumentTypesPass, name, descr, cfg, analysis)

Pass* createInstrumentTypesPass() {
  return new InstrumentTypesPass();
}
