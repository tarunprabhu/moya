#ifndef MOYA_FRONTEND_CUDA_CODEGEN_CONSUMER_H
#define MOYA_FRONTEND_CUDA_CODEGEN_CONSUMER_H

#include "CodegenVisitor.h"
#include "Properties.h"
#include "frontend/c-family/CodegenConsumerBase.h"

class CodegenConsumer : public CodegenConsumerBase {
public:
  CodegenConsumer(Properties&, CodegenVisitor&);
  virtual ~CodegenConsumer() = default;

  virtual void HandleCXXImplicitFunctionInstantiation(clang::FunctionDecl*);
  virtual void AssignInheritanceModel(clang::CXXRecordDecl*);
  virtual void HandleCXXStaticMemberVarInstantiation(clang::VarDecl*);
  virtual void HandleVTable(clang::CXXRecordDecl*);
};

#endif // MOYA_FRONTEND_CUDA_CODEGEN_CONSUMER_H
