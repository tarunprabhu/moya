#ifndef MOYA_FRONTEND_CUDA_ANNOTATOR_H
#define MOYA_FRONTEND_CUDA_ANNOTATOR_H

#include <clang/AST/ExprCXX.h>
#include <clang/AST/Stmt.h>
#include <clang/Rewrite/Core/Rewriter.h>

#include "common/Vector.h"

class Annotator : public AnnotatorBase {
protected:
  bool cxx11;

protected:
  virtual std::string generateFunctionCall(const std::string&,
                                           const moya::Vector<std::string>&,
                                           const std::string&);

public:
  Annotator(clang::Rewriter&);

  void generateCudaKernelSentinels(const clang::CUDAKernelCallExpr*);
};

#endif // MOYA_FRONTEND_CUDA_ANNOTATOR_H
