#ifndef MOYA_FRONTEND_INSTRUMENT_GENERATE_REDIRECT_CUDA_H
#define MOYA_FRONTEND_INSTRUMENT_GENERATE_REDIRECT_CUDA_H

#include "frontend/instrument/common/GenerateRedirect.h"

// Redirecter class for Cuda
// The redirecter function for Cuda kernels is different from what is used
// on the host. All the checking, compilation, and launch of the kernel is
// actually done within Moya's runtime. This function only spills the
// kernel arguments and invokes Moya's launcher function
class GenerateRedirectCuda : public GenerateRedirect {
protected:
  virtual void initializeRedirectFunction(State&, llvm::Function&);
  virtual Value* insertIsCompiled(State&);
  virtual void insertCompile(State&);
  virtual void insertGetCompiled(State&);
  virtual void insertExecute(State&);

public:
  GenerateRedirectCudaImpl(llvm::Module&);
};

#endif // MOYA_FRONTEND_INSTRUMENT_GENERATE_REDIRECT_CUDA_H
