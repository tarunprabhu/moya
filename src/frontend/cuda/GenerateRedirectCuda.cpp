#include "GenerateRedirectCuda.h"

GenerateRedirectCuda::GenerateRedirectCuda(Module& module, const Summary& store)
    : GenerateRedirectImpl(module, store) {
  ;
}

void GenerateRedirectCuda::initializeRedirectFunction(State& state,
                                                      Function& orig) {
  std::string redirName = moya::Metadata::getRedirecter(orig);
  Module& module = *orig.getParent();
  Function* f = module.getFunction(redirName);
  JITID key = moya::Metadata::getJITID(orig);

  fargsToIgnore[key] = moya::Set<unsigned>();
  for(Argument& arg : orig.args()) {
    unsigned argno = arg.getArgNo();
    if(arg.hasName())
      getArgument(f, argno)->setName(arg.getName());
    if(moya::Metadata::hasIgnore(getArgument(f, argno)))
      fargsToIgnore[key].insert(argno);
  }

  unsigned nargs = orig.getFunctionType()->getNumParams();

  getArgument(f, nargs + 0)->setName("grid_x");
  getArgument(f, nargs + 1)->setName("grid_y");
  getArgument(f, nargs + 2)->setName("grid_z");

  getArgument(f, nargs + 3)->setName("block_x");
  getArgument(f, nargs + 4)->setName("block_y");
  getArgument(f, nargs + 5)->setName("block_z");

  getArgument(f, nargs + 6)->setName("shared");
  getArgument(f, nargs + 7)->setName("hstream");

  state.f = f;
  state.nargs = nargs;
  state.key = key;
}

Value* GenerateRedirectCuda::insertIsCompiled(State& state) {
  IRBuilder<>& builder = state.builder;

  return builder.getTrue();
}

void GenerateRedirectCuda::insertGetCompiled(State& state) {
  IRBuilder<>& builder = state.builder;

  state.fnptr = ConstantPointerNull::get(builder.getInt8PtrTy());
}

void GenerateRedirectCuda::insertCompile(State& state) {
  return;
}

void GenerateRedirectCuda::insertExecute(State& state) {
  IRBuilder<>& builder = state.builder;
  Function& f = *state.f;
  JITID key = state.key;
  unsigned nargs = state.nargs;
  Value* sig = state.sig;
  Value* argptr = state.argptr;

  LLVMContext& context = f.getContext();
  Constant* ckey
      = ConstantInt::get(moya::Config::getJITIDType(context), key, false);
  Module& module = *f.getParent();
  Type* ppi8 = builder.getInt8PtrTy()->getPointerTo();

  Function* executeCudaFn
      = module.getFunction(moya::Config::getExecuteCudaFunctionName());
  moya::Vector<Value*> executeArgs;
  executeArgs.push_back(ckey);
  executeArgs.push_back(sig);
  for(unsigned i = 0; i < 6; i++)
    executeArgs.push_back(getArgument(f, nargs + i));
  executeArgs.push_back(getArgument(f, nargs + 6));
  executeArgs.push_back(getArgument(f, nargs + 7));
  executeArgs.push_back(builder.CreateBitOrPointerCast(argptr, ppi8));

  builder.CreateCall(executeCudaFn, executeArgs);
}
