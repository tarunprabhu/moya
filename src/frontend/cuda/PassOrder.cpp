#include "frontend/common/RegisterFrontendPass.h"

using namespace llvm;

REGISTER_PASS(KernelRedirect);
REGISTER_PASS(Instrument);
REGISTER_PASS(PromoteConstExprs);
REGISTER_PASS(InsertMoyaAPIDecls);
REGISTER_PASS(JITInstrumentation);
REGISTER_PASS(GenerateGetters);
REGISTER_PASS(GenerateMoyaSignature);
REGISTER_PASS(GenerateFileBitcodeGlobal);
