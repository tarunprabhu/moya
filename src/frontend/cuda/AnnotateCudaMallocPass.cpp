// Cuda malloc functions don't return a pointer explicitly, but instead
// return the pointer to the allocated buffer through an out parameter.
// On top of this, there are templated versions of cudaMalloc in the C++
// API which need to be handled correctly.
for(Function& f : module.functions()) {
  std::string fname = demangler.demangle(f.getName());
  if(fname.find("cudaMalloc") == 0 or fname.find("cudaError cudaMalloc") == 0) {
    moya::Metadata::setNoAnalyze(f);
    moya::Metadata::setCudaMalloc(f);
    moya::Metadata::setModel(f, "cudaMalloc");
    changed |= true;
  }
}
