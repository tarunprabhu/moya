#include "CodegenVisitor.h"

using namespace clang;

CodegenVisitor::CodegenVisitor(PropertiesClang& program)
    : CodegenVisitor(program) {
  ;
}

bool CodegenVisitor::VisitClassTemplateDecl(ClassTemplateDecl* templ) {
  for(const CXXRecordDecl* clss : templ->specializations())
    program.addType(clss->getTypeForDecl());

  return true;
}

bool CodegenVisitor::VisitCXXMemberCallExpr(CXXMemberCallExpr* call) {
  Expr* object = call->getImplicitObjectArgument();
  if(not object)
    object = call;

  if(CXXMethodDecl* method = call->getMethodDecl())
    if(method->isVirtual())
      program.addVirtualCall(call);

  return true;
}

bool CodegenVisitor::VisitCUDAKernelCallExpr(CUDAKernelCallExpr* call) {
  annotator.generateCudaKernelSentinels(call);

  return true;
}
