#include "CodegenConsumer.h"

using namespace clang;

CodegenConsumer::CodegenConsumer(Properties& program, CodegenVisitor& visitor)
    : CodegenConsumerBase(program, visitor) {
  ;
}

void CodegenConsumer::HandleCXXImplicitFunctionInstantiation(FunctionDecl* f) {
  shadowCg->HandleCXXImplicitFunctionInstantiation(f);
  cg->HandleCXXImplicitFunctionInstantiation(f);
}

void CodegenConsumerBase::AssignInheritanceModel(CXXRecordDecl* record) {
  shadowCg->AssignInheritanceModel(record);
  cg->AssignInheritanceModel(record);
}

void CodegenConsumerBase::HandleCXXStaticMemberVarInstantiation(VarDecl* decl) {
  shadowCg->HandleCXXStaticMemberVarInstantiation(decl);
  cg->HandleCXXStaticMemberVarInstantiation(decl);
}

void CodegenConsumerBase::HandleVTable(CXXRecordDecl* record) {
  shadowCg->HandleVTable(record);
  cg->HandleVTable(record);
}
