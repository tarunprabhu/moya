#include <llvm/ADT/Triple.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>

#include "common/Config.h"
#include "common/ContainerUtils.h"
#include "common/Metadata.h"
#include "frontend/clang/MoyaInfoClang.h"
#include "frontend/common/DirectiveUtils.h"

using namespace llvm;

class InstrumentPass : public FunctionPass {
public:
  static char ID;

protected:
  MoyaInfoClang& info;

private:
  pair<BasicBlock*, BasicBlock*> splitBlockAtKernelLaunch(Function&);
  void insertRedirect(Function& f, Function& fnRedir);
  moya::Vector<Type*> getCudaRedirecterParams(Function& f);

  void processSpecializeDirective(Function&);
  void processDeclareDirective(Function&);

public:
  explicit InstrumentPass(MoyaInfoClang& info);

  virtual bool runOnFunction(Function& Func);
};

InstrumentPass::InstrumentPass(MoyaInfoClang& info)
    : FunctionPass(ID), info(info) {
  ;
}

moya::Vector<Type*> InstrumentPass::getCudaRedirecterParams(Function& f) {
  Module& module = *f.getParent();
  LLVMContext& context = module.getContext();

  Type* i32 = Type::getInt32Ty(context);
  FunctionType* fty = f.getFunctionType();

  moya::Vector<Type*> params(fty->param_begin(), fty->param_end());

  // Grid and block dims
  for(unsigned i = 0; i < 6; i++)
    params.push_back(i32);

  // shared_mem
  params.push_back(i32);

  // stream
  params.push_back(
      module.getTypeByName(moya::Config::getCudaStreamStructName())
          ->getPointerTo());

  return params;
}

pair<BasicBlock*, BasicBlock*>
InstrumentPass::splitBlockAtKernelLaunch(Function& f) {
  for(inst_iterator i = inst_begin(f); i != inst_end(f); i++)
    if(CallInst* call = dyn_cast<CallInst>(&*i))
      if(Function* launch = call->getCalledFunction())
        if(launch->getName() == "cudaLaunch") {
          Instruction* inst = &*i;
          BasicBlock* block = inst->getParent();
          return std::make_pair(block, block->splitBasicBlock(inst));
        }
  return std::make_pair(nullptr, nullptr);
}

void InstrumentPass::insertRedirect(Function& f, Function& fnRedir) {
  // This should be mostly the same as inserting redirects for host functions
  moya_error("Unimplemented: JIT'ting host functions");

  // Module& module = *f.getParent();
  // LLVMContext& context = f.getContext();
  // Type* i32 = Type::getInt32Ty(context);

  // // Create Moya functions
  // Function* fnIsInRegion = cast<Function>(module.getOrInsertFunction(
  //     moya::Config::getIsInRegionFunctionName(),
  //     FunctionType::get(i32, ArrayRef<Type*>(), false)));

  // IRBuilder<> builder(context);

  // BasicBlock* bbRet = &f.back();
  // pair<BasicBlock*, BasicBlock*> p1 = splitBlockAtKernelLaunch(f);
  // BasicBlock* bbBeforeLaunch = p1.first;
  // BasicBlock* bbLaunch = p1.second;
  // BasicBlock* bbRedir = BasicBlock::Create(context, "moya.redir", &f,
  // bbLaunch); BasicBlock* bbCheck = BasicBlock::Create(context, "moya.check",
  // &f, bbRedir);

  // // Jump to the redirection checker
  // TerminatorInst* term = bbBeforeLaunch->getTerminator();
  // builder.SetInsertPoint(bbBeforeLaunch);
  // builder.CreateBr(bbCheck);
  // term->eraseFromParent();

  // // Check if we are inside a region
  // builder.SetInsertPoint(bbCheck);
  // CallInst* isInRegion = builder.CreateCall(fnIsInRegion,
  // ArrayRef<Value*>()); Value* ne = builder.CreateICmpNE(isInRegion,
  // builder.getInt32(0)); builder.CreateCondBr(ne, bbRedir, bbLaunch);

  // // Call redirecter
  // builder.SetInsertPoint(bbRedir);
  // moya::Vector<Value*> params;
  // for(Argument& arg : f.args())
  //   params.push_back(&arg);
  // builder.CreateCall(&fnRedir, params);
  // builder.CreateBr(bbRet);
}

void InstrumentPass::processSpecializeDirective(Function& f) {
  if(const MoyaSpecializeDirective* specialize
     = info.getSpecialize(f.getName())) {
    Module& module = *f.getParent();
    LLVMContext& context = f.getContext();
    std::string redirName = info.getRedirecter(f.getName());
    FunctionType* redirTy = nullptr;

    // The specialize attributes need to be attached to the device
    // function because that is what will actually be JIT'ted, but we associate
    // the redirecter with the host function because we have to insert the
    // redirection code at all the callsites in the case of Cuda.
    if(moya::Metadata::hasCudaKernelHost(f)) {
      moya::Vector<Type*> params = getCudaRedirecterParams(f);
      redirTy = FunctionType::get(Type::getVoidTy(context), params, false);
      moya::Metadata::setNoAnalyze(f);
    } else if(moya::Metadata::hasCudaKernelDevice(f)) {
      redirTy = f.getFunctionType();
      for(std::string argName : specialize->getIgnoreArgNames())
        for(Argument& arg : f.args())
          if(arg.getName() == argName)
            moya::Metadata::setIgnore(arg);
    } else {
      // We could still want to JIT host functions in a Cuda file.
      // For the moment we don't allow it because we need to find a way of
      // distinguishing between actual kernels that will be executed on the
      // GPU and host functions
      moya_error("UNIMPLEMENTED: JIT'ting host functions in Cuda");
    }

    // The host and the device functions have the same key because we don't
    // really ever need to distinguish between the two
    moya::Metadata::setKey(f, info.getKey(f.getName()));
    moya::Metadata::setFunctionSpecialize(f);
    moya::Metadata::setRedirecter(f, redirName);

    Function* fnRedir
        = cast<Function>(module.getOrInsertFunction(redirName, redirTy));
    moya::Metadata::setNoAnalyze(fnredir);
    moya::Metadata::setKey(
        fnRedir, moya::Config::generateJITID(fnRedir->getName(), "", 0, 0));
    moya::Metadata::setSourceLang(fnRedir, info.getSourceLang());
  }
}

void InstrumentPass::processDeclareDirective(Function& f) {
  if(const MoyaDeclareDirective* declare = info.getDeclare(f.getName()))
    addFunctionAttributes(declare, f);
}

bool InstrumentPass::runOnFunction(Function& f) {
  if(f.size()) {
    moya::Metadata::setSourceLang(f, info.getSourceLang());

    if(info.isCudaKernel(f.getName())) {
      Triple::ArchType arch
          = Triple(f.getParent()->getTargetTriple()).getArch();
      if(arch == Triple::ArchType::nvptx64 or arch == Triple::ArchType::nvptx)
        moya::Metadata::setCudaKernelDevice(f);
      else
        moya::Metadata::setCudaKernelHost(f);
    }
  }

  processSpecializeDirective(f);
  processDeclareDirective(f);

  return true;
}

char InstrumentPass::ID = 0;

void registerInstrumentPass(const PassManagerBuilder&,
                            legacy::PassManagerBase& pm) {
  pm.add(new InstrumentPass(*MoyaInfoClang::get()));
}
