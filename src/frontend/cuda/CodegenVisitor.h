#ifndef MOYA_FRONTEND_CUDA_CODEGEN_VISITOR_H
#define MOYA_FRONTEND_CUDA_CODEGEN_VISITOR_H

#include "Properties.h"
#include "frontend/c-family/CodegenVisitorBase.h"

class CodegenVisitor : public CodegenVisitorBase {
public:
  explicit CodegenVisitor(Properties&);
  virtual ~CodegenVisitor() = default;

  virtual bool VisitClassTemplateDecl(clang::ClassTemplateDecl*);
  virtual bool VisitCXXMemberCallExpr(clang::CXXMemberCallExpr*);
  virtual bool VisitCUDAKernelCallExpr(clang::CUDAKernelCallExpr*);
};

#endif // MOYA_FRONTEND_CUDA_CODEGEN_VISITOR_H
