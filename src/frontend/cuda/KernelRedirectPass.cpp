#include <llvm/Analysis/Interval.h>
#include <llvm/IR/Dominators.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/Transforms/Utils/CodeExtractor.h>

#include "common/Config.h"
#include "common/ContainerUtils.h"
#include "common/DemanglerCXX.h"
#include "common/Diagnostics.h"
#include "common/InstructionUtils.h"
#include "common/Metadata.h"
#include "frontend/clang/MoyaInfoClang.h"

using namespace llvm;

// The MoyaMetadata pass is a function pass which will already have run before
// this pass, so we have all the annotations to identify the kernels that
// need to be JIT'ted.
//
// This outlines the calls to Cuda kernels into separate functions and then
// adds redirects to those functions depending on whether or not the kernels
// being called are JIT'ted or not.
//
class CudaKernelRedirectPass : public ModulePass {
public:
  static char ID;

private:
  MoyaInfoClang& info;
  DemanglerCXX demangler;

private:
  moya::Vector<BasicBlock*> collectBasicBlocks(BasicBlock* first,
                                               BasicBlock* after);
  void collectBasicBlocks(BasicBlock* curr,
                          BasicBlock* after,
                          moya::Vector<BasicBlock*>&);

  moya::Vector<Value*> getDimensions(Function& wrapper, unsigned instance);
  moya::Vector<Value*>
  getRedirecterArgs(Function& kernel, const moya::Vector<Value*>& kernelArgs);
  void insertRedirect(Function& wrapper,
                      Function& kernel,
                      moya::Vector<Value*> kernelArgs);

  std::string demangle(Function* f);
  void processSentinel(CallInst*,
                       Function*,
                       moya::Map<SentinelID, pair<CallInst*, CallInst*>>&);

public:
  CudaKernelRedirectPass(MoyaInfoClang& info) : ModulePass(ID), info(info) {
    ;
  }

  virtual void getAnalysisUsage(AnalysisUsage& AU) const;
  virtual bool runOnModule(Module& module);
};

void CudaKernelRedirectPass::getAnalysisUsage(AnalysisUsage& AU) const {
  ;
}

void CudaKernelRedirectPass::collectBasicBlocks(
    BasicBlock* bbCurr,
    BasicBlock* bbAfter,
    moya::Vector<BasicBlock*>& blocks) {
  if(not ::contains(blocks, bbCurr)) {
    blocks.push_back(bbCurr);
    TerminatorInst* term = bbCurr->getTerminator();
    for(BasicBlock* bb : term->successors())
      if(bb != bbAfter)
        collectBasicBlocks(bb, bbAfter, blocks);
  }
}

moya::Vector<BasicBlock*>
CudaKernelRedirectPass::collectBasicBlocks(BasicBlock* bbFirst,
                                           BasicBlock* bbAfter) {
  moya::Vector<BasicBlock*> blocks;
  collectBasicBlocks(bbFirst, bbAfter, blocks);
  return blocks;
}

// instance = 0 is the first call to dim3::dim3(...) and is the grid
// instance = 1 is the second call to dim3::dim3(...) and is the block
moya::Vector<Value*> CudaKernelRedirectPass::getDimensions(Function& f,
                                                           unsigned instance) {
  unsigned curr = 0;
  for(inst_iterator i = inst_begin(f); i != inst_end(f); i++)
    if(CallInst* call = dyn_cast<CallInst>(&*i))
      if(Function* callee = call->getCalledFunction())
        if(demangler.demangle(callee->getName()).find("dim3::dim3") == 0)
          if(curr++ == instance)
            return {call->getArgOperand(1),
                    call->getArgOperand(2),
                    call->getArgOperand(3)};
  return {};
}

moya::Vector<Value*> CudaKernelRedirectPass::getRedirecterArgs(
    Function& f,
    const moya::Vector<Value*>& kernelArgs) {
  moya::Vector<Value*> redirArgs(kernelArgs.begin(), kernelArgs.end());

  // The first two calls to the dims constructor will be the grid and block
  // dimensions
  moya::Vector<Value*> gridDims = getDimensions(f, 0);
  for(Value* dim : gridDims)
    redirArgs.push_back(dim);

  moya::Vector<Value*> blockDims = getDimensions(f, 1);
  for(Value* dim : blockDims)
    redirArgs.push_back(dim);

  // Look at the call to cudaConfigureCall to determine the other parameters
  // that are needed for this call
  Value* sharedMemBytes = nullptr;
  Value* stream = nullptr;
  for(inst_iterator i = inst_begin(f); i != inst_end(f); i++)
    if(CallInst* call = dyn_cast<CallInst>(&*i))
      if(Function* callee = call->getCalledFunction())
        if(callee->getName() == "cudaConfigureCall") {
          // Do it this way because LLVM merges adjacent i32 arguments and I'm
          // not sure if these arguments will always be merged this way
          unsigned nargs = call->getNumArgOperands();
          sharedMemBytes = call->getArgOperand(nargs - 2);
          stream = call->getArgOperand(nargs - 1);
        }

  redirArgs.push_back(sharedMemBytes);
  redirArgs.push_back(stream);

  return redirArgs;
}

void CudaKernelRedirectPass::insertRedirect(Function& f,
                                            Function& kernelFn,
                                            moya::Vector<Value*> argPtrs) {
  Module& module = *f.getParent();
  LLVMContext& context = f.getContext();
  IRBuilder<> builder(context);

  BasicBlock* bbFirst = &*f.begin();
  BasicBlock* bbRedir = BasicBlock::Create(context, "", &f, bbFirst);
  BasicBlock* bbEntry = BasicBlock::Create(context, "", &f, bbRedir);
  BasicBlock* bbRet = nullptr;
  for(inst_iterator i = inst_begin(f); i != inst_end(f); i++)
    if(ReturnInst* ret = dyn_cast<ReturnInst>(&*i))
      bbRet = ret->getParent();

  // Create Moya functions
  Function* fnIsInRegion = cast<Function>(module.getOrInsertFunction(
      moya::Config::getIsInRegionFunctionName(),
      FunctionType::get(builder.getInt32Ty(), ArrayRef<Type*>(), false)));

  // Check if we are within a region
  builder.SetInsertPoint(bbEntry);
  CallInst* isInRegion = builder.CreateCall(fnIsInRegion, ArrayRef<Value*>());
  Value* ne = builder.CreateICmpNE(isInRegion, builder.getInt32(0));
  builder.CreateCondBr(ne, bbRedir, bbFirst);

  Function* redirFn
      = module.getFunction(moya::Metadata::getRedirecter(kernelFn));
  FunctionType* redirTy = redirFn->getFunctionType();

  // Redirect
  builder.SetInsertPoint(bbRedir);
  moya::Vector<Value*> kernelArgs;
  // argPtrs are pointers to the arguments that must be passed to the Cuda
  // kernel. We need to load them first before we send them along.
  for(Value* arg : argPtrs)
    kernelArgs.push_back(builder.CreateLoad(arg));
  moya::Vector<Value*> redirArgs = getRedirecterArgs(f, kernelArgs);
  for(unsigned i = 0; i < redirArgs.size(); i++) {
    Type* paramTy = redirTy->getParamType(i);
    if(redirArgs[i]->getType() != paramTy) {
      if(redirTy->isPointerTy())
        redirArgs[i] = builder.CreateBitOrPointerCast(redirArgs[i], paramTy);
      else
        redirArgs[i] = builder.CreateSExtOrTrunc(redirArgs[i], paramTy);
    }
  }
  builder.CreateCall(redirFn, redirArgs);
  builder.CreateBr(bbRet);
}

std::string CudaKernelRedirectPass::demangle(Function* f) {
  SourceLang lang = moya::Config::getSourceLang(f);
  if(lang == SourceLang::CXX or lang == SourceLang::Cuda)
    return demangler.getFunctionName(f->getName());
  return f->getName();
}

void CudaKernelRedirectPass::processSentinel(
    CallInst* call,
    Function* f,
    moya::Map<SentinelID, pair<CallInst*, CallInst*>>& sentinels) {
  std::string fname = demangle(f);
  int sentinel = 0;
  SentinelID id = moya::Config::getInvalidSentinelID();
  if(fname == moya::Config::getSentinelStartFunctionName()) {
    id = getArgOperandAsI64(call, 0);
    sentinel = 1;
  } else if(fname == moya::Config::getSentinelStopFunctionName()) {
    id = getArgOperandAsI64(call, 0);
    sentinel = 2;
  }

  if(sentinel) {
    if(moya::Config::getSentinelKind(id) == SentinelKind::CudaKernelCall) {
      if(sentinel == 1)
        sentinels[moya::Config::getSentinelBase(id)].first = call;
      else
        sentinels[moya::Config::getSentinelBase(id)].second = call;
    }
  }
}

bool CudaKernelRedirectPass::runOnModule(Module& module) {
  moya_error("Make sure that the sentinels are Cuda sentinels");
  moya::Map<SentinelID, pair<CallInst*, CallInst*>> sentinels;
  for(Function& f : module.functions())
    for(inst_iterator i = inst_begin(f); i != inst_end(f); i++)
      if(CallInst* call = dyn_cast<CallInst>(&*i))
        if(Function* callee = call->getCalledFunction())
          processSentinel(call, callee, sentinels);

  moya::Map<SentinelID, moya::Vector<BasicBlock*>> kernelBlocks;
  for(auto& it : sentinels) {
    SentinelID sentinelId = it.first;
    CallInst* callStart = it.second.first;
    CallInst* callStop = it.second.second;
    BasicBlock* bbBefore = callStart->getParent();
    BasicBlock* bbFirst = bbBefore->splitBasicBlock(callStart);
    BasicBlock* bbLast = callStop->getParent();
    BasicBlock* bbAfter = bbLast->splitBasicBlock(callStop);
    kernelBlocks[sentinelId] = collectBasicBlocks(bbFirst, bbAfter);
  }

  for(auto& it : kernelBlocks) {
    SentinelID sentinelId = it.first;
    Function& f = *getFunction(sentinels.at(sentinelId).first);
    DominatorTree dt(f);
    CodeExtractor ce(it.second, &dt, false);
    Function* outlined = ce.extractCodeRegion();

    // Rename the outlined function just so we can easily identify it for
    // debugging
    std::stringstream ss;
    ss << "__moya_cuda_wrapper_" << (unsigned)sentinelId;
    outlined->setName(ss.str());
    moya::Metadata::setSourceLang(outlined, moya::Metadata::getSourceLang(f));

    // The outlined function will have a single Cuda kernel that is called
    // Find the call and insert a redirect in the function if that is a kernel
    // that should be JIT'ted
    for(inst_iterator i = inst_begin(outlined); i != inst_end(outlined); i++)
      if(CallInst* call = dyn_cast<CallInst>(&*i))
        if(Function* callee = call->getCalledFunction())
          if(moya::Metadata::hasCudaKernelHost(callee)
             and moya::Metadata::isFunctionSpecialize(callee)) {
            moya::Vector<Value*> kernelArgs;
            for(Value* arg : call->arg_operands())
              kernelArgs.push_back(cast<LoadInst>(arg)->getPointerOperand());
            insertRedirect(*outlined, *callee, kernelArgs);
          }
  }

  for(auto& it : sentinels) {
    it.second.first->eraseFromParent();
    it.second.second->eraseFromParent();
  }

  return true;
}

char CudaKernelRedirectPass::ID = 0;

void registerCudaKernelRedirectPass(const PassManagerBuilder&,
                                    legacy::PassManagerBase& pm) {
  pm.add(new CudaKernelRedirectPass(MoyaInfoClang::get()));
}
