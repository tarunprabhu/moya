#ifndef MOYA_FRONTEND_CUDA_PROGRAM_ELEMENTS_H
#define MOYA_FRONTEND_CUDA_PROGRAM_ELEMENTS_H

#include "frontend/c-family/PropertiesBase.h"

class Properties : public PropertiesClang {
public:
  Properties(clang::CompilerInstance&);
  virtual ~Properties() = default;
};

#endif // MOYA_FRONTEND_CUDA_PROGRAM_ELEMENTS_H
