#include "DeclUtils.h"
#include "MoyaCodegenConsumer.h"
#include "MoyaInfo.h"
#include "MoyaPragmaConsumer.h"
#include "MoyaPragmaHandler.h"
#include "common/Diagnostics.h"
#include "common/PassManager.h"
#include "frontend/instrument/common/PopulatePasses.h"

#include <clang/Basic/Diagnostic.h>
#include <clang/Basic/TargetInfo.h>
#include <clang/Basic/TargetOptions.h>
#include <clang/CodeGen/BackendUtil.h>
#include <clang/CodeGen/CodeGenABITypes.h>
#include <clang/Frontend/CompilerInstance.h>
#include <clang/Frontend/FrontendPluginRegistry.h>
#include <clang/Frontend/TextDiagnosticPrinter.h>
#include <clang/Lex/Preprocessor.h>
#include <clang/Lex/PreprocessorOptions.h>
#include <clang/Parse/ParseAST.h>
#include <clang/Sema/Sema.h>

#include <llvm/IR/Function.h>
#include <llvm/Support/FileSystem.h>

#include <vector>

using namespace clang;

class MoyaAction : public PluginASTAction {
protected:
  BackendAction getBackendAction(const CompilerInstance&);

  std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance&,
                                                 llvm::StringRef);
  bool ParseArgs(const CompilerInstance&, const std::vector<std::string>&);
  void PrintHelp(llvm::raw_ostream&);
};

BackendAction MoyaAction::getBackendAction(const CompilerInstance& compiler) {
  frontend::ActionKind fe = compiler.getFrontendOpts().ProgramAction;
  switch(fe) {
  case frontend::ActionKind::EmitObj:
    return BackendAction::Backend_EmitObj;
  case frontend::ActionKind::EmitBC:
    return BackendAction::Backend_EmitBC;
  case frontend::ActionKind::EmitLLVM:
    return BackendAction::Backend_EmitLL;
  case frontend::ActionKind::EmitAssembly:
    return BackendAction::Backend_EmitAssembly;
  case frontend::ActionKind::EmitCodeGenOnly:
    return BackendAction::Backend_EmitMCNull;
  default:
    moya_error("Unsupported frontend action: " << fe);
  }
  return BackendAction::Backend_EmitNothing;
}

std::unique_ptr<ASTConsumer>
MoyaAction::CreateASTConsumer(CompilerInstance& compiler, llvm::StringRef) {
  MoyaInfo& info = MoyaInfo::create(compiler);

  std::shared_ptr<CompilerInvocation> ptrInvocation;

  std::error_code errorCode;
  CompilerInstance ci;
  DiagnosticsEngine* diags
      = new DiagnosticsEngine(new DiagnosticIDs(),
                              &compiler.getDiagnosticOpts(),
                              new IgnoringDiagConsumer());
  diags->setSourceManager(&compiler.getSourceManager());

  ci.setTarget(&compiler.getTarget());
  ci.setFileManager(&compiler.getFileManager());
  ptrInvocation.reset(&compiler.getInvocation());
  ci.setInvocation(ptrInvocation);
  ci.setSourceManager(&compiler.getSourceManager());
  ci.setDiagnostics(diags);
  ci.createPreprocessor(getTranslationUnitKind());
  ci.createASTContext();

  Preprocessor& ppp = ci.getPreprocessor();
  ppp.AddPragmaHandler(new MoyaPragmaHandler(info));
  ppp.getBuiltinInfo().initializeBuiltins(ppp.getIdentifierTable(),
                                          ppp.getLangOpts());

  ci.setASTConsumer(llvm::make_unique<MoyaPragmaConsumer>(compiler, info));
  ci.createSema(getTranslationUnitKind(), nullptr);

  ci.getDiagnosticClient().BeginSourceFile(ci.getLangOpts(), &ppp);

  // This has to be done after the compiler instance has been initialized,
  // so we create the mangler just before we actually need it
  ParseAST(ci.getSema(),
           ci.getFrontendOpts().ShowStats,
           ci.getFrontendOpts().SkipFunctionBodies);

  ci.getDiagnosticClient().EndSourceFile();

  compiler.getCodeGenOpts().setDebugInfo(clang::codegenoptions::FullDebugInfo);
  compiler.getCodeGenOpts().DebugColumnInfo = true;

  Preprocessor& pp = compiler.getPreprocessor();
  pp.AddPragmaHandler(new MoyaPragmaHandler(info));
  pp.getBuiltinInfo().initializeBuiltins(pp.getIdentifierTable(),
                                         pp.getLangOpts());

  compiler.setASTConsumer(llvm::make_unique<MoyaCodegenConsumer>(info));
  compiler.createSema(getTranslationUnitKind(), nullptr);

  compiler.getDiagnosticClient().BeginSourceFile(pp.getLangOpts(), &pp);

  ParseAST(compiler.getSema(),
           compiler.getFrontendOpts().ShowStats,
           compiler.getFrontendOpts().SkipFunctionBodies);

  compiler.getDiagnosticClient().EndSourceFile();

  llvm::Module* module = info.getCodeGenerator().GetModule();

  moya::PassManager pm(*module, false);
  populateInstrumentationPasses(pm);
  pm.run(*module);

  EmitBackendOutput(
      compiler.getDiagnostics(),
      compiler.getHeaderSearchOpts(),
      compiler.getCodeGenOpts(),
      compiler.getTargetOpts(),
      compiler.getLangOpts(),
      module->getDataLayout(),
      module,
      getBackendAction(compiler),
      compiler.createOutputFile(
          compiler.getFrontendOpts().OutputFile, true, true, "", "", false));

  // We have emitted everything correctly at this point, so just terminate
  exit(0);

  // Will never get here but need to keep the compiler happy.
  return nullptr;
}

bool MoyaAction::ParseArgs(const CompilerInstance&,
                           const std::vector<std::string>&) {
  return true;
}

void MoyaAction::PrintHelp(llvm::raw_ostream& ros) {
  ros << "Need to add -moya flag to parse Moya annotations\n";
}

static FrontendPluginRegistry::Add<MoyaAction> X("-moya",
                                                 "Process Moya annotations");
