#include "Annotator.h"
#include "common/Config.h"

#include <sstream>

using namespace clang;

Annotator::Annotator(Rewriter& rewriter)
    : AnnotatorBaserewriter(rewriter), lang(SourceLang::Cuda), cxx11(false) {
  const LangOptions& langOpts = rewriter.getLangOpts();
  if(langOpts.CPlusPlus11 or langOpts.CPlusPlus14 or langOpts.CPlusPlus17)
    cxx11 = true;
}

std::string
Annotator::generateFunctionCall(const std::string& func,
                                const moya::Vector<std::string>& args,
                                const std::string& sig) {
  std::stringstream ss;

  ss << "{";
  ss << " " << sig;
  if(cxx11)
    ss << "noexcept";
  else
    ss << "throw()";
  ss << " "
     << "asm(\"" << func << "\")";
  ss << ";";
  ss << " " << func << "(";
  if(args.size()) {
    ss << args.at(0);
    for(unsigned i = 1; i < args.size(); i++)
      ss << ", " << args.at(i);
  }
  ss << ");"
     << " "
     << "}\n";

  return ss.str();
}

void Annotator::generateCudaKernelSentinels(const CUDAKernelCallExpr* expr) {
  SourceManager& srcMgr = rewriter.getSourceMgr();
  SentinelID id = moya::Config::generateSentinelID(
      getLocation(expr, rewriter.getSourceMgr()), SentinelKind::CudaKernelCall);

  SourceLocation locStart = expr->getBeginLoc();
  SourceLocation locEnd = expr->getEndLoc();
  std::string funcStart
      = generateFunctionCall(moya::Config::getSentinelStartFunctionName(),
                             {moya::str(id)},
                             moya::Config::getSentinelStartFunctionSignature());
  std::string funcEnd
      = generateFunctionCall(moya::Config::getSentinelStopFunctionName(),
                             {moya::str(id)},
                             moya::Config::getSentinelStopFunctionSignature());

  rewriter.InsertTextBefore(locStart, funcStart);
  rewriter.InsertTextAfter(locEnd.deserializeLocWithOffset(2), funcEnd);
}
