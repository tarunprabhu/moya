#ifndef MOYA_FRONTEND_ANNOTATIONS_DIRECTIVES_H
#define MOYA_FRONTEND_ANNOTATIONS_DIRECTIVES_H

#include "DeclareDirective.h"
#include "JITDirective.h"
#include "ModelDirective.h"
#include "OptimizeDirective.h"
#include "SpecializeDirective.h"

#endif // MOYA_FRONTEND_ANNOTATIONS_DIRECTIVES_H
