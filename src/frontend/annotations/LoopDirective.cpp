#include "LoopDirective.h"
#include "common/Config.h"

namespace moya {

LoopDirective::LoopDirective(Directive::Kind kind, const Location& loc)
    : Directive(kind, loc), id(Config::getInvalidLoopID()) {
  generateLoopID();
}

void LoopDirective::generateLoopID() {
  if(loc.isValid())
    id = Config::generateLoopID(loc);
}

LoopID LoopDirective::getLoopID() const {
  return id;
}

void LoopDirective::setLocation(const Location& loc) {
  Directive::setLocation(loc);
  generateLoopID();
}

void LoopDirective::serialize(Serializer& s, bool start) const {
  Directive::serialize(s, start);
  if(start) {
    s.add("id", id);
  }
}

void LoopDirective::deserialize(Deserializer& d, bool start) {
  Directive::deserialize(d, start);
  if(start) {
    d.deserialize("id", id);
  }
}

bool LoopDirective::classof(const Directive* directive) {
  return (directive->getKind() > Directive::Kind::MinLoop)
         and (directive->getKind() < Directive::Kind::MaxLoop);
}

} // namespace moya
