#include "Parser.h"
#include "common/Diagnostics.h"
#include "common/StringUtils.h"
#include "frontend/components/DirectiveContext.h"

using llvm::dyn_cast;

namespace moya {

static bool isInteger(const std::string& s) {
  if(s.length() == 0)
    return false;

  unsigned base = 10;
  unsigned start = 0;
  if(s[0] == '-') {
    if(s.length() == 1)
      return false;
    start += 1;
  }

  if(s[start] == '0') {
    base = 8;
    start += 1;
    // This may just be 0, or it may be hexadecimal in which this will be an
    // 'x'. but if it is a hexadecimal number, there should be something
    // after the x
    if((s.length() > 1) and (s[start] == 'x')) {
      if(s.length() == 2)
        return false;
      base = 16;
      start += 1;
    }
  }

  switch(base) {
  case 8:
    for(unsigned i = start; i < s.length(); i++)
      if((s[i] < '0') or s[i] > '7')
        return false;
    break;
  case 10:
    for(unsigned i = start; i < s.length(); i++)
      if((s[i] < '0') or (s[i] > '9'))
        return false;
    break;
  case 16:
    for(unsigned i = start; i < s.length(); i++)
      if(not(((s[i] >= '0') and (s[i] <= '9'))
             or ((s[i] >= 'a') and (s[i] <= 'f'))
             or ((s[i] >= 'A') and (s[i] <= 'F'))))
        return false;
    break;
  default:
    return false;
  }

   return true;
}

Parser::Parser(DirectiveContext& directives) : directives(directives) {
  ;
}

void Parser::error(const std::string& msg, const Token& token) {
  moya_error("Annotation parsing error:\n"
             << "  msg: " << msg << "\n"
             << "  tok: " << token.getSpelling() << "\n"
             << "  loc: " << token.getFile() << ":" << token.getLine() << ":"
             << token.getColumn());
}

void Parser::warning(const std::string& msg, const Token& token) {
  moya_error("Annotation parser:\n"
             << "  msg: " << msg << "\n"
             << "  tok: " << token.getSpelling() << "\n"
             << "  loc: " << token.getFile() << ":" << token.getLine() << ":"
             << token.getColumn());
}

Token Parser::shift() {
  Token ret = peek();
  stream.pop();
  return ret;
}

Token& Parser::peek() {
  return stream.front();
}

void Parser::addToken(std::stringstream& ss,
                      const std::string& filename,
                      unsigned line,
                      unsigned column) {
  std::string s = ss.str();
  if(s.length() != 0) {
    Token::Kind kind = Token::Kind::TokenKindUnknown;
    if(s == "(")
      kind = Token::Kind::LParen;
    else if(s == ")")
      kind = Token::Kind::RParen;
    else if(s == "{")
      kind = Token::Kind::LBrace;
    else if(s == "}")
      kind = Token::Kind::RBrace;
    else if(s == "[")
      kind = Token::Kind::LBracket;
    else if(s == "]")
      kind = Token::Kind::RBracket;
    else if(s == ",")
      kind = Token::Kind::Comma;
    else if(isInteger(s))
      kind = Token::Kind::Integer;
    else
      kind = Token::Kind::Identifier;

    stream.push(
        Token(kind,
              s,
              Location(filename, line, column ? column - s.size() : column)));
  }
  ss.str("");
}

void Parser::lex(const std::string& text,
                 const std::string& filename,
                 unsigned line,
                 unsigned column) {
  std::stringstream ss;
  unsigned i = 0;
  for(i = 0; i < text.size(); i++) {
    char c = text[i];
    unsigned col = column ? column + i : column;
    if(isspace(c)) {
      if(ss.str().size())
        addToken(ss, filename, line, col);
      if(c == '\n') {
        line += 1;
        column = 0;
      }
    } else if(ispunct(c)) {
      if(c == '_')
        ss << c;
      else {
        addToken(ss, filename, line, col);
        ss << c;
        addToken(ss, filename, line, col);
      }
    } else if(isalnum(c)) {
      ss << c;
    }
  }
  if(ss.str().size())
    addToken(ss, filename, line, column ? text.size() : column);
}

Directive* Parser::parse(const std::string& annotation, const Location& loc) {
  // Empty existing stream
  while(not stream.empty())
    stream.pop();

  // Lex the string
  lex(annotation, loc.getFile(), loc.getLine(), loc.getColumn());

  // Parse the annotation
  return parseDirective();
}

Directive* Parser::parseDirective() {
  Token token = shift();
  const std::string& spelling = token.getSpelling();
  Directive::Kind kind = Directive::getKind(spelling);
  Location loc(token.getFile(), token.getLine(), token.getColumn());

  if(StringUtils::lower(spelling) == "end") {
    Token next = shift();
    Directive::Kind kind = Directive::getKind(next.getSpelling());
    Directive* directive = directives.getMatchingDirective(kind);
    if(auto* regionDirective = dyn_cast<RegionDirective>(directive))
      regionDirective->setEndLoc(loc);
    return directive;
  } else if(Directive* directive = directives.create(kind, loc)) {
    if(auto* regionDirective = dyn_cast<RegionDirective>(directive))
      regionDirective->setBeginLoc(loc);
    parseMoyaClauses(directive);
    return directive;
  } else {
    moya_error("Unknown directive: " << spelling);
  }

  return nullptr;
}

void Parser::parseMoyaClauses(Directive* directive) {
  while(not stream.empty()) {
    Token token = shift();
    Clause::Kind ckind = Clause::getKind(token.getSpelling());

    if(ckind == Clause::Kind::Unknown)
      error("Unknown clause", token);
    if(not directive->isClauseAllowed(ckind))
      error("Unsupported clause for directive", token);

    // All except one of these will be nullptr. Only here so I don't have to
    // create them in each of the clause processing routines in the following
    // switch
    auto* decl = llvm::dyn_cast<DeclareDirective>(directive);
    auto* specialize = llvm::dyn_cast<SpecializeDirective>(directive);
    auto* model = llvm::dyn_cast<ModelDirective>(directive);

    switch(ckind) {
    case Clause::Kind::Allocator:
      decl->setAllocator();
      break;
    case Clause::Kind::Array: {
      Pair<Vector<FixedArray>, Vector<DynArray>> arrays = parseArrayList();
      specialize->setFixedArrayArgs(arrays.car());
      specialize->setDynArrayArgs(arrays.cdr());
    } break;
    case Clause::Kind::Memsafe:
      decl->setMemsafe();
      break;
    case Clause::Kind::NoAnalyze:
      decl->setNoAnalyze();
      break;
    case Clause::Kind::Force:
      specialize->setForceArgNames(parseVarNameList());
      break;
    case Clause::Kind::Ignore:
      specialize->setIgnoreArgNames(parseVarNameList());
      break;
    case Clause::Kind::IgnoreAll:
      specialize->setIgnoreAllArgNames(parseVarNameList());
      break;
    case Clause::Kind::MaxVersions: {
      int maxVersions = parseSingleIntArg();
      if(maxVersions < 0)
        warning(
            "Max versions must be greater than 0. Setting max versions to -1.",
            token);
      specialize->setMaxVersions(maxVersions);
      break;
    }
    case Clause::Kind::AutoTune: {
      int tuningLevel = parseSingleIntArg();
      if(tuningLevel < 0 or tuningLevel > 3)
        warning("Tuning level must be between 0 and 3 (inclusive). "
                "Setting tuning level to 0",
                token);
      specialize->setTuningLevel(tuningLevel);
      break;
    }
    case Clause::Kind::Name: {
      model->setModelName(parseSingleVarName());
      break;
    }
    default:
      break;
    }
  }
}

std::string Parser::parseSingleVarName() {
  Token token = shift();

  // Fortran is not case-sensitive, so might as well lower things here
  // and save the bother
  SourceLang lang = directives.getSourceLang();
  if(lang == SourceLang::Fort or lang == SourceLang::F77)
    return StringUtils::lower(token.getSpelling());
  else
    return token.getSpelling();
}

Vector<std::string> Parser::parseVarNameList() {
  Vector<std::string> vars;

  // Consume '('
  Token token = shift();
  if(token.isNot(Token::Kind::LParen))
    error("Expected '('", token);

  // Collect names
  while(token.isNot(Token::Kind::RParen) and token.isNot(Token::Kind::Eol)) {
    vars.push_back(parseSingleVarName());

    // Skip ',' if any
    token = peek();
    if(token.is(Token::Kind::Comma))
      token = shift();
    else if(token.isNot(Token::Kind::RParen) and token.isNot(Token::Kind::Eol))
      error("Expected ','", token);
  }

  // Consume ')'
  shift();
  if(token.isNot(Token::Kind::RParen))
    error("Expected ')'", token);

  return vars;
}

void Parser::parseSingleArrayInto(
    Pair<Vector<FixedArray>, Vector<DynArray>>& arrays) {
  Token token = shift();
  std::string array = token.getSpelling();

  // Consume '['
  Token lb = shift();
  if(lb.isNot(Token::Kind::LBracket))
    error("Expected '['", lb);

  // Consume <integer>
  Token sz = shift();

  if(sz.is(Token::Kind::Integer)) {
    unsigned size = std::stoi(sz.getSpelling());
    arrays.car().emplace_back(array, size);
  } else if(sz.is(Token::Kind::Identifier)) {
    arrays.cdr().emplace_back(array, sz.getSpelling());
  } else  {
    error("Expected integer size or argument name", sz);
  }

  // Consume ']'
  Token rb = shift();
  if(rb.isNot(Token::Kind::RBracket))
    error("Expected ']'", rb);
}

Pair<Vector<Parser::FixedArray>, Vector<Parser::DynArray>>
Parser::parseArrayList() {
  Pair<Vector<FixedArray>, Vector<DynArray>> arrays;

  // Consume '('
  Token token = shift();
  if(token.isNot(Token::Kind::LParen))
    error("Expected '('", token);

  // Collect arrays
  while(token.isNot(Token::Kind::RParen) and token.isNot(Token::Kind::Eol)) {
    parseSingleArrayInto(arrays);

    // Skip ',' if any
    token = peek();
    if(token.is(Token::Kind::Comma))
      token = shift();
    else if(token.isNot(Token::Kind::RParen) and token.isNot(Token::Kind::Eol))
      error("Expected ','", token);
  }

  // Consume ')'
  shift();
  if(token.isNot(Token::Kind::RParen))
    error("Expected ')'", token);

  return arrays;
}

int64_t Parser::parseSingleIntArg() {
  int64_t arg = -1;

  // Parse '('
  Token token = shift();
  if(token.isNot(Token::Kind::LParen))
    error("Expected '('", token);

  // Parse constant
  token = shift();
  if(token.isNot(Token::Kind::Integer))
    error("Expected integer", token);
  arg = std::stol(token.getSpelling());

  // Parse ')'
  token = shift();
  if(token.isNot(Token::Kind::RParen))
    error("Expected ')'", token);

  return arg;
}

} // namespace moya
