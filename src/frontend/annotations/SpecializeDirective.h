#ifndef MOYA_ANNOTATIONS_SPECIALIZE_DIRECTIVE_H
#define MOYA_ANNOTATIONS_SPECIALIZE_DIRECTIVE_H

#include "FunctionDirective.h"
#include "common/APITypes.h"
#include "common/Pair.h"

namespace moya {

// #pragma moya specialize [ignore(id0, id1, ...)]
//                         [array(id0[size0], id1[size1], ...)]
//                         [force(id0, id1, ...)]
//                         [max(num)]
//                         [autotune(level)]
class SpecializeDirective : public FunctionDirective {
public:
  using FixedArray = Pair<std::string, unsigned>;
  using DynArray = Pair<std::string, std::string>;

protected:
  Vector<FixedArray> fixedArrayArgs;
  Vector<DynArray> dynArrayArgs;
  Vector<std::string> ignoreArgNames;
  Vector<std::string> ignoreAllArgNames;
  Vector<std::string> forceArgNames;
  unsigned maxVersions;
  unsigned tuningLevel;

public:
  SpecializeDirective(const Location& = Location());
  virtual ~SpecializeDirective() = default;

  void setFixedArrayArgs(const Vector<FixedArray>&);
  void setDynArrayArgs(const Vector<DynArray>&);
  void setForceArgNames(const Vector<std::string>&);
  void setIgnoreArgNames(const Vector<std::string>&);
  void setIgnoreAllArgNames(const Vector<std::string>&);
  void setMaxVersions(unsigned);
  void setTuningLevel(unsigned);

  const Vector<FixedArray>& getFixedArrayArgs() const;
  const Vector<DynArray>& getDynArrayArgs() const;
  const Vector<std::string>& getForceArgNames() const;
  const Vector<std::string>& getIgnoreArgNames() const;
  const Vector<std::string>& getIgnoreAllArgNames() const;
  unsigned getMax() const;
  unsigned getTuningLevel() const;

  virtual std::string str() const override;
  virtual void serialize(Serializer&) const override;
  virtual void deserialize(Deserializer&) override;

public:
  static bool classof(const Directive*);
};

} // namespace moya

#endif // MOYA_ANNOTATIONS_SPECIALIZE_DIRECTIVE_H
