#ifndef MOYA_ANNOTATIONS_LOOP_DIRECTIVE_H
#define MOYA_ANNOTATIONS_LOOP_DIRECTIVE_H

#include "Directive.h"
#include "common/APITypes.h"

namespace moya {

class LoopDirective : public Directive {
protected:
  LoopID id;

protected:
  LoopDirective(Directive::Kind, const Location&);

  void generateLoopID();

  virtual void serialize(Serializer&, bool) const override;
  virtual void deserialize(Deserializer&, bool) override;

public:
  virtual ~LoopDirective() = default;

  LoopID getLoopID() const;

  virtual void setLocation(const Location&) override;

  virtual std::string str() const override = 0;

  virtual void serialize(Serializer&) const override = 0;
  virtual void deserialize(Deserializer&) override = 0;

public:
  static bool classof(const Directive*);
};

} // namespace moya

#endif // MOYA_ANNOTATIONS_LOOP_DIRECTIVE_H
