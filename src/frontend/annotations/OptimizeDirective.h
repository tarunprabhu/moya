#ifndef MOYA_ANNOTATIONS_OPTIMIZE_DIRECTIVE_H
#define MOYA_ANNOTATIONS_OPTIMIZE_DIRECTIVE_H

#include "LoopDirective.h"

namespace moya {

// #pragma moya optimize
class OptimizeDirective : public LoopDirective {
public:
  OptimizeDirective(const Location& = Location());
  virtual ~OptimizeDirective() = default;

  virtual std::string str() const override;
  virtual void serialize(Serializer&) const override;
  virtual void deserialize(Deserializer&) override;

public:
  static bool classof(const Directive*);
};

} // namespace moya

#endif // MOYA_ANNOTATIONS_OPTIMIZE_DIRECTIVE_H
