#ifndef MOYA_ANNOTATIONS_FUNCTION_DIRECTIVE_H
#define MOYA_ANNOTATIONS_FUNCTION_DIRECTIVE_H

#include "Directive.h"

#include <llvm/IR/Function.h>

namespace moya {

// Directives associated with a function
class FunctionDirective : public Directive {
protected:
  FunctionDirective(Directive::Kind, const Location&);

  virtual void serialize(Serializer&, bool) const override;
  virtual void deserialize(Deserializer&, bool) override;

public:
  virtual ~FunctionDirective() = default;

  virtual std::string str() const override = 0;
  virtual void serialize(Serializer&) const override = 0;
  virtual void deserialize(Deserializer&) override = 0;

public:
  static bool classof(const Directive*);
};

} // namespace moya

#endif // MOYA_ANNOTATIONS_FUNCTION_DIRECTIVE_H
