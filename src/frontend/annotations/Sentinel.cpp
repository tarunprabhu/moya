#include "Sentinel.h"

namespace moya {

static const std::string sentinelC("moya");
static const std::string sentinelFortran("$moya");
static const std::string sentinelContinuation("$moya*");

const std::string& Sentinel::get(SourceLang lang) {
  switch(lang) {
  case SourceLang::C:
  case SourceLang::CXX:
  case SourceLang::Cuda:
  case SourceLang::ObjC:
  case SourceLang::ObjCXX:
    return sentinelC;
  case SourceLang::Fort:
  case SourceLang::F77:
    return sentinelFortran;
  default:
    break;
  }
  return sentinelC;
}

const std::string& Sentinel::getC() {
  return get(SourceLang::C);
}

const std::string& Sentinel::getCXX() {
  return get(SourceLang::CXX);
}

const std::string& Sentinel::getFortran() {
  return get(SourceLang::Fort);
}

const std::string& Sentinel::getFortran77() {
  return get(SourceLang::F77);
}

const std::string& Sentinel::getCuda() {
  return get(SourceLang::Cuda);
}

const std::string& Sentinel::getObjC() {
  return get(SourceLang::ObjC);
}

const std::string& Sentinel::getObjCXX() {
  return get(SourceLang::ObjCXX);
}

// Continuation sentinels only make sense ins Fortran
const std::string& Sentinel::getContinuation() {
  return sentinelContinuation;
}

} // namespace moya
