#include "RegionDirective.h"
#include "common/Config.h"
#include "common/PathUtils.h"

#include <sstream>

namespace moya {

RegionDirective::RegionDirective(Directive::Kind kind, const Location& loc)
    : Directive(kind, loc), id(Config::getInvalidRegionID()), name(loc.str()) {
  generateRegionID();
  generateRegionName();
}

void RegionDirective::generateRegionName() {
  if(loc.isValid()) {
    std::stringstream ss;
    ss << PathUtils::basename(loc.getFile()) << ":" << loc.getLine();
    name = ss.str();
  }
}

void RegionDirective::generateRegionID() {
  if(loc.isValid())
    id = Config::generateRegionID(loc);
}

void RegionDirective::setLocation(const Location& loc) {
  Directive::setLocation(loc);
  generateRegionID();
  generateRegionName();
}

void RegionDirective::setBeginLoc(const Location& loc) {
  this->begin = loc;
}

void RegionDirective::setEndLoc(const Location& loc) {
  this->end = loc;
}

const std::string& RegionDirective::getRegionName() const {
  return name;
}

RegionID RegionDirective::getRegionID() const {
  return id;
}

const Location& RegionDirective::getBeginLoc() const {
  return begin;
}

const Location& RegionDirective::getEndLoc() const {
  return end;
}

bool RegionDirective::classof(const Directive* directive) {
  return directive->getKind() > Directive::Kind::MinRegion
         and directive->getKind() < Directive::Kind::MaxRegion;
}

void RegionDirective::serialize(Serializer& s, bool start) const {
  Directive::serialize(s, start);
  if(start) {
    s.add("id", id);
    s.add("name", name);
    s.add("begin", begin);
    s.add("end", end);
  }
}

void RegionDirective::deserialize(Deserializer& d, bool start) {
  Directive::deserialize(d, start);
  if(start) {
    d.deserialize("id", id);
    d.deserialize("name", name);
    d.deserialize("begin", begin);
    d.deserialize("end", end);
  }
}

} // namespace moya
