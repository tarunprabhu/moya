#include "JITDirective.h"
#include "common/Diagnostics.h"

#include <sstream>

namespace moya {

JITDirective::JITDirective(const Location& loc)
    : RegionDirective(Directive::Kind::JIT, loc) {
  ;
}

std::string JITDirective::str() const {
  std::stringstream ss;

  ss << "#pragma moya jit <" << begin.str() << " => " << end.str() << ">";

  return ss.str();
}

void JITDirective::serialize(Serializer& s) const {
  RegionDirective::serialize(s, true);
  RegionDirective::serialize(s, false);
}

void JITDirective::deserialize(Deserializer& d) {
  RegionDirective::deserialize(d, true);
  RegionDirective::deserialize(d, false);
}

bool JITDirective::classof(const Directive* directive) {
  return directive->getKind() == Directive::Kind::JIT;
}

} // namespace moya
