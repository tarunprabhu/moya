#ifndef MOYA_ANNOTATIONS_DECLARE_DIRECTIVE_H
#define MOYA_ANNOTATIONS_DECLARE_DIRECTIVE_H

#include "FunctionDirective.h"

namespace moya {

// # pragma moya declare [memsafe] [allocator]
class DeclareDirective : public FunctionDirective {
protected:
  bool memsafe;
  bool allocator;
  bool noAnalyze;

public:
  DeclareDirective(const Location& loc = Location());
  virtual ~DeclareDirective() = default;

  void setMemsafe(bool = true);
  void setAllocator(bool = true);
  void setNoAnalyze(bool = true);

  bool getMemsafe() const;
  bool getAllocator() const;
  bool getNoAnalyze() const;

  virtual std::string str() const override;
  virtual void serialize(Serializer&) const override;
  virtual void deserialize(Deserializer&) override;

public:
  static bool classof(const Directive*);
};

} // namespace moya

#endif // MOYA_ANNOTATIONS_DECLARE_DIRECTIVE_H
