#ifndef MOYA_ANNOTATIONS_MOYA_CLAUSES_H
#define MOYA_ANNOTATIONS_MOYA_CLAUSES_H

#include <string>

namespace moya {

class Clause {
public:
  enum Kind {
    Allocator,
    Array,
    Memsafe,
    NoAnalyze,
    Force,
    Ignore,
    IgnoreAll,
    MaxVersions,
    AutoTune,
    Name,
    Unknown,
  };

protected:
  Kind kind;

public:
  Clause(Kind);

  bool isAllocator() const;
  bool isArray() const;
  bool isMemsafe() const;
  bool isNoAnalyze() const;
  bool isForce() const;
  bool isIgnore() const;
  bool isIgnoreAll() const;
  bool isMaxVersions() const;
  bool isAutoTune() const;
  bool isName() const;

public:
  static Clause::Kind getKind(const std::string&);
  static const std::string& getName(Clause::Kind);
};

} // namespace moya

#endif // MOYA_ANNOTATIONS_MOYA_CLAUSES_H
