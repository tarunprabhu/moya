#ifndef MOYA_ANNOTATIONS_SENTINEL_H
#define MOYA_ANNOTATIONS_SENTINEL_H

#include "common/APITypes.h"
#include "common/SourceLang.h"

#include <string>

namespace moya {

class Sentinel {
public:
  static const std::string& get(SourceLang);
  static const std::string& getC();
  static const std::string& getCXX();
  static const std::string& getFortran();
  static const std::string& getFortran77();
  static const std::string& getCuda();
  static const std::string& getObjC();
  static const std::string& getObjCXX();

  // Continuation sentinels only make sense ins Fortran
  static const std::string& getContinuation();
};

} // namespace moya

#endif // MOYA_ANNOTATIONS_SENTINEL_H
