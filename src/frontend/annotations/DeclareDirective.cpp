#include "DeclareDirective.h"
#include "common/Config.h"
#include "common/Metadata.h"

#include <sstream>

using namespace llvm;

namespace moya {

DeclareDirective::DeclareDirective(const Location& loc)
    : FunctionDirective(Directive::Kind::Declare, loc), memsafe(false),
      allocator(false), noAnalyze(false) {
  ;
}

void DeclareDirective::setMemsafe(bool memsafe) {
  this->memsafe = memsafe;
}

void DeclareDirective::setAllocator(bool allocator) {
  this->allocator = allocator;
}

void DeclareDirective::setNoAnalyze(bool noAnalyze) {
  this->noAnalyze = noAnalyze;
}

bool DeclareDirective::getMemsafe() const {
  return memsafe;
}

bool DeclareDirective::getAllocator() const {
  return allocator;
}

bool DeclareDirective::getNoAnalyze() const {
  return noAnalyze;
}

std::string DeclareDirective::str() const {
  std::stringstream ss;

  ss << "#pragma moya declare( ";
  if(getMemsafe())
    ss << "memsafe ";
  if(getAllocator())
    ss << "allocator ";
  if(getNoAnalyze())
    ss << "noanalyze ";
  ss << ")";

  return ss.str();
}

void DeclareDirective::serialize(Serializer& s) const {
  FunctionDirective::serialize(s, true);
  s.add("memsafe", getMemsafe());
  s.add("allocator", getAllocator());
  s.add("noanalyze", getNoAnalyze());
  FunctionDirective::serialize(s, false);
}

void DeclareDirective::deserialize(Deserializer& d) {
  FunctionDirective::deserialize(d, true);
  d.deserialize("memsafe", memsafe);
  d.deserialize("allocator", allocator);
  d.deserialize("noanalyze", noAnalyze);
  FunctionDirective::deserialize(d, false);
}

bool DeclareDirective::classof(const Directive* directive) {
  return directive->getKind() == Directive::Kind::Declare;
}

} // namespace moya
