#ifndef MOYA_ANNOTATIONS_TOKEN_H
#define MOYA_ANNOTATIONS_TOKEN_H

#include "common/Location.h"

namespace moya {

class Token {
public:
  enum Kind {
    Identifier,
    Integer,

    LParen,
    RParen,
    LBrace,
    RBrace,
    LBracket,
    RBracket,
    Comma,
    Eol,
    Eof,

    TokenKindUnknown,
  };

private:
  Token::Kind kind;
  std::string spelling;
  Location loc;

public:
  Token(Token::Kind, const std::string&, const Location&);

  Token::Kind getKind() const;
  const std::string& getSpelling() const;
  const std::string& getFile() const;
  unsigned getLine() const;
  unsigned getColumn() const;

  bool is(Token::Kind) const;
  bool isNot(Token::Kind) const;

  bool isIdentifier() const;
  bool isInteger() const;
  bool isEol() const;
  bool isEof() const;
};

} // namespace moya

#endif // MOYA_ANNOTAIONS_COMMON_TOKEN_H
