#include "FunctionDirective.h"

namespace moya {

FunctionDirective::FunctionDirective(Directive::Kind kind, const Location& loc)
    : Directive(kind, loc) {
  ;
}

void FunctionDirective::serialize(Serializer& s, bool start) const {
  Directive::serialize(s, start);
}

void FunctionDirective::deserialize(Deserializer& d, bool start) {
  Directive::deserialize(d, start);
}

bool FunctionDirective::classof(const Directive* directive) {
  return (directive->getKind() > Directive::Kind::MinFunction)
         and (directive->getKind() < Directive::Kind::MaxFunction);
}

} // namespace moya
