#include "Token.h"

namespace moya {

Token::Token(Token::Kind kind, const std::string& spelling, const Location& loc)
    : kind(kind), spelling(spelling), loc(loc) {
  ;
}

Token::Kind Token::getKind() const {
  return kind;
}

const std::string& Token::getSpelling() const {
  return spelling;
}

const std::string& Token::getFile() const {
  return loc.getFile();
}

unsigned Token::getLine() const {
  return loc.getLine();
}

unsigned Token::getColumn() const {
  return loc.getColumn();
}

bool Token::is(Token::Kind kind) const {
  return this->kind == kind;
}

bool Token::isNot(Token::Kind kind) const {
  return not is(kind);
}

bool Token::isIdentifier() const {
  return kind == Token::Kind::Identifier;
}

bool Token::isInteger() const {
  return kind == Token::Kind::Integer;
}

bool Token::isEol() const {
  return kind == Token::Kind::Eol;
}

bool Token::isEof() const {
  return kind == Token::Kind::Eof;
}

} // namespace moya
