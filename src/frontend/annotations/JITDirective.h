#ifndef MOYA_ANNOTATIONS_JIT_DIRECTIVE_H
#define MOYA_ANNOTATIONS_JIT_DIRECTIVE_H

#include "RegionDirective.h"

namespace moya {

// #pragma moya jit
class JITDirective : public RegionDirective {
public:
  JITDirective(const Location& = Location());
  virtual ~JITDirective() = default;

  virtual std::string str() const override;
  virtual void serialize(Serializer&) const override;
  virtual void deserialize(Deserializer&) override;

public:
  static bool classof(const Directive*);
};

} // namespace moya

#endif // MOYA_ANNOTATIONS_JIT_DIRECTIVE_H
