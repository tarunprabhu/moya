#ifndef MOYA_ANNOTATIONS_MODEL_DIRECTIVE_H
#define MOYA_ANNOTATIONS_MODEL_DIRECTIVE_H

#include "FunctionDirective.h"

#include <string>

namespace moya {

class ModelDirective : public FunctionDirective {
protected:
  // The actual model name to use. This should have been registered with
  // Aeryn, else there will be an error during the analysis
  std::string modelName;

public:
  ModelDirective(const Location& = Location());
  virtual ~ModelDirective() = default;

  void setModelName(const std::string&);

  bool hasModelName() const;
  const std::string& getModelName() const;

  virtual std::string str() const override;
  virtual void serialize(Serializer&) const override;
  virtual void deserialize(Deserializer&) override;

public:
  static bool classof(const Directive*);
};

} // namespace moya

#endif // MOYA_ANNOTATIONS_MODEL_DIRECTIVE_H
