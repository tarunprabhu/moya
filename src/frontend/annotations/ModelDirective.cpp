#include "ModelDirective.h"

namespace moya {

ModelDirective::ModelDirective(const Location& loc)
    : FunctionDirective(Directive::Kind::Model, loc), modelName("") {
  ;
}

void ModelDirective::setModelName(const std::string& name) {
  modelName = name;
}

bool ModelDirective::hasModelName() const {
  return modelName.length();
}

const std::string& ModelDirective::getModelName() const {
  return modelName;
}

std::string ModelDirective::str() const {
  std::stringstream ss;

  ss << "#pragma moya model(";
  ss << "name = " << modelName;
  ss << ")";

  return ss.str();
}

void ModelDirective::serialize(Serializer& s) const {
  FunctionDirective::serialize(s, true);
  s.add("model", getModelName());
  FunctionDirective::serialize(s, false);
}

void ModelDirective::deserialize(Deserializer& d) {
  FunctionDirective::deserialize(d, true);
  d.deserialize("model", modelName);
  FunctionDirective::deserialize(d, false);
}

bool ModelDirective::classof(const Directive* directive) {
  return directive->getKind() == Directive::Kind::Model;
}

} // namespace moya
