#ifndef MOYA_ANNOTATIONS_PARSER_H
#define MOYA_ANNOTATIONS_PARSER_H

#include "Directives.h"
#include "Token.h"
#include "common/Location.h"
#include "common/Queue.h"
#include "common/SourceLang.h"

#include <sstream>

namespace moya {

class DirectiveContext;

class Parser {
private:
  DirectiveContext& directives;
  Queue<Token> stream;

public:
  using FixedArray = Pair<std::string, unsigned>;
  using DynArray = Pair<std::string, std::string>;

private:
  Directive* parseDirective();
  void parseMoyaClauses(Directive*);
  std::string parseSingleVarName();
  Vector<std::string> parseVarNameList();
  void parseSingleArrayInto(Pair<Vector<FixedArray>, Vector<DynArray>>&);
  Pair<Vector<FixedArray>, Vector<DynArray>> parseArrayList();
  int64_t parseSingleIntArg();

  void error(const std::string&, const Token&);
  void warning(const std::string&, const Token&);

  Token shift();
  Token& peek();

  void addToken(std::stringstream&, const std::string&, unsigned, unsigned);
  void lex(const std::string&, const std::string&, unsigned, unsigned);

public:
  Parser(DirectiveContext&);

  Directive* parse(const std::string&, const Location&);
};

} // namespace moya

#endif // MOYA_ANNOTATIONS_PARSER_H
