#include "SpecializeDirective.h"
#include "common/Config.h"
#include "common/Diagnostics.h"
#include "common/Metadata.h"

#include <sstream>

using namespace llvm;

namespace moya {

SpecializeDirective::SpecializeDirective(const Location& loc)
    : FunctionDirective(Directive::Kind::Specialize, loc), maxVersions(-1),
      tuningLevel(0) {
  ;
}

void SpecializeDirective::setFixedArrayArgs(
    const Vector<FixedArray>& arrayArgs) {
  this->fixedArrayArgs = arrayArgs;
}

void SpecializeDirective::setDynArrayArgs(const Vector<DynArray>& arrayArgs) {
  this->dynArrayArgs = arrayArgs;
}

void SpecializeDirective::setForceArgNames(
    const Vector<std::string>& argNames) {
  this->forceArgNames = argNames;
}

void SpecializeDirective::setIgnoreArgNames(
    const Vector<std::string>& argNames) {
  this->ignoreArgNames = argNames;
}

void SpecializeDirective::setIgnoreAllArgNames(
    const Vector<std::string>& argNames) {
  this->ignoreAllArgNames = argNames;
}

void SpecializeDirective::setMaxVersions(unsigned maxVersions) {
  this->maxVersions = maxVersions;
}

void SpecializeDirective::setTuningLevel(unsigned level) {
  this->tuningLevel = level;
}

const Vector<SpecializeDirective::FixedArray>&
SpecializeDirective::getFixedArrayArgs() const {
  return fixedArrayArgs;
}

const Vector<SpecializeDirective::DynArray>&
SpecializeDirective::getDynArrayArgs() const {
  return dynArrayArgs;
}

const Vector<std::string>& SpecializeDirective::getForceArgNames() const {
  return forceArgNames;
}

const Vector<std::string>& SpecializeDirective::getIgnoreArgNames() const {
  return ignoreArgNames;
}

const Vector<std::string>& SpecializeDirective::getIgnoreAllArgNames() const {
  return ignoreAllArgNames;
}

unsigned SpecializeDirective::getMax() const {
  return maxVersions;
}

unsigned SpecializeDirective::getTuningLevel() const {
  return tuningLevel;
}

std::string SpecializeDirective::str() const {
  std::stringstream ss;

  ss << "#pragma moya specialize(";
  ss << "fixedarray = " << fixedArrayArgs.str() << ", ";
  ss << "dynarray = " << dynArrayArgs.str() << ", ";
  ss << "force = " << forceArgNames.str() << ", ";
  ss << "ignore = " << ignoreArgNames.str() << ", ";
  ss << "ignore_all = " << ignoreAllArgNames.str() << ", ";
  ss << "max = " << maxVersions << ", ";
  ss << "tuning = " << tuningLevel;
  ss << ")";

  return ss.str();
}

void SpecializeDirective::serialize(Serializer& s) const {
  FunctionDirective::serialize(s, true);
  s.add("fixedarray", fixedArrayArgs);
  s.add("dynarray", dynArrayArgs);
  s.add("force", forceArgNames);
  s.add("ignore", ignoreArgNames);
  s.add("ignore_all", ignoreAllArgNames);
  s.add("max", maxVersions);
  s.add("tuning", tuningLevel);
  FunctionDirective::serialize(s, false);
}

void SpecializeDirective::deserialize(Deserializer& d) {
  FunctionDirective::deserialize(d, true);
  d.deserialize("fixedarray", fixedArrayArgs);
  d.deserialize("dynarray", dynArrayArgs);
  d.deserialize("force", forceArgNames);
  d.deserialize("ignore", ignoreArgNames);
  d.deserialize("ignore_all", ignoreAllArgNames);
  d.deserialize("max", maxVersions);
  d.deserialize("tuning", tuningLevel);
  FunctionDirective::deserialize(d, false);
}

bool SpecializeDirective::classof(const Directive* directive) {
  return directive->getKind() == Directive::Kind::Specialize;
}

} // namespace moya
