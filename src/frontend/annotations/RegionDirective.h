#ifndef MOYA_ANNOTATIONS_REGION_DIRECTIVE_H
#define MOYA_ANNOTATIONS_REGION_DIRECTIVE_H

#include "Directive.h"
#include "common/APITypes.h"

namespace moya {

// Directives associated with a region
class RegionDirective : public Directive {
protected:
  // These are the locations of the opening and closing brace whereas loc
  // which is inherited from MoyaDirective is the location of the pragma
  // declaring the region. In the case of Fortran, begin will be the same as
  // MoyaDirective::loc and end is the location of "!$moya end <region>"
  Location begin;
  Location end;

  RegionID id;

  // This is mean to be a user-friendly name to identify a region. Right now,
  // there is no way for the user to specify it, but it could be added
  // someday
  std::string name;

protected:
  RegionDirective(Directive::Kind, const Location&);

  void generateRegionName();
  void generateRegionID();

  virtual void serialize(Serializer&, bool) const override;
  virtual void deserialize(Deserializer&, bool) override;

public:
  virtual ~RegionDirective() = default;

  virtual void setLocation(const Location&) override;
  void setBeginLoc(const Location&);
  void setEndLoc(const Location&);

  RegionID getRegionID() const;
  const std::string& getRegionName() const;
  const Location& getBeginLoc() const;
  const Location& getEndLoc() const;

  virtual std::string str() const override = 0;
  virtual void serialize(Serializer&) const override = 0;
  virtual void deserialize(Deserializer&) override = 0;

public:
  static bool classof(const Directive*);
};

} // namespace moya

#endif // MOYA_ANNOTATIONS_REGION_DIRECTIVE_H
