#include "Clauses.h"
#include "common/Map.h"
#include "common/StringUtils.h"

namespace moya {

static const Map<std::string, Clause::Kind> n2k = {
    // Assert that this function is a memory allocator. It may return the
    // result of malloc or may allocate memory some other way
    {"allocator", Clause::Kind::Allocator},

    // ARGS: (id0[size0], id1[size1], ...)
    // Assert that the arguments in the list are arrays with the specified
    // size
    {"array", Clause::Kind::Array},

    // If present, will ignore any "unsafe" instructions like ptrtoint and
    // inttoptr in the function
    {"memsafe", Clause::Kind::Memsafe},

    // If present, the function will not be analyzed
    {"noanalyze", Clause::Kind::NoAnalyze},

    // ARGS: (id0, id1, ...)
    // Always use the arguments in the list when specializing
    // even if the static analysis would otherwise ignore it
    {"force", Clause::Kind::Force},

    // ARGS: (id0, id1, ...)
    // Do not use thie arguments in the list when specializing
    {"ignore", Clause::Kind::Ignore},

    // ARGS: (id0, id1, ...)
    // This is only used in Fortran to ignore both the argument and any others
    // that are related to it. This is mainly for arrays because arrays are
    // passed as two arguments - the pointer and the metadata
    {"ignore_all", Clause::Kind::IgnoreAll},

    // ARGS: (int)
    // The maximum number of specializations to generate for this function
    {"max", Clause::Kind::MaxVersions},

    // ARGS: (int)
    // Do runtime autotuning. The integer is a "level" of tuning vaguely
    // similar to the compilers optimization levels
    {"autotune", Clause::Kind::AutoTune},
    {"name", Clause::Kind::Name},
};

static const std::string unknownClauseName = "<unknown>";

Clause::Clause(Clause::Kind kind) : kind(kind) {
  ;
}

Clause::Kind Clause::getKind(const std::string& name) {
  std::string lower = StringUtils::lower(name);
  if(n2k.contains(lower))
    return n2k.at(lower);
  return Clause::Kind::Unknown;
}

const std::string& Clause::getName(Clause::Kind kind) {
  for(const auto& it : n2k)
    if(it.second == kind)
      return it.first;
  return unknownClauseName;
}

bool Clause::isAllocator() const {
  return kind == Clause::Kind::Allocator;
}

bool Clause::isArray() const {
  return kind == Clause::Kind::Array;
}

bool Clause::isMemsafe() const {
  return kind == Clause::Kind::Memsafe;
}

bool Clause::isNoAnalyze() const {
  return kind == Clause::Kind::NoAnalyze;
}

bool Clause::isForce() const {
  return kind == Clause::Kind::Force;
}

bool Clause::isIgnore() const {
  return kind == Clause::Kind::Ignore;
}

bool Clause::isIgnoreAll() const {
  return kind == Clause::Kind::IgnoreAll;
}

bool Clause::isMaxVersions() const {
  return kind == Clause::Kind::MaxVersions;
}

bool Clause::isAutoTune() const {
  return kind == Clause::Kind::AutoTune;
}

bool Clause::isName() const {
  return kind == Clause::Kind::Name;
}

} // namespace moya
