#include "OptimizeDirective.h"
#include "common/Deserializer.h"
#include "common/Serializer.h"

#include <sstream>

namespace moya {

OptimizeDirective::OptimizeDirective(const Location& loc)
    : LoopDirective(Directive::Kind::Optimize, loc) {
  ;
}

std::string OptimizeDirective::str() const {
  std::stringstream ss;

  ss << "#pragma moya optimize";

  return ss.str();
}

void OptimizeDirective::serialize(Serializer& s) const {
  LoopDirective::serialize(s, true);
  LoopDirective::serialize(s, false);
}

void OptimizeDirective::deserialize(Deserializer& d) {
  LoopDirective::deserialize(d, true);
  LoopDirective::deserialize(d, false);
}

bool OptimizeDirective::classof(const Directive* directive) {
  return directive->getKind() == Directive::Kind::Optimize;
}

} // namespace moya
