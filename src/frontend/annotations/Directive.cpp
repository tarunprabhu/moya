#include "Directive.h"
#include "Directives.h"
#include "common/Diagnostics.h"
#include "common/Map.h"
#include "common/StringUtils.h"

namespace moya {

static const Map<std::string, Directive::Kind> n2k
    = {{"specialize", Directive::Kind::Specialize},
       {"jit", Directive::Kind::JIT},
       {"declare", Directive::Kind::Declare},
       {"optimize", Directive::Kind::Optimize},
       {"model", Directive::Kind::Model},
       {"<unknown>", Directive::Kind::Unknown}};

Directive::Directive(Directive::Kind kind, const Location& loc)
    : kind(kind), spelling(Directive::getSpelling(kind)), loc(loc) {
  ;
}

template <>
Vector<Directive::Kind> Directive::getKinds<RegionDirective>() {
  return {
      Directive::Kind::JIT,
  };
}

template <>
Vector<Directive::Kind> Directive::getKinds<LoopDirective>() {
  return {
      Directive::Kind::Optimize,
  };
}

template <>
Vector<Directive::Kind> Directive::getKinds<FunctionDirective>() {
  return {
      Directive::Kind::Specialize,
      Directive::Kind::Declare,
      Directive::Kind::Model,
  };
}

bool Directive::isClauseAllowed(Clause::Kind ckind) const {
  switch(getKind()) {
  case Directive::Kind::Specialize:
    switch(ckind) {
    case Clause::Kind::Array:
    case Clause::Kind::Force:
    case Clause::Kind::Ignore:
    case Clause::Kind::IgnoreAll:
    case Clause::Kind::MaxVersions:
    case Clause::Kind::AutoTune:
      return true;
    default:
      break;
    }
    break;
  case Directive::Kind::JIT:
    switch(ckind) {
    default:
      break;
    }
    break;
  case Directive::Kind::Declare:
    switch(ckind) {
    case Clause::Kind::Allocator:
    case Clause::Kind::Memsafe:
    case Clause::Kind::NoAnalyze:
      return true;
    default:
      break;
    }
    break;
  case Directive::Kind::Optimize:
    switch(ckind) {
    default:
      break;
    }
    break;
  case Directive::Kind::Model:
    switch(ckind) {
    case Clause::Kind::Name:
      return true;
    default:
      break;
    }
    break;
  default:
    break;
  }
  return false;
}

Directive::Kind Directive::getKind(const std::string& spelling) {
  std::string lowered = StringUtils::lower(spelling);
  if(n2k.contains(lowered))
    return n2k.at(lowered);
  return Directive::Kind::Unknown;
}

const std::string& Directive::getSpelling(Directive::Kind kind) {
  for(const auto& it : n2k)
    if(it.second == kind)
      return it.first;
  return getSpelling(Directive::Kind::Unknown);
}

void Directive::setLocation(const Location& loc) {
  this->loc = loc;
}

Directive::Kind Directive::getKind() const {
  return kind;
}

const std::string& Directive::getSpelling() const {
  return spelling;
}

const Location& Directive::getLocation() const {
  return loc;
}

Directive* Directive::deserializeFrom(Deserializer& d) {
  Directive::Kind kind;

  d.pushCheckPoint();
  d.mapStart();
  d.deserialize("kind", kind);
  d.popCheckPoint();

  Directive* directive = nullptr;
  switch(kind) {
  case Directive::Kind::JIT:
    directive = new JITDirective();
    break;
  case Directive::Kind::Declare:
    directive = new DeclareDirective();
    break;
  case Directive::Kind::Specialize:
    directive = new SpecializeDirective();
    break;
  case Directive::Kind::Optimize:
    directive = new OptimizeDirective();
    break;
  case Directive::Kind::Model:
    directive = new ModelDirective();
    break;
  default:
    moya_error("Unsupported directive kind to deserializer: " << kind);
    break;
  }
  directive->deserialize(d);

  return directive;
}

void Directive::serialize(Serializer& s, bool start) const {
  if(start) {
    s.mapStart();
    s.add("kind", kind);
    s.add("location", loc);
  } else {
    s.mapEnd();
  }
}

void Directive::deserialize(Deserializer& d, bool start) {
  if(start) {
    d.mapStart();
    d.deserialize("kind", kind);
    d.deserialize("location", loc);
  } else {
    d.mapEnd();
  }
}

} // namespace moya
