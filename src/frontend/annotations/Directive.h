#ifndef MOYA_ANNOTATIONS_MOYA_DIRECTIVE_H
#define MOYA_ANNOTATIONS_MOYA_DIRECTIVE_H

#include "Clauses.h"
#include "common/Deserializer.h"
#include "common/Location.h"
#include "common/Serializer.h"
#include "common/Vector.h"

#include <string>

namespace moya {

// # pragma moya ...
class Directive {
public:
  enum Kind {
    MinFunction, // Begin marker for function directives
    Specialize,
    Declare,
    Model,
    MaxFunction, // End marker for function directives

    MinRegion, // Begin marker for region directives
    JIT,
    MaxRegion, // End marker for region directives

    MinLoop, // Begin marker for loop directives
    Optimize,
    MaxLoop, // End marker for loop directives

    Unknown,
  };

protected:
  Directive::Kind kind;
  std::string spelling;
  Location loc;

protected:
  Directive(Directive::Kind, const Location& = Location());

  virtual void serialize(Serializer&, bool) const;
  virtual void deserialize(Deserializer&, bool);

public:
  virtual ~Directive() = default;

  virtual void setLocation(const Location&);

  const Location& getLocation() const;
  Directive::Kind getKind() const;
  const std::string& getSpelling() const;

  bool isClauseAllowed(Clause::Kind) const;

  virtual std::string str() const = 0;
  virtual void serialize(Serializer&) const = 0;
  virtual void deserialize(Deserializer&) = 0;

public:
  static const std::string& getSpelling(Directive::Kind);
  static Directive::Kind getKind(const std::string&);
  static Directive* deserializeFrom(Deserializer&);

  template <typename T>
  static Vector<Directive::Kind> getKinds();
};

} // namespace moya

#endif // MOYA_COMMON_MOYA_DIRECTIVE_H
